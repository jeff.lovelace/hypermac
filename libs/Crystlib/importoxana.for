      subroutine ImportOxana(ich)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      character*256 :: InMaps=' ',
     1                 InStructure=' ',
     2                 OutStructure,Veta,EdwStringQuest,t256
      character*80 Filter
      logical :: ExistFile,StructureExists,FeYesNo,EqIgCase,lpom,
     1           KeepDirectory=.false.
      real xp(6)
      real, allocatable :: Table(:,:,:,:,:,:)
      ln=0
      OutStructure=fln
      KeepDirectory=.false.
      ich=0
      id=NextQuestId()
      xqd=450.
      il=3
      Veta='Define input files and output structure'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,1,LightGray,0,0)
      Veta='%Information file:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+15.
      dpom=250.
      xpomb=xpom+dpom+10.
      dpomb=xqd-xpomb-10.
      il=0
      do i=1,3
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,'Browse')
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          Veta='%DFT maps:'
          nEdwInfoFile=EdwLastMade
          nButtInfoFile=ButtonLastMade
          call FeQuestStringEdwOpen(EdwLastMade,InStructure)
        else if(i.eq.2) then
          Veta='%Output structure:'
          nEdwMaps=EdwLastMade
          nButtMaps=ButtonLastMade
          call FeQuestStringEdwOpen(EdwLastMade,InMaps)
        else if(i.eq.3) then
          nEdwStructure=EdwLastMade
          nButtStructure=ButtonLastMade
          call FeQuestStringEdwOpen(EdwLastMade,OutStructure)
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        OutStructure=EdwStringQuest(nEdwStructure)
        if(StructureExists(OutStructure)) then
          if(FeYesNo(-1.,YBottomMessage,'The structure "'//
     1               OutStructure(:idel(OutStructure))//
     2                '" already exists, rewrite it?',0))
     3      QuestCheck(id)=0
        else
          QuestCheck(id)=0
        endif
        go to 1500
      else if(CheckType.eq.EventButton) then
        nEdw=CheckNumber-nButtInfoFile+nEdwInfoFile
        t256=EdwStringQuest(nEdw)
        if(CheckNumber.eq.nButtInfoFile) then
          Veta='Browse for the information file'
          i=0
          Filter='*.txt'
        else if(CheckNumber.eq.nButtMaps) then
          Veta='Browse for the file with maps'
          i=0
          Filter='*.asc'
        else if(CheckNumber.eq.nButtStructure) then
          Veta='Browse for the structure name'
          i=1
          Filter=' '
        endif
        call FeFileManager(Veta,t256,Filter,i,KeepDirectory,ich)
        if(ich.ne.0) go to 1500
        KeepDirectory=.true.
        call FeQuestStringEdwOpen(nEdw,t256)
        EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.eq.EventEdw) then
        nEdw=CheckNumber
        nButt=nEdw-nEdwInfoFile+nButtInfoFile
        Veta=EdwStringQuest(CheckNumber)
        if(nEdw.ne.nEdwStructure.and.Veta.ne.' '.and.
     1     (EventTypeSave.ne.EventButton.or.EventNumberSave.ne.nButt))
     2    then
          if(.not.ExistFile(Veta)) then
            Veta='The input file "'//Veta(:idel(Veta))//
     1           '" doesn''t exist, try once more.'
            call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
            EventType=EventEdw
            EventNumber=nEdw
          endif
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        InStructure=EdwStringQuest(nEdwInfoFile)
        OutStructure=EdwStringQuest(nEdwStructure)
        InMaps=EdwStringQuest(nEdwMaps)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0.or.InStructure.eq.' '.or.OutStructure.eq.' '.or.
     1   InMaps.eq.' ') then
        ich=1
        go to 9999
      endif
!      fln=OutStructure
!      ifln=idel(fln)
      MaxNDim=3
      MaxNDimI=0
      call SetBasicM50
      ChargeDensities=.false.
      KPhase=1
      NDatBlock=1
      KDatBlock=1
      call ReallocSymm(NDim(KPhase),48,8,1,1,0)
      if(allocated(AtType))
     1  deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2             AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3             FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4             TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
      if(allocated(TypeCore))
     1  deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2             HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3             CoreValSource)
      n=20
      m=121
      allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1         AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2         AtWeight(n,NPhase),AtRadius(n,NPhase),AtColor(n,NPhase),
     3         AtNum(n,NPhase),AtVal(n,NPhase),AtMult(n,NPhase),
     4         FFBasic(m,n,NPhase),
     5         FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6         FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7         FFra(n,NPhase,i),FFia(n,NPhase,i),
     8         FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9         TypeFFMag(n,NPhase),
     a         FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1         FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
      allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1         PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2         ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3         HNSlater(n,NPhase),HZSlater(n,NPhase),
     4         ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5         NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6         CoreValSource(n,NPhase))
      FFCoreD=0.
      FFValD=0.
      CoreValSource='Default'
      ln=NextLogicNumber()
      call OpenFile(ln,InStructure,'formatted','unknown')
2000  read(ln,FormA,end=2500) Veta
      k=0
      call Kus(Veta,k,t256)
      if(EqIgCase(t256,'dimension')) then
        call StToInt(Veta,k,NDim,1,.false.,ich)
        NDimI(KPhase)=NDim(KPhase)-3
        NDimQ(KPhase)=NDim(KPhase)**2
        MaxNDim=max(MaxNDim,NDim(KPhase))
        MaxNDimI=MaxNDim-3
      else if(EqIgCase(t256,'cell')) then
        call StToReal(Veta,k,CellPar(1,1,KPhase),6,.false.,ich)
      else if(EqIgCase(t256,'centers')) then
        NLattVec(KPhase)=0
2100    read(ln,FormA,end=2500) Veta
        k=0
        call Kus(Veta,k,t256)
        if(EqIgCase(t256,'endcenters')) go to 2000
        k=0
        NLattVec(KPhase)=NLattVec(KPhase)+1
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,0)
        call StToReal(Veta,k,vt6(1,NLattVec(KPhase),1,KPhase),
     1                NDim(KPhase),.false.,ich)
        go to 2100
      else if(EqIgCase(t256,'symmetry')) then
        NSymm(KPhase)=0
2200    read(ln,FormA,end=2500) Veta
        k=0
        call Kus(Veta,k,t256)
        if(EqIgCase(t256,'endsymmetry')) go to 2000
        k=0
        NSymm(KPhase)=NSymm(KPhase)+1
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,0)
        call ReadSymm(Veta,rm6(1,NSymm(KPhase),1,KPhase),
     1                s6(1,NSymm(KPhase),1,KPhase),
     2                symmc(1,NSymm(KPhase),1,KPhase),
     3                zmag(NSymm(KPhase),1,KPhase),0)
        call MatBlock3(rm6(1,NSymm(KPhase),1,KPhase),
     1                 rm(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        ISwSymm(NSymm(KPhase),1,KPhase)=1
        call CrlMakeRMag(NSymm(KPhase),1)
        go to 2200
      else if(EqIgCase(t256,'atoms')) then
        NAtFormula(KPhase)=0
        MaxNAtFormula=0
        nac=0
2300    read(ln,FormA,end=2500) Veta
        if(Veta.eq.' ') go to 2300
        k=0
        call Kus(Veta,k,t256)
        if(EqIgCase(t256,'endatoms')) go to 2000
        do i=idel(t256),1,-1
          if(Index(Cifry(1:10),t256(i:i)).gt.0) then
            t256(i:i)=' '
          else
            exit
          endif
        enddo
        nac=nac+1
        do i=1,NAtFormula(KPhase)
          if(EqIgCase(AtType(i,KPhase),t256)) then
            go to 2300
          endif
        enddo
        NAtFormula(KPhase)=NAtFormula(KPhase)+1
        MaxNAtFormula=max(MaxNAtFormula,NAtFormula(KPhase))
        call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
        AtTypeFull(NAtFormula(KPhase),KPhase)=t256
        AtTypeMag(NAtFormula(KPhase),KPhase)=' '
        AtTypeMagJ(NAtFormula(KPhase),KPhase)=' '
        call Uprat(AtTypeFull(NAtFormula(KPhase),KPhase))
        call GetPureAtType(AtTypeFull(NAtFormula(KPhase),KPhase),
     1                     AtType(NAtFormula(KPhase),KPhase))
        call EM50ReadOneFormFactor(NAtFormula(KPhase))
        call EM50OneFormFactorSet(NAtFormula(KPhase))
        j=LocateInStringArray(atn,98,
     1  AtType(NAtFormula(KPhase),KPhase),IgnoreCaseYes)
        if(j.gt.0) then
          AtNum(NAtFormula(KPhase),KPhase)=float(j)
        else
          AtNum(NAtFormula(KPhase),KPhase)=0.
        endif
        go to 2300
      endif
      go to 2000
2500  Formula(KPhase)=' '
      do i=1,NAtFormula(KPhase)
        Formula(KPhase)=Formula(KPhase)(:idel(Formula(KPhase)))//' '//
     1    AtTypeFull(i,KPhase)
      enddo
      Formula(KPhase)=Formula(KPhase)(2:)
      NUnits(KPhase)=1
      call FindSmbSg(Grupa(KPhase),.false.,1)
      call iom50(1,0,OutStructure(:idel(OutStructure))//'.m50')
      call iom50(0,0,OutStructure(:idel(OutStructure))//'.m50')
      rewind(ln)
      call AllocateAtoms(nac)
      xp=0.
3000  read(ln,FormA,end=2500) Veta
      k=0
      call Kus(Veta,k,t256)
      if(EqIgCase(t256,'atoms')) then
        nac=0
3100    read(ln,FormA,end=3500) Veta
        if(Veta.eq.' ') go to 3100
        k=0
        call Kus(Veta,k,t256)
        if(EqIgCase(t256,'endatoms')) go to 3500
        Cislo=t256
        do i=idel(Cislo),1,-1
          if(Index(Cifry(1:10),Cislo(i:i)).gt.0) then
            Cislo(i:i)=' '
          else
            exit
          endif
        enddo
        nac=nac+1
        call SetBasicKeysForAtom(nac)
        Atom(nac)=t256
        isf(nac)=max(KtAt(AtType(1,KPhase),NAtFormula(KPhase),Cislo),1)
        call StToReal(Veta,k,x(1,nac),3,.false.,ich)
        sx(1:3,nac)=0.
        call StToReal(Veta,k,beta(1,nac),6,.false.,ich)
        sbeta(1:3,nac)=0.
        pom=0.
        do i=2,6
          pom=pom+abs(beta(i,nac))
        enddo
        if(pom.le.0.) then
          itf(nac)=1
        else
          itf(nac)=2
        endif
!        iswa(nac)=1
        call SpecPos(x(1,nac),xp,0,iswa(nac),.01,nocc)
        ai(nac)=1./float(nocc)
        sai(nac)=0.
!        ifr(nac)=0
        if(nac.eq.1) then
          PrvniKiAtomu(nac)=ndoff+1
        else
          PrvniKiAtomu(nac)=PrvniKiAtomu(nac-1)+mxda
        endif
        DelkaKiAtomu(nac)=mxda
        call SetIntArrayTo(KiA(1,nac),mxda,0)
        NAtIndLen(1,KPhase)=nac
        call EM40UpdateAtomLimits
        go to 3100
      endif
      go to 3000
3500  call CloseIfOpened(ln)
      call SetFormula(Formula(KPhase))
      call iom50(1,0,OutStructure(:idel(OutStructure))//'.m50')
      call iom40(1,0,OutStructure(:idel(OutStructure))//'.m40')
      fln=OutStructure
      ifln=idel(fln)
      call OpenFile(ln,InMaps,'formatted','unknown')
      read(ln,FormA) Veta
      read(ln,FormA) Veta
      xfmn=0.
      nx=1
      read(Veta,*) nx(1:NDim(KPhase))
      do i=1,6
        if(i.le.NDim(KPhase)) then
          xdf(i)=1./float(nx(i))
          xfmx(i)=1.-xdf(i)
        else
          xdf(i)=1.
          xfmx(i)=0.
        endif
        iorien(i)=i
      enddo
      nxny=nx(1)*nx(2)
      nmap=nx(3)*nx(4)*nx(5)*nx(6)
      mapa=4
      j=1
      lpom=.false.
      lno=NextLogicNumber()
      call OpenMaps(lno,fln(:ifln)//'.m81',nxny,1)
      write(lno,rec=1) nx,nxny,nmap,(xfmn(i),xfmx(i),i=1,6),xdf,iorien,
     1                 mapa,j,lpom,lpom
      read(ln,FormA) Veta
      read(ln,'(2e12.6)') rmin,rmax
      allocate(Table(nx(1),nx(2),nx(3),nx(4),nx(5),nx(6)))
      read(ln,'(6e13.6)') Table
      iz=1
      do i6=1,nx(6)
        do i5=1,nx(5)
          do i4=1,nx(4)
            do i3=1,nx(3)
              iz=iz+1
              write(lno,rec=iz) Table(1:nx(1),1:nx(2),i3,i4,i5,i6)
            enddo
          enddo
        enddo
      enddo
      iz=iz+1
      write(lno,rec=iz) rmax,rmin
      close(lno)
      deallocate(Table)
      Uloha='Peak search'
      call OpenFile(lst,fln(:ifln)//'.fou','formatted','unknown')
      call DefaultFourier
      npeaks(1)=300
      npeaks(2)=0
      kharm=1
      YMinFlowChart=-1.
      call FouPeaks(0,0)
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
9999  call CloseIfOpened(ln)
      return
      end
