      subroutine CrlMagTester(ich)
      use Atoms_mod
      use Basic_mod
      use Refine_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension io(3),nd(5),rmx(9),ZMagO(:),TrMat(36),TrShift(6),
     1          s6o(:,:),Shift(6)
      character*256 FlnOld,Veta
      character*80  t80,p80,StMag(:,:,:)
      character*8   SvFile,AtMag(:),At,At1
      character*3   t3
      character*2   t2m
      integer CrlCentroSymm,CrlMagInver,UseTabsIn
      logical EqRv,FileDiff,FeYesNo,EqIgCase,FeYesNoHeader,ExistFile,
     1        VlnyJinak,StructureExists,PsatPosun,EqRV0,RefineEnd
      external CrlMagTesterCheck,FeVoid
      allocatable ZMagO,s6o,AtMag,StMag
      NAtMag=0
      do i=1,NAtAll
        if(MagPar(i).gt.0) NAtMag=NAtMag+1
      enddo
      allocate(AtMag(NAtMag),StMag(6,0:MagParMax-1,NAtMag))
      j=0
      do i=1,NAtAll
        if(MagPar(i).gt.0) then
          j=j+1
          AtMag(j)=Atom(i)
        endif
      enddo
      if(OpSystem.ge.0) then
        SvFile='full.pcx'
      else
        SvFile='full.bmp'
      endif
      ich=0
      if(FromRepAnalysis.or.FromGoToSub)
     1  call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Finish')
      mxe=100
      mxep=5
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      allocate(lnp(mxe),pnp(mxep,mxe),npa(mxe),pko(mxep,mxe),
     1         lat(mxe),pat(mxep,mxe),lpa(mxe),ppa(mxep,mxe),
     2         lnpo(mxe),pab(mxe),eqhar(mxe))
      KPhaseIn=KPhase
      FlnOld=Fln
      Fln=Fln(:ifln)//'_tmp'
      iFln=idel(Fln)
      i=0
1100  if(StructureExists(Fln)) then
        i=i+1
        write(Cislo,'(i5)') i
        call Zhusti(Cislo)
        Fln=Fln(:iFln)//'_'//Cislo(:idel(Cislo))
        go to 1100
      endif
      iFln=idel(Fln)
      iFlnOld=idel(FlnOld)
      call CopyFile(FlnOld(:iFlnOld)//'.m40',Fln(:iFln)//'.m40')
      if(isPowder)
     1  call CopyFile(FlnOld(:iFlnOld)//'.m41',Fln(:iFln)//'.m41')
      call CopyFile(FlnOld(:iFlnOld)//'.m50',Fln(:iFln)//'.m50')
      call CopyFile(FlnOld(:iFlnOld)//'.m90',Fln(:iFln)//'.m90')
      call iom50(0,0,Fln(:iFln)//'.m50')
      if(.not.FromRepAnalysis) then
        if(allocated(IGen)) deallocate(IGen,KGen,StGen,IoGen)
        allocate(IGen(NSymm(KPhase)),KGen(NSymm(KPhase)),
     1           StGen(NSymm(KPhase)),ioGen(3,NSymm(KPhase)))
        if(.not.FromGoToSub) then
          allocate(ZMagO(NSymm(KPhase)))
          allocate(s6o(6,NSymm(KPhase)))
        endif
      endif
      if(Allocated(KWSym)) deallocate(KwSym)
      if(NDimI(KPhase).gt.0) then
        allocate(KWSym(MaxUsedKwAll,NSymm(KPhase),NComp(KPhase),
     1           NPhase))
        call SetSymmWaves
      endif
      IStore=0
      call CrlStoreSymmetry(IStore)
      call RefPrelim(0)
      call CrlRestoreSymmetry(IStore)
      call CrlCleanSymmetry
      call SetIgnoreWTo(.false.)
      call SetIgnoreETo(.false.)
      neq=0
      close(lst,status='delete')
      KPhase=KPhaseIn
      LstOpened=.false.
      if(allocated(AtDisable)) deallocate(AtDisable,AtDisableFact)
      if(Allocated(LSMat))
     1  deallocate(LSMat,LSRS,LSSol,par,ader,bder,der,dc,NSingArr,dertw)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_groups.tmp','formatted','unknown')
      nGrpIn=1
      if(FromRepAnalysis) then
        PsatPosun=.false.
        do i=EpiSelFr,EpiSelTo-1
          ii=OrdEpi(i)
          do j=i+1,EpiSelTo
            jj=OrdEpi(j)
            if(EqIgCase(GrpEpi(ii),GrpEpi(jj))) then
              PsatPosun=.true.
              go to 1150
            endif
          enddo
        enddo
1150    do i=EpiSelFr,EpiSelTo
          ii=OrdEpi(i)
          Veta=GrpEpi(ii)(:idel(GrpEpi(ii)))
          if(PsatPosun.and..not.EqRV0(ShSgEpi(1,ii),3,.0001)) then
            call OriginShift2String(ShSgEpi(1,ii),3,t80)
            Veta=Veta(:idel(Veta))//'+'//t80(:idel(t80))
          endif
          write(ln,FormA) Veta(:idel(Veta))
        enddo
      else
        invc=CrlCentroSymm()
        invm=CrlMagInver()
        i6=0
        i4=0
        i3=0
        do i=1,NSymmN(KPhase)
          call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,t3,t2m,
     1               io,no,pom)
          if(no.eq.3) then
            if(i3.eq.0.and.i6.eq.0) i3=i
          else if(no.eq.4) then
            if(i4.eq.0) i4=i
          else if(no.eq.6) then
            if(i6.eq.0.and.t3.ne.'-3') then
              i6=i
              i3=0
            endif
          endif
          if(.not.FromGoToSub) then
            ZMagO(i)=ZMag(i,1,KPhase)
            call CopyVek(s6(1,i,1,KPhase),s6o(1,i),NDim(KPhase))
          endif
        enddo
        NGen=0
        if(i6.ne.0) then
          NGen=NGen+1
          IGen(NGen)=i6
          if(NDimI(KPhase).gt.0) then
            NPackArr(NGen)=12
          else
            NPackArr(NGen)=2
          endif
        else
          if(i3.gt.0) then
            NGen=NGen+1
            IGen(NGen)=i3
            if(NDimI(KPhase).gt.0) then
              NPackArr(NGen)=3
            else
              NPackArr(NGen)=1
            endif
          endif
          if(i4.gt.0) then
            NGen=NGen+1
            IGen(NGen)=i4
            if(NDimI(KPhase).gt.0) then
              NPackArr(NGen)=8
            else
              NPackArr(NGen)=2
            endif
          endif
        endif
        if(NGen.le.0.and.invc.ne.2) then
          NGen=NGen+1
          IGen(NGen)=2
          if(NDimI(KPhase).gt.0) then
            NPackArr(NGen)=8
          else
            NPackArr(NGen)=2
          endif
        endif
        if(invc.gt.0) then
          NGen=NGen+1
          IGen(NGen)=invc
          if(NCommQProduct(1,KPhase).eq.2.or.NDimI(KPhase).eq.0) then
            if(NDimI(KPhase).gt.0) then
              NPackArr(NGen)=4
            else
              NPackArr(NGen)=2
            endif
          else
            NPackArr(NGen)=1
          endif
        endif
1200    call CrlGenerMagSymm(NDim(KPhase),NSymmN(KPhase),
     1                       rm6(1,1,1,KPhase),s6(1,1,1,KPhase),
     2                       ZMag(1,1,KPhase),NGen,IGen,KGen,ich)
        NAll=1
        do i=1,NGen
          NAll=NAll*NPackArr(i)
          call SmbOp(rm6(1,IGen(i),1,KPhase),s6(1,IGen(i),1,KPhase),1,
     1             1.,t3,t2m,io,nn,pom)
          StGen(i)=t3
          call CopyVekI(io,IoGen(1,i),3)
        enddo
        do i=1,NSymmN(KPhase)
          if(KGen(i).le.0) then
            NGen=NGen+1
            if(NDimI(KPhase).gt.0) then
              NPackArr(NGen)=8
            else
              NPackArr(NGen)=2
            endif
            NAll=NAll*NPackArr(NGen)
            if(invc.le.0) then
              IGen(NGen)=i
            else
              IGen(NGen-1)=i
              IGen(NGen)=invc
              k=NPackArr(NGen-1)
              NPackArr(NGen-1)=NPackArr(NGen)
              NPackArr(NGen)=k
            endif
            go to 1200
          endif
        enddo
        if(allocated(GrpMenu)) deallocate(GrpMenu,ZMagGen,s6Gen)
        n=NAll
        allocate(GrpMenu(n),ZMagGen(NSymm(KPhase),n),
     1           s6Gen(6,NSymm(KPhase),n))
        n=0
        do 1300i=1,NAll
          call RecUnpack(i,nd,NPackArr,NGen)
          do j=1,NGen
            if(NDimI(KPhase).le.0) then
              if(nd(j).eq.1) then
                ZMag(IGen(j),1,KPhase)= 1.
              else
                ZMag(IGen(j),1,KPhase)=-1.
              endif
            else
              if(j.gt.nd(j)/2) then
                ZMag(IGen(j),1,KPhase)=-1.
                m=nd(j)-NPackArr(j)/2
              else
                ZMag(IGen(j),1,KPhase)= 1.
                m=nd(j)
              endif
              s6(4,IGen(j),1,KPhase)=float(2*m)/float(NPackArr(j))
            endif
          enddo
          call CrlGenerMagSymm(NDim(KPhase),NSymmN(KPhase),
     1      rm6(1,1,1,KPhase),s6(1,1,1,KPhase),ZMag(1,1,KPhase),NGen,
     2      IGen,KGen,ich)
          if(ich.ne.0) cycle
          n=n+1
          do j=1,NSymmN(KPhase)
            call CrlMakeRMag(j,1)
            ZMagGen(j,n)=ZMag(j,1,KPhase)
            call CopyVek(s6(1,j,1,KPhase),s6Gen(1,j,n),NDim(KPhase))
          enddo
          if(invm.gt.0) then
            do j=NSymmN(KPhase)+1,NSymm(KPhase)
              ZMag(j,1,KPhase)=-ZMag(j-NSymmN(KPhase),1,KPhase)
              call CopyVek(s6(1,j-NSymmN(KPhase),1,KPhase),
     1                     s6(1,j,1,KPhase),NDim(KPhase))
              s6(4,j,1,KPhase)=s6(4,j,1,KPhase)+.5
              call od0do1(s6(4,j,1,KPhase),s6(4,j,1,KPhase),1)
              call CrlMakeRMag(j,1)
              ZMagGen(j,n)=ZMag(j,1,KPhase)
              call CopyVek(s6(1,j,1,KPhase),s6Gen(1,j,n),NDim(KPhase))
            enddo
          endif
          call FindSmbSgTr(t80,Shift,p80,NGrTr,TrMat,TrShift,ic,ii)
          if(FromRepAnalysis.or.FromGoToSub) then
            GrpMenu(n)=p80
          else
            if(t80(1:1).ne.'?') then
              GrpMenu(n)=t80
            else
              GrpMenu(n)=p80
            endif
          endif
          do j=1,n-1
            if(EqIgCase(GrpMenu(j),GrpMenu(n))) then
              n=n-1
              go to 1300
            endif
          enddo
          write(ln,FormA) GrpMenu(n)(:idel(GrpMenu(n)))
          if(.not.FromRepAnalysis.and..not.FromGoToSub) then
            if(NDimI(KPhase).gt.0) then
              do j=1,NSymm(KPhase)
                if(.not.EqRV(s6(1,j,1,KPhase),s6o(1,j),NDim(KPhase),
     1                       .001)) go to 1300
              enddo
              nGrpIn=n
            else
              if(EqRV(ZMag(1,1,KPhase),ZMagO,NSymm(KPhase),.001))
     1          nGrpIn=n
            endif
          endif
1300    continue
        NAll=n
      endif
      call CloseIfOpened(ln)
      NGrpMenuLast=-1
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      if(NDimI(KPhase).eq.0) then
        kk=2
      else
        kk=2+2*min(NDimI(KPhase),2)
      endif
      if(NDimI(KPhase).eq.1.and.NCommQProduct(1,KPhase).eq.2) kk=kk+1
      xpom=50.
      do k=1,kk
        call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
        xpom=xpom+10.
        call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
        xpom=xpom+45.
      enddo
      id=NextQuestId()
      if(FromRepAnalysis.or.FromGoToSub) then
        xqd=WizardLength
        il=WizardLines
      else
        if(NDimI(KPhase).gt.0) then
          xqd=650.
          il=15
        else
          xqd=550.
          il=11
        endif
      endif
      if(NDimI(KPhase).gt.0) then
        Cislo='superspace'
        Fract=3.
      else
        Cislo='space'
        Fract=3.
      endif
      Veta='Select Shubnikov '//Cislo(:idel(Cislo))//' group:'
      if(FromRepAnalysis.or.FromGoToSub) then
        call FeQuestTitleMake(id,Veta)
      else
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      endif
      il=1
      Veta='Shubnikov'
      Veta=Veta(:idel(Veta)+1)//Cislo(:idel(Cislo))//' group'
      tpom=10.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=xqd/Fract+30.
      Veta='Atom'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+50.
      Veta='Moment'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+60.
      Veta='Global'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      if(NDimI(KPhase).gt.0.) then
        tpom=tpom+55.
        kk=2*min(NDimI(KPhase),2)
        do k=1,kk
          if(mod(k,2).eq.1) then
            Veta='sin#'
          else
            Veta='cos#'
          endif
          write(Cislo,FormI15) (k+1)/2
          call Zhusti(Cislo)
          Veta=Veta(:idel(Veta))//Cislo(1:1)
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          tpom=tpom+55.
        enddo
        if(NCommQProduct(1,KPhase).eq.2) then
          Veta='2*pi*q*x'
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        endif
      endif
      xpom=5.
      dpom=xqd/Fract-10.-SbwPruhXd
      il=8
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwGroup=SbwLastMade
      call FeQuestSbwMenuOpen(SbwLastMade,fln(:ifln)//'_groups.tmp')
      call FeQuestSbwItemOff(nSbwGroup,1)
      call FeQuestSbwItemOn(nSbwGroup,nGrpIn)
      Veta='Show %details'
      dpomb=FeTxLengthUnder(Veta)+20.
      tpom=xpom+(dpom-dpomb)*.5
      call FeQuestButtonMake(id,tpom,il+1,dpomb,ButYd,Veta)
      nButtDetails=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.+SbwPruhXd
      dpom=xqd-xpom-5.-SbwPruhXd
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwAtoms=SbwLastMade
      il=il+1
      Veta='Information: The symbol "M" indicates that the '//
     1     'component can have non-zero value.'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      il=-il*10-6
      Veta='For more details press the button "Show details".'
      xpom=xpom+65.
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      if(QPul) then
        call TrMat2String(TrMatSuperCell,3,Veta)
        Veta='Magnetic moments are expressed in the supercell '//
     1       Veta(:idel(Veta))
        il=il-6
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        Veta='of the parent cell.'
      else
        Veta='Magnetic moments are expressed in the parent cell.'
      endif
      il=il-6
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      Veta='%Start graphic simulation'
      dpom=FeTxLengthUnder(Veta)+20.
      if(isPowder) then
        xpom=xqd*.5-dpom-10.
      else
        xpom=(xqd-dpom)*.5
      endif
      il=il-10
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtGraphic=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(isPowder) then
        xpom=xpom+20.+dpom
        Veta='S%tart profile simulation'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtPowder=ButtonLastMade
      else
        nButtPowder=0
      endif
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  call FeQuestEventWithCheck(id,ich,CrlMagTesterCheck,FeVoid)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtGraphic) then
        if(FromRepAnalysis.or.FromGoToSub) WizardMode=.false.
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase) cycle
          if(MagPar(i).ne.0)
     1      call CrlMagTesterSetAtom(i,2.,.false.)
          neq=0
          call AtSpec(i,i,0,0,0)
        enddo
        MakeCIFForGraphicViewer=.true.
        call iom50(1,0,fln(:ifln)//'.m50')
        call iom40Only(1,0,fln(:ifln)//'.m40')
        call CrlPlotStructure
        MakeCIFForGraphicViewer=.false.
        if(FromRepAnalysis.or.FromGoToSub) WizardMode=.true.
        NGrpMenuLast=-1
        go to 1500
      else if(CheckType.eq.EventButton.and.(CheckNumber.eq.nButtPowder
     1        .or.CheckNumber.eq.nButtDetails)) then
        if(CheckNumber.eq.nButtPowder) then
          i=0
        else
          i=1
        endif
        call CrlMagTesterAction(i,SvFile,AtMag,StMag,NAtMag,MagParMax-1)
        NGrpMenuLast=-1
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(FromRepAnalysis.or.FromGoToSub) then
        if(ich.ne.0) go to 4000
      else
        call FeQuestRemove(id)
      endif
      ParentStructure=.false.
      call iom40Only(1,0,fln(:ifln)//'.m40')
      call iom50(1,0,fln(:ifln)//'.m50')
      call MoveFile(Fln(:iFln)//'.m40',FlnOld(:iFlnOld)//'.m40')
      call MoveFile(Fln(:iFln)//'.m50',FlnOld(:iFlnOld)//'.m50')
3000  call DeleteAllFiles(Fln(:iFln)//'*')
4000  Fln=FlnOld
      iFln=idel(Fln)
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      do i=1,NAtInd
        if(kswa(i) .ne.KPhase.or.AtTypeMag(isf(i),KPhase).eq.' ') cycle
        do j=1,3
           sm0(j,i)=.1/CellPar(j,iswa(i),KPhase)
          ssm0(j,i)=0.
          if(NDimI(KPhase).eq.1) then
             smx(j,1,i)=sm0(j,i)
            ssmx(j,1,i)=0.
             smy(j,1,i)=sm0(j,i)
            ssmy(j,1,i)=0.
          endif
        enddo
      enddo
      call iom40(1,0,fln(:ifln)//'.m40')
      if(allocated(IGen).and..not.FromRepAnalysis)
     1  deallocate(IGen,KGen,StGen,IoGen)
      if(allocated(GrpMenu)) deallocate(GrpMenu,ZMagGen,s6Gen)
      if(allocated(KWSym)) deallocate(KwSym)
      if(allocated(AtDisable)) deallocate(AtDisable,AtDisableFact)
      if(allocated(XGauss)) deallocate(XGauss,WGauss)
      if(allocated(ZMagO)) deallocate(ZMagO,s6o)
      if(allocated(AtMag)) deallocate(AtMag,StMag)
      call RefDeallocate(.false.,0)
      call FeTabsReset(UseTabs)
      UseTabsIn=UseTabs
      return
      end
