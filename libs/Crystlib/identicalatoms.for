      logical function IdenticalAtoms(i,j,dmez)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension y0(6),y1(6),y2(6),y3(6),y0p(6),y1p(6),y2p(6),y3p(6),
     1          dd(3),ddr(3),phi(3),eps(mxw),snm(mxw),csm(mxw),uxj(3),
     2          uyj(3),smp(36),rmp(9),rm6p(36),s6p(6),vt6p(:,:)
      logical EqMod1
      allocatable vt6p
      allocate(vt6p(NDim(KPhase),NLattVec(KPhase)))
      IdenticalAtoms=.false.
      if(kswa(i).ne.kswa(j).or.isf(i).ne.isf(j)) go to 9999
      kswai=kswa(i)
      kfsi=KFA(1,i)
      kfxi=KFA(2,i)
      KPhase=kswai
      if(Allocated(KWSym)) deallocate(KwSym)
      if(NDimI(KPhase).gt.0) then
        allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
        call SetSymmWaves
      endif
      iswi=iswa(i)
      call CopyVek(x(1,j),y0,3)
      call SetRealArrayTo(y0(4),NDimI(KPhase),0.)
      if(KFA(1,j).ne.0.or.KFA(2,j).ne.0) then
        call CopyVek(y0,y0p,NDim(KPhase))
        if(KFA(1,j).ne.0) then
          call CopyVek(ax(KModA(1,j)-NDimI(KPhase)+1,j),y0p(4),
     1                 NDimI(KPhase))
        else if(KFA(2,j).ne.0) then
          y0p(4)=uy(1,KModA(2,j),j)
        endif
      endif
      js=0
      do 5000jsym=1,NSymm(KPhase)
        js=js+1
        if(isa(js,j).le.0) go to 5000
        iswj=ISwSymm(jsym,iswa(j),KPhase)
        if(iswj.ne.iswi) go to 5000
        if(iswj.eq.iswa(j)) then
          call CopyMat(rm (1,jsym,iswj,KPhase),rmp ,3)
          call CopyMat(rm6(1,jsym,iswj,KPhase),rm6p,NDim(KPhase))
          call CopyVek( s6(1,jsym,iswj,KPhase), s6p,NDim(KPhase))
          do l=1,NLattVec(KPhase)
            call CopyVek(vt6(1,l,iswj,KPhase),vt6p(1,l),NDim(KPhase))
          enddo
        else
          call multm(zv(1,iswj,KPhase),zvi(1,iswa(j),KPhase),smp,
     1               NDim(KPhase),NDim(KPhase),NDim(KPhase))
          call multm(smp,rm6(1,jsym,iswa(j),KPhase),rm6p,
     1               NDim(KPhase),NDim(KPhase),NDim(KPhase))
          call MatBlock3(rm6p,rmp,NDim(KPhase))
          call multm(smp,s6(1,jsym,iswa(j),KPhase),s6p,NDim(KPhase),
     1               NDim(KPhase),1)
          do l=1,NLattVec(KPhase)
            call multm(smp,vt6(1,l,iswa(j),KPhase),vt6p(1,l),
     1                 NDim(KPhase),NDim(KPhase),1)
          enddo
        endif
        call multm(rm6p,y0,y1,NDim(KPhase),NDim(KPhase),1)
        if(KFA(1,j).ne.0.or.KFA(2,j).ne.0) then
          call multm(rm6p,y0p,y1p,NDim(KPhase),NDim(KPhase),1)
          call AddVek(y1p,s6p,y2p,NDim(KPhase))
        endif
        call AddVek(y1,s6p,y2,NDim(KPhase))
        do 3000jcenter=1,NLattVec(KPhase)
          do m=1,NDim(KPhase)
            y3(m)=y2(m)+vt6p(m,jcenter)
            if(KFA(1,j).ne.0.or.KFA(2,j).ne.0)
     1        y3p(m)=y2p(m)+vt6p(m,jcenter)
          enddo
          do m=1,3
            dd(m)=y3(m)-x(m,i)
            dd(m)=dd(m)-anint(dd(m))
          enddo
          call multm(MetTens(1,iswj,KPhase),dd,ddr,3,3,1)
          dist=sqrt(scalmul(dd,ddr))
          if(dist.gt.dmez) go to 3000
          write(Cislo,'(f9.4)') dist
          if(NDim(KPhase).le.3) go to 2000
          kmodsi=KModA(1,i)
          kmodxi=KModA(2,i)
          if((kmodsi.ne.KModA(1,j).or.kfsi.ne.KFA(1,j)).or.
     1       (kmodxi.ne.KModA(2,j).or.kfxi.ne.KFA(2,j))) go to 9999
          do m=1,NDimI(KPhase)
            phi(m)=-y3(m+3)*pi2
          enddo
          kmodmx=max(kmodxi,kmodsi)
          do k=1,kmodmx
            l=KwSym(k,jsym,iswj,KPhase)
            if(l.eq.0) then
              call FeChybne(-1.,-1.,'the set of modulation waves '
     1                    //'isn''t closed.','Select another set '
     2                    //'and try again.',SeriousError)
              ErrFlag=1
              go to 9999
            endif
            eps(k)=isign(1,l)
            l=iabs(l)
            pom=0.
            do m=1,NDimI(KPhase)
              pom=pom+float(kw(m,l,KPhase))*phi(m)
            enddo
            snp=sin(pom)
            csp=cos(pom)
            if(abs(snp).lt..001) snp=0.
            if(abs(csp).lt..001) csp=0.
            if(abs(abs(snp)-1.).lt..001) snp=sign(1.,snp)
            if(abs(abs(csp)-1.).lt..001) csp=sign(1.,csp)
            snm(k)=snp
            csm(k)=csp
          enddo
          if(kmodsi.gt.0) then
            if(NDim(KPhase).eq.4.and.abs(a0(i)-a0(j)).gt..0001)
     1        go to 9999
            if(kfsi.eq.1) then
              kk=KModA(1,i)-NDimI(KPhase)
              do k=4,NDim(KPhase)
                kk=kk+1
                if(NDim(KPhase).gt.4.and.
     1             abs(ay(kk,i)-ay(kk,j)).gt..0001) go to 9999
                pom=ax(kk,i)-y3p(k)
                if(.not.EqMod1(pom,0.,.0001)) go to 9999
              enddo
              kmodsi=kmodsi-NDimI(KPhase)
            endif
            do k=1,kmodsi
              snp=snm(k)
              csp=csm(k)
              epsp=eps(k)
              axp= epsp*csp*ax(k,j)+snp*ay(k,j)
              if(abs(axp-ax(k,i)).gt..0001) go to 9999
              ayp=-epsp*snp*ax(k,j)+csp*ay(k,j)
              if(abs(ayp-ay(k,i)).gt..0001) go to 9999
            enddo
          endif
          if(kmodxi.gt.0) then
            if(kfxi.eq.1) then
              if(abs(uy(2,kmodxi,i)-uy(2,kmodxi,j)).gt..0001)
     1          go to 9999
              pom=uy(1,kmodxi,i)-y3p(4)
              if(.not.EqMod1(pom,0.,.0001)) go to 9999
            else
              do k=1,kmodxi
                snp=snm(k)
                csp=csm(k)
                epsp=eps(k)
                call multm(rmp,ux(1,k,j),uxj,3,3,1)
                call multm(rmp,uy(1,k,j),uyj,3,3,1)
                do m=1,3
                  uxjm=uxj(m)
                  uyjm=uyj(m)
                  uxp= epsp*csp*uxjm+snp*uyjm
                  if(abs(uxp-ux(m,k,i)).gt..0001) go to 9999
                  uyp=-epsp*snp*uxjm+csp*uyjm
                  if(abs(uyp-uy(m,k,i)).gt..0001) go to 9999
                enddo
              enddo
            endif
          endif
2000      IdenticalAtoms=.true.
          go to 9999
3000    continue
5000  continue
9999  if(allocated(vt6p)) deallocate(vt6p)
      return
      end
