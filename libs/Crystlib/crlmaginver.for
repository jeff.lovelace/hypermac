      integer function CrlMagInver()
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical MatRealEqUnitMat,EqRV0
      CrlMagInver=0
      do i=1,NSymm(KPhase)
        if(MatRealEqUnitMat(rm6(1,i,1,KPhase),NDim(KPhase),.0001).and.
     1     ZMag(i,1,KPhase).lt.0.) then
          if(EqRV0(s6(1,i,1,KPhase),3,.001)) CrlMagInver=i
        endif
      enddo
      return
      end
