      subroutine CrlCreateCorrectedStructure
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'datred.cmn'
      character*256 Veta,FlnNew,FlnOld
      integer FeChDir
      logical StructureExists,FeYesNo,FeYesNoHeader,ExistFile
      FlnNew=' '
1100  call FeFileManager('Create a corrected structure',FlnNew,' ',1,
     1                   .false.,ich)
      if(ich.ne.0) go to 9999
      iflnn=idel(FlnNew)
      if(StructureExists(FlnNew)) then
        call ExtractFileName(FlnNew,Veta)
        if(.not.FeYesNo(-1.,-1.,'The structure "'//FlnNew(:iflnn)//
     1                  '" already exists, rewrite it?',0)) go to 1100
        call ChangeUSDFile(FlnNew,'closed','*')
      endif
      call CopyFile(Fln(:ifln)//'.m50',FlnNew(:iflnn)//'.m50')
      call CopyFile(Fln(:ifln)//'.m40',FlnNew(:iflnn)//'.m40')
      FlnOld=Fln
      iflno=ifln
      Fln=FlnNew
      ifln=iflnn
      call RefOpenCommands
      ExtTensor=0
      call RefRewriteCommands(1)
      call iom50(1,0,Fln(:ifln)//'.m50')
      sc(1,1:MxDatBlock)=1.
      sc(2:MxSc,1:MxDatBlock)=0.
      ec=0.
      kis(1,1:MxDatBlock)=1
      kis(2:MxScAll,1:MxDatBlock)=0
      call iom40(1,0,Fln(:ifln)//'.m40')
      if(iskip.gt.0.or.useunobs.gt.0.or.nskrt.gt.0) then
        Ninfo=1
        TextInfo(1)='The "corrected" structure will not use all '//
     1              'reflections as during the refinement:'
        if(iskip.gt.0) then
          NInfo=NInfo+1
          TextInfo(NInfo)='       Reflections with |Fo-Fc|>'
          write(Cislo,102) vyh
          call ZdrcniCisla(Cislo,1)
          TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     1                    Cislo(:idel(Cislo))//'sig(F) were omitted.'
        endif
        if(useunobs.gt.0) then
          NInfo=NInfo+1
          TextInfo(NInfo)='       Reflections with I<'
          write(Cislo,102) slevel
          call ZdrcniCisla(Cislo,1)
          TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     1                    Cislo(:idel(Cislo))//'sig(I) were omitted.'
        endif
        if(nskrt.gt.0) then
          NInfo=NInfo+1
          TextInfo(NInfo)='    Some "dontuse" or "useonly commands" '//
     1                    'were used.'
        endif
        if(.not.FeYesNoHeader(-1.,-1.,
     1                        'Do you want to continue anyhow?',1)) then
          call DeleteAllFiles('"'//fln(:ifln)//'.*"')
          go to 5000
        endif
      endif
      NRef95(KRefBlock)=0
      NLines95(KRefBlock)=0
      lni=NextLogicNumber()
      call OpenFile(lni,FlnOld(:iflno)//'.m83','formatted','unknown')
      if(ErrFlag.ne.0) go to 5000
      lno=NextLogicNumber()
      RefBlockFileName=Fln(:idel(Fln))//'.l01'
      call OpenFile(lno,RefBlockFileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 5000
      call SetRealArrayTo(uhly,4,0.)
      call SetRealArrayTo(dircos,6,0.)
      call SetRealArrayTo(corrf,2,1.)
      call SetIntArrayTo(iflg,2,1)
      iflg(3)=0
      tbar=0.
      KProf=0
      NProf=0
      DRlam=0.
      NRef95(KRefBlock)=0
      NLines95(KRefBlock)=0
2000  read(lni,Forma,end=3000) Veta
      if(Veta.eq.' ') go to 2000
      read(Veta,Format83e) ih(1:NDim(KPhase)),FCalcQ,FObsQ,sFCalcQ
      NRef95(KRefBlock)=NRef95(KRefBlock)+1
      ri=FObsQ
      rs=sFCalcQ
      expos=float(NRef95(KRefBlock))*.1
      no=NRef95(KRefBlock)
      call DRPutReflectionToM95(lno,nl)
      NLines95(KRefBlock)=NLines95(KRefBlock)+nl
      go to 2000
3000  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      call iom95(0,FlnOld(:iflno)//'.m95')
      SourceFileRefBlock(KRefBlock)='?The strucure created from "'//
     1                               fln(:ifln)//
     2                               '" as a "corrected" structure'
      SourceFileDateRefBlock(KRefBlock)='?'
      SourceFileTimeRefBlock(KRefBlock)='?'
      FormatRefBlock(KRefBlock)='(3i4,15x,2e15.6)'
      write(FormatRefBlock(KRefBlock)(2:2),'(i1)') NDim(KPhase)
      LamAveRefBlock(KRefBlock)=.5
      LamA1RefBlock(KRefBlock)=.5
      LamA2RefBlock(KRefBlock)=.5
      KLamRefBlock(KRefBlock)=.5
      if(RadiationRefBlock(KRefBlock).eq.NeutronRadiation)
     1  PolarizationRefBlock(KRefBlock)=PolarizedLinear
      UseTrRefBlock(KRefBlock)=.false.
      CellReadIn(KRefBlock)=.true.
      UseTrRefBlock(KRefBlock)=.false.
      ITwRead(KRefBlock)=1
      DifCode(KRefBlock)=IdImportGeneralI
      Veta=Fln(:ifln)//'.m95'
      CellRefBlock(1:6,0)=CellPar(1:6,1,1)
      CellRefBlock(1:6,KRefBlock)=CellPar(1:6,1,1)
      do i=1,NDimI(KPhase)
        QuRefBlock(1:3,i,0)=Qu(1:3,i,1,1)
        QuRefBlock(1:3,i,1)=Qu(1:3,i,1,1)
      enddo
      call UnitMat(TrMP,NDim(KPhase))
      call iom95(1,Veta)
      if(ExistFile(Veta)) then
        call CompleteM95(0)
        ExistM90=.false.
        call SetLogicalArrayTo(ModifyDatBlock,MxDatBlock,.true.)
        call EM9CreateM90(1,ich)
      endif
      Fln=FlnOld
      ifln=iflno
      if(FeYesNo(-1.,-1.,'Do you want to continue with the '//
     1           '"corrected" structure?',0)) then
        call ExtractDirectory(FlnNew,Veta)
        i=FeChdir(Veta)
        call FeGetCurrentDir
        call ExtractFileName(FlnNew,Veta)
        FlnNew=Veta
        iflnn=idel(FlnNew)
        call DeletePomFiles
        fln=FlnNew
        ifln=idel(fln)
        call FeBottomInfo(' ')
        call DeletePomFiles
        call FortFilesClean
      else
        go to 5000
      endif
      go to 9999
5000  Fln=FlnOld
      ifln=idel(Fln)
      call iom50(0,0,Fln(:ifln)//'.m50')
      call iom40(0,0,Fln(:ifln)//'.m40')
9999  return
102   format(f10.3)
      end
