      subroutine srots(a,b,c)
      include 'fepc.cmn'
      dimension a(3,3),b(3,3),c(6,9)
      call SetRealArrayTo(c,54,0.)
      do i=1,6
        call indext(i,i1,i2)
        k=i2
        do j=1,3
          c(i,k)=c(i,k)+a(i1,j)
          k=k+3
        enddo
        k=i1
        do j=1,3
          c(i,k)=c(i,k)+b(i2,j)
          k=k+3
        enddo
      enddo
      return
      end
