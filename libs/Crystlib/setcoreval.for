      subroutine SetCoreVal(ia)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension popc(7),popv(7),ff(62)
      character*80 Veta
      logical EqIgCase,ExistWaveF
      if(EqIgCase(CoreValSource(ia,KPhase),'FF-table')) go to 5000
      call SetRealArrayTo(popc,7,0.)
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      call ReadWaveF(AtType(ia,KPhase),ia,OrbitName(1),popc,FF,121,0,
     1               CoreValSource(ia,KPhase),ich)
      call ResetIgnoreW
      call ResetIgnoreE
      ExistWaveF=ich.eq.0
      if(.not.ExistWaveF) then
        Veta='STO wave function for atom type "'//
     1       AtType(ia,KPhase)(:idel(AtType(ia,KPhase)))//
     2       '" not defined.'
        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
        go to 5000
      endif
      call SetRealArrayTo(FFCore(1,ia,KPhase),121,0.)
      call SetRealArrayTo(FFVal(1,ia,KPhase),121,0.)
      kp=0
      fnormv=0.
      do l=1,7
        kp=kp+l
        k=kp
        spopc=0.
        spopv=0.
        do j=l,7
          jp=j-l+1
          popc(jp)=PopCore(k,ia,KPhase)
          spopc=spopc+popc(jp)
          if(PopVal(1,ia,KPhase).ge.0) then
            popv(jp)=PopVal(k,ia,KPhase)
            spopv=spopv+popv(jp)
            fnormv=fnormv+popv(jp)
          endif
          k=k+j
        enddo
        if(spopc.gt.0.)
     1    call ReadWaveF(AtType(ia,KPhase),ia,OrbitName(l),popc,
     2                   FFCore(1,ia,KPhase),121,0,
     3                   CoreValSource(ia,KPhase),ich)
        if(EqIgCase(AtType(ia,KPhase),'H')) then
          if(PopVal(1,ia,KPhase).eq.-1) then
            Veta='Xray_form_factor_in_steps'
            call RealAFromAtomFile(AtType(ia,KPhase),Veta,ff,62,ich)
            call TableFFToEquidistant(ff,FFVal(1,ia,KPhase),
     1                                AtType(ia,KPhase))
            go to 4100
          else if(PopVal(1,ia,KPhase).eq.-2) then
            s=0.
            pomk=HZSlater(ia,KPhase)/BohrRad
            do j=1,121
              FFVal(j,ia,KPhase)=
     1          SlaterFB(HNSlater(ia,KPhase),0,s,pomk,pom,pom,ich)/pi4
              s=s+.05
            enddo
            go to 4100
          endif
        endif
        if(spopv.gt.0.)
     1    call ReadWaveF(AtType(ia,KPhase),ia,OrbitName(l),popv,
     2                   FFVal(1,ia,KPhase),121,0,
     3                   CoreValSource(ia,KPhase),ich)
      enddo
4100  if(.not.ExistWaveF) then
        if(FFCore(1,ia,KPhase).le.0..and.FFVal(1,ia,KPhase).le.0.) then
          Veta='core and valence form factors'
          Veta='Please define manually '//Veta(:idel(Veta))//'.'
          call FeChybne(-1.,-1.,'the Clementi form factor for "'
     1                //AtType(ia,KPhase)(:idel(AtType(ia,KPhase)))//
     2               '" is not on the file.',Veta,Warning)
          call SetRealArrayTo(FFVal(1,ia,KPhase),121,0.)
          call SetRealArrayTo(FFCore(1,ia,KPhase),121,0.)
          go to 5000
        endif
      endif
      do j=1,FFType(KPhase)
        FFBasic(j,ia,KPhase)=FFCore(j,ia,KPhase)+FFVal(j,ia,KPhase)
      enddo
      if(fnormv.gt.0.) then
        fnormv=1./fnormv
        do j=1,FFType(KPhase)
          FFVal(j,ia,KPhase)=FFVal(j,ia,KPhase)*fnormv
        enddo
      endif
5000  return
101   format(f15.0)
      end
