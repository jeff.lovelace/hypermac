      subroutine GetRealEsdFromSt(St,a,sa,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) st
      ich=0
      i=index(St,'(')
      a=0.
      sa=0.
      if(i.ne.0) then
        St(i:i)=' '
        j=index(St,')')
        if(j.ne.0) then
          st(j:j)=' '
        else
          go to 9100
        endif
      endif
      k=0
      call kus(St,k,Cislo)
      if(Cislo.eq.'?') then
        ich=-1
        go to 9999
      endif
      read(Cislo,*,err=9100) a
      if(k.ge.len(St)) go to 9999
      ie=LocateSubstring(Cislo,'e',.false.,.true.)
      if(ie.gt.0) then
        Cislo=Cislo(ie+1:)
        read(Cislo,*,err=9200) ir
        i=0
      else
        ir=0
        call posun(Cislo,1)
        i=15-index(Cislo,'.')
      endif
      call kus(St,k,Cislo)
      read(Cislo,*,err=9200) sa
      sa=sa*float(10**ir)/float(10**i)
      go to 9999
9100  ich=-2
      go to 9999
9200  sa=0.
9999  return
100   format(f15.0)
      end
