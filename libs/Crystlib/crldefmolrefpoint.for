      subroutine CrlDefMolRefPoint(im,StRefPointP,ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StRefPointP
      character*80 ErrSt,t80
      dimension xmd(3),yp(3),xmo(3),DelXI(3),xmtro(3),xmtrd(3),snm(mxw),
     1          csm(mxw),tlo(3,3),ttztl(9),tztlp(6,6),tztsp(6,9),pp(9)
      ich=0
      iap=NAtMolFr(1,1)
      isw=iswmol(im)
      do i=1,im-1
        iap=iap+iam(i)
      enddo
      iak=iap+iam(im)-1
      call CopyVek(xm(1,im),xmo,3)
      if(RefPoint(im).eq.0.or.npoint(im).gt.1) then
        k=0
        call StToReal(StRefPointP,k,xm(1,im),1,.false.,ich)
        if(ich.eq.0) then
          call StToReal(StRefPointP,k,xm(2,im),2,.false.,ich)
          if(ich.ne.0) then
            if(ich.eq.1) then
              ErrSt='"'//StRefPointP(:idel(StRefPointP))//
     1              '" contains a non-numerical character.'
            else
              ErrSt='"'//StRefPointP(:idel(StRefPointP))//
     1              '" does not contain the requested number of items.'
            endif
            go to 9000
          endif
          ich=-1
        else
          call zhusti(StRefPointP)
          call uprat(StRefPointP)
          k=ktat(atom(iap),iam(im)/KPoint(im),StRefPointP)
          if(k.le.0) then
            ErrSt='atom "'//StRefPointP(:idel(StRefPointP))
            ErrSt=ErrSt(:idel(ErrSt))//'" does not exist.'
            go to 9000
          else
            k=k+iap-1
            call CopyVek(x(1,k),xm(1,im),3)
            StRefPoint(im)=StRefPointP
          endif
          ich=0
        endif
      endif
      if(RefPoint(im).gt.0) then
        suma=0.
        call SetRealArrayTo(xm(1,im),3,0.)
        do i=iap,iap+iam(im)/KPoint(im)-1
          if(RefPoint(im).eq.1) then
            pom=AtWeight(isf(i),KPhase)
          else
            pom=1.
          endif
          do ig=1,npoint(im)
            if(ig.eq.1) then
              call CopyVek(x(1,i),yp,3)
            else
              do j=1,3
                xmd(j)=x(j,i)-xmo(j)
              enddo
              call CopyVek(xmo,yp,3)
              call cultm(rpoint(1,ig,im),xmd,yp,3,3,1)
            endif
            do j=1,3
              xm(j,im)=xm(j,im)+pom*yp(j)
            enddo
            suma=suma+pom
          enddo
        enddo
        do i=1,3
          xm(i,im)=xm(i,im)/suma
        enddo
        ich=-1
      endif
      pom=0.
      do i=1,3
        xmd(i)=xm(i,im)-xmo(i)
        pom=pom+abs(xmd(i))
      enddo
      if(pom.lt..0001) go to 9999
      ji=(im-1)*mxp
      do 3500i=1,mam(im)
        ji=ji+1
        call AddVek(xmo,trans(1,ji),xmtro,3)
        call cultm(RotMol(1,ji),xmd,trans(1,ji),3,3,1)
        do j=1,3
          trans(j,ji)=trans(j,ji)-xmd(j)
          xmtrd(j)=xm(j,im)+trans(j,ji)-xmtro(j)
        enddo
        if(KTLS(im).gt.0.or.KModM(3,ji).gt.0) then
          pp(1)=0.
          pp(5)=0.
          pp(9)=0.
          pp(2)=-xmd(3)
          pp(4)= xmd(3)
          pp(3)= xmd(2)
          pp(7)=-xmd(2)
          pp(6)=-xmd(1)
          pp(8)= xmd(1)
          call multm(MetTensI(1,isw,KPhase),pp,ttztl,3,3,3)
          do k=1,9
            ttztl(k)=CellVol(isw,KPhase)*ttztl(k)
          enddo
          call srotb(ttztl,ttztl,tztlp)
          call srots(ttztl,ttztl,tztsp)
          if(KTLS(im).gt.0) then
            call cultm(tztlp,tl(1,ji),tt(1,ji),6,6,1)
            call cultm(tztsp,ts(1,ji),tt(1,ji),6,9,1)
            tlo(1,1)=tl(1,ji)
            tlo(2,2)=tl(2,ji)
            tlo(3,3)=tl(3,ji)
            tlo(1,2)=tl(4,ji)
            tlo(1,3)=tl(5,ji)
            tlo(2,3)=tl(6,ji)
            tlo(2,1)=tlo(1,2)
            tlo(3,1)=tlo(1,3)
            tlo(3,2)=tlo(2,3)
            call Cultm(ttztl,tlo,ts(1,ji),3,3,3)
            call CrlSetTraceS(ts(1,ji),isw)
          endif
        endif
        if(LocMolSystType(ji).gt.0) then
          do j=1,2
            call CrlGetXFromAtString(LocMolSystSt(j,1,ji),im,yp,t80,
     1                               ich)
            if(ich.eq.0)
     1        write(LocMolSystSt(j,1,ji),'(3f9.6)')
     2                                    (yp(k)-xm(k,im),k=1,3)
          enddo
        endif
        do l=1,npoint(im)
          call multm(rpoint(1,l,im),xm(1,im),spoint(1,l,im),3,3,1)
          do k=1,3
            spoint(k,l,im)=xm(k,im)-spoint(k,l,im)
          enddo
        enddo
        if(NDim(KPhase).le.3) go to 3500
        call qbyx(xmtrd,DelXI,isw)
        kmodmx=max(KModM(1,ji),KModM(2,ji),KModM(3,ji))
        do ia=iap,iak
          kmodmx=max(kmodmx,KModA(1,ia))
          kmodmx=max(kmodmx,KModA(2,ia))
          kmodmx=max(kmodmx,KModA(3,ia))
        enddo
        do k=1,kmodmx
          pom=0.
          do m=1,NDimI(KPhase)
            pom=pom+pi2*float(kw(m,k,KPhase))*DelXI(m)
          enddo
          snp=sin(pom)
          csp=cos(pom)
          if(abs(snp).lt..001) snp=0.
          if(abs(csp).lt..001) csp=0.
          if(abs(abs(snp)-1.).lt..001) snp=sign(1.,snp)
          if(abs(abs(csp)-1.).lt..001) csp=sign(1.,csp)
          snm(k)=snp
          csm(k)=csp
        enddo
        do k=1,KModM(1,ji)
          if(KFM(1,ji).eq.1.and.k.gt.KModM(1,ji)-NDimI(KPhase)) then
            axm(k,ji)=axm(k,ji)+DelXI(k-KModM(1,ji)+NDimI(KPhase))
          else
            uxo=axm(k,ji)
            uyo=aym(k,ji)
            axm(k,ji)= uxo*csm(k)+uyo*snm(k)
            aym(k,ji)=-uxo*snm(k)+uyo*csm(k)
          endif
        enddo
        do k=1,KModM(2,ji)
          if(KFM(2,ji).eq.1.and.k.eq.KModM(2,ji)) then
            uty(1,k,ji)=uty(1,k,ji)+DelXI(1)
          else
            do j=1,3
              uxo=utx(j,k,ji)
              uyo=uty(j,k,ji)
              utx(j,k,ji)= uxo*csm(k)+uyo*snm(k)
              uty(j,k,ji)=-uxo*snm(k)+uyo*csm(k)
            enddo
            do j=1,3
              uxo=urx(j,k,ji)
              uyo=ury(j,k,ji)
              urx(j,k,ji)= uxo*csm(k)+uyo*snm(k)
              ury(j,k,ji)=-uxo*snm(k)+uyo*csm(k)
            enddo
          endif
        enddo
        do k=1,KModM(2,ji)
          if(KFM(2,ji).eq.0.or.k.ne.KModM(2,ji)) then
            yp(1)=xmd(2)*urx(3,k,ji)-xmd(3)*urx(2,k,ji)
            yp(2)=xmd(3)*urx(1,k,ji)-xmd(1)*urx(3,k,ji)
            yp(3)=xmd(1)*urx(2,k,ji)-xmd(2)*urx(1,k,ji)
            do j=1,3
              yp(j)=-yp(j)*CellVol(isw,KPhase)
            enddo
            call cultm (MetTensI(1,isw,KPhase),yp,utx(1,k,ji),3,3,1)
            yp(1)=xmd(2)*ury(3,k,ji)-xmd(3)*ury(2,k,ji)
            yp(2)=xmd(3)*ury(1,k,ji)-xmd(1)*ury(3,k,ji)
            yp(3)=xmd(1)*ury(2,k,ji)-xmd(2)*ury(1,k,ji)
            do j=1,3
              yp(j)=-yp(j)*CellVol(isw,KPhase)
            enddo
            call cultm (MetTensI(1,isw,KPhase),yp,uty(1,k,ji),3,3,1)
          endif
        enddo
        do k=1,KModM(3,ji)
          if(KFM(3,ji).eq.1.and.k.eq.KModM(3,ji)) then
            uty(1,k,ji)=uty(1,k,ji)+DelXI(1)
          else
            do j=1,6
              uxo=ttx(j,k,ji)
              uyo=tty(j,k,ji)
              ttx(j,k,ji)= uxo*csm(k)+uyo*snm(k)
              tty(j,k,ji)=-uxo*snm(k)+uyo*csm(k)
            enddo
            do j=1,6
              uxo=tlx(j,k,ji)
              uyo=tly(j,k,ji)
              tlx(j,k,ji)= uxo*csm(k)+uyo*snm(k)
              tly(j,k,ji)=-uxo*snm(k)+uyo*csm(k)
            enddo
            do j=1,9
              uxo=tsx(j,k,ji)
              uyo=tsy(j,k,ji)
              tsx(j,k,ji)= uxo*csm(k)+uyo*snm(k)
              tsy(j,k,ji)=-uxo*snm(k)+uyo*csm(k)
            enddo
          endif
        enddo
        do k=1,KModM(3,ji)
          call cultm(tztlp,tlx(1,k,ji),ttx(1,k,ji),6,6,1)
          call cultm(tztsp,tsx(1,k,ji),ttx(1,k,ji),6,9,1)
          tlo(1,1)=tlx(1,k,ji)
          tlo(2,2)=tlx(2,k,ji)
          tlo(3,3)=tlx(3,k,ji)
          tlo(1,2)=tlx(4,k,ji)
          tlo(1,3)=tlx(5,k,ji)
          tlo(2,3)=tlx(6,k,ji)
          tlo(2,1)=tlo(1,2)
          tlo(3,1)=tlo(1,3)
          tlo(3,2)=tlo(2,3)
          call Cultm(ttztl,tlo,tsx(1,k,ji),3,3,3)
          call CrlSetTraceS(tsx(1,k,ji),isw)
          call cultm(tztlp,tly(1,k,ji),tty(1,k,ji),6,6,1)
          call cultm(tztsp,tsy(1,k,ji),tty(1,k,ji),6,9,1)
          tlo(1,1)=tly(1,k,ji)
          tlo(2,2)=tly(2,k,ji)
          tlo(3,3)=tly(3,k,ji)
          tlo(1,2)=tly(4,k,ji)
          tlo(1,3)=tly(5,k,ji)
          tlo(2,3)=tly(6,k,ji)
          tlo(2,1)=tlo(1,2)
          tlo(3,1)=tlo(1,3)
          tlo(3,2)=tlo(2,3)
          call Cultm(ttztl,tlo,tsy(1,k,ji),3,3,3)
          call CrlSetTraceS(tsy(1,k,ji),isw)
        enddo
3500  continue
      do ia=iap,iak
        do k=1,KModA(1,ia)
          if(KFA(1,ia).eq.1.and.k.eq.KModA(1,ia)) then
            ax(k,ia)=ax(k,ia)+DelXI(k-KModA(1,ia)+NDimI(KPhase))
          else
            uxo=ax(k,ia)
            uyo=ay(k,ia)
            ax(k,ia)= uxo*csm(k)+uyo*snm(k)
            ay(k,ia)=-uxo*snm(k)+uyo*csm(k)
          endif
        enddo
        do k=1,KModA(2,ia)
          if(KFA(2,ia).eq.1.and.k.eq.KModA(2,ia)) then
            uy(1,k,ia)=uy(1,k,ia)+DelXI(1)
          else
            do j=1,3
              uxo=ux(j,k,ia)
              uyo=uy(j,k,ia)
              ux(j,k,ia)= uxo*csm(k)+uyo*snm(k)
              uy(j,k,ia)=-uxo*snm(k)+uyo*csm(k)
            enddo
          endif
        enddo
        do k=1,KModA(3,ia)
          if(KFA(2,ia).eq.1.and.k.eq.KModA(3,ia)) then
            by(1,k,ia)=by(1,k,ia)+DelXI(1)
          else
            do j=1,6
              uxo=bx(j,k,ia)
              uyo=by(j,k,ia)
              bx(j,k,ia)= uxo*csm(k)+uyo*snm(k)
              by(j,k,ia)=-uxo*snm(k)+uyo*csm(k)
            enddo
          endif
        enddo
      enddo
      go to 9999
9000  call FeChybne(-1.,-1.,'in reference point of the molecule "'//
     1              MolName(im)(:idel(MolName(im)))//'"',ErrSt,
     2              SeriousError)
      ich=1
9999  return
      end
