      subroutine UpdateSummary
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (NLabels=7)
      character*256 Veta
      character*15 :: Labels(NLabels) =
     1                (/'Refine        ',
     2                  'Fourier       ',
     3                  'Dist          ',
     4                  'MakeRefFile   ',
     5                  'StandardRef   ',
     6                  'Absorption    ',
     7                  'DatColl       '/)
      logical ExistFile,NarazilNaKonec,EqIgCase
      ln1=NextLogicNumber()
      call OpenFile(ln1,fln(:ifln)//'.m70','formatted','unknown')
      ln2=NextLogicNumber()
      call OpenFile(ln2,fln(:ifln)//'.l70','formatted','unknown')
      lni=ln1
      lno=ln2
      do i=1,NLabels
        Veta=fln(:ifln)//'_'//Labels(i)(:idel(Labels(i)))//'.l70'
        if(.not.ExistFile(Veta)) cycle
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','unknown')
        NarazilNaKonec=.false.
        nl=0
1100    read(lni,FormA,end=1200) Veta
        write(lno,FormA) Veta(:idel(Veta))
        nl=nl+1
        if(Veta(1:1).eq.'#') then
          k=0
          call Kus(Veta,k,Cislo)
          call Kus(Veta,k,Cislo)
          if(EqIgCase(Labels(i),Cislo)) then
            if(nl.le.5) then
              rewind lno
            else
              backspace lno
              backspace lno
              backspace lno
            endif
            k=0
1120        read(lni,FormA,end=1200) Veta
            if(k.eq.0.and.Veta(1:1).ne.'#') then
              k=1
            else if(k.gt.0.and.Veta(1:1).eq.'#') then
              backspace lni
              go to 1220
            endif
            go to 1120
          endif
        endif
        go to 1100
1200    NarazilNaKonec=.true.
1220    write(lno,FormA)
1240    read(ln,FormA,end=1300) Veta
        write(lno,FormA) Veta(:idel(Veta))
        go to 1240
1300    if(.not.NarazilNaKonec) then
          write(lno,FormA)
1320      read(lni,FormA,end=1500) Veta
          write(lno,FormA) Veta(:idel(Veta))
          go to 1320
        endif
1500    close(ln,status='delete')
        ln=lno
        lno=lni
        lni=ln
        rewind lni
        rewind lno
      enddo
      call CloseIfOpened(lno)
      call CloseIfOpened(lni)
      if(lno.eq.ln1) then
        call MoveFile(fln(:ifln)//'.l70',fln(:ifln)//'.m70')
      else
        call DeleteFile(fln(:ifln)//'.l70')
      endif
      return
100   format('#',71('='))
      end
