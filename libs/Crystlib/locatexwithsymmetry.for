      subroutine LocateXWithSymmetry(x,xa,n,isw,dmez,ia,isym,ivt,xt)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(*),xa(3,*),y1(3),y2(3),y3(3),y3r(3),xt(*)
      do ia=1,n
        do jsym=1,NSymm(KPhase)
          call multm(rm(1,jsym,isw,KPhase),xa(1,ia),y1,3,3,1)
          call AddVek(y1,s6(1,jsym,isw,KPhase),y2,3)
          do ivt=1,NLattVec(KPhase)
            do m=1,3
              y3(m)=y2(m)+vt6(m,ivt,isw,KPhase)
              y3(m)=x(m)-y3(m)
              xt(m)=anint(y3(m))
              y3(m)=y3(m)-xt(m)
            enddo
            call multm(MetTens(1,isw,KPhase),y3,y3r,3,3,1)
            dist=sqrt(scalmul(y3,y3r))
            if(dist.le.dmez) go to 9999
          enddo
        enddo
      enddo
      ia=0
      isym=0
      ivt=0
9999  return
      end
