      integer function CrSystemFromCell(Cell,DiffAxe,DiffAngle)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Cell(6)
      CrSystemFromCell=CrSystemTriclinic
      n90=0
      MainAxis=0
      do i=1,3
        if(abs(Cell(i+3)-90.).lt.DiffAngle) then
          n90=n90+1
        else
          MainAxis=i
        endif
      enddo
      izn=1
      if(n90.eq.2) then
        if(abs(Cell(MainAxis+3)-120.).lt.DiffAngle.or.
     1     abs(Cell(MainAxis+3)- 60.).lt.DiffAngle) then
          i=mod(MainAxis,3)+1
          j=mod(MainAxis+1,3)+1
          if(abs(Cell(i)-Cell(j)).lt.DiffAxe) then
            CrSystemFromCell=CrSystemHexagonal
            if(abs(Cell(MainAxis+3)-60.).lt.DiffAngle) izn=-1
            if(MainAxis.eq.3) MainAxis=0
          else
            CrSystemFromCell=CrSystemMonoclinic
          endif
        else
          CrSystemFromCell=CrSystemMonoclinic
        endif
        CrSystemFromCell=(MainAxis*10+CrSystemFromCell)*izn
        go to 2000
      endif
      if(n90.eq.3) then
        if(abs(Cell(1)-Cell(2)).le.DiffAxe) then
          if(abs(Cell(1)-Cell(3)).le.DiffAxe) then
            CrSystemFromCell=CrSystemCubic
          else
            CrSystemFromCell=CrSystemTetragonal
          endif
        else
          if(abs(Cell(1)-Cell(3)).le.DiffAxe) then
            CrSystemFromCell=20+CrSystemTetragonal
          else if(abs(Cell(2)-Cell(3)).le.DiffAxe) then
            CrSystemFromCell=10+CrSystemTetragonal
          else
            CrSystemFromCell=CrSystemOrthorhombic
          endif
        endif
      else if(n90.eq.0) then
        do i=2,3
          if(abs(Cell(1)-Cell(i)).gt.DiffAxe) go to 2000
          if(abs(Cell(4)-Cell(i+3)).gt.DiffAxe) go to 2000
        enddo
        CrSystemFromCell=-CrSystemTrigonal
      endif
2000  return
      end
