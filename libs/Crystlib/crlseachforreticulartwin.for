      subroutine CrlSeachForReticularTwin
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer CrSystemTwin,TwinOrder,TwinFlag(48),OpShown(48),OpSel(48)
      real CellTwin(6),CellTrTwin(36),CellTrTwinI(36),RPG(9,48),RPom(9),
     1     QPom(9)
      character*256 Veta
      logical EqRV,VolatHide,CrwLogicQuest,FeYesNoHeader
      id=NextQuestId()
      xqd=500.
      il=11
      Veta='Look for reticular twinning'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      Veta='Tolerances for crystal system recognition:'
      il=1
      tpom=xqd*.5
      call FeQuestLblMake(id,tpom,il,Veta,'C','B')
      il=il+1
      tpom=5.
      Veta='Cell parameters:'
      write(Veta(idel(Veta)+1:),'(3f7.3,3f7.2)')(CellPar(j,1,KPhase),
     1                                         j=1,6)
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta='Maximal deviation for %cell lengths in [A]'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      do i=1,2
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          pom=DiffAxe
          Veta='Maximal deviation for cell %angles in deg'
          nEwdDiffAxe=EdwLastMade
        else if(i.eq.2) then
          pom=DiffAngle
          Veta='Maximal deviation for %modulation vector'
          nEwdDiffAngle=EdwLastMade
        else
          pom=DiffModVec
          nEwdDiffModVec=EdwLastMade
        endif
        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
      enddo
      il=il+1
      Veta='%Run test for more symmetrical supercell'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRun=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,5.,il,' ','L','N')
      call FeQuestLblOff(LblLastMade)
      nLblTwinOrder=LblLastMade
      XShow=QuestXMin(id)+10.
      YShow=QuestYPosition(id,il+4)+QuestYMin(id)-5.
      VolatHide=.false.
      il=il+1
      xpom=300.
      dpom=30.
      Veta='C%oset#'
      tpom=xpom-5.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'R',dpom,EdwYd,1)
      nEdwCoset=EdwLastMade
      il=il+1
      Veta='O%peration#'
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'R',dpom,EdwYd,1)
      nEdwOperation=EdwLastMade
      il=il+1
      Veta='%Select as representative'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'R',CrwgXd,CrwgYd,1,0)
      nCrwRepresentative=CrwLastMade
      NPG=0
      ICoset=0
      OpShown=0
      OpSel=0
1400  if(NPG.gt.0) then
        write(Veta,'(''Twin order:'',i2)') TwinOrder
        ICoset=2
        call FeQuestIntEdwOpen(nEdwCoset,ICoset,.false.)
        call FeQuestEudOpen(nEdwCoset,2,TwinOrder,1,0.,0.,0.)
        call FeQuestIntEdwOpen(nEdwOperation,1,.false.)
        call FeQuestEudOpen(nEdwOperation,1,NPG/TwinOrder,1,0.,0.,0.)
        OpShown(ICoset)=OpSel(ICoset)
      else
        Veta='No twinning'
        NPG=0
        if(VolatHide) call CrlMatHide
        call FeQuestEdwClose(nEdwCoset)
        call FeQuestEdwClose(nEdwOperation)
        call FeQuestCrwClose(nCrwRepresentative)
      endif
      call FeQuestLblChange(nLblTwinOrder,Veta)
1450  if(NPG.gt.0) then
        if(VolatHide) call CrlMatHide
        call CrlMatShow(XShow,YShow,3,0,RPG(1,OpShown(ICoset)),3)
        VolatHide=.true.
        call FeQuestCrwOpen(nCrwRepresentative,
     1                      OpSel(ICoset).eq.OpShown(ICoset))
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRun) then
        call UnitMat(CellTrTwin,NDim(KPhase))
        CrSystemTwin=mod(CrSystem(KPhase),10)
        CellTwin(1:6)=CellPar(1:6,1,KPhase)
        call DRSGTestSuperCell(CellTwin,CellTrTwin,CrSystemTwin,1,ich)
        if(CrSystemTwin.gt.mod(CrSystem(KPhase),10)) then
          call MatInv(CellTrTwin,CellTrTwinI,pom,3)
          if(CrSystemTwin.eq.1) then
            Cislo='-1'
          else if(CrSystemTwin.eq.2) then
            NPG=4
            do i=1,NPG
              call UnitMat(RPG(1,i),3)
              if(i.eq.2) then
                RPG(1,i)=-1.
                RPG(9,i)=-1.
              else if(i.eq.3) then
                RPG(1,i)=-1.
                RPG(5,i)=-1.
                RPG(9,i)=-1.
              else if(i.eq.4) then
                RPG(5,i)=-1.
              endif
            enddo
            go to 1550
          else if(CrSystemTwin.eq.3) then
            Cislo='mmm'
          else if(CrSystemTwin.eq.4) then
            Cislo='4/mmm'
          else if(CrSystemTwin.eq.5) then
            Cislo='-3m'
          else if(CrSystemTwin.eq.6) then
            Cislo='6/mmm'
          else if(CrSystemTwin.eq.7) then
            Cislo='m-3m'
          endif
          call GenPg(Cislo,RPG,NPG,ich)
          if(ich.ne.0) then
            NPG=0
            go to 1400
          endif
1550      do i=1,NPG
            call MultM(CellTrTwin,RPG(1,i),RPom,3,3,3)
            call MultM(RPom,CellTrTwinI,RPG(1,i),3,3,3)
          enddo
          TwinOrder=0
          TwinFlag=0
          do j=1,NPG
            if(TwinFlag(j).ne.0) cycle
            TwinOrder=TwinOrder+1
            OpSel(TwinOrder)=j
            call CopyMat(RPG(1,j),RPom,3)
            do i=1,NSymm(KPhase)
              call MultM(RPom,rm(1,i,1,KPhase),QPom,3,3,3)
              do k=1,NPG
                if(EqRV(QPom,RPG(1,k),9,.001)) then
                  TwinFlag(k)=TwinOrder
                  go to 1600
                endif
              enddo
1600        enddo
          enddo
        endif
        go to 1400
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwCoset) then
        call FeQuestIntFromEdw(nEdwCoset,ICoset)
        if(OpShown(ICoset).le.0) then
          do i=1,NPG
            if(TwinFlag(i).eq.ICoset) then
              OpShown(ICoset)=i
              OpSel(ICoset)=i
              exit
            endif
          enddo
        endif
        go to 1450
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwOperation)
     1  then
        call FeQuestIntFromEdw(nEdwOperation,j)
        k=0
        do i=1,NPG
          if(TwinFlag(i).eq.ICoset) then
            k=k+1
            if(k.eq.j) then
              OpShown(ICoset)=i
              go to 1450
            endif
          endif
        enddo
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwRepresentative) then
        if(CrwLogicQuest(nCrwRepresentative)) then
          OpSel(ICoset)=OpShown(ICoset)
        else
          j=0
          do i=1,NPG
            if(TwinFlag(i).ne.ICoset) cycle
            j=j+1
            if(OpSel(ICoset).eq.i) then
              j=mod(j,NPG/TwinOrder)+1
              exit
            endif
          enddo
          k=0
          do i=1,NPG
            if(TwinFlag(i).ne.ICoset) cycle
            k=k+1
            if(j.eq.k) then
              OpSel(ICoset)=i
              exit
            endif
          enddo
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0.and.NPG.gt.0) then
        NTwin=TwinOrder
        sctw(NTwin,KDatBlock)=.5
        j=mxscu
3300    if(sc(j,1).le.0.) then
          j=j-1
          if(j.gt.1) go to 3300
        endif
        mxscutw=max(j+NTwin-1,6)
        mxscu=mxscutw-NTwin+1
        do i=2,NTwin
          call CopyMat(RPG(1,OpSel(i)),RTw(1,i),3)
        enddo
        call iom50(1,0,Fln(:iFln)//'.m50')
        call iom50(0,0,Fln(:iFln)//'.m50')
        call iom40(1,0,Fln(:iFln)//'.m40')
        call iom40(0,0,Fln(:iFln)//'.m40')
        NInfo=1
        TextInfo(1)='A change of the twinning has been detected and '//
     1              'it calls for re-creation of the reflection file.'
        if(FeYesNoHeader(-1.,-1.,'Do you want to re-create '//
     1                   'refinement reflection file just now?',1)) then
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          call EM9CreateM90(0,ich)
        endif
      endif
      if(VolatHide) call CrlMatHide
      call FeQuestRemove(id)
      return
      end
