      subroutine DistFromAtom(At,dmez,isw)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(6),x1(3),y1(6),y2(6),u(3),ug(3),idist(50),uxp(6),
     1          uyp(6),uxpp(6),uypp(6),jsh(3),shj(3),shjc(3),uz(3),
     2          xpi(3),xdp(3),isfa(50)
      logical SymmRelated,EqIgCase
      logical, allocatable :: BratAtP(:)
      character*(*) At
      SymmRelated=index(At,'#').gt.0
      call atsym(At,ia,x1,u,ug,is,ich)
      if(ich.ne.0.and.NAtMolLenAll(KPhase).gt.0) then
        ia=ktat(Atom(NAtMolFrAll(KPhase)),NAtMolLenAll(KPhase),at)
        if(ia.gt.0) then
          ia=NAtMolFrAll(KPhase)+ia-1
          if(iswa(ia).ne.isw) go to 9999
          call CopyVek(x(1,ia),x1,3)
        else
          go to 9999
        endif
      endif
      if(ia.lt.NAtMolFr(isw,KPhase)) then
        ip=1
        ik=NAtInd+NAtPos
        imol=0
        allocate(BratAtP(ik))
        BratAtP=.true.
        go to 1040
      else
        imol=(kmol(ia)-1)/mxp+1
        ik=NAtMolFr(1,KPhase)-1
        do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          ip=ik+1
          ik=ik+iam(im)
          if(im.ge.imol) exit
        enddo
        ik=ik-iam(im)/KPoint(im)*(KPoint(im)-1)
        nsp=NSymm(KPhase)
        ncsp=1
        nvtp=1
        jcellmn=0
        jcellmx=0
        go to 1050
      endif
      entry DistFromPoint(xpi,dmez,isw)
      SymmRelated=.false.
      call CopyVek(xpi,x1,3)
      ip=1
      ik=NAtCalc
      ia=0
      imol=0
1040  nsp=NSymm(KPhase)
      nvtp=NLattVec(KPhase)
      jcellmn=-1
      jcellmx= 1
1050  NDist=0
      dmezi=dmez
      call SetRealArrayTo(xp,6,0.)
      if(imol.eq.0) call bunka(x1,xp,0)
      do i=ip,ik
!        if(.not.BratAtP(i)) cycle
        if(iswa(i).ne.isw.or.kswa(i).ne.KPhase) cycle
        call CopyVek(x(1,i),xp,3)
        js=0
        do jsym=1,nsp
          js=js+1
          if(js.eq.1.and.i.eq.ia.and..not.SymmRelated) cycle
          if(isa(js,i).le.0.and..not.SymmRelated) cycle
          call multm(rm6(1,jsym,isw,KPhase),xp,y1,NDim(KPhase),
     1               NDim(KPhase),1)
          do l=1,NDim(KPhase)
            y1(l)=y1(l)+s6(l,jsym,isw,KPhase)
          enddo
          eps=rm6(NDimQ(KPhase),jsym,isw,KPhase)
          do jcenter=1,nvtp
            call AddVek(y1,vt6(1,jcenter,isw,KPhase),y2,NDim(KPhase))
            if(imol.eq.0) then
              call bunka(y2,shj,1)
            else
              shj=0.
            endif
            do l=1,3
              uz(l)=y2(l)-x1(l)
            enddo
            do jcellx=jcellmn,jcellmx
              cellxj=jcellx
              u(1)=uz(1)+cellxj
              jsh(1)=nint(shj(1))+jcellx
              shjc(1)=shj(1)+cellxj+s6(1,jsym,isw,KPhase)+
     1                vt6(1,jcenter,isw,KPhase)
              do jcelly=jcellmn,jcellmx
                cellyj=jcelly
                u(2)=uz(2)+cellyj
                jsh(2)=nint(shj(2))+jcelly
                shjc(2)=shj(2)+cellyj+s6(2,jsym,isw,KPhase)+
     1                  vt6(2,jcenter,isw,KPhase)
                do 2800jcellz=jcellmn,jcellmx
                  cellzj=jcellz
                  u(3)=uz(3)+cellzj
                  jsh(3)=nint(shj(3))+jcellz
                  shjc(3)=shj(3)+cellzj+s6(3,jsym,isw,KPhase)+
     1                    vt6(3,jcenter,isw,KPhase)
                  call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
                  d=sqrt(scalmul(u,ug))
                  if(d.lt..0001.and.SymmRelated.and.ia.eq.i) cycle
                  if(d.ge.dmezi) go to 2800
                  do j=1,3
                    xdp(j)=u(j)+x1(j)
                  enddo
                  do j=1,ndist
                    if(isf(i).eq.isfa(j)) then
                       do k=1,3
                         if(abs(xdist(k,j)-xdp(k)).gt..00001)
     1                     go to 1200
                       enddo
                       go to 2800
                    endif
1200              enddo
                  m=d*1000.
                  if(ndist.lt.50) then
                    ndist=ndist+1
                    j1=ndist
                  else
                    j1=ipord(50)
                  endif
                  idist(j1)=m
                  if(ndist.eq.50) call indexx(50,idist,ipord)
                  do j=1,3
                    xdist(j,j1)=xdp(j)
                  enddo
                  if(NDimI(KPhase).gt.0) then
                    fi=pi2*y2(4)
                    do k=1,KModA(2,i)
                      fip=fi*float(k)
                      csm=cos(fip)
                      snm=sin(fip)
                      do j=1,3
                        uxp(j)=ux(j,k,i)
                        uyp(j)=uy(j,k,i)
                      enddo
                      do j=4,NDim(KPhase)
                        uxp(j)=0.
                        uyp(j)=0.
                      enddo
                      call multm(rm6(1,jsym,isw,KPhase),uxp,uxpp,
     1                           NDim(KPhase),NDim(KPhase),1)
                      call multm(rm6(1,jsym,isw,KPhase),uyp,uypp,
     1                           NDim(KPhase),NDim(KPhase),1)
                      do j=1,3
                        uxdist(j,k,j1)= eps*uxpp(j)*csm+uypp(j)*snm
                        uydist(j,k,j1)=-eps*uxpp(j)*snm+uypp(j)*csm
                      enddo
                    enddo
                  endif
                  ddist(j1)=d
                  adist(j1)=atom(i)
                  isfa(j1)=isf(i)
                  call scode(jsym,jcenter,shjc,jsh,isw,
     1                       SymCodeDist(j1),SymCodeJanaDist(j1))
                  if(ndist.eq.50) dmezi=idist(ipord(ndist))/1000.
2800            continue
              enddo
            enddo
          enddo
        enddo
      enddo
      if(ndist.gt.1) then
        if(ndist.lt.50) call indexx(ndist,idist,ipord)
      else
        ipord(1)=1
      endif
9999  if(allocated(BratAtP)) deallocate(BratAtP)
      return
      end
