      subroutine CrlPlotStructure
      include 'fepc.cmn'
      include 'basic.cmn'
      logical ProgramPresent,StructureExists,ExistM90Old,ExistFile,
     1        EqIgCase,EqRV
      character*256 :: flnNew=' ',Veta2,flnOld=' '
      integer :: NAtCalcOld=0, KCommenS = 0
      real :: trezs(3) = (/0.,0.,0./)
      if(.not.ProgramPresent('graphic program',CallGraphic)) then
        ich=1
        go to 9999
      endif
      if(flnNew.ne.' ') call DeleteFile(flnNew(:idel(flnNew))//'.cif')
      call FeSetGraphicProgram
      if(.not.EqIgCase(Fln,FlnOld).or.NAtCalc.ne.NAtCalcOld.or.
     1   KCommenS.ne.KCommen(KPhase).or.
     2   .not.EqRV(trezs,trez(1,1,KPhase),NDimI(KPhase),.0001))
     3  call GraphicOutputProlog
      flnOld=fln
      iflnOld=ifln
      NAtCalcOld=NAtCalc
      KCommenS=KCommen(KPhase)
      trezs(1:NDimI(KPhase))=trez(1:NDimI(KPhase),1,KPhase)
      fln=fln(:ifln)//'_tmp'
      ifln=idel(fln)
      n=0
1100  if(StructureExists(fln).and.n.lt.999) then
        n=n+1
        write(Cislo,'(''_'',i3)') n
        do i=1,idel(Cislo)
          if(Cislo(i:i).eq.' ') Cislo(i:i)='0'
        enddo
        fln(ifln+1:)=Cislo(:idel(Cislo))
        go to 1100
      endif
      flnNew=fln
      ifln=idel(fln)
      iflnNew=ifln
      call CopyFile(flnOld(:iflnOld)//'.m40',fln(:ifln)//'.m40')
      call CopyFile(flnOld(:iflnOld)//'.m50',fln(:ifln)//'.m50')
      NDatBlockOld=NDatBlock
      NDatBlock=1
      call iom40(1,0,fln(:ifln)//'.m40')
      KPhaseIn=KPhase
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      Veta2=fln(:ifln)//'.cif'
      if(ExistFile(Veta2)) call DeleteFile(Veta2)
      KPhase=KPhaseIn
      ExistM90Old=ExistM90
      ExistM90=.false.
2000  MakeCIFForGraphicViewer=.true.
      call GraphicOutput(-7,fln(:ifln),ich)
      if(ich.le.-10) then
        ich=ich+10
        Navrat=1
      else
        Navrat=0
      endif
      MakeCIFForGraphicViewer=.false.
      if(ich.eq.0) then
        if(UseVestaForMagDraw.and.MagneticType(KPhase).ne.0) then
          Veta2=Call3dMaps(:idel(Call3dMaps))//' "'//
     1          fln(:ifln)//'.cif"'
          call FeSystemCommand(Veta2,0)
          call FeWaitInfo('end of the graphic visualization.')
          call DeleteFile(fln(:ifln)//'.cif')
          go to 2200
        else if(LocateSubstring(CallGraphic,'platon.exe',
     1                          .false.,.true.).gt.0) then
          Veta2=fln(:ifln)//'.cif'
        else if(LocateSubstring(CallGraphic,'atoms',
     1                          .false.,.true.).gt.0) then
          Veta2=fln(:ifln)//'.cif'
        else
          Veta2=CurrentDir(:idel(CurrentDir))//fln(:ifln)//'.cif'
        endif
        if(LocateSubstring(CallGraphic,'bs.exe',.false.,.true.)
     1                     .le.0.and.
     2     LocateSubstring(CallGraphic,'platon.exe',.false.,
     3                     .true.).le.0.and.
     4     LocateSubstring(CallGraphic,'atoms',.false.,
     5                     .true.).le.0)
     6    Veta2='"'//Veta2(:idel(Veta2))//'"'
        call FeGraphicViewer(Veta2,Navrat)
        call DeleteFile(Veta2)
      else if(ich.eq.-1) then
        if(LocateSubstring(CallGraphic,'atoms',.false.,.true.).
     1     gt.0) then
          Veta2=fln(:ifln)//'.inp'
          call FeGraphicViewer(Veta2,Navrat)
          call DeleteFile(Veta2)
        endif
      endif
2200  if(Navrat.eq.1) then
        if(flnNew.ne.' ') call DeleteFile(flnNew(:idel(flnNew))//'.cif')
        go to 2000
      endif
      call DeleteFile(flnNew(:iflnNew)//'.m40')
      call DeleteFile(flnNew(:iflnNew)//'.m50')
      call DeleteFile(flnNew(:iflnNew)//'.l51')
      fln=flnOld
      ifln=iflnOld
      ExistM90=ExistM90Old
      NDatBlock=NDatBlockOld
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      call FeWait(1.)
      call FeResetGraphicProgram
9999  return
      end
