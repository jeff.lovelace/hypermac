      subroutine SetBasicM41(KDatBlockIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      UseCutOffPwd=.false.
      if(KDatBlockIn.le.0) then
        KDatBFrom=1
        KDatBTo=MxDatBlock
        do KDatB=KDatBFrom,KDatBTo
          do i=1,MxPhases
            DirBroad(1:2,i,KDatB)=0.
            DirPref(1:2,i,KDatB)=0.
            DirBroad(3,i,KDatB)=1.
            DirPref(3,i,KDatB)=1.
          enddo
        enddo
        SatFrMod=.false.
        SkipFriedel=.false.
        call SetIntArrayTo(kiPwd,MxParPwd,0)
        MMaxPwd=0
        do i=1,NPhase
          call CopyVek(CellPar(1,1,i),CellPwd(1,i),6)
          call CopyVek(qu(1,1,1,i),QuPwd(1,1,i),3*NDimI(i))
        enddo
      else
        KDatBFrom=KDatBlockIn
        KDatBTo=KDatBlockIn
      endif
      do KDatB=KDatBFrom,KDatBTo
        NSkipPwd(KDatB)=0
        KBackg(KDatB)=1
        NBackg(KDatB)=5
        KManBackg(KDatB)=0
        NManBackg(KDatB)=0
        KAbsor(KDatB)=0
        MirPwd(KDatB)=0.
        KWleBail(KDatB)=1
        KKleBail(KDatB)=0
        KRough(KDatB)=0
        KRefLam(KDatB)=0
        KUseLamFile(KDatB)=0
        KUseTOFAbs(KDatB)=0
        LamFile(KDatB)=' '
        KAsym(KDatB)=0
        NAsym(KDatB)=0
        KUseTOFJason(KDatB)=0
        TOFKey(KDatB)=0
        KUseHS(KDatB)=1
        call SetRealArrayTo(ShiftPwd(1,KDatB),3,0.)
        call SetRealArrayTo(ShiftPwdS(1,KDatB),3,0.)
        call SetRealArrayTo(GaussPwd(1,1,KDatB),4*MxPhases,0.)
        call SetRealArrayTo(GaussPwdS(1,1,KDatB),4*MxPhases,0.)
        call SetRealArrayTo(LorentzPwd(1,1,KDatB),5*MxPhases,0.)
        call SetRealArrayTo(LorentzPwdS(1,1,KDatB),5*MxPhases,0.)
        call SetRealArrayTo(BroadHKLPwd(1,1,KDatB),10*MxPhases,0.)
        call SetRealArrayTo(BroadHKLPwds(1,1,KDatB),10*MxPhases,0.)
        call SetRealArrayTo(ZetaPwd(1,KDatB),MxPhases,.5)
        call SetRealArrayTo(ZetaPwdS(1,KDatB),MxPhases,.5)
        call SetRealArrayTo(BackgPwd(1,KDatB),MxBackg,0.)
        call SetRealArrayTo(BackgPwdS(1,KDatB),MxBackg,0.)
        KUseInvX(KDatB)=0
        do i=1,MxPhases
          KProfPwd(i,KDatB)=1
          KPartBroad(i,KDatB)=-1
          KStrain(i,KDatB)=0
          SPref(i,KDatB)=2
          KPref(i,KDatB)=0
          PCutOff(i,KDatB)=8.
          NBroadHKLPwd(i,KDatB)=0
        enddo
        if(ExistM90.and.DataType(KDatB).eq.-2.and.
     1     Radiation(KDatB).eq.NeutronRadiation) then
          KUseTOFJason(KDatB)=TOFJason(KDatB)
          ShiftPwd(1,KDatB)=TOFZero(KDatB)
          ShiftPwd(2,KDatB)=TOFDIFC(KDatB)
          ShiftPwd(3,KDatB)=TOFDIFA(KDatB)
          ShiftPwd(4,KDatB)=TOFZeroE(KDatB)
          ShiftPwdS(4,KDatB)=0.
          ShiftPwd(5,KDatB)=TOFCe(KDatB)
          ShiftPwdS(5,KDatB)=0.
          ShiftPwd(6,KDatB)=TOFWCross(KDatB)
          ShiftPwdS(6,KDatB)=0.
          ShiftPwd(7,KDatB)=TOFTCross(KDatB)
          ShiftPwdS(7,KDatB)=0.
          do i=1,MxPhases
            GaussPwd(2,i,KDatB)=100.
            GaussPwdS(2,i,KDatB)=0.
          enddo
          AsymPwd(1,KDatB)=50.
          AsymPwd(2,KDatB)= 0.
          AsymPwd(3,KDatB)=50.
          AsymPwd(4,KDatB)= 0.
          AsymPwdS(1,KDatB)=0.
          AsymPwdS(2,KDatB)=0.
          AsymPwdS(3,KDatB)=0.
          AsymPwdS(4,KDatB)=0.
        else
          do i=1,MxPhases
            GaussPwd(3,i,KDatB)=5.
          enddo
        endif
        KIllum(KDatB)=0
        KFocusBB(KDatB)=0
      enddo
      ExistM41=.true.
      return
      end
