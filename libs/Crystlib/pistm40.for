      subroutine pistm40(file)
      use Atoms_mod
      character*(*) file
      include 'fepc.cmn'
      include 'basic.cmn'
      ln=NextLogicNumber()
      open(ln,file=file)
      do i=1,NAtCalc
        if(NDim(KPhase).eq.3) then
          write(ln,103) atom(i),isf(i),itf(i),ai(i),(x(j,i),j=1,3),
     1                   (beta(j,i),j=1,6)
        else
          write(ln,100) atom(i),isf(i),itf(i),ai(i),(x(j,i),j=1,3),
     1                  (KFA(j,i),j=1,3),(KModA(j,i),j=1,3),
     2                  (beta(j,i),j=1,6)
        endif
        if(itf(i).gt.2) then
          write(ln,101)(c3(j,i),j=1,10)
          if(itf(i).gt.3) then
            write(ln,101)(c4(j,i),j=1,15)
            if(itf(i).gt.4) then
              write(ln,101)(c5(j,i),j=1,21)
              if(itf(i).gt.5) then
                write(ln,101)(c6(j,i),j=1,28)
              endif
            endif
          endif
        endif
        if(KModA(1,i).gt.0) then
          write(ln,101) a0(i)
          write(ln,101)(ax(j,i),ay(j,i),j=1,KModA(1,i))
        endif
        do j=1,KModA(2,i)
          write(ln,101)(ux(k,j,i),k=1,3),(uy(k,j,i),k=1,3)
        enddo
        do j=1,KModA(3,i)
          write(ln,101)(bx(k,j,i),k=1,3),(by(k,j,i),k=1,3)
        enddo
      enddo
      close(ln)
100   format(a8,2i3,4x,4f9.6,6x,3i1,3i3/6f9.6,6x,10i1)
101   format(6f9.6)
102   format(1x,a8,2i3,4x,4f9.6/1x,6f9.6,3x,'=>',10i1,'<=')
103   format(a8,2i3,4x,4f9.6/6f9.6)
      return
      end
