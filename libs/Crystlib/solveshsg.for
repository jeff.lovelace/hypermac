      subroutine SolveShSg(rx,px,n,nx,ich)
      include 'fepc.cmn'
      dimension rx(2*n,n),px(2*n)
      m=2*n
      ich=0
      call TriangShSg(rx,px,m,n)
      nx=0
      do 4000i=1,m
        do j=1,n
          if(abs(rx(i,j)).gt..0001) go to 3000
        enddo
        call od0do1(px(i),px(i),1)
        if(abs(px(i)).gt..001.and.abs(px(i)-1.).gt..001) then
          ich=1
          px(i)=0.
        endif
        go to 4000
3000    nx=nx+1
4000  continue
      return
      end
