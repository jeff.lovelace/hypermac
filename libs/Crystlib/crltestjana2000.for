      subroutine CrlTestJana2000(Change)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension xp(15),ip(3),nsup(3),h(6),ipp(15),
     1          NRefP(MxRefBlock),NRefK(MxRefBlock),NRefD(MxRefBlock)
      character*256 NewFln,t256,EdwStringQuest
      character*80 Veta,FormulaNew,Format95Old,Format95New
      character*30 Format91old
      integer FeChdir
      logical EqIgCase,WasPowder,KonecDat,StructureExists,FeYesNo,
     1        CtiRefLam,ExistFile,Change,EqRV0
      data Format91Old/'(3i4,2f9.1,3i2,8f8.4, e15.6)'/
      data Format95Old
     1  /'(i6,3i4,4f7.2,2e15.6,f10.3,3i2/2e15.6,8f8.4,i8,2f10.5)'/
      KPhaseIn=KPhase
      Change=.false.
      lni=0
      lno=0
      ln=0
      NewFln=fln(:ifln)//'-2006'
      Format95New=Format95
      call OpenFile(m50,fln(:ifln)//'.m50','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      read(m50,FormA) Veta
      close(m50)
      k=0
      call kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'version')) then
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'Jana2006')) then
          go to 9999
        endif
      endif
      KDatBlock=1
      NDatBlock=1
      KRefBlock=1
      NRefBlock=1
      wasPowder=ExistFile(fln(:ifln)//'.m41')
      isPowder=wasPowder
      call PwdSetTOF(KRefBlock)
      if(wasPowder) then
        ExistPowder=.true.
        ExistSingle=.false.
      else
        ExistPowder=.false.
        ExistSingle=.true.
      endif
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      call iom50old(0,0)
      CoreValSource='Default'
      call CrlExpandCentroSym
      call SetMet(0)
      call ResetIgnoreW
      call ResetIgnoreE
      call FeBottomInfo(' ')
      do KPh=1,NPhase
        call SetFormula(Formula(KPh))
      enddo
      ich=0
      id=NextQuestId()
      xqd=450.
      il=6
      Veta='The Jana2000 format has been detected.'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,
     1                   -1,0)
      il=1
      call FeQuestLblMake(id,xqd*.5,il,'What to do?','C','N')
      xpom=5.
      tpom=xpom+CrwgXd+5.
      nSel=2
      do i=1,3
        il=il+1
        if(i.eq.1) then
          Veta='%Omit the stucture'
        else if(i.eq.2) then
          Veta='%Transform it to Jana2006'
        else if(i.eq.3) then
          Veta='%Restart it in Jana2000'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,1)
        if(i.eq.1) then
          nCrwOmit=CrwLastMade
        else if(i.eq.2) then
          nCrwTransform=CrwLastMade
        else if(i.eq.3) then
          nCrwJana2000=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.nSel)
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      call FeQuestLinkaOff(LinkaLastMade)
      nLinka=LinkaLastMade
      il=il+1
      tpom=5.
      Veta='%New structure name'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=250.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNewStructure=EdwLastMade
      xpom=xpom+dpom+15.
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
1030  if(NSel.eq.2) then
        call FeQuestStringEdwOpen(nEdwNewStructure,NewFln)
        call FeQuestButtonOpen(nButtBrowse,ButtonOff)
        call FeQuestLinkaOn(nLinka)
      else
        call FeQuestEdwClose(nEdwNewStructure)
        call FeQuestButtonClose(nButtBrowse)
        call FeQuestLinkaOff(nLinka)
      endif
1050  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        if(CheckNumber.ge.nCrwOmit.and.
     1     CheckNumber.le.nCrwJana2000) then
          nSel=CheckNumber-nCrwOmit+1
          go to 1030
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtBrowse) then
        NewFln=EdwStringQuest(nEdwNewStructure)
        call FeFileManager('Define a new Jana2006 struture name',
     1                     NewFln,' ',1,.true.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwNewStructure,NewFln)
        EventType=EventEdw
        EventNumber=nEdwNewStructure
        go to 1050
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1050
      endif
      if(NSel.eq.2) then
        NewFln=EdwStringQuest(nEdwNewStructure)
        if(NewFln.eq.fln) then
          call FeChybne(-1.,-1.,'The name of Jana2006 structure files',
     1                  'must be different from the original one',
     2                  SeriousError)
          go to 1075
        endif
        if(NewFln.ne.' ') then
          if(StructureExists(NewFln)) then
            call ExtractFileName(NewFln,Veta)
            if(.not.FeYesNo(-1.,YBottomMessage,'The structure "'//
     1         Veta(:idel(Veta))//'" already exists, rewrite it?',0))
     2        go to 1075
          endif
        endif
      endif
      go to 1070
1075  call FeQuestButtonOff(ButtonOK-ButtonFr+1)
      go to 1050
1070  call FeQuestRemove(id)
      if(NSel.eq.1) then
        call DeletePomFiles
        fln=' '
        ifln=0
        go to 9999
      else if(NSel.eq.3) then
        call FeGrQuit
        call DeletePomFiles
        call FeTmpFilesDelete
        t256='c:\jana2000\jana2000.exe '//fln(:ifln)
        call FeSystem(t256)
        call FeWait(10.)
        stop
      endif
      if(Radiation(1).eq.XRayRadiation) then
        ExistNeutronData=.false.
        ExistXRayData=.true.
        do KPh=1,NPhase
          KPhase=KPh
          do i=1,NAtFormula(KPh)
            call RealAFromAtomFile(AtTypeFull(i,KPh),
     1             'Neutron_scattering_length_real',xp,2,ich)
            ffn(i,KPh)=xp(2)
            call RealAFromAtomFile(AtTypeFull(i,KPh),
     1              'Neutron_scattering_length_imaginary',xp,2,ich)
            ffni(i,KPh)=xp(2)
          enddo
          if(FFType(KPh).eq.-56) FFType(KPh)=-62
        enddo
      else if(Radiation(1).eq.NeutronRadiation) then
        ExistNeutronData=.true.
        ExistXRayData=.false.
        do KPh=1,NPhase
          KPhase=KPh
          do i=1,NAtFormula(KPh)
            call RealAFromAtomFile(AtTypeFull(i,KPh),
     1        'Xray_form_factor_in_steps',FFBasic(1,i,KPh),62,ich)
            FFType(KPh)=-62
          enddo
        enddo
      endif
      call SetBasicM95(1)
      if(wasPowder) then
        DataType(1)=2
      else
        DataType(1)=1
      endif
      LamAveRefBlock(1)=LamAve(1)
      RadiationRefBlock(1)=Radiation(1)
      PolarizationRefBlock(1)=LpFactor(1)
      TempRefBlock(1)=DatCollTemp(1)
      AngleMonRefBlock(1)=AngleMon(1)
      NAlfaRefBlock(1)=NAlfa(1)
      LamA1RefBlock(1)=LamA1(1)
      LamA2RefBlock(1)=LamA2(1)
      LamRatRefBlock(1)=LamRat(1)
      RefDatCorrespond(1)=1
      call CrlChangeL51(fln,NewFln,0)
      do KPh=1,NPhase
        call SetFormula(Formula(KPh))
      enddo
      call iom50(1,0,NewFln(:idel(NewFln))//'.m50')
      call iom40old(0,0)
      do KPh=1,NPhase
        pom=float(NLattVec(KPh)*NSymm(KPh))/float(NUnits(KPh))
        call SetRealArrayTo(AtMult(1,KPh),NAtFormula(KPh),0.)
        do i=1,NAtCalc
          if(kswa(i).ne.KPh) cycle
          j=isf(i)
          pomm=ai(i)*pom*CellVol(1,KPh)/CellVol(iswa(i),KPh)
          if(KModA(1,i).gt.0) then
            pomm=pomm*a0(i)
          else if(KFA(2,i).gt.0) then
            pomm=pomm*uy(2,KModA(2,i),i)
          endif
          AtMult(j,KPh)=AtMult(j,KPh)+pomm
        enddo
        do i=1,NAtFormula(KPh)
          if(i.eq.1) then
            FormulaNew=AtType(i,KPh)
          else
            FormulaNew=FormulaNew(:idel(FormulaNew)+1)//
     1           AtType(i,KPh)(:idel(AtType(i,KPh)))
          endif
          if(abs(AtMult(i,KPh)-1.).lt..0001) cycle
          write(Cislo,'(f15.3)') AtMult(i,KPh)
          call ZdrcniCisla(Cislo,1)
          FormulaNew=FormulaNew(:idel(FormulaNew))//
     1               Cislo(:idel(Cislo))
        enddo
        Formula(KPh)=FormulaNew
      enddo
      call iom50(1,0,NewFln(:idel(NewFln))//'.m50')
      if(WasPowder) then
        do KPh=1,NPhase
          if(NDimI(KPh).eq.1.and.
     1       KStrain(KPh,KDatBlock).eq.IdPwdStrainTensor) then
            IZdvih=(KPh-1)*NParCellProfPwd+IStPwd-1
            call CopyVek(StPwd(1,KPh,KDatBlock),xp,15)
            call CopyVekI(KiPwd(IZdvih+1),ipp,15)
            m4=0
            m3=0
            do i1=1,4
              do i2=i1,4
                do i3=i2,4
                  do i4=i3,4
                    m4=m4+1
                    if(i1.eq.4.or.i2.eq.4.or.i3.eq.4.or.i4.eq.4) then
                      StPwd(m4,KPh,KDatBlock)=0.
                      KiPwd(IZdvih+m4)=0
                    else
                      m3=m3+1
                      StPwd(m4,KPh,KDatBlock)=xp(m3)
                      KiPwd(IZdvih+m4)=ipp(m3)
                    endif
                  enddo
                enddo
              enddo
            enddo
          endif
        enddo
      endif
      call iom40(1,0,NewFln(:idel(NewFln))//'.m40')
      call CopyVek(CellPar,CellRefBlock(1,0),6)
      call CopyVek(Qu(1,1,1,KPhase),QuRefBlock(1,1,0),3*NDimI(KPhase))
      call UnitMat(TrMP,NDim(KPhase))
      lno=NextLogicNumber()
      call OpenFile(lno,NewFln(:idel(NewFln))//'.l01','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 9900
      lni=NextLogicNumber()
      if(wasPowder) then
        write(Cislo,'(2l4)') ExistXRayData,ExistNeutronData
        DifCode(1)=IdDataM92
        t256=fln(:ifln)//'.m92'
        SourceFileRefBlock(1)=t256
        call FeGetFileTime(SourceFileRefBlock(1),
     1                     SourceFileDateRefBlock(1),
     2                     SourceFileTimeRefBlock(1))
        call OpenFile(lni,t256,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        NLines95(1)=0
        KonecDat=.false.
1600    read(lni,FormA,end=1700) t256
        if(t256.eq.' ') go to 1600
        k=0
        call StToReal(t256,k,xp,1,.false.,ich)
        if(xp(1).lt.900.) then
          if(xp(1).lt.0.) go to 1600
          NLines95(1)=NLines95(1)+1
          i=index(t256,'-')
          if(i.gt.0) t256(i:i)=' '
          write(lno,FormA) t256(:idel(t256))
          go to 1600
        else
          if(KonecDat) then
            go to 1700
          else
            write(lno,'(''manual_background'')')
            KonecDat=.true.
            go to 1600
          endif
        endif
1700    NRef95(1)=NLines95(1)
        n=1
        go to 5000
      else
        KPhaseDR=KPhase
        t256=fln(:ifln)//'.m95'
        write(Format95Old(5:5),'(i1)') NDim(KPhase)
        Format95New(5:5)=Format95Old(5:5)
        Format95(5:5)=Format95Old(5:5)
        if(ExistFile(t256)) then
          call iom94(0)
          call iom50old(0,0)
          call comsym(0,0,ich)
          if(ich.ne.0) go to 9000
          do i=1,NDimI(KPhase)
            if(EqRV0(QuRefBlock(1,i,0),3*NDimI(KPhase),.0001)) then
              call SetRealArrayTo(xp,NDim(KPhase),0.)
              call CopyVek(qu(1,i,1,KPhase),xp,3)
              call Multm(xp,TrMP,h,1,NDim(KPhase),NDim(KPhase))
              call CopyVek(h,QuRefBlock(1,i,0),3)
            endif
          enddo
          call OpenFile(lni,t256,'formatted','unknown')
          if(ErrFlag.ne.0) go to 4000
          if(DifCode(1).eq.99) then
            read(lni,FormA) t256
            if(EqIgCase(t256,'import_report_begin')) then
              n=0
2200          read(lni,FormA) t256
              if(EqIgCase(t256,'import_report_end')) go to 2400
              n=n+1
              if(n.gt.1) then
                call SetBasicM95(n)
                if(wasPowder) then
                  DataType(n)=2
                else
                  DataType(n)=1
                endif
                LamAveRefBlock(n)=LamAve(1)
                RadiationRefBlock(n)=Radiation(1)
                PolarizationRefBlock(n)=LpFactor(1)
                TempRefBlock(n)=DatCollTemp(1)
                AngleMonRefBlock(n)=AngleMon(1)
                NAlfaRefBlock(n)=NAlfa(1)
                LamA1RefBlock(n)=LamA1(1)
                LamA2RefBlock(n)=LamA2(1)
                LamRatRefBlock(n)=LamRat(1)
                RefDatCorrespond(n)=1
                call CopyMat(ub(1,1,1),ub(1,1,n),3)
              endif
              if(n.eq.1) then
                NRefP(1)=1
              else
                NRefP(n)=NRefK(n-1)+1
              endif
              SourceFileRefBlock(n)=t256
              call FeGetFileTime(SourceFileRefBlock(n),
     1                           SourceFileDateRefBlock(n),
     2                           SourceFileTimeRefBlock(n))
2210          read(lni,FormA) t256
              if(t256.eq.' ') go to 2210
              read(t256,102,err=2210) DifCode(n),NDim95(n),i,
     1          ITwRead(n),MMaxRefBlock(n),UseTrRefBlock(n),NSup,
     2          (DiffSatRefBlock(i,n),i=1,3)
              if(DifCode(n).eq.1) then
                DifCode(n)=IdImportGeneralF
              else if(DifCode(n).eq.2) then
                DifCode(n)=IdImportSHELXF
              else if(DifCode(n).eq.3) then
                DifCode(n)=IdImportSHELXI
              else if(DifCode(n).eq.4) then
                DifCode(n)=IdImportIPDS
              else if(DifCode(n).eq.5) then
                DifCode(n)=IdImportDatRed
              else if(DifCode(n).eq.6) then
                DifCode(n)=IdImportGeneralF
              else if(DifCode(n).eq.7) then
                DifCode(n)=IdImportGeneralI
              else if(DifCode(n).eq.8) then
                DifCode(n)=IdImportCCDBruker
              else if(DifCode(n).eq.9) then
                DifCode(n)=-13
              else if(DifCode(n).eq.10) then
                DifCode(n)=IdImportHKLF5
              endif
              pom=0.
              do i=1,3
                pom=pom+abs(DiffSatRefBlock(i,n))
              enddo
              if(pom.lt..001)
     1          call SetRealArrayTo(DiffSatRefBlock(1,n),3,.01)
              if(nsup(1).le.0) then
                NDimp=NDim(KPhase)
              else
                NDimp=NDim95(n)
              endif
              do i=1,NDim95(n)
                read(lni,101,err=4000)
     1            (TrRefBlock(i+(j-1)*NDim95(n),n),j=1,NDimp)
              enddo
              read(lni,FormA,err=4000) FormatRefBlock(n)
              if(DifCode(n).eq.IdImportDatRed)
     1          FormatRefBlock(n)=FormatRefBlock(n)(:5)//Format95(6:)
              read(lni,103,err=4000) NRefD(n)
              if(NRefD(n).le.0.and.n.eq.1) then
                NRefK(1)=999999999
              else
                NRefK(n)=NRefP(n)+NRefD(n)-1
              endif
              go to 2200
2400          do i=1,n
                NRef95(i)=0
                NLines95(i)=0
                do j=NRefP(i),NRefK(i)
                  Format95=Format95Old
                  call DRGetReflectionFromM95(lni,iend,ich)
                  if(iend.ne.0.or.ich.ne.0) go to 4000
                  Format95=Format95New
                  call DRPutReflectionToM95(lno,nl)
                  NRef95(i)=NRef95(i)+1
                  NLines95(i)=NLines95(i)+nl
                enddo
                call CloseIfOpened(lno)
                write(Cislo,'(''.l'',i2)') i+1
                if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
                call OpenFile(lno,NewFln(:idel(NewFln))//
     1                        Cislo(:idel(Cislo)),'formatted','unknown')
              enddo
            else
              call CloseIfOpened(lni)
              go to 2900
            endif
          else
            n=1
            if(DifCode(1).eq.2) then
              DifCode(1)=IdCAD4
            else if(DifCode(1).eq.3) then
              DifCode(1)=IdSiemensP4
            else if(DifCode(1).eq.4) then
              DifCode(1)=IdIPDSStoe
            else if(DifCode(1).eq.5) then
              DifCode(1)=IdD9ILL
              CorrLp(1)=-1
            else if(DifCode(1).eq.6) then
              DifCode(1)=IdHasyLabF1
              CorrLp(1)=-1
            else if(DifCode(1).eq.7) then
              DifCode(1)=IdKumaCCD
              CorrLp(1)=-1
            else if(DifCode(1).eq.8) then
              DifCode(1)=IdKumaPD
            else if(DifCode(1).eq.9) then
              DifCode(1)=IdNoniusCCD
              CorrLp(1)=-1
            else if(DifCode(1).eq.10) then
              DifCode(1)=IdBrukerCCD
              CorrLp(1)=-1
            else if(DifCode(1).eq.11) then
              DifCode(1)=IdXDS
              CorrLp(1)=-1
            endif
            SourceFileRefBlock(1)='?Imported from Jana2000 - original'//
     1                            ' source unknown'
            SourceFileDateRefBlock(1)='?'
            SourceFileTimeRefBlock(1)='?'
            NRef95(1)=0
2800        Format95=Format95Old
            call DRGetReflectionFromM95(lni,iend,ich)
            if(iend.ne.0) go to 5000
            if(ich.ne.0) go to 4000
            call CopyVekI(ih,ip,3)
            Format95=Format95New
            call DRPutReflectionToM95(lno,nl)
            NRef95(1)=NRef95(1)+1
            NLines95(1)=NLines95(1)+nl
            go to 2800
          endif
          go to 5000
        endif
2900    t256=fln(:ifln)//'.m91'
        n=1
        if(ExistFile(t256)) then
          call UnitMat(TrMP,NDim(KPhase))
          call CopyVek(CellPar,CellRefBlock(1,0),6)
          call CopyVek(Qu(1,1,1,KPhase),QuRefBlock(1,1,0),
     1                 3*NDimI(KPhase))
          SourceFileRefBlock(1)=t256
          DifCode(1)=IdImportGeneralI
          call FeGetFileTime(SourceFileRefBlock(1),
     1                       SourceFileDateRefBlock(1),
     2                       SourceFileTimeRefBlock(1))
          call OpenFile(lni,t256,'formatted','unknown')
          if(ErrFlag.ne.0) go to 4000
          read(lni,FormA,end=5000) t256
          i=NDim(KPhase)*4+19
          Veta=t256(i:)
          i=1
3000      if(Veta(i:i).eq.' '.and.i.lt.8) then
            i=i+1
            go to 3000
          endif
3010      if(Veta(i:i).ne.' '.and.i.lt.8) then
            i=i+1
            go to 3010
          endif
          write(Format91Old( 2: 2),100) NDim(KPhase)
          write(Format91Old(14:14),100) i-1
          idl=idel(t256)
          i=NDim(KPhase)*4+18+3*(i-1)
          if(idl.eq.i+16.or.idl.eq.i+64) then
            CtiRefLam=.true.
          else
            CtiRefLam=.false.
          endif
          FormatRefBlock(1)=Format91Old
          call SetRealArrayTo(uhly,4,0.)
          call SetRealArrayTo(dircos,6,0.)
          call SetRealArrayTo(corrf,2,1.)
          call SetIntArrayTo(iflg,2,1)
          iflg(3)=0
          tbar=0.
          NProf=0
          rewind lni
          NRef95(1)=0
3200      if(CtiRefLam) then
            read(lni,Format91Old,err=4000,end=5000)
     1        (ih(i),i=1,NDim(KPhase)),ri,rs,ip,tbar,RefLam,DirCos
          else
            read(lni,Format91Old,err=4000,end=5000)
     1        (ih(i),i=1,NDim(KPhase)),ri,rs,ip,tbar,RefLam,DirCos
            RefLam=0.
          endif
          if(ih(1).gt.900) go to 5000
          iflg(1)=ip(1)
          iflg(2)=ip(3)
          call FromIndSinthl(ih,h,sinthl,sinthlq,1,0)
          pom=sinthl*LamAveRefBlock(KRefBlock)
          if(pom.lt.1.) then
            uhly(3)=asin(pom)/ToRad
          else
            uhly(3)=89.
          endif
          uhly(4)=uhly(3)
          NRef95(1)=NRef95(1)+1
          no=NRef95(1)
          expos=float(no)*.1
          call DRPutReflectionToM95(lno,nl)
          NLines95(1)=NLines95(1)+nl
          go to 3200
        endif
      endif
4000  ErrFlag=0
      call DeleteFile(NewFln(:idel(NewFln))//'.m95')
      call DeleteFile(NewFln(:idel(NewFln))//'.l01')
5000  Format95=Format95New
      call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      call ChangeUSDFile(fln,'closed','*')
      fln=NewFln
      ifln=idel(fln)
      call iom50(0,0,fln(:ifln)//'.m50')
      NRefBlock=n
      Veta=NewFln(:idel(NewFln))//'.m95'
      if(ExistFile(NewFln(:idel(NewFln))//'.l01')) call iom95(1,Veta)
      if(ExistFile(Veta)) then
        call CompleteM95(0)
        ExistM90=.false.
        call SetLogicalArrayTo(ModifyDatBlock,MxDatBlock,.true.)
        call EM9CreateM90(1,ich)
      endif
      call iom90(0,fln(:ifln)//'.m90')
      call ChangeUSDFile(fln,'opened','*')
      Change=.true.
      DefRealRefine(nCmdblkoef)=0.
      call RefOpenCommands
      DefRealRefine(nCmdblkoef)=.01
      DefaultReal(nCmdblkoef)=.01
      if(NacetlReal(nCmdsnlmn).gt.0.or.
     1   NacetlReal(nCmdsnlmx).lt.10.) then
        NacetlInt(nCmdApplySinLim)=1
      else
        NacetlInt(nCmdApplySinLim)=0
      endif
      call RefRewriteCommands(1)
      call ExtractDirectory(Fln,t256)
      call ExtractFileName(Fln,NewFln)
      i=FeChdir(t256)
      call FeGetCurrentDir
      Fln=NewFln
      IFln=idel(Fln)
      call FeBottomInfo(' ')
      go to 9999
9000  call FeReadError(ln)
9900  ErrFlag=1
9999  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      call CloseIfOpened(ln)
      KPhaseIn=KPhase
      return
100   format(i1)
101   format(6f10.6)
102   format(5i5,l5,3i5,3f8.3,f10.5)
103   format(i10)
      end
