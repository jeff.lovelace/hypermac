      real function LatticeVolume(Cell)
      include 'fepc.cmn'
      dimension Cell(*)
      csa=cos(Cell(6)*ToRad)
      csb=cos(Cell(5)*ToRad)
      csg=cos(Cell(4)*ToRad)
      pom=1.-csa**2-csb**2-csg**2+2.*csa*csb*csg
      if(pom.gt.0.) then
        LatticeVolume=Cell(1)*Cell(2)*Cell(3)*sqrt(pom)
      else
        LatticeVolume=-1.
      endif
      return
      end
