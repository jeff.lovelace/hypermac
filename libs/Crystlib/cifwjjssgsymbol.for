      subroutine CIFWJJSSGSymbol(Symbol,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension qsymm(3)
      character*(*) Symbol
      if(index(Symbol,'?').gt.0) then
        ich=-1
        go to 9999
      endif
      call zhusti(Symbol)
      Grupa(KPhase)=Symbol
      if(NPhase.le.1) then
        n=1
        call AllocateSymm(n+1,n+1,1,NPhase)
      endif
      call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,QSymm,ich)
      if(ich.eq.0) call EM50MakeStandardOrder
9999  return
      end
