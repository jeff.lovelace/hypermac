      subroutine iom50old(klic,tisk)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*256 t256
      character*80 radka,t80
      character*12 nazev
      character*8 Tasks(4)
      character*2 nty
      dimension PomMat(36),npom(28),FFPom(56)
      integer tisk,State,ColorOrder,FeRGBCompress
      logical poprve,EqIgCase
      equivalence (npom,xpom)
      data tasks/'refine','fourier','dist','contour'/
      ln=0
      if(klic.eq.0) then
        NPhase=5
        NDim(1)=6
        KPhase=1
        call AllocateSymm(96,96,3,MxPhases)
        if(allocated(AtType))
     1    deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2               AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3               FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4               TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
        if(allocated(TypeCore))
     1    deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2               HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3               CoreValSource)
        n=20
        m=121
        allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1           AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2           AtWeight(n,NPhase),AtRadius(n,NPhase),
     3           AtColor(n,NPhase),AtNum(n,NPhase),
     4           AtVal(n,NPhase),AtMult(n,NPhase),
     5           FFBasic(m,n,NPhase),
     6           FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     7           FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     8           FFra(n,NPhase,1),FFia(n,NPhase,1),
     9           FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     a           TypeFFMag(n,NPhase),FFa(4,m,n,NPhase),
     1           FFae(4,m,n,NPhase),fx(n),fm(n),
     2           FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
        allocate(TypeCore(n,MxPhases),TypeVal(n,MxPhases),
     1           PopCore(28,n,MxPhases),PopVal(28,n,MxPhases),
     2           ZSlater(8,n,MxPhases),NSlater(8,n,MxPhases),
     3           HNSlater(n,MxPhases),HZSlater(n,MxPhases),
     4           ZSTOA(7,7,40,n,MxPhases),CSTOA(7,7,40,n,MxPhases),
     5           NSTOA(7,7,40,n,MxPhases),NCoefSTOA(7,7,n,MxPhases),
     6           CoreValSource(m,MxPhases))
        call SetRealArrayTo(TypicalDist(1,1,1),NPhase*n**2,-1.)
        call SetBasicM50
        if(ExistM50) then
          call OpenFile(m50,fln(:ifln)//'.m50','formatted','old')
          if(ErrFlag.ne.0) go to 9999
        else
          go to 9999
        endif
        do 4100i=1,33
          poprve=.true.
          KPhase=0
          rewind m50
          k=80
1000      if(k.eq.80) then
1005        read(m50,FormA,err=9000,end=9010) Radka
            if(Radka(1:1).eq.'*'.or.Radka(1:1).eq.'#'.or.
     1         idel(Radka).le.0.or.Radka(1:2).eq.'--') go to 1005
            if(EqIgCase(radka,'end')) go to 4100
            if(radka(1:3).eq.'ato'.and.i.ne.IdM50Atom) then
              k=80
              go to 1000
            endif
            call vykric(radka)
            k=0
          endif
          call kus(radka,k,nazev)
          j=islovo(nazev,IdM50,70)
          if(j.lt.0) then
            call FeChybne(-1.,-1.,'keyword on M50 file : '//nazev//
     1                    ' isn''t unique.',' ',SeriousError)
            ErrFlag=1
            go to 9999
          else if(j.eq.IdM50Phase) then
            KPhase=KPhase+1
            poprve=.true.
            if(i.eq.IdM50Cell) then
              call kus(radka,k,t80)
              if(t80.eq.' ') write(t80,'(''Phase#'',i1)') KPhase
              PhaseName(KPhase)=t80
              NPhase=KPhase
              if(KPhase.gt.1) StatusM50=StatusM50+10000
            else if(i.eq.IdM50NDim) then
            else if(i.eq.IdM50Atom) then
              if(KPhase.gt.1) StatusM50=StatusM50+10
            else if(i.eq.IdM50Symmetry) then
              if(KPhase.gt.1) StatusM50=StatusM50+1000
            endif
            go to 1000
          else if(j.ge.IdM50AtWeight) then
            go to 1000
          else if(j.ne.i) then
            if(j.eq.IdM50Lattice) then
              call kus(radka,k,cislo)
            else if(j.eq.IdNumbers(1)) then
              k=80
            endif
            go to 1000
          else
            KPh=max(KPhase,1)
            KPhase=KPh
            if(i.eq.IdM50Title) then
              if(.not.poprve) go to 4000
1100          if(Radka(k:k).eq.' '.and.k.lt.80) then
                k=k+1
                go to 1100
              endif
              StructureName=Radka(k:)
              k=80
            else if(i.eq.IdM50Cell) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,CellPar(1,1,KPh),6,.false.,ich)
              if(ich.ne.0) go to 9000
              if(CellPar(1,1,KPh).gt..1) StatusM50=StatusM50-10000
            else if(i.eq.IdM50EsdCell) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,CellParSU(1,1,KPh),6,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM50NDim) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) NDim(KPh)
              NDimI(KPh)=NDim(KPh)-3
              NDimQ(KPh)=NDim(KPh)**2
              MaxNDim=max(MaxNDim,NDim(KPh))
              MaxNDimI=MaxNDim-3
            else if(i.eq.IdM50Ncomp) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) NComp(KPh)
              MaxNComp=max(MaxNComp,NComp(KPh))
            else if(i.eq.IdM50Qi.or.i.eq.IdM50Qr) then
              if(poprve) idim=0
              idim=idim+1
              if(idim.gt.NDimI(KPh)) go to 4000
              if(i.eq.IdM50Qi) then
                call StToReal(Radka,k,qui (1,idim,KPh),3,.false.,ich)
                if(ich.ne.0) go to 9000
                call CopyVek(qui(1,idim,KPh),qu(1,idim,1,KPh),3)
              else
                call StToReal(Radka,k,quir(1,idim,KPh),3,.false.,ich)
                if(ich.ne.0) go to 9000
                call AddVek(qui(1,idim,KPh),quir(1,idim,KPh),
     1                      qu(1,idim,1,KPh),3)
              endif
            else if(i.eq.IdM50WMatrix) then
              if(poprve) icomp=1
              icomp=icomp+1
              if(icomp.gt.NComp(KPh)) go to 4000
              do j=1,NDimQ(KPh),NDim(KPh)
                read(m50,FormA,err=9000,end=9010) radka
                k=0
                call StToReal(Radka,k,PomMat(j),NDim(KPh),.false.,ich)
                if(ich.ne.0) go to 9000
              enddo
              call trmat(PomMat,zv(1,icomp,KPh),NDim(KPh),NDim(KPh))
              k=80
            else if(i.eq.IdM50Commen) then
              if(.not.poprve) go to 4000
              KCommen(KPh)=1
              ICommen(KPh)=0
              call StToInt(Radka,k,NCommen(1,1,KPh),3,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM50Tzero) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,trez(1,1,KPh),NDimI(KPh),.false.,
     1                      ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM50Lambda) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,1)
              read(cislo,101,err=9000) LamAve(1)
              if(LamAve(1).ge.0) then
                Radiation(1)=XRayRadiation
              else
                LamAve(1)=abs(LamAve(1))
                Radiation(1)=NeutronRadiation
              endif
            else if(i.eq.IdM50RadType) then
              if(.not.poprve) go to 4000
              call StToInt(Radka,k,Radiation(1),1,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(j.eq.IdM50LpFactor) then
              if(.not.poprve) go to 4000
              call StToInt(Radka,k,LpFactor(1),1,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(j.eq.IdM50NAlpha) then
              if(.not.poprve) go to 4000
              call StToInt(Radka,k,NAlfa(1),1,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(j.eq.IdM50MonAngle) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,AngleMon(1),1,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(j.eq.IdM50Kalpha1) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,LamA1(1),1,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(j.eq.IdM50Kalpha2) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,LamA2(1),1,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(j.eq.IdM50Kalphar) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,LamRat(1),1,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM50Centro) then
              if(.not.poprve) go to 4000
              NCSymm(KPh)=2
            else if(i.eq.IdM50Symmetry) then
              if(poprve) NSymm(KPh)=0
              NSymm(KPh)=NSymm(KPh)+1
              ISwSymm(NSymm(KPh),1,KPh)=1
              MaxNSymm=max(MaxNSymm,NSymm(KPh))
              if(NSymm(KPh).eq.1) StatusM50=StatusM50-1000
              call ReadSymm(radka(k:),rm6(1,NSymm(KPh),1,KPh),
     1          s6(1,NSymm(KPh),1,KPh),symmc(1,NSymm(KPh),1,KPh),
     2          pom,tisk)
              if(ErrFlag.ne.0) go to 9999
              k=80
            else if(i.eq.IdM50Lattice) then
              if(.not.poprve) go to 4000
              call kus(radka,k,lattice(KPh))
              call velka(Lattice(KPh))
            else if(i.eq.IdM50LattVec) then
              if(poprve) NLattVec(KPh)=0
              NLattVec(KPh)=NLattVec(KPh)+1
              call StToReal(Radka,k,Vt6(1,NLattVec(KPh),1,KPh),
     1                      NDim(KPh),.false.,ich)
              if(ich.ne.0) go to 9000
              MaxNLattVec=max(MaxNLattVec,NLattVec(KPh))
              k=80
            else if(i.eq.IdM50SpGroup) then
              if(.not.poprve) go to 4000
              call kus(radka,k,grupa(KPh))
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) ngrupa(KPh)
              call uprat(grupa(KPh))
              if(k.lt.80) then
                call kus(radka,k,cislo)
                call posun(cislo,0)
                read(cislo,FormI15,err=9000) CrSystem(KPh)
                Monoclinic(KPh)=CrSystem(KPh)/10
              endif
            else if(i.eq.IdM50UnitsNumb) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) NUnits(KPh)
              NUnits(KPh)=max(NUnits(KPh),1)
            else if(i.eq.IdM50SgShift) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,ShSg(1,KPh),NDim(KPh),.false.,
     1                      ich)
              if(ich.ne.0) go to 9000
              pom=0.
              do j=1,NDim(KPh)
                pom=pom+abs(ShSg(j,KPh))
              enddo
              if(Pom.gt..001) then
                StdSg(KPh)=0
              else
                StdSg(KPh)=1
              endif
            else if(i.eq.IdM50NoOfRef) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) NRef90(KDatBlock)
            else if(i.eq.IdM50ChemFormula) then
              if(.not.poprve) go to 4000
c              Formula(KPh)=Radka(k+1:)
            else if(i.eq.IdM50Atom) then
              if(poprve) NAtFormula(KPh)=0
              NAtFormula(KPh)=NAtFormula(KPh)+1
              if(NAtFormula(KPh).eq.1) StatusM50=StatusM50-10
              ffra(NAtFormula(KPh),KPh,1)=0.
              ffia(NAtFormula(KPh),KPh,1)=0.
              AtMult(NAtFormula(KPh),KPh)=1.
              HNSlater(NAtFormula(KPh),KPh)=0
              HZSlater(NAtFormula(KPh),KPh)=0.
              call kus(radka,k,AtTypeFull(NAtFormula(KPh),KPh))
              AtTypeMag(NAtFormula(KPh),KPh)=' '
              AtTypeMagJ(NAtFormula(KPh),KPh)=' '
              call uprat(AtTypeFull(NAtFormula(KPh),KPh))
              call RealFromAtomFile(AtTypeFull(NAtFormula(KPh),KPh),
     1          'atradius',AtRadius(NAtFormula(KPh),KPh),0,ich)
              call IntAFromAtomFile(AtTypeFull(NAtFormula(KPh),KPhase),
     1                              'NSlater',npom,3,ich)
              AtColor(NAtFormula(KPh),KPh)=
     1          FeRGBCompress(npom(1),npom(2),npom(3))
1510          if(k.eq.80) then
                read(m50,FormA,err=9000,end=9010) Radka
                if(radka(1:1).eq.'*'.or.radka(1:1).eq.'*') go to 1510
                k=0
                kp=0
                call vykric(radka)
              endif
              if(EqIgCase(Radka,'end')) go to 1600
              kp=k
              call kus(radka,k,nazev)
              j=islovo(nazev,IdM50,70)
              if(j.eq.IdM50FormTab) j=IdM50FormTabAt
              if(j.eq.0) then
                go to 1510
              else if(j.lt.IdM50AtRadius.or.j.gt.IdM50FormTabAt) then
                go to 1600
              endif
              if(j.eq.IdM50FormTabAt.or.j.eq.IdM50FFFree.or.
     1           j.eq.IdM50FFCore.or.j.eq.IdM50FFVal) then
                call StToInt(Radka,k,npom,1,.false.,ich)
                FFType(KPh)=npom(1)
                if(ich.eq.1) go to 9000
                if(j.eq.IdM50FormTabAt.or.j.eq.IdM50FFFree) then
                  call ReadFFFromM50(FFBasic(1,NAtFormula(KPh),KPh),
     1                               FFType(KPh))
                else if(j.eq.IdM50FFCore) then
                  call StToInt(Radka,k,PopCore(1,NAtFormula(KPh),KPh),
     1                         28,.false.,ich)
                  if(ich.eq.1) go to 9000
                  call ReadFFFromM50(FFCore(1,NAtFormula(KPh),KPh),
     1                               FFType(KPh))
                  TypeCore(NAtFormula(KPh),KPh)=0
                else
                  call StToInt(Radka,k,PopVal(1,NAtFormula(KPh),KPh),1,
     1                         .false.,ich)
                  if(ich.eq.1) go to 9000
                  call SetIntArrayTo(PopVal(2,NAtFormula(KPh),KPh),27,0)
                  if(PopVal(1,NAtFormula(KPh),KPh).ge.0) then
                    call StToInt(Radka,k,PopVal(2,NAtFormula(KPh),KPh),
     1                           27,.false.,ich)
                    if(ich.eq.1) go to 9000
                    TypeVal(NAtFormula(KPh),KPh)=0
                  else if(PopVal(1,NAtFormula(KPh),KPh).eq.-1) then
                    TypeVal(NAtFormula(KPh),KPh)=-1
                  else if(PopVal(1,NAtFormula(KPh),KPh).eq.-2) then
                    call StToInt(Radka,k,HNSlater(NAtFormula(KPh),KPh),
     1                           1,.false.,ich)
                    if(ich.eq.1) go to 9000
                    call StToReal(Radka,k,
     1                HZSlater(NAtFormula(KPh),KPh),1,.false.,ich)
                    if(ich.eq.1) go to 9000
                    TypeVal(NAtFormula(KPh),KPh)=-2
                  endif
                  call ReadFFFromM50(FFVal(1,NAtFormula(KPh),KPh),
     1                               FFType(KPh))
                endif
                if(FFType(KPh).eq.-56) then
                   FFType(KPh)=-62
                   call SetRealArrayTo(FFBasic(57,NAtFormula(KPh),KPh),
     1                                 6,0.)
                endif
                if(ErrFlag.eq.1) then
                  go to 9000
                else if(ErrFlag.eq.2) then
                  go to 9010
                endif
                k=80
              else if(j.eq.IdM50NSlater) then
                call StToInt(Radka,k,NSlater(1,NAtFormula(KPh),KPh),8,
     1                       .false.,ich)
                if(ich.eq.1) go to 9000
                k=80
              else if(j.eq.IDM50ZSlater.or.j.eq.IDM50DzSlater) then
                call StToReal(Radka,k,ZSlater(1,NAtFormula(KPh),KPh),8,
     1                        .false.,ich)
                if(ich.eq.1) go to 9000
                k=80
              else
                if(j.ne.IdM50Color) then
                  call StToReal(Radka,k,FFPom,1,.false.,ich)
                  pom=FFPom(1)
                  if(ich.ne.0) go to 9000
                endif
                if(j.eq.IdM50AtWeight) then
                  AtWeight(NAtFormula(KPh),KPh)=pom
                else if(j.eq.IdM50Dmax.or.j.eq.IdM50AtRadius) then
                  AtRadius(NAtFormula(KPh),KPh)=pom
                else if(j.eq.IdM50Color) then
                  call kus(Radka,k,Cislo)
                  j=ColorOrder(Cislo)
                  if(j.gt.0) then
                    AtColor(NAtFormula(KPh),KPh)=ColorNumbers(j)
                  else
                    AtColor(NAtFormula(KPh),KPh)=White
                  endif
                else if(j.eq.IdM50Formula) then
                  AtMult(NAtFormula(KPh),KPh)=pom
                else if(j.eq.IdM50FPrime) then
                  ffra(NAtFormula(KPh),KPh,1)=pom
                else if(j.eq.IdM50FDoublePrime) then
                  ffia(NAtFormula(KPh),KPh,1)=pom
                else if(j.eq.IdM50FNeutron) then
                  ffn(NAtFormula(KPh),KPh)=pom
                else if(j.eq.IdM50FNeutronIm) then
                  ffni(NAtFormula(KPh),KPh)=pom
                endif
              endif
              go to 1510
1600          call GetPureAtType(AtTypeFull(NAtFormula(KPh),KPh),
     1                           AtType(NAtFormula(KPh),KPh))
              if(Radiation(1).eq.XRayRadiation) then
                pom=0.
                do j=1,98
                  if(AtType(NAtFormula(KPhase),KPhase).eq.atn(j)) then
                    pom=float(j)
                    go to 1715
                  endif
               enddo
1715           AtNum(NAtFormula(KPh),KPh)=pom
                if(AtType(NAtFormula(KPh),KPh).eq.'D') then
                  AtNum(NAtFormula(KPh),KPh)=1.
                else
                  AtNum(NAtFormula(KPh),KPh)=0.
                  do j=1,98
                    if(AtType(NAtFormula(KPh),KPh).eq.atn(j)) then
                      AtNum(NAtFormula(KPh),KPh)=float(j)
                      go to 1730
                    endif
                  enddo
                endif
              endif
1730          if(EqIgCase(Radka,'end')) then
                go to 4100
              else
                k=kp
                go to 3000
              endif
            else if(i.eq.IdM50SLimits) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,sinmez,8,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM50FLimits) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,fmez,8,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM50Twin) then
              if(.not.poprve) go to 4000
              call StToInt(Radka,k,npom,1,.false.,ich)
              if(ich.ne.0) go to 9000
              NTwin=npom(1)
              do n=2,NTwin
                do j=1,3
                  read(m50,*,err=9000,end=9010)(rtw(j+(k-1)*3,n),
     1                                          k=1,3)
                enddo
                call matinv(rtw(1,n),rtwi(1,n),pom,3)
                if(pom.eq.0.) then
                  write(t80,'(i1,a2,1x)') n,nty(n)
                  call FeChybne(-1.,-1.,t80(1:4)//'twinning matrix is'
     1                        //' singular.',' ',SeriousError)
                  ErrFlag=1
                  go to 9999
                endif
              enddo
              k=80
            else if(i.eq.IdM50PhaseTwin) then
              if(.not.poprve) go to 4000
              call StToInt(Radka,k,KPhaseTwin,NTwin,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM50RefOfAnom) then
              if(.not.poprve) go to 4000
              KAnRef(1)=1
            else if(i.eq.IdM50Powder) then
              if(.not.poprve) go to 4000
              IsPowder=.true.
            else if(i.eq.IdM50DatCollTemp) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,1)
              read(cislo,101,err=9000) DatCollTemp(1)
            else if(i.eq.IdM50Densities) then
              if(.not.poprve) go to 4000
              ChargeDensities=.true.
            else if(i.eq.IdM50WaveFile) then
              call Kus(Radka,k,NameOfWaveFile)
              if(EqIgCase(NameOfWaveFile,'wavef.dat'))
     1          NameOfWaveFile='wavefn.dat'
            endif
          endif
3000      poprve=.false.
          go to 1000
4000      call FeChybne(-1.,-1.,'duplicate occurrence of "'//
     1                  nazev(1:idel(nazev))//'"',
     2                  'remember that the first occurrence will be '//
     3                  'accepted.',Warning)
4100    continue
        NPhase=max(NPhase,1)
        itwph=max(NPhase,NTwin)
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.l51','formatted','unknown')
        write(ln,FormA1)('*',i=1,79)
        j=0
        State=0
4150    read(m50,FormA,end=4160,err=4160) Radka
        if(j.eq.0.and.Radka(1:4).eq.'****') go to 4150
        k=0
        call kus(Radka,k,Cislo)
        do i=1,4
          if(EqIgCase(Cislo,Tasks(i))) State=State+10**(i-1)
        enddo
        write(ln,FormA) Radka(:idel(Radka))
        j=1
        go to 4150
4160    do i=4,1,-1
          if(State.eq.1111) go to 4166
          j=10**(i-1)
          if(State/j.eq.0) then
            if(NPhase.gt.1.and.i.ne.1) then
              write(ln,FormA) Tasks(i)(:idel(Tasks(i)))//' '//
     1           PhaseName(1)(1:idel(PhaseName(1)))
            else
              write(ln,FormA) Tasks(i)(:idel(Tasks(i)))
            endif
            write(ln,FormA)
            write(ln,'(''end'')')
          else
            State=State-j
          endif
        enddo
4166    call CloseIfOpened(ln)
        if(StatusM50.lt.10000) then
          do KPh=1,NPhase
            kkk=NCSymm(KPh)
            KPhase=KPh
            call SetMet(tisk)
            if(ErrFlag.ne.0) go to 9999
            if(StatusM50.lt.1000) then
              if(idel(Lattice(KPh)).gt.1) then
                Lattice(KPh)='X'
                call FindSmbSg(Grupa(KPh),ChangeOrderYes,1)
              endif
              if(lattice(KPh).ne.'X') then
                call CtiLatt(tisk)
                if(ErrFlag.ne.0) go to 9999
              endif
              call CorrectForStandardSetting
              call FinSym(tisk)
              call SetScode
              MaxNLattVec=max(MaxNLattVec,NLattVec(KPh))
            endif
            if(Formula(KPh).ne.' ') then
              t80=Formula(KPh)
              call PitFor(0,ich)
              call SetFormula(Formula(KPhase))
            else
              call SetFormula(Formula(KPhase))
            endif
            FFType(KPh)=min(FFType(KPh),121)
            if(CrSystem(KPh).le.0) then
              call CheckSystem(CellPar(1,1,i),Monoclinic(KPh),
     1                         CrSystem(KPh))
              if(CrSystem(KPh).eq.2)
     1          CrSystem(KPh)=CrSystem(KPh)+Monoclinic(KPh)*10
            endif
            if((Radiation(1).eq.XRayRadiation.and.
     1          (FFType(KPh).eq.-56.or.FFType(KPh).eq.-62)))
     2         call SetIntFormFCoef
            NCSymm(KPh)=kkk
          enddo
        endif
        do KPh=1,NPhase
          if(NDim(KPh).le.3) then
            KCommen(KPh)=0
            call SetRealArrayTo(trez(1,1,KPh),NDimI(KPh),0.)
          endif
          do i=1,NAtFormula(KPh)
            if(ChargeDensities) then
              call SetCoreVal(i)
              if(ErrFlag.ne.0) go to 9999
            endif
          enddo
          KCommenMax=max(KCommenMax,KCommen(KPh))
          if(tisk.eq.1) call inform50
        enddo
        UseEFormat91=.false.
        Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
        write(format80(2:2),100) maxNDim
        format83 (2:2)=format80(2:2)
        format83e(2:2)=format80(2:2)
        format83p(2:2)=format80(2:2)
        format91 (2:2)=format80(2:2)
        format95 (5:5)=format80(2:2)
        write(PrfFormat(2:2),100) maxNDim
        if(isPowder) NTwin=1
        cs2m=cos(2.*AngleMon(1)*torad)
        cs2mq=cs2m**2
      else if(Klic.eq.1) then
        call OpenFile(m50,fln(:ifln)//'.m50','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        t80=IdM50(1)(:idel(IdM50(IdM50Title)))//' '//StructureName
        write(m50,FormA) t80(:idel(t80))
        write(radka,'(f11.5,2(1x,a12,1x,i1))')
     1               LamAve(1),IdM50(IdM50RadType),Radiation(KDatBlock),
     2                         IdM50(IdM50LpFactor),LpFactor(KDatBlock)
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          k=idel(Radka)+2
          write(radka(k:),'(a12,f10.3)') IdM50(IdM50MonAngle),
     1                                   AngleMon(KDatBlock)
        else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        endif
        k=0
        call WriteLabeledRecord(50,IdM50(IdM50Lambda),Radka,k)
        if(NAlfa(KDatBlock).gt.1) then
          write(Radka,'(i1,3(1x,a12,1x,f10.6))') NAlfa(KDatBlock),
     1     IdM50(IdM50KAlpha1),LamA1(KDatBlock),
     2     IdM50(IdM50KAlpha2),LamA2(KDatBlock),
     3     IdM50(IdM50KAlphar),LamRat(KDatBlock)
          k=0
          call WriteLabeledRecord(50,IdM50(IdM50NAlpha),Radka,k)
          write(radka,104) DatCollTemp(KDatBlock)
          k=0
          call WriteLabeledRecord(50,IdM50(IdM50DatCollTemp),Radka,k)
        endif
        do KPh=1,NPhase
          if(NPhase.gt.1) then
            write(m50,FormA1)('-',i=1,60)
            t80=IdM50(IdM50Phase)(:idel(IdM50(IdM50Phase)))//' '//
     1          PhaseName(KPh)(:idel(PhaseName(KPh)))
            write(m50,FormA) t80(:idel(t80))
          endif
          if(CellPar(1,1,KPh).gt..1) then
            if(IsPowder) then
              write(radka,107)(CellPar(i,1,KPh),i=1,6)
            else
              write(radka,104)(CellPar(i,1,KPh),i=1,6)
            endif
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50Cell),Radka,k)
            if(IsPowder) then
              write(radka,107)(CellParSU(i,1,KPh),i=1,6)
            else
              write(radka,104)(CellParSU(i,1,KPh),i=1,6)
            endif
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50EsdCell),Radka,k)
          endif
          if(NDim(KPh).gt.3) then
            write(radka,108) NDim(KPh),IdM50(IdM50Ncomp),NComp(KPh)
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50NDim),Radka,k)
            do i=1,NDimI(KPh)
              do j=1,3
                if(abs(qu(j,i,1,KPh)).lt..0001.and.
     1             abs(quir(j,i,KPh)).gt..0001) quir(j,i,KPh)=0.
              enddo
              write(radka,107)(qu(j,i,1,KPh)-quir(j,i,KPh),j=1,3)
              k=0
              call WriteLabeledRecord(50,IdM50(IdM50Qi),Radka,k)
              write(radka,107)(quir(j,i,KPh),j=1,3)
              k=0
              call WriteLabeledRecord(50,IdM50(IdM50Qr),Radka,k)
            enddo
            do i=2,NComp(KPh)
              write(m50,FormA)
     1           IdM50(IdM50WMatrix)(:idel(IdM50(IdM50WMatrix)))
              do k=1,NDim(KPh)
                write(m50,105)(nint(zv(k+(j-1)*NDim(KPh),i,KPh)),
     1                         j=1,NDim(KPh))
              enddo
            enddo
          endif
          if(NSymm(KPh).gt.0) then
            radka=Grupa(KPh)
            if(Radka.eq.' ') Radka='???'
            write(radka(idel(radka)+1:),'(2i4)')
     1        ngrupa(KPh),CrSystem(KPh)
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50SpGroup),Radka,k)
            pom=0.
            do j=1,NDim(KPh)
              pom=pom+abs(ShSg(j,KPh))
            enddo
            if(pom.gt..001) then
              StdSg(KPh)=0
            else
              StdSg(KPh)=1
            endif
            if(StdSg(KPh).eq.0) then
              write(radka,107)(shsg(i,KPh),i=1,NDim(KPh))
              k=0
              call WriteLabeledRecord(50,IdM50(IdM50SgShift),Radka,k)
            endif
            if(NCSymm(KPh).eq.2) write(m50,FormA)
     1                   IdM50(IdM50Centro)(:idel(IdM50(IdM50Centro)))
            Radka=Lattice(KPh)
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50Lattice),Radka,k)
            if(lattice(KPh)(1:1).eq.'X') then
              do i=1,NLattVec(KPh)
                write(radka,107)(vt6(j,i,1,KPh),j=1,NDim(KPh))
                k=0
                call WriteLabeledRecord(50,IdM50(IdM50LattVec),Radka,k)
              enddo
            endif
            do i=1,NSymm(KPh)
              radka=' '
              do j=1,NDim(KPh)
                if(i.eq.1) then
                  if(NDim(KPh).le.3) then
                    Cislo=smbx(j)
                  else
                    Cislo=smbx6(j)
                  endif
                  if(.not.EqIgCase(symmc(j,i,1,KPh),Cislo)) go to 5420
                endif
                radka(idel(Radka)+2:)=symmc(j,i,1,KPh)
              enddo
              k=0
              call WriteLabeledRecord(50,IdM50(IdM50Symmetry),Radka,k)
            enddo
            go to 5450
5420        call FeChybne(-1.,-1.,'The first space group operator is '//
     1                    'not identity.','Please contact authors.',
     2                    SeriousError)
          endif
5450      write(radka,'(i5)') NUnits(KPh)
          k=0
          call WriteLabeledRecord(50,IdM50(IdM50Unitsnumb),Radka,k)
          if(Formula(KPh).ne.' ') then
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50ChemFormula),
     1                              Formula(KPh),k)
          endif
          if(KAnRef(1).ne.0) write(m50,FormA)
     1       IdM50(IdM50RefOfAnom)(:idel(IdM50(IdM50RefOfAnom)))
          do i=1,NAtFormula(KPh)
            call uprat(AtTypeFull(i,KPh))
            write(m50,'(''atom '',7a1)')
     1        (AtTypeFull(i,KPh)(j:j),j=1,idel(AtTypeFull(i,KPh)))
            write(t256,103) AtWeight(i,KPh),IdM50(IdM50AtRadius),
     1                      AtRadius(i,KPh)
            write(t256(idel(t256)+2:),'(a12,1x,a15)')
     1        IdM50(IdM50Color),'White'
            if(Radiation(1).eq.NeutronRadiation) then
              write(t256(idel(t256)+2:),'(2(a12,1x,f10.5,1x))')
     1          IdM50(IdM50Fneutron),ffn(i,KPh),
     2          IdM50(IdM50FneutronIm),ffni(i,KPh)
            endif
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50AtWeight),t256,k)
            if(Radiation(1).ne.NeutronRadiation) then
              write(t256,103) ffra(i,KPh,1),IdM50(IdM50FDoublePrime),
     1                        ffia(i,KPh,1)
              k=0
              call WriteLabeledRecord(50,IdM50(IdM50FPrime),t256,k)
              if(ChargeDensities) then
                j1=IdM50FFFree
                j2=IdM50FFVal
              else
                j1=IdM50FormTabAt
                j2=IdM50FormTabAt
              endif
              m=0
              do j=j1,j2
                write(radka,105) FFType(KPh)
                if(j.eq.IdM50FFCore) then
                  call CopyVekI(PopCore(1,i,KPh),npom,28)
                else if(j.eq.IdM50FFVal) then
                  call CopyVekI(PopVal(1,i,KPh),npom,28)
                else
                  call SetIntArrayTo(npom,28,0)
                endif
                n=28
5550            if(npom(n).eq.0) then
                  n=n-1
                  if(n.gt.0) go to 5550
                endif
                if(n.gt.0) write(radka(6:),109)(npom(k),k=1,n)
                if(npom(1).eq.-2) then
                  n=idel(Radka)+2
                  write(Radka(n:),'(i5,f10.4)') HNSlater(i,KPh),
     1                                          HZSlater(i,KPh)
                endif
                k=0
                call WriteLabeledRecord(50,IdM50(j),Radka,k)
                m=m+1
                k2=iabs(FFType(KPh))
                if(m.eq.1) then
                  write(m50,106)(FFBasic(k,i,KPh),k=1,k2)
                else if(m.eq.2) then
                  write(m50,106)(FFCore(k,i,KPh),k=1,k2)
                else
                  write(m50,106)(FFVal(k,i,KPh),k=1,k2)
                endif
              enddo
              if(ChargeDensities) then
                write(radka,105)(NSlater(j,i,KPh),j=1,8)
                k=0
                call WriteLabeledRecord(50,IdM50(IdM50NSlater),Radka,k)
                write(radka,106)(ZSlater(j,i,KPh),j=1,8)
                k=0
                call WriteLabeledRecord(50,IdM50(IdM50ZSlater),Radka,k)
              endif
            endif
          enddo
          if(KCommen(KPh).ne.0.and.NDim(KPh).gt.3) then
            write(radka,105)(NCommen(i,1,KPh),i=1,3)
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50Commen),Radka,k)
            write(radka,107)(trez(i,1,KPh),i=1,NDimI(KPh))
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50TZero),Radka,k)
          else
            KCommen(KPh)=0
            call SetRealArrayTo(trez(1,1,KPh),NDimI(KPh),0.)
          endif
        enddo
        if(NPhase.gt.1) write(m50,FormA1)('-',i=1,40)
        if(.not.IsPowder.and.NRef90(KDatBlock).gt.0) then
          write(radka,'(i7)') NRef90(KDatBlock)
          k=0
          call WriteLabeledRecord(50,IdM50(IdM50NoOfRef),Radka,k)
          write(radka,'(8f9.6)') sinmez
          k=0
          call WriteLabeledRecord(50,IdM50(IdM50SLimits),Radka,k)
          write(radka,'(8f10.1)') fmez
          k=0
          call WriteLabeledRecord(50,IdM50(IdM50FLimits),Radka,k)
        endif
        if(NTwin.gt.1) then
          write(radka,105) NTwin
          k=0
          call WriteLabeledRecord(50,IdM50(IdM50Twin),Radka,k)
          do n=2,NTwin
            do i=1,3
              write(m50,104)(rtw(i+(j-1)*3,n),j=1,3)
            enddo
          enddo
          if(NPhase.gt.1) then
            write(radka,105)(KPhaseTwin(i),i=1,NTwin)
            k=0
            call WriteLabeledRecord(50,IdM50(IdM50PhaseTwin),Radka,k)
          endif
        endif
        if(IsPowder) then
          write(m50,FormA) IdM50(IdM50Powder)(:idel(IdM50(IdM50Powder)))
        endif
        if(ChargeDensities) then
          write(m50,FormA)
     1       IdM50(IdM50Densities)(:idel(IdM50(IdM50Densities)))
          k=0
          call WriteLabeledRecord(50,IdM50(IdM50WaveFile),
     1                            NameOfWaveFile,k)
        endif
        write(m50,'(''end'')')
        write(m50,FormA1)('*',i=1,79)
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.l51','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        j=0
5810    read(ln,FormA,end=5850,err=5850) Radka
        if(j.eq.0.and.Radka(1:4).eq.'****') go to 5810
        write(m50,FormA) Radka(:idel(Radka))
        j=1
        go to 5810
5850    call CloseIfOpened(ln)
        do i=1,NPhase
          if(CellPar(1,1,i).gt..1) call setmet(0)
        enddo
        ExistM50=.true.
      endif
      go to 9999
9000  call FeReadError(m50)
      go to 9900
9010  t80='Unexpected end of file.'
      call FeChybne(-1.,-1.,'Wrong data on M50 file',t80,SeriousError)
      go to 9900
9020  call FeReadError(ln)
9900  ErrFlag=1
9999  call CloseIfOpened(m50)
      call CloseIfOpened(ln)
      SwitchedToComm=.false.
      return
100   format(10i1)
101   format(f15.0)
103   format(f8.4,2(1x,a12,1x,f8.4))
104   format(6f11.4)
105   format(18i4)
106   format(8f9.4)
107   format(7f11.6)
108   format(i4,2(1x,a12,i4))
109   format(28i3)
      end
