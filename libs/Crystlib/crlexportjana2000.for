      subroutine CrlExportJana2000
      use Basic_mod
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'refine.cmn'
      dimension ih(6)
      character*256 t256,p256
      character*80 flno
      logical FeYesNo,StructureExists,EqIgCase,PwdCheckTOFInD,Uvnitr
      real Iobs,SigIobs
      t256=' '
1000  call FeFileManager('Define a struture name where to export',t256,
     1                   ' ',1,.true.,ich)
      if(ich.ne.0) go to 9999
      if(StructureExists(t256)) then
        call ExtractFileName(t256,p256)
        if(.not.FeYesNo(-1.,-1.,'The structure "'//p256(:idel(p256))//
     1                  '" already exists, rewrite it?',0)) go to 1000
      endif
      idl=idel(t256)
      isPowder=iabs(DataType(KDatBlock)).eq.2
      call PwdSetTOF(KDatBlock)
      ln=NextLogicNumber()
      if(isPowder) then
        call iom40(0,0,fln(:ifln)//'.m40')
        do i=1,mxsc
          sc(i,1)=sc(i,1)/sqrt(2.)
        enddo
        flno=fln
        fln=t256
        ifln=idl
        call iom40old(1,0)
        fln=flno
        ifln=idel(fln)
        call PwdM92Nacti
        call OpenFile(ln,t256(:idl)//'.m92','formatted','unknown')
        write(ln,100)(XPwd(i),abs(YoPwd(i)),YsPwd(i),i=1,npnts)
        write(ln,100) 999.
        call CloseIfOpened(ln)
      else
        call CopyFile(fln(:ifln)//'.m40',t256(:idl)//'.m40')
        call OpenDatBlockM90(ln,KDatBlock,fln(:ifln)//'.m90')
        if(ErrFlag.ne.0) go to 1200
        lno=NextLogicNumber()
        call OpenFile(lno,t256(:idl)//'.m91','formatted','unknown')
        UseEFormat91=.false.
        Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
1100    read(ln,FormA,end=1150) p256
        k=0
        call kus(p256,k,Cislo)
        if(EqIgCase(Cislo,'data')) go to 1150
        read(p256,Format91,err=1150)
     1    (ih(i),i=1,NDim(KPhase)),Iobs,SigIobs,iq,nxx,itwr,tbar,RefLam,
     2    RefDirCos
        write(lno,format91)
     1    (ih(i),i=1,NDim(KPhase)),Iobs,SigIobs,iq,nxx,itwr,tbar,RefLam,
     2    RefDirCos
        go to 1100
1150    call CloseIfOpened(ln)
        call CloseIfOpened(lno)
      endif
1200  call iom50(0,0,fln(:ifln)//'.m50')
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      allocate(KFixOrigin(NPhase),AtFixOrigin(NPhase),AtFixX4(NPhase),
     1         PromAtFixX4(NPhase),KFixX4(NPhase))
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      allocate(ScSup(3,NPhase),NSuper(3,3,NPhase))
      call RefDefault
      IgnoreW=.true.
      IgnoreE=.true.
      call RefReadCommands
      IgnoreW=.false.
      IgnoreE=.false.
      if(Radiation(1).eq.XRayRadiation) then
        do KPh=1,NPhase
          KPhase=KPh
          do i=1,NAtFormula(KPh)
            if(klam(KDatBlock).gt.0) then
              call RealFromAtomFile(AtTypeFull(i,KPh),'Xray_f''',
     1          ffra(i,KPh,KDatBlock),klam(KDatBlock),ich)
              call RealFromAtomFile(AtTypeFull(i,KPh),'Xray_f"',
     1          ffia(i,KPh,KDatBlock),klam(KDatBlock),ich)
            else
              call EM50ReadAnom(AtTypeFull(i,KPh),ffra(i,KPh,KDatBlock),
     1                          ffia(i,KPh,KDatBlock))
            endif
          enddo
        enddo
      endif
      call CrlChangeL51(fln,t256,1)
      lni=NextLogicNumber()
      call OpenFile(lni,t256(:idel(t256))//'.l51','formatted','unknown')
      lno=NextLogicNumber()
      call OpenFile(lno,t256(:idel(t256))//'.l52','formatted','unknown')
      Uvnitr=.false.
1400  read(lni,FormA,end=1450) p256
      if(Uvnitr) then
        i=LocateSubstring(p256,'end',.false.,.true.)
        if(i.eq.1) then
          write(Cislo,'(f10.5)') blkoef*100.
          call ZdrcniCisla(Cislo,1)
          write(lno,'(''unstab '',a)') Cislo(:idel(Cislo))
          uvnitr=.false.
        else
          i=LocateSubstring(p256,'unstab',.false.,.true.)
          if(i.gt.0) then
            k=i-1
            call kus(p256,k,Cislo)
            call kus(p256,k,Cislo)
            if(k.lt.len(p256)) then
              p256=p256(:i-1)//p256(k+1:)
            else
              p256=p256(:i-1)
            endif
          endif
        endif
      else
        i=LocateSubstring(p256,'refine',.false.,.true.)
        if(i.eq.1) Uvnitr=.true.
      endif
      write(lno,FormA) p256(:idel(p256))
      go to 1400
1450  close(lni)
      close(lno)
      call MoveFile(t256(:idel(t256))//'.l52',t256(:idel(t256))//'.l51')
      flno=fln
      fln=t256
      ifln=idl
      call iom50old(1,0)
      fln=flno
      ifln=idel(fln)
      if(FeYesNo(-1.,-1.,'Do you want to continue with the exported '//
     1                   'structure?',0)) then
        call FeGrQuit
        call DeletePomFiles
        call FeTmpFilesDelete
        t256='c:\jana2000\jana2000.exe '//t256(:idl)//'&'
        call FeSystem(t256)
        stop
      else
        fln=flno
        ifln=idel(fln)
      endif
9999  if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YcPwd,YbPwd,YsPwd,
     1                                YiPwd)
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      return
100   format(f10.3,2f15.1)
      end
