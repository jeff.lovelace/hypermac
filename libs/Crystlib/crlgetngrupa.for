      subroutine CrlGetNGrupa(Grp,NGrp,CrSystemP)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Grp
      character*256 Veta
      character*80  GrPom
      character*8   GrupaI
      integer CrSystemP
      logical EqIgCase
      ln=NextLogicNumber()
      GrPom=Grp
      if(mod(CrSystemP,10).eq.CrSystemMonoclinic) then
        if(EqIgCase(GrPom(1:1),'I')) then
          i=index(GrPom,'(')
          if(i.gt.0) GrPom=GrPom(:i-1)
          i1=index(GrPom,'[')
          i2=index(GrPom,']')
          if(i1.gt.0) GrPom=GrPom(:i1-1)//GrPom(i2+1:)
          if(EqIgCase(GrPom(1:1),'I')) GrPom(1:1)='C'
          i=idel(GrPom)
          if(EqIgCase(GrPom(i:i),'a')) GrPom(i:i)='c'
          if(EqIgCase(GrPom(i:i),'n')) GrPom(i:i)='m'
        else
          go to 9999
        endif
      else
        go to 9999
      endif
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      kkk=0
      read(ln,FormA) Veta
      if(Veta(1:1).ne.'#') rewind ln
      ngrupa(KPhase)=0
2100  read(ln,'(i3,7x,a8)',err=9999,end=2200) igi,GrupaI
      if(EqIgCase(GrPom,GrupaI)) then
        NGrp=igi
        go to 9999
      else
        go to 2100
      endif
2200  continue
9999  call CloseIfOpened(ln)
      return
      end
