      subroutine DeleteXYZFromAtomName(AtName)
      include 'fepc.cmn'
      character*(*) AtName
      i1=index(AtName,'#')
      if(i1.gt.0) then
        i2=index(AtName(i1+1:),'#')+i1
        if(i2.gt.i1) then
          AtName=AtName(1:i1-1)//AtName(i2:)
          i1=idel(AtName)
          if(AtName(i1:i1).eq.'#') AtName(i1:i1)=' '
        else
          if(index(AtName(i1+1:),'x').gt.0.or.
     1       index(AtName(i1+1:),'X').gt.0) AtName=AtName(1:i1-1)
        endif
      endif
      return
      end
