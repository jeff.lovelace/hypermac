      subroutine SetIntFormFCoef
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension am(4,4),ami(4,4),b(4)
      do ia=1,NAtFormula(KPhase)
        do i=1,56
          if(i.eq.1) then
            jp=0
          else if(i.gt.52) then
            jp=52
          else
            jp=i-2
          endif
          if(AtType(ia,KPhase).eq.'H') then
            pom=1./(ffxh(jp+1)-ffxh(jp+2))
          else
            pom=1./(ffx(jp+1)-ffx(jp+2))
          endif
          do j=1,4
            b(j)=FFBasic(jp+j,ia,KPhase)-FFBasic(jp+2,ia,KPhase)
            if(AtType(ia,KPhase).eq.'H') then
              xp=(ffxh(jp+j)-ffxh(jp+2))*pom
            else
              xp=(ffx(jp+j)-ffx(jp+2))*pom
            endif
            xs=1.
            do k=1,4
              am(j,k)=xs
              xs=xs*xp
            enddo
          enddo
          call matinv(am,ami,xp,4)
          call multm(ami,b,ffa(1,i,ia,KPhase),4,4,1)
          ffa(1,i,ia,KPhase)=ffa(1,i,ia,KPhase)+FFBasic(jp+2,ia,KPhase)
        enddo
      enddo
      return
      end
