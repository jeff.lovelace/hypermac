      subroutine CrlSigCountSigStat
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xpp(3),xo(3),xp(2),yp(2),ih(3)
      dimension SigCount(:),SigCountN(:),SigStat(:),Ri(:)
      character*256 Veta
      character*60 format83a
      character*17 :: Labels(4) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Sho%w it in DPlot'/)
      allocatable SigCount,SigCountN,SigStat,Ri
      Tiskne=.false.
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m89','formatted','unknown')
      n=0
1100  read(ln,100,end=1200) ih
      n=n+1
      go to 1100
1200  rewind ln
      allocate(SigCount(n),SigCountN(n),SigStat(n),Ri(n))
      j=0
      PMax=0.
1300  read(ln,100,end=1400) ih,pom,pomc,poms
      j=j+1
      if(j.gt.n) go to 1300
      Ri(j)=pom
      SigCount(j)=pomc
      SigStat(j)=poms
      PMax=max(SigCount(j),SigStat(j),PMax)
      go to 1300
1400  close(ln)
      Suma1=0.
      Suma2=0.
      do i=1,n
        Suma1=Suma1+Ri(i)**4
        Suma2=Suma2+(SigStat(i)**2-SigCount(i)**2)*RI(i)**2
      enddo
c      write(Veta,'(2e15.6)') Suma1,Suma2
c      call FeWinMessage('Sumy:'//Veta,' ')
      pom=Suma2/Suma1
c      write(Veta,'(2f15.8)') pom,sqrt(abs(pom))/2.
c      call FeWinMessage('Vysledek:'//Veta,' ')
      do i=1,n
        SigCountN(i)=sqrt(SigCount(i)**2+pom*Ri(i)**2)
        PMax=max(SigCountN(i),PMax)
      enddo
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,3
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtDPlot=ButtonLastMade
        endif
        if(i.le.4) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.4) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
2100  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        rewind ln
        xomn=0.
        xomx=PMax*1.1
        yomn=0.
        yomx=PMax*1.1
c        write(Cislo,'(f10.3)') PMax
c        call FeWinMessage(Cislo,' ')
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.true.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'sig(count)')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'sig(stat)')
        xpp(3)=0.
        do i=1,n
          xpp(1)=SigCount(i)
          xpp(2)=SigStat(i)
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,White)
          xpp(1)=SigCountN(i)
          xpp(2)=SigStat(i)
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,Green)
        enddo
2300    xpp(1)=0.
        xpp(2)=0.
        call FeXf2X(xpp,xo)
        xp(1)=xo(1)
        yp(1)=xo(2)
        xpp(1)=xomx
        xpp(2)=yomx
        call FeXf2X(xpp,xo)
        xp(2)=xo(1)
        yp(2)=xo(2)
        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
9999  return
100   format(3i4,3f9.1)
      end
