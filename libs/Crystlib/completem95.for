      subroutine CompleteM95(BlockFlow)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 FileOut,Veta1,Veta2
      integer BlockFlow,RefDatCorrespondOld(MxRefBlock)
      logical ExistFile,ExistAtLeastEmptyFile,EqIgCase,UseRefBlock
      NRefB=0
      do i=1,NRefBlock
        if(NRef95(i).gt.0) NRefB=NRefB+1
      enddo
      if(NRefB.le.0) then
        call DeleteFile(fln(:ifln)//'.m95')
        ExistM95=.false.
        NRefBlock=0
        go to 9999
      endif
      RefDatCorrespondOld=RefDatCorrespond
      NDatB=0
      do i=1,NDatBlock
        if(NRef90(i).gt.0) then
          NDatB=NDatB+1
          j=NDatB
        else
          j=0
        endif
        do k=1,NRefBlock
          if(RefDatCorrespond(k).eq.i.or.
     1       (RefDatCorrespondOld(k).eq.0.and.DifCode(k).lt.100))
     2      RefDatCorrespond(k)=j
        enddo
      enddo
      FileOut=fln(:ifln)//'.l95'
      call iom95(1,FileOut)
      lno=NextLogicNumber()
      lni=0
      if(ExistM95) then
        call OpenFile(lno,FileOut,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        lni=NextLogicNumber()
        call OpenFile(lni,fln(:ifln)//'.m95','formatted','old')
        if(ErrFlag.ne.0) go to 9999
1050    read(lni,FormA,end=1200) Veta1
        read(lno,FormA,end=1200) Veta2
        if(.not.EqIgCase(Veta1,Veta2)) go to 1200
        if(.not.EqIgCase(Veta1,'end')) go to 1050
      endif
      do i=1,NRefBlock
        write(Cislo,'(''.l'',i2)') i
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        if(ModifiedRefBlock(i).or.
     1     ExistFile(fln(:ifln)//Cislo(:idel(Cislo)))) go to 1200
      enddo
      go to 9000
1200  call CloseIfOpened(lno)
      call CloseIfOpened(lni)
      call OpenForAppend(lno,FileOut)
      if(ErrFlag.ne.0) go to 9000
      if(lni.eq.0) lni=NextLogicNumber()
      n=0
      do i=1,NRefBlock
        n=n+NLines95(i)
      enddo
      if(BlockFlow.eq.0)
     1  call FeFlowChartOpen(-1.,-1.,max(nint(float(n)*.005),10),n,
     2                       'Completing repository file',' ',' ')
      iz=0
      NRefB=0
      do KRefB=1,NRefBlock
        UseRefBlock=NRef95(KRefB).gt.0
        if(UseRefBlock) then
          NRefB=NRefB+1
          ModifiedRefBlock(NRefB)=ModifiedRefBlock(KRefB)
        endif
        write(Cislo,'(''.l'',i2)') KRefB
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
        if(ExistAtLeastEmptyFile(RefBlockFileName)) then
          call OpenFile(lni,RefBlockFileName,'formatted','unknown')
        else
          call OpenRefBlockM95(lni,KRefB,fln(:ifln)//'.m95')
        endif
        if(ErrFlag.ne.0) cycle
        if(UseRefBlock) write(lno,'(''Data '',a)') DatBlockName(NRefB)
2100    read(lni,FormA,end=2200) Veta1
        k=0
        call kus(Veta1,k,Cislo)
        if(EqIgCase(Cislo,'data')) go to 2200
        if(UseRefBlock) write(lno,'(a)') Veta1(:idel(Veta1))
        if(BlockFlow.eq.0) call FeFlowChartEvent(iz,ie)
        go to 2100
2200    call CloseIfOpened(lni)
        if(ExistFile(RefBlockFileName))
     1    call DeleteFile(RefBlockFileName)
      enddo
      NRefBlock=NRefB
      if(BlockFlow.eq.0) call FeFlowChartRemove
      call CloseIfOpened(lno)
      call MoveFile(fln(:ifln)//'.l95',fln(:ifln)//'.m95')
      ExistM95=.true.
9000  call CloseIfOpened(lno)
      call CloseIfOpened(lni)
      call DeleteFile(FileOut)
9999  return
      end
