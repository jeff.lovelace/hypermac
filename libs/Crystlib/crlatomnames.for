      subroutine CrlAtomNames
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension ic(1)
      character*5555 Veta,Radka
      character*256 t256
      character*8 AtomRenameNewP(:,:),AtOld,AtNew
      integer CoDelame,Refine,Dist,Fourier,Contour
      logical EqIgCase
      allocatable AtomRenameNewP
      equivalence (Refine ,IdNumbers(1)),
     1            (Dist   ,IdNumbers(2)),
     2            (Fourier,IdNumbers(3)),
     3            (Contour,IdNumbers(4))
      entry CrlAtomNamesIni
      NRenameAt=NAtAll
      NRenameItems=1
      if(allocated(AtomRenameOld))
     1  deallocate(AtomRenameOld,AtomRenameNew)
      allocate(AtomRenameOld(NAtAll),AtomRenameNew(NAtAll,1))
      do i=1,NAtAll
        AtomRenameOld(i)=Atom(i)
        AtomRenameNew(i,1)=' '
      enddo
      go to 9999
      entry CrlAtomNamesSave(AtOld,AtNew,n)
      do k=1,NRenameAt
        if(EqIgCase(AtomRenameOld(k),AtOld)) then
          if(iabs(n).gt.0) then
            m=iabs(n)
          else
            m=1
1100        if(AtomRenameNew(k,m).ne.' ') then
              m=m+1
              if(m.le.NRenameItems) go to 1100
            endif
          endif
          if(m.gt.NRenameItems) then
            allocate(AtomRenameNewP(NRenameAt,NRenameItems))
            do i=1,NRenameAt
              do j=1,NRenameItems
                AtomRenameNewP(i,j)=AtomRenameNew(i,j)
              enddo
            enddo
            deallocate(AtomRenameNew)
            NRenameItemsP=NRenameItems
            NRenameItems=m
            allocate(AtomRenameNew(NRenameAt,NRenameItems))
            do i=1,NRenameAt
              do j=1,NRenameItems
                if(j.gt.NRenameItemsP) then
                  AtomRenameNew(i,j)=' '
                else
                  AtomRenameNew(i,j)=AtomRenameNewP(i,j)
                endif
              enddo
            enddo
            deallocate(AtomRenameNewP)
          endif
          AtomRenameNew(k,m)=AtNew
          go to 9999
        endif
      enddo
      if(n.ge.0) go to 9999
      do k=1,NRenameAt
        if(EqIgCase(AtomRenameNew(k,1),AtOld)) then
          AtomRenameNew(k,1)=AtNew
          exit
        endif
      enddo
      go to 9999
      entry CrlAtomNamesApply
      do i=1,NAtAll
        if(ChargeDensities) then
          do j=1,2
            k=0
            Radka=' '
            do while (k<len(LocAtSystSt(j,i)))
              call CrlAtomNamesAddNewString(LocAtSystSt(j,i),k,' ',1,
     1                                      Radka,ich)
              if(ich.lt.0) then
                write(Radka,100)(LocAtSystX(m,j,i),m=1,3)
                exit
              endif
            enddo
            LocAtSystSt(j,i)=Radka
          enddo
        endif
      enddo
      do im=1,NMolec
        if(StRefPoint(im).ne.' ') then
          k=0
          Radka=' '
          do while (k<len(StRefPoint(im)))
            call CrlAtomNamesAddNewString(StRefPoint(im),k,' ',1,
     1                                    Radka,ich)
            if(ich.lt.0) then
              Radka=' '
              exit
            endif
          enddo
          StRefPoint(im)=Radka(2:)
        endif
        do j=1,2
          k=0
          Radka=' '
          do while (k<len(LocPGSystSt(j,im)))
            call CrlAtomNamesAddNewString(LocPGSystSt(j,im),k,' ',1,
     1                                    Radka,ich)
            if(ich.lt.0) then
              write(Radka,100)(LocAtSystX(m,j,im),m=1,3)
              exit
            endif
          enddo
          LocPGSystSt(j,im)=Radka
        enddo
        ji=(im-1)*mxp
        do ip=1,mam(im)
          ji=ji+1
          if(AtTrans(ji).ne.' ') then
            k=0
            Radka=' '
            do while (k<len(AtTrans(ji)))
              call CrlAtomNamesAddNewString(AtTrans(ji),k,' ',1,
     1                                      Radka,ich)
              if(ich.lt.0) then
                Radka=' '
                exit
              endif
            enddo
            AtTrans(ji)=Radka(2:)
          endif
          do i=1,2
            do j=1,2
              if(j.gt.LocMolSystType(ji)) cycle
              k=0
              Radka=' '
              do while (k<len(LocMolSystSt(i,j,ji)))
                call CrlAtomNamesAddNewString(LocMolSystSt(i,j,ji),k,
     1                                        ' ',1,Radka,ich)
                if(ich.lt.0) then
                  write(Radka,100)(LocMolSystX(m,i,j,ji),m=1,3)
                  exit
                endif
              enddo
              LocMolSystSt(i,j,ji)=Radka
            enddo
          enddo
        enddo
      enddo
      lni=0
      lno=0
      lni=NextLogicNumber()
      call OpenFile(lni,Fln(:iFln)//'.l51','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      lno=NextLogicNumber()
      call OpenFile(lno,Fln(:iFln)//'.z51','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1500  read(lni,FormA,end=9900) Veta
      write(lno,FormA) Veta(:idel(Veta))
      k=0
      call kus(Veta,k,t256)
      if(LocateSubstring(t256,'refine',.false.,.true.).eq.1) then
        CoDelame=Refine
      else if(LocateSubstring(t256,'dist',.false.,.true.).eq.1) then
        CoDelame=Dist
      else if(LocateSubstring(t256,'fourier',.false.,.true.).eq.1) then
        CoDelame=Fourier
      else if(LocateSubstring(t256,'contour',.false.,.true.).eq.1) then
        CoDelame=Contour
      else
        go to 1500
      endif
1900  if(CoDelame.eq.Refine) then
        go to 2000
      else if(CoDelame.eq.Dist) then
        go to 3000
      else if(CoDelame.eq.Fourier) then
        go to 4000
      else if(CoDelame.eq.Contour) then
        go to 5000
      endif
2000  Veta=' '
2010  read(lni,FormA,end=2020) t256
      if(t256.eq.' ') go to 2010
      idl=idel(Veta)
      if(idl.gt.0) then
        idp=idel(t256)
        do i=1,idp
          if(t256(i:i).ne.' ') exit
        enddo
        if(t256(i:i).eq.'&') then
          Veta=Veta(:idl)//t256(i+1:idp)
          go to 2010
        else
          backspace lni
        endif
      else
        Veta=t256
        go to 2010
      endif
      go to 2030
2020  if(Veta.eq.' ') go to 9999
2030  k=0
      call kus(Veta,k,t256)
      kp=k
      if(LocateSubstring(t256, 'restric',.false.,.true.).eq.1.or.
     1   LocateSubstring(t256,'!restric',.false.,.true.).eq.1) then
        do i=1,NRenameItems
          k=kp
          Radka=Veta(:k)
          call CrlAtomNamesAddNewString(Veta,k,' ',i,Radka,ich)
          if(ich.lt.0) then
            cycle
          else if(ich.gt.0) then
            go to 9000
          endif
          call kus(Veta,k,t256)
          Radka=Radka(:idel(Radka))//' '//t256(:idel(t256))
          kpp=k
          call kus(Veta,k,t256)
          if(EqIgCase(t256(1:3),'ls#')) then
            Radka=Radka(:idel(Radka))//' '//t256(:idel(t256))
          else
            k=kpp
          endif
          nn=0
          mm=0
          do while (k<len(Veta))
            mm=mm+1
            call CrlAtomNamesAddNewString(Veta,k,' ',i,Radka,ich)
            if(ich.lt.0) then
              nn=nn+1
            else if(ich.gt.0) then
              go to 9000
            endif
          enddo
          if(nn.eq.0.or.mm.ne.nn) then
            call PisPo80(lno,Radka)
            if(nn.eq.0) exit
          endif
        enddo
        go to 1900
      else if(LocateSubstring(t256, 'equation',.false.,.true.).eq.1.or.
     1        LocateSubstring(t256,'!equation',.false.,.true.).eq.1)
     2  then
        call kus(Veta,k,t256)
        if(t256.ne.':') go to 9000
        Radka=Veta(:k)
        do while (k<idel(Veta))
          kp=index(Veta(k:),'[')+k
          kk=index(Veta(k:),']')+k-2
          if(kp-2.lt.k) then
            Radka=Radka(:idel(Radka))//Veta(k:)
            exit
          else
            Radka=Radka(:idel(Radka))//Veta(k:kp-2)
          endif
          j=0
          call CrlAtomNamesAddNewString(Veta(kp:kk),j,'[',1,Radka,ich)
          if(ich.eq.0) then
            Radka=Radka(:idel(Radka))//']'
          else if(ich.lt.0) then
            go to 1900
          else if(ich.gt.0) then
            go to 9000
          endif
          k=kk+2
        enddo
        Veta=Radka
      else if(LocateSubstring(t256, 'keep',.false.,.true.).eq.1.or.
     1        LocateSubstring(t256,'!keep',.false.,.true.).eq.1) then
        call kus(Veta,k,t256)
        i=islovo(t256,CKeepType,3)
        if(i.le.0) then
          go to 9000
        else
          call kus(Veta,k,t256)
        endif
        KeepTypeMain=i
        if(KeepTypeMain.eq.IdKeepHydro) then
          i=islovo(t256,CKeepHydro,3)
        else if(KeepTypeMain.eq.IdKeepGeom) then
          i=islovo(t256,CKeepGeom,2)
        else if(KeepTypeMain.eq.IdKeepADP) then
          i=islovo(t256,CKeepADP,2)
        endif
        if(i.le.0) go to 9000
        KeepTypeP=KeepTypeMain*10+i
        Radka=Veta(:k)
        if(KeepTypeMain.eq.IdKeepHydro) then
          if(KeepTypeP.eq.IdKeepHApical) then
            NAtMax=6
          else if(KeepTypeP.eq.IdKeepHTetraHed) then
            NAtMax=4
          else if(KeepTypeP.eq.IdKeepHTriangl) then
            NAtMax=3
          endif
          if(k.ge.len(Veta)) go to 9000
          call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
          if(ich.lt.0) then
            go to 1900
          else if(ich.gt.0) then
            go to 9000
          endif
          if(k.ge.len(Veta)) go to 9000
          call kus(Veta,k,t256)
          kk=0
          call StToInt(t256,kk,ic,1,.false.,ich)
          if(ich.ne.0) go to 9000
          KeepNNeighP=ic(1)
          Radka=Radka(:idel(Radka))//' '//t256(:idel(t256))
          if(KeepNNeighP.gt.NAtMax) then
            go to 9000
          else if(KeepTypeP.eq.IdKeepHApical.and.KeepNNeighP.lt.1)
     1      then
            go to 9000
          endif
          if(KeepTypeP.ne.IdKeepHApical) then
            call kus(Veta,k,t256)
            kk=0
            call StToInt(t256,kk,ic,1,.false.,ich)
            if(ich.ne.0) go to 9000
            KeepNHP=ic(1)
            Radka=Radka(:idel(Radka))//' '//t256(:idel(t256))
          else
            KeepNHP=1
          endif
          if(KeepNNeighP+KeepNHP.gt.NAtMax+1) go to 9000
          do m=1,KeepNNeighP
            if(m+KeepNHP.eq.NAtMax+1.and.KeepTypeP.ne.IdKeepHApical)
     1        then
              if(k.ge.len(Veta)) go to 9000
              call kus(Veta,k,t256)
              Radka=Radka(:idel(Radka))//' '//t256(:idel(t256))
            endif
            if(k.ge.len(Veta)) go to 9000
            call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
            if(ich.lt.0) then
              go to 1900
            else if(ich.gt.0) then
              go to 9000
            endif
          enddo
          if(k.ge.len(Veta)) go to 9000
          call kus(Veta,k,t256)
          Radka=Radka(:idel(Radka))//' '//t256(:idel(t256))
          do m=1,KeepNHP
            if(k.ge.len(Veta)) go to 9000
            call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
            if(ich.lt.0) then
              go to 1900
            else if(ich.gt.0) then
              go to 9000
            endif
          enddo
        else if(KeepTypeMain.eq.IdKeepGeom) then
2100      call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
          if(ich.lt.0) then
            go to 1900
          else if(ich.gt.0) then
            go to 9000
          endif
          if(k.lt.len(Veta)) go to 2100
        else if(KeepTypeMain.eq.IdKeepADP) then
          if(k.ge.len(Veta)) go to 9000
          call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
          if(ich.lt.0) then
            go to 1900
          else if(ich.gt.0) then
            go to 9000
          endif
          if(k.ge.len(Veta)) go to 9000
          call kus(Veta,k,t256)
          Radka=Radka(:idel(Radka))//' '//t256(:idel(t256))
          nn=0
2200      call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
          if(ich.lt.0) then
            if(nn.le.0) go to 1900
          else if(ich.gt.0) then
            go to 9000
          endif
          nn=nn+1
          if(k.lt.len(Veta)) go to 2200
        endif
        Veta=Radka
      else if(LocateSubstring(t256, 'fixed',.false.,.true.).eq.1.or.
     1        LocateSubstring(t256,'!fixed',.false.,.true.).eq.1) then
        call kus(Veta,k,t256)
        i=islovo(t256,CFix,10)
        if(i.le.0.or.k.ge.len(Veta)) go to 9000
        if(i.gt.3) i=i-1
        Radka=Veta(:k)
        if(i.eq.IdFixAllPar.or.i.eq.IdFixCoord.or.i.eq.IdFixADP.or.
     1     i.eq.IdFixModul.or.i.eq.IdFixOrigin.or.i.eq.IdFixChden) then
          nn=0
          do while (k<len(Veta))
            call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
            if(ich.lt.0) then
              if(nn.le.0) go to 1900
            else if(ich.gt.0) then
              go to 9000
            endif
            nn=nn+1
          enddo
          Veta=Radka
        else if(i.eq.IdFixOriginX4.or.i.eq.IdFixIndividual.or.
     1          i.eq.IdFixSetTo) then
          if(i.eq.IdFixSetTo) then
            call kus(Veta,k,t256)
            Radka=Veta(:k)
          endif
          do while (k<idel(Veta))
            kp=index(Veta(k:),'[')+k
            kk=index(Veta(k:),']')+k-2
            if(kp.gt.k) then
              Radka=Radka(:idel(Radka))//Veta(k:kp-2)
              j=0
              call CrlAtomNamesAddNewString(Veta(kp:kk),j,'[',1,Radka,
     1                                      ich)
              if(ich.eq.0) then
                Radka=Radka(:idel(Radka))//']'
              else if(ich.lt.0) then
                go to 1900
              else if(ich.gt.0) then
                go to 9000
              endif
              k=kk+2
            else
              call kus(Veta,k,t256)
              Radka=Radka(:idel(Radka))//' '//t256(:idel(t256))
            endif
          enddo
        endif
2250    Veta=Radka
      else if(LocateSubstring(t256,  'distfix',.false.,.true.).eq.1.or.
     1        LocateSubstring(t256, '!distfix',.false.,.true.).eq.1.or.
     2        LocateSubstring(t256, 'anglefix',.false.,.true.).eq.1.or.
     3        LocateSubstring(t256,'!anglefix',.false.,.true.).eq.1.or.
     4        LocateSubstring(t256,  'torsfix',.false.,.true.).eq.1.or.
     5        LocateSubstring(t256, '!torsfix',.false.,.true.).eq.1)
     6  then
        call kus(Veta,k,t256)
        call kus(Veta,k,t256)
        Radka=Veta(:k)
        do while (k<idel(Veta))
2300      call kus(Veta,k,t256)
          m=0
          idl=idel(t256)
          if(t256.eq.';') then
            Radka=Radka(:idel(Radka))//';'
            go to 2300
          else if(t256(1:1).eq.';') then
            Radka=Radka(:idel(Radka))//';'
            t256=t256(2:)
          else if(t256(idl:idl).eq.';') then
            t256(idl:)=' '
            m=1
          endif
          j=0
          call CrlAtomNamesAddNewString(t256,j,' ',1,Radka,ich)
          if(ich.eq.0) then
            if(m.ne.0) Radka=Radka(:idel(Radka))//';'
          else if(ich.lt.0) then
            Radka=Radka(1:idel(Radka))//' #$%'
            if(m.ne.0) Radka=Radka(:idel(Radka))//';'
            go to 2300
          else if(ich.gt.0) then
            go to 9000
          endif
        enddo
        idl=idel(Radka)
        if(Radka(idl:idl).ne.';') Radka(idl+1:idl+1)=';'
        m=0
        k=0
        call kus(Radka,k,Veta)
        call kus(Radka,k,Veta)
        call kus(Radka,k,Veta)
        Veta=Radka(:k-1)
        Radka=Radka(k+1:)
2400    i=index(Radka,';')
        if(i.le.0) then
          t256=Radka
        else
          t256=Radka(:i-1)
          Radka=Radka(i+1:)
        endif
        if(index(t256,'#$%').le.0) then
          ip=0
          do i=1,idel(t256)
            if(t256(i:i).ne.' ') then
              ip=i
              exit
            endif
          enddo
          if(ip.gt.0) t256=t256(ip:)
          Veta=Veta(:idel(Veta)+1)//t256(:idel(t256))//';'
          m=m+1
        endif
        if(Radka.ne.' ') go to 2400
        if(m.eq.0) go to 1900
      else if(LocateSubstring(t256,'end',.false.,.true.).eq.1) then
        write(lno,FormA) Veta(:idel(Veta))
        go to 1500
      endif
      go to 9000
3000  Veta=' '
3010  read(lni,FormA,end=3020) t256
      if(t256.eq.' ') go to 3010
      idl=idel(Veta)
      if(idl.gt.0) then
        idp=idel(t256)
        do i=1,idp
          if(t256(i:i).ne.' ') exit
        enddo
        if(t256(i:i).eq.'&') then
          Veta=Veta(:idl)//t256(i+1:idp)
          go to 3010
        else
          backspace lni
        endif
      else
        Veta=t256
        go to 3010
      endif
      go to 3030
3020  if(Veta.eq.' ') go to 9999
3030  k=0
      call kus(Veta,k,t256)
      Radka=Veta(:k)
      if(LocateSubstring(t256,  'torsion',.false.,.true.).eq.1.or.
     1   LocateSubstring(t256, '!torsion',.false.,.true.).eq.1.or.
     2   LocateSubstring(t256,    'plane',.false.,.true.).eq.1.or.
     3   LocateSubstring(t256,   '!plane',.false.,.true.).eq.1.or.
     4   LocateSubstring(t256, 'selfirst',.false.,.true.).eq.1.or.
     5   LocateSubstring(t256,'!selfirst',.false.,.true.).eq.1.or.
     6   LocateSubstring(t256, 'selsecnd',.false.,.true.).eq.1.or.
     7   LocateSubstring(t256,'!selsecnd',.false.,.true.).eq.1) then
        do while (k<len(Veta))
          kp=k
          call kus(Veta,k,t256)
          if(t256.eq.'|') then
            Radka=Radka(:idel(Radka))//' |'
          else
            k=kp
          endif
          call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
          if(ich.lt.0) go to 1900
        enddo
        Veta=Radka
      else if(LocateSubstring(t256,'end',.false.,.true.).eq.1) then
        write(lno,FormA) Veta(:idel(Veta))
        go to 1500
      endif
      go to 9000
4000  Veta=' '
4010  read(lni,FormA,end=4020) t256
      if(t256.eq.' ') go to 4010
      idl=idel(Veta)
      if(idl.gt.0) then
        idp=idel(t256)
        do i=1,idp
          if(t256(i:i).ne.' ') exit
        enddo
        if(t256(i:i).eq.'&') then
          Veta=Veta(:idl)//t256(i+1:idp)
          go to 4010
        else
          backspace lni
        endif
      else
        Veta=t256
        go to 4010
      endif
      go to 4030
4020  if(Veta.eq.' ') go to 9999
4030  k=LocateSubstring(Veta,'center',.false.,.true.)
      if(k.gt.0) then
        k=k+6
        Radka=Veta(:k)
        call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
        if(ich.lt.0) then
          go to 1900
        else if(ich.gt.0) then
          go to 9000
        endif
        Veta=Radka(:idel(Radka))//' '//Veta(k+1:)
      endif
      call kus(Veta,k,t256)
      if(LocateSubstring(t256,'end',.false.,.true.).eq.1) then
        write(lno,FormA) Veta(:idel(Veta))
        go to 1500
      endif
      go to 9000
5000  Veta=' '
5010  read(lni,FormA,end=5020) t256
      if(t256.eq.' ') go to 5010
      idl=idel(Veta)
      if(idl.gt.0) then
        idp=idel(t256)
        do i=1,idp
          if(t256(i:i).ne.' ') exit
        enddo
        if(t256(i:i).eq.'&') then
          Veta=Veta(:idl)//t256(i+1:idp)
          go to 5010
        else
          backspace lni
        endif
      else
        Veta=t256
        go to 5010
      endif
      go to 5030
5020  if(Veta.eq.' ') go to 9999
5030  k=0
      call kus(Veta,k,t256)
      if(LocateSubstring(t256, 'drawatom',.false.,.true.).eq.1.or.
     1   LocateSubstring(t256,'!drawatom',.false.,.true.).eq.1) then
        Radka=Veta(:k)
        call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
        if(ich.lt.0) then
          go to 1900
        else if(ich.gt.0) then
          go to 9000
        endif
        Veta=Radka(:idel(Radka))//' '//Veta(k+1:)
      else if(LocateSubstring(t256, 'pntplane',.false.,.true.).eq.1.or.
     1        LocateSubstring(t256,'!pntplane',.false.,.true.).eq.1)
     2  then
        call StToInt(Veta,k,ic,1,.false.,ich)
        if(ich.ne.0) go to 9000
        Radka=Veta(:k)
        if(ic(1).eq.1) then
          call CrlAtomNamesAddNewString(Veta,k,' ',1,Radka,ich)
          if(ich.lt.0) then
            go to 1900
          else if(ich.gt.0) then
            go to 9000
          endif
          Veta=Radka(:idel(Radka))//' '//Veta(k+1:)
        else
          go to 9000
        endif
      else if(LocateSubstring(t256,'end',.false.,.true.).eq.1) then
        write(lno,FormA) Veta(:idel(Veta))
        go to 1500
      endif
      go to 9000
9000  call PisPo80(lno,Veta)
!      write(lno,FormA) Veta(:idel(Veta))
      go to 1900
9900  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      call MoveFile(Fln(:iFln)//'.z51',Fln(:iFln)//'.l51')
      call iom50(1,0,Fln(:iFln)//'.m50')
9999  return
100   format(3f9.6)
      end
