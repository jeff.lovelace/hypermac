      subroutine CIFGetLoop(CIFLabel,StLabel,nmax,CIFItem,ia,sta,a,
     1                      sa,key,n,ErrSt,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256,p256
      character*(*) CIFLabel,StLabel(*),CIFItem,ErrSt,sta(*)
      dimension a(*),sa(*),ia(*)
      logical loop,EqIgCase,CIFEq
      ich=0
      n=0
      loop=.false.
      mm=0
1000  call CIFNewRecord(mm,t256,*9000)
      k=0
      call kus(t256,k,p256)
      call mala(p256)
      if(.not.loop) then
        if(EqIgCase(p256,'loop_')) then
          loop=.true.
          m=0
          il=0
          ii=0
        endif
        go to 1000
      endif
      if(p256(1:1).eq.'_') then
        m=m+1
        if(CIFEq(p256,CIFLabel)) il=m
        if(CIFEq(p256,CIFItem)) ii=m
        go to 1000
      else
        loop=.false.
      endif
      if(CIFItem.eq.' ') ii=-1
      if(CIFLabel.eq.' ') il=-1
      if(ii.eq.0.or.il.eq.0) go to 1000
      i=0
      k=0
1150  call kusap(t256,k,p256)
      if(p256(1:1).eq.'_'.or.EqIgCase(p256,'loop_')) go to 9999
      if(p256(1:1).eq.';') then
        istr=index(t256,';')
      else
        istr=0
      endif
      i=mod(i,m)+1
      if(i.eq.1) n=n+1
      if(n.gt.nmax) then
        n=nmax
        go to 9200
      endif
1200  if(i.eq.il) then
        StLabel(n)=p256
      else if(i.eq.ii) then
        if(key.gt.0) then
          call GetRealEsdFromSt(p256,a(n),sa(n),ich)
          if(ich.eq.-1) ich=0
        else if(key.eq.0) then
          sta(n)=p256
        else
          call GetIntFromSt(p256,ia(n),ich)
        endif
        if(ich.ne.0) go to 9100
      else
        if(istr.ne.0) then
1300      k=index(t256(istr+1:),';')
          if(k.le.0) then
            call CIFNewRecord(mm,t256,*9999)
            istr=0
            go to 1300
          endif
          k=k+istr-1
          call kus(t256,k,p256)
        endif
      endif
      if(k.ge.256) then
        call CIFNewRecord(mm,t256,*9999)
        k=0
      endif
      go to 1150
9000  ich=-1
      if(CifItem.ne.' ') then
        ErrSt=CifItem
      else
        ErrSt=CifLabel
      endif
      go to 9999
9100  ich=-2
      ErrSt=t256
      go to 9999
9200  ich=333
9999  return
      end
