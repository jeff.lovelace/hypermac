      subroutine CrlMakeMolAtShift(ia)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension qrat(3)
      call trortho(0)
      if(TypeModFun(ia).eq.1) TypeModFun(ia)=0
      call qbyx(x(1,ia),qrat,iswa(ia))
      kk=0
      do k=1,KModA(1,ia)
        if(k.gt.KModA(1,ia)-NDimI(KPhase).and.KFA(1,ia).ne.0)
     1    then
          kk=kk+1
          ax(k,ia)=ax(k,ia)+qrat(kk)-qcnt(kk,ia)
        else
          fik=0.
          do m=1,NDimI(KPhase)
            fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
          enddo
          sinfik=sin(pi2*fik)
          cosfik=cos(pi2*fik)
          xp=ax(k,ia)
          yp=ay(k,ia)
          ax(k,ia)= xp*cosfik+yp*sinfik
          ay(k,ia)=-xp*sinfik+yp*cosfik
          sxp=sax(k,ia)**2
          syp=say(k,ia)**2
          sax(k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
          say(k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
        endif
      enddo
      do k=1,KModA(2,ia)
        if(k.eq.KModA(2,ia).and.KFA(2,ia).ne.0) then
          uy(1,k,ia)=uy(1,k,ia)+qrat(1)-qcnt(1,ia)
        else
          fik=0.
          do m=1,NDimI(KPhase)
            fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
          enddo
          sinfik=sin(pi2*fik)
          cosfik=cos(pi2*fik)
          do l=1,3
            xp=ux(l,k,ia)
            yp=uy(l,k,ia)
            ux(l,k,ia)= xp*cosfik+yp*sinfik
            uy(l,k,ia)=-xp*sinfik+yp*cosfik
            sxp=sux(l,k,ia)**2
            syp=suy(l,k,ia)**2
            sux(l,k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
            suy(l,k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
          enddo
        endif
      enddo
      do k=1,KModA(3,ia)
        fik=0.
        do m=1,NDimI(KPhase)
          fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
        enddo
        sinfik=sin(pi2*fik)
        cosfik=cos(pi2*fik)
        do l=1,6
          xp=bx(l,k,ia)
          yp=by(l,k,ia)
          bx(l,k,ia)= xp*cosfik+yp*sinfik
          by(l,k,ia)=-xp*sinfik+yp*cosfik
          sxp=sbx(l,k,ia)**2
          syp=sby(l,k,ia)**2
          sbx(l,k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
          sby(l,k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
        enddo
      enddo
      call trortho(1)
      return
      end
