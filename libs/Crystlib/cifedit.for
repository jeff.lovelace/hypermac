      subroutine CIFEdit(FileIn)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) FileIn
      character*256 Veta
      logical CIFAllocated,ExistFile,FeYesNo
      CIFAllocated=allocated(CifKey)
      if(.not.CIFAllocated) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9999
      endif
      if(FileIn.eq.' ') then
        FileIn=fln(:ifln)//'.cif'
1050    call FeFileManager('Select input CIF file',FileIn,'*.cif',0,
     1                     .true.,ich)
        if(ich.ne.0.or.FileIn.eq.' ') go to 9999
        if(.not.ExistFile(FileIn)) then
          if(.not.FeYesNo(-1.,-1.,'The file "'//FileIn(:idel(FileIn))//
     1                  '" does not exist, try again?',0)) go to 1050
        endif
      endif
      lni=NextLogicNumber()
      call OpenFile(lni,FileIn(:idel(FileIn)),'formatted','unknown')
      if(ErrFlag.ne.0) go to 9100
      if(allocated(CIFArray)) deallocate(CIFArray)
      nCIFArray=1000
      allocate(CIFArray(nCIFArray))
      nCIFArrayUpdate=1000
      allocate(CIFArrayUpdate(nCIFArrayUpdate))
      n=0
1100  read(lni,FormA,end=1200) Veta
      n=n+1
      if(n.gt.nCIFArray) call ReallocateCIFArray(nCIFArray+1000)
      CIFArray(n)=Veta
      go to 1100
1200  nCIFUsed=n
      KartIdZpet=1
      xdq=600.
      il=14
      Veta='CIF edit:'
      call FeKartCreate(-1.,-1.,xdq,il,Veta,0,0)
      xdqp=xdq-2.*KartSidePruh
      call FeCreateListek('Title/authors',1)
      KartIdTitle=KartLastId
      call CIFEditTitleMake(KartIdTitle)
      call FeCreateListek('Crystal',1)
      KartIdCrystal=KartLastId
      call CIFEditCrystalMake(KartIdCrystal)
      call FeCreateListek('Diffractometer',1)
      KartIdDiffract=KartLastId
      call CIFEditDiffractMake(KartIdDiffract)
      call FeCreateListek('Texts',1)
      KartIdTexts=KartLastId
      call CIFEditTextsMake(KartIdTexts)
      call FeCompleteKart(KartIdZpet)
1500  call FeQuestEvent(KartId,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        call FePrepniListek(KartIdCrystal-1)
        call CIFEditCrystalUpdate
        rewind lni
        n=1
2100    write(lni,FormA) CIFArray(n)(:idel(CIFArray(n)))
        n=n+1
        if(n.le.nCIFUsed) go to 2100
      else if(CheckType.eq.EventKartSw) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeDestroyKart
9100  call CloseIfOpened(lni)
9999  if(.not.CIFAllocated) deallocate(CifKey,CifKeyFlag)
      if(allocated(CIFArray)) deallocate(CIFArray)
      if(allocated(CIFArrayUpdate)) deallocate(CIFArrayUpdate)
      return
      end
