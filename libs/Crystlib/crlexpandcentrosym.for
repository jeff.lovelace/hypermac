      subroutine CrlExpandCentroSym
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      KPhaseIn=KPhase
      do 2000KPh=1,NPhase
        if(NCSymm(KPh).eq.1) go to 2000
        n=NSymm(KPh)
        call ReallocSymm(NDim(KPh),2*n,NLattVec(KPh),NComp(KPh),
     1                   NPhase,0)
        do i=1,NSymm(KPh)
          n=n+1
          call CopyVek(s6(1,i,1,KPh),s6(1,n,1,KPh),NDim(KPh))
          call RealMatrixToOpposite(rm6(1,i,1,KPh),rm6(1,n,1,KPh),
     1                              NDim(KPh))
          call RealMatrixToOpposite(rm (1,i,1,KPh),rm (1,n,1,KPh),3)
          call CodeSymm(rm6(1,n,1,KPh),s6(1,n,1,KPh),
     1                  symmc(1,n,1,KPh),0)
          iswSymm(n,1,KPh)=iswSymm(i,1,KPh)
        enddo
        NSymm(KPh)=n
        MaxNSymm=max(MaxNSymm,n)
        NCSymm(KPh)=1
        call SetMet(0)
        call FinSym(0)
2000  continue
      if(MagneticType(KPh).gt.0) call CrlOrderMagSymmetry
      return
      end
