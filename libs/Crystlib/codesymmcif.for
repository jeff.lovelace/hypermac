      subroutine CodeSymmCIF(isym,ncentr,SmbOp)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmp(36),sp(6)
      character*(*) SmbOp
      character*15 StPom(6)
      call CopyMat(rm6(1,isym,1,KPhase),rmp,NDim(KPhase))
      call CopyVek(s6(1,isym,1,KPhase),sp,NDim(KPhase))
      do i=1,NDim(KPhase)
        sp(i)=s6(i,isym,1,KPhase)+vt6(i,ncentr,1,KPhase)
      enddo
      call od0do1(sp,sp,NDim(KPhase))
      call CodeSymm(rmp,sp,StPom,0)
      SmbOp=StPom(1)
      call Zhusti(SmbOp)
      do i=2,NDim(KPhase)
        call Zhusti(StPom(i))
        SmbOp=SmbOp(1:idel(SmbOp))//','//StPom(i)(1:idel(StPom(i)))
      enddo
      return
      end
