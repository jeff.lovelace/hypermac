      subroutine UpdateDatBlocksM40
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      logical PwdCheckTOFInD
      KPhaseIn=KPhase
      j=0
      IZdvihI=0
      IZdvihJ=0
      do i=1,NDatBlock
        if(NRef90(i).gt.0) then
          call PwdSetTOF(i)
          j=j+1
          if(i.gt.j) then
            call CopyVek(sc(1,i),sc(1,j),mxscutw)
            call CopyVek(ec(1,i),ec(1,j),12)
            do KPh=1,NPhase
              if(KAnRef(i).ne.0) then
                call CopyVek(FFrRef(1,KPh,i),FFrRef(1,KPh,j),
     1                       NAtFormula(KPh))
                call CopyVek(FFiRef(1,KPh,i),FFiRef(1,KPh,j),
     1                       NAtFormula(KPh))
              endif
            enddo
            DataType(j)=DataType(i)
            call CopyVekI(kis(1,i),kis(1,j),MxScAll)
            if(iabs(DataType(i)).ne.2) go to 1100
            KBackg(j)=KBackg(i)
            NBackg(j)=NBackg(i)
            KManBackg(j)=KManBackg(i)
            KWleBail(j)=KWleBail(i)
            KKleBail(j)=KKleBail(i)
            KAbsor(j)=KAbsor(i)
            KRefLam(j)=KRefLam(i)
            KAsym(j)=KAsym(i)
            NAsym(j)=NAsym(i)
            MirPwd(j)=MirPwd(i)
            NSkipPwd(j)=NSkipPwd(i)
            KUseTOFJason(j)=KUseTOFJason(i)
            TOFKey(j)=TOFKey(i)
            call CopyVek(SkipPwdFr(1,i),SkipPwdFr(1,j),NSkipPwd(i))
            call CopyVek(SkipPwdTo(1,i),SkipPwdTo(1,j),NSkipPwd(i))
            KProfPwd(KPh,j)=KProfPwd(KPh,i)
            call CopyVekI(KStrain(1,i),KStrain(1,j),NPhase)
            call CopyVekI(KPref(1,i),KPref(1,j),NPhase)
            call CopyVekI(SPref(1,i),SPref(1,j),NPhase)
            call CopyVek (PCutOff(1,i),PCutOff(1,j),NPhase)
            if(isTOF) then
              if(KUseTOFJason(j).le.0) then
                n=3
              else
                n=7
              endif
            else
              n=3
            endif
            call CopyVek (ShiftPwd(1,i),ShiftPwd(1,j),n)
            call CopyVekI(kipwd(IShiftPwd+IZdvihI),
     1                    kipwd(IShiftPwd+IZdvihJ),3)
            if(NBackg(j).gt.0) then
              call CopyVek (BackgPwd(1,i),BackgPwd(1,j),NBackg(j))
              call CopyVekI(kipwd(IBackgPwd+IZdvihI),
     1                      kipwd(IBackgPwd+IZdvihJ),NBackg(j))
            endif
            if(KRough(j).gt.0) then
              call CopyVek (RoughPwd(1,i),RoughPwd(1,j),2)
              call CopyVekI(kipwd(IRoughPwd+IZdvihI),
     1                      kipwd(IRoughPwd+IZdvihJ),2)
            endif
            if(KRefLam(j).gt.0) then
              call CopyVek (LamPwd(1,i),LamPwd(1,j),Nalfa(i))
              call CopyVekI(kipwd(ILamPwd+IZdvihI),
     1                      kipwd(ILamPwd+IZdvihJ),Nalfa(i))
            endif
            if(KAsym(j).ne.IdPwdAsymNone) then
              call CopyVek (AsymPwd(1,i),AsymPwd(1,j),NAsym(i))
              call CopyVekI(kipwd(IAsymPwd+IZdvihI),
     1                      kipwd(IAsymPwd+IZdvihJ),NAsym(i))
            endif
          endif
1100      IZdvihJ=IZdvihJ+NParRecPwd
        endif
        IZdvihI=IZdvihI+NParRecPwd
      enddo
      if(.not.ExistPowder) go to 9999
      IZdvihP=0
      do KPh=1,NPhase
        IZdvihI=IZdvihP
        IZdvihJ=IZdvihP
        j=0
        do i=1,NDatBlock
          if(NRef90(i).gt.0) then
            j=j+1
            if(iabs(DataType(i)).ne.2) go to 2700
            KPhase=KPh
            if(KProfPwd(KPh,i).eq.IdPwdProfGauss.or.
     1         KProfPwd(KPh,i).eq.IdPwdProfVoigt) then
              call CopyVek (GaussPwd(1,KPh,i),GaussPwd(1,KPh,j),4)
              call CopyVekI(kipwd(IGaussPwd+IZdvihI),
     1                      kipwd(IGaussPwd+IZdvihJ),4)
            endif
            if(KProfPwd(KPh,i).eq.IdPwdProfLorentz.or.
     1         KProfPwd(KPh,i).eq.IdPwdProfModLorentz.or.
     2         KProfPwd(KPh,i).eq.IdPwdProfVoigt) then
              if(KStrain(KPh,i).eq.IdPwdStrainTensor.and.
     1           KProfPwd(KPh,i).eq.IdPwdProfVoigt) then
                n=5
              else
                n=4
              endif
              call CopyVek (LorentzPwd(1,KPh,i),LorentzPwd(1,KPh,j),n)
              ZetaPwd(NPhase,j)=ZetaPwd(1,i)
              call CopyVekI(kipwd(ILorentzPwd+IZdvihI),
     1                      kipwd(ILorentzPwd+IZdvihJ),n)
            endif
            if(KStrain(KPh,i).eq.IdPwdStrainTensor) then
              if(NDimI(KPh).eq.1) then
                 n=35
              else
                 n=15+NDimI(KPh)
              endif
              call CopyVek (StPwd(1,KPh,i),StPwd(1,KPh,j),n)
              call CopyVekI(kipwd(IStPwd+IZdvihI),
     1                      kipwd(IStPwd+IZdvihJ),n)
            endif
            if(KPref(KPh,i).ne.IdPwdPrefNone) then
              call CopyVek (PrefPwd(1,KPh,i),PrefPwd(1,KPh,j),2)
              call CopyVekI(kipwd(IPrefPwd+IZdvihI),
     1                      kipwd(IPrefPwd+IZdvihJ),2)
            endif
            IZdvihJ=IZdvihJ+NParProfPwd
          endif
2700      IZdvihI=IZdvihI+NParProfPwd
        enddo
        IZdvihP=IZdvihP+NParCellProfPwd
      enddo
9999  KPhase=KPhaseIn
      return
      end
