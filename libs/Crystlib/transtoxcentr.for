      subroutine TransToXCentr
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension IKde(3),INul(3),NKde(3),NNul(3),TrMat(4,4),xp(4),
     1          xpp(4)
      character*256 Veta,flnOld
      logical EqIgCase,ExistFile
      flnOld=fln
      iflnOld=ifln
      call CrlDefineNewFLN(Konec)
      if(Konec.eq.1) go to 9999
      if(.not.EqIgCase(fln,flnOld)) then
        do i=1,5
          if(i.eq.1) then
            Cislo='.m40'
          else if(i.eq.2) then
            if(.not.isPowder) cycle
            Cislo='.m41'
          else if(i.eq.3) then
            Cislo='.m50'
          else if(i.eq.4) then
            Cislo='.m90'
          else if(i.eq.5) then
            Cislo='.m95'
          endif
          Veta=flnOld(:iflnOld)//Cislo(:idel(Cislo))
          if(ExistFile(Veta)) call CopyFile(Veta,fln(:ifln)//
     1                                    Cislo(:idel(Cislo)))
        enddo
      endif
      n=0
      do i=1,3
        IKde(i)=0
        NKde(i)=0
        INul(i)=0
        NNul(i)=0
        pom=i
        do j=1,3
          if(abs(quir(j,1,KPhase)).lt..01) then
            NNul(i)=NNul(i)+1
            INul(i)=j
          else if(abs(abs(quir(j,1,KPhase)*pom)-1.).lt..01) then
            NKde(i)=NKde(i)+1
            IKde(i)=j
          endif
        enddo
      enddo
      j=0
      do i=1,3
        if(NKde(i).gt.0) then
          if(j.gt.0) then
            Veta='has unacceptable form.'
            go to 9000
          endif
          n=i
          j=j+1
        endif
      enddo
      if(NNul(n).le.0) then
        Veta='has three non-zero components.'
        go to 9000
      else if(NNul(n).ge.3) then
        Veta='is the null vector.'
        go to 9000
      endif
      call UnitMat(TrMat,4)
      if(n.eq.2) then
        if(NNul(n).eq.1) then
          do i=INul(n)+1,INul(n)+2
            ii=mod(i-1,3)+1
            do j=INul(n)+1,INul(n)+2
              jj=mod(j-1,3)+1
              if(i.eq.INul(n)+1.and.j.eq.INul(n)+2) then
                TrMat(ii,jj)=-1.
              else
                TrMat(ii,jj)= 1.
              endif
            enddo
          enddo
        else if(NNul(n).eq.2) then
          ii=IKde(n)
          TrMat(ii,ii)=2.
        endif
      else if(n.eq.3) then
        if(NNul(n).eq.1) then
          ii=mod(INul(n)  ,3)+1
          jj=mod(INul(n)+1,3)+1
          TrMat(ii,ii)= 2.
          TrMat(ii,jj)=-1.
          TrMat(jj,ii)= 1.
          TrMat(jj,j)= 1.
        else if(NNul(n).eq.2) then
          ii=IKde(n)
          TrMat(ii,ii)=3.
        endif
      endif
      call CopyVek(Quir(1,1,KPhase),xp,3)
      xp(4)=0.
      call MultM(xp,TrMat,xpp,1,4,4)
      do i=1,3
        TrMat(4,i)=xpp(i)
      enddo
      call DRCellDefinedTrans(TrMat)
      if(.not.EqIgCase(fln,FlnOld)) then
        Veta=fln
        fln=FlnOld
        ifln=iFlnOld
        call ContinueWithNewStructure(Veta)
      endif
      go to 9999
9000  Veta='the rational part of the modulation vector '//
     1     Veta(:idel(Veta))
      call FeChybne(-1.,-1.,Veta,' ',SeriousError)
      if(.not.EqIgCase(fln,flnOld)) then
        do i=1,5
          if(i.eq.1) then
            Cislo='.m40'
          else if(i.eq.2) then
            if(.not.isPowder) cycle
            Cislo='.m41'
          else if(i.eq.3) then
            Cislo='.m50'
          else if(i.eq.4) then
            Cislo='.m90'
          else if(i.eq.5) then
            Cislo='.m95'
          endif
          Veta=fln(:ifln)//Cislo(:idel(Cislo))
          if(ExistFile(Veta)) call DeleteFile(Veta)
        enddo
        fln=flnOld
        ifln=iflnOld
        call iom50(0,0,fln(:ifln)//'.m50')
        if(ExistM90) call iom90(0,fln(:ifln)//'.m90')
        if(ExistM95) call iom95(0,fln(:ifln)//'.m95')
      endif
9999  return
      end
