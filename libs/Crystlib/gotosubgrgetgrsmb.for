      subroutine GoToSubGrGetGrSmb(nss,nvts,Grp,GrpTr,NGrp,ncoset,TrAll,
     1                             TrAllI,TrShift,CrSystemNew,IsNormal,
     2                             Klic)
      use Basic_mod
      use GoToSubGr_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension TrAll(*),TrAllI(*),TrShift(*),xp(6),ShSgS(6),RPom(36),
     1          RPomI(36),RPomC(36),ShSgP(6)
      integer CrSystemS,CrSystemNew
      logical EqRV,MatRealEqUnitMat,IsNormal
      character*(*) Grp,GrpTr
      character*4   LatticeS
      nss=NSymm(KPhase)
      nssn=NSymmN(KPhase)
      nvts=NLattVec(KPhase)
      CrSystemS=CrSystem(KPhase)
      call CopyVek(ShSg(1,KPhase),ShSgS,min(NDim(KPhase),4))
      BratSymmSub(1)=.true.
      LatticeS=Lattice(KPhase)
      call UnitMat(TrAll ,NDim(KPhase))
      call UnitMat(TrAllI,NDim(KPhase))
      nvtsr=0
      j=0
      do i=1,nss
        call CopyMat(rm6(1,i,1,KPhase),rm6s(1,i),NDim(KPhase))
        call CopyVek(s6(1,i,1,KPhase),s6s(1,i),NDim(KPhase))
        OpSmbS(i)=OpSmb(i)
        IswSymmS(i)=ISwSymm(i,1,KPhase)
        call CopyVekI(io(1,i),ios(1,i),3)
        ZMagS(i)=ZMag(i,1,KPhase)
        call CopyMat(RMag(1,i,1,KPhase),RMagS(1,i),3)
        if(BratSymmSub(i)) then
          j=j+1
          if(i.ne.j) then
            call CopySymmOperator(i,j,1)
            OpSmb(j)=OpSmb(i)
            call CopyVekI(io(1,i),io(1,j),3)
          endif
        endif
        if(MatRealEqUnitMat(rm6(1,i,1,KPhase),NDim(KPhase),.001).and.
     1     ZMag(i,1,KPhase).eq.1.) nvtsr=nvtsr+1
      enddo
      do i=1,nvts
        call CopyVek(vt6(1,i,1,KPhase),vt6s(1,i),NDim(KPhase))
      enddo
      NSymm(KPhase)=j
      NSymmN(KPhase)=j
      call CompleteSymm(1,ich)
      indc=nss/NSymm(KPhase)
      ii=0
      do 1100i=1,NSymm(KPhase)
        do j=1,nss
          if(.not.Eqrv(rm6(1,i,1,KPhase),rm6s(1,j),NDimQ(KPhase),.001)
     1       .or.ZMag(i,1,KPhase).ne.ZMagS(j)) cycle
          do k=1,nvts
            call AddVek(s6s(1,j),Vt6s(1,k),xp,NDim(KPhase))
            call od0do1(xp,xp,NDim(KPhase))
            if(EqRV(s6(1,i,1,KPhase),xp,NDim(KPhase),.0001)) then
              ii=ii+1
              BratSymmSub(j)=.true.
              OpSmb(i)=OpSmbS(j)
              call CopyVekI(ios(1,j),io(1,i),3)
              go to 1100
            endif
          enddo
        enddo
1100  continue
      IsNormal=.true.
      do j=2,nss
        if(BratSymmSub(j)) cycle
        call MatInv(rm6s(1,j),RPomI,det,NDim(KPhase))
        do 1200i=2,NSymm(KPhase)
          call MultM(RPomI,rm6(1,i,1,KPhase),RPom,NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          call MultM(RPom,rm6s(1,j),RPomC,NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          do k=1,NSymm(KPhase)
            if(EqRV(RPomC,rm6(1,k,1,KPhase),NDimQ(KPhase),.001))
     1         go to 1200
          enddo
          IsNormal=.false.
          go to 1250
1200    continue
      enddo
1250  call CrlCheckSymmForLattVec(1)
      ind=(indc*NLattVec(KPhase))/(nvts*nvtsr)
      if(indc.eq.1.and.Klic.eq.0) then
        call FindSmbSg(Grp,ChangeOrderNo,1)
        GrpTr=Grp
        call CopyVek(ShSg(1,KPhase),TrShift,min(NDim(KPhase),4))
      else
        call FindSmbSgTr(Grp,ShSgP,GrpTr,NGrTr,TrAll,TrShift,
     1                   CrSystemNew,i)
        call MatInv(TrAll,TrAllI,VolRatio,NDim(KPhase))
        NGrp=NGrupa(KPhase)
      endif
      ncoset=NSymm(KPhase)
      NSymm(KPhase)=nss
      NSymmN(KPhase)=nssn
      NLattVec(KPhase)=nvts
      do i=1,nss
        call CopyMat (rm6s(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
        call MatBlock3(rm6s(1,i),rm(1,i,1,KPhase),NDim(KPhase))
        call CopyVek(s6s(1,i),s6(1,i,1,KPhase),NDim(KPhase))
        call codesymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        call CopyVekI(ios(1,i),io(1,i),3)
        OpSmb(i)=OpSmbS(i)
        ISwSymm(i,1,KPhase)=IswSymmS(i)
        ZMag(i,1,KPhase)=ZMagS(i)
        call CopyMat(RMagS(1,i),RMag(1,i,1,KPhase),3)
      enddo
      do i=1,NLattVec(KPhase)
        call CopyVek(vt6s(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
      enddo
      Lattice(KPhase)=LatticeS
      CrSystemNew=CrSystem(KPhase)
      CrSystem(KPhase)=CrSystemS
      call CopyVek(ShSgS,ShSg(1,KPhase),min(NDim(KPhase),4))
      return
      end
