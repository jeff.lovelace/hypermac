      subroutine Fract2ShpCoor(xx)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xx(3),xp(3)
      call MultM(TrToOrtho(1,1,KPhase),xx,xp,3,3,1)
      xx(1)=VecOrtLen(xp,3)
      if(xp(3).lt.xx(1)) then
        xx(3)=acos(xp(3)/xx(1))/ToRad
        if(abs(xx(3)-90. ).lt..001) xx(3)= 90.
        if(abs(xx(3)+90. ).lt..001) xx(3)=-90.
        if(abs(xx(3)     ).lt..001) xx(3)=  0.
        if(abs(xx(3)-180.).lt..001) xx(3)=180.
      else
        xx(3)=0.
      endif
      pom=sqrt(xp(1)**2+xp(2)**2)
      if(xp(1).lt.pom) then
        xx(2)=atan2(xp(2),xp(1))/ToRad
        if(abs(xx(2)-90. ).lt..001) xx(2)= 90.
        if(abs(xx(2)+90. ).lt..001) xx(2)=-90.
        if(abs(xx(2)     ).lt..001) xx(2)=  0.
        if(abs(xx(2)-180.).lt..001) xx(2)=180.
      else
        xx(2)=0.
      endif
      return
      end
