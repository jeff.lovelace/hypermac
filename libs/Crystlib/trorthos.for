      subroutine trorthos(klic)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kmodap(7)
      KPhaseIn=KPhase
      do ia=1,NAtCalc
        if(TypeModFun(ia).ne.1) cycle
        isw=iswa(ia)
        itfi=itf(ia)
        if(klic.eq.0) then
          call SetIntArrayTo(kmodap,7,0)
          kmodap(1)=KModA(1,ia)
          do n=2,itfi+1
            if(KModA(n,ia).le.0) cycle
            kmodap(n)=(OrthoSel(2*KModA(n,ia)+1,ia)+1)/2
          enddo
          call ReallocateAtomParams(NAtAll,itfi,IFrMax,kmodap,MagParMax)
          call SetIntArrayTo(kmodap,7,0)
          kmodap(1)=KModA(1,ia)
          do n=2,itfi+1
            kmodap(n)=KModA(n,ia)
            nrank=TRank(n-1)
            if(n.eq.2) then
              call trzors( x(1,ia), ux(1,1,ia), uy(1,1,ia),
     1                    sx(1,ia),sux(1,1,ia),suy(1,1,ia),nrank,
     2                    KFA(n,ia),kmodap(n),kmodao(n,ia),
     3                    OrthoMat(1,ia),OrthoSel(1,ia),
     4                    OrthoOrd)
            else if(n.eq.3) then
              call trzors( beta(1,ia), bx(1,1,ia), by(1,1,ia),
     1                    sbeta(1,ia),sbx(1,1,ia),sby(1,1,ia),nrank,
     2                    KFA(n,ia),kmodap(n),kmodao(n,ia),
     3                    OrthoMat(1,ia),OrthoSel(1,ia),
     4                    OrthoOrd)
            else if(n.eq.4) then
              call trzors( c3(1,ia), c3x(1,1,ia), c3y(1,1,ia),
     1                    sc3(1,ia),sc3x(1,1,ia),sc3y(1,1,ia),nrank,
     2                    KFA(n,ia),kmodap(n),kmodao(n,ia),
     3                    OrthoMat(1,ia),OrthoSel(1,ia),
     4                    OrthoOrd)
            else if(n.eq.5) then
              call trzors( c4(1,ia), c4x(1,1,ia), c4y(1,1,ia),
     1                    sc4(1,ia),sc4x(1,1,ia),sc4y(1,1,ia),nrank,
     2                    KFA(n,ia),kmodap(n),kmodao(n,ia),
     3                    OrthoMat(1,ia),OrthoSel(1,ia),
     4                    OrthoOrd)
            else if(n.eq.6) then
              call trzors( c5(1,ia), c5x(1,1,ia), c5y(1,1,ia),
     1                    sc5(1,ia),sc5x(1,1,ia),sc5y(1,1,ia),nrank,
     2                    KFA(n,ia),kmodap(n),kmodao(n,ia),
     3                    OrthoMat(1,ia),OrthoSel(1,ia),
     4                    OrthoOrd)
            else
              call trzors( c6(1,ia), c6x(1,1,ia), c6y(1,1,ia),
     1                    sc6(1,ia),sc6x(1,1,ia),sc6y(1,1,ia),nrank,
     2                    KFA(n,ia),kmodap(n),kmodao(n,ia),
     3                    OrthoMat(1,ia),OrthoSel(1,ia),
     4                    OrthoOrd)
            endif
          enddo
          call ShiftKiAt(ia,itf(ia),ifr(ia),lasmax(ia),kmodap,
     2                   MagPar(ia),.true.)
          do n=2,itfi+1
            KModA(n,ia)=kmodap(n)
          enddo
          if(kmol(ia).eq.0) call qbyx(x(1,ia),qcnt(1,ia),isw)
        else
          do n=2,itfi+1
            nrank=TRank(n-1)
            kmod=kmodao(n,ia)
            if(kmod.le.0) kmod=KModA(n,ia)
            if(n.eq.2) then
              call trdoor(x(1,ia),ux(1,1,ia),uy(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else if(n.eq.3) then
              call trdoor(beta(1,ia),bx(1,1,ia),by(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else if(n.eq.4) then
              call trdoor(c3(1,ia),c3x(1,1,ia),c3y(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else if(n.eq.5) then
              call trdoor(c4(1,ia),c4x(1,1,ia),c4y(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else if(n.eq.6) then
              call trdoor(c5(1,ia),c5x(1,1,ia),c5y(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            else
              call trdoor(c6(1,ia),c6x(1,1,ia),c6y(1,1,ia),nrank,
     1                    KFA(n,ia),KModA(n,ia),kmod,OrthoMatI(1,ia),
     2                    OrthoSel(1,ia),OrthoOrd)
            endif
          enddo
          do n=itfi+2,7
            kmodao(n,ia)=0
          enddo
        endif
      enddo
      return
      end
