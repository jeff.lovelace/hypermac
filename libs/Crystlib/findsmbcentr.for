      subroutine FindSmbCentr(LatS,isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ika(3)
      character*1 LatS
      if(NLattVec(KPhase).eq.1) then
        LatS='P'
      else if(NLattVec(KPhase).eq.2) then
        ik=0
        ic=0
        n=0
        do i=1,3
          if(abs(abs(vt6(i,2,isw,KPhase)*2.)-1.).lt..1) then
            ic=ic+1
          else if(abs(vt6(i,2,isw,KPhase)).lt..1) then
            n=n+1
            ik=i+1
          else
            go to 9000
          endif
        enddo
        if(ic.eq.3) then
          LatS='I'
        else if(n.eq.1) then
          LatS=smbc(ik:ik)
        else
          LatS='X'
        endif
      else if(NLattVec(KPhase).eq.4) then
        do j=2,NLattVec(KPhase)
          ik=0
          ic=0
          do i=1,3
            if(abs(abs(vt6(i,j,isw,KPhase)*2.)-1.).lt..1) then
              ic=ic+1
            else if(abs(vt6(i,j,isw,KPhase)).lt..1) then
              ik=i
            else
              go to 9000
            endif
          enddo
          if(ic.ne.2) then
            go to 9000
          else
            do i=1,j-2
              if(ik.eq.ika(i)) go to 9000
            enddo
            ika(j-1)=ik
          endif
        enddo
        LatS='F'
      else if(NLattVec(KPhase).eq.3) then
        ik=0
        ic=0
        do i=2,3
          if(abs(vt6(1,i,isw,KPhase)*3-2.).lt..01) then
            if(abs(vt6(2,i,isw,KPhase)*3-1.).gt..01.or.
     1         abs(vt6(3,i,isw,KPhase)*3-1.).gt..01.or.ik.ne.0)
     2        go to 9000
            ik=1
          else if(abs(vt6(1,i,isw,KPhase)*3-1.).lt..01) then
            if(abs(vt6(2,i,isw,KPhase)*3-2.).gt..01.or.
     1         abs(vt6(3,i,isw,KPhase)*3-2.).gt..01.or.ic.ne.0)
     2        go to 9000
            ic=1
          else
            go to 9000
          endif
        enddo
        LatS='R'
      else
        go to 9000
      endif
      if(NDim(KPhase).eq.3.or.NLattVec(KPhase).le.1) go to 9999
      do i=1,NLattVec(KPhase)
        pom=0.
        do j=4,NDim(KPhase)
          pom=pom+abs(vt6(j,i,isw,KPhase))
        enddo
        if(pom.gt..01) go to 9000
      enddo
      go to 9999
9000  LatS='X'
9999  return
      end
