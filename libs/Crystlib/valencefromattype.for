      integer function ValenceFromAtType(AtType)
      include 'fepc.cmn'
      character*(*) AtType
      character*1   Znak
      i=idel(AtType)
      Znak=AtType(i:i)
      if(Znak.eq.'+') then
        ValenceFromAtType= 1
      else if(Znak.eq.'-') then
        ValenceFromAtType=-1
      else
        ValenceFromAtType= 0
      endif
      if(ValenceFromAtType.ne.0) then
        i=i-1
        read(AtType(i:i),'(i1)',err=2000) j
        ValenceFromAtType=ValenceFromAtType*j
      endif
      go to 9999
2000  ValenceFromAtType=0
9999  return
      end
