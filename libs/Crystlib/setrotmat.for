      subroutine SetRotMat(Angles,RotMat,DRotMat,ksw,isw)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Angles(3),RotMat(3,3),DRotMat(3,3,3),
     1          PA(9),PB(9),PC(9),PAd(9),PBd(9),PCd(9)
      snf=sin(torad*Angles(1))
      csf=cos(torad*Angles(1))
      snc=sin(torad*Angles(2))
      csc=cos(torad*Angles(2))
      snp=sin(torad*Angles(3))
      csp=cos(torad*Angles(3))
      PA=0.
      PB=0.
      PC=0.
      PAd=0.
      PBd=0.
      PCd=0.
      PC(1) = csf
      PC(2) = snf
      PC(4) =-snf
      PC(5) = csf
      PC(9) = 1.
      PCd(1)=-snf
      PCd(2)= csf
      PCd(4)=-csf
      PCd(5)=-snf
      PA(1) = 1.
      PA(5) = csp
      PA(6) = snp
      PA(8) =-snp
      PA(9) = csp
      PAd(5)=-snp
      PAd(6)= csp
      PAd(8)=-csp
      PAd(9)=-snp
      PB(1) = csc
      PB(3) =-snc
      PB(5) = 1.
      PB(7) = snc
      PB(9) = csc
      PBd(1)=-snc
      PBd(3)=-csc
      PBd(7)= csc
      PBd(9)=-snc
      call multm5(RotMat,TrToOrthoI(1,isw,ksw),PC,PB,PA,
     1            TrToOrtho(1,isw,ksw),1)
      call multm5(DRotMat(1,1,1),TrToOrthoI(1,isw,ksw),PCd,PB,PA,
     1            TrToOrtho(1,isw,ksw),1)
      call multm5(DRotMat(1,1,2),TrToOrthoI(1,isw,ksw),PC,PBd,PA,
     1            TrToOrtho(1,isw,ksw),1)
      call multm5(DRotMat(1,1,3),TrToOrthoI(1,isw,ksw),PC,PB,PAd,
     1            TrToOrtho(1,isw,ksw),1)
      do i=1,3
        do j=1,3
          do k=1,3
            DRotMat(i,j,k)=ToRad*DRotMat(i,j,k)
          enddo
        enddo
      enddo
      return
      end
