      logical function EqIndices(H1,H2)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer H1(*),H2(*),H(6)
      logical EqIV
      EqIndices=.false.
      do i=1,NSymm(KPhase)
        call MultmIRI(H1,rm6(1,i,1,1),H,1,NDim(KPhase),NDim(KPhase))
        if(EqIV(H2,H,NDim(KPhase))) then
          EqIndices=.true.
          exit
        endif
      enddo
      return
      end
