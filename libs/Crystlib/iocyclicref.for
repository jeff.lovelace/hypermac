      subroutine IOCyclicRef(Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      integer ip(1)
      real xp(1)
      logical EqIgCase
      ErrFlag=0
      ln=NextLogicNumber()
      call OpenFile(ln,CyclicRefMasterFile,'formatted','unknown')
      if(Klic.eq.0) then
        if(allocated(CyclicRefFile))
     1    deallocate(CyclicRefFile,CyclicRefStart,CyclicRefUse,
     2               CyclicRefUpDown,CyclicRefOrder,CyclicRefParam)
        read(ln,FormA,end=9900) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'parameter')) then
          call StToInt(Veta,k,ip,1,.false.,ich)
          if(ich.ne.0) go to 9900
          CyclicRefType=ip(1)
        else
          go to 9900
        endif
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'units')) then
          call kus(Veta,k,CyclicRefTypeUnits)
        else
          go to 9900
        endif
        NCyclicRefFile=0
1100    read(ln,FormA,end=1200) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'end')) go to 1200
        read(ln,FormA,end=1200) Veta
        NCyclicRefFile=NCyclicRefFile+1
        go to 1100
1200    allocate(CyclicRefFile(NCyclicRefFile),
     1           CyclicRefStart(NCyclicRefFile),
     2           CyclicRefUse(NCyclicRefFile),
     3           CyclicRefUpDown(NCyclicRefFile),
     4           CyclicRefOrder(NCyclicRefFile),
     5           CyclicRefParam(NCyclicRefFile))
        rewind ln
        read(ln,FormA,end=9900) Veta
        i=0
1300    read(ln,FormA,end=1400) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'end')) go to 1400
        if(.not.EqIgCase(Cislo(1:4),'file')) go to 9900
        i=i+1
        call Kus(Veta,k,CyclicRefFile(i))
        read(ln,FormA,end=1400) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'parameter')) go to 9900
        call StToReal(Veta,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9900
        CyclicRefParam(i)=xp(1)
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'use')) go to 9900
        call StToInt(Veta,k,ip,1,.false.,ich)
        if(ich.ne.0) go to 9900
        CyclicRefUse(i)=ip(1)
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'start')) go to 9900
        call StToInt(Veta,k,ip,1,.false.,ich)
        if(ich.ne.0) go to 9900
        CyclicRefStart(i)=ip(1)
        call Kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'updown')) go to 9900
        call StToInt(Veta,k,ip,1,.false.,ich)
        if(ich.ne.0) go to 9900
        CyclicRefUpDown(i)=ip(1)
        go to 1300
1400    call CloseIfOpened(ln)
        do i=1,NCyclicRefFile
          CyclicRefOrder(i)=i
        enddo
      else
        write(Cislo,FormI15) CyclicRefType
        call Zhusti(Cislo)
        Veta='parameter '//Cislo(:idel(Cislo))//' units '//
     1       CyclicRefTypeUnits
        write(ln,FormA) Veta(:idel(Veta))
        do i=1,NCyclicRefFile
          write(Cislo,'(''#'',i5)') i
          call Zhusti(Cislo)
          j=CyclicRefOrder(i)
          Veta='File'//Cislo(:idel(Cislo))//' '//CyclicRefFile(j)
          write(ln,FormA) Veta(:idel(Veta))
          write(Cislo,'(f10.5)') CyclicRefParam(j)
          call ZdrcniCisla(Cislo,1)
          Veta='parameter '//Cislo(:idel(Cislo))
          write(Cislo,'(i5)') CyclicRefUse(j)
          call Zhusti(Cislo)
          Veta=Veta(:idel(Veta))//' use '//Cislo(:idel(Cislo))
          write(Cislo,'(i5)') CyclicRefStart(j)
          call Zhusti(Cislo)
          Veta=Veta(:idel(Veta))//' start '//Cislo(:idel(Cislo))
          write(Cislo,'(i5)') CyclicRefUpDown(j)
          call Zhusti(Cislo)
          Veta=Veta(:idel(Veta))//' updown '//Cislo(:idel(Cislo))
          write(ln,FormA) Veta(:idel(Veta))
        enddo
      endif
      go to 9999
9900  call FeReadError(ln)
      ErrFlag=1
9999  return
      end
