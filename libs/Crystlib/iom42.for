      subroutine iom42(klic,tisk,FileName)
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(9)
      integer tisk
      character*(*) FileName
      character*8   Nazev
      character*80  t80
      character*256 Veta
      logical EqIgCase
      equivalence (IdNumbers(1) ,IdNZones),
     1            (IdNumbers(2) ,IdOrMat),
     2            (IdNumbers(3) ,IdOmega),
     3            (IdNumbers(4) ,IdGMax),
     4            (IdNumbers(5) ,IdSGMaxM),
     3            (IdNumbers(6) ,IdSGMaxR),
     4            (IdNumbers(7) ,IdCSGMaxR),
     5            (IdNumbers(8) ,IdIntSteps),
     6            (IdNumbers(9) ,IdCalcDyn),
     7            (IdNumbers(10),IdCommands),
     8            (IdNumbers(11),IdUseWKS),
     9            (IdNumbers(12),IdAbsFlag),
     a            (IdNumbers(13),IdEnd),
     1            (IdNumbers(14),IdM42RefBlock),
     2            (IdNumbers(15),IdThreads),
     3            (IdNumbers(16),IdTiltcorr),
     4            (IdNumbers(17),IdGeometryIEDT),
     5            (IdNumbers(18),IdRescaleToFCalc)
      ln=NextLogicNumber()
      KPhaseIn=KPhase
      if(Klic.eq.0) then
        Veta='old'
      else
        Veta='unknown'
      endif
      call OpenFile(ln,FileName,'formatted',Veta)
      if(ErrFlag.ne.0) go to 9999
      if(Klic.eq.0) then
        call DeallocateEDZones
        if(Allocated(NEDZone))
     1    deallocate(NEDZone,EDIntSteps,OrMatEDZone,GMaxEDZone,
     2               SGMaxMEDZone,SGMaxREDZone,CSGMaxREDZone,OmEDZone,
     3               EDThreads,EDTiltcorr,EDGeometryIEDT)
        allocate(NEDZone(NRefBlock),EDIntSteps(NRefBlock),
     1           OrMatEDZone(3,3,NRefBlock),GMaxEDZone(NRefBlock),
     2           SGMaxMEDZone(NRefBlock),SGMaxREDZone(NRefBlock),
     3           CSGMaxREDZone(NRefBlock),OmEDZone(NRefBlock),
     4           EDThreads(NRefBlock),EDTiltcorr(NRefBlock),
     5           EDGeometryIEDT(NRefBlock))
        NRefBlockED=NRefBlock
        call SetBasicM42(0,0)
        KRefB=0
        k=len(Veta)
1100    if(k.ge.len(Veta)) then
          read(ln,FormA,end=9010) Veta
          k=0
        endif
        call kus(Veta,k,Nazev)
        j=islovo(Nazev,IdM42,18)
        if(j.eq.IdM42RefBlock) then
1200      KRefB=KRefB+1
          if(RadiationRefBlock(KRefB).ne.ElectronRadiation) then
            go to 1200
          else
            go to 1100
          endif
        else if(j.eq.IdCommands) then
          EDCommands=Veta(k+1:)
          k=len(Veta)
          go to 1100
        endif
        if(KRefB.le.0) then
          KB=1
        else
          KB=KRefB
        endif
        call StToReal(Veta,k,xp(1),1,.false.,ich)
        if(j.eq.IdNZones) then
          n=nint(xp(1))
          call SetBasicM42(-1,0)
          call ReallocateEDZones(max(n-NMaxEDZone,0))
          NEDZone(KB)=n
        else if(j.eq.IdIntSteps) then
          EDIntSteps(KB)=nint(xp(1))
        else if(j.eq.IdOmega) then
          OmEDZone(KB)=xp(1)
        else if(j.eq.IdGMax) then
          GMaxEDZone(KB)=xp(1)
        else if(j.eq.IdSGMaxM) then
          SGMaxMEDZone(KB)=xp(1)
        else if(j.eq.IdSGMaxR) then
          SGMaxREDZone(KB)=xp(1)
        else if(j.eq.IdCSGMaxR) then
          CSGMaxREDZone(KB)=xp(1)
        else if(j.eq.IdOrMat) then
          j=1
          do l=1,3
            read(ln,FormA) Veta
            k=0
            call StToReal(Veta,k,xp(j),3,.false.,ich)
            if(ich.ne.0) go to 9000
            j=j+3
          enddo
          call TrMat(xp,OrMatEDZone(1,1,KB),3,3)
          k=len(Veta)
        else if(j.eq.IdCalcDyn) then
          CalcDyn=xp(1).gt..5
        else if(j.eq.IdRescaleToFCalc) then
          write(Cislo,'(f5.2)') xp(1)
          RescaleToFCalc=xp(1).gt..5
        else if(j.eq.IdUseWKS) then
          UseWKS=xp(1).gt..5
        else if(j.eq.IdThreads) then
          EDThreads(KB)=nint(xp(1))
        else if(j.eq.IdTiltcorr) then
          EDTiltcorr(KB)=nint(xp(1))
        else if(j.eq.IdGeometryIEDT) then
          EDGeometryIEDT(KB)=nint(xp(1))
        else if(j.eq.IdAbsFlag) then
          AbsFlagED=nint(xp(1))
        else if(j.eq.IdEnd) then
          go to 2000
        endif
        go to 1100
      else
        k=0
        call WriteLabeledRecord(ln,IdM42(IdCommands),EDCommands,k)
        if(CalcDyn) then
          i=1
        else
          i=0
        endif
        if(UseWKS.and.CalcDyn) then
          j=1
        else
          j=0
        endif
        if(RescaleToFCalc.and.CalcDyn) then
          k=1
        else
          k=0
        endif
        write(Veta,'(i5,2(1x,a,i5))') i,IdM42(IdUseWKS),j,
     1                          IdM42(IdRescaleToFCalc),k
        call ZdrcniCisla(Veta,5)
        if(UseWKS) write(Veta(idel(Veta)+2:),'(1x,a,i15)')
     1                   IdM42(IdAbsFlag),AbsFlagED
        k=0
        call WriteLabeledRecord(ln,IdM42(IdCalcDyn),Veta,k)
        do KRefB=1,NRefBlock
          if(RadiationRefBlock(KRefB).ne.ElectronRadiation) cycle
          if(NRefBlock.gt.1) then
            write(Veta,FormA) RefBlockName(KRefB)
            k=0
            call WriteLabeledRecord(ln,IdM42(IdM42RefBlock),Veta,k)
          endif
          write(Veta,102) EDThreads(KRefB),IdM42(IdTiltcorr),
     1                    EDTiltcorr(KRefB),IdM42(IdGeometryIEDT),
     2                    EDGeometryIEDT(KRefB)
          k=0
          call WriteLabeledRecord(ln,IdM42(IdThreads),Veta,k)
          write(Veta,102) NEDZone(KRefB),IdM42(IdIntSteps),
     1                    EDIntSteps(KRefB)
          k=0
          call WriteLabeledRecord(ln,IdM42(IdNZones),Veta,k)
          write(ln,FormA) IdM42(IdOrMat)(:idel(IdM42(IdOrMat)))
          do i=1,3
            write(ln,101)(OrMatEDZone(i,j,KRefB),j=1,3)
          enddo
          write(Veta,100) OmEDZone(1),
     1                    IdM42(IdGMax),GMaxEDZone(KRefB),
     2                    IdM42(IdSGMaxM),SGMaxMEDZone(KRefB),
     3                    IdM42(IdSGMaxR),SGMaxREDZone(KRefB),
     4                    IdM42(IdCSGMaxR),CSGMaxREDZone(KRefB)
          k=0
          call WriteLabeledRecord(ln,IdM42(IdOmega),Veta,k)
        enddo
        write(ln,FormA) IdM42(IdEnd)(:idel(IdM42(IdEnd)))
      endif
2000  do kk=1,2
        if(Klic.eq.0) then
          if(kk.eq.2) read(ln,FormA,err=9000,end=9999)
        else
          if(kk.eq.1) then
            write(ln,'(79(''*''))')
          else
            write(ln,'(28(''-''),''   s.u. block   '',35(''-''))')
          endif
        endif
        do KRefB=1,NRefBlock
          if(RadiationRefBlock(KRefB).ne.ElectronRadiation) cycle
          k=0
          if(Klic.eq.0) then
            read(ln,FormA) Veta
            if(Veta(1:4).eq.'****') read(ln,FormA) Veta
            call kus(Veta,k,t80)
            if(.not.EqIgCase(t80,IdM42(IdM42RefBlock))) backspace ln
          else
            if(NRefBlock.gt.1) then
              write(Veta,FormA) RefBlockName(KRefB)
              call WriteLabeledRecord(ln,IdM42(IdM42RefBlock),Veta,k)
            endif
          endif
          do i=1,NEDZone(KRefB)
            if(Klic.eq.0) then
              read(ln,FormA) Veta
              if(Veta(1:4).eq.'****') read(ln,FormA) Veta
              if(kk.eq.1) then
                k=0
                call Kus(Veta,k,Cislo)
                call Kus(Veta,k,Cislo)
                call Kus(Veta,k,Cislo)
                if(k.lt.len(Veta)) then
                  call StToReal(Veta,k,RFacEDZone(i,KRefB),1,.false.,
     1                          ich)
                  if(ich.ne.0) go to 9000
                else
                  RFacEDZone(i,KRefB)=0.
                endif
              endif
            else
              if(kk.eq.1) then
                write(Veta,'(f15.4)') RFacEDZone(i,KRefB)
                call Zhusti(Veta)
              else
                Veta=' '
              endif
              write(Cislo,FormI15) i
              call Zhusti(Cislo)
              Veta='# Zone '//Cislo(:idel(Cislo)+1)//Veta(:idel(Veta))
              write(ln,FormA) Veta(:idel(Veta))
            endif
            if(kk.eq.1) then
              if(Klic.ne.0) then
                call CopyVek(HEDZone(1,i,KRefB),xp,3)
                xp(4)=AlphaEDZone(i,KRefB)
                xp(5)=BetaEDZone(i,KRefB)
                xp(6)=PrAngEDZone(i,KRefB)
              endif
              call IOEDZone(ln,xp,6,UseEDZone(i,KRefB),
     1                      NThickEDZone(i,KRefB),Klic)
              if(ErrFlag.ne.0) go to 9000
              if(Klic.eq.0) then
                call CopyVek(xp,HEDZone(1,i,KRefB),3)
                AlphaEDZone(i,KRefB)=xp(4)
                BetaEDZone(i,KRefB)=xp(5)
                PrAngEDZone(i,KRefB)=xp(6)
              endif
            endif
            kip=1
            n=4
            if(Klic.ne.0) then
              if(kk.eq.1) then
                xp(1)=ScEDZone(i,KRefB)
                xp(2)=ThickEDZone(i,KRefB)
                xp(3)=XNormEDZone(i,KRefB)
                xp(4)=YNormEDZone(i,KRefB)
              else
                xp(1)=ScEDZoneS(i,KRefB)
                xp(2)=ThickEDZoneS(i,KRefB)
                xp(3)=XNormEDZoneS(i,KRefB)
                xp(4)=YNormEDZoneS(i,KRefB)
              endif
            endif
            if(kk.eq.1) then
              call iost(ln,xp,KiED(kip,i,KRefB),n,Klic,0)
              if(ErrFlag.ne.0) go to 9000
            else
              call iosts(ln,xp,n,Klic)
            endif
            n=2
            if(Klic.eq.0) then
              if(kk.eq.1) then
                ScEDZone(i,KRefB)=xp(1)
                ThickEDZone(i,KRefB)=xp(2)
                XNormEDZone(i,KRefB)=xp(3)
                YNormEDZone(i,KRefB)=xp(4)
              else
                ScEDZoneS(i,KRefB)=xp(1)
                ThickEDZoneS(i,KRefB)=xp(2)
                XNormEDZoneS(i,KRefB)=xp(3)
                YNormEDZoneS(i,KRefB)=xp(4)
              endif
            endif
            kip=kip+4
            if(Klic.ne.0) then
              if(kk.eq.1) then
                xp(1)=PhiEDZone(i,KRefB)
                xp(2)=ThetaEDZone(i,KRefB)
              else
                xp(1)=PhiEDZoneS(i,KRefB)
                xp(2)=ThetaEDZoneS(i,KRefB)
              endif
            endif
            if(kk.eq.1) then
              call iost(ln,xp,KiED(kip,i,KRefB),n,Klic,0)
              if(ErrFlag.ne.0) go to 9000
            else
              call iosts(ln,xp,n,Klic)
            endif
            if(Klic.eq.0) then
              if(kk.eq.1) then
                PhiEDZone(i,KRefB)=xp(1)
                ThetaEDZone(i,KRefB)=xp(2)
              else
                PhiEDZoneS(i,KRefB)=xp(1)
                ThetaEDZoneS(i,KRefB)=xp(2)
              endif
            endif
            kip=kip+2
          enddo
        enddo
      enddo
      if(Klic.eq.1) write(ln,'(79(''-''))')
      go to 9999
9000  call FeReadError(ln)
      go to 9900
9010  Veta='Unexpected end of file'
      call FeChybne(-1.,-1.,'wrong record on M42 file',Veta,
     1              SeriousError)
      go to 9900
9900  ErrFlag=1
9999  call CloseIfOpened(ln)
      KPhase=KPhaseIn
      return
100   format(f12.6,5(1x,a8,f12.6))
101   format(3f12.6)
102   format(i5,5(1x,a8,i5))
      end
