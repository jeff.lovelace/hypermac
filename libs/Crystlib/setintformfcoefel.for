      subroutine SetIntFormFCoefEl
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension am(4,4),ami(4,4),b(4)
      do ia=1,NAtFormula(KPhase)
        do i=1,56
          if(i.eq.1) then
            jp=0
          else if(i.gt.52) then
            jp=52
          else
            jp=i-2
          endif
          pom=1./(ffx(jp+1)-ffx(jp+2))
          do j=1,4
            b(j)=FFEl(jp+j,ia,KPhase)-FFEl(jp+2,ia,KPhase)
            xp=(ffx(jp+j)-ffx(jp+2))*pom
            xs=1.
            do k=1,4
              am(j,k)=xs
              xs=xs*xp
            enddo
          enddo
          call matinv(am,ami,xp,4)
          call multm(ami,b,ffae(1,i,ia,KPhase),4,4,1)
          ffae(1,i,ia,KPhase)=ffae(1,i,ia,KPhase)+FFEl(jp+2,ia,KPhase)
        enddo
      enddo
      return
      end
