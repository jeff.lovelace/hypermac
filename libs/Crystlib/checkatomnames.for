      subroutine CheckAtomNames(MenitNazvy)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Text
      character*16 t16(3)
      character*2 Att
      data t16/'%Continue','C%hange+continue','%Return'/
      MenitNazvy=0
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase) cycle
        Att=AtType(isf(i),KPhase)
        j=idel(Att)
        if(index(Atom(i),Att(:j)).eq.1) then
          if(idel(Atom(i)).eq.j) cycle
          if(index(Cifry,Atom(i)(j+1:j+1)).le.0) go to 1500
        else
          go to 1500
        endif
      enddo
      go to 9999
1500  gap=10.
      dbutt=0.
      do i=1,3
        dbutt=dbutt+FeTxLength(t16(i))+20.
      enddo
      dbutt=dbutt+2.*gap
      call FeBoldFont
      Text='Atoms names don''t have form "atom_type"+number'
      xd=max(dbutt+10.,FeTxLength(Text)+10.)
      call FeNormalFont
      id=NextQuestid()
      il=1
      call FeQuestCreate(id,-1.,-1.,xd,il,Text,0,LightGray,-1,-1)
      pomx=(xd-dbutt)*.5
      do i=1,3
        w=FeTxLength(t16(i))+20.
        call FeQuestButtonMake(id,pomx,il,w,ButYd,t16(i))
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        pomx=pomx+w+gap
        if(i.eq.1) nButtFirst=ButtonLastMade
      enddo
      call FeQuestButtonActivate(nButtFirst+1)
2000  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        iout=CheckNumber-nButtFirst+1
        call FeQuestRemove(id)
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(iout.eq.2) then
        MenitNazvy= 1
      else if(iout.eq.3) then
        MenitNazvy=-1
      endif
      if(iout.eq.2.and.NDim(KPhase).le.3)
     1   call RenameAtomsAccordingToAtomType(NAtCalc)
9999  return
      end
