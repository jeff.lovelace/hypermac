      subroutine ReadSymm(Veta,rm6p,s6p,symmcp,zmagp,Tisk)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rm6p(*),s6p(*)
      character*80 t80
      character*(*) symmcp(*)
      character*(*) veta
      integer tisk
      logical xyz,EqIgCase
      call mala(Veta)
      xyz=index(Veta,'y').gt.0
      if(NDim(KPhase).gt.3.and.xyz) go to 9000
      call SetRealArrayTo(rm6p,NDimQ(KPhase),0.)
      k=0
      ij=0
      do 5000i=1,NDim(KPhase)
        ij=i-NDim(KPhase)
        call kus(veta,k,t80)
        idl=idel(t80)
        if(idl.le.0) go to 9000
        do j=1,idl
          if(t80(j:j).eq.'*') t80(j:j)=' '
        enddo
        call zhusti(t80)
        s6p(i)=0.
        do 2000j=1,NDim(KPhase)
          ij=ij+NDim(KPhase)
          if(xyz) then
            kk=index(t80,smbx(j))
          else
            kk=index(t80,smbx6(j))
          endif
          if(kk.eq.0) then
            rm6p(ij)=0.
            go to 2000
          endif
          if(kk.gt.1) then
            kp=kk-1
1100        if(t80(kp:kp).ne.'+'.and.t80(kp:kp).ne.'-') then
              if(kp.eq.1) go to 1150
              kp=kp-1
              go to 1100
            endif
          else
            kp=kk
          endif
1150      if(kp.ne.kk) then
            cislo=t80(kp:kk-1)
            if(cislo.eq.'+') then
              l=1
            else if(cislo.eq.'-') then
              l=-1
            else
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) l
            endif
          else
            l=1
          endif
          rm6p(ij)=l
          if(.not.xyz) kk=kk+1
          do l=kp,kk
            t80(l:l)=' '
          enddo
          call zhusti(t80)
          if(idel(t80).le.0) go to 5000
2000    continue
        s6p(i)=fract(t80,ich)
        if(index(t80,'.').gt.0) then
          call ToFract(s6p(i),Cislo,24)
          s6p(i)=fract(Cislo,ich)
        endif
        if(ich.ne.0) go to 9000
5000  continue
      if(MaxMagneticType.gt.0.and..not.ParentStructure) then
        call kus(Veta,k,t80)
        if(EqIgCase(t80,'m').or.EqIgCase(t80,'+m')) then
          zmagp= 1.
        else if(EqIgCase(t80,'-m')) then
          zmagp=-1.
        else
          zmagp= 1.
        endif
      else
        zmagp=1.
      endif
      call CodeSymm(rm6p,s6p,symmcp,0)
      go to 9999
9000  call FeChybne(-1.,-1.,'a syntactic error in the following '//
     1              'symmetry operator.',veta,SeriousError)
      ErrFlag=1
9999  return
      end
