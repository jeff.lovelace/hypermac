      subroutine atsym(slovo,k,xyz,x4dif,tdif,isym,ich)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xyz(3),xa(6),xp(6),xd(6),x4dif(3),tdif(3),ish(3),
     1          smp(36),rm6p(36),vt6p(6),s6p(6),xdd(6)
      character*(*) slovo
      character*80 symst
      character*8 at
      integer CrlCentroSymm
      klic=0
      go to 1000
      entry AtSymSpec(slovo,k,xyz,x4dif,tdif,isym,icentro,ish,ich)
      call SetIntArrayTo(ish,3,0)
      icentro=1
      klic=1
1000  ich=0
      n=idel(slovo)
      l=index(slovo,'#')
      if(l.le.0) l=n+1
      at=slovo(:l-1)
      k=ktat(atom,NAtAll,at)
      if(k.le.0) then
        ich=2
        go to 9999
      endif
      if(l.eq.n+1) then
        isym=1
        call CopyVek(x(1,k),xyz,3)
        call SetRealArrayTo(x4dif,3,0.)
        call SetRealArrayTo( tdif,3,0.)
      else
        symst=slovo(l+1:n)
1010    i=index(symst,',')
        if(i.ne.0) then
          symst(i:i)=' '
          go to 1010
        endif
        n=n-l
        call mala(symst)
        m=0
        is=index(symst,'s')
        if(is.gt.0) then
          symst(is:is)=' '
          m=m+1
        endif
        ic=index(symst,'c')
        if(ic.gt.0) then
          symst(ic:ic)=' '
          m=m+1
        endif
        it=index(symst,'t')
        if(it.gt.0) then
          symst(it:it)=' '
          m=m+1
        endif
        if(m.le.0) go to 8000
        if(is.gt.0) then
          j=is
          call kus(symst,j,cislo)
          call posun(cislo,0)
          read(cislo,FormI15,err=8000) isym
          if(isym.lt.0) then
            ip=CrlCentroSymm()
            if(ip.gt.0) then
              isym=ip+iabs(isym)-1
            else
              isym=1
            endif
          endif
        else
          isym=1
        endif
        if(ic.gt.0) then
          j=ic
          call kus(symst,j,cislo)
          call posun(cislo,0)
          read(cislo,FormI15,err=8000) icentr
        else
          icentr=1
        endif
        if(is.le.0) then
          call CopyVek(x(1,k),xyz,3)
          call SetRealArrayTo(x4dif,3,0.)
          call SetRealArrayTo( tdif,3,0.)
          call SetRealArrayTo(xd,6,0.)
          call SetRealArrayTo(xdd,6,0.)
          zn=1.
          isw=iswa(k)
          call CopyVek(vt6(1,icentr,isw,KPhase),vt6p,NDim(KPhase))
        else
          if(isym.gt.NSymm(KPhase).or.icentr.gt.NLattVec(KPhase))
     1      go to 8000
          isw=ISwSymm(isym,iswa(k),kswa(k))
          if(isw.eq.iswa(k)) then
            call CopyVek(rm6(1,iabs(isym),isw,KPhase),rm6p,
     1                   NDimQ(kswa(k)))
            call CopyVek(s6 (1,iabs(isym),isw,KPhase),s6p,NDim(kswa(k)))
            call CopyVek(vt6(1,icentr,isw,KPhase),vt6p,NDim(kswa(k)))
          else
            call multm(zv(1,isw,KPhase),zvi(1,iswa(k),KPhase),smp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,rm6(1,iabs(isym),iswa(k),KPhase),rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,s6(1,iabs(isym),iswa(k),KPhase),s6p,
     1                 NDim(KPhase),NDim(KPhase),1)
            call multm(smp,vt6(1,icentr,iswa(k),KPhase),vt6p,
     1                 NDim(KPhase),NDim(KPhase),1)
          endif
          if(isym.eq.0.or.iabs(isym).gt.NSymm(KPhase)) go to 8000
          do i=1,NDim(KPhase)
            if(i.le.3) then
              xp(i)=x(i,k)
            else
              xp(i)=0.
            endif
          enddo
          call multm(rm6p,xp,xa,NDim(KPhase),NDim(KPhase),1)
          zn=sign(1.,float(isym))
          do i=1,NDim(KPhase)
            xd (i)=s6p(i)
            xdd(i)=s6p(i)
            if(i.le.3) then
              xyz(i)=zn*xa(i)+xd(i)
            else
              xd(i)=xd(i)+xa(i)
            endif
          enddo
        endif
        if(ic.gt.0) then
          j=ic
          call kus(symst,j,cislo)
          call posun(cislo,0)
          read(cislo,FormI15,err=8000) icentr
          if(icentr.lt.1.or.icentr.gt.NLattVec(KPhase)) go to 8000
          if(icentr.ne.1) then
            do i=1,NDim(KPhase)
              xd(i)=xd(i)+vt6p(i)
              if(i.le.3) xyz(i)=xyz(i)+vt6(i,icentr,isw,KPhase)
            enddo
          endif
          if(klic.eq.1) icentro=icentr
        endif
        if(it.gt.0) then
          j=it
          do i=1,3
            call kus(symst,j,cislo)
            call posun(cislo,0)
            read(cislo,FormI15,err=8000) l
            if(klic.eq.1) ish(i)=l
            xd(i)=xd(i)+float(l)
            xdd(i)=xdd(i)+float(l)
            xyz(i)=xyz(i)+float(l)
          enddo
        endif
        if(NDim(KPhase).gt.3) then
          call qbyx(xdd,tdif,isw)
          do i=1,NDimI(KPhase)
            x4dif(i)=-xd(i+3)
            tdif(i)=tdif(i)-xdd(i+3)
          enddo
        endif
      endif
      go to 9999
8000  ich=3
9999  return
      end
