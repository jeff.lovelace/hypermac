      subroutine CrlCheckForAlternative(kosy,kmirr,Grp,KCentroSymm,isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kosy(3),kmirr(3),s6p(6),io(3,3)
      character*(*) Grp
      character*256 Veta
      character*60  GrpPom,GrpMin
      character*8  GrupaI
      character*3 t3(3)
      character*2 t2m(3)
      logical EqIgCase
      ln=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1        'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      if(CrSystem(KPhase).eq.CrSystemOrthorhombic.and.
     1   NLattVec(KPhase).le.4) then
        GrpPom(1:1)='P'
        nvtp=NLattVec(KPhase)
        NLattVec(KPhase)=1
        nlmin=99999
        ip=index(Grp,'(')
        ik=index(Grp,')')
        do i1=1,nvtp
          j1=kmirr(1)
          if(j1.le.0) j1=kosy(1)
          call AddVek(s6(1,j1,isw,KPhase),vt6(1,i1,isw,KPhase),s6p,
     1                NDim(KPhase))
          call od0do1(s6p,s6p,NDim(KPhase))
          call SmbOp(rm6(1,j1,isw,KPhase),s6p,isw,1.,t3(1),t2m(1),
     1               io(1,1),nn,pom)
          if(t3(1)(1:1).eq.'2'.and.KCentroSymm.gt.0)
     1      call SmbOp(rm6(1,j1,isw,KPhase),s6p,isw,-1.,t3(1),t2m(1),
     2                 io(1,1),nn,pom)
          do i2=1,nvtp
            j2=kmirr(2)
            if(j2.le.0) j2=kosy(2)
            call AddVek(s6(1,j2,isw,KPhase),vt6(1,i2,isw,KPhase),s6p,
     1                  NDim(KPhase))
            call od0do1(s6p,s6p,NDim(KPhase))
            call SmbOp(rm6(1,j2,isw,KPhase),s6p,isw,1.,t3(2),t2m(2),
     1                 io(1,2),nn,pom)
            if(t3(2)(1:1).eq.'2'.and.KCentroSymm.gt.0)
     1        call SmbOp(rm6(1,j2,isw,KPhase),s6p,isw,-1.,t3(2),t2m(2),
     2                   io(1,2),nn,pom)
            do 1100i3=1,nvtp
              j3=kmirr(3)
              if(j3.le.0) j3=kosy(3)
              call AddVek(s6(1,j3,isw,KPhase),vt6(1,i3,isw,KPhase),s6p,
     1                    NDim(KPhase))
              call od0do1(s6p,s6p,NDim(KPhase))
              call SmbOp(rm6(1,j3,isw,KPhase),s6p,isw,1.,t3(3),t2m(3),
     1                   io(1,3),nn,pom)
              if(t3(3)(1:1).eq.'2'.and.KCentroSymm.gt.0)
     1           call SmbOp(rm6(1,j3,isw,KPhase),s6p,isw,-1.,t3(3),
     2                      t2m(3),io(1,3),nn,pom)
              GrpPom='P'//t3(1)//t3(2)//t3(3)
              call Zhusti(GrpPom)
              rewind ln
              read(ln,FormA) Veta
              if(Veta(1:1).ne.'#') rewind ln
              nl=0
2000          read(ln,'(3i3,1x,a8)',end=1100) igi,ipgi,idli,GrupaI
              nl=nl+1
              if(EqIgCase(GrupaI,GrpPom).and.nl.lt.nlmin) then
                if(NDim(KPhase).eq.4) then
                  GrpMin=Grp(1:1)//GrpPom(2:idel(GrpPom))//Grp(ip:ik)//
     1                   t2m(1)(:idel(t2m(1)))//
     2                   t2m(2)(:idel(t2m(2)))//
     3                   t2m(3)(:idel(t2m(3)))
                else
                  GrpMin=Grp(1:1)//GrpPom(2:idel(GrpPom))
                endif
                nlmin=nl
                go to 1100
              endif
              go to 2000
1100        continue
          enddo
        enddo
        if(nlmin.lt.99000) Grp=GrpMin
        NLattVec(KPhase)=nvtp
      endif
9999  call CloseIfOpened(ln)
      return
      end
