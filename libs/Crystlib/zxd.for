      subroutine ZXD
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'datred.cmn'
      dimension  xp(64),ksf(:),XDKap(:,:),iar(18)
      character*256 FileNames(0:2),EdwStringQuest,Veta,p256,t256
      character*8 Nazev
      character*7 At
      character*5 Ext(0:2)
      character*4 WFLabel(3)
      logical ExistFile,EqIgCase,CteI,StructureExists,FeYesNo
      allocatable ksf,XDKap
      data Ext/'*.mas','*.inp','*.hkl'/,
     1     FileNames/'xd.mas','xd.inp','xd.hkl'/
      allocate(ksf(100),XDKap(6,100))
      ln=0
      lno=0
      if(StructureExists(fln)) then
        if(.not.FeYesNo(-1.,-1.,'The structure "'//fln(:ifln)//
     1                  '" already exists, rewrite it?',0)) go to 9999
      endif
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,3,'Specify XD files',1,
     1                   LightGray,0,0)
      il=1
      tpom=5.
      xpom=100.
      dpom=120.
      il=0
      do i=0,2
        il=il+1
        if(i.eq.0) then
          Veta='XD %master file'
        else if(i.eq.1) then
          Veta='XD %parameter file'
        else if(i.eq.2) then
          Veta='XD %HKL file'
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.0) nEdwFileName=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,FileNames(i))
      enddo
      Veta='Browse'
      tpom=xpom+dpom+15.
      dpom=xqd-tpom-5.
      do il=1,3
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        if(il.eq.1) nButtBrowse=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        j=0
        do i=nEdwFileName,nEdwFileName+2
          Veta=EdwStringQuest(i)
          if(.not.ExistFile(Veta)) then
            if(i.ne.nEdwFileName+2) then
              call FeChybne(-1.,-1.,'file "'//Veta(:idel(Veta))//
     1          '" doesn''t exist, please try again',' ',SeriousError)
              EventType=EventEdw
              EventNumber=i
              call FeQuestButtonOff(ButtonOK-ButtonFr+1)
              go to 1500
            else
               Veta=' '
            endif
          endif
          FileNames(j)=Veta
          j=j+1
        enddo
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventButton) then
        nb=nButtBrowse
        j=CheckNumber-nButtBrowse
        i=nEdwFileName+j
        Veta=EdwStringQuest(i)
        call FeFileManager('Browse',Veta,Ext(j),0,.true.,ich)
        call FeQuestButtonOff(nb)
        if(ich.eq.0) then
          call FeQuestStringEdwOpen(i,Veta)
          EventType=EventEdw
          EventNumber=i
          go to 1500
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      lno=0
      call SetBasicM40(.true.)
      call SetBasicM50
      lite(KPhase)=0
      irot(KPhase)=1
      nac=0
      nor=0
      NSymm(KPhase)=1
      NSymmN(KPhase)=1
      MaxNSymm=1
      NCSymm(KPhase)=2
      call AllocateSymm(96,96,1,1)
      if(allocated(AtType))
     1  deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2             AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3             FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4             TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
      if(allocated(TypeCore))
     1  deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2             HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3             CoreValSource)
      n=20
      m=121
      i=1
      allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1         AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2         AtWeight(n,NPhase),AtRadius(n,NPhase),AtColor(n,NPhase),
     3         AtNum(n,NPhase),AtVal(n,NPhase),AtMult(n,NPhase),
     4         FFBasic(m,n,NPhase),
     5         FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6         FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7         FFra(n,NPhase,i),FFia(n,NPhase,i),
     8         FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9         TypeFFMag(n,NPhase),
     a         FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1         FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
      allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1         PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2         ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3         HNSlater(n,NPhase),HZSlater(n,NPhase),
     4         ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5         NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6         CoreValSource(n,NPhase))
      FFCoreD=0.
      FFValD=0.
      CoreValSource='Default'
      NAtFormula(KPhase)=0
      ntab=40
      ndum=0
      ChargeDensities=.true.
      ExistXRayData=.true.
      call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
      call UnitMat(rm (1,1,1,KPhase),3)
      call SetRealArrayTo(s6(1,1,1,KPhase),6,0.)
      call SetRealArrayTo(sc(1,1),6,0.)
      ISwSymm(1,1,KPhase)=1
      ln=NextLogicNumber()
      call OpenFile(ln,FileNames(0),'formatted','old')
      if(ich.ne.0) go to 9999
2000  call ReadXDRecord(ln,t256,iend)
      if(iend.ne.0) go to 3000
      k=0
      call kus(t256,k,Nazev)
      if(Nazev.eq.'titl') then
        StructureName=t256(k:)
      else if(Nazev.eq.'cell') then
        call StToReal(t256,k,CellPar,6,.false.,ich)
        if(ich.ne.0) go to 9999
        call setmet(0)
      else if(Nazev.eq.'wave') then
        call StToReal(t256,k,LamAve,1,.false.,ich)
        if(ich.ne.0) go to 9999
        j=LocateInArray(LamAve(1),LamAveD,7,.0001)
        KLam(1)=j
        if(j.eq.0) then
          LamA1(1)=LamAve(1)
          LamA2(1)=LamAve(1)
          LamRat(1)=0.
          NAlfa(1)=1
        else
          LamA1(1)=LamA1D(j)
          LamA2(1)=LamA2D(j)
          LamRat(1)=LamRatD(j)
          NAlfa(1)=2
        endif
      else if(Nazev.eq.'latt') then
        if(k.le.79) then
          call kus(t256,k,Nazev)
          if(Nazev.eq.'a') then
            NCSymm(KPhase)=1
          else
            NCSymm(KPhase)=2
          endif
          if(k.le.79) call kus(t256,k,Lattice(KPhase))
        endif
      else if(Nazev.eq.'symm') then
        do i=1,3
          if(index(t256(k+1:),smbx(i)).gt.0) go to 2200
        enddo
        call StToReal(t256,k,xp,12,.false.,ich)
        NSymm(KPhase)=NSymm(KPhase)+1
        NSymmN(KPhase)=NSymmN(KPhase)+1
        MaxNSymm=NSymm(KPhase)
        call ReallocSymm(NDim(KPhase),NSymm(KPhase),
     1                   NLattVec(KPhase),NComp(KPhase),NPhase,0)
        i=1
        do k=1,12,4
          s6(i,NSymm(KPhase),1,KPhase)=xp(k)
          do j=1,3
            rm6(i+(j-1)*NDim(KPhase),NSymm(KPhase),1,KPhase)=xp(k+j)
          enddo
          i=i+1
        enddo
        go to 2220
2200    if(t256.eq.' ') go to 2220
        t256=t256(k+1:)
        k=index(t256,';')
        if(k.gt.0) then
          p256=t256(:k-1)
          k=k+1
        else
          p256=t256
          t256=' '
        endif
        if(p256.ne.' ') then
          if(index(p256,',').gt.0) call Zhusti(p256)
          do i=1,idel(p256)
            if(p256(i:i).eq.',') p256(i:i)=' '
          enddo
          call FeDelTwoSpaces(p256)
          if(p256.eq.'x y z') go to 2200
          NSymm(KPhase)=NSymm(KPhase)+1
          NSymmN(KPhase)=NSymmN(KPhase)+1
          MaxNSymm=NSymm(KPhase)
          call ReadSymm(p256,rm6(1,NSymm(KPhase),1,KPhase),
     1      s6(1,NSymm(KPhase),1,KPhase),symmc(1,NSymm(KPhase),1,KPhase)
     2     ,pom,0)
          go to 2200
        endif
2220    call CodeSymm(rm6(1,NSymm(KPhase),1,KPhase),
     1                s6(1,NSymm(KPhase),1,KPhase),
     2                symmc(1,NSymm(KPhase),1,KPhase),0)
        call MatBlock3(rm6(1,NSymm(KPhase),1,KPhase),
     1                 rm(1,NSymm(KPhase),1,KPhase),NDim(KPhase))
        ISwSymm(NSymm(KPhase),1,KPhase)=1
      else if(Nazev.eq.'scat') then
        NAtFormula(KPhase)=0
2300    call ReadXDRecord(ln,t256,iend)
        if(iend.ne.0) go to 9000
        k=0
        call kus(t256,k,At)
        i=ichar(At(1:1))-48
        if(i.ge.0.and.i.le.9) go to 2300
        if(At.eq.'end') go to 2000
        NAtFormula(KPhase)=NAtFormula(KPhase)+1
        FFType(KPhase)=40
        call UprAt(At)
        AtTypeFull(NAtFormula(KPhase),KPhase)=At
        AtTypeMag(NAtFormula(KPhase),KPhase)=' '
        AtTypeMagJ(NAtFormula(KPhase),KPhase)=' '
        AtType(NAtFormula(KPhase),KPhase)=At
        AtMult(NAtFormula(KPhase),KPhase)=0.
        call EM50ReadOneFormFactor(NAtFormula(KPhase))
        call RealAFromAtomFile(AtTypeFull(NAtFormula(KPhase),KPhase),
     1                         'ZSlater',
     2                         ZSlater(1,NAtFormula(KPhase),KPhase),
     3                                  8,ich)
        call IntAFromAtomFile(AtTypeFull(NAtFormula(KPhase),KPhase),
     1                        'NSlater',
     2                        NSlater(1,NAtFormula(KPhase),KPhase),
     3                        8,ich)
        if(ich.ne.0) go to 9100
        do i=1,3
          call kus(t256,k,WFLabel(i))
        enddo
        call SetIntArrayTo(PopCore(1,NAtFormula(KPhase),KPhase),28,0)
        call SetIntArrayTo(PopVal (1,NAtFormula(KPhase),KPhase),28,0)
        call StToInt(t256,k,iar,18,.false.,ich)
        if(ich.ne.0) go to 9000
        i=1
        do l=0,4
          j=l+1
          do m=0,l
            if(j.gt.10.or.i.gt.10) go to 2315
            n=iar(j)
            if(n.gt.0) then
              PopCore(i,NAtFormula(KPhase),KPhase)=n
            else
              PopVal(i,NAtFormula(KPhase),KPhase)=-n
            endif
2315        i=i+1
            j=j+3-m
          enddo
        enddo
        do i=11,22
          if(i.eq.11) then
            j=11
          else if(i.eq.12) then
            j=12
          else if(i.eq.13) then
            j=15
          else if(i.eq.14) then
            j=18
          else if(i.eq.16) then
            j=13
          else if(i.eq.17) then
            j=14
          else if(i.eq.22) then
            j=17
          else
            cycle
          endif
          n=iar(j)
          if(n.gt.0) then
            PopCore(i,NAtFormula(KPhase),KPhase)=n
          else
            PopVal(i,NAtFormula(KPhase),KPhase)=-n
          endif
        enddo
        call StToReal(t256,k,xp,3,.false.,ich)
        if(ich.ne.0) go to 9000
        call EM50OneFormFactorSet(NAtFormula(KPhase))
        do j=1,2
          if(WFLabel(j).eq.'rdtb') then
            n=40
            i=1
2354        read(ln,FormA,end=2358) p256
            if(p256.eq.' ') then
              if(n.eq.40) then
                go to 2354
              else
                go to 2358
              endif
            endif
            k=0
            call StToReal(p256,k,xp(i),n-i+1,.false.,ich)
            if(ich.eq.1) then
              go to 9000
            else if(ich.ge.2) then
              i=i+ich-2
              go to 2354
            endif
2358        if(j.eq.1) then
              call CopyVek(xp,FFCore(1,NAtFormula(KPhase),KPhase),n)
            else
              call CopyVek(xp,FFVal (1,NAtFormula(KPhase),KPhase),n)
            endif
          endif
        enddo
        call SetCoreVal(NAtFormula(KPhase))
        if(WFLabel(3).eq.'rdsd') then
          call ReadXDRecord(ln,t256,iend)
          if(iend.ne.0) go to 9000
          k=0
          call StToReal(t256,k,xp,10,.false.,ich)
          if(ich.ne.0) go to 9000
          j=0
          call SetIntArrayTo(NSlater(1,NAtFormula(KPhase),KPhase),8,0)
          call SetRealArrayTo(ZSlater(1,NAtFormula(KPhase),KPhase),8,
     1                        0.)
          do i=1,5
            j=j+1
            NSlater(i,NAtFormula(KPhase),KPhase)=nint(xp(j))
            j=j+1
            ZSlater(i,NAtFormula(KPhase),KPhase)=xp(j)
          enddo
        else if(WFLabel(3).eq.'cszd') then
!          call SetIntArrayTo(NSlater(1,NAtFormula(KPhase),KPhase),8,0)
!          call SetRealArrayTo(ZSlater(1,NAtFormula(KPhase),KPhase),8,
!     1                        0.)
        endif
        go to 2300
      else if(Nazev.eq.'atom'.and.nac.le.0) then
        nac=0
        call AllocateAtoms(1000)
2400    call ReadXDRecord(ln,t256,iend)
        if(iend.ne.0) go to 9000
        call mala(t256)
        k=0
        call kus(t256,k,At)
        if(At.eq.'end') go to 2000
        nac=nac+1
        if(nac.gt.MxAtInd) call ReallocateAtoms(1000)
        call UprAt(At)
        Atom(nac)=At
        iswa(nac)=1
        kswa(nac)=1
        ifr(nac)=0
        call SetIntArrayTo(KModA(1,nac),7,0)
        call SetIntArrayTo(KModAO(1,nac),7,0)
        call SetIntArrayTo(KFA(1,nac),7,0)
        MagPar(nac)=0
        call SetRealArrayTo(sbeta(1,nac),6,0.)
        call SetRealArrayTo(sx(1,nac),3,0.)
        call SetRealArrayTo(spopas(1,nac),64,0.)
        spopc(nac)=0.
        spopv(nac)=0.
        if(At(1:3).ne.'Dum') then
          do i=1,NAtFormula(KPhase)
            id=index(At,'(')
            if(id.le.0) id=idel(At)
            if(EqIgCase(At(:id),AtType(i,KPhase))) then
              isf(nac)=i
              go to 2420
            endif
          enddo
          isf(nac)=0
2420      do i=1,2
            if(i.eq.2) then
              call kus(t256,k,At)
              call UprAt(At)
            endif
            call kus(t256,k,LocAtSystSt(i,nac))
            call UprAt(LocAtSystSt(i,nac))
            call kus(t256,k,Nazev)
            LocAtSystAx(nac)(i:i)=Nazev(1:1)
          enddo
          if(.not.EqIgCase(At,Atom(nac))) then
            LocAtSystSt(2,nac)=
     1        LocAtSystSt(2,nac)(:idel(LocAtSystSt(2,nac)))//' - '//
     2        At(:idel(At))
          endif
          LocAtSense(nac)=' '
          do i=1,2
            LocAtSystSt(i,nac)=' '//
     1        LocAtSystSt(i,nac)(:idel(LocAtSystSt(i,nac)))
          enddo
          call kus(t256,k,Nazev)
          call StToInt(t256,k,iar,4,.false.,ich)
          if(ich.ne.0) go to 9000
          itf(nac)=iar(1)
          isf(nac)=iar(2)
          ksf(nac)=iar(3)
          lasmaxi=iar(4)+2
        else
          ndum=ndum+1
          itf(nac)=1
          isf(nac)=1
          ksf(nac)=1
          iswa(nac)=1
          kswa(nac)=1
          lasmaxi=0
          ai(nac)=0.
          sai(nac)=0.
          call StToReal(t256,k,x(1,nac),3,.false.,ich)
          call SetRealArrayTo(beta(1,nac),6,0.)
          if(ich.ne.0) go to 9000
        endif
        if(nac.gt.1) then
          PrvniKiAtomu(nac)=PrvniKiAtomu(nac-1)+mxda
        else
          PrvniKiAtomu(nac)=ndoff+1
        endif
        DelkaKiAtomu(nac)=max(TRankCumul(itf(nac)),10)
        if(lasmaxi.ne.0) then
          DelkaKiAtomu(nac)=DelkaKiAtomu(nac)+3
          if(lasmaxi.gt.1) DelkaKiAtomu(nac)=DelkaKiAtomu(nac)+1+
     1                                       (lasmaxi-1)**2
        endif
        lasmax(nac)=lasmaxi
        call SetIntArrayTo(KiA(1,nac),mxda,0)
        go to 2400
      else if(Nazev.eq.'key') then
2500    call ReadXDRecord(ln,t256,iend)
        if(iend.ne.0) go to 9000
        k=0
        call kus(t256,k,At)
        if(At.eq.'end') go to 2000
        call UprAt(At)
        ia=ktat(Atom,nac,At)
        if(ia.le.0) go to 2500
        i=0
        n=3
        nk=1
2520    call kus(t256,k,p256)
        i=i+1
        np=nk+1
        nk=nk+n
        if(n.gt.0) then
          read(p256,101)(KiA(j,ia),j=np,nk)
          if(i.eq.5) then
            nk=nk+3
          else if(i.eq.6) then
            j=KiA(np+2,ia)
            KiA(np+2,ia)=KiA(np+1,ia)
            KiA(np+1,ia)=KiA(np,  ia)
            KiA(np,  ia)=j
          endif
        endif
        n=0
        if(i.eq.1) then
          n=6
        else if(i.eq.2) then
          if(itf(ia).gt.2) n=10
        else if(i.eq.3) then
          if(itf(ia).gt.2) n=15
        else if(i.eq.4) then
          if(lasmax(ia).gt.0) then
            n=1
            nk=nk+1
          endif
        else if(i.eq.5) then
          if(lasmax(ia).gt.1) n=3
        else if(i.eq.6) then
          if(lasmax(ia).gt.2) n=5
        else if(i.eq.7) then
          if(lasmax(ia).gt.3) n=7
        else if(i.eq.8) then
          if(lasmax(ia).gt.4) n=9
        else if(i.eq.9) then
          if(lasmax(ia).gt.5) n=11
        else
          go to 2500
        endif
        go to 2520
      endif
      go to 2000
3000  close(ln)
      if(KPhase.ne.1) then
        nn=NMolecToAll(KPhase-1)
      else
        nn=0
      endif
      do isw=1,3
        NMolecFr(isw,KPhase)=nn+1
        NMolecTo(isw,KPhase)=nn
        NMolecLen(isw,KPhase)=0
      enddo
      NMolecFrAll(KPhase)=nn+1
      NMolecToAll(KPhase)=nn
      NMolecLenAll(KPhase)=0
      NAtIndLen(1,KPhase)=nac
      call EM40UpdateAtomLimits
      ln=NextLogicNumber()
      call OpenFile(ln,FileNames(1),'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      iver=1
      call ReadXDRecord(ln,t256,iend)
      k=0
      call kus(t256,k,Veta)
      if(Veta.eq.'xdparfile') then
        call kus(t256,k,Veta)
        call StToInt(t256,k,iar,1,.false.,ich)
        iver=iar(1)
      endif
3010  call ReadXDRecord(ln,t256,iend)
      if(iend.ne.0) go to 5000
      k=0
      call kus(t256,k,Veta)
      if(Veta.eq.'limits') then
        go to 3010
      else if(Veta.eq.'usage') then
        p256=t256(k+1:)
      else
        if(iver.eq.1) then
          p256=t256
        else if(iver.eq.2) then
          go to 3010
        endif
      endif
      k=0
      call StToInt(p256,k,iar,14,.false.,ich)
      if(ich.ne.0) go to 9000
      if(iver.eq.1) then
        nkap=iar(12)
        nq=iar(14)
      else
        nkap=iar(4)
        nq=iar(6)
      endif
      do i=1,ndum+2
        call ReadXDRecord(ln,t256,iend)
        if(iend.ne.0) go to 5000
      enddo
      nap=0
3100  call ReadXDRecord(ln,t256,iend)
      if(iend.ne.0) go to 5000
      k=0
      call kus(t256,k,Nazev)
      ia=ktat(Atom,nac,Nazev)
      nap=nap+1
      call UprAt(Nazev)
      if(ia.le.0) then
        call FeChybne(-1.,-1.,'atom "'//Nazev(:idel(Nazev))//
     1    '" present only on parameter file.',' ',SeriousError)
        go to 9999
      endif
      call StToInt(t256,k,iar,11,.false.,ich)
      if(ich.ne.0) go to 9000
      i=max(iar(10),1)
      SmbPGAt(ia)=SmbPGI(i)
      NPGAt(ia)=1
      call UnitMat(RPGAt(1,1,ia),3)
      call StToReal(t256,k,xp,4,.false.,ich)
      if(ich.ne.0) go to 9000
      call CopyVek(xp,x(1,ia),3)
      ai(ia)=xp(4)
      sai(ia)=0.
      isfi=isf(ia)
      AtMult(isfi,KPhase)=AtMult(isfi,KPhase)+ai(ia)
      call ReadXDRecord(ln,t256,iend)
      if(iend.ne.0) go to 5000
      k=0
      call StToReal(t256,k,beta(1,ia),6,.false.,ich)
      if(ich.ne.0) go to 9000
      if(itf(ia).ge.2) then
        do i=1,6
          beta(i,ia)=urcp(i,1,KPhase)*beta(i,ia)
        enddo
      else
        beta(1,ia)=beta(1,ia)*episq
      endif
      n=26
      i=1
      call SetRealArrayTo(xp,64,0.)
3200  call ReadXDRecord(ln,t256,iend)
      if(iend.ne.0) go to 5000
      k=0
      call Kus(t256,k,Veta)
      if(index(Veta,'.').le.0) go to 3250
      k=0
      call StToReal(t256,k,xp(i),n-i+1,.false.,ich)
      if(ich.eq.1) then
        go to 3250
      else if(ich.ge.2) then
        i=i+ich-2
        go to 3200
      endif
      go to 3260
3250  backspace ln
      n=i-1
3260  do i=1,n
        xp(i)=xp(i)/ai(ia)
      enddo
      popv(ia)=xp(1)
      popc(ia)=nint(FFCore(1,isf(ia),KPhase))
      call CopyVek(xp(2),popas(1,ia),n-1)
      popas(2,ia)=xp(5)
      popas(3,ia)=xp(3)
      popas(4,ia)=xp(4)
      j=0
      k=DelkaKiAtomu(ia)-(lasmax(ia)-1)**2
      do i=1,(lasmax(ia)-1)**2
        k=k+1
        if(abs(popas(i,ia)).gt.0..or.KiA(k,ia).ne.0) j=i
      enddo
      do i=0,8
        if(j.le.i**2) then
          lasmaxi=i+1
          go to 3430
        endif
      enddo
3430  call ShiftKiAt(ia,itf(ia),ifr(ia),lasmaxi,KModA,MagPar(ia),
     1               .false.)
      lasmax(ia)=lasmaxi
      if(nap.lt.nac-ndum) go to 3100
      do i=1,nkap
        call ReadXDRecord(ln,t256,iend)
        if(iend.ne.0) go to 5000
        k=0
        call kus(t256,k,Cislo)
        call StToReal(t256,k,XDKap(1,i),6,.false.,ich)
        if(ich.ne.0) go to 9000
      enddo
      do i=1,nac
        k=ksf(i)
        kapa1(i)=XDKap(1,k)
        skapa1(i)=0.
        kapa2(i)=XDKap(2,k)
        skapa2(i)=0.
      enddo
5000  NAtInd=nac
      NAtAll=nac
      if(KPhase.ne.1) then
        nn=NMolecToAll(KPhase-1)
      else
        nn=0
      endif
      do isw=1,3
        NMolecFr(isw,KPhase)=nn+1
        NMolecTo(isw,KPhase)=nn
        NMolecLen(isw,KPhase)=0
      enddo
      NMolecFrAll(KPhase)=nn+1
      NMolecToAll(KPhase)=nn
      NMolecLenAll(KPhase)=0
      NAtIndLen(1,KPhase)=nac
      call EM40UpdateAtomLimits
      do i=1,3
        call ReadXDRecord(ln,t256,iend)
        if(iend.ne.0) go to 5150
      enddo
      k=0
      call StToReal(t256,k,sc,nq,.false.,ich)
      if(ich.ne.0) go to 9000
      do i=1,nq
        sc(i,1)=sc(i,1)*10.
      enddo
5150  close(ln)
      call CtiLatt(0)
      if(NCSymm(KPhase).eq.2) then
        call CrlExpandCentroSym
        deallocate(isa)
        i=ubound(isf,1)
        allocate(isa(NSymm(KPhase),i))
        do i=1,nac
          do j=1,NSymm(KPhase)
            isa(j,i)=j
          enddo
        enddo
        NCSymm(KPhase)=1
      endif
      call FindSmbSg(Grupa(KPhase),ChangeOrderNo,1)
      call SetFormula(Formula(KPhase))
      NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)*NCSymm(KPhase)
      if(FileNames(2).eq.' ') go to 6000
      KRefBlock=1
      NRefBlock=1
      call SetBasicM95(KRefBlock)
      SourceFileRefBlock(KRefBlock)=FileNames(2)
      call FeGetFileTime(SourceFileRefBlock(KRefBlock),
     1                   SourceFileDateRefBlock(KRefBlock),
     2                   SourceFileTimeRefBlock(KRefBlock))
      ln=NextLogicNumber()
      call OpenFile(ln,FileNames(2),'formatted','old')
      read(ln,FormA,end=5600) t256
      call Mala(t256)
      k=0
      call Kus(t256,k,Cislo)
      call Kus(t256,k,Cislo)
      CteI=Cislo.eq.'f^2'.or.Cislo.eq.'f2'
      ndat=6
      call Kus(t256,k,Cislo)
      if(Cislo.eq.'ndat') then
        call StToInt(t256,k,iar,1,.false.,ich)
        ndat=iar(1)
      endif
      DifCode(KRefBlock)=IdImportXD
      call UnitMat(TrMP,3)
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,0),6)
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,KRefBlock),6)
      CellReadIn(KRefBlock)=.true.
      CorrLp(KRefBlock)=-1
      CorrAbs(KRefBlock)=-1
      LamAveRefBlock(KRefBlock)=LamAve(1)
      NDim95(KRefBlock)=3
      RadiationRefBlock(KRefBlock)=XRayRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedMonoPer
      AngleMonRefBlock(KRefBlock)=CrlMonAngle(MonCell(1,3),MonH(1,3),
     1                                        LamAveRefBlock(KRefBlock))
      AlphaGMonRefBlock(KRefBlock)=0.
      BetaGMonRefBlock(KRefBlock)=0.
      TempRefBlock(KRefBlock)=293
      UseTrRefBlock(KRefBlock)=.false.
      ITwRead(KRefBlock)=1
      RefBlockFileName=fln(:ifln)//'.l01'
      tbar=0.
      call SetRealArrayTo(uhly,4,0.)
      call SetRealArrayTo(dircos,6,0.)
      call SetRealArrayTo(corrf,2,1.)
      itw=1
      iq=1
      UseEFormat91=.false.
      Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
      Format95(5:5)='3'
      KProf=0
      NProf=0
      DRlam=0.
      NRef95(KRefBlock)=0
      if(ndat.gt.6) XDDirCosRefBlock(KRefBlock)=1
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      p256='     0 reflections already transformed'
      write(p256(1:6),100) NRef90(KRefBlock)
      call FeTxOut(-1.,-1.,p256)
5200  read(ln,FormA,end=5600) t256
      NRef95(KRefBlock)=NRef95(KRefBlock)+1
      if(mod(NRef95(KRefBlock),50).eq.0) then
        write(p256(1:6),100) NRef95(KRefBlock)
        call FeTxOutCont(p256)
      endif
      k=0
      call StToInt(t256,k,iar,4,.false.,ich)
      if(ich.ne.0) go to 9000
      call StToReal(t256,k,xp,2,.false.,ich)
      if(CteI) then
        ri=100.*xp(1)
        rs=100.*xp(2)
      else
        ri=100.*xp(1)**2
        rs=100.*2.*xp(1)*xp(2)
      endif
      if(ndat.gt.6) then
        call StToReal(t256,k,xp,1,.false.,ich)
        tbar=xp(1)
        call StToReal(t256,k,DirCos,6,.false.,ich)
      endif
      if(ich.ne.0) go to 9000
      no=NRef95(KDatBlock)
      expos=float(NRef95(KDatBlock))*.1
      iflg(1)=iq
      iflg(2)=itw
      iflg(3)=0
      call CopyVekI(iar,ih,3)
      call DRPutReflectionToM95(95,n)
      NLines95(KRefBlock)=NLines95(KRefBlock)+n
      go to 5200
5600  close(ln)
      close(95)
      write(p256(1:6),100) NRef95(KRefBlock)
      call FeTxOutCont(p256)
      call FeTxOutEnd
      call iom95(1,fln(:ifln)//'.m95')
      call CompleteM95(0)
      ExistM95=.true.
6000  call FinalStepOfImport(1,'XD')
      call ZXDSetCommands
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      go to 9999
9000  call FeReadError(ln)
      go to 9999
9100  call FeChybne(-1.,-1.,'data bank error for atom "'//
     1              at(:max(1,idel(at)))//'".',' ',SeriousError)
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      deallocate(ksf,XDKap)
      return
100   format(i6)
101   format(80i1)
      end
