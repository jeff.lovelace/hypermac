      subroutine StringFromAtomFile(AtomTypeToRead,WhatToRead,Value,
     1                              Ktery,ierr)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) AtomTypeToRead,WhatToRead,Value
      character*256 t256
      character*80  t80,Label
      character*7 At
      logical EqIgCase
      equivalence (t80,t256)
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//'atoms.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'formfac/atoms.dat'
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9200
      read(ln,FormA) t80
      if(t80(1:1).ne.'#') rewind ln
      ierr=0
1000  read(ln,'(a7)',end=9000,err=9100) At
      if(EqIgCase(At,AtomTypeToRead)) then
2000    read(ln,FormA,end=9000,err=9100) t80
        k=0
        call kus(t80,k,Label)
        if(EqIgCase(Label,WhatToRead)) then
          n=0
          if(Ktery.le.0) go to 2550
2500      read(ln,FormA,end=9050,err=9100) t80
          k=0
2550      call kus(t80,k,Value)
          n=n+1
          if(n.ge.Ktery) go to 9999
          if(k.ge.80) then
            go to 2500
          else
            go to 2550
          endif
        else
          go to 2000
        endif
      else
3000    read(ln,FormA,end=9000,err=9100) t80
        if(.not.EqIgCase(t80,'end')) go to 3000
      endif
      go to 1000
9000  iErr=1
      go to 9999
9050  iErr=2
      go to 9999
9100  call FeReadError(ln)
      iErr=3
      go to 9999
9200  iErr=4
9999  call CloseIfOpened(ln)
      return
100   format(f15.0)
      end
