      subroutine CIFUpdateRecord(CifKeyToBeFound,LabelToBeFound,n)
      use Basic_mod
      character*(*) CifKeyToBeFound,LabelToBeFound
      character*80 t80
      character*20 CatIn1,CatIn2,Cat1,Cat2
      logical EqIgCase,JizMame,AddLine
      nfrom=0
      nloop=0
      m=0
      ncat1=0
      ncat2=0
      nloopcat=0
      AddLine=.false.
      call CIFGetCategory(CifKeyToBeFound,CatIn1,CatIn2)
      do nfrom=1,nCIFUsed
        k=0
        call Kus(CIFArray(nfrom),k,t80)
        if(EqIgCase(t80,'loop_')) then
          nloop=nfrom
          m=-1
        else
          if(m.ge.0) then
            if(t80(1:1).eq.'_'.or.t80(1:1).eq.'#'.or.
     1         EqIgCase(t80,'loop_').or.EqIgCase(t80,' ')) then
              nloop=0
              m=0
            endif
          else
            if(t80(1:1).ne.'_') m=m+1
          endif
        endif
        if(EqIgCase(t80,CifKeyToBeFound)) then
          go to 1200
        else if(nloop.eq.0) then
          call CIFGetCategory(t80,Cat1,Cat2)
          if(EqIgCase(Cat1,CatIn1)) then
            ncat1=nfrom
            if(EqIgCase(Cat2,CatIn2)) ncat2=nfrom
          endif
        endif
      enddo
      if(LabelToBeFound.ne.' ') then
        m=0
        do nfrom=1,nCIFUsed
          if(CIFArray(nfrom)(1:1).eq.'#') then
            if(m.eq.0) then
              if(LocateSubstring(CIFArray(nfrom),LabelToBeFound,.false.,
     1                           .true.).gt.0) then
                j=nfrom
                m=nfrom
              endif
            else
              m=nfrom
              exit
            endif
          endif
        enddo
      else
        if(ncat2.gt.0) then
          nloop=0
          nfrom=ncat2+1
          nto=nfrom-1
          go to 1350
        endif
        if(ncat1.gt.0) then
          nloop=0
          nfrom=ncat1+1
          nto=nfrom-1
          go to 1350
        endif
        m=0
      endif
      if(m.eq.0) then
        nfrom=nCIFUsed+1
        nto=nfrom-1
        go to 1350
      else
        if(j.lt.nfrom) then
          nfrom=m
          i=m
          AddLine=.true.
        else
          nfrom=nfrom+1
          i=nfrom-1
        endif
      endif
      go to 1300
1200  if(nloop.gt.0) then
        nfrom=nloop
        m=-1
      else
        m=0
      endif
      JizMame=.false.
      do i=nfrom,nCIFUsed
        k=0
        call Kus(CIFArray(i),k,t80)
        if(m.le.0) then
          if(m.lt.0) then
            if(t80(1:1).ne.'_'.and..not.EqIgCase(t80,'loop_')) m=m+2
          else
            if(t80(1:1).eq.'_') m=m+1
          endif
        else
          if(.not.JizMame.and.
     1       (t80(1:1).eq.'_'.or.t80(1:1).eq.'#'.or.
     2        EqIgCase(t80,'loop_').or.EqIgCase(t80,' '))) go to 1300
        endif
        if(t80(1:1).eq.';') then
          if(JizMame) then
            JizMame=.false.
            if(m.eq.0) then
              nto=i
              go to 1350
            endif
          else
            JizMame=.true.
          endif
        endif
      enddo
1300  nto=i-1
1350  m=nto-nfrom+1
      nn=n
      if(AddLine) nn=nn+1
      if(nn.gt.m) then
        nm=nn-m
        do i=nCIFUsed+1,nCIFArray
          CIFArray(i)=' '
        enddo
        call ReallocateCIFArray(nCIFUsed+nm)
        do i=nCIFUsed,nto+1,-1
          CIFArray(i+nm)=CIFArray(i)
        enddo
        nCIFUsed=nCIFUsed+nm
      endif
      do i=1,nn
        if(i.le.n) then
          CIFArray(nfrom+i-1)=CIFArrayUpdate(i)
        else
          CIFArray(nfrom+i-1)=' '
        endif
      enddo
      if(nn.lt.m) then
        mn=m-nn
        do i=nto+1,nCIFUsed
          CIFArray(i-mn)=CIFArray(i)
        enddo
        nCIFUsed=nCIFUsed-mn
      endif
9999  return
      end
