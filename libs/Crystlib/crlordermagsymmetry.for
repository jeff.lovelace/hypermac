      subroutine CrlOrderMagSymmetry
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer CrlMagInver,CrlMagCenter
      logical EqRV
      real, allocatable :: rm6s(:,:),rms(:,:),s6s(:,:),RMagS(:,:)
      real rmp(36),sp1(6),sp2(6)
      integer, allocatable :: IswSymmS(:),ZMagS(:),ipor(:)
      MagInv=CrlMagInver()
      if(MagInv.eq.0) then
        MagInv=CrlMagCenter()
        if(MagInv.eq.0) then
          NSymmN(KPhase)=NSymm(KPhase)
          go to 9999
        endif
      endif
      allocate(rm6s(36,NSymm(KPhase)),rms(9,NSymm(KPhase)),
     1         s6s(6,NSymm(KPhase)),IswSymmS(NSymm(KPhase)),
     2         ZMagS(NSymm(KPhase)),RMagS(9,NSymm(KPhase)),
     3         ipor(NSymm(KPhase)))
      call SetIntArrayTo(ipor,NSymm(KPhase),0)
      do i=1,NSymm(KPhase)
        call CopyMat(rm6(1,i,1,KPhase),rm6s(1,i),NDim(KPhase))
        call CopyMat(rm(1,i,1,KPhase),rms(1,i),3)
        call CopyVek(s6(1,i,1,KPhase),s6s(1,i),NDim(KPhase))
        IswSymmS(i)=ISwSymm(i,1,KPhase)
        ZMagS(i)=ZMag(i,1,KPhase)
        call CopyMat(RMag(1,i,1,KPhase),RMagS(1,i),3)
      enddo
      k=0
      do i=1,NSymm(KPhase)
        if(ZMagS(i).gt.0.) then
          k=k+1
          ipor(i)=k
        endif
      enddo
      if(CrlMagInver().gt.0) then
        NSymmN(KPhase)=k
      else
        NSymmN(KPhase)=NSymm(KPhase)
      endif
      do i=1,NSymm(KPhase)
        if(ZMagS(i).lt.0.) cycle
        call MultM(rm6s(1,MagInv),rm6s(1,i),rmp,NDim(KPhase),
     1             NDim(KPhase),NDim(KPhase))
        sp1(1:NDim(KPhase))=s6s(1:NDim(KPhase),MagInv)
        call CultM(rm6s(1,MagInv),s6s(1,i),sp1,NDim(KPhase),
     1             NDim(KPhase),1)
        call NormCentr(sp1)
        do j=1,NSymm(KPhase)
          if(ZMagS(j).gt.0.) cycle
          if(EqRV(rmp,rm6s(1,j),NDimQ(KPhase),.0001)) then
            sp2(1:NDim(KPhase))=s6s(1:NDim(KPhase),j)
            call NormCentr(sp2)
            if(Eqrv(sp1,sp2,NDim(KPhase),.0001)) then
              ipor(j)=ipor(i)+k
              exit
            endif
          endif
        enddo
      enddo
      do i=1,NSymm(KPhase)
        j=ipor(i)
        call CopyMat(rm6s(1,i),rm6(1,j,1,KPhase),NDim(KPhase))
        call CopyMat(rms(1,i),rm(1,j,1,KPhase),3)
        call CopyVek(s6s(1,i),s6(1,j,1,KPhase),NDim(KPhase))
        call codesymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                symmc(1,j,1,KPhase),0)
        ISwSymm(j,1,KPhase)=IswSymmS(i)
        ZMag(j,1,KPhase)=ZMagS(i)
        call CopyMat(RMagS(1,i),RMag(1,j,1,KPhase),3)
      enddo
9999  MaxNSymmN=max(MaxNSymmN,NSymmN(KPhase))
      if(allocated(rm6s)) deallocate(rm6s,rms,s6s,IswSymmS,ZMagS,RMagS,
     1                               ipor)
      return
      end
