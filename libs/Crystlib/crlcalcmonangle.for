      subroutine CrlCalcMonAngle(AngleMonActual,LamActual,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      logical WizardModeIn,CrwLogicQuest
      real LamActual
      ich=0
      WizardModeIn=WizardMode
      WizardMode=.false.
      id=NextQuestId()
      xqdp=400.
      il=5
      Veta='Calculate the monochomator glancing angle'
      call FeQuestCreate(id,-1.,-1.,xqdp,il,Veta,0,LightGray,0,0)
      il=1
      xpom=5.
      Veta='Material:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      tpom=xpom+CrwgYd+10.
      do i=1,NMon
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,MonName(i),'L',CrwXd,
     1                      CrwYd,1,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.MonSel)
        if(i.eq.1) nCrwMonFirst=CrwLastMade
      enddo
      nCrwMonLast=CrwLastMade
      il=1
      Veta='Reflection plane:'
      tpom=100.
      dpom=80.
      call FeQuestEdwMake(id,tpom,il,tpom,il+1,Veta,'L',dpom,EdwYd,0)
      nEdwIndices=EdwLastMade
      tpom=tpom+120.
      Veta='=>'
      dpomb=FeTxLength(Veta)+20.
      call FeQuestButtonMake(id,tpom,il+1,dpomb,ButYd,Veta)
      nButtCalc=ButtonLastMade
      Veta='Glancing angle:'
      tpom=tpom+80.
      call FeQuestEdwMake(id,tpom,il,tpom,il+1,Veta,'L',dpom,EdwYd,0)
      nEdwGlancingAngle=EdwLastMade
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblGlancingAngle1=LblLastMade
      call FeQuestLblMake(id,tpom+6.,il+1,Veta,'L','N')
      nLblGlancingAngle2=LblLastMade
1550  write(Veta,100) AngleMonActual
      call Zhusti(Veta)
      if(MonSel.le.NMon-1) then
        call FeQuestEdwClose(nEdwGlancingAngle)
        call FeQuestIntAEdwOpen(nEdwIndices,MonH(1,MonSel),3,.false.)
        call FeQuestLblOn(nLblGlancingAngle1)
        call FeQuestLblChange(nLblGlancingAngle2,Veta)
        call FeQuestButtonOff(nButtCalc)
      else
        call FeQuestEdwClose(nEdwIndices)
        call FeQuestLblOff(nLblGlancingAngle1)
        call FeQuestLblOff(nLblGlancingAngle2)
        call FeQuestRealEdwOpen(nEdwGlancingAngle,AngleMonActual,
     1                          .false.,.false.)
        call FeQuestButtonClose(nButtCalc)
      endif
1600  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        nCrw=nCrwMonFirst
        do i=1,NMon
          if(CrwLogicQuest(nCrw)) then
            MonSel=i
            go to 1550
          endif
          nCrw=nCrw+1
        enddo
        go to 1550
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCalc)
     1  then
        call FeQuestIntAFromEdw(nEdwIndices,MonH(1,MonSel))
        AngleMonActual=CrlMonAngle(MonCell(1,MonSel),MonH(1,MonSel),
     1                             LamActual)
        go to 1550
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1600
      endif
      if(ich.eq.0.and.CrwLogicQuest(nCrwMonLast))
     1  call FeQuestRealFromEdw(nEdwGlancingAngle,AngleMonActual)
      call FeQuestRemove(id)
      WizardMode=WizardModeIn
      return
100   format(f10.4)
      end
