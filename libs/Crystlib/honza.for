      subroutine Honza
      use Basic_mod
      use Atoms_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension IDistArr(50),SDistArr(50),ipor(50),ValArr(50),
     1          SValArr(50),aija(50)
      character*256 FileIn,flno,Veta
      character*80  SecondAtom(50),t80
      character*8   At1,At2,StNMax
      character*3   FlagString(50),FlagStringOverAll
      character*2   Label,ValLabelArr(50),ValLabel
      logical EqIgCase,iesd,Konec
      data FileIn/' '/
      flno=fln
      call FeFileManager('Select input CIF file',FileIn,'*.cif',0,
     1                   .true.,ich)
      if(ich.ne.0) go to 9999
      lni=NextLogicNumber()
      call OpenFile(lni,FileIn,'formatted','old')
      if(ich.ne.0) go to 9999
      call OpenFile(lst,'ICSD.lst','formatted','unknown')
      if(ich.ne.0) go to 9999
      lno=NextLogicNumber()
      lnb=NextLogicNumber()
      nmax=0
1000  read(lni,FormA,end=1010) Veta
      if(EqIgCase(Veta(1:5),'data_')) nmax=nmax+1
      go to 1000
1010  rewind lni
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      write(StNmax,'(i8)') nmax
      call Zhusti(StNmax)
      call FeFlowChartOpen(-1.,50.,1,nmax,'Progress of the task',
     1                     ' ',' ')
      nn=0
      nerr=0
      NInfo=2
      TextInfo(1)='Bond valence caculation for XXXXXXXXXXXXXXXXXXXXX'
      TextInfo(2)=' '
      call FeMsgShow(1,-1.,YMaxBasWin-84.,.true.)
1100  ErrFlag=0
      read(lni,FormA,end=8000) Veta
      if(.not.EqIgCase(Veta(1:5),'data_')) go to 1100
      fln=Veta(6:)
      ifln=idel(fln)
      NInfo=2
      TextInfo(1)='Bond valence calculation for '//fln(:ifln)
      write(Cislo,'(i8)') nn
      call Zhusti(Cislo)
      TextInfo(2)=Cislo(:idel(Cislo))//' items of '//
     1            Stnmax(:idel(Stnmax))//' already finished with'
      write(Cislo,'(i8)') nerr
      call Zhusti(Cislo)
      TextInfo(2)=TextInfo(2)(:idel(TextInfo(2))+1)//
     1            Cislo(:idel(Cislo))//' errors'
      call FeMsgShow(1,-1.,YMaxBasWin-80.,.false.)
      write(lst,'(/''0000 Summary for '',a)') fln(:ifln)
      call FeFlowChartEvent(nn,ie)
      if(ie.ne.0) then
        call FeBudeBreak
        if(ErrFlag.ne.0) go to 8000
      endif
      call OpenFile(lno,fln(:ifln)//'.cif','formatted','unknown')
1200  read(lni,FormA,end=2000) Veta
      if(EqIgCase(Veta(1:5),'data_')) then
        backspace lni
        go to 2000
      else
        write(lno,FormA) Veta(:idel(Veta))
        go to 1200
      endif
2000  call CloseIfOpened(lno)
      call ReadCIF(2,i)
      if(ErrFlag.ne.0) then
        write(lst,'(''### Error during creation of CIF file'')')
        go to 2010
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) then
        write(lst,'(''### Error during reading of M50 file'')')
        go to 2010
      endif
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.ne.0) then
        write(lst,'(''### Error during reading of M40 file'')')
        go to 2010
      endif
      call OpenFile(lnb,fln(:ifln)//'.bib','formatted','unknown')
2002  read(lnb,FormA,end=2005) Veta
      write(lst,FormA) Veta(:idel(Veta))
      go to 2002
2005  close(lnb,status='delete')
      write(Cislo,FormI15) NUnits(KPhase)
      call Zhusti(Cislo)
      write(lst,'(''0045 '',a)') Cislo(:idel(Cislo))
      go to 2020
2010  nerr=nerr+1
      call DeletePomFiles
      go to 1100
2020  do i=1,NAtCalc
        BratPrvni(i)=.true.
        BratDruhyATreti(i)=ai(i).gt.0.
      enddo
      call ReadBondValCoeff
      call DefaultDist
      call SetRealArrayTo(dt,9,0.)
      call SetRealArrayTo(tfirst,9,0.)
      call SetIntArrayTo(nt,9,1)
      dmdk=0.
      dmhk=4.3
      iuhl=0
      fullcoo=1
      VolaToDist=.false.
      PrvniSymmString=' '
      RejectIdentical=0
      call inm40(iesd)
      call DistAt
      rewind 78
      Konec=.false.
      n=0
3000  read(78,end=5000) Label,At1,isfi,aii,(pom,i=1,5),t80
      if(Label.ne.'#1'.or.aii.le.0.) go to 3000
      FlagStringOverAll=' '
      n=0
      write(ValLabel,'(i2)') AtVal(isfi,KPhase)
      if(ValLabel(1:1).eq.' ') ValLabel(1:1)='+'
3100  read(78,end=5000) Label,At2,isfj,aij,du,sdu,(pom,i=1,4),t80
      if(Label.eq.'#2') then
        n=n+1
        if(n.le.50) then
          SecondAtom(n)=At2(:idel(At2))//'#'//t80(:idel(t80))
          IDistArr(n)=du*1000.
          SDistArr(n)=sdu
          aija(n)=aij
          PomRho=dBondVal(isfi,isfj)
          PomB=cBondVal(isfi,isfj)
          write(ValLabelArr(n),'(i2)') AtVal(isfj,KPhase)
          if(ValLabelArr(n)(1:1).eq.' ') ValLabelArr(n)(1:1)='+'
          FlagString(n)=' '
          if(PomRho.gt.0.) then
            ValArr(n)=ExpJana(((PomRho-du)/PomB),ich)*aij
            SValArr(n)=ValArr(n)*sdu/PomB
          else
            ValArr(n)=0.
            SValArr(n)=0.
            if(AtVal(isfi,KPhase)*AtVal(isfj,KPhase).lt.0) then
              FlagString(n)='!!!'
              FlagStringOverAll='!!!'
            endif
          endif
        else if(n.eq.50) then
          call FeChybne(-1.,YBottomMessage,
     1                  'more then 50 neighbours for atom '//
     2                  At1(:idel(At1))//'.',' ',SeriousError)
        endif
        go to 3100
      else
        go to 5100
      endif
5000  Konec=.true.
5100  if(n.gt.0) then
        n=min(n,50)
        iat=ktat(atom,NAtCalc,At1)
        write(lst,'(''0050 '',a8,2x,i3,1x,a1,1x,3f8.4,3x,a2,f8.4)') At1,
     1    WyckoffMult(iat),WyckoffSmb(iat),(x(m,iat),m=1,3),ValLabel,aii
        call indexx(n,IDistArr,ipor)
        Veta='0051 '
        do m=1,n
          k=ipor(m)
          if(IDistArr(k).le.0) then
            i=index(SecondAtom(k),'#')
            At2=SecondAtom(k)(:i-1)
            Veta=Veta(:idel(Veta)+1)//At2(:idel(At2))
          else
            go to 5160
          endif
        enddo
5160    write(lst,FormA) Veta(:idel(Veta))
        SumVal=0.
        SumSval=0.
        do j=m,n
          k=ipor(j)
          i=index(SecondAtom(k),'#')
          At2=SecondAtom(k)(:i-1)
          l=index(SecondAtom(k)(i+1:),'#')+i
          Veta=SecondAtom(k)(i+1:l-1)
          Val=ValArr(k)
          SVal=SValArr(k)
          SumVal=SumVal+Val
          SumSVal=SumSVal+SVal**2
          write(lst,'(''0060 '',a8,2x,a30,3x,a2,5f8.4,2x,a3)') At2,
     1      Veta(:30),ValLabelArr(k),aija(k),float(IDistArr(k))*.001,
     2      SDistArr(k),Val,SVal,FlagString(k)
        enddo
        write(lst,'(''0061 '',69x,2f8.4,2x,a3)') SumVal,sqrt(SumSVal),
     1                                           FlagStringOverAll
        backspace 78
        if(.not.Konec) go to 3000
      endif
6000  call DeleteFile(fln(:ifln)//'.cif')
      call DeleteFile(fln(:ifln)//'.m40')
      call DeleteFile(fln(:ifln)//'.m50')
      call DeletePomFiles
      go to 1100
8000  call CloseIfOpened(lni)
      call CloseIfOpened(lst)
      call FeMsgClear(1)
      call FeFlowChartRemove
9999  fln=flno
      ifln=idel(fln)
      call ResetIgnoreW
      call ReSetIgnoreE
      return
101   format(i8)
      end
