      subroutine FinalStepOfImport(Klic,JakyImport)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) JakyImport
      logical ExistFile,EqIgCase
      call SetFormula(Formula(KPhase))
      call iom40Only(1,0,fln(:ifln)//'.m40')
      if(NCSymm(KPhase).eq.2) then
        call CrlExpandCentroSym
        deallocate(isa)
        i=ubound(isf,1)
        allocate(isa(NSymm(KPhase),i))
        do i=1,NAtAll
          do j=1,NSymm(KPhase)
            isa(j,i)=j
          enddo
        enddo
        NCSymm(KPhase)=1
      endif
      call EM50MakeStandardOrder
      call iom50(1,0,fln(:ifln)//'.m50')
      ExistM40=.true.
      ExistM50=.true.
      if(Klic.eq.2) go to 9999
      if(ExistFile(fln(:ifln)//'.m95')) then
        if(Klic.eq.0.and.EqIgCase(JakyImport,'SHELX')) then
          call FeFillTextInfo('finalstepofimport.txt',0)
          i=index(TextInfo(1),'#$%')
          if(i.gt.0)
     1      TextInfo(1)=TextInfo(1)(1:i-1)//
     2        JakyImport(:idel(JakyImport))//TextInfo(1)(i+3:)
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
        endif
        call iom50(0,0,fln(:ifln)//'.m50')
        call EM9CreateM90(0,ich)
        if(KPhase.eq.1.and.abs(EM9ScToM90(KDatBlock)-1.).gt..01) then
          sc(1,1)=sc(1,1)*sqrt(EM9ScToM90(KDatBlock))
          call iom40Only(1,0,fln(:ifln)//'.m40')
        endif
      endif
9999  return
      end
