      subroutine SetCommen(i,ndi)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer ip(2),iq(3),mp(3),mk(3)
      real xc(6),xp(6)
      logical EqRV,MatRealEqUnitMat
      call SetIntArrayTo(NCommQ(1,i,KPhase),3,1)
      call SetIntArrayTo(NCommQ(1,i,KPhase),3,1)
      do 2100j=1,ndi
        fn=1.
        do k=1,1000
          fk=k
          fp=0.
          do l=1,3
            if(Qu(l,j,i,KPhase).ne.0.) then
              pom=fk*Qu(l,j,i,KPhase)
              pom=abs(pom-anint(pom))
              fp=max(fp,pom)
            endif
          enddo
          if(fp.lt.fn-.001) then
            fn=fp
            kk=k
          endif
        enddo
        ip(1)=kk
        do k=1,3
          iq(k)=nint(float(kk)*Qu(k,j,i,KPhase))
          if(iq(k).eq.0) then
            mp(k)=0
            mk(k)=0
          else
            mp(k)=-kk
            mk(k)= kk
          endif
        enddo
        mn=999999
        do m1=mp(1),mk(1)
          do m2=mp(2),mk(2)
            do m3=mp(3),mk(3)
              mnp=iabs(m1)+iabs(m2)+iabs(m3)
              if(mnp.gt.mn) cycle
              ip(2)=iq(1)*m1+iq(2)*m2+iq(3)*m3
              call MinMultMaxFract(ip,2,MinMult,MaxFract)
              if(MaxFract.le.1) then
                VCommQ(1,j,i,KPhase)=m1
                VCommQ(2,j,i,KPhase)=m2
                VCommQ(3,j,i,KPhase)=m3
                mn=mnp
              endif
            enddo
          enddo
        enddo
        if(mn.gt.900000) then
          call FeUnforeseenError(' ')
          call SetRealArrayTo(VCommQ(1,j,i,KPhase),3,0.)
          VCommQ(1,j,i,KPhase)=1.
        endif
        NCommAdd(j,i,KPhase)=1
        NCommQ(j,i,KPhase)=kk
        call SetRealArrayTo(VCommAdd(1,j,i,KPhase),3,0.)
        if(i.gt.1) then
          m=
     1      max(kk,nint(float(NCommQ(j,1,KPhase))*
     2                        CellVol(1,KPhase)/CellVol(i,KPhase)))
          if(m.gt.kk) then
            pom=float(kk)
            do k=1,3
              xc(k)=VCommQ(k,j,i,KPhase)*pom
            enddo
            call qbyx(xc,xc(4),i)
            call multm(zvi(1,i,KPhase),xc,xp,nd,nd,1)
            call od0do1(xp,xp,3)
            do l=1,3
              if(abs(xp(l)).gt..001.and.abs(xp(l)-1.).gt..001) then
                NCommQ(j,i,KPhase)=m
                go to 2100
              endif
            enddo
            do k=1,3
              call SetRealArrayTo(xc,6,0.)
              xc(k)=1.
              if(EqRV(xc,VCommQ(1,j,i,KPhase),3,.001)) cycle
              call qbyx(xc,xc(4),i)
              call multm(zvi(1,i,KPhase),xc,xp,nd,nd,1)
              do l=1,3
                if(abs(xp(l)).gt..001.and.abs(xp(l)-1.).gt..001)
     1            then
                  call CopyVek(xc,VCommAdd(1,j,i,KPhase),3)
                  NCommAdd(j,i,KPhase)=m/kk
                  go to 2100
                endif
              enddo
            enddo
          endif
        endif
2100  continue
      NCommQProduct(i,KPhase)=NCommQ(1,i,KPhase)*
     1                        NCommQ(2,i,KPhase)*
     2                        NCommQ(3,i,KPhase)
      if(ICommen(KPhase).eq.0) then
        call UnitMat(RCommen(1,i,KPhase),3)
        do j=1,3
          RCommen((j-1)*4+1,i,KPhase)=NCommen(j,i,KPhase)
          if(NDimI(KPhase).gt.1) NCommQ(j,i,KPhase)=NCommen(j,i,KPhase)
        enddo
      endif
      call MatInv(RCommen(1,i,KPhase),RCommenI(1,i,KPhase),pom,3)
      DetRCommen(KPhase)=pom
      do j=1,10
        pom=DetRCommen(KPhase)*j
        if(abs(anint(pom)-pom).le..0001) exit
      enddo
      nn=nint(pom)
      if(ICommen(KPhase).ne.0) then
        call SetIntArrayTo(NCommen(1,i,KPhase),3,1)
        k=1
        do j=1,3
          call CopyVek(RCommenI(k,i,KPhase),xp,3)
          do 2500l=1,nn
            do m=1,3
              pom=xp(m)*float(l)
              if(abs(anint(pom)-pom).gt..0001) go to 2500
            enddo
            NCommen(j,i,KPhase)=l
            go to 2600
2500      continue
2600      k=k+3
        enddo
        do j=1,ndi
          call MultM(qu(1,j,i,KPhase),RCommen(1,i,KPhase),xp,1,3,3)
          do k=1,3
            xp(k)=anint(xp(k))
          enddo
          call MultM(xp,RCommenI(1,i,KPhase),xc,1,3,3)
          if(EqRV(qu(1,j,i,KPhase),xc,3,.01))
     1      call CopyVek(xc,qu(1,j,i,KPhase),3)
        enddo
      endif
9999  return
100   format(i1,a2,' composite part')
      end
