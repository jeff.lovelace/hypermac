      subroutine SetBasicM90FromM95(KRefB,KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      LamAve(KDatB)=LamAveRefBlock(KRefB)
      LamA1(KDatB)=LamA1RefBlock(KRefB)
      LamA2(KDatB)=LamA2RefBlock(KRefB)
      NAlfa(KDatB)=NAlfaRefBlock(KRefB)
      LamRat(KDatB)=LamRatRefBlock(KRefB)
      LpFactor(KDatB)=PolarizationRefBlock(KRefB)
      AngleMon(KDatB)=AngleMonRefBlock(KRefB)
      AlphaGMon(KDatB)=AlphaGMonRefBlock(KRefB)
      BetaGMon(KDatB)=BetaGMonRefBlock(KRefB)
      FractPerfMon(KDatB)=FractPerfMonRefBlock(KRefB)
      Radiation(KDatB)=RadiationRefBlock(KRefB)
      ExistNeutronData=Radiation(KDatB).eq.NeutronRadiation
      ExistXRayData=Radiation(KDatB).eq.XRayRadiation
      DatCollTemp(KDatB)=TempRefBlock(KRefB)
      PwdMethod(KDatB)=PwdMethodRefBlock(KRefB)
      TOFJason(KDatB)=TOFJasonRefBlock(KRefB)
      TOFTTh(KDatB)=TOFTThRefBlock(KRefB)
      TOFDifc(KDatB)=TOFDifcRefBlock(KRefB)
      TOFDifa(KDatB)=TOFDifaRefBlock(KRefB)
      TOFZero(KDatB)=TOFZeroRefBlock(KRefB)
      TOFCe(KDatB)=TOFCeRefBlock(KRefB)
      TOFZeroe(KDatB)=TOFZeroeRefBlock(KRefB)
      TOFTCross(KDatB)=TOFTCrossRefBlock(KRefB)
      TOFWCross(KDatB)=TOFWCrossRefBlock(KRefB)
      XDDirCos(KDatB)=XDDirCosRefBlock(KRefB)
      call CopyVek(TOFSigmaRefBlock(1,KRefB),TOFSigma(1,KDatB),3)
      call CopyVek(TOFGammaRefBlock(1,KRefB),TOFGamma(1,KDatB),3)
      call CopyVek(TOFAlfbeRefBlock(1,KRefB),TOFAlfbe(1,KDatB),4)
      call CopyVek(TOFAlfbtRefBlock(1,KRefB),TOFAlfbt(1,KDatB),4)
      EDSlope(KDatB)=EDSlopeRefBlock(KRefB)
      EDOffset(KDatB)=EDOffsetRefBlock(KRefB)
      EDTTh(KDatB)=EDTThRefBlock(KRefB)
      EDEnergy2D(KDatB)=EDEnergy2DRefBlock(KRefB)
      if(DifCode(KRefB).gt.200) then
        DataType(KDatB)=-2
      else if(DifCode(KRefB).gt.100) then
        DataType(KDatB)=2
      else
        DataType(KDatB)=1
      endif
      HKLF5(KDatB)=HKLF5RefBlock(KRefB)
      if(IncSpectFileRefBlock(KRefB).eq.' ') then
        UseIncSpect(KDatB)=0
      else
        UseIncSpect(KDatB)=1
      endif
      MagFieldDir(1:3,KDatB)=MagFieldDirRefBlock(1:3,KRefB)
      MagPolFlag(KDatB)=MagPolFlagRefBlock(KRefB)
      MagPolPlus(KDatB)=MagPolPlusRefBlock(KRefB)
      MagPolMinus(KDatB)=MagPolMinusRefBlock(KRefB)
      return
      end
