      integer function PrvKiMag(ia)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      PrvKiMag=max(TRankCumul(itf(ia)),10)+1
      k=0
      do j=1,7
        if(KModA(j,ia).gt.0) then
          PrvKiMag=PrvKiMag+2*TRank(j-1)*KModA(j,ia)
          if(j.eq.1) PrvKiMag=PrvKiMag+1
          if(k.le.0) k=1
        endif
      enddo
      PrvKiMag=PrvKiMag+k
      return
      end
