      subroutine ReadBondValCoeff
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      dimension ip(1),xp(2)
      character*256 t256
      character*2 At1,At2,Atp1,Atp2
      logical ExistFile,EqIgCase
      integer ValenceFromAtType
      call SetRealArrayTo(dBondVal,400,0.)
      call SetRealArrayTo(cBondVal,400,.37)
      ln=NextLogicNumber()
      t256=HomeDir
      if(OpSystem.le.0) then
        t256=t256(:idel(t256))//'bondval'//ObrLom//'bvparm.cif'
      else
        t256=t256(:idel(t256))//'bondval/bvparm.cif'
      endif
      if(ExistFile(t256)) then
        call OpenFile(ln,t256,'formatted','old')
        if(ErrFlag.eq.0) then
          read(ln,FormA) t256
          do i=1,NAtFormula(KPhase)
            Atp1=AtType(i,KPhase)
            iv1=ValenceFromAtType(AtType(i,KPhase))
            AtVal(i,KPhase)=iv1
            do 1500j=1,NAtFormula(KPhase)
              Atp2=AtType(j,KPhase)
              iv2=ValenceFromAtType(AtType(j,KPhase))
              write(t256,'(i5,a,i4,a,i4)') NAtFormula(KPhase),
     1                                     Atp1,iv1,Atp2,iv2
              rewind ln
1000          read(ln,FormA,end=1200) t256
              if(t256(1:1).eq.'#') go to 1000
              call Mala(t256)
              if(index(t256,'_valence_param_details').le.0) go to 1000
1100          read(ln,FormA,end=1200) t256
              k=0
              call kus(t256,k,At1)
              call StToInt(t256,k,ip,1,.false.,ich)
              if(ich.ne.0) go to 1100
              i1=ip(1)
              call kus(t256,k,At2)
              call StToInt(t256,k,ip,1,.false.,ich)
              if(ich.ne.0) go to 1100
              i2=ip(1)
              call StToReal(t256,k,xp,2,.false.,ich)
              if(iv1.eq.0) i1=0
              if(iv2.eq.0) i2=0
              if(((EqIgCase(At1,Atp1).and.EqIgCase(At2,Atp2)).and.
     1            ((i1.eq.iv1.or.i1.eq.9).and.(i2.eq.iv2.or.i2.eq.9)))
     2           .or.
     3           ((EqIgCase(At1,Atp2).and.EqIgCase(At2,Atp1)).and.
     4            ((i1.eq.iv2.or.i1.eq.9).and.(i2.eq.iv1.or.i2.eq.9))))
     5          then
                dBondVal(i,j)=xp(1)
                cBondVal(i,j)=xp(2)
                go to 1500
              else
                go to 1100
              endif
1200          dBondVal(i,j)=0.
              cBondVal(i,j)=0.
1500        continue
          enddo
        endif
        call CloseIfOpened(ln)
      endif
      return
      end
