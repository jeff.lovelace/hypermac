      subroutine MakeOccMod(occ,x40,tzero,nt,dt,a0,ax,ay,kmod,kf,Gamma)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension occ(*),ax(*),ay(*),tzero(3),nt(3),dt(3),x4i(3),x4(3),
     1          nd(3),x40(3),Gamma(9),d4(3)
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      do i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        occo=0.
        occx=1.
        if(kmod.gt.0) then
          if(kf.eq.0) occo=a0-occx
          kk=0
          do k=1,kmod
            if(kf.ne.0.and.k.gt.kmod-NDimI(KPhase)) then
              kk=kk+1
              x4p=x4(kk)-ax(k)
              ix4p=x4p
              if(x4p.lt.0.) ix4p=ix4p-1
              x4p=x4p-float(ix4p)
              if(x4p.gt..5) x4p=x4p-1.
              if(NDim(KPhase).eq.4) then
                delta=a0*.5
              else
                delta=ay(kk)*.5
              endif
              if(x4p.ge.-delta.and.x4p.le.delta) then
                occx=1.
              else
                occx=0.
              endif
            else
              arg=0.
              do j=1,NDimI(KPhase)
                arg=arg+x4(j)*float(kw(j,k,KPhase))
              enddo
              arg=pi2*arg
              occo=occo+ax(k)*sin(arg)+ay(k)*cos(arg)
            endif
          enddo
          if(occx.gt.0.) then
            occ(i)=occx+occo
          else
            occ(i)=0.
          endif
        else
          occ(i)=1.
        endif
      enddo
      return
      end
