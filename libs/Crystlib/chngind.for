      subroutine ChngInd(h,ih,icomp,dif,MMin,MMax,klic,CheckExtRefIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension h(3),ih(6),dif(3),difp(3),ihp(6),ihin(6),
     1          iqu1(3),iqu2(3),iqu3(3),iqp(3,3)
      integer hint(3),p,hp(3),d,MMin(3),MMax(3)
      logical SystExtRef,eqiv,CheckExtRefIn
      equivalence (iqu1,iqp(1,1)),(iqu2,iqp(1,2)),(iqu3,iqp(1,3))
      data ish,ishpul/10000,5000/,rsh,rshi/10000.,.0001/
      if(Klic.eq.0) then
        go to 5000
      else
        do i=1,3
          hint(i)=nint(h(i)*rsh)
        enddo
        call SetIntArrayTo(iqp,9,0)
        do i=1,NDimI(KPhase)
          do j=1,3
            iqp(j,i)=nint(rsh*qu(j,i,icomp,KPhase))
          enddo
        enddo
        if(klic.eq.2) call CopyVekI(ih,ihin,NDim(KPhase))
        d=999999
        modsmn=999999
        modcmn=999999
        if(NDim(KPhase).gt.3) then
          imp=MMin(1)
          imk=MMax(1)
        else
          imp= 0
          imk= 0
        endif
        if(NDim(KPhase).gt.4) then
          inp=MMin(2)
          ink=MMax(2)
        else
          inp= 0
          ink= 0
        endif
        if(NDim(KPhase).gt.5) then
          ipp=MMin(3)
          ipk=MMax(3)
        else
          ipp= 0
          ipk= 0
        endif
        do ip=ipp,ipk
          do in=inp,ink
            do 2800im=imp,imk
              p=0
              do i=1,3
                ipom=hint(i)-im*iqu1(i)
                if(NDim(KPhase).le.4) then
                  j=mod(iabs(ipom),ish)
                  if(j.gt.ishpul) j=ish-j
                  p=p+j
                  if(p.gt.d) go to 2800
                  ihp(i)=nint(float(ipom)*rshi)
                  difp(i)=float(j)*rshi
                else
                  hp(i)=ipom
                endif
              enddo
              if(NDim(KPhase).gt.4) then
                do i=1,3
                  hp(i)=hp(i)-in*iqu2(i)
                  if(NDim(KPhase).eq.5) then
                    j=mod(iabs(hp(i)),ish)
                    if(j.gt.ishpul) j=ish-j
                    p=p+j
                    if(p.gt.d) go to 2800
                    ihp(i)=nint(float(hp(i))*rshi)
                    difp(i)=float(j)*rshi
                  endif
                enddo
                if(NDim(KPhase).gt.5) then
                  do i=1,3
                    hp(i)=hp(i)-ip*iqu3(i)
                    j=mod(iabs(hp(i)),ish)
                    if(j.gt.ishpul) j=ish-j
                    p=p+j
                    if(p.gt.d) go to 2800
                    ihp(i)=nint(float(hp(i))*rshi)
                    difp(i)=float(j)*rshi
                  enddo
                endif
              endif
              ihp(4)=im
              ihp(5)=in
              ihp(6)=ip
              if(CheckExtRefIn) then
                if(SystExtRef(ihp,.false.,icomp)) go to 2800
              endif
              mods=0
              do i=4,NDim(KPhase)
                mods=mods+iabs(ihp(i))
              enddo
              modc=mods
              do i=1,3
                modc=modc+iabs(ihp(i))
              enddo
              if(Klic.eq.2) then
                if(eqiv(ihp,ihin,NDim(KPhase))) go to 2800
              endif
              if(p.lt.d-10.or.mods.lt.modsmn.or.
     1           (mods.eq.modsmn.and.modc.lt.modcmn)) then
                modsmn=mods
                modcmn=modc
                call CopyVekI(ihp,ih,6)
                d=p
                call CopyVek(difp,dif,3)
              endif
2800        continue
          enddo
        enddo
        if(d.gt.ish) call SetRealArrayTo(dif,3,1.)
        go to 9999
      endif
      entry IndRealFromInd(ih,h,icomp)
5000  do j=1,3
        pom=ih(j)
        do i=4,NDim(KPhase)
          pom=pom+float(ih(i))*qu(j,i-3,icomp,KPhase)
        enddo
        h(j)=pom
      enddo
9999  return
      end
