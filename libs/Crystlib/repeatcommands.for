      subroutine RepeatCommands
      include 'fepc.cmn'
      character*(*) FileName
      character*256 ta256(2),t256,OldFile,SbwStringQuest,SbwFileQuest,
     1              EdwStringQuest
      character*80 Veta
      character*10 LabelWrite,Label(2)
      external ReadCommand,WriteCommand,CheckCommand,MakeCommand
      integer WhatHappened,WhereToWrite,FeDoSomethingOrCancel,ExtOK,
     1        SbwItemPointerQuest,SbwItemNMaxQuest,SbwLnQuest,
     2        SbwItemFromQuest,SbwLinesQuest,SbwItemToQuest,
     3        SbwItemSelNQuest,SbwItemSelQuest
      logical CommandEnabled,Konec,WithEnable,FeYesNoHeader,FileDiff,
     1        FeYesNo
      equivalence (ta256,t256)
      save nSbwSelect,OldFile,WhereToWrite,LabelWrite,nLblWrite,nActButt
      data CommandEnabled/.true./,Label/'%Yes','%No'/
      entry RepeatCommandsProlog(id,FileName,XLength,NLines,LastLine,
     1                           ExtOK)
      WithEnable=.true.
      go to 1100
      entry RepeatCommandsPrologWithoutEnable(id,FileName,XLength,
     1                                      NLines,LastLine,ExtOK)
      WithEnable=.false.
1100  WhereToWrite=0
      OldFile='jcmd'
      call CreateTmpFile(OldFile,i,0)
      call FeTmpFilesAdd(OldFile)
      call CopyFile(FileName,OldFile)
      id=NextQuestId()
      il=NLines+11
      call FeQuestCreate(id,-1.,-1.,XLength,il,' ',0,LightGray,0,ExtOK)
      CheckKeyboard=.true.
      dbutt=120.
      xpom=5.
      dpoms=XLength-dbutt-5.
      LastLine=11
      il=10
      call FeQuestSbwMake(id,xpom,il,dpoms,il,1,CutTextFromRight,
     1                    SbwVertical)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      nSbwSelect=SbwLastMade
      SbwLen=SbwXMaxPruhQuest(1,SbwLastMade)-
     1       SbwXMinPruhQuest(1,SbwLastMade)
      dbutt=dbutt-SbwLen
      dpome=dpoms+SbwLen
      call FeQuestSbwSelectOpen(nSbwSelect,FileName)
      il=0
      Veta='Edi%t'
      xpom=XLength-dbutt+10.
      dpom=dbutt-20.
      nActButt=0
      do i=1,9
        if(.not.WithEnable) then
          if(i.eq.6.or.i.eq.7) then
            if(i.eq.7) Veta='%Select all'
            cycle
          endif
        endif
        il=il+1
        if(i.eq.4) then
          dpomo=dpom
          dpom=dpom*.5-5.
        else if(i.eq.5) then
          il=il-1
          xpomo=xpom
          xpom=xpom+dpom+10.
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          Veta='%Delete'
          nButtRepeatEdit=ButtonLastMade
        else if(i.eq.2) then
          Veta='%Clone'
          nButtRepeatDelete=ButtonLastMade
        else if(i.eq.3) then
          Veta='Do%wn'
          nButtRepeatClone=ButtonLastMade
        else if(i.eq.4) then
          Veta='%Up'
          nButtRepeatDown=ButtonLastMade
        else if(i.eq.5) then
          Veta='D%isable'
          nButtRepeatUp=ButtonLastMade
          xpom=xpomo
          dpom=dpomo
        else if(i.eq.6) then
          Veta='Ena%ble'
          nButtRepeatDisable=ButtonLastMade
        else if(i.eq.7) then
          Veta='%Select all'
          nButtRepeatEnable=ButtonLastMade
        else if(i.eq.8) then
          Veta='Re%fresh'
          nButtRepeatAll=ButtonLastMade
        else if(i.eq.9) then
          nButtRepeatRefresh=ButtonLastMade
        endif
        nActButt=nActButt+1
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      enddo
      if(.not.WithEnable) then
        nButtRepeatDisable=0
        nButtRepeatEnable=0
      endif
      nEdwRepeatCheck=0
      Veta='Load'
      tpom=dpoms*.5-60.
      dpom=ButYd
      xpom=dpome*.5-tpom
      il=10
      il=-10*il-7
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'#')
      nButtRepeatRead=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom-FeTxLength(Veta)-10.
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      xpom=dpome*.5+tpom-dpom
      dpom=ButYd
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'^')
      nButtRepeatWrite=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      LabelWrite='Add'
      xpoml=xpom+dpom+10.
      call FeQuestLblMake(id,xpoml,il,LabelWrite,'L','N')
      nLblWrite=LblLastMade
      Nastavit=1
      RepeatMode=.true.
      go to 1400
      entry RepeatCommandsEvent(id,ich,WhatHappened,
     1                          ReadCommand,WriteCommand,
     2                          CheckCommand,MakeCommand)
      Nastavit=0
1400  nButt=nButtRepeatEdit
      if(SbwItemNMaxQuest(nSbwSelect).gt.0) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      do i=1,nActButt
        if(nButt.ne.nButtRepeatEdit) call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
      call FeQuestButtonOpen(nButtRepeatRead,j)
1500  if(WhereToWrite.eq.0) then
        LabelWrite='Add'
      else
        LabelWrite='Rewrite'
      endif
      call FeQuestLblChange(nLblWrite,LabelWrite)
      if(Nastavit.gt.0) go to 9999
2000  if(CheckType.eq.EventButton) then
        call FeQuestButtonOff(CheckNumber)
        BlockedActiveObj=.false.
        call FeQuestActiveObjOff
        ActiveObj=ActSbw*10000+nSbwSelect+SbwFr-1
        call FeQuestActiveObjOn
      endif
2005  call FeQuestEventWithCheck(id,ich,CheckCommand,MakeCommand)
      Konec=.false.
2010  if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        IgnoreW=.true.
        IgnoreE=.true.
        call WriteCommand(t256,ichp)
        IgnoreW=.false.
        IgnoreE=.false.
        if(WhereToWrite.gt.0) then
          if(ichp.eq.0.and.t256.ne.
     1      SbwStringQuest(SbwItemPointerQuest(nSbwSelect),nSbwSelect))
     2      then
            i=FeDoSomethingOrCancel('Do you want to save the last '//
     1                              'command?',Label,2)
          else
            i=2
          endif
        else
          if(ichp.eq.0) then
            i=FeDoSomethingOrCancel('Do you want to add the new '//
     1                              'command?',Label,2)
          else
            i=2
          endif
        endif
        if(i.eq.1) then
          CheckType=EventButton
          CheckNumber=nButtRepeatWrite
          Konec=.true.
        else if(i.eq.0) then
          go to 2000
        endif
      endif
      if(CheckType.eq.EventKey.and.CtrlPressed.and.
     1   (CheckNumber.eq.JeDown.or.CheckNumber.eq.JeUp)) then
        CheckType=EventButton
        if(CheckNumber.eq.JeDown) then
          CheckNumber=nButtRepeatRead
        else
          CheckNumber=nButtRepeatWrite
        endif
      else if(CheckType.eq.EventKey.or.CheckType.eq.EventASCII.or.
     1        CheckType.eq.EventCtrl) then
        EventType=EventEdw
        EventNumber=EdwActive-EdwFr+1
        go to 2005
      endif
2100  if(CheckType.eq.EventButton) then
        ItemFromOld=SbwItemFromQuest(nSbwSelect)
        ItemSelOld=SbwItemPointerQuest(nSbwSelect)
        ItemToOld=SbwItemToQuest(nSbwSelect)
        if(SbwItemSelNQuest(nSbwSelect).gt.0) then
          j1=1
          j2=SbwItemNMaxQuest(nSbwSelect)
          k=0
          l=SbwItemSelQuest(ItemSelOld,nSbwSelect)
        else
          j1=ItemSelOld
          j2=ItemSelOld
          k=1
          l=1
        endif
        if(CheckNumber.eq.nButtRepeatEdit) then
          call CloseIfOpened(SbwLnQuest(nSbwSelect))
          call FeEdit(SbwFileQuest(nSbwSelect))
          call FeQuestSbwSelectOpen(nSbwSelect,SbwFileQuest(nSbwSelect))
        else if(CheckNumber.eq.nButtRepeatDelete) then
          call FeQuestSbwItemSelDel(nSbwSelect)
        else if(CheckNumber.eq.nButtRepeatEnable.or.
     1          CheckNumber.eq.nButtRepeatDisable) then
          do j=j1,j2
            if(k.eq.0.and.SbwItemSelQuest(j,nSbwSelect).eq.0) cycle
            t256=SbwStringQuest(j,nSbwSelect)
            if(t256(1:1).eq.'!') then
              if(CheckNumber.eq.nButtRepeatEnable) t256=t256(2:)
            else
              if(CheckNumber.eq.nButtRepeatDisable)
     1                               t256='!'//t256(:idel(t256))
            endif
            call CloseIfOpened(SbwLnQuest(nSbwSelect))
            call RewriteLinesOnFile(SbwFileQuest(nSbwSelect),j,j,ta256,
     1                              1)
            open(SbwLnQuest(nSbwSelect),file=SbwFileQuest(nSbwSelect))
          enddo
          call CloseIfOpened(SbwLnQuest(nSbwSelect))
          call FeQuestSbwSelectOpen(nSbwSelect,SbwFileQuest(nSbwSelect))
          call FeQuestSbwShow(nSbwSelect,ItemFromOld)
          call FeQuestSbwItemOff(nSbwSelect,
     1                           SbwItemFromQuest(nSbwSelect))
          call FeQuestSbwItemOn(nSbwSelect,ItemSelOld)
        else if(CheckNumber.eq.nButtRepeatClone) then
          do j=j2,j1,-1
            if(k.eq.0.and.SbwItemSelQuest(j,nSbwSelect).eq.0) cycle
            ta256(1)=SbwStringQuest(j,nSbwSelect)
            ta256(2)=ta256(1)
            call FeQuestSetSbwItemSel(j,nSbwSelect,0)
            call CloseIfOpened(SbwLnQuest(nSbwSelect))
            call RewriteLinesOnFile(SbwFileQuest(nSbwSelect),j,j,ta256,
     1                              2)
            open(SbwLnQuest(nSbwSelect),file=SbwFileQuest(nSbwSelect))
            if(j.le.ItemSelOld) ItemSelOld=ItemSelOld+1
          enddo
          call CloseIfOpened(SbwLnQuest(nSbwSelect))
          call FeQuestSbwSelectOpen(nSbwSelect,SbwFileQuest(nSbwSelect))
          if(ItemSelOld.ge.ItemFromOld+SbwLinesQuest(nSbwSelect)-1) then
            j=ItemSelOld-SbwLinesQuest(nSbwSelect)+2
          else
            j=ItemFromOld
          endif
          call FeQuestSbwShow(nSbwSelect,j)
          call FeQuestSbwItemOff(nSbwSelect,
     1                           SbwItemFromQuest(nSbwSelect))
          call FeQuestSbwItemOn(nSbwSelect,ItemSelOld)
        else if(CheckNumber.eq.nButtRepeatUp) then
          do j=j1,j2
            if(k.eq.0.and.SbwItemSelQuest(j,nSbwSelect).eq.0) cycle
            if(j.le.1) exit
            ta256(1)=SbwStringQuest(j  ,nSbwSelect)
            ta256(2)=SbwStringQuest(j-1,nSbwSelect)
            call CloseIfOpened(SbwLnQuest(nSbwSelect))
            call RewriteLinesOnFile(SbwFileQuest(nSbwSelect),j-1,j,
     1                              ta256,2)
            m1=SbwItemSelQuest(j  ,nSbwSelect)
            m2=SbwItemSelQuest(j-1,nSbwSelect)
            call FeQuestSetSbwItemSel(j  ,nSbwSelect,m2)
            call FeQuestSetSbwItemSel(j-1,nSbwSelect,m1)
            open(SbwLnQuest(nSbwSelect),file=SbwFileQuest(nSbwSelect))
            if(l.eq.0) then
              if(j-1.eq.ItemSelOld) ItemSelOld=ItemSelOld+1
            else
              if(j.eq.ItemSelOld) ItemSelOld=ItemSelOld-1
            endif
          enddo
          call CloseIfOpened(SbwLnQuest(nSbwSelect))
          call FeQuestSbwSelectOpen(nSbwSelect,SbwFileQuest(nSbwSelect))
          if(ItemSelOld.le.ItemFromOld) then
            j=ItemFromOld-1
          else
            j=ItemFromOld
          endif
          call FeQuestSbwShow(nSbwSelect,j)
          call FeQuestSbwItemOff(nSbwSelect,
     1                           SbwItemFromQuest(nSbwSelect))
          call FeQuestSbwItemOn(nSbwSelect,ItemSelOld)
        else if(CheckNumber.eq.nButtRepeatDown) then
          do j=j2,j1,-1
            if(k.eq.0.and.SbwItemSelQuest(j,nSbwSelect).eq.0) cycle
            if(j.ge.SbwItemNMaxQuest(nSbwSelect)) exit
            ta256(1)=SbwStringQuest(j+1,nSbwSelect)
            ta256(2)=SbwStringQuest(j  ,nSbwSelect)
            call CloseIfOpened(SbwLnQuest(nSbwSelect))
            call RewriteLinesOnFile(SbwFileQuest(nSbwSelect),j,j+1,
     1                              ta256,2)
            m1=SbwItemSelQuest(j  ,nSbwSelect)
            m2=SbwItemSelQuest(j+1,nSbwSelect)
            call FeQuestSetSbwItemSel(j  ,nSbwSelect,m2)
            call FeQuestSetSbwItemSel(j+1,nSbwSelect,m1)
            open(SbwLnQuest(nSbwSelect),file=SbwFileQuest(nSbwSelect))
            if(l.eq.0) then
              if(j+1.eq.ItemSelOld) ItemSelOld=ItemSelOld-1
            else
              if(j.eq.ItemSelOld) ItemSelOld=ItemSelOld+1
            endif
          enddo
          call CloseIfOpened(SbwLnQuest(nSbwSelect))
          call FeQuestSbwSelectOpen(nSbwSelect,SbwFileQuest(nSbwSelect))
          if(ItemSelOld.le.ItemFromOld) then
            j=ItemFromOld-1
          else
            j=ItemFromOld
          endif
          call FeQuestSbwShow(nSbwSelect,j)
          call FeQuestSbwItemOff(nSbwSelect,
     1                           SbwItemFromQuest(nSbwSelect))
          call FeQuestSbwItemOn(nSbwSelect,ItemSelOld)
        else if(CheckNumber.eq.nButtRepeatAll.or.
     1          CheckNumber.eq.nButtRepeatRefresh) then
          if(CheckNumber.eq.nButtRepeatAll) then
            k=1
            call FeQuestSetSbwItemSelN(nSbwSelect,
     1                  SbwItemNMaxQuest(nSbwSelect))
          else
            k=0
            call FeQuestSetSbwItemSelN(nSbwSelect,0)
          endif
          do i=1,SbwItemNMaxQuest(nSbwSelect)
            call FeQuestSetSbwItemSel(i,nSbwSelect,k)
          enddo
          call FeQuestSbwShow(nSbwSelect,ItemFromOld)
        else if(CheckNumber.eq.nButtRepeatWrite) then
          call WriteCommand(t256,ich)
          if(.not.CommandEnabled) t256='!'//t256(1:idel(t256))
          if(ich.eq.0) then
            call CloseIfOpened(SbwLnQuest(nSbwSelect))
            if(WhereToWrite.gt.0) then
              call RewriteLinesOnFile(SbwFileQuest(nSbwSelect),
     1                                ItemSelOld,ItemSelOld,ta256,1)
            else
              call AppendFile(SbwFileQuest(nSbwSelect),ta256,1)
            endif
            call FeQuestSbwSelectOpen(nSbwSelect,
     1                                SbwFileQuest(nSbwSelect))
            if(WhereToWrite.le.0)
     1        ItemSelOld=SbwItemNMaxQuest(nSbwSelect)
            if(ItemSelOld.ge.ItemFromOld+SbwLinesQuest(nSbwSelect)-1)
     1        then
              j=ItemSelOld-SbwLinesQuest(nSbwSelect)+2
            else
              j=ItemFromOld
            endif
            call FeQuestSbwShow(nSbwSelect,j)
            call FeQuestSbwItemOff(nSbwSelect,SbwItemFrom(nSbwSelect))
            call FeQuestSbwItemOn(nSbwSelect,ItemSelOld)
            WhatHappened=RepeatCommandWrittenOut
            WhereToWrite=0
            CommandEnabled=.true.
            if(Konec) then
              CheckType=0
              WhatHappened=RepeatOtherEvent
            endif
            go to 9999
          else
            go to 2000
          endif
        else if(CheckNumber.eq.nButtRepeatRead) then
          t256=SbwStringQuest(SbwItemPointerQuest(nSbwSelect),
     1                        nSbwSelect)
          CommandEnabled=t256(1:1).ne.'!'
          call ReadCommand(t256,ich)
          if(ich.ne.0) then
            go to 2000
          else
            WhatHappened=RepeatCommandReadIn
            WhereToWrite=SbwItemPointerQuest(nSbwSelect)
            go to 9999
          endif
        else
          WhatHappened=RepeatOtherEvent
          go to 9999
        endif
        go to 1400
      else if(CheckType.eq.EventSbwDoubleClick.and.
     1        mod(CheckNumber,100).eq.nSbwSelect) then
        CheckType=EventButton
        CheckNumber=nButtRepeatRead
        go to 2100
      endif
      WhatHappened=RepeatOtherEvent
      go to 9999
      entry RepeatCommandsEpilog(id,ich)
      if(nEdwRepeatCheck.gt.0.and.ich.eq.0) then
        Veta=EdwStringQuest(nEdwRepeatCheck)
        if(EdwStringQuest(nEdwRepeatCheck).ne.' ') then
          NInfo=1
          TextInfo(1)='The last edited item hasn''t been saved and '//
     1                'therefore this last change will be lost.'
          if(.not.FeYesNoHeader(-1.,-1.,'Do you want to close the '//
     1                          'dialog form anyhow?',0)) then
            ich=ich+10
            go to 9999
          endif
        endif
      endif
      if(ich.ne.0) then
        call CloseIfOpened(SbwLnQuest(nSbwSelect))
        if(FileDiff(OldFile,SbwFileQuest(nSbwSelect))) then
          if(FeYesNo(-1.,-1.,'Do you really want to discard made '//
     1               'changes?',0)) then
            call MoveFile(OldFile,SbwFileQuest(nSbwSelect))
          else
            call DeleteFile(OldFile)
          endif
        endif
      else
        call DeleteFile(OldFile)
      endif
      call FeTmpFilesClear(OldFile)
      call FeQuestRemove(id)
      RepeatMode=.false.
9999  return
      end
