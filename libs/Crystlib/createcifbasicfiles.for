      subroutine CreateCIFBasicFiles
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256,s256,Veta
      logical ExistFile
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'cif'//ObrLom//'cif_specific.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'cif/cif_specific.dat'
      endif
      if(.not.ExistFile(t256)) then
        if(OpSystem.le.0) then
          s256=HomeDir(:idel(HomeDir))//'cif'//ObrLom//
     1         'cif_specific_basic.dat'
        else
          s256=HomeDir(:idel(HomeDir))//'cif/cif_specific_basic.dat'
        endif
        if(ExistFile(s256)) call CopyFile(s256,t256)
        CIFSpecificFile=t256
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
        lni=NextLogicNumber()
        call OpenFile(lni,t256,'formatted','unknown')
        if(allocated(CIFArray)) deallocate(CIFArray)
        nCIFArray=1000
        allocate(CIFArray(nCIFArray))
        nCIFArrayUpdate=1000
        allocate(CIFArrayUpdate(nCIFArrayUpdate))
        n=0
1100    read(lni,FormA,end=1200) Veta
        n=n+1
        if(n.gt.nCIFArray) call ReallocateCIFArray(nCIFArray+1000)
        CIFArray(n)=Veta
        go to 1100
1200    nCIFUsed=n
        s256=HomeDir(:idel(HomeDir))//'cif'//ObrLom
        do i=1,3
          if(i.eq.1) then
            Veta=s256(:idel(s256))//'cif_publ.dat'
          else if(i.eq.2) then
            Veta=s256(:idel(s256))//'cif_references.dat'
          else if(i.eq.3) then
            Veta=s256(:idel(s256))//'cif_experiment.dat'
          endif
          if(ExistFile(Veta)) call CIFUpdateFromUserFile(Veta)
        enddo
        rewind lni
        do i=1,nCIFUsed
          write(lni,FormA) CIFArray(i)(:idel(CIFArray(i)))
        enddo
        call CloseIfOpened(lni)
        if(allocated(CIFArray)) deallocate(CIFArray)
        deallocate(CIFArrayUpdate)
      endif
      return
      end
