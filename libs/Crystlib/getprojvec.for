      subroutine GetProjVec(rm6p,s6p,ProjVec)
      include 'fepc.cmn'
      include 'basic.cmn'
      logical eqrv
      dimension rm6p(*),s6p(*),ProjVec(*),rs(36),rp(36),re(36),rpp(36)
      nn=0
      call SetRealArrayTo(rs,NDimQ(KPhase),0.)
      call UnitMat(re,NDim(KPhase))
      call CopyMat(rm6p,rp,NDim(KPhase))
1020  nn=nn+1
      call AddVek(rs,rp,rs,NDimQ(KPhase))
      if(.not.eqrv(rp,re,NDimQ(KPhase),.0001)) then
        call CopyVek(rp,rpp,NDimQ(KPhase))
        call multm(rpp,rm6p,rp,NDim(KPhase),NDim(KPhase),NDim(KPhase))
        go to 1020
      endif
      do i=1,NDimQ(KPhase)
        rs(i)=rs(i)/float(nn)
      enddo
      call multm(rs,s6p,ProjVec,NDim(KPhase),NDim(KPhase),1)
      call od0do1(ProjVec,ProjVec,NDim(KPhase))
      return
      end
