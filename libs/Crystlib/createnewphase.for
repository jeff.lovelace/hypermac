      subroutine CreateNewPhase
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      real QSymm(3)
      real, allocatable :: ScOld(:)
      character*256 Veta,EdwStringQuest,FileName(3)
      character*20  t20
      logical ExistFile,CrwLogicQuest
      data ImportType/1/
      FileName=' '
      KPhaseIn=KPhase
      id=NextQuestId()
      Veta='New phase:'
      xqd=350.
      il=5
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      Veta='%File name:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=220.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      Veta='%Browse'
      tpom=FeTxLengthUnder(Veta)+10.
      xpom=(xqd+xpom+dpom-tpom)*.5
      call FeQuestButtonMake(id,xpom,il,tpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='Import from %Jana2006 format'
      do i=1,4
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,1)
        if(i.eq.1) then
          nCrwJana=CrwLastMade
          Veta='Import from %CIF format'
        else if(i.eq.2) then
          nCrwCIF=CrwLastMade
          Veta='Import from %SHELX format'
        else if(i.eq.3) then
          nCrwSHELX=CrwLastMade
          Veta='%Manually'
        else if(i.eq.4) then
          nCrwManually=CrwLastMade
        endif
        il=il+1
        call FeQuestCrwOpen(CrwLastMade,i.eq.ImportType)
      enddo
      ImportTypeOld=-1
1400  if(ImportTypeOld.gt.0.and.ImportTypeOld.ne.4)
     1  FileName(ImportTypeOld)=EdwStringQuest(nEdwFileName)
1450  if(ImportType.le.3) then
        call FeQuestStringEdwOpen(nEdwFileName,FileName(ImportType))
        call FeQuestButtonOff(nButtBrowse)
      else
        call FeQuestEdwDisable(nEdwFileName)
        call FeQuestButtonDisable(nButtBrowse)
      endif
      ImportTypeOld=ImportType
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw) then
        ImportType=CheckNumber-nCrwJana+1
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1   then
        if(ImportType.eq.1) then
          t20='*.m40 *.m50'
        else if(ImportType.eq.2) then
          t20='*.cif'
        else
          t20='*.ins *.res'
        endif
        Veta=FileName(ImportType)
        call FeFileManager('Select the input file',Veta,t20,0,.true.,
     1                     ich)
        if(ich.eq.0) then
          FileName(ImportType)=Veta
          go to 1450
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(ImportType.lt.4) then
          FileName(ImportType)=EdwStringQuest(nEdwFileName)
          if(FileName(ImportType).eq.' ') then
            Veta='the input file name was not specified'
          else if(ImportType.eq.1) then
            call GetPureFileName(FileName(ImportType),Veta)
            do i=1,2
              if(i.eq.1) then
                Cislo='.m40'
              else
                Cislo='.m50'
              endif
              if(.not.ExistFile(Veta(:idel(Veta))//Cislo(:4))) then
                Veta='the input file "'//Veta(:idel(Veta))//Cislo(:4)//
     1               '" doesn''t exist.'
                go to 1600
              endif
            enddo
            FileName(ImportType)=Veta
            Veta=' '
          else if(.not.ExistFile(FileName(ImportType))) then
            Veta='the input file doesn''t exist'
          else
            Veta=' '
          endif
1600      if(Veta.ne.' ') then
            call FeChybne(-1.,YBottomMessage,Veta(:idel(Veta))//
     1                    ', try again.',' ',SeriousError)
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1500
          endif
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(ImportType.eq.1) then
        idl=idel(FileName(ImportType))
        call iom50(0,0,FileName(ImportType)(:idl)//'.m50')
        call iom40Only(0,0,FileName(ImportType)(:idl)//'.m40')
        if(NPhase.gt.1) then
          ich=0
          id=NextQuestId()
          xqd=400.
          il=2*NPhase+1
          write(Cislo,FormI15) NPhase
          call Zhusti(Cislo)
          Veta='The structure "'//FileName(ImportType)(:idl)//
     1         '" consists of '//Cislo(:idel(Cislo))//' phases.'
          call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
          il=1
          tpom=5.
          Veta='Select the phase to be imported :'
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          xpom=10.
          tpom=xpom+CrwgXd+10.
          do i=1,NPhase
            il=il+1
            write(Veta,'(''Cell parameters:'',3f10.3,3f10.2)')
     1        (CellPar(j,1,i),j=1,6)
            j=-il*10-3
            call FeQuestCrwMake(id,tpom,il,xpom,j,Veta,'L',CrwXd,CrwYd,
     1                          0,1)
            call FeQuestCrwOpen(CrwLastMade,i.eq.1)
            il=il+1
            j=-il*10+4
            Veta='Space group: '//Grupa(i)(:idel(Grupa(i)))
            call FeQuestLblMake(id,tpom,j,Veta,'L','N')
            if(i.eq.1) nCrwFirst=CrwLastMade
          enddo
1650      call FeQuestEvent(id,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 1650
          endif
          if(ich.eq.0) then
            KPhase=1
            nCrw=nCrwFirst
            do i=1,NPhase
              if(CrwLogicQuest(nCrw)) then
                KPhase=i
                exit
              endif
              nCrw=nCrw+1
            enddo
          endif
          call FeQuestRemove(id)
          if(ich.ne.0) go to 9999
        endif
        n=0
1700    n=n+1
        write(Cislo,'(i2)') n
        if(Cislo(1:1).eq.' ') Cislo(1:1)='0'
        Veta='tmp_'//Cislo(1:2)//'.cif'
        if(ExistFile(Veta)) go to 1700
        MakeCIFForGraphicViewer=.true.
        call CIFMakeBasicTemplate(Veta,ich)
        call CIFUpdate(Veta,.true.,0)
        MakeCIFForGraphicViewer=.false.
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        FileName(2)=Veta
        ImportType=-2
      endif
      NPhase=NPhase+1
      do i=1,NPhase
        if(PhaseName(i).eq.' ') then
          write(PhaseName(i),'(''Phase#'',i2)') i
          call Zhusti(PhaseName(i))
        endif
      enddo
      itwpho=itwph
      itwph=itwph+1
      NScOld=mxscu
2000  if(sc(NScOld,1).le.0.) then
        NScOld=NScOld-1
        go to 2000
      endif
      allocate(ScOld(NScOld))
      call CopyVek(Sc(1,1),ScOld,NScOld)
      mxscutw=max(NScOld+itwph-1,6)
      mxscu=mxscutw-itwph+1
      do KDatB=1,NDatBlock
        pom=.01
        ScTw(itwph,KDatB)=pom
        pom=1.-pom
        do i=1,itwph-1
          ScTw(i,KDatB)=ScTw(i,KDatB)*pom
          kis(mxscu+i-1,KDatB)=kis(mxscu+i,KDatB)
        enddo
        kis(mxscu+itwph-1,KDatB)=1
      enddo
      KPhase=NPhase
      call ReallocSymm(NDim(KPhase),NSymm(KPhase),NLattVec(KPhase),
     1                 NComp(KPhase),NPhase,0)
      if(ImportType.eq.4) then
        StatusM50=11100
        Grupa(NPhase)='P1'
      else if(iabs(ImportType).eq.2) then
        call ReadSpecifiedCIF(FileName(iabs(ImportType)),-1)
      else if(ImportType.eq.3) then
        call ReadSpecifiedSHELX(FileName(ImportType),ich)
      endif
      if(ExistM90) call iom90(0,fln(:ifln)//'.m90')
      KPhaseTwin(itwph)=NPhase
      if(isPowder) then
        call CopyVek(GaussPwd(1,1,KDatBlock),
     1               GaussPwd(1,NPhase,KDatBlock),4)
        call CopyVek(LorentzPwd(1,1,KDatBlock),
     1               LorentzPwd(1,NPhase,KDatBlock),5)
        ZetaPwd(NPhase,KDatBlock)=ZetaPwd(1,KDatBlock)
        KProfPwd(NPhase,KDatBlock)=KProfPwd(1,KDatBlock)
        PCutOff(NPhase,KDatBlock)=PCutOff(1,KDatBlock)
      endif
      if(ImportType.eq.4) then
        call EM50GenSym(RunForFirstTimeYes,AskForDeltaNo,QSymm,ich)
        call EM50MakeStandardOrder
        call EditM50(ich)
      else
        if(ExistSingle) then
          NTwin=NTwin+1
          call FindCellTrans(CellPar(1,1,1),CellPar(1,1,KPhase),
     1                       RTw(1,NTwin),ich)
          if(ich.eq.0) then
            call MatInv(RTw(1,NTwin),RTwI(1,NTwin),det,3)
          else
            call UnitMat(RTw (1,NTwin),3)
            call UnitMat(RTwI(1,NTwin),3)
          endif
          call ReadTw(RTw,RTwI,NTwin,CellPar(1,1,1),ich)
          call iom50(1,0,fln(:ifln)//'.m50')
          call iom50(0,0,fln(:ifln)//'.m50')
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          call EM9CreateM90(0,ich)
          call CopyVek(ScOld,Sc(1,1),NScOld)
          call iom40(1,0,fln(:ifln)//'.m40')
        else
          call iom40(1,0,fln(:ifln)//'.m40')
          call iom50(1,0,fln(:ifln)//'.m50')
        endif
        if(ImportType.eq.-2) call DeleteFile(FileName(iabs(ImportType)))
        call DeleteFile(PreviousM40)
        call DeleteFile(PreviousM50)
      endif
9999  KPhase=KPhaseIn
      if(allocated(ScOld)) deallocate(ScOld)
      if(ImportType.eq.-2) ImportType=1
      return
      end
