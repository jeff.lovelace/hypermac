      subroutine CrlMakeRMag(i,isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rp(9)
      call MatInv(rm(1,i,isw,KPhase),rp,det,3)
      if(zmag(i,isw,KPhase).lt.0.) det=-det
      if(det.gt.0.) then
        call CopyMat(rm(1,i,isw,KPhase),rmag(1,i,isw,KPhase),3)
      else
        call RealMatrixToOpposite(rm(1,i,isw,KPhase),
     1                            rmag(1,i,isw,KPhase),3)
      endif
      return
      end
