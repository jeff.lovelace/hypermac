      logical function CrSystemDiff(CrSystemCell,CrSystemSymm)
      include 'fepc.cmn'
      integer CrSystemCell,CrSystemSymm
      isc=mod(CrSystemCell,10)
      iss=mod(CrSystemSymm,10)
      CrSystemDiff=.false.
      if(iss.eq.2) then
        if(isc.eq.1.or.isc.eq.5) then
          CrSystemDiff=.true.
        else if(isc.eq.2) then
          CrSystemDiff=CrSystemCell.ne.CrSystemSymm
        else if(isc.eq.6) then
          CrSystemDiff=CrSystemSymm.ne.32
        endif
      else if(iss.eq.3) then
        CrSystemDiff=isc.ne.3.and.isc.ne.4.and.isc.ne.7
      else if(iss.eq.4) then
        CrSystemDiff=isc.ne.4.and.isc.ne.7
      else if(iss.eq.5) then
        CrSystemDiff=isc.ne.5.and.isc.ne.6
      else if(iss.ne.1) then
        CrSystemDiff=isc.ne.iss
      endif
      return
      end
