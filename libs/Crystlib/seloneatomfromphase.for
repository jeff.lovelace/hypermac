      subroutine SelOneAtomFromPhase(Text,AtomP,kf,ia,n,ich)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kf(n)
      character*(*) Text,AtomP(n)
      character*8   AtomVyber(:)
      allocatable AtomVyber
      allocate(AtomVyber(NAtAll))
      nv=0
      iav=0
      do i=1,n
        if(kf(i).eq.KPhase) then
          nv=nv+1
          if(i.eq.ia) iav=nv
          AtomVyber(nv)=AtomP(i)
        endif
      enddo
      iav=max(iav,1)
      call SelOneAtom(Text,AtomVyber,iav,nv,ich)
      nv=0
      do i=1,n
        if(kf(i).eq.KPhase) then
          nv=nv+1
          if(nv.eq.iav) then
            ia=i
            go to 9999
          endif
        endif
      enddo
9999  deallocate(AtomVyber)
      return
      end
