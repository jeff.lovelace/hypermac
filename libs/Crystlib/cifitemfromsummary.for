      subroutine CIFItemFromSummary(CifKeyToBeFound,LabelToBeFound,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) CifKeyToBeFound,LabelToBeFound
      character*256 Veta
      character*80  t80
      logical EqIgCase
      ich=0
      if(LnSum.le.0) go to 9999
      n=0
      nline=0
      nloop=0
      rewind LnSum
1100  read(LnSum,FormA,end=9999) Veta
      nline=nline+1
      k=0
      call Kus(Veta,k,t80)
      if(EqIgCase(CifKeyToBeFound,t80)) go to 1200
      if(EqIgCase(t80,'loop_')) then
        nloop=nline
      else
        if(t80(1:1).eq.'_'.or.t80(1:1).eq.'#') nloop=0
      endif
      go to 1100
1200  if(nloop.gt.0) then
        m=-1
        do i=1,nline-nloop+1
          backspace LnSum
        enddo
      else
        m=1
        n=n+1
        if(n.gt.nCIFArrayUpdate)
     1     call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
        CIFArrayUpdate(n)=Veta
      endif
1500  read(LnSum,FormA,end=9999) Veta
      if(Veta.eq.' ') go to 1500
      k=0
      call Kus(Veta,k,t80)
      if(t80(1:1).eq.'#') go to 1500
      if(m.le.0) then
        if(m.lt.0) then
          if(t80(1:1).ne.'_'.and..not.EqIgCase(t80,'loop_')) m=m+2
        else
          if(t80(1:1).eq.'_') m=m+1
        endif
      else
        if(t80(1:1).eq.'_'.or.EqIgCase(t80,'loop_')) go to 9999
      endif
      n=n+1
      if(n.gt.nCIFArrayUpdate)
     1   call ReallocateCIFArrayUpdate(nCIFArrayUpdate+1000)
      CIFArrayUpdate(n)=Veta
      go to 1500
9999  if(n.gt.0) then
        call CIFUpdateRecord(CifKeyToBeFound,LabelToBeFound,n)
      else
        ich=1
      endif
      return
      end
