      subroutine iomols(klic,*)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 t80
      iak=NAtMolFrAll(KPhase)-1
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        iap=iak+1
        iak=iak+iam(i)/KPoint(i)
        if(klic.eq.0) then
          read(m40,100,err=5500,end=5500) t80(1:80)
        else
          write(m40,100,err=5500) molname(i)
        endif
        call ioats(iap,iak,klic,*5500)
        n=iak
        do 3500k=2,npoint(i)
          if(ipoint(k,i).le.0) go to 3500
          do j=iap,iak
            n=n+1
            if(sx(1,j).gt.0.) then
              call multmq(rpoint(1,k,i),sx(1,j),sx(1,n),3,3,1)
            else
              call CopyVek(sx(1,j),sx(1,n),3)
            endif
            if(itf(n).gt.1) then
              call multmq(tpoint(1,k,i),sbeta(1,j),sbeta(1,n),6,6,1)
            else
              sbeta(1,n)=sbeta(1,j)
            endif
          enddo
3500    continue
        do j=1,mam(i)
          ji=j+mxp*(i-1)
          if(klic.eq.0) then
            read(m40,101,err=5500,end=5500) t80(1:8),saimol(ji),
     1               (seuler(k,ji),k=1,3),(strans(k,ji),k=1,3)
          else
            write(t80,'(''pos#'',i2)') j
            call zhusti(t80)
            write(m40,101) t80(1:8),saimol(ji),(seuler(k,ji),k=1,3),
     1                     (strans(k,ji),k=1,3)
          endif
          if(KTLS(i).gt.0) then
            if(klic.eq.0) then
              read(m40,102,err=5500,end=5500)(stt(k,ji),k=1,6)
              read(m40,102,err=5500,end=5500)(stl(k,ji),k=1,6)
              read(m40,102,err=5500,end=5500)(sts(k,ji),k=1,9)
            else
              write(m40,102)(stt(k,ji),k=1,6)
              write(m40,102)(stl(k,ji),k=1,6)
              write(m40,102)(sts(k,ji),k=1,9)
            endif
          endif
          if(KModM(1,ji).gt.0) then
            if(klic.eq.0) then
              read(m40,102,err=5500,end=5500) sa0m(ji)
            else
              write(m40,102) sa0m(ji)
            endif
            do k=1,KModM(1,ji)
              if(klic.eq.0) then
                read(m40,102,err=5500,end=5500) saxm(k,ji),saym(k,ji)
              else
                write(m40,102) saxm(k,ji),saym(k,ji)
              endif
            enddo
          endif
          if(klic.eq.0) then
            if(KModM(2,ji).gt.0) then
              read(m40,102,err=5500,end=5500)((sutx(l,k,ji),l=1,3),
     1                     (suty(l,k,ji),l=1,3),k=1,KModM(2,ji))
              read(m40,102,err=5500,end=5500)((surx(l,k,ji),l=1,3),
     1                     (sury(l,k,ji),l=1,3),k=1,KModM(2,ji))
            endif
            if(KModM(3,ji).gt.0) then
              read(m40,102,err=5500,end=5500)((sttx(l,k,ji),l=1,6),
     1                                             k=1,KModM(3,ji))
              read(m40,102,err=5500,end=5500)((stty(l,k,ji),l=1,6),
     1                                             k=1,KModM(3,ji))
              read(m40,102,err=5500,end=5500)((stlx(l,k,ji),l=1,6),
     1                                             k=1,KModM(3,ji))
              read(m40,102,err=5500,end=5500)((stly(l,k,ji),l=1,6),
     1                                             k=1,KModM(3,ji))
              read(m40,102,err=5500,end=5500)((stsx(l,k,ji),l=1,9),
     1                                             k=1,KModM(3,ji))
              read(m40,102,err=5500,end=5500)((stsy(l,k,ji),l=1,9),
     1                                             k=1,KModM(3,ji))
            endif
            if(KModM(1,ji).gt.0.or.KModM(2,ji).gt.0.or.
     1         KModM(3,ji).gt.0)
     2        read(m40,102,err=5500,end=5500) sphfm(ji)
          else
            if(KModM(2,ji).gt.0) then
              write(m40,102)((sutx(l,k,ji),l=1,3),(suty(l,k,ji),l=1,3)
     1                      ,k=1,KModM(2,ji))
              write(m40,102)((surx(l,k,ji),l=1,3),(sury(l,k,ji),l=1,3)
     1                      ,k=1,KModM(2,ji))
            endif
            if(KModM(3,ji).gt.0) then
              write(m40,102)((sttx(l,k,ji),l=1,6),k=1,KModM(3,ji))
              write(m40,102)((stty(l,k,ji),l=1,6),k=1,KModM(3,ji))
              write(m40,102)((stlx(l,k,ji),l=1,6),k=1,KModM(3,ji))
              write(m40,102)((stly(l,k,ji),l=1,6),k=1,KModM(3,ji))
              write(m40,102)((stsx(l,k,ji),l=1,9),k=1,KModM(3,ji))
              write(m40,102)((stsy(l,k,ji),l=1,9),k=1,KModM(3,ji))
            endif
            if(KModM(1,ji).gt.0.or.KModM(2,ji).gt.0.or.
     1         KModM(3,ji).gt.0) write(m40,102) sphfm(ji)
          endif
        enddo
        iak=n
      enddo
      return
5500  return 1
100   format(a8)
101   format(a8,10x,f9.6/3f9.3,3f9.6)
102   format(6f9.6)
      end
