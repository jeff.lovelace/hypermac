      subroutine MagFFFromAtomFile(AtTypeToRead,AtTypeMagToRead,Label,
     1                             FF,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) AtTypeToRead,AtTypeMagToRead,Label
      character*256 t256
      character*80  Veta
      character*7   At,AtMagPom
      dimension FF(*)
      logical EqIgCase
      AtMagPom=AtTypeMagToRead
      i=idel(AtMagPom)
      if(EqIgCase(AtMagPom(i:i),'m')) AtMagPom(i:i)=' '
      if(OpSystem.le.0) then
        t256=HomeDir(:idel(HomeDir))//'formfac'//ObrLom//'atoms.dat'
      else
        t256=HomeDir(:idel(HomeDir))//'formfac/atoms.dat'
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9200
      read(ln,FormA) t256
      if(t256(1:1).ne.'#') rewind ln
      ierr=0
1000  read(ln,'(a7)',end=9000,err=9100) At
      if(EqIgCase(At,AtTypeToRead)) then
2000    read(ln,FormA,end=9000,err=9100) t256
        if(EqIgCase(t256,'end')) go to 9999
        k=0
        call kus(t256,k,Veta)
        if(EqIgCase(Label,Veta)) then
2500      read(ln,FormA,end=9050,err=9100) t256
          k=0
          call Kus(t256,k,Veta)
          if(EqIgCase(Veta,AtMagPom)) then
            call StToReal(t256,k,FF,7,.false.,ich)
            if(ich.eq.0) then
              go to 9999
            else
              go to 9100
            endif
          else if(EqIgCase(t256,'end')) then
            go to 9999
          endif
          go to 2500
        else
          go to 2000
        endif
      else
3000    read(ln,FormA,end=9000,err=9100) t256
        if(.not.EqIgCase(t256,'end')) go to 3000
      endif
      go to 1000
9000  iErr=1
      go to 9999
9050  iErr=2
      go to 9999
9100  call FeReadError(ln)
      iErr=3
      go to 9999
9200  iErr=4
9999  call CloseIfOpened(ln)
      return
      end
