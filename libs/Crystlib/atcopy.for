      subroutine AtCopy(n,m)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      call CopyBasicKeysForAtom(n,m)
      i=KModA(1,n)
      if(MaxNDimI.gt.0) call CopyVek(qcnt(1,n),qcnt(1,m),MaxNDimI)
      if(i.gt.0) then
        call CopyVek(ax(1,n),ax(1,m),i)
        call CopyVek(ay(1,n),ay(1,m),i)
        call CopyVek(sax(1,n),sax(1,m),i)
        call CopyVek(say(1,n),say(1,m),i)
      endif
      i=3*KModA(2,n)
      if(i.gt.0) then
        call CopyVek(ux(1,1,n),ux(1,1,m),i)
        call CopyVek(uy(1,1,n),uy(1,1,m),i)
        call CopyVek(sux(1,1,n),sux(1,1,m),i)
        call CopyVek(suy(1,1,n),suy(1,1,m),i)
      endif
      i=6*KModA(3,n)
      if(i.gt.0) then
        call CopyVek(bx(1,1,n),bx(1,1,m),i)
        call CopyVek(by(1,1,n),by(1,1,m),i)
        call CopyVek(sbx(1,1,n),sbx(1,1,m),i)
        call CopyVek(sby(1,1,n),sby(1,1,m),i)
      endif
      if(itf(n).gt.2) then
        i=10
        call CopyVek(c3(1,n),c3(1,m),i)
        call CopyVek(sc3(1,n),sc3(1,m),i)
        i=KModA(4,n)*i
        if(i.gt.0) then
          call CopyVek(c3x(1,1,n),c3x(1,1,m),i)
          call CopyVek(c3y(1,1,n),c3y(1,1,m),i)
          call CopyVek(sc3x(1,1,n),sc3x(1,1,m),i)
          call CopyVek(sc3y(1,1,n),sc3y(1,1,m),i)
        endif
        if(itf(n).gt.3) then
          i=15
          call CopyVek(c4(1,n),c4(1,m),i)
          call CopyVek(sc4(1,n),sc4(1,m),i)
          i=KModA(5,n)*i
          if(i.gt.0) then
            call CopyVek(c4x(1,1,n),c4x(1,1,m),i)
            call CopyVek(c4y(1,1,n),c4y(1,1,m),i)
            call CopyVek(sc4x(1,1,n),sc4x(1,1,m),i)
            call CopyVek(sc4y(1,1,n),sc4y(1,1,m),i)
          endif
          if(itf(n).gt.4) then
            i=21
            call CopyVek(c5(1,n),c5(1,m),i)
            call CopyVek(sc5(1,n),sc5(1,m),i)
            i=KModA(6,n)*i
            if(i.gt.0) then
              call CopyVek(c5x(1,1,n),c5x(1,1,m),i)
              call CopyVek(c5y(1,1,n),c5y(1,1,m),i)
              call CopyVek(sc5x(1,1,n),sc5x(1,1,m),i)
              call CopyVek(sc5y(1,1,n),sc5y(1,1,m),i)
            endif
            if(itf(n).gt.5) then
              i=28
              call CopyVek(c6(1,n),c6(1,m),i)
              call CopyVek(sc6(1,n),sc6(1,m),i)
              i=KModA(7,n)*i
              if(i.gt.0) then
                call CopyVek(c6x(1,1,n),c6x(1,1,m),i)
                call CopyVek(c6y(1,1,n),c6y(1,1,m),i)
                call CopyVek(sc6x(1,1,n),sc6x(1,1,m),i)
                call CopyVek(sc6y(1,1,n),sc6y(1,1,m),i)
              endif
            endif
          endif
        endif
      endif
      if(MagneticType(kswa(n)).ne.0) then
        if(MagPar(n).gt.0) then
          call CopyVek( sm0(1,n), sm0(1,m),3)
          call CopyVek(ssm0(1,n),ssm0(1,m),3)
          if(MagPar(n).gt.1) then
            i=(MagPar(n)-1)*3
            call CopyVek( smx(1,1,n), smx(1,1,m),i)
            call CopyVek( smy(1,1,n), smy(1,1,m),i)
            call CopyVek(ssmx(1,1,n),ssmx(1,1,m),i)
            call CopyVek(ssmy(1,1,n),ssmy(1,1,m),i)
          endif
          KUsePolar(m)=KUsePolar(n)
        else
          KUsePolar(m)=0
        endif
      else
        KUsePolar(m)=0
      endif
      if(NDimI(kswa(n)).gt.0) then
        phf(m)=phf(n)
        sphf(m)=sphf(n)
      endif
      if(ChargeDensities) then
        popc(m)=popc(n)
        spopc(m)=spopc(n)
        popv(m)=popv(n)
        spopv(m)=spopv(n)
        kapa1(m)=kapa1(n)
        skapa1(m)=skapa1(n)
        kapa2(m)=kapa2(n)
        skapa2(m)=skapa2(n)
        i=lasmax(n)-1
        if(i.gt.1) then
          call CopyVek(popas(1,n),popas(1,m),(i+1)**2)
          call CopyVek(spopas(1,n),spopas(1,m),(i+1)**2)
        endif
        SmbPGAt(m)=SmbPGAt(n)
        NPGAt(m)=NPGAt(n)
        call CopyVek(RPGAt(1,1,n),RPGAt(1,1,m),9)
        LocAtSystSt(1,m)=LocAtSystSt(1,n)
        LocAtSystSt(2,m)=LocAtSystSt(2,n)
        call CopyVek(LocAtSystX(1,1,n),LocAtSystX(1,1,m),6)
        LocAtSystAx(m)=LocAtSystAx(n)
        LocAtSense(m)=LocAtSense(n)
        call CopyMat(TrAt(1,n),TrAt(1,m),3)
        call CopyMat(TriAt(1,n),TriAt(1,m),3)
        call CopyMat(TroAt(1,n),TroAt(1,m),3)
        call CopyMat(TroiAt(1,n),TroiAt(1,m),3)
      endif
      call CopyVekI(KiA(1,n),KiA(1,m),mxda)
      DelkaKiAtomu(m)=DelkaKiAtomu(n)
      PrvniKiAtomu(m)=PrvniKiAtomu(n)
      return
      end
