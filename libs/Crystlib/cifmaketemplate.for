      subroutine CIFMakeTemplate(FileOut,ich)
      use Basic_mod
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension isub(8),ijour(24),ichem(11),ipwdc(11),
     1          icompute(7)
      character*(*) FileOut
      character*256 Veta
      character*80 t80,p80
      character*2  nty
      logical ExistFile,JenZaklad,FeYesNo,CIFAllocated
      data isub/5,2,3,4,6,13,11,7/
     4     ijour/19,13,12,11,15,14,17,16,7,4,8,30,26,0,34,1,23,37,36,21,
     5          24,25,27,28/,
     6     ichem/7,4,21,22,19,20,23,24,3,2,1/,
     7     ipwdc/54,55,56,50,49,53,175,174,169,170,171/,
     8     icompute/2,1,3,7,6,4,5/
      KRefBlockIn=KRefBlock
      FileOut=fln(:ifln)//'.cif'
      call FeFileManager('Choose cif file name',FileOut,'*.cif',0,
     1                   .true.,ich)
      if(ich.ne.0.or.FileOut.eq.' ') go to 9999
      if(ExistFile(FileOut)) then
        if(.not.FeYesNo(-1.,-1.,'The file "'//FileOut(:idel(FileOut))//
     1                  '" already exists, rewrite it?',0)) then
          ich=1
          go to 9999
        endif
      endif
      JenZaklad=.false.
      go to 1000
      entry CIFMakeBasicTemplate(FileOut,Klic)
      KRefBlockIn=KRefBlock
      JenZaklad=.true.
1000  CIFAllocated=allocated(CifKey)
      if(.not.CIFAllocated) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9990
      endif
      ln=0
      lno=NextLogicNumber()
      call OpenFile(lno,FileOut(:idel(FileOut)),'formatted','unknown')
      if(ErrFlag.ne.0) go to 9100
      if(JenZaklad) go to 1500
      call WriteCIFFooter(lno)
      write(lno,FormA) 'data_global'
      write(lno,FormA1) '#',('=',i=1,71)
      write(lno,FormA)
      t80='''Jana2006 '//VersionString(:idel(VersionString))//''''
      call FeDelTwoSpaces(t80)
      write(lno,FormCIF) CifKey(3,4),t80(:idel(t80))
      write(lno,FormA)
      call WriteCIFHeader(lno,'# 1. PROCESSING SUMMARY (IUCr Office '//
     1                   'Use Only)')
      do i=1,24
        j=ijour(i)
        if(j.eq.8.or.j.eq.34) then
          write(lno,111) CifKey(j,13)
        else if(j.eq.23) then
          write(lno,FormCIF) CifKey(j,13),
     1                       '''Acta Crystallographica Section C'''
        else if(j.eq.0) then
          cycle
        else
          write(lno,FormCIF) CifKey(j,13),'?'
        endif
      enddo
      call WriteCIFFooter(lno)
      call WriteCIFHeader(lno,'# 2. SUBMISSION DETAILS')
      do i=1,8
        j=isub(i)
        if(j.eq.2.or.j.eq.7) then
          write(lno,111) CifKey(j,14)
        else if(j.eq.13) then
          write(lno,FormCIF) CifKey(j,14),
     1                       '''Acta Crystallographica Section C'''
        else
          write(lno,FormCIF) CifKey(j,14),'?'
        endif
        if(j.eq.6.or.j.eq.11) write(lno,FormA)
      enddo
      call WriteCIFFooter(lno)
      call WriteCIFHeader(lno,'# 3. TITLE AND AUTHOR LIST')
      write(lno,111) CifKey(14,14)
      write(lno,111) CifKey(15,14)
      write(lno,102)
      do i=31,29,-1
        write(lno,FormCIF) ' '//CifKey(i,14)
      enddo
      write(lno,'(''; ?      # name    ''/'';'')')
      write(lno,'(''; ?      # footnote''/'';'')')
      write(lno,'(''; ?      # address ''/'';'')')
      call WriteCIFFooter(lno)
      call WriteCIFHeader(lno,'# 4. TEXT')
      do i=16,28
        if(i.ne.26) then
          write(lno,111) CifKey(i,14)
        else
          write(lno,FormCIF) CifKey(i,14)
          write(lno,'('';'')')
          write(lno,'('';'')')
        endif
      enddo
      call WriteCIFFooter(lno)
1500  write(lno,FormA) 'data_I'
      if(.not.JenZaklad) then
        write(lno,FormA1) '#',('=',i=1,71)
        call WriteCIFHeader(lno,'# 5. CHEMICAL DATA')
      endif
      do 2010i=1,11
        j=ichem(i)
        if(JenZaklad.and.j.ne.23) go to 2010
        if(j.eq.7) then
          write(lno,111) CifKey(j,6)
        else
          write(lno,FormCIF) CifKey(j,6),'?'
        endif
2010  continue
      if(.not.JenZaklad) then
        call WriteCIFFooter(lno)
        if(isPowder) then
          t80='# 6. POWDER SPECIMEN AND CRYSTAL DATA'
        else
          t80='# 6. CRYSTAL DATA'
        endif
        call WriteCIFHeader(lno,t80)
      endif
      write(lno,FormCIF) CifKey(1,18),'?'
      if(NDimI(KPhase).eq.1) then
        m=2
        n=21
      else
        m=4
        n=18
      endif
      write(lno,FormCIF) CifKey(m,n),'?'
      if(NDimI(KPhase).le.0) then
        write(lno,FormCIF) CifKey(3,n),'?'
        write(lno,FormCIF) CifKey(2,n),'?'
      endif
      write(lno,FormA)
      write(lno,102)
      n=21
      if(NDimI(KPhase).eq.0) then
        if(MagneticType(KPhase).gt.0) then
          i=39
          j=41
        else
          i=8
          j=9
        endif
      else
        if(MagneticType(KPhase).gt.0) then
          i=69
          j=70
        else
          i=6
          j=7
        endif
      endif
!   zatim
!      Veta='_space_group_symop.magn_id'
      Veta=CifKey(i,n)
      write(lno,FormA) ' '//Veta(:idel(Veta))
      write(lno,FormA) ' '//CifKey(j,n)(:idel(CifKey(j,n)))
      write(lno,'('' ? ?'')')
      write(lno,FormA)
      if(MagneticType(KPhase).gt.0) then
        n=21
        if(NDimI(KPhase).eq.0) then
          i=46
          j=48
        else
          i=73
          j=74
        endif
        write(lno,102)
        write(lno,FormA) ' '//CifKey(i,n)(:idel(CifKey(i,n)))
        write(lno,FormA) ' '//CifKey(j,n)(:idel(CifKey(j,n)))
        write(lno,'('' ? ?'')')
        write(lno,FormA)
      endif
      m=5
      do i=1,6
        write(lno,FormCIF) CifKey(m,5),'?'
        if(i.ne.3) then
          m=m+1
        else
          m=1
        endif
      enddo
      write(lno,FormCIF) CifKey(16,5),'?'
      write(lno,FormA)
      if(.not.isPowder) then
        n=23
        write(lno,102)
        do m=6,16
          write(lno,FormCIF) ' '//CifKey(m,n)
        enddo
        write(lno,'('' ? ? ? ? ? ? ? ? ? ? ?'')')
      endif
      write(lno,FormA)
      if(NDimI(KPhase).gt.0) then
        write(lno,FormA)
        write(Cislo,FormI15) NDimI(KPhase)
        call ZdrcniCisla(Cislo,1)
        write(lno,FormCIF) CifKey(21,5),Cislo
        write(lno,102)
        write(lno,FormCIF) ' '//CifKey(150,5)
        do i=147,149
          write(lno,FormCIF) ' '//CifKey(i,5)
        enddo
        do i=1,NDimI(KPhase)
          write(lno,'('' ? ? ?'')')
        enddo
        if(NComp(KPhase).gt.1) then
          write(Cislo,FormI15) NComp(KPhase)
          call ZdrcniCisla(Cislo,1)
          write(lno,FormCIF) CifKey(146,5),Cislo(:idel(Cislo))
          write(lno,102)
          write(lno,FormCIF) ' '//CifKey(24,5)
          write(lno,FormCIF) ' '//CifKey(23,5)
          p80=CifKey(25,5)
          idl=index(p80,'W')
          do i=1,NDim(KPhase)
            do j=1,NDim(KPhase)
              write(t80,'(2(''_'',i2))') i,j
              call zhusti(t80)
              p80=p80(:idl)//t80(:idel(t80))
              write(lno,FormA) p80(:idel(p80))
            enddo
          enddo
          do i=1,NComp(KPhase)
            write(lno,'(''?'',14x,'''''''',i1,''-'',a2,
     1                  '' subsystem'''''')') i,nty(i)
            do j=1,NDim(KPhase)
              write(lno,'(6(2x,a1))')('?',k=1,NDim(KPhase))
            enddo
          enddo
        endif
        if(KCommen(KPhase).gt.0) then
          write(lno,FormA)
          m=1
          n=22
          write(lno,FormCIF) CifKey(m,n),'?'
          m=10
          write(lno,FormCIF) CifKey(m,n),'?'
          write(lno,FormA)
        endif
      endif
      write(lno,FormCIF) CifKey(4,5),'?'
      if(JenZaklad) go to 2500
      if(.not.isPowder) then
        write(lno,FormA)
        write(lno,FormCIF) CifKey(10,5),'?'
        write(lno,FormCIF) CifKey(13,5),'?'
        write(lno,FormCIF) CifKey(12,5),'?'
      endif
      write(lno,FormCIF) CifKey(11,5),'?'
      write(lno,111) CifKey(15,5)
      write(lno,FormA)
      do i=9,12
        if(i.ne.11) write(lno,FormCIF) CifKey(i,11),'?'
      enddo
      write(lno,FormCIF) CifKey(14,11),'?'
      write(lno,FormA)
      if(isPowder) then
        do i=1,11
          if(i.eq.49) then
            write(lno,111) CifKey(ipwdc(i),20)
          else
            write(lno,FormCIF) CifKey(ipwdc(i),20),'?'
          endif
        enddo
        write(lno,FormA)
      endif
      write(lno,FormCIF) CifKey(1,11),'?'
      if(.not.isPowder) then
        write(lno,FormCIF) CifKey(13,11),'?'
        do i=17,20
          write(lno,FormCIF) CifKey(i,11),'?'
        enddo
        write(lno,FormCIF) CifKey(8,11),'?'
      endif
      do i=4,5
        write(lno,FormCIF) CifKey(i,11),'?'
      enddo
      do i=1,2
        write(lno,FormCIF) CifKey(4-i,11),'?'
      enddo
      write(lno,102)
      t80=' '
      do i=25,28
        write(lno,FormCIF) ' '//CifKey(i,11)
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      if(.not.JenZaklad) then
        call WriteCIFFooter(lno)
        call WriteCIFHeader(lno,'# 7. EXPERIMENTAL DATA')
      endif
      write(lno,FormCIF) CifKey(7,11),'?'
      if(isPowder) then
        write(lno,111) CifKey(83,20)
        write(lno,111) CifKey(47,20)
      endif
      write(lno,FormA)
      write(lno,FormCIF) CifKey(3,10),'?'
      write(lno,FormCIF) CifKey(110,10),'?'
      if(isPowder) then
        write(lno,FormCIF) CifKey(115,10),'?'
        write(lno,FormCIF) CifKey(116,10),'?'
      else
        write(lno,FormCIF) CifKey(113,10),'?'
        write(lno,FormCIF) CifKey(117,10),'?'
        write(lno,FormCIF) CifKey(111,10),'?'
      endif
      write(lno,FormCIF) CifKey(49,10),'?'
      if(.not.isPowder)
     1  write(lno,FormCIF) CifKey(109,10),'?'
      write(lno,FormCIF) CifKey(51,10),'?'
      write(lno,FormCIF) CifKey(45,10),'?'
      write(lno,FormCIF) CifKey(18,10),'?'
      write(lno,FormCIF) CifKey(20,10),'?'
      if(isPowder) then
        write(lno,FormCIF) CifKey(11,10),'?'
        write(lno,FormCIF) CifKey(14,10),'?'
        write(lno,FormA)
        write(lno,FormCIF) CifKey(31,20),'?'
        write(lno,111) CifKey(32,20)
        write(lno,FormCIF) CifKey(27,20),'?'
        do i=4,5
          write(lno,FormCIF) CifKey(i,20),'?'
        enddo
        write(lno,FormCIF) CifKey(6,20),'?'
      else
        write(lno,FormCIF) CifKey(11,10),'?'
        write(lno,FormCIF) CifKey(12,10),'?'
        write(lno,FormCIF) CifKey(21,10),'?'
        write(lno,FormCIF) CifKey(22,10),'?'
        write(lno,FormA)
        write(lno,FormCIF) CifKey(93,10),'?'
        do i=1,2
          write(lno,FormCIF) CifKey(98-i,10),'?'
        enddo
        write(lno,FormCIF) CifKey( 95,10),'?'
        write(lno,FormCIF) CifKey(  6,10),'?'
        write(lno,FormCIF) CifKey(  5,10),'?'
        write(lno,FormCIF) CifKey( 85,10),'?'
        write(lno,FormCIF) CifKey(130,10),'?'
        m=88
        do j=1,2*NDim(KPhase)
          write(lno,FormCIF) CifKey(m,10),'?'
          if(j.eq.6) m=140
          if(mod(j,2).eq.1) then
            m=m-1
          else
            m=m+3
          endif
        enddo
        write(lno,FormCIF) CifKey(94,10),'?'
        write(lno,FormA)
        write(lno,FormCIF) CifKey(125,10),'?'
        write(lno,FormCIF) CifKey(123,10),'?'
        write(lno,FormCIF) CifKey(124,10),'?'
        write(lno,FormCIF) CifKey(122,10),'?'
        write(lno,102)
        do i=119,121
          write(lno,FormCIF) CifKey(i,10)
        enddo
        write(lno,FormA) '  ? ? ?'
      endif
      call WriteCIFFooter(lno)
      call WriteCIFHeader(lno,'# 8. REFINEMENT DATA')
      if(isPowder) then
        write(lno,111) CifKey(146,20)
        write(lno,FormCIF) CifKey(145,20),'?'
        write(lno,FormCIF) CifKey(139,20),'?'
        write(lno,FormCIF) CifKey(141,20),'?'
        do i=1,3
          write(lno,FormCIF) CifKey(141+i,20),'?'
        enddo
        write(lno,FormA)
      endif
      write(lno,111) CifKey(41,15)
      write(lno,FormA)
      if(.not.isPowder) then
        do i=12,10,-2
          write(lno,FormCIF) CifKey(i,17),'?'
        enddo
        write(lno,FormCIF) CifKey(15,17),'?'
        write(lno,FormA)
        write(lno,FormCIF) CifKey(34,15),'?'
      endif
      do i=1,4
        if(i.eq.1) then
          m=23
        else if(i.eq.2) then
          m=38
        else if(i.eq.3) then
          m=22
        else
          if(isPowder) then
            m=37
          else
            m=40
          endif
        endif
        write(lno,FormCIF) CifKey(m,15),'?'
      enddo
      if(isPowder) then
        write(lno,FormCIF) CifKey(12,15),'?'
      else
        write(lno,FormCIF) CifKey(15,15),'?'
        write(lno,FormCIF) CifKey(13,15),'?'
      endif
      write(lno,FormCIF) CifKey(28,15),'?'
      write(lno,FormCIF) CifKey(27,15),'?'
      if(isPowder) then
        j=2
      else
        j=1
      endif
      do i=j,2
        write(lno,FormCIF) CifKey(21-i,15),'?'
      enddo
      write(lno,FormCIF) CifKey(21,15),'?'
      write(lno,FormCIF) CifKey(18,15),'?'
      write(lno,FormCIF) CifKey(36,15),'?'
      write(lno,FormCIF) CifKey(35,15),'?'
      write(lno,FormCIF) CifKey(16,15),'?'
      do i=32,33
        write(lno,FormCIF) CifKey(i,15),'?'
      enddo
      do i=1,2
        write(lno,FormCIF) CifKey(i,15),'?'
      enddo
      write(lno,FormCIF) CifKey(11,15),'?'
      write(lno,FormCIF) CifKey(9,15),'?'
      if(.not.isPowder) then
        do i=4,6
          write(lno,FormCIF) CifKey(i,15),'?'
        enddo
      endif
      if(isPowder.and..not.JenZaklad) then
        write(lno,FormA)
        do i=115,117
          write(lno,FormCIF) CifKey(i,20),'?'
        enddo
        write(lno,FormCIF) CifKey(138,20),'?'
        write(lno,FormA)
        write(lno,FormCIF) CifKey(1,20),'?'
        write(lno,FormA)
        write(lno,FormCIF) CifKey(128,20),'?'
        write(lno,FormCIF) CifKey(126,20),'?'
      endif
2500  write(lno,FormA)
      write(lno,102)
      write(lno,FormCIF) ' '//CifKey(21,3)
      t80=' ? ? ?'
      if(Radiation(KDatBlock).eq.XRayRadiation) then
        write(lno,FormCIF) ' '//CifKey(17,3)
        write(lno,FormCIF) ' '//CifKey(16,3)
        t80=t80(:idel(t80))//' ?'
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        if(MagneticType(KPhase).ne.0) then
          write(lno,FormCIF) ' '//CifKey(56,3)
          t80=t80(:idel(t80))//' ?'
        endif
        write(lno,FormCIF) ' '//CifKey(18,3)
      endif
      write(lno,FormCIF) CifKey(19,3)
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      if(.not.JenZaklad) then
        do i=1,7
          j=icompute(i)
          write(lno,FormCIF) CifKey(j,8),'?'
        enddo
        write(lno,FormA)
        write(lno,102)
        t80=' '
        do i=1,nCIFRestDist
          p80=' '//CifKey(CIFRestDist(i),24)
          write(lno,FormA) p80(:idel(p80))
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
        write(lno,102)
        t80=' '
        do i=1,nCIFRestAngle
          p80=' '//CifKey(CIFRestAngle(i),24)
          write(lno,FormA) p80(:idel(p80))
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
        write(lno,102)
        t80=' '
        do i=1,nCIFRestTorsion
          p80=' '//CifKey(CIFRestTorsion(i),24)
          write(lno,FormA) p80(:idel(p80))
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
        write(lno,102)
        t80=' '
        do i=1,nCIFRestEqDist
          p80=' '//CifKey(CIFRestEqDist(i),24)
          write(lno,FormA) p80(:idel(p80))
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
        write(lno,102)
        t80=' '
        do i=1,nCIFRestEqAngle
          p80=' '//CifKey(CIFRestEqAngle(i),24)
          write(lno,FormA) p80(:idel(p80))
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
        write(lno,102)
        t80=' '
        do i=1,nCIFRestEqTorsion
          p80=' '//CifKey(CIFRestEqTorsion(i),24)
          write(lno,FormA) p80(:idel(p80))
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
        call CloseIfOpened(ln)
        call WriteCIFFooter(lno)
        call WriteCIFHeader(lno,'# 9. ATOMIC COORDINATES AND '//
     1                      'DISPLACEMENT PARAMETERS')
      endif
      write(lno,102)
      t80=' '
      do i=1,nCIFAtX
        write(lno,FormCIF) ' '//CifKey(CIFAtX(i),1)
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      do i=1,nCIFAtT
        write(lno,FormCIF) ' '//CifKey(CIFAtT(i),1)
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      m=81
      mm=8
      do nn=3,6
        write(lno,102)
        t80=' '
        mm=mm+nn+1
        do i=1,mm
          m=m+1
          write(lno,FormCIF) ' '//CifKey(m,22)
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
      enddo
      if(MagneticType(KPhase).gt.0) then
        write(lno,102)
        write(lno,FormA) ' '//CifKey(116,1)(:idel(CifKey(116,1)))
        write(lno,FormA) ' '//CifKey(117,1)(:idel(CifKey(117,1)))
        t80=' ? ?'
        m=122
        do i=1,3
          m=m+1
          write(lno,FormA) ' '//CifKey(m,1)(:idel(CifKey(m,1)))
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
      endif
      if(NDimI(KPhase).le.0) go to 5000
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=1
      if(CIFNewFourierWaves) then
        write(lno,FormA) ' '//CifKey(64,n)(:idel(CifKey(64,n)))
        t80=t80(:idel(t80))//' ?'
        n=22
        do j=1,NDimI(KPhase)
          write(lno,FormA) ' '//CifKey(52+j,n)(:idel(CifKey(52+j,n)))
          t80=t80(:idel(t80))//' ?'
        enddo
      else
        do j=64,67
          write(lno,FormA) ' '//CifKey(j,n)(:idel(CifKey(j,n)))
          t80=t80(:idel(t80))//' ?'
        enddo
      endif
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=22
      do j=49,52
        write(lno,FormA) ' '//CifKey(j,n)(:idel(CifKey(j,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      n=22
      t80=' '
      do m=200,201
        write(lno,FormA) CifKey(m,n)(:idel(CifKey(m,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      n=1
      t80=' '
      do i=1,nCIFAtSM
        m=CIFAtSM(i)
        write(lno,FormA) CifKey(m,n)(:idel(CifKey(m,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      n=22
      t80=' '
      do i=28,30
        write(lno,FormA) CifKey(i,n)(:idel(CifKey(i,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      n=22
      t80=' '
      do i=40,43
        write(lno,FormA) CifKey(i,n)(:idel(CifKey(i,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=1
      do i=103,105
        write(lno,FormA) CifKey(i,n)(:idel(CifKey(i,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=1
      do i=1,nCIFAtXM
        m=CIFAtXM(i)
        write(lno,FormA) CifKey(m,n)(:idel(CifKey(m,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=22
      do i=24,27
        write(lno,FormA) CifKey(i,n)(:idel(CifKey(i,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=22
      do i=35,39
        write(lno,FormA) CifKey(i,n)(:idel(CifKey(i,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=1
      do i=97,102
        write(lno,FormA) CifKey(i,n)(:idel(CifKey(i,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=1
      do i=1,nCIFAtTM
        m=CIFAtTM(i)
        write(lno,FormA) CifKey(m,n)(:idel(CifKey(m,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=22
      do i=31,34
        write(lno,FormA) CifKey(i,n)(:idel(CifKey(i,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      n=22
      do i=44,48
        write(lno,FormA) CifKey(i,n)(:idel(CifKey(i,n)))
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      mm=0
      do nn=3,6
        write(lno,102)
        t80=' '
        n=22
        do i=1,nCIFAtCM
          m=CIFAtCM(i)+mm
          write(lno,FormA) CifKey(m,n)(:idel(CifKey(m,n)))
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
        mm=mm+9
      enddo
      if(MagneticType(KPhase).ne.0) then
        write(lno,102)
        write(lno,FormA) ' '//CifKey(108,1)(:idel(CifKey(108,1)))
        write(lno,FormA) ' '//CifKey(109,1)(:idel(CifKey(109,1)))
        write(lno,FormA) ' '//CifKey(110,1)(:idel(CifKey(110,1)))
        t80=' ? ? ?'
        m=111
        do i=1,2
          write(lno,FormA) ' '//CifKey(m,1)(:idel(CifKey(m,1)))
          t80=t80(:idel(t80))//' ?'
          m=m+1
        enddo
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
      endif
5000  if(JenZaklad) go to 9100
      call WriteCIFFooter(lno)
      call WriteCIFHeader(lno,'# 10. MOLECULAR GEOMETRY')
      write(lno,FormA)
      write(lno,102)
      t80=' '
      do i=10,15
        if(i.eq.12.or.i.eq.13) cycle
        j=i
        if(i.ge.14.and.i.le.15.and.NDimI(KPhase).gt.0) j=j+39
        write(lno,FormCIF) ' '//CifKey(j,12)
        t80=t80(:idel(t80))//' ?'
      enddo
      if(NDimI(KPhase).le.0) then
        write(lno,FormCIF) ' '//CifKey(12,12)
        t80=t80(:idel(t80))//' ?'
      else
        do i=52,50,-1
          write(lno,FormCIF) ' '//CifKey(i,12)
          t80=t80(:idel(t80))//' ?'
        enddo
      endif
      write(lno,FormCIF) ' '//CifKey(13,12)
      t80=t80(:idel(t80))//' ?'
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      do i=3,9
        if(i.eq.6) cycle
        j=i
        if(i.ge.7.and.i.le.9.and.NDimI(KPhase).gt.0) j=j+40
        write(lno,FormCIF) ' '//CifKey(j,12)
        t80=t80(:idel(t80))//' ?'
      enddo
      if(NDimI(KPhase).le.0) then
        write(lno,FormCIF) ' '//CifKey(2,12)
        t80=t80(:idel(t80))//' ?'
      else
        do i=46,44,-1
          write(lno,FormCIF) ' '//CifKey(i,12)
          t80=t80(:idel(t80))//' ?'
        enddo
      endif
      write(lno,FormCIF) ' '//CifKey(6,12)
      t80=t80(:idel(t80))//' ?'
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      do i=34,42
        if(i.eq.38) cycle
        j=i
        if(i.ge.39.and.i.le.42.and.NDimI(KPhase).gt.0) j=j+24
        write(lno,FormCIF) ' '//CifKey(j,12)
        t80=t80(:idel(t80))//' ?'
      enddo
      if(NDimI(KPhase).le.0) then
        write(lno,FormCIF) ' '//CifKey(33,12)
        write(lno,FormCIF) ' '//CifKey(38,12)
        t80=t80(:idel(t80))//' ? ?'
      else
        do i=62,60,-1
          write(lno,FormCIF) ' '//CifKey(i,12)
          t80=t80(:idel(t80))//' ?'
        enddo
      endif
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      write(lno,102)
      t80=' '
      do i=1,nCIFHBonds
        write(lno,FormCIF) ' '//CifKey(CIFHBonds(i),12)
        t80=t80(:idel(t80))//' ?'
      enddo
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
      call WriteCIFFooter(lno)
      if(isPowder) then
        call WriteCIFHeader(lno,'# 11. POWDER PROFILE')
        write(lno,102)
        t80=' '
        if(isTOF) then
          do i=1,nCIFPwdTOF
            m=CIFPwdTOF(i)
            write(lno,FormCIF) ' '//CifKey(m,20)
            t80=t80(:idel(t80))//' ?'
          enddo
        else
          do i=1,nCIFPwdProf
            m=CIFPwdProf(i)
            write(lno,FormCIF) ' '//CifKey(m,20)
            t80=t80(:idel(t80))//' ?'
          enddo
        endif
        write(lno,FormA) t80(:idel(t80))
        call WriteCIFHeader(lno,'# 12. STRUCTURE-FACTOR LIST')
        t80=' '
        write(lno,102)
        ll=16
        do m=13,15
          write(lno,FormCIF) ' '//CifKey(m,ll)
          t80=t80(:idel(t80))//' ?'
        enddo
        do m=32,31+NDimI(KPhase)
          write(lno,FormCIF) ' '//CifKey(m,ll)
          t80=t80(:idel(t80))//' ?'
        enddo
        do m=1,4
          if(m.eq.3) cycle
          write(lno,FormCIF) ' '//CifKey(CIFSingleRfl(m),ll)
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormCIF) ' '//CifKey(31,ll)
        t80=t80(:idel(t80))//' ?'
      else
        call WriteCIFHeader(lno,'# 11. STRUCTURE-FACTOR LIST')
        if(Radiation(KDatBlock).eq.ElectronRadiation.and.CalcDyn) then
          write(lno,102)
          write(lno,FormCIF) ' '//CifKey(16,17)
          write(lno,FormCIF) ' '//CifKey(19,17)
          t80=' ? ?'
          write(lno,FormA) t80(:idel(t80))
          write(lno,FormA)
        endif
        write(lno,102)
        t80=' '
        do i=13,15
          write(lno,FormCIF) ' '//CifKey(i,16)
          t80=t80(:idel(t80))//' ?'
        enddo
        do i=32,31+NDimI(KPhase)
          write(lno,FormCIF) ' '//CifKey(i,16)
          t80=t80(:idel(t80))//' ?'
        enddo
        if(Radiation(KDatBlock).eq.ElectronRadiation.and.CalcDyn) then
          do i=1,nCIFElectronRfl
            m=CIFElectronRfl(i)
            write(lno,FormCIF) ' '//CifKey(m,16)
            t80=t80(:idel(t80))//' ?'
          enddo
        else
          do i=1,nCIFSingleRfl
            m=CIFSingleRfl(i)
            write(lno,FormCIF) ' '//CifKey(m,16)
            t80=t80(:idel(t80))//' ?'
          enddo
        endif
        write(lno,FormA) t80(:idel(t80))
        write(lno,FormA)
        write(lno,102)
        t80=' '
        write(lno,FormCIF) ' '//CifKey(17,23)
        t80=t80(:idel(t80))//' ?'
        write(lno,FormCIF) ' '//CifKey(34,23)
        t80=t80(:idel(t80))//' ?'
        do i=23,22+NDim(KPhase)
          write(lno,FormCIF) ' '//CifKey(i,23)
          t80=t80(:idel(t80))//' ?'
        enddo
        write(lno,FormCIF) ' '//CifKey(18,23)
        t80=t80(:idel(t80))//' ?'
        do i=20,22
          write(lno,FormCIF) ' '//CifKey(i,23)
          t80=t80(:idel(t80))//' ?'
        enddo
      endif
      write(lno,FormA) t80(:idel(t80))
      write(lno,FormA)
9100  call CloseIfOpened(lno)
      call CloseIfOpened(ln)
9990  if(.not.CIFAllocated) deallocate(CifKey,CifKeyFlag)
9999  KRefBlock=KRefBlockIn
      return
102   format('loop_')
111   format(a40/'; ?'/';')
      end
