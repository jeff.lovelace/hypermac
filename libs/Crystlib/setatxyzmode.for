      subroutine SetAtXYZMode
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(3)
      call SetRealArrayTo(xp,3,0.)
      do i=1,NAtXYZMode
        do j=1,NMAtXYZMode(i)
          kt=KtAt(LXYZAMode,NXYZAMode,LAtXYZMode(j,i))
          KAtXYZMode(j,i)=kt
          pom=0.
          ii=0
          do k=1,MXYZAMode(kt),3
            ii=ii+1
            ia=IAtXYZMode(ii,i)
            call SpecPos(x(1,ia),xp,1,1,.01,nocc)
            nocc=NSymm(KPhase)/nocc
            call MultM(MetTens(1,1,KPhase),XYZAMode(k,kt),xp,3,3,1)
            pom=pom+scalmul(XYZAMode(k,kt),xp)*float(nocc)
          enddo
          FAtXYZMode(j,i)=1./sqrt(pom)
        enddo
      enddo
      return
      end
