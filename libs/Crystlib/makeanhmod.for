      subroutine MakeAnhMod(bd,n,x40,tzero,nt,dt,bx,by,kmod,kf,Gamma)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension bd(n,*),bx(n,*),by(n,*),x40(3),tzero(3),nt(3),dt(3),
     1          x4i(3),x4(3),nd(3),Gamma(9),d4(3)
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      do i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        call SetRealArrayTo(bd(1,i),6,0.)
        do k=1,kmod
          arg=0.
          do j=1,NDimI(KPhase)
            arg=arg+x4(j)*float(kw(j,k,KPhase))
          enddo
          arg=pi2*arg
          do l=1,n
            bd(l,i)=bd(l,i)+bx(l,k)*sin(arg)+by(l,k)*cos(arg)
          enddo
        enddo
      enddo
      return
      end
