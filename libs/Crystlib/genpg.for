      subroutine GenPg(PgNameIn,Rpg,npg,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Rpg(9,*),Rpp(9),ua(3,6)
      character*(*) PgNameIn
      character*8  PgName
      integer PgSym(4),PgOr(4)
      logical Eqrv
      data ua/ 1., 0., 0., 0., 1., 0., 0., 0., 1.,
     1         0.000000, 0.707107, 0.707107,
     2         0.577350, 0.577350, 0.577350,
     3         0.525731, 0.850651, 0.000000/
      ich=0
      PgName=PgNameIn
      if(PgName.eq.' ') then
        kpg=1
        go to 450
      endif
      call mala(PgName)
      do kpg=1,nSmbPg
        if(PgName.eq.SmbPgi(kpg)) go to 450
      enddo
      call Uprat(PgName)
      do kpg=1,nSmbPg
        if(PgName.eq.SmbPgo(kpg)) go to 450
      enddo
      go to 9000
450   PgName=SmbPgi(kpg)
      ipg=SmbPGN(kpg)
      call UnitMat(Rpg,3)
      do i=1,4
        PgSym(i)=1
        PgOr(i)=0
      enddo
      id=idel(PgName)
      i=0
      n=1
      npg=1
1000  i=i+1
      ic=ichar(PgName(i:i))-48
      if(PgName(i:i).eq.'-') then
        PgSym(n)=-1
        go to 1000
      else if(PgName(i:i).eq.'m') then
        PgSym(n)=-2
        go to 1100
      else if(PgName(i:i).eq.'/') then
        i=i+1
        PgSym(n)=-2
        PgOr(1)=3
        PgOr(2)=3
      else if(ic.ge.1.and.ic.le.9) then
        PgSym(n)=PgSym(n)*ic
        go to 1100
      else if(ic.eq.0) then
        n=n-1
        PgSym(n)=PgSym(n)*10
      endif
1100  n=n+1
      if(i.lt.id) then
        if(n.le.4) then
          PgSym(n)=1
          go to 1000
        endif
      endif
1200  n=n-1
      if(PgSym(n).eq.1) n=n-1
      if(ipg.ge.28.and.ipg.le.32) then
        PgOr(1)=3
        PgOr(2)=5
        if(n.eq.3) PgOr(3)=4
        go to 1900
      else if(ipg.gt.43) then
        PgOr(1)=3
        PgOr(2)=5
        PgOr(3)=6
        n=3
        go to 1900
      endif
      if(n.eq.3) then
        do i=1,n
          if(iabs(PgSym(i)).ne.2) go to 1320
        enddo
        do i=1,n
          PgOr(i)=i
        enddo
        go to 1900
      else if(n.eq.1) then
        PgOr(1)=3
        go to 1900
      endif
1320  if(iabs(PgSym(1)).gt.2) then
        PgOr(1)=3
        if(n.ge.2) then
          if(n.gt.2) n=n-1
          if(iabs(PgSym(1)).eq.3) then
            if(abs(PgSym(2)).gt.1) then
              if(PgOr(n).eq.0) PgOr(n)=1
            else
              PgSym(2)=PgSym(3)
              PgOr(2)=2
            endif
          else
            if(PgOr(n).eq.0) PgOr(n)=1
          endif
        endif
      endif
1900  do i=1,n
        npg=npg+1
        call SetGenRot(pi2/float(iabs(PgSym(i))),Ua(1,PgOr(i)),
     1                 isign(1,PgSym(i)),Rpg(1,npg))
      enddo
      i=1
2000  if(i.ge.npg) go to 9999
      i=i+1
      j=1
2200  if(j.ge.npg) go to 2000
      j=j+1
      call multm(Rpg(1,i),Rpg(1,j),Rpp,3,3,3)
      do k=1,npg
        if(Eqrv(Rpp,Rpg(1,k),9,.0001)) go to 2200
      enddo
      npg=npg+1
      call CopyMat(Rpp,Rpg(1,npg),3)
      go to 2200
9000  ich=1
9999  return
      end
