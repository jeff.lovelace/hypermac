      subroutine PitFor(Klic,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension pa(1)
      character*256 Veta,KusVety
      if(index(Formula(KPhase),'(').le.0) then
        call PitForSimple(Klic,ich)
        go to 9999
      endif
      NAtFormula(KPhase)=0
      isfh(KPhase)=0
      Veta=Formula(KPhase)
1100  i1=0
      i2=0
      do i=1,idel(Veta)
        if(Veta(i:i).eq.'(') then
          i1=i
        else if(Veta(i:i).eq.')') then
          if(i1.gt.0) then
            i2=i
            exit
          else
            go to 9000
          endif
        endif
      enddo
      if(i1.gt.0) then
        Cislo=' '
        do i=i2+1,idel(Veta)
          if(index(Cifry(:11),Veta(i:i)).gt.0) then
            Cislo=Cislo(:idel(Cislo))//Veta(i:i)
          else
            i3=i
            go to 1200
          endif
        enddo
        i3=idel(Veta)
1200    if(Cislo.eq.' ') then
          pa(1)=1.
        else
          k=0
          call StToReal(Cislo,k,pa,1,.false.,ich)
          if(ich.ne.0) go to 9100
        endif
        ich=0
        NAtFormula(KPhase)=0
        isfh(KPhase)=0
        KusVety=Veta(i1+1:i2-1)
        call PitForKus(Klic,KusVety,ich)
        if(ich.ne.0) go to 9999
        if(Klic.eq.0) then
          do i=1,NAtFormula(KPhase)
            AtMult(i,KPhase)=pa(1)*AtMult(i,KPhase)
          enddo
          call SetFormula(KusVety)
        endif
        Veta=Veta(:i1-1)//' '//KusVety(:idel(KusVety))//' '//
     1       Veta(i3:)
        go to 1100
      endif
      NAtFormula(KPhase)=0
      isfh(KPhase)=0
      call PitForKus(Klic,Veta,ich)
      go to 9999
9000  call FeChybne(-1.,-1.,'synax error in the formula "'//
     1              Formula(KPhase)(:idel(Formula(KPhase)))//'.',
     2              ' ',SeriousError)
      go to 9900
9100  call FeChybne(-1.,-1.,'incorrect number in chemical formula '
     1            //Formula(KPhase)(:idel(Formula(KPhase)))//'"',
     2              Cislo,SeriousError)
9900  ich=1
9999  return
      end
