      subroutine GetQiQr(q,qi,qr,ndi,isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension q(3,ndi),qi(3,ndi),qr(3,ndi),qp(9),qpp(9),GammaRI(9),
     1          GammaI(9),QMat(9),QMatS(9)
      k=0
      do i=1,3
        do j=1,NDimI(KPhase)
          k=k+1
          QMat(k)=q(i,j)
        enddo
      enddo
      QMatS=0.
      qp=0.
      nd=ndi+3
      do is=1,NSymm(KPhase)
        call MatInv(rm(1,is,isw,KPhase),GammaRI,det,3)
        k=0
        do i=1,ndi
          do j=1,ndi
            k=k+1
            m=j+3+(i+2)*nd
            GammaI(k)=rm6(m,is,isw,KPhase)
          enddo
        enddo
        call Multm(GammaI,QMat,qpp,ndi,ndi,3)
        call Multm(qpp,GammaRI,qp,ndi,3,3)
        QMatS=QMatS+qp
      enddo
      k=0
      do i=1,3
        do j=1,NDimI(KPhase)
          k=k+1
          qi(i,j)=QMatS(k)/float(NSymm(KPhase))
          qr(i,j)=q(i,j)-qi(i,j)
        enddo
      enddo
      return
      end
