      subroutine CrlGenerMagSymm(n,m,rmp,sp,zmg,NGen,IGen,KGen,ich)
      dimension rmp(n**2,m),sp(n,m),zmg(m),IGen(m),KGen(m),rp(36),
     1          sp1(6),sp2(6)
      logical Eqrv
      ich=0
      call SetIntArrayTo(KGen,m,0)
      do i=1,NGen
        KGen(IGen(i))=1
      enddo
1500  i=0
2000  if(i.ge.m) go to 9999
      i=i+1
      if(KGen(i).le.0) go to 2000
      j=0
2100  if(j.ge.m) go to 2000
      j=j+1
      if(KGen(j).le.0) go to 2100
      call multm(rmp(1,i),rmp(1,j),rp,n,n,n)
      call CopyVek(sp(1,i),sp1,n)
      call cultm(rmp(1,i),sp(1,j),sp1,n,n,1)
      call od0do1(sp1,sp1,n)
      do k=1,m
        if(Eqrv(rp,rmp(1,k),n**2,.001)) then
          if(KGen(k).le.0) then
            KGen(k)=1
            zmg(k)=zmg(i)*zmg(j)
            call CopyVek(sp1,sp(1,k),n)
            go to 1500
          else
            if(zmg(k).ne.zmg(i)*zmg(j)) then
              ich=1
              go to 9000
            endif
            call NormCentr(sp1)
            call CopyVek(sp(1,k),sp2,n)
            call NormCentr(sp2)
            if(.not.Eqrv(sp1,sp2,n,.0001)) then
              ich=1
              go to 9000
            endif
          endif
        endif
      enddo
      go to 2100
9000  ich=1
9999  return
      end
