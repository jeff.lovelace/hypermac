      subroutine CodeSymm(rmm,s,is,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) is(*)
      dimension rmm(*),s(*)
      do i=1,NDim(KPhase)
        ij=i
        is(i)=' '
        kk=0
        do j=1,NDim(KPhase)
          l=nint(rmm(ij))
          if(abs(rmm(ij)).gt..0001) then
            if(rmm(ij).gt.0..and.kk.gt.0) then
              kk=kk+1
              is(i)(kk:kk)='+'
            else if(rmm(ij).lt.0.) then
              kk=kk+1
              is(i)(kk:kk)='-'
            endif
            if(abs(float(l)-rmm(ij)).gt..0001) then
              write(Cislo,'(f8.3)') abs(rmm(ij))
              call ZdrcniCisla(Cislo,1)
              is(i)(kk+1:)=Cislo
            else
              if(iabs(l).gt.1) then
                write(is(i)(kk+1:),'(i2)') iabs(l)
              endif
            endif
            call zhusti(is(i))
            kk=max(idel(is(i)),1)
            if(klic.eq.1.or.NDim(KPhase).gt.3) then
              is(i)=is(i)(1:kk)//smbx6(j)
              kk=kk+2
            else
              is(i)=is(i)(1:kk)//smbx(j)
              kk=kk+1
            endif
            call zhusti(is(i))
            kk=idel(is(i))
          endif
          ij=ij+NDim(KPhase)
        enddo
        kk=idel(is(i))+1
        do k=1,100
          p=s(i)*float(k)
          l=nint(p)
          if(abs(p-float(l)).lt..0001) go to 1200
        enddo
        is(i)='?/?'
        go to 1200
1200    if(l.ne.0) then
          if(k.eq.1) then
            is(i)(kk:)=' '
          else
            write(Cislo,'(''+'',i4,''/'',i3)') l,k
            if(l.lt.0) Cislo(1:1)=' '
            call Zhusti(Cislo)
            is(i)(kk:)=Cislo(:idel(Cislo))
          endif
        endif
1300    call zhusti(is(i))
      enddo
      return
      end
