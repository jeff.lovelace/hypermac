      subroutine indext(i,j,k)
      include 'fepc.cmn'
      if(i.gt.3) then
        j=max(i-4,1)
        k=min(i-2,3)
      else
        j=i
        k=i
      endif
      return
      end
