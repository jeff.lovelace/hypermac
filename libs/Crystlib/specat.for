      subroutine SpecAt
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension px(36),xp(6),x0(6),x1(6),x2(6),x3(6),rm6p(36),s6pi(6),
     1          s6pj(6),vt6p(:,:)
      allocatable vt6p
      KPhaseIn=KPhase
      allocate(vt6p(MaxNDim,MaxNLattVec))
      do i=1,NAtCalc
        isw=iswa(i)
        KPhase=kswa(i)
        AtSiteMult(i)=1
        do j=1,NSymm(KPhase)
          isa(j,i)=j
        enddo
        call CopyVek(x(1,i),xp,3)
        NDimp=NDim(KPhase)
        if(NDimP.gt.3) then
          if(KFA(1,i).gt.0.and.KModA(1,i)-NDimP+4.gt.0) then
            call CopyVek(ax(KModA(1,i)-NDimP+4,i),xp(4),
     1                   NDimI(KPhase))
          else if(KFA(2,i).eq.1.and.KModA(2,i).gt.0) then
            xp(4)=uy(1,KModA(2,i),i)
          else
            call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
            NDimp=3
          endif
        endif
        do isym=1,NSymm(KPhase)
          if(isa(isym,i).lt.0) cycle
          iswi=IswSymm(isym,isw,KPhase)
          if(iswi.eq.isw) then
            call CopyMat(rm6(1,isym,isw,KPhase),rm6p,NDim(KPhase))
            call CopyVek( s6(1,isym,isw,KPhase), s6pi,NDim(KPhase))
          else
            call Multm(ZVi(1,isw,KPhase),rm6(1,isym,isw,KPhase),px,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call Multm(ZV(1,iswi,KPhase),px,rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MultM(ZVi(1,isw,KPhase),s6(1,isym,isw,KPhase),px,
     1                 NDim(KPhase),NDim(KPhase),1)
            call MultM(ZV(1,iswi,KPhase),px,s6pi,NDim(KPhase),
     1                 NDim(KPhase),1)
          endif
          call multm(rm6p,xp,x0,NDim(KPhase),NDim(KPhase),1)
          call AddVek(x0,s6pi,x1,NDim(KPhase))
          if(InvMag(KPhase).gt.0.and.NDim(KPhase).gt.3) then
            x1(4)=x1(4)-s6pi(4)
          endif
          do jsym=isym,NSymm(KPhase)
            if(jsym.le.isym.or.isa(jsym,i).lt.0) cycle
            iswj=IswSymm(jsym,isw,KPhase)
            if(iswi.ne.iswj) cycle
            if(iswj.eq.isw) then
              call CopyMat(rm6(1,jsym,isw,KPhase),rm6p,NDim(KPhase))
              call CopyVek( s6(1,jsym,isw,KPhase),s6pj,NDim(KPhase))
              do ivt=1,NLattVec(KPhase)
                call CopyVek(vt6(1,ivt,isw,KPhase),vt6p(1,ivt),
     1                       NDim(KPhase))
              enddo
            else
              call Multm(ZVi(1,isw,KPhase),rm6(1,jsym,isw,KPhase),px,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call Multm(ZV(1,iswj,KPhase),px,rm6p,
     1                   NDim(KPhase),NDim(KPhase),NDim(KPhase))
              call MultM(ZVi(1,isw,KPhase),s6(1,jsym,isw,KPhase),px,
     1                   NDim(KPhase),NDim(KPhase),1)
              call MultM(ZV(1,iswj,KPhase),px,s6pj,NDim(KPhase),
     1                   NDim(KPhase),1)
              do ivt=1,NLattVec(KPhase)
                call MultM(ZVi(1,isw,KPhase),vt6(1,ivt,isw,KPhase),px,
     1                     NDim(KPhase),NDim(KPhase),1)
                call MultM(ZV(1,iswj,KPhase),px,vt6p(1,ivt),
     1                     NDim(KPhase),NDim(KPhase),1)
              enddo
            endif
            call multm(rm6p,xp,x2,NDim(KPhase),NDim(KPhase),1)
            call AddVek(x2,s6pj,x3,NDim(KPhase))
            do 1500jvt=1,NLattVec(KPhase)
              do m=1,NDimp
                x4m=x3(m)+vt6p(m,jvt)
                pom=x1(m)-x4m
                if(InvMag(KPhase).gt.0.and.m.gt.3) pom=pom-s6pj(m)
                apom=anint(pom)
                if(abs(pom-apom).gt..00001) go to 1500
              enddo
              isa(jsym,i)=-isym
              if(isym.eq.1) AtSiteMult(i)=AtSiteMult(i)+1
              exit
1500        continue
          enddo
        enddo
      enddo
      KPhase=KPhaseIn
      deallocate(vt6p)
      if(NMolec.gt.0) call SpecAtMol
      return
      end
