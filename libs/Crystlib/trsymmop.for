      subroutine TrSymmOp(trm,rm,trmi,s,n)
      include 'fepc.cmn'
      dimension trm(n**2),rm(n**2),trmi(n**2),s(n),xp(36)
      call MultM(trm,rm,xp,n,n,n)
      call MultM(xp,trmi,rm,n,n,n)
      call MultM(trm,s,xp,n,n,1)
      call CopyVek(xp,s,n)
      call od0do1(s,s,n)
      return
      end
