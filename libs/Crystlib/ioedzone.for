      subroutine IOEDZone(ln,a,n,RefFlag,NThick,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension a(*)
      character*80 FormatOut,Veta
      integer Exponent10
      logical RefFlag
      if(klic.eq.0) then
        read(ln,FormA,end=9000) Veta
        if(idel(Veta).le.54) then
          read(Veta,102,err=9000)(a(i),i=1,n)
          RefFlag=.true.
          NThink=1
        else
          read(Veta,102,err=9000)(a(i),i=1,6),RefFlag,NThick
          if(NThick.lt.0) NThick=1
        endif
      else
        FormatOut='('
        k=2
        do i=1,n
          if(abs(a(i)).gt.0.) then
            j=max(6-Exponent10(a(i)),0)
            j=min(j,6)
          else
            j=6
          endif
          write(FormatOut(k:),'(''f9.'',i1,'','')') j
          k=k+5
        enddo
        write(FormatOut(k+1:),'(i2,''x'','',)'')') 54-n*9+6
        FormatOut(k+1:)='6x,l1,i4)'
        write(ln,FormatOut)(a(i),i=1,n),RefFlag,NThick
      endif
      go to 9999
9000  call SetRealArrayTo(a,n,0.)
      ErrFlag=1
9999  return
102   format(6f9.6,6x,l1,i4)
      end
