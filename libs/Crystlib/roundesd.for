      subroutine RoundESD(StResult,r,sr,irmax,tab,zdrc)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StResult
      integer tab,zdrc,Exponent10
      if(RoundMethod.eq.Round_2_19) then
        sul=1.95
      else if(RoundMethod.eq.Round_2_15) then
        sul=1.55
      else
        sul=0.95
      endif
      if(sr.le..000001) then
        j=max(min(6-Exponent10(r),irmax),0)
      else
        f=1.
        j=0
1040    pom=sr*f
        if(pom.lt.sul) then
          f=f*10.
          j=j+1
          go to 1040
        endif
        if(j.eq.0) then
1050      pom=sr*f
          if(pom.ge.10.*sul) then
            f=f*.1
            j=j-1
            go to 1050
          endif
        endif
        is=nint(pom)
      endif
      write(RealFormat(6:7),'(i2)') max(j,0)
      if(j.ge.0) then
        pom=r
      else
        pom=10.**j
        pom=anint(r*pom)/pom
      endif
      write(StResult,RealFormat) pom
      if(zdrc.eq.1.or.sr.le.0.) then
        call ZdrcniCisla(StResult,1)
      else
        call Zhusti(StResult)
      endif
      id=idel(StResult)
      if(j.ge.0) then
        if(StResult(id:id).eq.'.') then
          StResult(id:id)=' '
          id=id-1
        endif
        if(sr.gt.0..and.id.lt.13) then
          write(StResult(id+1:id+4),'(''('',i2,'')'')') is
          call Zhusti(StResult(id+1:id+4))
        endif
      else
        id=id-1
        write(Cislo,FormI15) is*10**(-j)
        call Zhusti(Cislo)
        StResult(id+1:)='('//Cislo(:idel(Cislo))//')'
      endif
      if(tab.eq.1.and.StResult(1:1).ne.'-')
     1  StResult=' '//StResult(:idel(StResult))
      return
      end
