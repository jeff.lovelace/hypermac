      subroutine TrMagMode(Klic)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      real MagAtom(:,:)
      integer MagAtomFlag(:)
      allocatable MagAtom,MagAtomFlag
      save MagAtom,MagAtomFlag
      if(Klic.lt.0) then
        MagModeInAtoms=.false.
        go to 9999
      endif
      if(MagModeInAtoms.eqv.Klic.eq.0) then
        if(VasekTest.ne.0) then
          if(Klic.eq.0) then
            Veta='chce rozpocitavat magneticke mody, ale ono je jiz '//
     1           'rozpocitano'
          else
            Veta='chce obnovovat magneticke momenty  atomu, ale ono '//
     1           'je to jiz obnoveno'
          endif
          call FeWinMessage(Veta,' ')
        endif
        go to 9999
      endif
      if(Klic.eq.0) then
        if(allocated(MagAtom)) deallocate(MagAtom,MagAtomFlag)
        allocate(MagAtom(3,NAtCalc),MagAtomFlag(NAtCalc))
        call SetIntArrayTo(MagAtomFlag,NAtCalc,0)
        do i=1,NAtMagMode
          l=0
          do j=1,MAtMagMode(i)
            ia=IAtMagMode(j,i)
            if(MagAtomFlag(ia).le.0) then
              MagAtomFlag(ia)=1
              call CopyVek(sm0(1,ia),MagAtom(1,ia),3)
            endif
            do k=1,NMAtMagMode(i)
              kt=KAtMagMode(k,i)
              do m=1,3
                sm0(m,ia)=sm0(m,ia)+
     1                  FAtMagMode(k,i)*AtMagMode(k,i)*MagAMode(l+m,kt)
              enddo
            enddo
            l=l+3
          enddo
        enddo
        MagModeInAtoms=.true.
        NAtCalcUsed=NAtCalc
      else if(Klic.eq.1) then
        do i=1,NAtCalcUsed
          if(MagAtomFlag(i).gt.0)
     1      call CopyVek(MagAtom(1,i),sm0(1,i),3)
        enddo
        MagModeInAtoms=.false.
      endif
9999  return
      end
