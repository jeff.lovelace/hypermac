      subroutine CrlSetTraceS(S,isw)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension S(9),rmp(9),TrP(9)
      call multm(TrToOrtho(1,isw,KPhase),S,rmp,3,3,3)
      call TrMat(TrToOrtho(1,isw,KPhase),TrP,3,3)
      call multm(rmp,TrP,S,3,3,3)
      TrS=(S(1)+S(5)+S(9))/3.
      S(1)=S(1)-TrS
      S(5)=S(5)-TrS
      S(9)=S(9)-TrS
      call multm(TrToOrthoI(1,isw,KPhase),S,rmp,3,3,3)
      call TrMat(TrToOrthoI(1,isw,KPhase),TrP,3,3)
      call multm(rmp,TrP,S,3,3,3)
      return
      end
