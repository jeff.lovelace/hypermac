      subroutine CrlStoreSymmetry(IStore)
      use Basic_mod
      use MatStore_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension TrMat(*),TwMat(9,*),RMExpl(NDimExpl**2,NSymmExpl),
     1          SExpl(NDimExpl,NSymmExpl),VTExpl(NDimExpl,NLattVecExpl)
      integer, allocatable :: FlagSymm(:)
      logical SymmEqual,EqRV,MatRealEqUnitMat,EqualNDim
      if(IStore.gt.NStore.or.IStore.le.0) IStore=NStore+1
      call CrlGetReflectionConditions
      call ReallocateStoreSymmetry(IStore,NSymm(KPhase),
     1  NLattVec(KPhase),NDim(KPhase),NExtRefCond)
      do is=1,NSymm(KPhase)
        call CopyMat(rm6(1,is,1,KPhase),RM6Store(1,is,IStore),
     1               NDim(KPhase))
        call MatBlock3(rm6(1,is,1,KPhase),RMStore(1,is,IStore),
     1                 NDim(KPhase))
        call CopyVek( s6(1,is,1,KPhase), S6Store(1,is,IStore),
     1               NDim(KPhase))
        call CopyMat(rm6(1,is,1,KPhase),rms,NDim(KPhase))
        call CopyVek(s6(1,is,1,KPhase),xp,NDim(KPhase))
        ProjStore(1:NDim(KPhase),is,IStore)=0
        n=1
        do while(.not.MatRealEqUnitMat(rms,NDim(KPhase),.001))
          n=n+1
          if(n.gt.20) then
            xp=0.
            exit
          endif
          call MultM(rm6(1,is,1,KPhase),rms,rmp,
     1               NDim(KPhase),NDim(KPhase),NDim(KPhase))
          call CopyMat(rmp,rms,NDim(KPhase))
          call MultM(rm6(1,is,1,KPhase),xp,xq,NDim(KPhase),NDim(KPhase),
     1               1)
          call AddVek(xq,s6(1,is,1,KPhase),xp,NDim(KPhase))
        enddo
        fn=1./float(n)
        do i=1,NDim(KPhase)
          ProjStore(i,is,IStore)=xp(i)*fn
        enddo
      enddo
      do ivt=1,NLattVec(KPhase)
        call CopyVek(vt6(1,ivt,1,KPhase),VT6Store(1,ivt,IStore),
     1               NDim(KPhase))
      enddo
      do iext=1,NExtRefCond
        call CopyMat(ExtRefGroup(1,iext),
     1               ExtRefGroupStore(1,iext,IStore),NDim(KPhase))
        call CopyVek(ExtRefCond(1,iext),
     1               ExtRefCondStore(1,iext,IStore),NDim(KPhase)+1)
      enddo
      NExtRefCondStore(IStore)=NExtRefCond
      go to 9999
      entry CrlStoreSymmetryExplicite(IStore,RMExpl,SExpl,VTExpl,
     1                                NSymmExpl,NLattVecExpl,NDimExpl)
      if(IStore.gt.NStore.or.IStore.le.0) IStore=NStore+1
      call ReallocateStoreSymmetry(IStore,NSymmExpl,NLattVecExpl,
     1                             NDimExpl,0)
      do is=1,NSymmExpl
        call CopyMat(RMExpl(1,is),RM6Store(1,is,IStore),NDimExpl)
        call MatBlock3(RMExpl(1,is),RMStore(1,is,IStore),NDimExpl)
        call CopyVek(SExpl(1,is), S6Store(1,is,IStore),NDimExpl)
        call CopyMat(RMExpl(1,is),rms,NDimExpl)
        call CopyVek(RMExpl(1,is),xp,NDimExpl)
        n=1
        do while(.not.MatRealEqUnitMat(rms,NDimExpl,.001))
          n=n+1
          call MultM(RMExpl(1,is),rms,rmp,NDimExpl,NDimExpl,NDimExpl)
          call CopyMat(rmp,rms,NDimExpl)
          call MultM(RMExpl(1,is),xp,xq,NDimExpl,NDimExpl,1)
          call AddVek(xq,SExpl(1,is),xp,NDimExpl)
        enddo
        fn=1./float(n)
        do i=1,NDimExpl
          ProjStore(i,is,IStore)=xp(i)*fn
        enddo
      enddo
      do ivt=1,NLattVecExpl
        call CopyVek(VTExpl(1,ivt),VT6Store(1,ivt,IStore),NDimExpl)
      enddo
      NExtRefCondStore(IStore)=0
      go to 9999
      entry CrlCompRefExtSymmetry(IStore,SymmEqual)
      SymmEqual=.true.
      if(IStore.gt.NStore.or.NStore.eq.0) go to 9999
      SymmEqual=NLattVecStore(IStore).eq.NLattVec(KPhase).and.
     1          NSymmStore(IStore).eq.NSymm(KPhase).and.
     2          NDimStore(IStore).eq.NDim(KPhase)
      if(.not.SymmEqual) go to 9999
      do 3100i=1,NSymm(KPhase)
        do j=1,NSymm(KPhase)
          if(EqRV(RM6(1,i,1,KPhase),RM6Store(1,j,IStore),NDimQ(KPhase),
     1            .0001)) go to 3100
        enddo
        go to 3300
3100  continue
      if(NExtRefCondStore(IStore).ne.NExtRefCond) go to 3300
      do 3200i=1,NExtRefCond
        do j=1,NExtRefCond
          if(EqRV(ExtRefGroup(1,i),ExtRefGroupStore(1,j,IStore),
     1            NDimQ(KPhase),.0001).and.
     2       EqRV(ExtRefCond(1,i),ExtRefCondStore(1,j,IStore),
     3            NDim(KPhase)+1,.0001)) go to 3200
        enddo
        go to 3300
3200  continue
      go to 9999
3300  SymmEqual=.false.
      go to 9999
      entry CrlCompareSymmetry(IStore,SymmEqual)
      SymmEqual=.true.
      if(IStore.gt.NStore.or.NStore.eq.0) go to 9999
      SymmEqual=NLattVecStore(IStore).eq.NLattVec(KPhase).and.
     1          NSymmStore(IStore).eq.NSymm(KPhase)
      if(.not.SymmEqual) go to 9999
      EqualNDim=NDimStore(IStore).eq.NDim(KPhase)
      if(.not.EqualNDim.and.NDimStore(IStore).ne.3.and.
     1   NDim(KPhase).ne.3) go to 4500
      NDimP=min(NDim(KPhase),NDimStore(IStore))
      do 4100i=1,NSymm(KPhase)
        do j=1,NSymm(KPhase)
          if(EqualNDim) then
            if(EqRV(RM6(1,i,1,KPhase),RM6Store(1,j,IStore),
     1              NDimQ(KPhase),.0001)) go to 4100
          else
            if(EqRV(RM(1,i,1,KPhase),RMStore(1,j,IStore),9,.0001))
     1        go to 4100
          endif
        enddo
        go to 4500
4100  continue
      do 4200i=1,NLattVec(KPhase)
        do j=1,NLattVec(KPhase)
          if(EqRV(VT6(1,i,1,KPhase),VT6Store(1,j,IStore),NDimP,
     1            .0001)) go to 4200
        enddo
        go to 4500
4200  continue
      do 4300i=1,NSymm(KPhase)
        do j=1,NSymm(KPhase)
          if(EqualNDim) then
            if(EqRV(RM6(1,i,1,KPhase),RM6Store(1,j,IStore),
     1              NDimQ(KPhase),.0001)) go to 4250
          else
            if(EqRV(RM(1,i,1,KPhase),RMStore(1,j,IStore),9,.0001))
     1        go to 4250
          endif
        enddo
        go to 4500
4250    if(EqualNDim) then
          call CopyMat(rm6(1,i,1,KPhase),rms,NDim(KPhase))
        else
          call CopyMat(rm(1,i,1,KPhase),rms,3)
        endif
        call CopyVek(s6(1,i,1,KPhase),xp,NDimP)
        n=1
        do while(.not.MatRealEqUnitMat(rms,NDimP,.001))
          n=n+1
          if(EqualNDim) then
            call MultM(rm6(1,i,1,KPhase),rms,rmp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
          else
            call MultM(rm(1,i,1,KPhase),rms,rmp,3,3,3)
          endif
          call CopyMat(rmp,rms,NDimP)
          if(EqualNDim) then
            call MultM(rm6(1,i,1,KPhase),xp,xq,NDim(KPhase),
     1                 NDim(KPhase),1)
          else
            call MultM(rm(1,i,1,KPhase),xp,xq,3,3,1)
          endif
          call AddVek(xq,s6(1,i,1,KPhase),xp,NDimP)
        enddo
        fn=1./float(n)
        do k=1,NDimP
          xp(k)=xp(k)*fn-ProjStore(k,j,IStore)
        enddo
        call od0do1(xp,xp,NDimP)
        do k=1,NLattVec(KPhase)
          if(EqRV(xp,VT6(1,k,1,KPhase),NDimP,.0001)) go to 4300
        enddo
        go to 4500
4300  continue
      go to 9999
4500  SymmEqual=.false.
      go to 9999
      entry CrlCompleteSymmetry(IStore)
      if(IStore.gt.NStore.or.NStore.eq.0) go to 9999
      EqualNDim=NDimStore(IStore).eq.NDim(KPhase)
      if(.not.EqualNDim.and.NDimStore(IStore).ne.3.and.
     1   NDim(KPhase).ne.3) go to 4500
      NDimP=min(NDim(KPhase),NDimStore(IStore))
      do 5100i=1,NSymm(KPhase)
        do j=1,NSymm(KPhase)
          if(EqRV(RM(1,i,1,KPhase),RMStore(1,j,IStore),9,.0001)) then
            call CopyMat(RM6Store(1,j,IStore),RM6(1,i,1,KPhase),
     1                   NDimStore(IStore))
            call CopyVek(S6Store(4,j,IStore),S6(4,i,1,KPhase),
     1                   NDimStore(IStore)-3)
            go to 5100
          endif
        enddo
5100  continue
      do 5200i=1,NLattVec(KPhase)
        do j=1,NLattVec(KPhase)
          if(EqRV(VT6(1,i,1,KPhase),VT6Store(1,j,IStore),3,.0001)) then
            call CopyVek(VT6Store(4,j,IStore),Vt6(4,i,1,KPhase),
     1                   NDimStore(IStore)-3)
             go to 5200
          endif
        enddo
5200  continue
      go to 9999
      entry CrlTransSymmetry(IStoreIn,TrMat,IStoreOut)
      if(IStoreIn .le.0.or.IStoreIn .gt.NStore.or.
     1   IStoreOut.le.0.or.IStoreOut.gt.NStore) go to 9999
      NDimP=NDimStore(IStoreIn)
      call MatInv(TrMat,rmp,det,NDimP)
      do is=1,NSymmStore(IStoreIn)
        call MultM(RM6Store(1,is,IStoreIn),TrMat,rms,NDimP,NDimP,NDimP)
        call MultM(rmp,rms,RM6Store(1,is,IStoreOut),NDimP,NDimP,NDimP)
        call MatBlock3(RM6Store(1,is,IStoreOut),RMStore(1,is,IStoreOut),
     1                 NDimP)
        call MultM(TrMat,S6Store(1,is,IStoreIn),
     1                   S6Store(1,is,IStoreOut),NDimP,NDimP,1)
        call od0do1(S6Store(1,is,IStoreOut),
     1              S6Store(1,is,IStoreOut),NDimP)
      enddo
      do ivt=1,NLattVecStore(IStoreIn)
        call MultM(TrMat,VT6Store(1,ivt,IStoreIn),
     1                   VT6Store(1,ivt,IStoreOut),NDimP,NDimP,1)
        call od0do1(VT6Store(1,ivt,IStoreOut),
     1              VT6Store(1,ivt,IStoreOut),NDimP)
      enddo
      go to 9999
      entry CrlRestoreSymmetry(IRestore)
      if(IRestore.le.0.or.IRestore.gt.NStore) go to 9999
      call ReallocSymm(NDimStore(KPhase),NSymmStore(IRestore),
     1  NLattVecStore(IRestore),NComp(KPhase),NPhase,0)
      do is=1,NSymmStore(IRestore)
        call CopyMat(RM6Store(1,is,IRestore),rm6(1,is,1,KPhase),
     1               NDimStore(IRestore))
        call MatBlock3(rm6(1,is,1,KPhase),rm(1,is,1,KPhase),
     1                 NDimStore(IRestore))
        call CopyVek( S6Store(1,is,IRestore), s6(1,is,1,KPhase),
     1               NDimStore(IRestore))
      enddo
      do ivt=1,NLattVecStore(IRestore)
        call CopyVek(VT6Store(1,ivt,IRestore),vt6(1,ivt,1,KPhase),
     1               NDimStore(IRestore))
        enddo
      NSymm(KPhase)=NSymmStore(IRestore)
      NLattVec(KPhase)=NLattVecStore(IRestore)
      NDim(KPhase)=NDimStore(IRestore)
      NExtRefCond=NExtRefCondStore(IRestore)
      do iext=1,NExtRefCond
        call CopyMat(ExtRefGroupStore(1,iext,IRestore),
     1               ExtRefGroup(1,iext),NDim(KPhase))
        call CopyVek(ExtRefCondStore(1,iext,IRestore),
     1               ExtRefCond(1,iext),NDim(KPhase)+1)
      enddo
      go to 9999
      entry CrlCompTwinSymmetry(IStoreIn,TwMat,NTwMat,ich)
      ich=0
      if(IStoreIn.gt.NStore.or.NStore.le.0) then
        ich=1
        go to 9999
      endif
      ns=NSymmStore(IStoreIn)
      if(allocated(FlagSymm)) deallocate(FlagSymm)
      allocate(FlagSymm(ns))
      FlagSymm=0
      n=0
      do i=1,ns
        do j=1,NSymm(KPhase)
          if(EqRV(RM(1,j,1,KPhase),RMStore(1,i,IStoreIn),9,.0001)) then
            n=n+1
            FlagSymm(i)=1
            exit
          endif
        enddo
      enddo
      n=1
      do i=1,ns
        if(FlagSymm(i).ne.0) cycle
        n=n+1
        do 6100j=1,ns
          if(FlagSymm(j).ne.1) go to 6100
          call MultM(RMStore(1,i,IStoreIn),RMStore(1,j,IStoreIn),rmp,
     1               3,3,3)
          do k=1,ns
            if(FlagSymm(k).eq.0.and.
     1         EqRV(RMStore(1,k,IStoreIn),rmp,9,.0001)) then
              FlagSymm(k)=n
              go to 6100
            endif
          enddo
6100    continue
      enddo
      NTwMat=n
      do i=2,n
        do j=1,ns
          if(FlagSymm(j).eq.i) then
             call CopyVek(RMStore(1,j,IStoreIn),TwMat(1,i),9)
             exit
          endif
        enddo
      enddo
      go to 9999
      entry CrlRestoreSymmetryExplicite(IRestore,RMExpl,SExpl,VTExpl,
     1                                  NSymmEx,NLattVecEx,NDimEx)
      do is=1,NSymmStore(IRestore)
        call CopyMat(RM6Store(1,is,IRestore),RMExpl(1,is),
     1               NDimStore(IRestore))
        call CopyVek( S6Store(1,is,IRestore),SExpl(1,is),
     1               NDimStore(IRestore))
      enddo
      do ivt=1,NLattVecStore(IRestore)
        call CopyVek(vt6(1,ivt,1,KPhase),VTExpl,NDimStore(IRestore))
      enddo
      NSymmEx=NSymmStore(IRestore)
      NLattVecEx=NLattVecStore(IRestore)
      NDimEx=NDimStore(IRestore)
      go to 9999
      entry CrlCleanSymmetry
      if(allocated(NSymmStore))
     1  deallocate(NSymmStore,NLattVecStore,NDimStore,NExtRefCondStore,
     2             RM6Store,RMStore,S6Store,ProjStore,VT6Store,
     3             ExtRefGroupStore,ExtRefCondStore)
      NStore=0
      NDimStoreMax=0
      NSymmStoreMax=0
      NLattVecStoreMax=0
      NExtRefCondStoreMax=0
      go to 9999
9999  if(allocated(FlagSymm)) deallocate(FlagSymm)
      return
      end
