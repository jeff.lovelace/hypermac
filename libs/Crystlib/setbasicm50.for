      subroutine SetBasicM50
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      StructureName=' '
      NPhase=1
      KPhase=1
      StatusM50=11010
      call SetStringArrayTo(PhaseName,MxPhases,' ')
      call SetIntArrayTo(NAtFormula,MxPhases,0)
      call SetIntArrayTo(KAnRef,MxDatBlock,0)
      call SetIntArrayTo(FFType,MxPhases,-62)
      NTwin=1
      call SetIntArrayTo(KPhaseTwin,mxsc,1)
      ChargeDensities=.false.
      ParentStructure=.false.
      MaxMagneticType=0
      call SetIntArrayTo(MagneticType,MxPhases,0)
      call SetIntArrayTo(NQMag,MxPhases,0)
      call SetRealArrayTo(QMag,9*MxPhases,0.)
      NameOfWaveFile='wavefj.dat'
      call SetStringArrayTo(Formula,MxPhases,' ')
      call SetIntArrayTo(NUnits,MxPhases,0)
      call SetIntArrayTo(ngc,MxPhases,0)
      MaxNDim=3
      MaxNDimI=0
      call SetIntArrayTo(NDimI,MxPhases,0)
      call SetIntArrayTo(NDimQ,MxPhases,9)
      call SetIntArrayTo(NDim, MxPhases,3)
      MaxNComp=1
      call SetIntArrayTo(NComp,MxPhases,1)
      KCommenMax=0
      call SetIntArrayTo(KCommen,MxPhases,0)
      call SetIntArrayTo(ICommen,MxPhases,0)
      call SetIntArrayTo(NCommQProduct,3*MxPhases,1)
      ngcMax=0
      call SetIntArrayTo(ngc,MxPhases,0)
      MaxNLattVec=1
      call SetIntArrayTo(NLattVec,MxPhases,1)
      call SetIntArrayTo(NCSymm,MxPhases,1)
      MaxNSymm=0
      call SetIntArrayTo(NSymm,MxPhases,0)
      MaxNSymmN=0
      call SetIntArrayTo(NSymmN,MxPhases,0)
      MaxNSymmL=0
      call SetIntArrayTo(NSymmL,MxPhases,0)
      call SetStringArrayTo(Grupa,MxPhases,'???')
      call SetIntArrayTo(CrSystem,MxPhases,0)
      call SetStringArrayTo(Lattice,MxPhases,'P')
      StdSg(KPhase)=1
      call SetIntArrayTo(NCommen,9*MxPhases,1)
      call SetRealArrayTo(trez,9*MxPhases,0.)
      do i=1,MxPhases
        call UnitMat(RCommen(1,1,i),3)
      enddo
      call SetRealArrayTo(shsg(1,KPhase),6,0.)
      do i=1,MxPhases
        call SetRealArrayTo(quir(1,1,i),9,0.)
        call SetRealArrayTo(CellPar(1,1,i),6,0.)
        call SetRealArrayTo(CellParSU(1,1,i),6,0.)
      enddo
      MaxNAtFormula=0
      MaxNComp=1
      MaxNDim=3
      MaxNCSymm=1
      if(.not.ExistM90.and..not.ExistM95) then
        NDatBlock=1
        isPowder=ExistM41
        ExistPowder=isPowder
        if(isPowder) then
          DataType(1)=2
        else
          DataType(1)=1
        endif
        Radiation(1)=XRayRadiation
        NAlfa(1)=1
        KLam(1)=6
        LamAve(1)=LamAveD(6)
        LamA1(1)=LamA1D(6)
        LamA2(1)=LamA2D(6)
        LamRat(1)=LamRatD(6)
        LPFactor(1)=PolarizedMonoPer
        AngleMon(1)=CrlMonAngle(MonCell(1,3),MonH(1,3),LamAve(1))
        call PwdSetTOF(1)
      endif
      return
      end
