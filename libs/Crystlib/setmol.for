      subroutine SetMol(klic,tisk)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*12 PosName
      integer tisk
      KPhaseIn=KPhase
      iak=NAtMolFr(1,1)-1
      KPh=0
      do i=1,NMolec
        KPhase=kswmol(i)
        iamni=iam(i)
        if(KPhase.ne.KPh) then
          KPh=KPhase
          NAtIndP=0
        endif
        NAtIndP=NAtIndP+iamni
        isw=iswmol(i)
        iap=iak+1
        iak=iak+iamni
        if(StRefPoint(i).ne.' ') then
          k=ktat(atom(iap),iam(i)/KPoint(i),StRefPoint(i))+iap-1
          call CopyVek(x(1,k),xm(1,i),3)
        endif
        do j=1,mam(i)
          ji=j+mxp*(i-1)
          write(PosName,'(''#'',i3)') j
          call Zhusti(PosName)
          PosName=MolName(i)(:idel(MolName(i)))//PosName(:idel(PosName))
          if(AtTrans(ji).ne.' ') then
            k=ktat(atom,NAtInd,AtTrans(ji))
            if(kswa(k).eq.KPhase) then
              do l=1,3
                trans(l,ji)=x(l,k)-xm(l,i)
              enddo
            endif
          endif
          do k=iap,iak
            call SetIntArrayTo(isa(1,k),MaxNSymm,0)
            do l=1,NSymm(KPhase)
              isa(l,k)=l
            enddo
          enddo
          call setpos(i,j,ji,iap,iak,isw,0)
        enddo
        if(klic.eq.0) then
          l=NAtCalc
          do ji=1,mam(i)
            do j=iap,iak
              l=l+1
              PrvniKiAtomu(l)=PosledniKiAtPos+1
              k=10
              if(KModA(1,l).gt.0.or.KModA(2,l).gt.0.or.KModA(3,l).gt.0)
     1          then
                if(KModA(1,l).gt.0) k=k+1+2*KModA(1,l)
                k=k+ 6*KModA(2,l)
                k=k+12*KModA(3,l)
                k=k+1
              endif
              DelkaKiAtomu(l)=k
              PosledniKiAtPos=PosledniKiAtPos+DelkaKiAtomu(l)
            enddo
          enddo
        endif
        NAtCalc=NAtCalc+iamni*mam(i)
        NAtCalcBasic=NAtCalc
      enddo
      if(klic.eq.0) then
        j=PosledniKiAtPos-PrvniKiAtMol+1
        k=PosledniKiAtMol-PrvniKiAtMol+1
        PrvniKiAtMol=PrvniKiAtMol+j
        PosledniKiAtMol=PosledniKiAtMol+j
        do i=NAtMolFr(1,1),NAtAll
          PrvniKiAtomu(i)=PrvniKiAtomu(i)+j
        enddo
        do i=1,NMolec
          do j=1,mam(i)
            ji=j+(i-1)*mxp
            PrvniKiMolekuly(ji)=PrvniKiMolekuly(ji)+PosledniKiAtMol
          enddo
        enddo
        PrvniKiMol=PrvniKiMolekuly(1)
        PosledniKiMol=PosledniKiMol+PosledniKiAtMol
      endif
9999  KPhase=KPhaseIn
      return
      end
