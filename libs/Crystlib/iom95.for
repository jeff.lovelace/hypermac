      subroutine iom95(klic,FileName)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(36)
      character*256 Veta
      character*80  t80
      character*(*) FileName
      character*12 :: Nazev,id(100) =
     1   (/'sourcefile  ','difcode     ','corrlp      ','corrabs     ',
     2     'nref        ','ndim95      ','lambda      ','radtype     ',
     3     'datcolltemp ','cell        ','qvec        ','mi          ',
     4     'radius      ','face        ','ormat       ','trmat       ',
     5     'mmax        ','diffsat     ','refblock    ','end         ',
     6     'gauss       ','commcell    ','commqvec    ','trcell      ',
     7     'filedate    ','filetime    ','format      ','twread      ',
     8     'monangle    ','polarization','perfmono    ','nalpha      ',
     9     'kalpha1     ','kalpha2     ','kalphar     ','toftth      ',
     a     'tofjason    ','tofdifc     ','tofdifa     ','tofzero     ',
     1     'tofct       ','tofat       ','tofzerot    ','tofce       ',
     2     'tofzeroe    ','tofwcross   ','toftcross   ','tofsig2     ',
     3     'tofsig1     ','tofsig0     ','tofgam2     ','tofgam1     ',
     4     'tofgam0     ','tofalfa0    ','tofalfa1    ','tofbeta0    ',
     5     'tofbeta1    ','tofalfa0t   ','tofalfa1t   ','tofbeta0t   ',
     6     'tofbeta1t   ','tofalfa0e   ','tofalfa1e   ','tofbeta0e   ',
     7     'tofbeta1e   ','nlines      ','scmax       ','correspond  ',
     8     'scale       ','edoffset    ','edslope     ','edtth       ',
     9     'pwdmethod   ','hklf5       ','incspectfile','insparfile  ',
     a     'cellesd     ','commcellesd ','imponlysat  ','alphagmono  ',
     1     'betagmono   ','run         ','framegr     ','cutminave   ',
     2     'cutmaxave   ','nrefmaxave  ','spheven     ','sphodd      ',
     3     'scfrlist    ','sphlist     ','pgave       ','flimcullave ',
     4     'xddircos    ','useexpcorr  ','magfield    ','magpol      ',
     5     'magpolplus  ','magpolminus ','scfrmethod  ','delmi       '/)
      equivalence
     1    (IdNumbers( 1),IdFile),
     2    (IdNumbers( 2),IdDifCode),
     3    (IdNumbers( 3),IdCorrLp),
     4    (IdNumbers( 4),IdCorrAbs),
     5    (IdNumbers( 5),IdNref95),
     6    (IdNumbers( 6),IdNDim95),
     7    (IdNumbers( 7),IdLambda),
     8    (IdNumbers( 8),IdRadType),
     9    (IdNumbers( 9),IdDatCollTemp),
     a    (IdNumbers(10),IdCell),
     1    (IdNumbers(11),IdQVec),
     2    (IdNumbers(12),IdAMi),
     3    (IdNumbers(13),IdRadius),
     4    (IdNumbers(14),IdFace),
     5    (IdNumbers(15),IdOrmat),
     6    (IdNumbers(16),IdTrMat),
     7    (IdNumbers(17),Idmmax),
     8    (IdNumbers(18),IdDiffSat),
     9    (IdNumbers(19),IdRefBlock),
     a    (IdNumbers(20),IdEnd),
     1    (IdNumbers(21),IdGauss),
     2    (IdNumbers(22),IdCommCell),
     3    (IdNumbers(23),IdCommQvec),
     4    (IdNumbers(24),IdTrCell),
     5    (IdNumbers(25),IdFileDate),
     6    (IdNumbers(26),IdFileTime),
     7    (IdNumbers(27),IdFormat),
     8    (IdNumbers(28),IdTwRead),
     9    (IdNumbers(29),IdMonAngle),
     a    (IdNumbers(30),IdPolar),
     1    (IdNumbers(31),IdPerfMono),
     2    (IdNumbers(32),IdNAlpha),
     3    (IdNumbers(33),IdKalpha1),
     4    (IdNumbers(34),IdKalpha2),
     5    (IdNumbers(35),IdKalphar),
     6    (IdNumbers(36),IdTOFTTh),
     7    (IdNumbers(37),IdTOFJason),
     8    (IdNumbers(38),IdTOFDIFC),
     9    (IdNumbers(39),IdTOFDIFA),
     a    (IdNumbers(40),IdTOFZero),
     1    (IdNumbers(41),IdTOFCt),
     2    (IdNumbers(42),IdTOFAt),
     3    (IdNumbers(43),IdTOFZeroT),
     4    (IdNumbers(44),IdTOFCe),
     5    (IdNumbers(45),IdTOFZeroe),
     6    (IdNumbers(46),IdTOFWCross),
     7    (IdNumbers(47),IdTOFTCross),
     8    (IdNumbers(48),IdTOFSig2),
     9    (IdNumbers(49),IdTOFSig1),
     a    (IdNumbers(50),IdTOFSig0),
     1    (IdNumbers(51),IdTOFGam2),
     2    (IdNumbers(52),IdTOFGam1),
     3    (IdNumbers(53),IdTOFGam0),
     4    (IdNumbers(54),IdTOFAlpha0),
     5    (IdNumbers(55),IdTOFAlpha1),
     6    (IdNumbers(56),IdTOFBeta0),
     7    (IdNumbers(57),IdTOFBeta1),
     8    (IdNumbers(58),IdTOFAlpha0t),
     9    (IdNumbers(59),IdTOFAlpha1t),
     a    (IdNumbers(60),IdTOFBeta0t),
     1    (IdNumbers(61),IdTOFBeta1t),
     2    (IdNumbers(62),IdTOFAlpha0e),
     3    (IdNumbers(63),IdTOFAlpha1e),
     4    (IdNumbers(64),IdTOFBeta0e),
     5    (IdNumbers(65),IdTOFBeta1e),
     6    (IdNumbers(66),IdNLines95),
     7    (IdNumbers(67),IdScMax),
     8    (IdNumbers(68),IdCorrespond),
     9    (IdNumbers(69),IdScale),
     a    (IdNumbers(70),IdEDOffset),
     1    (IdNumbers(71),IdEDSlope),
     2    (IdNumbers(72),IdEDTTh),
     3    (IdNumbers(73),IdPwdMethod),
     4    (IdNumbers(74),IdHKLF5),
     5    (IdNumbers(75),IdIncSpectFile),
     6    (IdNumbers(76),IdInsParFile),
     7    (IdNumbers(77),IdCellEsd),
     8    (IdNumbers(78),IdCommCellEsd),
     9    (IdNumbers(79),IdImpOnlySat),
     a    (IdNumbers(80),IdAlphaGMono),
     1    (IdNumbers(81),IdBetaGMono),
     2    (IdNumbers(82),IdRun),
     3    (IdNumbers(83),IdFrameGr),
     4    (IdNumbers(84),IdCutMinAve),
     5    (IdNumbers(85),IdCutMaxAve),
     6    (IdNumbers(86),IdNRefMaxAve),
     7    (IdNumbers(87),IdSpHEven),
     8    (IdNumbers(88),IdSpHOdd),
     9    (IdNumbers(89),IdScFrList),
     a    (IdNumbers(90),IdSpHList),
     1    (IdNumbers(91),IdPGAve),
     2    (IdNumbers(92),IdFLimCullAve),
     3    (IdNumbers(93),IdXDDirCos),
     4    (IdNumbers(94),IdUseExpCorr),
     5    (IdNumbers(95),IdMagField),
     6    (IdNumbers(96),IdMagPolFlag),
     7    (IdNumbers(97),IdMagPolPlus),
     8    (IdNumbers(98),IdMagPolMinus),
     9    (IdNumbers(99),IdScFrMethod),
     a    (IdNumbers(100),IdDelMi)
      equivalence (xp,Value)
      KRefBlockIn=KRefBlock
      ln=NextLogicNumber()
      ExistPowder=.false.
      if(Klic.eq.0) then
        NRefBlock=0
        ndq=0
        ndqc=0
        call SetIntArrayTo(NFaces,MxRefBlock,0)
        if(.not.ExistM95) go to 9999
        call OpenFile(ln,FileName,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        k=256
1000    if(k.ge.256) then
          read(ln,FormA,end=8000) Veta
          k=0
        endif
        call kus(Veta,k,Nazev)
        j=islovo(Nazev,id,100)
        if(j.eq.IdRefBlock.or.j.eq.IdEnd) then
          if(NRefBlock.gt.0) then

          endif
          if(j.eq.IdRefBlock) then
            NRefBlock=NRefBlock+1
            k=256
            call SetBasicM95(NRefBlock)
            ndq=0
            go to 1000
          else
            go to 8000
          endif
        else if(j.eq.IdCommCell) then
          call StToReal(Veta,k,CellRefBlock(1,0),6,.false.,ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdCommCellEsd) then
          call StToReal(Veta,k,CellRefBlockSU(1,0),6,.false.,ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdCommQVec) then
          if(ndqc.le.0) call SetRealArrayTo(QuRefBlock(1,1,0),9,-333.)
          ndqc=ndqc+1
          call StToReal(Veta,k,QuRefBlock(1,ndqc,0),3,.false.,ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdTrCell) then
          n=ndqc+3
          j=1
          do l=1,n
            read(ln,FormA) Veta
            k=0
            call StToReal(Veta,k,xp(j),n,.false.,ich)
            if(ich.ne.0) go to 9000
            j=j+n
          enddo
          call TrMat(xp,TrMP,n,n)
          call MatInv(TrMP,TrMPI,pom,n)
          k=256
        else if(j.eq.IdFile) then
          SourceFileRefBlock(NRefBlock)=Veta(k+1:)
          k=256
        else if(j.eq.IdInsParFile) then
          InsParFileRefBlock(NRefBlock)=Veta(k+1:)
          k=256
        else if(j.eq.IdIncSpectFile) then
          IncSpectFileRefBlock(NRefBlock)=Veta(k+1:)
          k=256
        else if(j.eq.IdFile) then
          SourceFileRefBlock(NRefBlock)=Veta(k+1:)
          k=256
        else if(j.eq.IdFileDate) then
          call kus(Veta,k,SourceFileDateRefBlock(NRefBlock))
          k=256
        else if(j.eq.IdFileTime) then
          call kus(Veta,k,SourceFileTimeRefBlock(NRefBlock))
          k=256
        else if(j.eq.IdFormat) then
          FormatRefBlock(NRefBlock)=Veta(k+1:)
          k=256
        else if(j.eq.IdQVec) then
          ndq=ndq+1
          call StToReal(Veta,k,QuRefBlock(1,ndq,NRefBlock ),3,.false.,
     1                  ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdCell) then
          CellReadIn(NRefBlock)=.true.
          call StToReal(Veta,k,CellRefBlock(1,NRefBlock ),6,.false.,ich)
          if(ich.ne.0) go to 9000
        else if(j.eq.IdCellEsd) then
          call StToReal(Veta,k,CellRefBlockSU(1,NRefBlock ),6,.false.,
     1                  ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdFace) then
          NFaces(NRefBlock)=NFaces(NRefBlock)+1
          n=NFaces(NRefBlock)
          call StToReal(Veta,k,DFace(1,n,NRefBlock ),4,.false.,ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdRun) then
          NRuns(NRefBlock)=NRuns(NRefBlock)+1
          call StToReal(Veta,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9000
          RunNFrames(NRuns(NRefBlock),NRefBlock)=nint(xp(1))
          RunMeasureTime(NRuns(NRefBlock),NRefBlock)=xp(2)
          RunCCDMax(NRefBlock)=RunCCDMax(NRefBlock)+nint(xp(1))
          go to 1000
        else if(j.eq.IdGauss) then
          call StToInt(Veta,k,GaussRefBlock(1,NRefBlock),3,.false.,ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdOrmat) then
          j=1
          do l=1,3
            read(ln,FormA) Veta
            k=0
            call StToReal(Veta,k,xp(j),3,.false.,ich)
            if(ich.ne.0) go to 9000
            j=j+3
          enddo
          call TrMat(xp,ub(1,1,NRefBlock),3,3)
          k=256
        else if(j.eq.IdTrMat) then
          n=ndqc+3
          j=1
          UseTrRefBlock(NRefBlock)=.true.
          IntTrRefBlock(NRefBlock)=.true.
          do l=1,n
            read(ln,FormA) Veta
            k=0
            call StToReal(Veta,k,xp(j),n,.false.,ich)
            if(ich.ne.0) go to 9000
            if(IntTrRefBlock(NRefBlock)) then
              do m=j,j+n-1
                if(abs(anint(xp(m))-xp(m)).gt..0001) then
                  IntTrRefBlock(NRefBlock)=.false.
                  go to 1035
                endif
              enddo
            endif
1035        j=j+n
          enddo
          call TrMat(xp,TrRefBlock(1,NRefBlock),n,n)
          k=256
        else if(j.eq.IdDiffSat) then
          call StToReal(Veta,k,DiffSatRefBlock(1,NRefBlock),3,.false.,
     1                  ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdPGAve) then
          call Kus(Veta,k,PGAve(NRefBlock))
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.IdMagField) then
          call StToReal(Veta,k,MagFieldDirRefBlock(1,NRefBlock),3,
     1                  .false.,ich)
          if(ich.ne.0) then
            go to 9000
          else
            go to 1000
          endif
        else if(j.eq.0) then
          go to 1000
        endif
        if(k.ge.256) go to 1000
        call StToReal(Veta,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 1000
        IValue=nint(Value)
        if(j.eq.IdDifCode) then
          DifCode(NRefBlock)=IValue
          ExistPowder=ExistPowder.or.DifCode(NRefBlock).gt.100
        else if(j.eq.IdCorrLp) then
          CorrLp(NRefBlock)=IValue
        else if(j.eq.IdCorrAbs) then
          CorrAbs(NRefBlock)=IValue
        else if(j.eq.IdNRef95) then
          NRef95(NRefBlock)=IValue
        else if(j.eq.IdNLines95) then
          NLines95(NRefBlock)=IValue
        else if(j.eq.IdNDim95) then
          NDim95(NRefBlock)=IValue
        else if(j.eq.IdTwRead) then
          ITwRead(NRefBlock)=IValue
        else if(j.eq.IdLambda) then
          LamAveRefBlock(NRefBlock)=Value
          KLamRefBlock(NRefBlock)=
     1      LocateInArray(LamAveRefBlock(NRefBlock),LamAveD,7,.0001)
        else if(j.eq.IdRadType) then
          RadiationRefBlock(NRefBlock)=IValue
        else if(j.eq.IdDatCollTemp) then
          TempRefBlock(NRefBlock)=Value
        else if(j.eq.IdAMi) then
          AmiRefBlock(NRefBlock)=Value
        else if(j.eq.IdRadius) then
          RadiusRefBlock(NRefBlock)=Value
        else if(j.eq.Idmmax) then
          MMaxRefBlock(NRefBlock)=IValue
        else if(j.eq.IdMonAngle) then
          AngleMonRefBlock(NRefBlock)=Value
        else if(j.eq.IdAlphaGMono) then
          AlphaGMonRefBlock(NRefBlock)=Value
        else if(j.eq.IdBetaGMono) then
          BetaGMonRefBlock(NRefBlock)=Value
        else if(j.eq.IdPolar) then
          PolarizationRefBlock(NRefBlock)=IValue
        else if(j.eq.IdPerfMono) then
          FractPerfMonRefBlock(NRefBlock)=Value
        else if(j.eq.IdNAlpha) then
          NAlfaRefBlock(NRefBlock)=IValue
        else if(j.eq.IdKalpha1) then
          LamA1RefBlock(NRefBlock)=Value
        else if(j.eq.IdKalpha2) then
          LamA2RefBlock(NRefBlock)=Value
        else if(j.eq.IdKalphar) then
          LamRatRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFTTh) then
          TOFTThRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFJason) then
          TOFJasonRefBlock(NRefBlock)=IValue
        else if(j.eq.IdTOFDIFC) then
          TOFDifcRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFDIFA) then
          TOFDifaRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFZERO) then
          TOFZeroRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFCt) then
          TOFCtRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFAt) then
          TOFAtRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFZeroT) then
          TOFZeroTRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFCe) then
          TOFCeRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFZeroE) then
          TOFZeroERefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFWCross) then
          TOFWCrossRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFTCross) then
          TOFTCrossRefBlock(NRefBlock)=Value
        else if(j.eq.IdTOFSig2) then
          TOFSigmaRefBlock(1,NRefBlock)=Value
        else if(j.eq.IdTOFSig1) then
          TOFSigmaRefBlock(2,NRefBlock)=Value
        else if(j.eq.IdTOFSig0) then
          TOFSigmaRefBlock(3,NRefBlock)=Value
        else if(j.eq.IdTOFGam2) then
          TOFGammaRefBlock(1,NRefBlock)=Value
        else if(j.eq.IdTOFGam1) then
          TOFGammaRefBlock(2,NRefBlock)=Value
        else if(j.eq.IdTOFGam0) then
          TOFGammaRefBlock(3,NRefBlock)=Value
        else if(j.eq.IdTOFAlpha0) then
          TOFAlfbeRefBlock(1,NRefBlock)=Value
        else if(j.eq.IdTOFBeta0) then
          TOFAlfbeRefBlock(2,NRefBlock)=Value
        else if(j.eq.IdTOFAlpha1) then
          TOFAlfbeRefBlock(3,NRefBlock)=Value
        else if(j.eq.IdTOFBeta1) then
          TOFAlfbeRefBlock(4,NRefBlock)=Value
        else if(j.eq.IdTOFAlpha0e) then
          TOFAlfbeRefBlock(1,NRefBlock)=Value
        else if(j.eq.IdTOFBeta0e) then
          TOFAlfbeRefBlock(2,NRefBlock)=Value
        else if(j.eq.IdTOFAlpha1e) then
          TOFAlfbeRefBlock(3,NRefBlock)=Value
        else if(j.eq.IdTOFBeta1e) then
          TOFAlfbeRefBlock(4,NRefBlock)=Value
        else if(j.eq.IdTOFAlpha0t) then
          TOFAlfbtRefBlock(1,NRefBlock)=Value
        else if(j.eq.IdTOFBeta0t) then
          TOFAlfbtRefBlock(2,NRefBlock)=Value
        else if(j.eq.IdTOFAlpha1t) then
          TOFAlfbtRefBlock(3,NRefBlock)=Value
        else if(j.eq.IdTOFBeta1t) then
          TOFAlfbtRefBlock(4,NRefBlock)=Value
        else if(j.eq.IdScMax) then
          ScMaxRead(NRefBlock)=IValue
        else if(j.eq.IdCorrespond) then
          RefDatCorrespond(NRefBlock)=IValue
        else if(j.eq.IdScale) then
          ScaleRefBlock(NRefBlock)=Value
        else if(j.eq.IdEDOffset) then
          EDOffsetRefBlock(NRefBlock)=Value
        else if(j.eq.IdEDSlope) then
          EDSlopeRefBlock(NRefBlock)=Value
        else if(j.eq.IdEDTTh) then
          EDTThRefBlock(NRefBlock)=Value
          EDEnergy2DRefBlock(NRefBlock)=
     1      .5*TokEV/sin(EDTThRefBlock(NRefBlock)*.5*ToRad)
        else if(j.eq.IdPwdMethod) then
          PwdMethodRefBlock(NRefBlock)=IValue
        else if(j.eq.IdHKLF5) then
          HKLF5RefBlock(NRefBlock)=IValue
        else if(j.eq.IdImpOnlySat) then
          ImportOnlySatellites(NRefBlock)=IValue.eq.1
        else if(j.eq.IdFrameGr) then
          RunScGr(NRefBlock)=IValue
          call StToInt(Veta,k,RunScN(NRefBlock),1,.false.,ich)
        else if(j.eq.IdCutMinAve) then
          CutMinAve(NRefBlock)=Value
        else if(j.eq.IdCutMaxAve) then
          CutMaxAve(NRefBlock)=Value
        else if(j.eq.IdNRefMaxAve) then
          NRefMaxAve(NRefBlock)=IValue
        else if(j.eq.IdFLimCullAve) then
          FLimCullAve(NRefBlock)=IValue
        else if(j.eq.IdSpHEven) then
          SpHEven(NRefBlock)=IValue
        else if(j.eq.IdSpHOdd) then
          SpHOdd(NRefBlock)=IValue
        else if(j.eq.IdScFrMethod) then
          ScFrMethod(NRefBlock)=IValue
        else if(j.eq.IdScFrList) then
          if(allocated(ScFrame)) deallocate(ScFrame)
          allocate(ScFrame(IValue,MxRefBlock))
          read(ln,*,end=9000,err=9000) ScFrame(1:IValue,NRefBlock)
        else if(j.eq.IdSpHList) then
          if(allocated(SpHAlm)) deallocate(SpHAlm)
          allocate(SpHAlm(IValue,MxRefBlock))
          read(ln,*,end=9000,err=9000) SpHAlm(1:IValue,NRefBlock)
          SphN(NRefBlock)=IValue
        else if(j.eq.IdXDDirCos) then
          XDDirCosRefBlock(NRefBlock)=IValue
        else if(j.eq.IdUseExpCorr) then
          UseExpCorr(NRefBlock)=IValue
        else if(j.eq.IdMagPolFlag) then
          MagPolFlagRefBlock(NRefBlock)=IValue
        else if(j.eq.IdMagPolPlus) then
          MagPolPlusRefBlock(NRefBlock)=Value
        else if(j.eq.IdMagPolMinus) then
          MagPolMinusRefBlock(NRefBlock)=Value
        else if(j.eq.IdDelMi) then
          DelMi(NRefBlock)=Value
        endif
        go to 1000
      else if(Klic.eq.1) then
        call OpenFile(ln,FileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        if(NRefBlock.gt.0) then
          write(Veta,109)(CellRefBlock(j,0),j=1,6)
          k=0
          call WriteLabeledRecord(ln,Id(IdCommCell),Veta,k)
          write(Veta,109)(CellRefBlockSU(j,0),j=1,6)
          k=0
          call WriteLabeledRecord(ln,Id(IdCommCellEsd),Veta,k)
          do k=1,NDimI(KPhase)
            write(Veta,103)(QuRefBlock(j,k,0),j=1,3)
            l=0
            call WriteLabeledRecord(ln,Id(IdCommQVec),Veta,l)
          enddo
          write(ln,FormA) Id(IdTrCell)(:idel(Id(IdTrCell)))
          do j=1,NDim(KPhase)
            write(ln,105)(TrMP(j+(k-1)*NDim(KPhase)),k=1,NDim(KPhase))
          enddo
        endif
        n=0
        do i=1,NRefBlock
          if(NRef95(i).le.0) cycle
          n=n+1
          k=0
          call WriteLabeledRecord(ln,id(IdRefBlock),DatBlockName(n),k)
          k=0
          call WriteLabeledRecord(ln,id(IdFile),SourceFileRefBlock(i),k)
          k=0
          Veta=SourceFileDateRefBlock(i)//' '//
     1         id(IdFileTime)(:idel(id(IdFileTime)))//' '//
     2         SourceFileTimeRefBlock(i)
          call WriteLabeledRecord(ln,id(IdFileDate),Veta,k)
          if(InsParFileRefBlock(i).ne.' ') then
            k=0
            call WriteLabeledRecord(ln,id(IdInsParFile),
     1                              InsParFileRefBlock(i),k)
          endif
          if(IncSpectFileRefBlock(i).ne.' ') then
            k=0
            call WriteLabeledRecord(ln,id(IdIncSpectFile),
     1                              IncSpectFileRefBlock(i),k)
          endif
          ExistPowder=ExistPowder.or.DifCode(i).gt.100
          if(DifCode(i).lt.0.and.DifCode(i).ne.IdImportCIF.and.
     1       DifCode(i).ne.IdImportGraindex) then
            k=0
            call WriteLabeledRecord(ln,id(IdFormat),FormatRefBlock(i),k)
          endif
          if(DifCode(i).gt.0.and.DifCode(i).lt.100) then
            write(Veta,102) DifCode(i),
     1                      Id(IdCorrespond),RefDatCorrespond(i),
     2                      Id(IdCorrLp),CorrLp(i),
     3                      Id(IdCorrAbs),CorrAbs(i),
     4                      Id(IdHKLF5),HKLF5RefBlock(i),
     5                      Id(IdUseExpCorr),UseExpCorr(i)
          else if(DifCode(i).lt.0) then
            write(Veta,102) DifCode(i),
     1                      Id(IdCorrespond),RefDatCorrespond(i),
     2                      Id(IdHKLF5),HKLF5RefBlock(i)
          else
            write(Veta,102) DifCode(i),
     1                      Id(IdCorrespond),RefDatCorrespond(i),
     2                      Id(IdPwdMethod),PwdMethodRefBlock(i)
          endif
          if(XDDirCosRefBlock(i).gt.0) then
            write(t80,100) Id(IdXDDirCos),XDDirCosRefBlock(i)
            call ZdrcniCisla(t80,2)
            Veta=Veta(:idel(Veta)+1)//t80(:idel(t80))
          endif
          k=0
          call WriteLabeledRecord(ln,Id(IdDifCode),Veta,k)
          if(LamAveRefBlock(i).gt.0.) then
            write(Veta,101) LamAveRefBlock(i),Id(IdRadType),
     1                      RadiationRefBlock(i),
     2                      Id(IdPolar),PolarizationRefBlock(i),
     3                      Id(IdDatCollTemp),TempRefBlock(i)
            Nazev=id(IdLambda)
          else
            write(Veta,106) RadiationRefBlock(i),
     1                      Id(IdDatCollTemp),TempRefBlock(i)
            Nazev=Id(IdRadType)
          endif
          k=0
          call WriteLabeledRecord(ln,Nazev,Veta,k)
          if(DifCode(i).le.200.and.
     1       RadiationRefBlock(i).eq.XRayRadiation) then
            if(PolarizationRefBlock(i).eq.PolarizedMonoPer.or.
     1         PolarizationRefBlock(i).eq.PolarizedMonoPar) then
              write(Veta,107) AngleMonRefBlock(i),
     1                        Id(IdPerfMono),FractPerfMonRefBlock(i)
              k=0
              call WriteLabeledRecord(ln,Id(IdMonAngle),Veta,k)
            else if(PolarizationRefBlock(i).eq.PolarizedGuinier) then
              write(Veta,107) AlphaGMonRefBlock(i),
     1                        Id(IdBetaGMono),BetaGMonRefBlock(i),
     2                        Id(IdPerfMono),FractPerfMonRefBlock(i)
              k=0
              call WriteLabeledRecord(ln,Id(IdAlphaGMono),Veta,k)
            endif
          endif
          if(DifCode(i).gt.100) then
            if(DifCode(i).le.200) then
              if(LamAveRefBlock(i).gt.0.) then
                if(NAlfaRefBlock(i).gt.1) then
                  write(Veta,'(i1,3(1x,a12,1x,f10.6))')
     1              NAlfaRefBlock(i),
     2              id(IdKAlpha1),LamA1RefBlock(i),
     3              id(IdKAlpha2),LamA2RefBlock(i),
     4              id(IdKAlphar),LamRatRefBlock(i)
                  k=0
                  call WriteLabeledRecord(ln,Id(IdNAlpha),Veta,k)
                endif
              endif
            endif
            write(Veta,106) NLines95(i)
            k=0
            call WriteLabeledRecord(ln,Id(IdNLines95),Veta,k)
            if(DifCode(i).gt.200.and.DifCode(i).le.300) then
              if(TOFDifcRefBlock(i).gt.0.) then
                if(TOFJasonRefBlock(i).eq.0) then
                  write(Veta,111)
     1                      float(TOFJasonRefBlock(i)),
     2              id(IdTOFDIFC),TOFDifcRefBlock(i),
     3              id(IdTOFDIFA),TOFDifaRefBlock(i),
     4              id(IdTOFZero),TOFZeroRefBlock(i),
     5              id(IdTOFTTh) ,TOFTThRefBlock(i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFJason),Veta,k)
                else
                  write(Veta,111)
     1                       float(TOFJasonRefBlock(i)),
     2              id(IdTOFCt)   ,TOFCtRefBlock(i),
     3              id(IdTOFAt)   ,TOFAtRefBlock(i),
     4              id(IdTOFZerot),TOFZerotRefBlock(i),
     5              id(IdTOFTTh)  ,TOFTThRefBlock(i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFJason),Veta,k)
                  write(Veta,111)
     1                              TOFCeRefBlock(i),
     2              id(IdTOFZeroe) ,TOFZeroeRefBlock(i),
     3              id(IdTOFWCross),TOFWCrossRefBlock(i),
     4              id(IdTOFTCross),TOFTCrossRefBlock(i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFCe),Veta,k)
                endif
              endif
              if(TOFSigmaRefBlock(1,i).gt.-3000.) then
                write(Veta,111) TOFSigmaRefBlock(1,i),
     2            id(IdTOFSig1),TOFSigmaRefBlock(2,i),
     3            id(IdTOFSig0),TOFSigmaRefBlock(3,i)
                k=0
                call WriteLabeledRecord(ln,id(IdTOFSig2),Veta,k)
              endif
              if(TOFGammaRefBlock(1,i).gt.-3000.) then
                write(Veta,111) TOFGammaRefBlock(1,i),
     2            id(IdTOFGam1),TOFGammaRefBlock(2,i),
     3            id(IdTOFGam0),TOFGammaRefBlock(3,i)
                k=0
                call WriteLabeledRecord(ln,id(IdTOFGam2),Veta,k)
              endif
              if(TOFAlfbeRefBlock(1,i).gt.-3000.) then
                if(TOFJasonRefBlock(i).eq.0) then
                  write(Veta,111) TOFAlfbeRefBlock(1,i),
     2              id(IdTOFBeta0),TOFAlfbeRefBlock(2,i),
     3              id(IdTOFAlpha1),TOFAlfbeRefBlock(3,i),
     4              id(IdTOFBeta1),TOFAlfbeRefBlock(4,i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFAlpha0),Veta,k)
                else
                  write(Veta,111) TOFAlfbeRefBlock(1,i),
     2              id(IdTOFBeta0e),TOFAlfbeRefBlock(2,i),
     3              id(IdTOFAlpha1e),TOFAlfbeRefBlock(3,i),
     4              id(IdTOFBeta1e),TOFAlfbeRefBlock(4,i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFAlpha0e),Veta,k)
                  write(Veta,111) TOFAlfbtRefBlock(1,i),
     2              id(IdTOFBeta0t),TOFAlfbtRefBlock(2,i),
     3              id(IdTOFAlpha1t),TOFAlfbtRefBlock(3,i),
     4              id(IdTOFBeta1t),TOFAlfbtRefBlock(4,i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFAlpha0t),Veta,k)
                endif
              endif
            else if(DifCode(i).gt.300) then
              write(Veta,111)
     1                        EDOffsetRefBlock(i),
     2          id(IdEDSlope),EDSlopeRefBlock(i),
     4          id(IdEDTTh  ),EDTThRefBlock(i)
              k=0
              call WriteLabeledRecord(ln,id(IdEDOffset),Veta,k)
            endif
            cycle
          endif
          if(RadiationRefBlock(i).eq.NeutronRadiation.and.
     1       MagPolFlagRefBlock(i).ne.0) then
            write(Veta,'(i5,1x,a12,3f10.6,2(1x,a12,f10.6))')
     1        MagPolFlagRefBlock(i),
     2        id(IdMagField),MagFieldDirRefBlock(1:3,i),
     3        id(IdMagPolPlus),MagPolPlusRefBlock(i),
     4        id(IdMagPolMinus),MagPolMinusRefBlock(i)
            call ZdrcniCisla(Veta,9)
            k=0
            call WriteLabeledRecord(ln,id(IdMagPolFlag),Veta,k)
          endif
          if(CorrAbs(i).gt.0) then
            if(mod(CorrAbs(i),10).eq.IdCorrAbsSphere) then
              write(Veta,'(a12,f10.4)') id(IdRadius),RadiusRefBlock(i)
              call ZdrcniCisla(Veta,2)
            else
              Veta=' '
            endif
            if(mod(CorrAbs(i),10).eq.IdCorrAbsGauss) then
              write(Veta,100) id(IdGauss),
     1                                (GaussRefBlock(j,i),j=1,3)
              call ZdrcniCisla(Veta,4)
            else
              Veta=' '
            endif
            if(CorrAbs(i)/10.eq.1) then
              write(t80,100) id(IdFrameGr),RunScGr(i),RunScN(i)
              call ZdrcniCisla(t80,3)
              if(Veta.ne.' ') then
                Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
              else
                Veta=t80
              endif
              if(PGAve(i).ne.' ') then
                t80=id(IdPGAve)//' '//PGAve(i)
                call ZdrcniCisla(t80,2)
                Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
              endif
              if(SpHEven(i).gt.0) then
                write(Cislo,FormI15) SpHEven(i)
                t80=id(IdSpHEven)//Cislo
                call ZdrcniCisla(t80,2)
                write(Cislo,FormI15) SpHOdd(i)
                t80=t80(:idel(t80))//' '//id(IdSpHOdd)//Cislo
                call ZdrcniCisla(t80,4)
                Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
                if(mod(CorrAbs(i),10).eq.IdCorrAbsGauss) then
                  write(Cislo,'(f15.4)') DelMi(i)
                  t80=id(IdDelMi)//' '//Cislo
                  call ZdrcniCisla(t80,2)
                  Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
                endif
              endif
            endif
            if(AmiRefBlock(i).gt.0.) then
              write(Cislo,109) AmiRefBlock(i)
              t80=id(IdAMi)//' '//Cislo
              call ZdrcniCisla(t80,2)
              Veta=t80(:idel(t80))//' '//Veta(:idel(Veta))
            endif
            if(Veta.ne.' ') write(ln,FormA) Veta(:idel(Veta))
            if(mod(CorrAbs(i),10).eq.IdCorrAbsGauss.or.
     1         CorrAbs(i)/10.eq.1) then
              Veta=' '
              if(CutMinAve(i).gt.0.) then
                write(Cislo,104) CutMinAve(i)
                Veta=id(IdCutMinAve)//' '//Cislo
                call ZdrcniCisla(Veta,2)
              endif
              if(CutMaxAve(i).gt.0.) then
                write(Cislo,104) CutMaxAve(i)
                t80=id(IdCutMaxAve)//' '//Cislo
                call ZdrcniCisla(t80,2)
                Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
              endif
              if(FLimCullAve(i).gt.0.) then
                write(Cislo,104) FLimCullAve(i)
                t80=id(IdFLimCullAve)//' '//Cislo
                call ZdrcniCisla(t80,2)
                Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
              endif
              if(NRefMaxAve(i).gt.0.) then
                write(Cislo,FormI15) NRefMaxAve(i)
                t80=id(IdNRefMaxAve)//' '//Cislo
                call ZdrcniCisla(t80,2)
                Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
              endif
              if(Veta.ne.' ') write(ln,FormA) Veta(:idel(Veta))
            endif
            if(CorrAbs(i)/10.eq.1) then
              if(RunScN(i).gt.0) then
                write(Veta,102) ScFrMethod(i),id(IdScFrList),RunScN(i)
                k=0
                call WriteLabeledRecord(ln,id(IdScFrMethod),Veta,k)
                if(.not.allocated(ScFrame)) then
                  allocate(ScFrame(RunScN(i),MxRefBlock))
                  ScFrame(1:RunScN(i),KRefBlock)=1.
                endif
                write(ln,103) ScFrame(1:RunScN(i),KRefBlock)
              endif
              if(SpHEven(i).gt.0) then
                SpHN(i)=0
                do l=1,8
                  if(mod(l,2).eq.1) then
                    if(l.gt.SpHOdd(i)) cycle
                  else
                    if(l.gt.SpHEven(i)) cycle
                  endif
                  do k=-l,l
                    SpHN(i)=SpHN(i)+1
                  enddo
                enddo
                write(Veta,FormI15) SpHN(i)
                k=0
                call WriteLabeledRecord(ln,id(IdSpHList),Veta,k)
                if(.not.allocated(SpHAlm)) then
                  allocate(SpHAlm(SphN(KRefBlock),MxRefBlock))
                  SpHAlm(1,KRefBlock)=1.
                  SpHAlm(2:SphN(KRefBlock),KRefBlock)=0.
                endif
                write(ln,103) SpHAlm(1:SphN(KRefBlock),KRefBlock)
              endif
            endif
          endif
          write(Veta,102) nref95(i),Id(IdNDim95),NDim95(i),
     1                              Id(IdNLines95),NLines95(i)
          if(NTwin.gt.1) then
            write(t80,108) Id(IdTwRead),ITwRead(i)
            call ZdrcniCisla(t80,2)
            Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
          endif
          write(t80,108) Id(IdScMax),ScMaxRead(i)
          Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
          write(t80,'(a12,1x,f10.6)') Id(IdScale),ScaleRefBlock(i)
          call ZdrcniCisla(t80,2)
          Veta=Veta(:idel(Veta))//' '//t80(:idel(t80))
          k=0
          call WriteLabeledRecord(ln,Id(IdNRef95),Veta,k)
          if(NDimI(KPhase).gt.0) then
            if(ImportOnlySatellites(i)) then
              k=1
            else
              k=0
            endif
            write(Veta,110)(DiffSatRefBlock(j,i),j=1,3),Id(Idmmax),
     1                      MMaxRefBlock(i),
     2                      Id(IdImpOnlySat),k
            k=0
            call WriteLabeledRecord(ln,Id(IdDiffSat),Veta,k)
          endif
          if(CellReadIn(i)) then
            write(Veta,109)(CellRefBlock(j,i),j=1,6)
            k=0
            call WriteLabeledRecord(ln,Id(IdCell),Veta,k)
            k=0
            write(Veta,109)(CellRefBlockSU(j,i),j=1,6)
            call WriteLabeledRecord(ln,Id(IdCellEsd),Veta,k)
            do k=1,NDimI(KPhase)
              if(QuRefBlock(1,k,i).gt.-300.) then
                write(Veta,103)(QuRefBlock(j,k,i),j=1,3)
                l=0
                call WriteLabeledRecord(ln,Id(IdQVec),Veta,l)
              endif
            enddo
          endif
          if(ub(1,1,i).gt.-300.) then
            write(ln,FormA) Id(IdOrmat)(:idel(Id(IdOrmat)))
            do j=1,3
              write(ln,105)(ub(j,k,i),k=1,3)
            enddo
          endif
          if(UseTrRefBlock(i)) then
            write(ln,FormA) Id(IdTrMat)(:idel(Id(IdTrMat)))
            do j=1,NDim(KPhase)
              write(ln,105)(TrRefBlock(j+(k-1)*NDim(KPhase),i),k=1,
     1                      NDim(KPhase))
            enddo
          endif
          do j=1,NFaces(i)
            write(Veta,104)(dface(k,j,i),k=1,4)
            k=0
            call WriteLabeledRecord(ln,Id(IdFace),Veta,k)
          enddo
          if(RunCCDMax(i).gt.1) then
            do j=1,NRuns(i)
              write(Veta,'(i10,f15.2)')
     1          RunNFrames(j,i),RunMeasureTime(j,i)
              k=0
              call WriteLabeledRecord(ln,Id(IdRun),Veta,k)
            enddo
          else
            NRuns=0
          endif
        enddo
        Veta=' '
        k=0
        call WriteLabeledRecord(ln,Id(IdEnd),Veta,k)
      endif
      go to 9999
8000  do i=1,NRefBlock
        if(DifCode(i).gt.100) then
          NRef95(i)=NLines95(i)
        endif
        if(LamA1RefBlock(i).le.0.and.LamAveRefBlock(i).gt.0) then
          LamA1RefBlock(i)=LamAveRefBlock(i)
          LamA2RefBlock(i)=LamAveRefBlock(i)
        endif
        KRefBlock=i
        if(RunScGr(i).gt.0) call DRSetRunScGrN(ich)
      enddo
      go to 9999
9000  call FeChybne(-1.,-1.,'syntax problem during reading m95.',
     1              Veta,SeriousError)
      ErrFlag=1
9999  call CloseIfOpened(ln)
      KRefBlock=KRefBlockIn
      return
100   format(a12,3i5)
101   format(f11.5,1x,2(a12,1x,i1,1x),a12,f11.2)
102   format(i12,10(1x,a12,1x,i12))
103   format(8f10.6)
104   format(6f11.4)
105   format(6f12.6)
106   format(i12,1x,a12,f11.2)
107   format(f11.4,1x,4(a12,f11.2,1x))
108   format(a12,1x,i5)
109   format(3f10.4,3f10.3)
110   format(3f10.4,3(1x,a12,i10))
111   format(f10.2,6(1x,a12,1x,f10.4))
      end
