      subroutine BatchReadCIF(InputFile,icho)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*256 InputFile
      character*256 Veta,t256
      integer FeChDir
      logical Change,ExistFile
      icho=0
      KDatBlock=1
      call DeleteFile(fln(:ifln)//'.m40')
      call DeleteFile(fln(:ifln)//'.m50')
      call DeleteFile(fln(:ifln)//'.m90')
      call DeleteFile(fln(:ifln)//'.m95')
      call JanaReset(Change)
      Veta='CIF file import'
      call BatchHlavicka(Veta,'-')
      if(InputFile.eq.' ') then
        read(BatchLN,FormA,end=9100,err=9200) Veta
        call DeleteFirstSpaces(Veta)
      else
        Veta=InputFile
      endif
      t256='Input file: '//Veta(:idel(Veta))
      call FeLstWriteLine(t256,-1)
      write(LogLn,FormA) t256(:idel(t256))
      call ReadSpecifiedCIF(Veta,-1)
      if(ErrFlag.ne.0) go to 9300
      call iom40(0,0,fln(:ifln)//'.m40')
      call iom50(0,0,fln(:ifln)//'.m50')
      go to 9999
9100  icho=1
      go to 9999
9200  icho=2
      go to 9999
9300  icho=3
9999  ErrFlag=0
      return
      end
