      subroutine SetSymmWaves
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer GammaInt(:,:,:)
      logical eqiv
      dimension GammaIntP(9),GammaIntPI(9),kwp(3),kwz(3)
      allocatable GammaInt
      allocate(GammaInt(9,NSymm(KPhase),NComp(KPhase)))
      do isw=1,NComp(KPhase)
        do is=1,NSymm(KPhase)
          do j=4,NDim(KPhase)
            do i=4,NDim(KPhase)
              GammaIntP(i-3+(j-4)*NDimI(KPhase))=
     1          rm6(i+(j-1)*NDim(KPhase),is,isw,KPhase)
            enddo
          enddo
          call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
          do j=1,NDimI(KPhase)**2
            GammaInt(j,is,isw)=nint(GammaIntPI(j))
          enddo
          do 1700j=1,MaxUsedKw(KPhase)
            call multmi(kw(1,j,KPhase),GammaInt(1,is,isw),kwp,1,
     1                  NDimI(KPhase),NDimI(KPhase))
            do k=1,NDimI(KPhase)
              kwz(k)=-kwp(k)
            enddo
            do k=1,MaxUsedKw(KPhase)
              if(eqiv(kw(1,k,KPhase),kwp,NDimI(KPhase))) then
                KwSym(j,is,isw,KPhase)=k
                go to 1700
              else if(eqiv(kw(1,k,KPhase),kwz,NDimI(KPhase))) then
                KwSym(j,is,isw,KPhase)=-k
                go to 1700
              endif
            enddo
            KwSym(j,is,isw,KPhase)=0
1700      continue
        enddo
      enddo
      deallocate(GammaInt)
      return
      end
