      subroutine CrlSetLocAtSyst(ia)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical EqIgCase
      real xx(3)
      SmbPGAt(ia)='1'
      LocAtSystAx(ia)(1:2)='xy'
      if(index(LocAtSystSt(1,ia),'*').gt.0) then
        if(isf(ia).eq.isfh(KPhase).or..not.
     1       (EqIgCase(AtType(isf(ia),KPhase),'C').or.
     2        EqIgCase(AtType(isf(ia),KPhase),'N').or.
     3        EqIgCase(AtType(isf(ia),KPhase),'O'))) then
          call DistFromAtom(Atom(ia),3.,iswa(ia))
          n=0
          do i=1,NDist
            k=ipord(i)
            iah=ktat(Atom,NAtCalc,ADist(k))
            if(isf(iah).eq.isfh(KPhase)) cycle
            n=n+1
            KeepAtNeigh(1,n)=ADist(k)
            j=idel(SymCodeJanaDist(k))
            if(j.gt.0) KeepAtNeigh(1,n)=
     1        KeepAtNeigh(1,n)(:idel(KeepAtNeigh(1,n)))//'#'//
     2        SymCodeJanaDist(k)(:j)
            if(n.ge.2) exit
          enddo
          if(n.ge.2) then
            KeepNNeigh(1)=2
            if(isf(ia).eq.isfh(KPhase)) then
              LocAtSystAx(ia)(1:2)='zx'
              SmbPGAt(ia)='2'
            endif
          else
            KeepNNeigh(1)=0
          endif
        else
          call EM40SetKeepForNewH(ia,1,NAtCalc,isfh(KPhase))
          LocAtSystAx(ia)(1:2)='xy'
          if(EqIgCase(AtType(isf(ia),KPhase),'C').or.
     1       EqIgCase(AtType(isf(ia),KPhase),'N').or.
     2       EqIgCase(AtType(isf(ia),KPhase),'O')) then
            SmbPGAt(ia)='m'
          endif
        endif
        do i=1,2
          if(KeepNNeigh(1).ge.2) then
            LocAtSystSt(i,ia)=KeepAtNeigh(1,i)
          else
            xx=0.
            xx(i)=1.
            write(LocAtSystSt(i,ia),'(3f9.6)') xx(1:3)
            call ZdrcniCisla(LocAtSystSt(i,ia),3)
          endif
        enddo
      endif
      return
      end
