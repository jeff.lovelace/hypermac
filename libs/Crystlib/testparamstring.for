      subroutine TestParamString(EdwStringIn,KeyWild,KeyAtMolMix,
     1                           ErrString)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) EdwStringIn,ErrString
      character*80  t80
      logical JeToAtomMolekula
      k=0
      ErrString=' '
1000  kp=k+1
      call kus(EdwStringIn,k,t80)
      call mala(t80)
      i=index(t80,'[')
      id=idel(t80)
      if(i.gt.0) then
        if(index(t80(:i-1),'*').le.0.and.index(t80(:i-1),'?').le.0) then
          if(ktera(t80(:i-1)).eq.0) then
            if(KteraSc(t80(:i-1)).eq.0) then
              id=i-1
              go to 9000
            endif
            JeToAtomMolekula=.false.
          else
            JeToAtomMolekula=.true.
          endif
        else
          JeToAtomMolekula=.true.
        endif
        j=index(t80,']')
        if(j.lt.0.or.i.ge.j-1.or.j.ne.id) go to 9100
        if(JeToAtomMolekula) then
          call TestAtomString(t80(i+1:j-1),KeyWild,IdAtMolNo,
     1                        IdMolYes,IdSymmNo,KeyAtMolMix,ErrString)
        else
          call TestScString(t80(i+1:j-1),ErrString)
        endif
        if(ErrString.ne.' ') go to 9999
      else
        if(index(t80,'*').le.0.and.index(t80,'?').le.0) then
          if(ktera(t80).gt.0) go to 9200
          if(kterasc(t80).le.0) go to 9000
        endif
      endif
5000  EdwStringIn(kp:kp+id-1)=t80
      if(k.lt.len(EdwStringIn)) go to 1000
      go to 9999
9000  ErrString='parameter "'//t80(:id)//'" doesn''t exist'
      go to 9999
9100  ErrString='the character "]" missing : "'//t80(1:idel(t80))//
     1          '"'
      go to 9999
9200  ErrString='atom/molecule name "'//t80(1:id)//'" missing'
9999  return
      end
