      subroutine MakeOccModPol(occ,x40,tzero,nt,dt,ax,ay,kmod,x4s,delta,
     1                         Gamma,Type)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer Type
      dimension occ(*),ax(*),ay(*),tzero(3),nt(3),dt(3),x4i(3),x4(3),
     1          nd(3),x40(3),Gamma(9),d4(3),FPolA(2*mxw+1)
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      do 1500i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        occ(i)=1.
        kk=0
        do k=1,kmod
          if(k.eq.1) then
            pom=x4(1)-x4s
            j=pom
            if(pom.lt.0.) j=j-1
            pom=pom-float(j)
            if(pom.gt..5) pom=pom-1.
            if(pom.lt.-delta.or.pom.gt.delta) then
              occ(i)=0.
              go to 1500
            endif
            pom=pom/delta
            call GetFPol(pom,FPolA,2*kmod+1,Type)
          endif
          kk=kk+2
          occ(i)=occ(i)+ax(k)*FPolA(kk)+ay(k)*FPolA(kk+1)
        enddo
1500  continue
      return
      end
