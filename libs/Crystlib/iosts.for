      subroutine iosts(ln,a,n,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension a(n)
      character*80 FormatOut
      integer Exponent10
      nc=n/6
      nd=n-6*nc
      nc=nc+1
      i2=0
      if(klic.eq.0) then
        read(ln,102,err=9000,end=9000)(a(i),i=1,n)
      else
        do ic=1,nc
          FormatOut='('
          k=2
          if(ic.ne.nc) then
            i1=i2+1
            i2=i2+6
            nn=6
          else
            if(nd.eq.0) cycle
            i1=i2+1
            i2=i2+nd
            nn=nd
          endif
          do i=i1,i2
            if(abs(a(i)).gt.0.) then
              j=max(6-Exponent10(a(i)),0)
              j=min(j,6)
            else
              j=6
            endif
            write(FormatOut(k:),'(''f9.'',i1,'','')') j
            k=k+5
          enddo
          FormatOut(k:)=')'
          write(ln,FormatOut)(a(i),i=i1,i2)
        enddo
      endif
      go to 9999
9000  call SetRealArrayTo(a,n,0.)
      ErrFlag=1
9999  return
102   format(6f9.6)
      end
