      subroutine ShelxGetPar(x,fvar)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension fvar(*)
      if(x.ge. 9.) x=x-10.
      if(x.le.-9.) x=x+10.
      if(x.ge. 10..or.x.le.-10.) then
        i=nint(x*.1)
        fct=x-float(i)*10.
        pom=fvar(iabs(i))
        if(x.lt.0.) then
          pom=1.-pom
          fct=-fct
        endif
        x=fct*pom
      endif
      return
      end
