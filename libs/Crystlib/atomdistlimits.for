      subroutine AtomDistLimits
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/AtomDist/ nRolMenuFirst,nEdwDMin,nEdwDMax,DMin,DMax,
     1                 AtNames(2)
      character*80  t80
      character*2   nty,AtNames
      integer WhatHappened
      logical EqIgCase
      external AtomDistLimitsReadCommand,AtomDistLimitsWriteCommand,
     1         FeVoid
      save /AtomDist/
      xqd=500.
      il=2
      call RepeatCommandsProlog(id,fln(:ifln)//'_distlim.tmp',xqd,il,
     1                          ilp,0)
      il=ilp+1
      xpom=5.
      dpom=80.
      spom=120.
      do i=1,4
        if(i.le.2) then
          write(t80,'(''%'',i1,a2,'' atom type'')') i,nty(i)
        else if(i.eq.3) then
          t80='D(min)'
        else if(i.eq.4) then
          t80='D(Max)'
        endif
        if(i.le.2) then
          call FeQuestRolMenuMake(id,xpom+dpom*.5,il,xpom,il+1,t80,'C',
     1                            dpom+EdwYd,EdwYd,0)
          if(i.eq.1) nRolMenuFirst=RolMenuLastMade
        else
          call FeQuestEdwMake(id,xpom+dpom*.5,il,xpom,il+1,t80,'C',dpom,
     1                        EdwYd,0)
        endif
        if(i.eq.3) then
          nEdwDMin=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
        else if(i.eq.4) then
          nEdwDMax=EdwLastMade
        endif
        xpom=xpom+spom
      enddo
1300  nRolMenu=nRolMenuFirst
      do i=1,2
        call FeQuestRolMenuOpen(nRolMenu,AtType(1,KPhase),
     1                          NAtFormula(KPhase),0)
        nRolMenu=nRolMenu+1
      enddo
      DMin=0.
      DMax=3.
1350  call FeQuestRealEdwOpen(nEdwDMin,DMin,.false.,.false.)
      call FeQuestRealEdwOpen(nEdwDMax,DMax,.false.,.false.)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  AtomDistLimitsReadCommand,AtomDistLimitsWriteCommand,FeVoid,
     2  FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        nRolMenu=nRolMenuFirst
        do i=1,2
          do j=1,NAtFormula(KPhase)
            if(EqIgCase(AtNames(i),AtType(j,KPhase))) then
              call FeQuestRolMenuOpen(nRolMenu,AtType(1,KPhase),
     1                                NAtFormula(KPhase),j)
              go to 2025
            endif
          enddo
2025      nRolMenu=nRolMenu+1
        enddo
        go to 1350
      endif
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      return
      end
