      subroutine SehrajBondVal
      include 'fepc.cmn'
      include 'basic.cmn'
      integer ipor(5000)
      real RoVal(5000),BVal(5000)
      character*2 Atom1(5000),Atom2(5000),Charge1(5000),Charge2(5000),
     1            Reference(5000)
      character*4 StPor(5000)
      character*80 Detail(5000)
      character*256 t256
      t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//'bvparm.cif'
      open(44,file=t256)
      t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//'bvparm_new.cif'
      open(45,file=t256)
      t256=HomeDir(:idel(HomeDir))//'bondval'//ObrLom//
     1     'Gagne-Hawthorne.txt'
      open(46,file=t256)
1100  read(44,FormA,end=2000) t256
      write(45,FormA) t256(:idel(t256))
      if(LocateSubstring(t256,'_valence_param_details',.false.,.true.)
     1   .le.0) go to 1100
      i=0
1200  read(44,FormA,end=1300) t256
      if(idel(t256).le.0.or.t256(1:1).eq.'#') go to 1200
      i=i+1
      k=0
      call kus(t256,k,Atom1(i))
      call kus(t256,k,Charge1(i))
      call kus(t256,k,Atom2(i))
      call kus(t256,k,Charge2(i))
      if(Charge2(i)(1:1).ne.'-') Charge2(i)=' '//Charge2(i)(1:1)
      call StToReal(t256,k,RoVal(i),1,.false.,ich)
      call StToReal(t256,k,BVal(i),1,.false.,ich)
      call kus(t256,k,Reference(i))
      Detail(i)=t256(k+1:)
      go to 1200
1300  read(46,FormA,end=1500) t256
      if(idel(t256).le.0.or.t256(1:1).eq.'#') go to 1300
      i=i+1
      Atom2(i)='O'
      Charge2(i)='-2'
      Detail(i)='?'
      Reference(i)='z'
      k=0
      call kus(t256,k,Cislo)
      do ii=1,idel(Cislo)
        j=index(Cifry,Cislo(ii:ii))
        if(j.gt.0) then
          Atom1(i)=Cislo(1:ii-1)
          if(Cislo(ii:ii).eq.'+') then
            Charge1(i)='1'
          else
            Charge1(i)=Cislo(ii:ii)
          endif
          call kus(t256,k,Cislo)
          call StToReal(t256,k,RoVal(i),1,.false.,ich)
          call StToReal(t256,k,BVal(i),1,.false.,ich)
          write(6,'('' Pridal: '',a2,1x,a2,3x,a2,1x,a2,f10.4,f9.3)')
     1      Atom1(i),Charge1(i),Atom2(i),Charge2(i),RoVal(i),BVal(i)
          go to 1300
        endif
      enddo
      i=i-1
      go to 1300
1500  n=i
      do i=1,n
        StPor(i)=Atom1(i)(1:idel(Atom1(i)))//Charge1(i)
!        write(6,'(1x,a)') StPor(i)(:idel(StPor(i)))
      enddo
      call SortText(n,StPor,ipor)
      write(45,FormA)
      do i=1,n
        j=ipor(i)
        write(45,'(a2,1x,a2,3x,a2,1x,a2,f10.4,f9.3,4x,a1,3x,a)')
     1    Atom1(j),Charge1(j),Atom2(j),Charge2(j),RoVal(j),BVal(j),
     2    Reference(j),Detail(j)(:idel(Detail(j)))
      enddo
2000  close(44)
      close(45)
      close(46)
      return
      end
