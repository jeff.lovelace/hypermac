      subroutine SetBasicKeysForAtom(n)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      Atom(n)=' '
      isf(n)=1
      itf(n)=1
      ifr(n)=0
      iswa(n)=1
      kswa(n)=1
      kmol(n)=0
      MagPar(n)=0
      lasmax(n)=0
      TypeModFun(n)=0
      AtSiteMult(n)=1
      KUsePolar(n)=0
      do j=1,MaxNSymm
        isa(j,n)=j
      enddo
      ai(n)=1.
      sai(n)=0.
      a0(n)=1.
      sa0(n)=0.
      xfr(n)=0.
      sxfr(n)=0.
      call SetIntArrayTo(KFA(1,n),7,0)
      call SetIntArrayTo(KModA(1,n),7,0)
      call SetIntArrayTo(KModAO(1,n),7,0)
      call SetRealArrayTo( x(1,n),3,0.)
      call SetRealArrayTo(sx(1,n),3,0.)
      call SetRealArrayTo( beta(1,n),6,0.)
      call SetRealArrayTo(sbeta(1,n),6,0.)
      if(NMolec.gt.0) call SetRealArrayTo(durdr(1,n),9,0.)
      call SetIntArrayTo(KiA(1,n),mxda,0)
      if(NDimI(KPhase).gt.0) then
         phf(n)=0.
        sphf(n)=0.
        OrthoX40(n)=.5
        OrthoDelta(n)=1.
        OrthoEps(n)=.95
        call qbyx(x(1,n),qcnt(1,n),1)
      endif
      DelkaKiAtomu(n)=0
      return
      end
