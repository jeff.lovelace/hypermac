      subroutine GetGammaN(isym,isw,ksw,GammaN)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension GammaN(*)
      is=iabs(isym)
      zn=isign(1,isym)
      do j=4,NDim(KPhase)
        do i=1,3
          GammaN(i+(j-4)*3)=zn*rm6(i+(j-1)*NDim(KPhase),is,isw,ksw)
        enddo
      enddo
      return
      end
