      subroutine ShiftKiAt(ia,itfn,ifrn,lasmaxn,kmodan,MagParN,Keep)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer CumOld(11),CumNew(11),kmodan(7)
      logical Keep
      dimension kip(:)
      allocatable kip
      equivalence (idp,CumNew(11))
      if(DelkaKiAtomu(ia).gt.0) then
        CumOld(1)=max(TRankCumul(itf(ia)),10)
        if(ifr(ia).ne.0) CumOld(1)=CumOld(1)+1
        if(lasmax(ia).ne.0) then
          CumOld(2)=CumOld(1)+3
          if(lasmax(ia).gt.1) CumOld(2)=CumOld(2)+1+(lasmax(ia)-1)**2
        else
          CumOld(2)=CumOld(1)
        endif
        do i=3,9
          j=i-2
          if(KModA(j,ia).gt.0) then
            CumOld(i)=CumOld(i-1)+2*TRank(j-1)*KModA(j,ia)
            if(i.eq.3) CumOld(i)=CumOld(i)+1
          else
            CumOld(i)=CumOld(i-1)
          endif
        enddo
        if(CumOld(9).gt.CumOld(1).and.lasmax(ia).eq.0) then
          CumOld(10)=CumOld(9)+1
        else
          CumOld(10)=CumOld(9)
        endif
        CumOld(11)=CumOld(10)
        if(MagPar(ia).gt.0) CumOld(11)=CumOld(11)+3+(MagPar(ia)-1)*6
      else
        call SetIntArrayTo(CumOld,11,0)
      endif
      CumNew(1)=max(TRankCumul(itfn),10)
      if(ifrn.ne.0) CumNew(1)=CumNew(1)+1
      if(lasmaxn.ne.0) then
        CumNew(2)=CumNew(1)+3
        if(lasmaxn.gt.1) CumNew(2)=CumNew(2)+1+(lasmaxn-1)**2
      else
        CumNew(2)=CumNew(1)
      endif
      do i=3,9
        j=i-2
        if(kmodan(j).gt.0) then
          CumNew(i)=CumNew(i-1)+2*TRank(j-1)*kmodan(j)
          if(i.eq.3) CumNew(i)=CumNew(i)+1
        else
          CumNew(i)=CumNew(i-1)
        endif
      enddo
      if(CumNew(9).gt.CumNew(1).and.lasmaxn.eq.0) then
        CumNew(10)=CumNew(9)+1
      else
        CumNew(10)=CumNew(9)
      endif
      CumNew(11)=CumNew(10)
      if(MagParN.gt.0) CumNew(11)=CumNew(11)+3+(MagParN-1)*6
      idd=idp-DelkaKiAtomu(ia)
      allocate(kip(DelkaKiAtomu(ia)))
      do i=1,DelkaKiAtomu(ia)
        kip(i)=KiA(i,ia)
      enddo
      ido=DelkaKiAtomu(ia)
      if(idd.eq.0) go to 3000
      do i=ia+1,NAtAll
        PrvniKiAtomu(i)=PrvniKiAtomu(i)+idd
      enddo
      DelkaKiAtomu(ia)=idp
      if(NAtInd.gt.0) then
        PosledniKiAtInd=PrvniKiAtomu(NAtInd)+DelkaKiAtomu(NAtInd)-1
      else
        PosledniKiAtInd=NdOff
      endif
      PrvniKiAtPos=PosledniKiAtInd+1
      if(NAtMol.gt.0) then
        PosledniKiAtPos=PrvniKiAtomu(NAtCalc)+DelkaKiAtomu(NAtCalc)-1
        PrvniKiAtMol=PosledniKiAtPos+1
        PosledniKiAtMol=PrvniKiAtomu(NAtAll)+
     1                  DelkaKiAtomu(NAtAll)-1
        PrvniKiMol=PosledniKiAtMol+1
        PrvniKiMolekuly(1)=PrvniKiMol
        do i=1,NMolec
          do j=1,mam(i)
            ji=j+(i-1)*mxp
            if(ji.eq.1) then
              PrvniKiMolekuly(ji)=PrvniKiMol
            else
              PrvniKiMolekuly(ji)=PosledniKiMol+1
            endif
            PosledniKIMol=PrvniKiMolekuly(ji)+DelkaKiMolekuly(ji)-1
          enddo
        enddo
      else
        PosledniKiAtPos=PosledniKiAtInd
        PrvniKiAtMol=PosledniKiAtInd+1
        PosledniKiAtMol=PosledniKiAtInd
        PrvniKiMol=PosledniKiAtInd+1
        PosledniKiMol=PosledniKiAtInd
      endif
      PrvniKiAtXYZMode=PosledniKiMol+1
      PosledniKiAtXYZMode=PosledniKiMol
      PrvniKiAtMagMode=PosledniKiAtXYZMode+1
      PosledniKiAtMagMode=PosledniKiAtXYZMode
3000  if(.not.Keep) then
        do i=1,11
          if(CumOld(i).ne.CumNew(i)) go to 3150
        enddo
        go to 9999
3150    jb=0
        do i=1,11
          if(i.eq.1) then
            jp=jb+1
            k=1
            jd=min(CumOld(i),CumNew(i))
          else
            jp=jb+CumNew(i-1)+1
            k=CumOld(i-1)+1
            jd=min(CumOld(i)-CumOld(i-1),CumNew(i)-CumNew(i-1))
          endif
          if(jd.gt.0) call CopyVekI(kip(k),KiA(jp,ia),jd)
        enddo
      else
        call SetIntArrayTo(KiA(1,ia),DelkaKiAtomu(ia),0)
        call CopyVekI(kip,KiA(1,ia),ido)
      endif
9999  if(allocated(kip)) deallocate(kip)
      return
      end
