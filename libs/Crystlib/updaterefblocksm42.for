      subroutine UpdateRefBlocksM42
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      j=0
      do i=1,NRefBlock
        if(NRef90(i).le.0) cycle
        j=j+1
        if(i.eq.j) cycle
        NEDZone(j)=NEDZone(i)
        EDIntSteps(j)=EDIntSteps(i)
        EDThreads(j)=EDThreads(i)
        EDTiltcorr(j)=EDTiltcorr(i)
        EDGeometryIEDT(j)=EDGeometryIEDT(i)
        OrMatEDZone(1:3,1:3,j)=OrMatEDZone(1:3,1:3,i)
        GMaxEDZone(j)=GMaxEDZone(i)
        SGMaxMEDZone(j)=SGMaxMEDZone(i)
        SGMaxREDZone(j)=SGMaxREDZone(i)
        CSGMaxREDZone(j)=CSGMaxREDZone(i)
        OmEDZone(j)=OmEDZone(i)
        do k=1,NEDZone(j)
          RFacEDZone(k,j)=RFacEDZone(k,i)
          HEDZone(1:3,k,j)=HEDZone(1:3,k,i)
          AlphaEDZone(k,j)=AlphaEDZone(k,i)
          BetaEDZone(k,j)=BetaEDZone(k,i)
          PrAngEDZone(k,j)=PrAngEDZone(k,i)
          ScEDZone(k,j)=ScEDZone(k,i)
          ThickEDZone(k,j)=ThickEDZone(k,i)
          XNormEDZone(k,j)=XNormEDZone(k,i)
          YNormEDZone(k,j)=YNormEDZone(k,i)
        enddo
      enddo
      return
      end
