      subroutine EditAtMolSplit
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EditAtMolSplitC/ nEdwAtMol,nCrwAtoms,nCrwMolecules,
     1                        AtMolString
      character*256 Veta,AtMolString
      character*35 :: Labels(2)= (/'S%elect atoms from the list    ',
     1                             'S%elect molecules from the list'/)
      character*8, allocatable :: MolMenu(:)
      integer WhatHappened
      logical Prazdny,CrwLogicQuest,FileDiff,EqIgCase,FeYesNo
      logical, allocatable :: BratSplit(:)
      external EditAtMolSplitReadCommand,
     1         EditAtMolSplitWriteCommand,FeVoid
      save /EditAtMolSplitC/
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_split.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NMolSplit
        Veta='molsplit'
        do j=1,MMolSplit(i)
          Veta=Veta(:idel(Veta))//' '//MolSplit(j,i)
        enddo
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      do i=1,NAtSplit
        Veta='atsplit'
        do j=1,MAtSplit(i)
          Veta=Veta(:idel(Veta))//' '//AtSplit(j,i)
        enddo
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      xqd=500.
      il=3
      call RepeatCommandsPrologWithoutEnable(id,
     1                       fln(:ifln)//'_split.tmp',xqd,il,ilp,0)
      il=ilp+1
      xpom=5.
      tpom=xpom+10.+CrwgXd
      Veta='Define split %atoms'
      ill=-10*il-2
      do i=1,2
        call FeQuestCrwMake(id,tpom,ill,xpom,ill,Veta,'L',CrwgXd,CrwgYd,
     1                      1,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        if(NMolec.le.0.and.i.eq.2) call FeQuestCrwDisable(CrwLastMade)
        if(i.eq.1) then
          nCrwAtoms=CrwLastMade
          Veta='Define split %molecules'
        else
          nCrwMolecules=CrwLastMade
        endif
        ill=ill-8
        il=il+1
      enddo
      tpom=tpom+FeTxLengthUnder(Veta)+15.
      dpom=FeTxLengthUnder(Labels(2))+20.
      call FeQuestButtonMake(id,tpom,-10*il+15,dpom,ButYd,Labels(1))
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtDefine=ButtonLastMade
      Veta='Atoms%/Molecules:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwAtMol=EdwLastMade
      nEdwRepeatCheck=EdwLastMade
1300  AtMolString=' '
      Prazdny=.true.
1350  call FeQuestStringEdwOpen(nEdwAtMol,AtMolString)
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  EditAtMolSplitReadCommand,EditAtMolSplitWriteCommand,FeVoid,
     2  FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        Prazdny=.false.
        go to 1350
      endif
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwAtoms.or.CheckNumber.eq.nCrwMolecules))
     2  then
        call FeQuestButtonLabelChange(nButtDefine,
     1                                Labels(CheckNumber-nCrwAtoms+1))
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDefine)
     1  then
        if(CrwLogicQuest(nCrwAtoms)) then
          n=NAtCalc
          if(allocated(BratSplit)) deallocate(BratSplit)
          allocate(BratSplit(n))
          BratSplit=.false.
          call SelAtoms('Select group of split atoms',Atom,BratSplit,
     1                  isf,n,ich)
          if(ich.eq.0) then
            AtMolString=' '
            do i=1,n
              if(BratSplit(i))
     1          AtMolString=AtMolString(:idel(AtMolString))//' '//
     2                      Atom(i)(:idel(Atom(i)))
            enddo
          endif
          if(allocated(BratSplit)) deallocate(BratSplit)
          if(ich.eq.0) go to 1350
        else
          n=0
          do i=1,NMolec
            n=n+mam(i)
          enddo
          if(allocated(MolMenu)) deallocate(MolMenu)
          allocate(MolMenu(n))
          n=0
          do i=1,NMolec
            do j=1,mam(i)
              write(Cislo,'(i5)') j
              call Zhusti(Cislo)
              n=n+1
              MolMenu(n)=MolName(i)(:idel(MolName(i)))//'#'//
     1                   Cislo(:idel(Cislo))
            enddo
          enddo
          if(allocated(BratSplit)) deallocate(BratSplit)
          allocate(BratSplit(n))
          BratSplit=.false.
          call SelMolecules('Select group of split molecules',MolMenu,
     1                      BratSplit,n,ich)
          if(ich.eq.0) then
            AtMolString=' '
            do i=1,n
              if(BratSplit(i))
     1          AtMolString=AtMolString(:idel(AtMolString))//' '//
     2                      MolMenu(i)(:idel(MolMenu(i)))
            enddo
          endif
          if(allocated(BratSplit)) deallocate(BratSplit)
          if(ich.eq.0) go to 1350
        endif
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_split.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      NAtSplit=0
      NMolSplit=0
2500  read(ln,FormA,end=3000) Veta
      k=0
      call kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'atsplit')) then
        if(NAtSplit+1.gt.NAtSplitMax) call ReallocateAtSplit(1,2)
        NAtSplit=NAtSplit+1
        MAtSplit(NAtSplit)=0
2600    if(k.lt.len(Veta)) then
          call ReallocateAtSplit(0,MAtSplit(NAtSplit)+1)
          MAtSplit(NAtSplit)=MAtSplit(NAtSplit)+1
          call kus(Veta,k,AtSplit(MAtSplit(NAtSplit),NAtSplit))
          IAtSplit(MAtSplit(NAtSplit),NAtSplit)=0
          go to 2600
        endif
        go to 2500
      else
        if(NMolSplit+1.gt.NMolSplitMax) call ReallocateMolSplit(1,2)
        NMolSplit=NMolSplit+1
        MMolSplit(NMolSplit)=0
2700    if(k.lt.len(Veta)) then
          call ReallocateMolSplit(0,MMolSplit(NMolSplit)+1)
          MMolSplit(NMolSplit)=MMolSplit(NMolSplit)+1
          call kus(Veta,k,MolSplit(MMolSplit(NMolSplit),NMolSplit))
          IMolSplit(MMolSplit(NMolSplit),NMolSplit)=0
          go to 2700
        endif
        go to 2500
      endif
3000  call CloseIfOpened(ln)
      call DeleteFile(fln(:ifln)//'_split.tmp')
      call iom40(1,0,fln(:ifln)//'.m40')
      if(FileDiff(PreviousM40,fln(:ifln)//'.m40')) then
        if(FeYesNo(-1.,-1.,'Do you want to rewrite m40?',
     1             1)) then
          call DeleteFile(PreviousM40)
          call iom40(0,0,fln(:ifln)//'.m40')
          go to 9999
        else
          call CopyFile(PreviousM40,fln(:ifln)//'.m40')
          call DeleteFile(PreviousM40)
        endif
      endif
9999  return
      end
