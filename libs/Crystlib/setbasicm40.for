      subroutine SetBasicM40(Only)
      use Atoms_mod
      use Basic_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical Only
      NAtInd=0
      NAtCalc=0
      NAtCalcBasic=0
      NAtAll=0
      NAtMol=0
      NMolec=0
      NSavedPoints=0
      n=3*MxPhases
      call SetIntArrayTo(NAtIndFr,n,1)
      call SetIntArrayTo(NAtIndTo,n,0)
      call SetIntArrayTo(NAtIndLen,n,0)
      call SetIntArrayTo(NAtIndFrAll,MxPhases,1)
      call SetIntArrayTo(NAtIndToAll,MxPhases,0)
      call SetIntArrayTo(NAtIndLenAll,MxPhases,0)
      call SetIntArrayTo(NAtPosFr,n,1)
      call SetIntArrayTo(NAtPosTo,n,0)
      call SetIntArrayTo(NAtPosLen,n,0)
      call SetIntArrayTo(NAtPosFrAll,MxPhases,1)
      call SetIntArrayTo(NAtPosToAll,MxPhases,0)
      call SetIntArrayTo(NAtPosLenAll,MxPhases,0)
      call SetIntArrayTo(NAtMolFr,n,1)
      call SetIntArrayTo(NAtMolTo,n,0)
      call SetIntArrayTo(NAtMolLen,n,0)
      call SetIntArrayTo(NAtMolFrAll,MxPhases,1)
      call SetIntArrayTo(NAtMolToAll,MxPhases,0)
      call SetIntArrayTo(NAtMolLenAll,MxPhases,0)
      call SetIntArrayTo(NMolecFr,n,1)
      call SetIntArrayTo(NMolecTo,n,0)
      call SetIntArrayTo(NMolecLen,n,0)
      call SetIntArrayTo(NMolecFrAll,MxPhases,1)
      call SetIntArrayTo(NMolecToAll,MxPhases,0)
      call SetIntArrayTo(NMolecLenAll,MxPhases,0)
      call SetLogicalArrayTo(NonModulated,MxPhases,.true.)
      nor=0
      NAtXYZMode=0
      NXYZAMode=0
      NAtMagMode=0
      NMagAMode=0
      NPolynom=0
      NPolar=0
      lasmaxm=0
      call SetRealArrayTo(sc ,MxSc*MxDatBlock,0.)
      call SetRealArrayTo(scs,MxSc*MxDatBlock,0.)
      do i=1,MxDatBlock
        sc(1,i)=1.
      enddo
      mxscutw=max(NTwin,6)
      call SetRealArrayTo(OverAllB ,MxDatBlock,0.)
      call SetRealArrayTo(OverAllBS,MxDatBlock,0.)
      call SetRealArrayTo(ec,12*MxDatBlock,0.)
      call SetRealArrayTo(ecs,12*MxDatBlock,0.)
      call SetRealArrayTo(ecMag,2*MxDatBlock,1.)
      call SetRealArrayTo(ecsMag,2*MxDatBlock,0.)
      do KPh=1,NPhase
        if(NDim(KPh).gt.3) then
          call SetIntArrayTo(kw(1,1,KPh),3*mxw,0)
          if(NDim(KPh).eq.4) then
            j=mxw
          else
            j=NDimI(KPh)
          endif
          do i=1,j
            kw(mod(i-1,NDimI(KPh))+1,i,KPh)=(i-1)/NDimI(KPh)+1
          enddo
        endif
      enddo
      call SetIntArrayTo(KiS,MxScAll*MxDatBlock,0)
      call SetIntArrayTo(lite,NPhase,0)
      call SetIntArrayTo(irot,NPhase,1)
      KTLSMax=0
      if(ExistPowder.and..not.Only) call SetBasicM41(0)
      return
      end
