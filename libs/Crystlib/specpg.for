      subroutine SpecPg(xp,rpg,isw,n)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(3),rpg(9,*),x(6),x1(6),x2(6),x3(6),x3r(6)
      do l=1,6
        if(l.le.3) then
          x(l)=xp(l)
        else
          x(l)=0.
        endif
      enddo
      n=0
      do jsym=1,NSymmN(KPhase)
        call multm(rm6(1,jsym,isw,KPhase),x,x1,NDim(KPhase),
     1             NDim(KPhase),1)
        do m=1,NDim(KPhase)
          x2(m)=x1(m)+s6(m,jsym,isw,KPhase)
        enddo
        do ivt=1,NLattVec(KPhase)
          do m=1,3
            x3(m)=x2(m)+vt6(m,ivt,isw,KPhase)
            x3(m)=x(m)-x3(m)
            apom=anint(x3(m))
            x3(m)=x3(m)-apom
          enddo
          call multm(MetTens(1,isw,KPhase),x3,x3r,3,3,1)
          if(sqrt(scalmul(x3,x3r)).gt..0001) cycle
          n=n+1
          call MatBlock3(rm6(1,jsym,isw,KPhase),rpg(1,n),NDim(KPhase))
        enddo
      enddo
      return
      end
