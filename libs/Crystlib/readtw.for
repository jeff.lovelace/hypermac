      subroutine ReadTw(RtwIn,RtwiIn,itwIn,CellIn,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension RtwIn(9,*),RtwiIn(9,*),CellIn(6),CellOut(6),FMat(3,3),
     1          FMatInv(3,3),FMTIn(3,3),FMTOut(3,3),PomMat(3,3),
     2          FMatTr(3,3),DirIn(3),DirOrt(3),DirP(3),Tr2Ort(9),
     3          Tr2OrtI(9)
      data DirIn/0.,0.,1./,IBase,IProp/2*1/
      character*2  nty
      character*3  StAngle(8)
      character*80 Veta
      integer RolMenuSelectedQuest
      logical FeYesNoHeader,CrwLogicQuest
      data StAngle/'  0',' 60',' 90','120','180','240','270','300'/
      call DRCell2MetTens(CellIn,FMTIn)
      call DRCell2OrtTr(CellIn,Tr2Ort)
      call matinv(Tr2Ort,Tr2OrtI,pom,3)
      if(NPhase.gt.1) then
        nfr=1
      else
        nfr=2
      endif
      xqd=400.
      ill=14
      if(NPhase.gt.1) ill=ill+1
      do n=nfr,itwIn
        id=NextQuestId()
        write(Veta,'(i2,a2,'' twinning matrix'')') n,nty(n)
        call FeQuestCreate(id,-1.,-1.,xqd,ill,Veta,1,LightGray,0,0)
        il=0
        if(NPhase.gt.1) then
          il=il+1
          Veta='Applied to p%hase:'
          tpom=5.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          dpom=100.
          call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                            EdwYd,1)
          nRolMenuPhase=RolMenuLastMade
          KPh=KPhaseTwin(n)
          call FeQuestRolMenuOpen(RolMenuLastMade,PhaseName,NPhase,KPh)
        else
          nRolMenuPhase=0
        endif
        dpom=80.
        do i=1,3
          tpom=20.
          il=il+1
          Veta=Indices(i)//'''='
          call FeQuestLblMake(id,tpom,il,Veta,'L','N')
          if(i.eq.1) dll=FeTxLength(Veta)+5.
          xpom=tpom+dll
          tpom=xpom+dpom+5.
          do j=1,3
            tpom=xpom+dpom+5.
            Veta='* '//Indices(j)
            if(j.ne.3) Veta=Veta(:idel(Veta))//'+'
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1.and.j.eq.1) then
              nEdwMatFirst=EdwLastMade
              dlp=FeTxLength(Veta)+5.
            endif
            xpom=tpom+dlp
          enddo
        enddo
        call CopyMat(RtwIn(1,n),FMat,3)
        il=il+1
        Veta='%Round to closest integers'
        dpom=FeTxLengthUnder(Veta)+10.
        tpom=xqd*.5-dpom-5.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        nButtRoundI=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='Roun%d to closest rationals'
        tpom=xqd*.5+5.
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        nButtRoundR=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='Matrix %calculator'
        dpom=FeTxLengthUnder(Veta)+10.
        tpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        nButtMatCalc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='^'
        tpom=(xqd-ButYd)*.5
        call FeQuestButtonMake(id,tpom,-10*il+2,ButYd,ButYd,Veta)
        nButtApply=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        tpom=5.
        Veta='Rotation a%xis'
        xpom=tpom+FeTxLength(Veta)+10.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwDirection=EdwLastMade
        call FeQuestRealAEdwOpen(EdwLastMade,DirIn,3,.false.,.false.)
        xpom=5.
        tpom=xpom+CrwgXd+10.
        do i=1,2
          if(i.eq.1) then
            Veta='in d%irect base'
          else
            Veta='in r%ecirocal base'
          endif
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        0,2)
          if(i.eq.2) nCrwRecip=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.IBase)
        enddo
        tpom=tpom+200.
        xpom=xpom+200.
        il=il-2
        do i=1,2
          if(i.eq.1) then
            Veta='%Proper rotation'
          else
            Veta='I%mp'//Veta(3:idel(Veta))
          endif
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        0,3)
          if(i.eq.1) nCrwProper=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.IProp)
        enddo
        il=il+1
        Veta='Rotation angle:'
        tpom=5.
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        ilp=il
        il=il+1
        xpom=5.
        tpom=xpom+CrwgXd+10.
        do i=1,9
          if(i.le.8) then
            Veta=StAngle(i)//' deg'
          else
            Veta='explicitely'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(mod(i,3).eq.0.and.i.ne.9) then
            il=ilp
            tpom=tpom+100.
            xpom=xpom+100.
          else if(i.eq.1) then
            nCrwDegsFirst=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          il=il+1
        enddo
        nCrwDegsLast=CrwLastMade
        il=il-1
        tpom=tpom+FeTxLength(Veta)
        Veta=' =>'
        xpom=tpom+FeTxLength(Veta)+10.
        dpom=60.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwFill=EdwLastMade
        il=il+1
1310    nEdw=nEdwMatFirst
        do i=1,3
          do j=1,3
            call FeQuestRealEdwOpen(nEdw,FMat(j,i),.false.,.true.)
            nEdw=nEdw+1
          enddo
        enddo
1320    if(CrwLogicQuest(nCrwDegsLast)) then
          call FeQuestRealEdwOpen(nEdwFill,0.,.true.,.false.)
        else
          call FeQuestEdwClose(nEdwFill)
        endif
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
          nEdw=nEdwMatFirst
          do i=1,3
            do j=1,3
              call FeQuestRealFromEdw(nEdw,FMat(j,i))
              nEdw=nEdw+1
            enddo
          enddo
          call matinv(FMat,FMatInv,pom,3)
          if(abs(pom).le..001) then
            call FeChybne(-1.,-1.,'The matrix is singular - try again.',
     1                    ' ',SeriousError)
            go to 1600
          else
            call TrMat(FMat,FMatTr,3,3)
            call multm(FMatTr,FMTIn,PomMat,3,3,3)
            call multm(PomMat,FMat,FMTOut,3,3,3)
            call DRMetTens2Cell(FMTOut,CellOut)
            TextInfo(1)='Original cell parameters:'
            TextInfoFlags(1)='CB'
            write(TextInfo(2),100) CellIn
            TextInfo(3)='Twin cell parameters:'
            TextInfoFlags(3)='CB'
            write(TextInfo(4),100) CellOut
            Ninfo=4
            if(.not.FeYesNoHeader(-1.,YBottomMessage,
     1                            'Do you want to accept the '//
     2                            'twinning matrix?',1)) go to 1600
            call CopyMat(FMat,RtwIn(1,n),3)
            call CopyMat(FMatInv,RtwiIn(1,n),3)
            QuestCheck(id)=0
            go to 1500
          endif
1600      call FeQuestButtonOff(ButtonOK-ButtonFr+1)
          go to 1500
        else if(CheckType.eq.EventCrw.and.
     1          CheckNumber.ge.nCrwDegsFirst.and.
     2          CheckNumber.le.nCrwDegsLast) then
          go to 1320
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtApply)
     1    then
          if(CrwLogicQuest(nCrwDegsLast)) then
            call FeQuestRealFromEdw(nEdwFill,Angle)
          else
            nCrw=nCrwDegsFirst
            do i=1,8
              if(CrwLogicQuest(nCrw)) then
                Cislo=StAngle(i)
                call Posun(Cislo,1)
                read(Cislo,'(f15.0)') Angle
                go to 1615
              endif
              nCrw=nCrw+1
            enddo
          endif
1615      call FeQuestRealAFromEdw(nEdwDirection,DirIn)
          if(CrwLogicQuest(nCrwRecip)) then
            call MultM(MetTensI(1,1,KPhase),DirIn,DirOrt,3,3,1)
            call CopyVek(DirOrt,DirP,3)
            IBase=2
          else
            call CopyVek(DirIn,DirP,3)
            IBase=1
          endif
          call MultM(Tr2Ort,DirP,DirOrt,3,3,1)
          call VecOrtNorm(DirOrt,3)
          pom=Angle*ToRad
          csf=cos(pom)
          snf=sin(pom)
          onemcsf=1.-csf
          if(.not.CrwLogicQuest(nCrwProper)) then
            csf=-csf
            snf=-snf
            onemcsf=-onemcsf
            IProp=2
          else
            IProp=1
          endif
          call SetRealArrayTo(FMat,9,0.)
          do i=1,3
            do j=i,3
              if(i.eq.j) then
                FMat(i,i)=FMat(i,i)+csf
                FMat(i,i)=FMat(i,i)+onemcsf*DirOrt(i)**2
              else
                pom=onemcsf*DirOrt(i)*DirOrt(j)
                FMat(i,j)=FMat(i,j)+pom
                FMat(j,i)=FMat(j,i)+pom
                k=6-i-j
                pom=-DirOrt(k)*snf
                if(k.eq.2) pom=-pom
                FMat(i,j)=FMat(i,j)+pom
                FMat(j,i)=FMat(j,i)-pom
              endif
           enddo
          enddo
          call MultM(Tr2OrtI,FMat,PomMat,3,3,3)
          call MultM(PomMat,Tr2Ort,FMat,3,3,3)
          go to 1310
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtRoundI.or.
     2           CheckNumber.eq.nButtRoundR)) then
          nEdw=nEdwMatFirst
          do i=1,3
            do j=1,3
              call FeQuestRealFromEdw(nEdw,pom)
              if(CheckNumber.eq.nButtRoundI) then
                pom=anint(pom)
              else
                diff=.1
                iden=0
                do k=1,5
                  l=nint(pom*float(k))
                  diffp=abs(float(l)/float(k)-pom)
                  if(diffp.lt.diff) then
                    diff=diffp
                    inum=l
                    iden=k
                  endif
                enddo
                if(iden.ne.0) pom=float(inum)/float(iden)
              endif
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.true.)
              nEdw=nEdw+1
            enddo
          enddo
          go to 1500
        else if(CheckType.eq.EventRolMenu.and.
     1          CheckNumber.eq.nRolMenuPhase) then
          KPhaseTwin(n)=RolMenuSelectedQuest(nRolMenuPhase)
          go to 1500
        else if(CheckNumber.eq.nButtMatCalc) then
          NRow=3
          NCol=3
          nEdw=nEdwMatFirst
          do i=1,3
            do j=1,3
              call FeQuestRealFromEdw(nEdw,FMat(j,i))
              nEdw=nEdw+1
            enddo
          enddo
          call TrMat(FMat,FMatTr,3,3)
          call MatrixCalculatorInOut(FMatTr,NRow,NCol,ichp)
          if(ichp.eq.0) then
            call TrMat(FMatTr,FMat,3,3)
            go to 1310
          else
            go to 1500
          endif
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9999
      enddo
9999  return
100   format(3f9.3,3f9.2)
      end
