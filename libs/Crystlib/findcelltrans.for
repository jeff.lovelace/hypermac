      subroutine FindCellTrans(Cell1In,Cell2In,TrCell,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Cell1In(*),Cell2In(*),TrCell(*),Cell1(6),Cell2(6),
     1          Tr(36),TrRed1(3,3),TrRed2(3,3),TrPom(3,3),CellPom(6),
     2          TrCell2(3,3),TrCell1Opt(3,3),TrCell2Opt(3,3)
      logical MatRealEqUnitMat
      real LatticeVolume
      ich=0
      Vol1=LatticeVolume(Cell1In)
      if(Vol1.lt.0.) go to 9000
      Vol2=LatticeVolume(Cell2In)
      if(Vol2.lt.0.) go to 9000
      if(Vol2.ge.Vol1) then
        Ratio=Vol2/Vol1
        Key=12
      else if(Vol2.lt.Vol1) then
        Ratio=Vol1/Vol2
        Key=21
      endif
      IVolRat=nint(Ratio)
      if(abs(float(IVolRat)-Ratio).gt..1) go to 9000
      Opt=999999999.
      do i2=1,2
        if(Key.eq.12) then
          call CopyVek(Cell2In,Cell2,6)
        else
          call CopyVek(Cell1In,Cell2,6)
        endif
1100    if(i2.eq.1) then
          call UnitMat(TrRed2,3)
        else
          call UnitMat(Tr,NDim(KPhase))
          call DRCellReduction(Cell2,Tr)
          call MatBlock3(Tr,TrRed2,NDim(KPhase))
          if(MatRealEqUnitMat(TrRed2,3,.001)) cycle
        endif
        do i1=1,2
          if(Key.eq.12) then
            call CopyVek(Cell1In,Cell1,6)
          else
            call CopyVek(Cell2In,Cell1,6)
          endif
1200      if(i1.eq.1) then
            call UnitMat(TrRed1,3)
          else
            call UnitMat(Tr,NDim(KPhase))
            call DRCellReduction(Cell1,Tr)
            call MatBlock3(Tr,TrRed1,NDim(KPhase))
            if(MatRealEqUnitMat(TrRed1,3,.001)) cycle
          endif
          call UnitMat(TrPom,3)
          if(IVolRat.eq.8) then
            jk=4
          else if(IVolRat.eq.6) then
            jk=3
          else if(IVolRat.eq.4.or.IVolRat.eq.9) then
            jk=2
          else
            jk=1
          endif
          do j=1,jk
            if(j.eq.1) then
              IPom1=IVolRat
              IPom2=1
              IPom3=1
            else if(j.eq.2) then
              if(IVolRat.eq.4) then
                IPom1=1
                IPom2=2
                IPom3=2
              else if(IVolRat.eq.6) then
                IPom1=1
                IPom2=2
                IPom3=3
              else if(IVolRat.eq.8) then
                IPom1=1
                IPom2=2
                IPom3=4
              else if(IVolRat.eq.9) then
                IPom1=1
                IPom2=3
                IPom3=3
              endif
            else if(j.eq.3) then
              if(IVolRat.eq.6) then
                 IPom1=1
                 IPom2=3
                 IPom3=2
               else if(IVolRat.eq.8) then
                 IPom1=1
                 IPom2=4
                 IPom3=2
               endif
            else if(j.eq.4) then
              if(IVolRat.eq.8) then
                 IPom1=2
                 IPom2=2
                 IPom3=2
              endif
            endif
            do k=1,3
              do l=1,3
                if(l.eq.k) then
                  TrPom(l,l)=IPom1
                else if(l.eq.mod(k,3)+1) then
                  TrPom(l,l)=IPom2
                else if(l.eq.mod(k+1,3)+1) then
                  TrPom(l,l)=IPom3
                endif
              enddo
              i21p=0
              i21k=nint(TrPom(2,2))-1
              i31p=0
              i31k=nint(TrPom(3,3))-1
              i32p=0
              i32k=nint(TrPom(3,3))-1
              do i21=i21p,i21k
                TrPom(2,1)=i21
                do i31=i31p,i31k
                  TrPom(3,1)=i31
                  do i32=i32p,i32k
                    TrPom(3,2)=i32
                    do jj=1,2
                      call CopyVek(Cell1,CellPom,6)
                      call MatFromBlock3(TrRed1,Tr,NDim(KPhase))
                      call DRMatTrCell(TrPom,CellPom,Tr)
                      if(jj.ne.1) then
                        call DRCellReduction(CellPom,Tr)
                      endif
                      pom=0.
                      do ii=1,3
                        dif=abs(Cell2(ii)-CellPom(ii))
                        pom=pom+dif
                      enddo
                      if(pom.gt.Opt) cycle
                      pomu=99999.
                      do iu=1,4
                        difu=0.
                        do ii=4,6
                          if(iu.eq.1) then
                            difu=difu+abs(Cell2(ii)-CellPom(ii))
                          else
                            if(ii.eq.iu+2) then
                              difu=difu+abs(Cell2(ii)-CellPom(ii))
                            else
                              difu=difu+abs(Cell2(ii)+CellPom(ii)-
     1                                      180.)
                            endif
                          endif
                        enddo
                        if(difu.lt.pomu) then
                          iuo=iu
                          pomu=difu
                        endif
                      enddo
                      pom=pom+pomu*.1
                      if(pom.lt.Opt) then
                        call MatBlock3(Tr,TrCell1Opt,NDim(KPhase))
                        call CopyMat(TrRed2,TrCell2Opt,3)
                        if(iuo.ne.1) then
                          kk=5-iuo
                          do ii=1,3
                            if(ii.ne.kk)
     1                        TrCell1Opt(1:3,ii)=-TrCell1Opt(1:3,ii)
                          enddo
                        endif
                        Opt=pom
                      endif
                    enddo
                  enddo
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo
      call CopyMat(TrCell2Opt,TrCell2,3)
      call MatInv(TrCell2,Tr,pom,3)
      call Multm(TrCell1Opt,Tr,TrPom,3,3,3)
      if(Key.eq.12) then
        call CopyMat(TrPom,TrCell,3)
      else
        call MatInv(TrPom,TrCell,pom,3)
      endif
      go to 9999
9000  ich=1
9999  close(44)
      return
      end
