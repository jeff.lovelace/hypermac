      subroutine iom41Old(klic,tisk,JinyFln)
      use Atoms_mod
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension Vek001(3)
      integer tisk
      character*(*) JinyFln
      character*256 t256
      character*80  Radka,t80
      logical eqrv,OldFile,poprve,EqIgCase
      character*8 nazev,IdP
      character*9 IdPP
      equivalence (t80,t256)
      data Vek001/0.,0.,1./
      if(klic.le.0) then
        call DeleteFile(fln(:ifln)//'_skipfrto.tmp')
        call SetBasicM41(0)
        ln=NextLogicNumber()
        do 1300i=1,20
          KPhase=0
          poprve=.true.
          rewind m41
1000      k=80
1010      if(k.eq.80) then
1015        read(m41,FormA,err=9000,end=9010) Radka
            t80=Radka
            if(Radka(1:1).eq.'*'.or.Radka(1:1).eq.'#'.or.
     1         idel(Radka).le.0.or.Radka(1:2).eq.'--') go to 1015
            if(EqIgCase(Radka,'end')) go to 1300
            call Vykric(Radka)
            if(Radka(1:1).eq.'!') Radka(1:1)=' '
            k=0
          endif
          call kus(Radka,k,Nazev)
          j=islovo(Nazev,IdM41,20)
          if(j.lt.0) then
            call FeChybne(-1.,-1.,'keyword on M41 file : '//nazev//
     1                    ' isn''t unique',' ',SeriousError)
            ErrFlag=1
            go to 9999
          else if(j.eq.IdM41Phase) then
            KPhase=KPhase+1
            poprve=.true.
            go to 1000
          else if(j.ne.i) then
            go to 1010
          else
            KPh=max(KPhase,1)
            call kus(Radka,k,Cislo)
            call Posun(Cislo,1)
            read(Cislo,104,err=9000) Value
            IValue=nint(Value)
            if(i.ne.IdM41SkipFrTo.and..not.poprve) go to 1200
            if(i.eq.IdM41KBackg) then
              KBackg(1)=IValue
            else if(i.eq.IdM41NBackg) then
              NBackg(1)=IValue
            else if(i.eq.IdM41KManBackg) then
              KManBackg(1)=IValue
            else if(i.eq.IdM41KWleBail) then
              KWleBail(1)=IValue
            else if(i.eq.IdM41KAbsor) then
              KAbsor(1)=max(IValue,1)
            else if(i.eq.IdM41Mir) then
              MirPwd(1)=Value
            else if(i.eq.IdM41SkipFrTo) then
              NSkipPwd(1)=NSkipPwd(1)+1
              if(NSkipPwd(1).ge.MxSkipPwd) then
                if(NSkipPwd(1).eq.MxSkipPwd)
     1            call FeChybne(-1.,-1.,'number of skip regions '//
     2                          'exceeds the limit.',' ',SeriousError)
                go to 1000
              endif
              SkipPwdFr(NSkipPwd(1),1)=Value
              call kus(Radka,k,Cislo)
              call posun(cislo,1)
              read(cislo,104,err=9000) SkipPwdTo(NSkipPwd(1),1)
              if(t80(1:1).eq.'!') then
                SkipPwdFr(NSkipPwd(1),1)=-SkipPwdFr(NSkipPwd(1),1)-200.
                SkipPwdTo(NSkipPwd(1),1)=-SkipPwdTo(NSkipPwd(1),1)-200.
              endif
              go to 1000
            else if(i.eq.IdM41KProfPwd) then
              KProfPwd(KPh,1)=IValue
            else if(i.eq.IdM41KAsym) then
              KAsym(KPh)=IValue
            else if(i.eq.IdM41KStrain) then
              KStrain(KPh,1)=IValue
            else if(i.eq.IdM41KPref) then
              KPref(KPh,1)=IValue
            else if(i.eq.IdM41SPref) then
              SPref(KPh,1)=IValue
            else if(i.eq.IdM41PCutOff) then
              PCutOff(KPh,1)=Value
            else if(i.eq.IdM41DirPref) then
              DirPref(1,KPh,1)=Value
              call StToReal(Radka,k,DirPref(2,KPh,1),2,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM41DirBroad) then
              DirBroad(1,KPh,1)=Value
              call StToReal(Radka,k,DirBroad(2,KPh,1),2,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM41MMax) then
              MMaxPwd(1,KPh,1)=IValue
              if(NDim(KPh).gt.4) then
                call StToInt(Radka,k,MMaxPwd(2,KPh,1),NDim(KPh)-4,
     1                       .false.,ich)
                if(ich.ne.0) go to 9000
              endif
            else if(i.eq.IdM41SatFrMod) then
              SatFrMod(KPh,1)=IValue.eq.1
            else if(i.eq.IdM41SkipFrdl) then
              SkipFriedel(KPh,1)=IValue.eq.1
            else if(i.eq.IdM41KRough) then
              KRough(1)=IValue
            endif
          endif
          poprve=.false.
          go to 1010
1200      call FeChybne(-1.,-1.,'duplicate occurrence of "'//
     1                  nazev(:idel(nazev))//'"',
     2                  'remember that the first occurrence will be '//
     3                  'accepted.',Warning)
1300    continue
        call CloseIfOpened(ln)
        do KPh=1,NPhase
          KPhase=KPh
          if(KAsym(KPh).eq.IdPwdAsymSimpson) then
            NAsym(KPh)=1
          else if(mod(KAsym(1),100).eq.IdPwdAsymBerar) then
            NAsym(KPh)=KAsym(1)/100
            if(NAsym(KPh).eq.0) NAsym(KPh)=4
            KAsym(KPh)=mod(KAsym(KPh),100)
          else if(KAsym(1).eq.IdPwdAsymDivergence) then
            NAsym(KPh)=2
          else
            NAsym(KPh)=0
          endif
          if(NAtIndLenAll(KPh).le.0) SatFrMod(KPh,1)=.false.
        enddo
      else
        write(Radka,101) KBackg(1),IdM41(IdM41NBackg),NBackg(1),
     1                   IdM41(IdM41KManBackg),KManBackg(1),
     2                   IdM41(IdM41KWleBail),KWleBail(1),
     2                   IdM41(IdM41KRough),KRough(1)
        k=0
        call WriteLabeledRecord(m41,IdM41(IdM41KBackg),Radka,k)
        if(KAbsor(1).gt.0) then
          write(Radka,101) KAbsor(1)
          idl=idel(Radka)
          write(Radka(idl+2:),112) IdM41(IdM41Mir),MirPwd(1)
          k=0
          call WriteLabeledRecord(m41,IdM41(IdM41KAbsor),Radka,k)
        endif
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_skipfrto.tmp','formatted',
     1                'unknown')
        NSkipPwd=0
1440    read(ln,FormA,end=1445,err=1445) Radka
        k=0
        call kus(Radka,k,Cislo)
        call mala(Cislo)
        j=index(Cislo,'skipfrto')
        if(j.gt.0) then
          NacetlVykricnik=Cislo(1:1).eq.'!'
          NSkipPwd=NSkipPwd+1
          call kus(Radka,k,Cislo)
          call Posun(Cislo,1)
          read(Cislo,104,err=1442) SkipPwdFr(NSkipPwd(1),1)
          call kus(Radka,k,Cislo)
          call Posun(Cislo,1)
          read(Cislo,104,err=1442) SkipPwdTo(NSkipPwd(1),1)
          if(NacetlVykricnik) then
            SkipPwdFr(NSkipPwd,1)=-SkipPwdFr(NSkipPwd,1)-200.
            SkipPwdTo(NSkipPwd,1)=-SkipPwdTo(NSkipPwd,1)-200.
          endif
          go to 1440
1442      NSkipPwd(1)=NSkipPwd(1)-1
        endif
        go to 1440
1445    call PwdOrderSkipRegions
        close(ln,status='delete')
        IdP=IdM41(IdM41SkipFrTo)
        idl=idel(IdP)
        do j=1,NSkipPwd(1)
          if(SkipPwdFr(j,1).gt.-180.) then
            write(Radka,102) SkipPwdFr(j,1),SkipPwdTo(j,1)
            IdPP=IdP
          else
            write(Radka,102) -SkipPwdFr(j,1)-200.,
     1                       -SkipPwdTo(j,1)-200.
            IdPP='!'//IdP(:idl)
          endif
          k=0
          call WriteLabeledRecord(m41,IdPP,Radka,k)
        enddo
        do KPh=1,NPhase
          KPhase=KPh
          if(NPhase.gt.1) then
            t80=IdM41(IdM41Phase)(:idel(IdM41(IdM41Phase)))//' '//
     1          PhaseName(KPh)(:idel(PhaseName(KPh)))
            write(m41,FormA) t80(:idel(t80))
          endif
          if(KAsym(KPh).eq.IdPwdAsymBerar) then
            i=KAsym(KPh)+100*NAsym(KPh)
          else
            i=KAsym(KPh)
          endif
          write(Radka,101) KProfPwd(KPh,1),IdM41(IdM41KAsym),i,
     1                     IdM41(IdM41KStrain),KStrain(KPh,1)
          idl=idel(Radka)
          write(Radka(idl+2:),110) IdM41(IdM41PCutOff),PCutOff(KPh,1)
          k=0
          call WriteLabeledRecord(m41,IdM41(IdM41KProfPwd),Radka,k)
          if(KPref(KPh,1).ne.IdPwdPrefNone) then
            write(Radka,101) KPref(KPh,1)
            idl=idel(Radka)+1
            write(Radka(idl+2:),111) IdM41(IdM41SPref),SPref(KPh,1)
            k=0
            call WriteLabeledRecord(m41,IdM41(IdM41KPref),Radka,k)
          endif
          if(.not.eqrv(DirPref(1,KPh,1),Vek001,3,.0001).and.
     1       KPref(KPh,1).ne.IdPwdPrefNone) then
            write(Radka,102)(DirPref(i,KPh,1),i=1,3)
            k=0
            call WriteLabeledRecord(m41,IdM41(IdM41DirPref),Radka,k)
          endif
          if(.not.eqrv(DirBroad(1,KPh,1),Vek001,3,.0001).and.
     1       (KStrain(KPh,1).eq.IdPwdStrainAxial.or.
     2        abs(LorentzPwd(2,KPh,1)).gt.0.)) then
            write(Radka,102)(DirBroad(i,KPh,1),i=1,3)
            k=0
            call WriteLabeledRecord(m41,IdM41(IdM41DirBroad),Radka,k)
          endif
          if(SkipFriedel(KPh,1)) then
            i=1
          else
            i=0
          endif
          write(Radka,103) i
          k=0
          call WriteLabeledRecord(m41,IdM41(IdM41SkipFrdl),Radka,k)
          if(NDim(KPh).gt.3) then
            if(SatFrMod(KPh,1).and.NAtIndLenAll(KPh).gt.0) then
              i=1
            else
              SatFrMod(KPh,1)=.false.
              i=0
            endif
            write(Radka,103) i
            k=0
            call WriteLabeledRecord(m41,IdM41(IdM41SatFrMod),Radka,k)
            if(.not.SatFrMod(KPh,1)) then
              write(Radka,103)(MMaxPwd(i,KPh,1),i=1,NDimI(KPh))
              k=0
              call WriteLabeledRecord(m41,IdM41(IdM41MMax),Radka,k)
            endif
          endif
        enddo
        write(m41,'(''end'')')
        write(m41,'(79(''*''))')
      endif
      if(Klic.eq.0) then
3000    read(m41,FormA) Radka
        call mala(Radka)
        if(Radka(1:4).eq.'****') go to 3000
        OldFile=index(Radka,'# cell').gt.0
      else
        OldFile=.false.
      endif
      if(OldFile) then
        call SkipComments(m41)
        if(ErrFlag.ne.0) go to 9000
        call iostfree(m41,CellPwd(1,1),kiPwd(ICellPwd),6,klic)
        call CopyVek(CellPwd(1,1),CellPar(1,1,1),6)
        if(NDim(KPhase).gt.3) then
          call SkipComments(m41)
          if(ErrFlag.ne.0) go to 9000
          j=IQuPwd
          do i=1,NDimI(1)
            call iostfree(m41,QuPwd(1,i,1),kiPwd(j),3,klic)
            call CopyVek(QuPwd(1,i,1),Qu(1,i,1,1),3)
            j=j+3
          enddo
        endif
      endif
      if(klic.le.0) then
        call SkipComments(m41)
        if(ErrFlag.ne.0) go to 9000
      else
        write(m41,'(''# Shift parameters - zero, sycos, sysin'')')
      endif
      call iost(m41,ShiftPwd,kipwd(IShiftPwd),3,klic,tisk)
      if(ErrFlag.ne.0) go to 9000
      if(NBackg(1).gt.0) then
        if(klic.le.0) then
          call SkipComments(m41)
          if(ErrFlag.ne.0) go to 9000
        else
          write(m41,'(''# Background parameters'')')
        endif
        call iost(m41,BackgPwd,kiPwd(IBackgPwd),NBackg(1),klic,tisk)
        if(ErrFlag.ne.0) go to 9000
      endif
      if(KRough(1).gt.0) then
        if(klic.le.0) then
          call SkipComments(m41)
          if(ErrFlag.ne.0) go to 9000
        else
          write(m41,'(''# Roughness parameters'')')
        endif
        call iost(m41,RoughPwd,kiPwd(IRoughPwd),2,klic,tisk)
        if(ErrFlag.ne.0) go to 9000
      endif
      IZdvih=0
      do KPh=1,NPhase
        KPhase=KPh
        if(NPhase.gt.1.and.Klic.gt.0) then
          t80='### '//IdM41(IdM41Phase)(:Idel(IdM41(IdM41Phase)))//' '//
     1        PhaseName(KPh)(:idel(PhaseName(KPh)))
          write(m41,FormA) t80(:idel(t80))
        endif
        if(OldFile) go to 3300
        if(klic.le.0) then
          call SkipComments(m41)
          if(ErrFlag.ne.0) go to 9000
        else
          write(m41,'(''# Cell parameters - a,b,c,alpha,beta,gamma'')')
        endif
        j=ICellPwd+IZdvih
        if(klic.eq.0) then
          call iostfree(m41,CellPwd(1,KPh),kiPwd(j),6,klic)
          if(ErrFlag.eq.3) then
            backspace m41
          else
            go to 3050
          endif
        endif
        call iost(m41,CellPwd(1,KPh),kiPwd(j),6,klic,tisk)
3050    call CopyVek(CellPwd(1,KPh),CellPar(1,1,KPh),6)
!        if(Klic.eq.0) then
!          call CopyVek(CellParSU(1,1,KPh),CellPwds(1,KPh),6)
!        else
          call CopyVek(CellPwds(1,KPh),CellParSU(1,1,KPh),6)
!        endif
        if(NDim(KPhase).gt.3) then
          if(klic.le.0) then
            call SkipComments(m41)
            if(ErrFlag.ne.0) go to 9000
          else
            write(m41,'(''# Modulation vector(s)'')')
          endif
          j=IQuPwd+IZdvih
          do i=1,NDimI(KPh)
            if(klic.eq.0) then
              call iostfree(m41,QuPwd(1,i,KPh),kiPwd(j),3,klic)
              if(ErrFlag.eq.3) then
                backspace m41
              else
                go to 3100
              endif
            endif
            call iost(m41,QuPwd(1,i,KPh),kiPwd(j),3,klic,tisk)
3100        call CopyVek(QuPwd(1,i,KPh),Qu(1,i,1,KPh),3)
            j=j+3
          enddo
        endif
3300    if(KProfPwd(KPh,1).eq.IdPwdProfGauss.or.
     1     KProfPwd(KPh,1).eq.IdPwdProfVoigt) then
          j=IGaussPwd+IZdvih
          if(klic.le.0) then
            call SkipComments(m41)
            if(ErrFlag.ne.0) go to 9000
          else
            write(m41,'(''# Gaussian parameters - U,V,W,P'')')
          endif
          call iost(m41,GaussPwd(1,KPh,1),kiPwd(j),4,klic,tisk)
          if(ErrFlag.ne.0) go to 9000
        endif
        if(KProfPwd(KPh,1).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPh,1).eq.IdPwdProfModLorentz.or.
     2     KProfPwd(KPh,1).eq.IdPwdProfVoigt) then
          j=ILorentzPwd+IZdvih
          if(klic.le.0) then
            call SkipComments(m41)
            if(ErrFlag.ne.0) go to 9000
          else
            Radka='# Lorentzian parameters - X,Xe/Xs,Y,Ye'
            if(KStrain(KPh,1).eq.IdPwdStrainTensor.and.
     1         KProfPwd(KPh,1).eq.IdPwdProfVoigt) Radka=Radka//',zeta'
            write(m41,FormA) Radka(:idel(Radka))
          endif
          if(KStrain(KPh,1).eq.IdPwdStrainTensor.and.
     1       KProfPwd(KPh,1).eq.IdPwdProfVoigt) then
            i=5
          else
            i=4
          endif
          call iost(m41,LorentzPwd(1,KPh,1),kiPwd(j),i,klic,tisk)
          if(ErrFlag.ne.0) go to 9000
        endif
        if(KStrain(KPh,1).eq.IdPwdStrainTensor) then
          j=IStPwd+IZdvih
          if(klic.le.0) then
            call SkipComments(m41)
            if(ErrFlag.ne.0) go to 9000
          else
            write(m41,'(''# Strain parameters'')')
          endif
          call iost(m41,StPwd(1,KPh,1),kiPwd(j),15,klic,tisk)
          if(ErrFlag.ne.0) go to 9000
        endif
        if(KAsym(KPh).ne.IdPwdAsymNone) then
          j=IAsymPwd+IZdvih
          if(klic.le.0) then
            call SkipComments(m41)
            if(ErrFlag.ne.0) go to 9000
          else
            write(m41,'(''# Asymmetry parameters'')')
          endif
          call iost(m41,AsymPwd,kiPwd(j),NAsym(KPh),klic,tisk)
          if(ErrFlag.ne.0) go to 9000
        endif
        if(KPref(KPh,1).ne.IdPwdPrefNone) then
          j=IPrefPwd+IZdvih
          if(klic.le.0) then
            call SkipComments(m41)
            if(ErrFlag.ne.0) go to 9000
          else
            write(m41,'(''# Preferred orientation'')')
          endif
          call iost(m41,PrefPwd(1,KPh,1),kiPwd(j),2,klic,tisk)
        endif
        IZdvih=IZdvih+NParCellProfPwd
      enddo
      go to 9999
9000  call FeReadError(m41)
      go to 9900
9010  t80='Unexpected end of file'
      call FeChybne(-1.,-1.,'wrong data on M41 file',t80,SeriousError)
      go to 9900
9900  ErrFlag=1
9999  return
100   format(f12.6,5(1x,a8,f12.6))
101   format(i5,5(1x,a8,i5))
102   format(3f8.3)
103   format(3i5)
104   format(f15.0)
110   format(5(a8,f12.6,1x))
111   format(5(a8,i5,1x))
112   format(a8,3f8.3)
      end
