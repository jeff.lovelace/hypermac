      subroutine SymmetryCodeGetFromCIF(CIFCode,SymmCode)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ish(6),sh(6),in(2)
      character*(*) CIFCode,SymmCode
      character*40 SymmCodeArr(20),t40
      character*80 t80
      equivalence (isymc,in(1)),(ishc,in(2))
      save SymmCodeArr,NSymmCode
      SymmCode=' '
      if(CIFCode.eq.'.'.or.CIFCode.eq.' ') go to 9999
      t80=CIFCode
      i=index(t80,'_')
      t80(i:i)=' '
      i=0
      call StToInt(t80,i,in,2,.false.,ich)
      if(ich.ne.0) go to 9999
      isym=mod(isymc-1,NSymm(KPhase))+1
      isymc=(isymc-1)/NSymm(KPhase)
      do i=NDim(KPhase),1,-1
        ish(i)=mod(ishc,10)-5
        sh(i)=ish(i)
        ishc=ishc/10
      enddo
      isymc=isymc+1
      call AddVek(sh, s6(1,isym ,1,KPhase),sh,3)
      call AddVek(sh,vt6(1,isymc,1,KPhase),sh,3)
      call scode(isym,isymc,sh,ish,1,t80,t40)
      call mala(t80)
      do i=1,NSymmCode
        if(t80.eq.SymmCodeArr(i)) go to 1500
      enddo
      if(NSymmCode.le.20) then
        NSymmCode=NSymmCode+1
        SymmCodeArr(NSymmCode)=t80
        i=NSymmCode
      else
        i=999
      endif
1500  if(i.gt.20) then
        SymmCode='^?^'
        go to 9999
      endif
      call RomanNumber(i,SymmCode)
      SymmCode='^'//SymmCode(:idel(SymmCode))//'^'
      go to 9999
      entry SymmetryCodeOutput(ln)
      write(ln,FormA) ' '
      do i=1,NSymmCode
        call RomanNumber(i,t40)
        t40='('//t40(:idel(t40))//')'
        write(ln,FormA) t40(:20)//
     1                  SymmCodeArr(i)(:idel(SymmCodeArr(i)))
      enddo
      entry SymmetryCodeReset
5000  NSymmCode=0
9999  return
      end
