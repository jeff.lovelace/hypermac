      subroutine CrlGetReflectionConditions
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmt(36),rmp(36),rmf(36),px(7),pxx(7),pp(6),isort(500),
     1          p1(6),p2(6)
      logical eqrv
      call SetRealArrayTo(px,6,0.)
      NExtRefCond=0
      do is=1,NSymm(KPhase)
        call trmat(rm6(1,is,1,KPhase),rmt,NDim(KPhase),NDim(KPhase))
        k=0
        kk=NDimQ(KPhase)+1
        do i=1,NDim(KPhase)
          do j=1,NDim(KPhase)
            k=k+1
            kk=kk-1
            if(i.ne.j) then
              rmp(k)=rmt(kk)
            else
              rmp(k)=rmt(kk)-1.
            endif
          enddo
        enddo
        call triangl(rmp,px,NDim(KPhase),NDim(KPhase),nx)
        call SetRealArrayTo(rmf,36,0.)
        call SetRealArrayTo(rmf,NDim(KPhase),-333.)
        do 1100i=1,NDim(KPhase)
          k=i
          do j=1,NDim(KPhase)
            if(abs(rmp(k)).gt..0001) then
              jj=NDim(KPhase)+1-j
              rmf(jj)=0.
              kk=jj+(jj-2)*NDim(KPhase)
              k=k+NDim(KPhase)
              do l=j+1,NDim(KPhase)
                rmf(kk)=-rmp(k)
                k =k +NDim(KPhase)
                kk=kk-NDim(KPhase)
              enddo
              go to 1100
            endif
            k=k+NDim(KPhase)
          enddo
          exit
1100    continue
        do i=1,NDim(KPhase)
          if(rmf(i).lt.-300.) then
            rmf(i)=0.
            k=i+(i-1)*NDim(KPhase)
            rmf(k)=1.
          endif
        enddo
        k=1
        do i=1,NDim(KPhase)
          call FractVecToNomDenForm(rmf(k),pom,NDim(KPhase),ich)
        enddo
        do 1500ivt=1,NLattVec(KPhase)
          call AddVek(s6(1,is,1,KPhase),vt6(1,ivt,1,KPhase),pp,
     1                NDim(KPhase))
          call Multm(pp,rmf,pxx,1,NDim(KPhase),NDim(KPhase))
          do izn=1,-1,-2
            if(izn.lt.0) then
              call RealVectorToOpposite(pxx,px,NDim(KPhase))
            else
              call CopyVek(pxx,px,NDim(KPhase))
            endif
            call od0do1(px,px,NDim(KPhase))
            pom=0.
            do i=1,NDim(KPhase)
              pom=pom+abs(px(i))
            enddo
            if(pom.lt..0001) go to 1500
            zn=0.
            do m=1,NDim(KPhase)
              if(px(m).gt..5001) px(m)=px(m)-1.
              if(abs(zn).lt..0001.and.abs(px(m)).gt..0001)
     1          zn=sign(1.,px(m))
              px(m)=px(m)*zn
            enddo
            call FractVecToNomDenForm(px,px(NDim(KPhase)+1),
     1                                NDim(KPhase),ich)
            do i=1,NExtRefCond
              if(eqrv(rmf,ExtRefGroup(1,i),NDimQ(KPhase),.0001).and.
     1           eqrv(px, ExtRefCond (1,i),NDim(KPhase) ,.0001)) then
                if(px(NDim(KPhase)+1).gt.ExtRefCond(NDim(KPhase)+1,i))
     1            then
                  nn=i
                  go to 1450
                else
                  if(j.eq.1) then
                    continue
                  else
                    go to 1500
                  endif
                endif
              endif
            enddo
          enddo
          if(NExtRefCond.ge.500) then
            NExtRefCond=0
            pause 333
            go to 9999
          endif
          NExtRefCond=NExtRefCond+1
          nn=NExtRefCond
          call CopyMat(rmf,ExtRefGroup(1,nn),NDim(KPhase))
1450      call CopyVek(px,ExtRefCond(1,nn),NDim(KPhase)+1)
1500    continue
      enddo
      call SetIntArrayTo(ExtRefFlag,NExtRefCond,0)
      do i=1,NExtRefCond
        if(ExtRefFlag(i).ne.0) cycle
        do l=1,NDim(KPhase)
          p1(l)=ExtRefCond(l,i)/ExtRefCond(NDim(KPhase)+1,i)
        enddo
        do j=i+1,NExtRefCond
          if(ExtRefFlag(j).ne.0.or.i.eq.j) cycle
          call Multm(ExtRefGroup(1,i),ExtRefGroup(1,j),rmt,NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          if(.not.eqrv(ExtRefGroup(1,j),rmt,NDimQ(KPhase),.0001)) cycle
          do l=1,NDim(KPhase)
            p2(l)=ExtRefCond(l,j)/ExtRefCond(NDim(KPhase)+1,j)
          enddo
          do k=1,2
            if(k.eq.1) then
              call CopyVek(p1,pxx,NDim(KPhase))
            else
              call AddVek(p1,p2,pxx,NDim(KPhase))
            endif
            call Multm(pxx,ExtRefGroup(1,j),px,1,NDim(KPhase),
     1                 NDim(KPhase))
            call od0do1(px,px,NDim(KPhase))
            zn=0.
            do m=1,NDim(KPhase)
              if(px(m).gt..5001) px(m)=px(m)-1.
              if(abs(zn).lt..0001.and.abs(px(m)).gt..0001)
     1          zn=sign(1.,px(m))
              px(m)=px(m)*zn
            enddo
            call FractVecToNomDenForm(px,px(NDim(KPhase)+1),
     1                                NDim(KPhase),ich)
            pom=px(NDim(KPhase)+1)/ExtRefCond(NDim(KPhase)+1,j)
            do l=1,NExtRefCond
              if(ExtRefFlag(l).ne.0.or.i.eq.l) cycle
              if(.not.eqrv(ExtRefGroup(1,l),rmt,NDimQ(KPhase),.0001))
     1          cycle
              if(eqrv(px,ExtRefCond(1,l),NDim(KPhase),.0001).and.
     1           abs(anint(pom)-pom).lt..0001.and.pom.gt..9999) then
                ExtRefFlag(l)=1
                cycle
              endif
            enddo
          enddo
        enddo
      enddo
      do i=1,NExtRefCond
        isort(i)=0
        ir=1
        do l=1,NDim(KPhase)
          k=l
          ip=0
          do m=1,NDim(KPhase)
            pom=ExtRefGroup(k,i)
            if(abs(pom).gt..0001) then
              if(l.eq.m) then
                ip=ip+(NDim(KPhase)-m+7)
              else
                ip=ip+(NDim(KPhase)-m+4)
                if(pom.gt.0.) ip=ip+1
              endif
            endif
            k=k+NDim(KPhase)
          enddo
          if(ip.gt.0) then
            isort(i)=isort(i)-ip*ir
            ir=ir*16
          endif
        enddo
      enddo
      call indexx(NExtRefCond,isort,ExtRefPor)
9999  return
      end
