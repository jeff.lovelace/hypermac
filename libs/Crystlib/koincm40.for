      function KoincM40(xp,dmez,dist,isw)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(*),y1(3),y2(3),y3(3),y3r(3)
      do 2000i=1,NAtCalc
        if(kswa(i).ne.KPhase) go to 2000
        do jsym=1,NSymm(KPhase)
          call multm(rm(1,jsym,isw,KPhase),x(1,i),y1,3,3,1)
          call AddVek(y1,s6(1,jsym,isw,KPhase),y2,3)
          do ivt=1,NLattVec(KPhase)
            do m=1,3
              y3(m)=y2(m)+vt6(m,ivt,isw,KPhase)
              y3(m)=xp(m)-y3(m)
              y3(m)=y3(m)-anint(y3(m))
            enddo
            call multm(MetTens(1,isw,KPhase),y3,y3r,3,3,1)
            dist=sqrt(scalmul(y3,y3r))
            if(dist.le.dmez) then
              KoincM40=i
              go to 9999
            endif
          enddo
        enddo
2000  continue
      dist=-1.
      KoincM40=0
9999  return
      end
