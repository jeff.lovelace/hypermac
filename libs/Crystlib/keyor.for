      function KeyOr(io)
      include 'fepc.cmn'
      logical eqiv
      dimension ios(3,9),io(3),iosp(3)
      data ios/1,0,0,0,1,0,0,0,1,1,-1,0,1,1,0,1,1,1,1,-1,1,1,1,-1,
     1         1,-1,-1/
      call CopyVekI(io,iosp,3)
      do i=1,3
        if(iosp(i).lt.0) then
          do j=1,3
            iosp(j)=-iosp(j)
          enddo
        else if(iosp(i).gt.0) then
          go to 1600
        endif
      enddo
1600  do KeyOr=1,9
        if(eqiv(io,ios(1,KeyOr),3)) go to 9999
      enddo
      KeyOr=0
9999  return
      end
