      subroutine GetPointGroupFromSpaceGroup(PointGroup,NPg,NPgPor)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) PointGroup
      character*80 Grp
      logical EqRV
      real rmp(3,3)
      ngp=NGrupa(KPhase)
      if(ngp.le.0) then
        NLattVec(KPhase)=1
        do i=1,NSymm(KPhase)
          s6(1:NDim(KPhase),i,1,KPhase)=0.
        enddo
        call FindSmbSg(Grp,.false.,1)
      endif
      if(NGrupa(KPhase).le.1) then
        NPg=1
      else if(NGrupa(KPhase).eq.2) then
        NPg=2
      else if(NGrupa(KPhase).ge.3.and.NGrupa(KPhase).le.5) then
        NPg=3
      else if(NGrupa(KPhase).ge.6.and.NGrupa(KPhase).le.9) then
        NPg=4
      else if(NGrupa(KPhase).ge.10.and.NGrupa(KPhase).le.15) then
        NPg=5
      else if(NGrupa(KPhase).ge.16.and.NGrupa(KPhase).le.24) then
        NPg=6
      else if(NGrupa(KPhase).ge.25.and.NGrupa(KPhase).le.46) then
        NPg=7
      else if(NGrupa(KPhase).ge.47.and.NGrupa(KPhase).le.74) then
        NPg=8
      else if(NGrupa(KPhase).ge.75.and.NGrupa(KPhase).le.80) then
        NPg=9
      else if(NGrupa(KPhase).ge.81.and.NGrupa(KPhase).le.82) then
        NPg=10
      else if(NGrupa(KPhase).ge.83.and.NGrupa(KPhase).le.88) then
        NPg=11
      else if(NGrupa(KPhase).ge.89.and.NGrupa(KPhase).le.98) then
        NPg=12
      else if(NGrupa(KPhase).ge.99.and.NGrupa(KPhase).le.110) then
        NPg=13
      else if(NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.122) then
        NPg=14
        if((NGrupa(KPhase).ge.111.and.NGrupa(KPhase).le.114).or.
     1     (NGrupa(KPhase).ge.121.and.NGrupa(KPhase).le.122)) then
          NPgPor=14
        else
          NPgPor=15
        endif
      else if(NGrupa(KPhase).ge.123.and.NGrupa(KPhase).le.142) then
        NPg=15
      else if(NGrupa(KPhase).ge.143.and.NGrupa(KPhase).le.146) then
c        CellPar(6,1,KPhase)=120.
        NPg=16
      else if(NGrupa(KPhase).ge.147.and.NGrupa(KPhase).le.148) then
        NPg=17
      else if(NGrupa(KPhase).ge.149.and.NGrupa(KPhase).le.155) then
        NPg=18
        if(NGrupa(KPhase).eq.155) then
          NPgPor=25
C          32
        else if(mod(NGrupa(KPhase)-148,2).eq.1) then
          NPgPor=19
C          312
        else
          NPgPor=22
C          321
        endif
      else if(NGrupa(KPhase).ge.156.and.NGrupa(KPhase).le.161) then
        NPg=19
        if(NGrupa(KPhase).ge.160) then
          NPgPor=26
C          3m
        else if(mod(NGrupa(KPhase)-155,2).eq.1) then
          NPgPor=23
C          3m1
        else
          NPgPor=20
C          31m
        endif
      else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.167) then
        NPg=20
        if(NGrupa(KPhase).ge.166) then
          NPgPor=27
C          -3m
        else if(NGrupa(KPhase).ge.162.and.NGrupa(KPhase).le.163) then
          NPgPor=21
C          -31m
        else if(NGrupa(KPhase).ge.164.and.NGrupa(KPhase).le.165) then
          NPgPor=24
C          -3m1
        endif
      else if(NGrupa(KPhase).ge.168.and.NGrupa(KPhase).le.173) then
        NPg=21
      else if(NGrupa(KPhase).ge.174.and.NGrupa(KPhase).le.174) then
        NPg=22
      else if(NGrupa(KPhase).ge.175.and.NGrupa(KPhase).le.176) then
        NPg=23
      else if(NGrupa(KPhase).ge.177.and.NGrupa(KPhase).le.182) then
        NPg=24
      else if(NGrupa(KPhase).ge.183.and.NGrupa(KPhase).le.186) then
        NPg=25
      else if(NGrupa(KPhase).ge.187.and.NGrupa(KPhase).le.190) then
        NPg=26
        if(NGrupa(KPhase).le.188) then
          NPgPor=33
        else
          NPgPor=34
        endif
      else if(NGrupa(KPhase).ge.191.and.NGrupa(KPhase).le.194) then
        NPg=27
      else if(NGrupa(KPhase).ge.195.and.NGrupa(KPhase).le.199) then
c        CellPar(6,1,KPhase)=120.
        NPg=28
      else if(NGrupa(KPhase).ge.200.and.NGrupa(KPhase).le.206) then
        NPg=29
      else if(NGrupa(KPhase).ge.207.and.NGrupa(KPhase).le.214) then
        NPg=30
      else if(NGrupa(KPhase).ge.215.and.NGrupa(KPhase).le.220) then
        NPg=31
      else if(NGrupa(KPhase).ge.221.and.NGrupa(KPhase).le.230) then
        NPg=32
      endif
      if(NPg.le.13) then
        NPgPor=NPg
      else if(NPg.ge.15.and.NPg.le.17) then
        NPgPor=NPg+1
      else if(NPg.ge.21.and.NPg.le.25) then
        NPgPor=NPg+7
      else if(NPg.ge.27) then
        NPgPor=NPg+8
      endif
      PointGroup=SmbPGI(NPgPor)
      if(NPgPor.eq.7) then
        rmp=0.
        do i=1,3
          do j=1,3
            if(j.eq.i) then
              rmp(j,j)=1.
            else
              rmp(j,j)=-1.
            endif
          enddo
          do j=1,NSymm(KPhase)
            if(EqRV(rmp,rm(1,j,1,KPhase),9,.0001)) then
              PointGroup='mmm'
              PointGroup(i:i)='2'
              go to 2000
            endif
          enddo
        enddo
      endif
2000  if(ngp.le.0) call iom50(0,0,fln(:ifln)//'.m50')
      return
      end
