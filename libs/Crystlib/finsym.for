      subroutine FinSym(tisk)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*2 nty
      integer tisk,CrlMagInver,CrlMagCenter
      dimension smp(36),rm6p(36),GammaIntInv(9),kwp(3)
      logical EqIV,EqIVM
      Centr(KPhase)=NLattVec(KPhase)
      do i=1,NComp(KPhase)
        if(i.eq.1) then
          InvMag(KPhase)=CrlMagInver()
          CenterMag(KPhase)=CrlMagCenter()
        else
          do j=1,NSymm(KPhase)
            call multm(zv(1,i,KPhase),rm6(1,j,1,KPhase),smp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,zvi(1,i,KPhase),rm6(1,j,i,KPhase),
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(zv(1,i,KPhase),s6(1,j,1,KPhase),s6(1,j,i,KPhase),
     1                 NDim(KPhase),NDim(KPhase),1)
          enddo
          do j=1,NLattVec(KPhase)
            call multm(zv(1,i,KPhase),vt6(1,j,1,KPhase),
     1                 vt6(1,j,i,KPhase),NDim(KPhase),NDim(KPhase),1)
          enddo
          do j=1,NSymmL(KPhase)
            call multm(zv(1,i,KPhase),rm6l(1,j,1,KPhase),smp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,zvi(1,i,KPhase),rm6l(1,j,i,KPhase),
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(zv(1,i,KPhase),s6l(1,j,1,KPhase),
     1                 s6l(1,j,i,KPhase),NDim(KPhase),NDim(KPhase),1)
          enddo
        endif
        ns=0
        do j=1,NSymm(KPhase)
          call MatBlock3(rm6(1,j,i,KPhase),rm(1,j,i,KPhase),
     1                   NDim(KPhase))
          call CodeSymm(rm6(1,j,i,KPhase),s6(1,j,i,KPhase),
     1                  symmc(1,j,i,KPhase),0)
          call Multm(ZVi(1,i,KPhase),rm6(1,j,i,KPhase),smp,
     1               NDim(KPhase),NDim(KPhase),NDim(KPhase))
          ISwSymm(j,i,KPhase)=0
          do 1250l=1,NComp(KPhase)
            call Multm(ZV(1,l,KPhase),smp,rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            do k=1,NDimQ(KPhase)
              if(abs(anint(rm6p(k))-rm6p(k)).gt..0001) go to 1250
            enddo
            if(i.eq.l) ns=ns+1
            do m=4,NDim(KPhase)
              do k=1,3
                if(abs(rm6p(k+(m-1)*NDim(KPhase))).gt..0001)
     1            go to 1250
              enddo
            enddo
            if(ISwSymm(j,i,KPhase).eq.0) ISwSymm(j,i,KPhase)=l
1250      continue
          if(ISwSymm(j,i,KPhase).eq.0) then
            write(TextInfo(1),'(i3,a2)') j,nty(j)
            call zhusti(TextInfo(1))
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//
     1                  ' symmetry matrix'
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//' for '
            write(TextInfo(1)(idel(TextInfo(1))+2:),'(i1,a2)') i,nty(i)
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//
     1                  ' composite part does not have a proper form.'
            TextInfo(2)='There is no composite subsystem where it is '//
     1                  'both integer and upper block-like.'
            call FeChybne(-1.,-1.,TextInfo(1),TextInfo(2),SeriousError)
            ErrFlag=1
            go to 9999
          endif
          zmag(j,i,KPhase)=zmag(j,1,KPhase)
          call CrlMakeRMag(j,i)
        enddo
        if(ns.eq.NSymm(KPhase)) then
          do j=1,NSymm(KPhase)
            call od0do1(s6(1,j,i,KPhase),s6(1,j,i,KPhase),
     1                  NDim(KPhase))
          enddo
          do j=1,NLattVec(KPhase)
            call od0do1(vt6(1,j,i,KPhase),vt6(1,j,i,KPhase),
     1                  NDim(KPhase))
          enddo
        endif
        ns=0
        do j=1,NSymmL(KPhase)
          call MatBlock3(rm6l(1,j,i,KPhase),rml(1,j,i,KPhase),
     1                   NDim(KPhase))
          call GetGammaIntInv(rm6l(1,j,i,KPhase),GammaIntInv)
          do k=1,NDimI(KPhase)**2
            GammaIntL(k,j,i,KPhase)=nint(GammaIntInv(k))
          enddo
          do 1300k=1,mxw
            call multmi(kw(1,k,KPhase),GammaIntL(1,j,i,KPhase),kwp,1,
     1                  NDimI(KPhase),NDimI(KPhase))
            do m=1,mxw
              if(EqIV(kw(1,m,KPhase),kwp,NDimI(KPhase))) then
                KwSymL(k,j,i,KPhase)= m
                go to 1300
              else if(EqIVM(kw(1,m,KPhase),kwp,NDimI(KPhase))) then
                KwSymL(k,j,i,KPhase)=-m
                go to 1300
              endif
            enddo
            call FeChybne(-1.,-1.,'some of transformed modulation '//
     1                    'waves not waves are not defined.',
     2                    'The local transformation cannot be '//
     3                    'performed.',SeriousError)
            ErrFlag=1
            go to 9999
1300      continue
          call srotb(rml(1,j,i,KPhase),rml(1,j,i,KPhase),
     1               rtl(1,j,i,KPhase))
          call srotc(rml(1,j,i,KPhase),3,rc3l(1,j,i,KPhase))
          call srotc(rml(1,j,i,KPhase),4,rc4l(1,j,i,KPhase))
          call srotc(rml(1,j,i,KPhase),5,rc5l(1,j,i,KPhase))
          call srotc(rml(1,j,i,KPhase),6,rc6l(1,j,i,KPhase))
          call Multm(ZVi(1,i,KPhase),rm6l(1,j,i,KPhase),smp,
     1               NDim(KPhase),NDim(KPhase),NDim(KPhase))
          ISwSymmL(j,i,KPhase)=i
c   Vyhozeno jako blbost. Lokalni operace by mela pusobit 
c   unvnitr jedineho system
c   a muze byt i necelociselna.
c          ISwSymmL(j,i,KPhase)=0
c          do 2250l=1,NComp(KPhase)
c            call Multm(ZV(1,l,KPhase),smp,rm6p,
c     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
c            do k=1,NDimQ(KPhase)
c              if(abs(anint(rm6p(k))-rm6p(k)).gt..0001) go to 2250
c            enddo
c            if(i.eq.l) ns=ns+1
c            do m=4,NDim(KPhase)
c              do k=1,3
c                if(abs(rm6p(k+(m-1)*NDim(KPhase))).gt..0001)
c     1            go to 2250
c              enddo
c            enddo
c            if(ISwSymmL(j,i,KPhase).eq.0) ISwSymmL(j,i,KPhase)=l
c2250      continue
c          if(ISwSymmL(j,i,KPhase).eq.0) then
c            write(TextInfo(1),'(i3,a2)') j,nty(j)
c            call zhusti(TextInfo(1))
c            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//
c     1                  ' local symmetry matrix'
c            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//' for '
c            write(TextInfo(1)(idel(TextInfo(1))+2:),'(i1,a2)') i,nty(i)
c            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//
c     1                  ' composite part does not have a proper form.'
c            TextInfo(2)='There is no composite subsystem where it is '//
c     1                  'both integer and upper block-like.'
c            call FeChybne(-1.,-1.,TextInfo(1),TextInfo(2),SeriousError)
c            ErrFlag=1
c            go to 9999
c          endif
        enddo
c        if(ns.eq.NSymmL(KPhase)) then
!        do j=1,NSymmL(KPhase)
!          call od0do1(s6l(1,j,i,KPhase),s6l(1,j,i,KPhase),
!     1                NDim(KPhase))
!        enddo
c        endif
      enddo
9999  return
      end
