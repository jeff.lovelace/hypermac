      subroutine ContinueWithNewStructure(NewFln)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) NewFln
      character*256 t256
      integer FeChdir
      logical FeYesNo
      if(FeYesNo(-1.,-1.,'Do you want to continue with the new '//
     1           'structure?',0)) then
        call DeletePomFiles
        call ExtractDirectory(NewFln,t256)
        i=FeChdir(t256)
        call ExtractFileName(NewFln,fln)
        ifln=idel(fln)
        call FeGetCurrentDir
        call FeBottomInfo(' ')
        call DeletePomFiles
        call ChangeUSDFile(fln,'opened','*')
      else
        call ChangeUSDFile(NewFln,'closed','*')
      endif
      return
      end
