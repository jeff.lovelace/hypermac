      subroutine CrlCommenInd2SSGInd(ihc,ihs,itwi,KPh)

!      Pozor nevyzkoušeno !!!!

      include 'fepc.cmn'
      include 'basic.cmn'
      integer ihc(*),ihs(*),ih(6)
      real hp(3),difi(3)
      if(KCommen(KPh).gt.0) then
        call CrlRestoreSymmetry(ISymmSSG(KPh))
        do i=1,3
          do j=1,NDimI(KPh)
            hp(i)=float(ihc(i))+qu(i,j,1,KPh)*float(ihc(j+3))
          enddo
        enddo
        call IndFromIndReal(hp,NCommQProduct(1,KPh),difi,ih,itw,
     1                      iswp,-1.,CheckExtRefYes)
        call CrlRestoreSymmetry(ISymmCommen(KPh))
        if(itw.eq.itwi.and.iswp.gt.0) then
          ihs(1:NDim(KPhase))=ih(1:NDim(KPhase))
        else
          ihs(1:NDim(KPhase))=ihc(1:NDim(KPhase))
        endif
      endif
      return
      end
