      logical function PointAlreadyPresent(xn,xo,xa,n,dmez,BratPrvni,
     1                                     dist,jsymo,jcento,isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xn(*),xa(3,*),xo(*),y1(6),y3(6),dd(3),ddr(3),do(3)
      logical BratPrvni
      PointAlreadyPresent=.false.
      dmeza=dmez
      jsymo=0
      jcento=0
      do jsym=1,NSymm(KPhase)
        call multm(rm6(1,jsym,isw,KPhase),xn,y1,NDim(KPhase),
     1             NDim(KPhase),1)
        call AddVek(y1,s6(1,jsym,isw,KPhase),y1,NDim(KPhase))
        do jcenter=1,NLattVec(KPhase)
          do m=1,NDim(KPhase)
            y3(m)=y1(m)+vt6(m,jcenter,isw,KPhase)
          enddo
          do i=1,n
            do m=1,3
              dd(m)=y3(m)-xa(m,i)
              do(m)=-anint(dd(m))
              dd(m)=dd(m)+do(m)
            enddo
            call multm(MetTens(1,isw,KPhase),dd,ddr,3,3,1)
            distp=sqrt(scalmul(dd,ddr))
            if(distp.lt.dmeza) then
              call CopyVek(y3,xo,NDim(KPhase))
              call AddVek(xo,do,xo,3)
              PointAlreadyPresent=.true.
              dist=distp
              dmeza=distp
              jsymo=jsym
              jcento=jcenter
              if(jsym.eq.1.and.BratPrvni) go to 9999
            endif
          enddo
        enddo
      enddo
9999  return
      end
