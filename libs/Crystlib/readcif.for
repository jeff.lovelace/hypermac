      subroutine ReadCIF(Klic,CoDal)
      use Basic_mod
      use Powder_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      real qup(3,mxw),rmp(36),gpp(36),CellParPom(6),xp(6),xq(6),sp1(6),
     1     sp2(6)
      real, allocatable :: pa(:),spa(:),aip(:),saip(:),AtMultP(:),
     1                     OrX40P(:),OrDelP(:),OrEpsP(:),rm6o(:,:),
     2                     s6o(:,:),vt6o(:,:)
      integer imn(3),imx(3),il(10),mrt(3),kmodn(7),iwa(20),kwp(3,mxw),
     1        IOldKey(30),JOldKey(30),i90(3), FullMultiplicity,CoDal,
     2        DataPosition(50),UseTabsIn,CrSystemFromCell,ProHonzu,
     3        kwi(mxw),mrto(3),NMenuJ(5),IMenuJ(5)
      integer, allocatable :: ia(:),EqSymm(:)
      character*(*) FileCIFIn
      character*256 FileIn,t256,p256,s256,e256
      character*80  OldCifKey(30)
      character*80, allocatable :: StLbl(:),sta(:),stap(:),IdCode(:)
      character*30  SymmCP(6)
      character*16  DataLabel(50)
      character*5   MenuJ(10,5)
      character*3   smbu(6),t3,StPh
      character*2   nty
      character*1   Znak
      logical eqiv,eqrv,CellMissing,QMissing,EqIgCase,FeYesNo,EqRV0,
     1        FeYesNoHeader,StructureExists,ExistFile,lpom,Psat,
     2        MatRealEqUnitMat,ReadMagneticCIF,EqRVM,ZShelx,DelejOrtho,
     3        eqivm
      equivalence (qup,gpp),(ProHonzu,IdNumbers(2))
      data smbu/'u11','u22','u33','u12','u13','u23'/
      data (OldCifKey(i),i=1,29)
     1              /'_symmetry_ssg_name_IT_number',
     2               '_symmetry_ssg_name',
     3               '_symmetry_ssg_name_IT',
     4               '_symmetry_ssg_name_WJJ',
     5               '_symmetry_ssg_WJJ_code',
     6               '_symmetry_ssg_equiv_pos_as_xyz',
     7               '_symmetry_ssg_equiv_pos_seq_id',
     8               '_atom_site_occ_fourier_cos',
     9               '_atom_site_occ_fourier_id',
     a               '_atom_site_occ_fourier_modulus',
     1               '_atom_site_occ_fourier_phase',
     2               '_atom_site_occ_fourier_sin',
     3               '_atom_site_displace_fourier_cos',
     4               '_atom_site_displace_fourier_id',
     5               '_atom_site_displace_fourier_modulus',
     6               '_atom_site_displace_fourier_phase',
     7               '_atom_site_displace_fourier_sin',
     8               '_atom_site_rot_fourier_cos',
     9               '_atom_site_rot_fourier_id',
     a               '_atom_site_rot_fourier_modulus',
     1               '_atom_site_rot_fourier_phase',
     2               '_atom_site_rot_fourier_sin',
     3               '_atom_site_u_fourier_cos',
     4               '_atom_site_u_fourier_id',
     5               '_atom_site_u_fourier_modulus',
     6               '_atom_site_u_fourier_phase',
     7               '_atom_site_u_fourier_sin',
     8               '_chemical_formula',
     9               '_space_group_symop.magn_id'/
      data (IOldKey(i),JOldKey(i),i=1,29)/
     1                                     1, 21,
     2                                     2, 21,
     3                                     3, 21,
     4                                     4, 21,
     5                                     5, 21,
     6                                     6, 21,
     7                                     7, 21,
     8                                    71,  1,
     9                                    72,  1,
     a                                    73,  1,
     1                                    74,  1,
     2                                    75,  1,
     3                                    58,  1,
     4                                    59,  1,
     5                                    60,  1,
     6                                    61,  1,
     7                                    62,  1,
     8                                    83,  1,
     9                                    84,  1,
     a                                    85,  1,
     1                                    86,  1,
     2                                    87,  1,
     3                                    92,  1,
     4                                    93,  1,
     5                                    94,  1,
     6                                    95,  1,
     7                                    96,  1,
     8                                    22,  6,
     9                                    39, 21/
      data NOldCifKeys/29/
      DelejOrtho=.false.
      OrthoOrd=0
      CoDal=0
      lni=0
      KlicUsed=Klic
      if(StructureExists(fln).and.KlicUsed.ne.5) then
        if(.not.FeYesNo(-1.,-1.,'The structure "'//fln(:ifln)//
     1                  '" already exists, rewrite it?',0)) go to 9999
        if(ExistM40)
     1    call MoveFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
        if(ExistM50)
     1    call MoveFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      endif
      FileIn=fln(:ifln)//'.cif'
      if(KlicUsed.ne.ProHonzu) then
1120    call FeFileManager('Select input CIF file',FileIn,
     1                     '*.cif *.mcif',0,.true.,ich)
        if(ich.ne.0) go to 9900
        if(.not.ExistFile(FileIn)) then
          call FeChybne(-1.,YBottomMessage,'the file "'//
     1                  FileIn(:idel(FileIn))//
     1                  '" does not exist, try again.',' ',SeriousError)
          go to 1120
        endif
      endif
      go to 1150
      entry ReadSpecifiedCIF(FileCIFIn,Klic)
      DelejOrtho=.false.
      OrthoOrd=0
      lni=0
      FileIn=FileCIFIn
      KlicUsed=Klic
1150  UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call CheckEOLOnFile(FileIn,2)
      if(ErrFlag.ne.0) go to 9999
      if(KPhase.le.1) then
        StPh=' '
      else
        write(StPh,'(''_'',i2)') KPhase
        call Zhusti(STPh)
      endif
      lni=NextLogicNumber()
      call OpenFile(lni,FileIn,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      allocate(CifKey(400,40),CifKeyFlag(400,40))
      if(KlicUsed.ne.ProHonzu.and..not.ICDDBatch) then
        nmax=0
        nl=0
        n=0
        p256=' '
1200    read(lni,FormA,end=1300) t256
        call SkrtniPrvniMezery(t256)
        nl=nl+1
        if(EqIgCase(t256(1:5),'data_')) then
          n=nl
          p256=t256(6:)
          if(p256.eq.' ') then
            write(p256,'(''Data#'',i3)') nmax+1
            call Zhusti(p256)
          endif
          go to 1200
        endif
        if(LocateSubstring(t256,'_cell_length',.false.,.true.).eq.1
     1                     .and.p256.ne.' ') then
          if(nmax.lt.50) then
            nmax=nmax+1
            DataLabel(nmax)=p256
            DataPosition(nmax)=n
            p256=' '
          endif
        endif
        go to 1200
1300    rewind lni
        if(nmax.ge.1) then
          i=1
          if(nmax.gt.1) then
            call SelOneAtom('Select CIF part to be imported',DataLabel,
     1                      i,nmax,ich)
            if(ich.ne.0) go to 9999
          endif
          i=DataPosition(i)
          nl=0
1320      read(lni,FormA,end=1330) t256
          nl=nl+1
          if(nl.eq.i) then
            go to 1330
          endif
          call SkrtniPrvniMezery(t256)
          if(EqIgCase(t256(1:5),'data_')) then
            if(nl.eq.i) go to 1330
          endif
          go to 1320
        endif
      endif
1330  call NactiCifKeys(CifKey,CifKeyFlag,0)
      if(ErrFlag.ne.0) go to 9999
      if(KPhase.le.1) then
        call SetBasicM40(.true.)
        call SetBasicM50
        isPowder=.false.
        ExistPowder=.false.
      endif
      if(allocated(CIFArray)) deallocate(CIFArray)
      nCIFArray=1000
      allocate(CIFArray(nCIFArray))
      m=0
      Psat=.true.
      ReadMagneticCIF=.false.
1400  read(lni,FormA,end=1500) t256
      s256=t256
      call SkrtniPrvniMezery(s256)
      if(LocateSubstring(s256,'data_',.false.,.true.).eq.1) then
        if(ICDDBatch) then
          Psat=LocateSubstring(s256,'data_AUDIT',.false.,.true.)
     1         .ne.1.and.
     2         LocateSubstring(s256,'data_REFERENCE',.false.,.true.)
     3         .ne.1.and.
     4         LocateSubstring(s256,'data_AtomicCoordinates_REFERENCE',
     5                         .false.,.true.)
     6         .ne.1.and.
     7         LocateSubstring(s256,'data_DILIST_REFERENCE',.false.,
     8                         .true.)
     9         .ne.1.and.
     a         LocateSubstring(s256,'data_POWDERPATTERN_REFERENCE',
     1                         .false.,.true.)
     2         .ne.1
        else
          n=0
          do i=1,idel(t256)
            if(t256(i:i).eq.'_') then
              n=n+1
              if(n.gt.1) go to 1400
            endif
          enddo
          go to 1500
        endif
      endif
      if(.not.Psat) go to 1400
      if(LocateSubstring(s256,'###non-st#',.false.,.true.).eq.1) then
        i=11
        t256=s256(i:)
        s256=s256(i:)
      endif
      if(.not.ReadMagneticCIF.and.
     1  (LocateSubstring(s256,'_magn_',.false.,.true.).gt.1.or.
     2   LocateSubstring(s256,'.magn_',.false.,.true.).gt.1))
     3  ReadMagneticCIF=.true.
      if(LocateSubstring(s256,'_cell_length',.false.,.true.).eq.1.and.
     1   index(s256,'?').gt.0) go to 1400
      if(LocateSubstring(s256,'_cell_angle',.false.,.true.).eq.1..and.
     1   index(s256,'?').gt.0) go to 1400
      p256=t256
      k=0
      call Kus(t256,k,s256)
      do n=1,NOldCifKeys
        it=idel(OldCifKey(n))
        if(EqIgCase(s256,OldCifKey(n)(:it))) then
          i=LocateSubstring(t256,OldCifKey(n)(:it),.false.,.true.)
          if(i.gt.0) then
            j=i+it
            s256=CifKey(IOldKey(n),JOldKey(n))
            p256=' '
            p256(i:)=s256(:idel(s256))//t256(j:)
          endif
          exit
        endif
      enddo
      if(KPhase.le.1) then
        k=0
        call kus(p256,k,s256)
        if(EqIgCase(s256,CifKey(27,20))) then
          call kus(p256,k,Cislo)
          if(Cislo(1:1).ne.'?') then
            isPowder=.true.
            if(KPhase.le.1) call SetBasicM41(KDatBlock)
          endif
        endif
      endif
      m=m+1
      if(m.gt.nCIFArray) call ReallocateCIFArray(nCIFArray+1000)
      j=index(p256,'_')
      if(j.gt.0) then
        i=index(p256,'#')
        if(i.gt.0) p256(i:)=' '
      endif
      CIFArray(m)=p256
      go to 1400
1500  nCIFUsed=m
      do j=1,40
        do i=1,400
          call mala(CifKey(i,j))
        enddo
      enddo
      call CIFGetMxLoop(MxLoop)
      MxLoop=max(MxLoop+100,1000)
      if(ReadMagneticCIF) then
        MagneticType(KPhase)=1
        MaxMagneticType=1
      endif
      if(allocated(pa)) deallocate(pa,spa,ia,StLbl,sta,stap)
      allocate(pa(MxLoop),spa(MxLoop),ia(MxLoop),StLbl(MxLoop),
     1         sta(MxLoop),stap(MxLoop))
      if(KlicUsed.eq.3) then
        write(out,'(''Spelling of used CIF keywords'')')
        write(out,'(''============================='')')
        rewind(lni)
1600    read(lni,FormA,end=1620) t256
        call SkrtniPrvniMezery(t256)
        call mala(t256)
        k=0
        call kus(t256,k,p256)
        if(p256(1:1).ne.'_') then
          go to 1600
        else
          do j=1,40
            do i=1,400
              k=idel(CifKey(i,j))
              if(k.le.0) cycle
              if(index(p256,CifKey(i,j)(:k)).gt.0) then
                CifKeyFlag(i,j)=0
                go to 1600
              endif
            enddo
          enddo
          call FeChybne(-1.,-1.,'unknown keyword:',p256,SeriousError)
          ErrFlag=1
          go to 1600
        endif
1620    call CloseIfOpened(lni)
        if(ErrFlag.ne.0) go to 9999
        write(out,'(''All used keywords OK'')')
        write(out,FormA)
        write(out,'(''Presence of crutial CIF keywords'')')
        write(out,'(''================================'')')
        do j=1,40
          do i=1,400
            if(CifKeyFlag(i,j).ne.0) then
              call FeChybne(-1.,-1.,'crutial keyword missing:',
     1                      CifKey(i,j),SeriousError)
              ErrFlag=1
            endif
          enddo
        enddo
        if(ErrFlag.ne.0) go to 9999
        write(out,'(''All crutial data present'')')
        write(out,FormA)
        write(out,'(''Other problems'')')
        write(out,'(''=============='')')
      else if(KlicUsed.eq.ProHonzu) then
        lnb=NextLogicNumber()
        call OpenFile(lnb,fln(:ifln)//'.bib','formatted','unknown')
        s256=CifKey(31,14)
        call CIFGetSt(s256,sta,mm,t256,ich)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          n=1
        else
          call CIFGetLoop(' ',StLbl,MxLoop,s256,ia,Sta,pa,spa,0,n,
     1                    t256,ich)
          if(ich.eq.-2) go to 9000
        endif
        if(ich.eq.0) then
          t256='0010 '//sta(1)(:idel(sta(1)))
          do i=2,n-1
            t256=t256(:idel(t256))//', '//sta(i)(:idel(sta(i)))
          enddo
          if(n.gt.1) t256=t256(:idel(t256))//' & '//
     1                    sta(n)(:idel(sta(n)))
          write(lnb,FormA) t256(:idel(t256))
        endif
        call CIFGetSt(CifKey(23,13),sta,mm,t256,ich)
        s256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          s256='0010 '//s256(:idel(s256))
          write(lnb,FormA) s256(:idel(s256))
        else
          write(lnb,'(''0010 Unknown journal'')')
        endif
        call CIFGetSt(CifKey(36,13),sta,mm,t256,ich)
        s256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.ne.0) then
          s256='?'
        endif
        call CIFGetSt(CifKey(37,13),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          s256=s256(:idel(s256))//' ('//p256(:idel(p256))//'),'
        endif
        call CIFGetSt(CifKey(24,13),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          s256=s256(:idel(s256)+1)//p256(:idel(p256))
        else
          s256=s256(:idel(s256))//' ?'
        endif
        call CIFGetSt(CifKey(25,13),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          s256=s256(:idel(s256))//'-'//p256(:idel(p256))
        else
          s256=s256(:idel(s256))//'-?'
        endif
        s256='0010 '//s256(:idel(s256))
        write(lnb,FormA) s256(:idel(s256))
        call CIFGetReal(CifKey(22,15),pom,spom,t256,ich)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.0) then
          write(Cislo,'(f8.3)') pom
        else
          Cislo='?'
        endif
        call Zhusti(Cislo)
        write(lnb,'(''0020 '',a)') Cislo(:idel(Cislo))
        do i=1,4
          k=20+i
          if(i.eq.1) then
            j=7
          else if(i.eq.2) then
            j=22
          else if(i.eq.3) then
            j=23
          else
            j=5
            k=29
          endif
          write(Cislo,'(''00'',i2)') k
          call CIFGetSt(CifKey(j,6),sta,mm,t256,ich)
          s256=sta(1)
          if(ich.eq.-2) then
            go to 9000
          else if(ich.ne.0) then
            s256='?'
          endif
          s256=Cislo(1:5)//s256(:idel(s256))
          write(lnb,FormA) s256(:idel(s256))
        enddo
        do i=1,2
          k=29+i
          if(i.eq.1) then
            j=1
          else
            j=4
          endif
          write(Cislo,'(''00'',i2)') k
          call CIFGetSt(CifKey(j,18),sta,mm,t256,ich)
          s256=sta(1)
          if(ich.eq.-2) then
            go to 9000
          else if(ich.ne.0) then
            s256='?'
          endif
          s256=Cislo(1:5)//s256(:idel(s256))
          write(lnb,FormA) s256(:idel(s256))
        enddo
      endif
      CIFFlag=0
      NSymm(KPhase)=0
      NAtFormulaP=0
      isTOF=.false.
      isED=.false.
      TOFInD=.false.
      ChargeDensities=.false.
      CellMissing=.false.
         QMissing=.false.
      do i=1,3
        s256=CifKey(i+4,5)
        call CIFGetReal(s256,CellPar(i,1,KPhase),
     1                  CellParSU(i,1,KPhase),t256,ich)
        if(ich.lt.0) then
          CellPar(i,1,KPhase)=10.
          CellParSU(i,1,KPhase)=0.
          if(ich.eq.-1) then
            CellMissing=.true.
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        endif
      enddo
      do i=4,6
        s256=CifKey(i-3,5)
        call CIFGetReal(s256,CellPar(i,1,KPhase),
     1                  CellParSU(i,1,KPhase),t256,ich)
        if(ich.lt.0) then
          CellPar(i,1,KPhase)=90.
          CellParSU(i,1,KPhase)=0.
          if(ich.eq.-1) then
            CellMissing=.true.
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        endif
      enddo
      if(isPowder) call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
      s256=CifKey(21,5)
      call CIFGetInt(s256,n,t256,ich)
      if(ich.lt.0) then
        NComp(KPhase)=1
        n=0
        if(ich.eq.-1) then
          n=0
        else if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        endif
      endif
      if(n.le.0) then
        s256=CifKey(147,5)
        call CIFGetReal(s256,qu(1,1,1,KPhase),pom,t256,ich)
        if(ich.gt.0) n=ich
      endif
      NDim(KPhase)=n+3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      MaxNDim=max(MaxNDim,NDim(KPhase))
      MaxNDimI=MaxNDim-3
      if(NDimI(KPhase).gt.0) then
        call SetIntArrayTo(kw(1,1,KPhase),3*mxw,0)
        if(NDimI(KPhase).eq.1) then
          j=mxw
        else
          j=NDimI(KPhase)
        endif
        do i=1,j
          kw(mod(i-1,NDimI(KPhase))+1,i,KPhase)=(i-1)/NDimI(KPhase)+1
        enddo
      endif
      call SetRealArrayTo(qu(1,1,1,KPhase),9,0.)
      if(KPhase.le.1) then
        m=22
        mm=23
        n=22
        call CIFGetLoop(CifKey(m,n),StLbl,MxLoop,CifKey(mm,n),ia,Sta,
     1                  pa,spa,1,nn,t256,ich)
        if(ich.eq.-1) then
          m=6
          mm=7
          n=23
          call CIFGetLoop(CifKey(m,n),StLbl,MxLoop,CifKey(mm,n),ia,Sta,
     1                    pa,spa,1,nn,t256,ich)
        endif
        NTwinOld=NTwin
        if(ich.lt.0) then
          NTwin=NTwinOld
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        else
          NTwin=nn
        endif
        if(nn.gt.1) then
          if(n.eq.22) then
            m=22
            m0=12
          else
            m=6
            m0=7
          endif
          allocate(IdCode(NTwin))
          do i=1,NTwin
            ScTw (i,KDatBlock)= pa(i)
            ScTwS(i,KDatBlock)=spa(i)
            IdCode(i)=StLbl(i)
          enddo
          k=0
          do i=1,3
            do j=1,3
              k=k+1
              mm=m0+i+(j-1)*3
              call CIFGetLoop(CifKey(m,n),StLbl,MxLoop,CifKey(mm,n),ia,
     1                        Sta,pa,spa,1,nn,t256,ich)
              do l=1,nn
                do kk=1,NTwin
                  if(EqIgCase(StLbl(l),IdCode(kk))) then
                    RTw(k,kk)=pa(l)
                    exit
                  endif
                enddo
              enddo
            enddo
          enddo
          deallocate(IdCode)
          mxscutw=max(NTwin,6)
          mxscu=mxscutw-NTwin+1
        endif
      endif
      if(NDimI(KPhase).gt.0) then
        s256=CifKey(146,5)
        call CIFGetInt(s256,NComp(KPhase),t256,ich)
        if(ich.lt.0) then
          NComp(KPhase)=1
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        endif
        do i=1,3
          s256=CifKey(146+i,5)
          call CIFGetReal(s256,qu(i,1,1,KPhase),pom,t256,ich)
          if(ich.le.0) then
            if(ich.ge.-1) then
              cycle
            else if(ich.eq.-2) then
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          endif
          s256=CifKey(150,5)
          p256=CifKey(146+i,5)
          call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,n,t256,
     1                    ich)
          if(ich.le.0) then
            if(ich.eq.-1) then
              cycle
            else if(ich.eq.-2) then
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          endif
          jj=0
          do j=1,n
            if(StLbl(j).eq.'?') cycle
            jj=jj+1
            StLbl(jj)=StLbl(j)
            pa(jj)=pa(j)
          enddo
          n=jj
          do j=1,NDimI(KPhase)
            k=0
            call kus(StLbl(j),k,Cislo)
            call posun(Cislo,0)
            read(Cislo,FormI15) k
            qu(i,k,1,KPhase)=pa(k)
          enddo
        enddo
        do i=1,NDimI(KPhase)
          if(EqRV0(qu(1,i,1,KPhase),3,.0001)) then
            QMissing=.true.
            exit
          endif
        enddo
        if(NComp(KPhase).gt.1) then
          do i=1,NComp(KPhase)
            call SetRealArrayTo(zv(1,i,KPhase),NDimQ(KPhase),0.)
          enddo
          allocate(IdCode(NComp(KPhase)))
          p256=CifKey(25,5)
          ip=LocateSubstring(p256,'W',.false.,.true.)+1
          k=0
          do i=1,NDim(KPhase)
            do j=1,NDim(KPhase)
              k=k+1
              write(p256(ip:),'(2(''_'',i2))') j,i
              call Zhusti(p256)
              s256=CifKey(24,5)
              call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,n,
     1                        t256,ich)
              if(ich.eq.-2) then
                call CIFSyntaxError(CIFLastReadRecord)
                go to 2000
              else if(ich.eq.0) then
                do l=1,n
                  ZV(k,l,KPhase)=pa(l)
                enddo
              endif
            enddo
          enddo
          call CopyStringArray(StLbl,IdCode,n)
        else
          call UnitMat(zv(1,1,KPhase),NDim(KPhase))
        endif
        do i=1,NComp(KPhase)
          call MatInv(ZV(1,i,KPhase),ZVI(1,i,KPhase),Det,NDim(KPhase))
          if(abs(Det).lt..001) then
            write(Cislo,'(i1,a2)') i,nty(i)
            call FeChybne(-1.,-1.,'W matrix for '//Cislo(:3)//
     1                    ' subsystem is singular.',' ',SeriousError)
            CIFFlag=1
            call UnitMat(zv(1,i,KPhase),NDim(KPhase))
          endif
        enddo
        n=22
        do m=1,9
          call CIFGetReal(CifKey(m,n),rmp(m),spom,t256,ich)
          if(ich.ne.0) then
            if(ich.ge.-1) then
              KCommen(KPhase)=0
              exit
            else if(ich.eq.-2) then
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          endif
          KCommen(KPhase)=1
        enddo
        if(KCommen(KPhase).gt.0) then
          pom=0.
          m=0
          do i=1,3
            do j=1,3
              m=m+1
              if(i.ne.j) pom=pom+abs(rmp(m))
            enddo
          enddo
          if(pom.gt..00001) then
            ICommen(KPhase)=1
            call TrMat(rmp,RCommen(1,1,KPhase),3,3)
          else
            ICommen(KPhase)=0
            m=0
            do i=1,3
              do j=1,3
                m=m+1
                if(i.eq.j) NCommen(i,1,KPhase)=nint(rmp(m))
              enddo
            enddo
          endif
          do i=1,NDimI(KPhase)
            m=i+9
            call CIFGetReal(CifKey(m,n),trez(i,1,KPhase),spom,t256,ich)
            if(ich.ne.0) then
              if(ich.ge.-1) then
                KCommen(KPhase)=0
                exit
              else if(ich.eq.-2) then
                call CIFSyntaxError(CIFLastReadRecord)
              endif
            endif
          enddo
        endif
      endif
      go to 2010
2000  do i=1,NComp(KPhase)
        call UnitMat(zv(1,i,KPhase),NDim(KPhase))
      enddo
2010  if(CellMissing) then
        call FeChybne(-1.,-1.,'Some of cell parameters are missing.',
     1                ' ',SeriousError)
        CIFFlag=1
      endif
      if(QMissing) then
        call FeChybne(-1.,-1.,'some components of modulation vector '//
     1                'are missing.',' ',SeriousError)
        CIFFlag=1
      endif
      CrSystem(KPhase)=CrSystemFromCell(CellPar(1,1,KPhase),.001,.05)
      Monoclinic(KPhase)=CrSystem(KPhase)/10
      MameSymbol=0
      if(ReadMagneticCIF) go to 2020
      if(NDimI(KPhase).eq.0) then
        call CIFGetSt(CifKey(4,18),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.0) then
          ShSg(1:NDim(KPhase),KPhase)=0.
          call Zhusti(p256)
          Grupa(KPhase)=p256
          if(NPhase.le.1) then
            n=1
            call AllocateSymm(n+1,n+1,1,1)
          endif
          call SetIgnoreWTo(.true.)
          call SetIgnoreETo(.true.)
          call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,pa,ich)
          call ResetIgnoreW
          call ResetIgnoreE
          if(ich.eq.0) then
            MameSymbol=1
            n=NSymm(KPhase)
          endif
        else if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        endif
      else
        MameSymbol=0
        call CIFGetSt(CifKey(3,21),sta,mm,t256,ich)
        p256=sta(1)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.-1) then
          call CIFGetSt(CifKey(2,21),sta,mm,t256,ich)
          p256=sta(1)
        endif
        if(ich.eq.0) then
          call CIFITSSGSymbol(p256,ich)
          if(ich.eq.0) then
            MameSymbol=1
            n=NSymm(KPhase)
          endif
        else
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          call CIFGetSt(CifKey(4,21),sta,mm,t256,ich)
          p256=sta(1)
          if(ich.eq.0) then
            call CIFWJJSSGSymbol(p256,ich)
            if(ich.eq.0) then
              MameSymbol=2
              n=NSymm(KPhase)
            endif
          else if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        endif
      endif
      if(MameSymbol.ne.0) then
        MaxNSymm=NSymm(KPhase)
        MaxNSymmN=NSymm(KPhase)
        NSymmN(KPhase)=NSymm(KPhase)
        Lattice(KPhase)=Grupa(KPhase)(1:1)
      endif
2020  MameOperatory=0
      e256=' '
      kk=0
2025  n=0
      if(NDimI(KPhase).le.0) then
        if(kk.eq.0) then
          s256=CifKey(8,21)
          p256=CifKey(9,21)
        else
          s256=CifKey(5,18)
          p256=CifKey(6,18)
        endif
        kk=kk+1
      else
        s256=CifKey(6,21)
        p256=CifKey(7,21)
      endif
2030  call CIFGetSt(s256,sta,mm,t256,ich)
      if(ich.gt.0) then
        n=mm
      else if(ich.eq.-2) then
        e256=CIFLastReadRecord
      else if(ich.eq.-1) then
        call CIFGetSt(p256,sta,mm,t256,ich)
        if(ich.gt.0) then
          n=-mm
        else if(ich.eq.-2) then
          e256=CIFLastReadRecord
        endif
      endif
      if(e256.ne.' ') then
        ich=-2
        CIFLastReadRecord=e256
        go to 9000
      endif
      if(ich.lt.0) then
        if(kk.eq.1.and.NDimI(KPhase).le.0) go to 2025
      else
        MameOperatory=1
        if(n.lt.0) then
          s256=p256
          kk=1
        else
          kk=0
        endif
        call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,
     1                  ich)
        if(ich.eq.-2) then
          go to 9000
        else if(ich.eq.-1) then
          MameOperatory=0
        endif
        if(kk.eq.1) then
          do i=1,n
            Sta(i)=StLbl(i)
            write(StLbl(i),'(i5)') i
            call Zhusti(StLbl(i))
          enddo
        endif
      endif
      if(MameOperatory.le.0.and.ReadMagneticCIF) then
        if(NDimI(KPhase).le.0) then
          s256=CifKey(39,21)
          p256=CifKey(41,21)
        else
          s256=CifKey(69,21)
          p256=CifKey(70,21)
        endif
        call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,
     1                  ich)
        if(ich.eq.-2) then
          n=0
          call CIFSyntaxError(CIFLastReadRecord)
        endif
      endif
2040  if(n.le.0) then
        if(MameSymbol.gt.0) go to 2250
        n=1
        if(NDimI(KPhase).eq.0) then
          sta(1)='x,y,z'
        else
          sta(1)=' '
          do i=1,NDim(KPhase)
            write(t3,'(''x'',i1,'','')') i
            sta(1)=sta(1)(:idel(sta(1)))//t3
          enddo
          i=idel(sta(1))
          sta(1)(i:i)=' '
        endif
        if(MameSymbol.eq.0.and.MameOperatory.eq.0) then
          if(ICDDBatch) then
            call FeChybne(-1.,-1.,'symmetry information is missing.',
     1                    'Trivial symmetry will be used.',Warning)
            NSymm(KPhase)=1
            NSymmN(KPhase)=1
            NLattVec(KPhase)=1
            if(NPhase.le.1) call AllocateSymm(24,4,1,1)
            call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
            call SetRealArrayTo(vt6(1,1,1,KPhase),NDim(KPhase),0.)
            call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
          else
            NInfo=1
            TextInfo(NInfo)='Symmetry information is missing. The '//
     1                      'program will use just trivial symmetry.'
            NInfo=NInfo+1
            TextInfo(NInfo)='You should either correct manually the'//
     1                      ' input CIF file and import it once more'
            NInfo=NInfo+1
            TextInfo(NInfo)='or correct the file M50 by Jana tools.'
            call FeInfoOut(-1.,-1.,'Warning','L')
          endif
        endif
      endif
2050  if(MameSymbol.ne.0) then
        if(allocated(rm6o)) deallocate(rm6o,s6o,vt6o)
        allocate(rm6o(NDimQ(KPhase),NSymm(KPhase)),
     1           s6o(NDim(KPhase),NSymm(KPhase)),
     2           vt6o(NDim(KPhase),NLattVec(KPhase)))
        nso=NSymm(KPhase)
        nvto=NLattVec(KPhase)
        do i=1,NLattVec(KPhase)
          call CopyVek(vt6(1,i,1,KPhase),vt6o(1,i),NDim(KPhase))
        enddo
        do i=1,NSymm(KPhase)
          call CopyMat(rm6(1,i,1,KPhase),rm6o(1,i),NDim(KPhase))
          call CopyVek(s6(1,i,1,KPhase),s6o(1,i),NDim(KPhase))
        enddo
      endif
      if(NPhase.le.1) then
        call AllocateSymm(n+1,n+1,1,1)
      else
        call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),NComp(KPhase),
     1                   NPhase,1)
      endif
      NLattVec(KPhase)=1
      call SetRealArrayTo(vt6(1,1,1,KPhase),NDim(KPhase),0.)
      NSymm(KPhase)=n
      MaxNSymm=n
      NSymmN(KPhase)=n
      MaxNSymmN=n
      Identita=0
      do i=1,n
        if(KlicUsed.eq.ProHonzu) then
          t256='0032 '//sta(i)(:idel(sta(i)))
          write(lnb,FormA) t256(:idel(t256))
        endif
2060    call DeleteString(sta(i),'~',k)
        if(k.gt.0) go to 2060
2065    call DeleteString(sta(i),'''',k)
        if(k.gt.0) go to 2065
        do j=1,idel(sta(i))
          if(sta(i)(j:j).eq.',') sta(i)(j:j)=' '
        enddo
        k=0
        do j=1,NDim(KPhase)
          call kus(Sta(i),k,Cislo)
        enddo
        call SetIgnoreWTo(.true.)
        call SetIgnoreETo(.true.)
        call ReadSymm(sta(i)(:k),rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),pom,0)
        call MatBlock3(rm6(1,i,1,KPhase),rm(1,i,1,KPhase),NDim(KPhase))
        call MatInv(rm(1,i,1,KPhase),rmp,det,3)
        if(ReadMagneticCIF.and.k.lt.len(Sta(i))) then
          Cislo=Sta(i)(k:)
          call posun(Cislo,1)
          read(Cislo,'(f15.0)') ZMag(i,1,KPhase)
        else
          ZMag(i,1,KPhase)=1.
        endif
        rmag(1:3,i,1,KPhase)=ZMag(i,1,KPhase)*det*rm(1:3,i,1,KPhase)
        BratSymm(i,KPhase)=.true.
        ISwSymm(i,1,KPhase)=1
        call ResetIgnoreW
        call ResetIgnoreE
        if(ErrFlag.ne.0) then
          call CIFSyntaxError('symmetry card : '//sta(i)(:idel(sta(i))))
          n=0
          go to 2040
        endif
        call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
      enddo
      if(NDimI(KPhase).eq.0) then
        if(ReadMagneticCIF) then
          s256=CifKey(46,21)
          p256=CifKey(48,21)
        else
          s256=CifKey(43,21)
          p256=CifKey(45,21)
        endif
      else
        if(ReadMagneticCIF) then
          s256=CifKey(73,21)
          p256=CifKey(74,21)
        else
          s256=CifKey(71,21)
          p256=CifKey(72,21)
        endif
      endif
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,StaP,pa,spa,0,m,t256,
     1                ich)
      if(ich.eq.-2) then
        go to 9000
      else if(ich.ne.-1.and.m.gt.0) then
        call ReallocSymm(NDim(KPhase),NSymm(KPhase)+m,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,1)
        nold=n
        do i=1,m
2090      call DeleteString(StaP(i),'~',k)
          if(k.gt.0) go to 2090
2095      call DeleteString(StaP(i),'''',k)
          if(k.gt.0) go to 2095
          do j=1,idel(StaP(i))
            if(StaP(i)(j:j).eq.',') StaP(i)(j:j)=' '
          enddo
          k=0
          do j=1,NDim(KPhase)
            call kus(StaP(i),k,Cislo)
          enddo
          call SetIgnoreWTo(.true.)
          call SetIgnoreETo(.true.)
          call ReadSymm(StaP(i)(:k),rmp,xp,SymmCP,pom,0)
          call ResetIgnoreW
          call ResetIgnoreE
          if(ErrFlag.ne.0.or.
     1       .not.MatRealEqUnitMat(rmp,NDim(KPhase),.001)) then
            call CIFSyntaxError('centering operation : '//
     1                          StaP(i)(:idel(StaP(i))))
            cycle
          endif
          call od0do1(xp,xp,NDim(KPhase))
          if(ReadMagneticCIF.and.k.lt.len(StaP(i))) then
            Cislo=StaP(i)(k:)
            call posun(Cislo,1)
            read(Cislo,'(f15.0)') ZMagP
          else
            ZMagP=1.
          endif
          if(EqRV0(xp,NDim(KPhase),.001).and.abs(ZMagP-1.).le..001 )
     1      cycle
          n=n+1
          BratSymm(n,KPhase)=.true.
          ISwSymm(n,1,KPhase)=1
          rm6(1:NDimQ(KPhase),n,1,KPhase)=rmp(1:NDimQ(KPhase))
          s6(1:NDim(KPhase),n,1,KPhase)=xp(1:NDim(KPhase))
          symmc(1:NDim(KPhase),n,1,KPhase)=symmcp(1:NDim(KPhase))
          ZMag(n,1,KPhase)=ZMagP
          call MatBlock3(rm6(1,n,1,KPhase),rm(1,n,1,KPhase),
     1                   NDim(KPhase))
          rmag(1:9,n,1,KPhase)=ZMagP*rm(1:9,n,1,KPhase)
        enddo
        if(n.gt.nold) then
          NSymm(KPhase)=n
          call CompleteSymm(1,ich)
          n=NSymm(KPhase)
        endif
      endif
      do i=1,n
        if(MatRealEqUnitMat(rm6(1,i,1,KPhase),NDim(KPhase),.001)) then
          if(EqRV0(s6(1,i,1,KPhase),NDim(KPhase),.001)) then
            identita=i
          else if(MagneticType(KPhase).le.0.or.ZMag(i,1,KPhase).gt.0)
     1      then
            BratSymm(i,KPhase)=.false.
            call ReallocSymm(NDim(KPhase),NSymm(KPhase),
     1                       NLattVec(KPhase)+1,NComp(KPhase),NPhase,1)
            NLattVec(KPhase)=NLattVec(KPhase)+1
            call CopyVek(s6(1,i,1,KPhase),
     1                   vt6(1,NLattVec(KPhase),1,KPhase),NDim(KPhase))
            call od0do1(vt6(1,NLattVec(KPhase),1,KPhase),
     1                  vt6(1,NLattVec(KPhase),1,KPhase),NDim(KPhase))
          endif
        endif
      enddo
      if(Identita.ne.1) then
        call CopyMat(rm6(1,1,1,KPhase),rm6(1,Identita,1,KPhase),
     1               NDim(KPhase))
        call MatBlock3(rm6(1,1,1,KPhase),rm(1,Identita,1,KPhase),
     1                 NDim(KPhase))
        call CopyMat(rm(1,1,1,KPhase),rmag(1,Identita,1,KPhase),3)
        call CopyVek(s6(1,1,1,KPhase),s6(1,Identita,1,KPhase),
     1               NDim(KPhase))
        BratSymm(Identita,KPhase)=BratSymm(1,KPhase)
        call UnitMat(rm6(1,1,1,KPhase),NDim(KPhase))
        call UnitMat(rm(1,1,1,KPhase),3)
        call UnitMat(rmag(1,1,1,KPhase),3)
        call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
        BratSymm(1,KPhase)=.true.
      endif
      if(KlicUsed.eq.ProHonzu) close(lnb)
      do i=1,n
        call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
      enddo
      if(NLattVec(KPhase).gt.0) then
        do i=1,n-1
          if(.not.BratSymm(i,KPhase)) cycle
          rmp(1:NDimQ(KPhase))=rm6(1:NDimQ(KPhase),i,1,KPhase)
          sp1(1:NDim(KPhase))=s6(1:NDim(KPhase),i,1,KPhase)
          call NormCentr(sp1)
          do j=i+1,n
            if(EqRV(rmp,rm6(1,j,1,KPhase),NDimQ(KPhase),.001).and.
     1         BratSymm(j,KPhase)) then
              sp2(1:NDim(KPhase))=s6(1:NDim(KPhase),j,1,KPhase)
              call NormCentr(sp2)
              if(Eqrv(sp1,sp2,NDim(KPhase),.0001))
     1          BratSymm(j,KPhase)=.false.
            endif
          enddo
        enddo
        NSymm(KPhase)=0
        do i=1,n
          if(.not.BratSymm(i,KPhase)) cycle
          NSymm(KPhase)=NSymm(KPhase)+1
          call CopySymmOperator(i,NSymm(KPhase),1)
        enddo
      endif
      call CrlOrderMagSymmetry
      MaxNSymm=NSymm(KPhase)
      MaxNSymmN=NSymmN(KPhase)
      IgnoreE=.true.
      call CompleteSymm(1,ich)
      IgnoreE=.false.
      if(MaxNSymm.ne.NSymm(KPhase)) then
        NSymm(KPhase)=MaxNSymm
        dlk=0.
        do i=1,NSymm(KPhase)
          do j=1,NDim(KPhase)
            dlk=max(dlk,FeTxLength(SymmC(j,i,1,KPhase)))
          enddo
        enddo
        dlk=dlk*2.
        pom=0.
        do i=1,NDim(KPhase)
          call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
          pom=pom+dlk
        enddo
        dls=0.
        NInfo=0
        do i=1,NSymm(KPhase)
          if(NInfo.gt.ubound(TextInfo,1)-4) cycle
          if(ICDDBatch) then
            TextInfo(i)=sta(i)
          else
            TextInfo(i)=' '
            do j=1,NDim(KPhase)
              TextInfo(i)=TextInfo(i)(:idel(TextInfo(i)))//Tabulator//
     1          SymmC(j,i,1,KPhase)(:idel(SymmC(j,i,1,KPhase)))
            enddo
            dls=max(dls,FeTxLength(TextInfo(i)))
          endif
          NInfo=NInfo+1
        enddo
        call FeBoldFont
        if(ICDDBatch) then
          NInfo=NInfo+1
          TextInfo(NInfo)='does not make a group.'
        else
          if(MameSymbol.gt.0) then
            dlp=dls
            NInfo=NInfo+1
            TextInfo(NInfo)='does not make a group. The program will '//
     1                      'try to use symmetry'
            TextInfoFlags(NInfo)='LB'
            dlp=FeTxLength(TextInfo(NInfo))
            NInfo=NInfo+1
            TextInfo(NInfo)='as follows from the symbol.'
            TextInfoFlags(NInfo)='LB'
            dlp=FeTxLength(TextInfo(NInfo))
          else
            NInfo=NInfo+1
            TextInfo(NInfo)='does not make a group. The program will '//
     1                      'reduce the symmetry'
            TextInfoFlags(NInfo)='LB'
            dlp=FeTxLength(TextInfo(NInfo))
            NInfo=NInfo+1
            TextInfo(NInfo)='to trivial one. You should either '//
     1                      'correct manually the input CIF'
            TextInfoFlags(NInfo)='LB'
            dlp=max(dlp,FeTxLength(TextInfo(NInfo)))
            NInfo=NInfo+1
            TextInfo(NInfo)='file and import it once more or correct '//
     1                      'the file M50 by Jana tools.'
            TextInfoFlags(NInfo)='LB'
            dlp=max(dlp,FeTxLength(TextInfo(NInfo)))
            call FeNormalFont
          endif
        endif
        if(ICDDBatch) then
          call FeChybne(-1.,-1.,'the set of symmetry operations:',' ',
     1                  SeriousError)
          do i=1,NInfo
            if(LogLn.gt.0)
     1        write(LogLn,FormA) TextInfo(i)(:idel(TextInfo(i)))
            call FeLstWriteLine(TextInfo(i),-1)
          enddo
          CIFFFlag=1
        else
          if(dlp.gt.dls) then
            call FeTabsReset(UseTabs)
            pom=(dlp-dls)*.5
            do i=1,NDim(KPhase)
              call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
              pom=pom+dlk
            enddo
          endif
          call FeInfoOut(-1.,-1.,'The set of symmetry operations:','L')
        endif
        call FeTabsReset(UseTabs)
        if(MameSymbol.gt.0) then
          NSymm(KPhase)=nso
          MaxNSymm=NSymm(KPhase)
          MaxNSymmN=NSymm(KPhase)
          NSymmN(KPhase)=NSymm(KPhase)
          NLattVec(KPhase)=nvto
          do i=1,NLattVec(KPhase)
            call CopyVek(vt6o(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
          enddo
          do i=1,NSymm(KPhase)
            call CopyMat(rm6o(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
            call CopyVek(s6o(1,i),s6(1,i,1,KPhase),NDim(KPhase))
            call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                    symmc(1,i,1,KPhase),0)
            call MatBlock3(rm6(1,i,1,KPhase),rm (1,i,1,KPhase),
     1                     NDim(KPhase))
          enddo
        else
          NSymm(KPhase)=1
          MaxNSymm=NSymm(KPhase)
          MaxNSymmN=NSymm(KPhase)
          NSymmN(KPhase)=NSymm(KPhase)
          NLattVec(KPhase)=1
          call UnitMat(rm (1,1,1,KPhase),3)
          call UnitMat(rm6(1,1,1,KPhase),3)
          call CodeSymm(rm6(1,1,1,KPhase),s6(1,1,1,KPhase),
     1                  symmc(1,1,1,KPhase),0)
          ISwSymm(1,1,KPhase)=1
          ZMag(1,1,KPhase)=1.
          BratSymm(1,KPhase)=.true.
          call SetRealArrayTo( s6(1,1,1,KPhase),NDim(KPhase),0.)
          call SetRealArrayTo(vt6(1,1,1,KPhase),NDim(KPhase),0.)
        endif
        go to 2200
      endif
      if(MameSymbol.gt.0) then
        call FindSmbSg(t256,ChangeOrderNo,1)
        Nesouhlas=0
        nn=max(NSymm(KPhase),NLattVec(KPhase))
        allocate(EqSymm(nn))
        call SetIntArrayTo(EqSymm,nn,0)
        n=0
        do i=1,NLattVec(KPhase)
          do 2120j=1,nvto
            do m=1,NDim(KPhase)
              xp(m)=vt6(m,i,1,KPhase)-vt6o(m,j)
            enddo
            call od0do1(xp,xp,NDim(KPhase))
            do m=1,NDim(KPhase)
              if(abs(anint(xp(m))-xp(m)).gt..0001) go to 2120
            enddo
            n=n+1
            EqSymm(i)=j
2120      continue
        enddo
        if((n.ne.NLattVec(KPhase).or.nvto.gt.NLattVec(KPhase)).and.
     1     .not.EqIgCase(Lattice(KPhase),'X')) then
          Nesouhlas=1
          if(.not.ICDDBatch) then
            ln=NextLogicNumber()
            call OpenFile(ln,fln(:ifln)//'_list.tmp','formatted',
     1                    'unknown')
          endif
          t256='some translation vectors following from the symbol '//
     1         'do not correspond'
          p256=' to those from operators or vice versa.'
          if(ICDDBatch) then
            call FeChybne(-1.,-1.,t256,p256,SeriousError)
          else
            call Velka(t256(1:1))
            write(ln,FormA) t256(:idel(t256))
            write(ln,FormA) p256(:idel(p256))
          endif
          do i=1,NLattVec(KPhase)
            write(t256,'(''  #'',i2,'' from operators ('')') i
            do m=1,NDim(KPhase)
              call ToFract(vt6(m,i,1,KPhase),Cislo,6)
              t256=t256(:idel(t256))//Cislo(:idel(Cislo))
              if(m.ne.NDim(KPhase)) then
                t256=t256(:idel(t256))//','
              else
                t256=t256(:idel(t256))//')'
              endif
            enddo
            j=EqSymm(i)
            if(j.gt.0) then
              write(Cislo,'(''#'',i2,'' from symbol'')') j
              t256=t256(:idel(t256))//' = '//Cislo(:idel(Cislo))
            endif
            if(ICDDBatch) then
              call FeLstWriteLine(t256,-1)
              if(LogLN.gt.0) write(LogLn,FormA) t256(:idel(t256))
            else
              write(ln,FormA) t256(:idel(t256))
            endif
          enddo
          ii=0
          do 2130i=1,nvto
            do j=1,NLattVec(KPhase)
              if(EqSymm(j).eq.i) go to 2130
            enddo
            write(t256,'(''  #'',i2,'' from the symbol ('')') i
            do m=1,NDim(KPhase)
              call ToFract(vt6o(m,i),Cislo,6)
              t256=t256(:idel(t256))//Cislo(:idel(Cislo))
              if(m.ne.NDim(KPhase)) then
                t256=t256(:idel(t256))//','
              else
                t256=t256(:idel(t256))//')'
              endif
            enddo
            if(ii.eq.0) then
              p256='  Translation vectors from the symbol not having '//
     1             'some correspondant:'
              if(ICDDBatch) then
                call FeLstWriteLine(p256,-1)
                if(LogLn.gt.0) write(LogLN,FormA) p256(:idel(p256))
              else
                write(ln,FormA) p256(:idel(p256))
              endif
            endif
            ii=ii+1
            if(ICDDBatch) then
              call FeLstWriteLine(t256,-1)
              if(LogLn.gt.0) write(LogLN,FormA) t256(:idel(t256))
            else
              write(ln,FormA) t256(:idel(t256))
            endif
2130      continue
        endif
        call SetIntArrayTo(EqSymm,nn,0)
        n=0
        do i=1,NSymm(KPhase)
          do j=1,nso
            if(.not.EqRV(rm6(1,i,1,KPhase),rm6o(1,j),NDimQ(KPhase),.01))
     1        cycle
            do 2140k=1,NLattVec(KPhase)
              call AddVek(s6(1,i,1,KPhase),vt6(1,k,1,KPhase),xp,
     1                    NDim(KPhase))
              call MultM(rm6(1,i,1,KPhase),ShSg(1,KPhase),xq,
     1                   NDim(KPhase),NDim(KPhase),1)
              do m=1,NDim(KPhase)
                xp(m)=xp(m)-s6o(m,j)-xq(m)+ShSg(m,KPhase)
              enddo
              call od0do1(xp,xp,NDim(KPhase))
              do m=1,NDim(KPhase)
                if(abs(anint(xp(m))-xp(m)).gt..0001) go to 2140
              enddo
              n=n+1
              EqSymm(i)=j
              exit
2140        continue
          enddo
        enddo
        if(n.ne.NSymm(KPhase).or.nso.gt.NSymm(KPhase)) then
          Nesouhlas=1
          if(.not.ICDDBatch) then
            ln=NextLogicNumber()
            call OpenFile(ln,fln(:ifln)//'_list.tmp','formatted',
     1                    'unknown')
          endif
          t256='some symmetry operations following from the symbol '//
     1         'do not correspond'
          p256=' to those from operators or vice versa.'
          if(ICDDBatch) then
            call FeChybne(-1.,-1.,t256,p256,SeriousError)
          else
            call Velka(t256(1:1))
            write(ln,FormA) t256(:idel(t256))
            write(ln,FormA) p256(:idel(p256))
          endif
          do i=1,NSymm(KPhase)
            write(t256,'(''  #'',i2,'' from operators ('')') i
            do m=1,NDim(KPhase)
              p256=symmc(m,i,1,KPhase)
              t256=t256(:idel(t256))//p256(:idel(p256))
              if(m.ne.NDim(KPhase)) then
                t256=t256(:idel(t256))//','
              else
                t256=t256(:idel(t256))//')'
              endif
            enddo
            j=EqSymm(i)
            if(j.gt.0) then
              write(Cislo,'(''#'',i2,'' from symbol'')') j
              t256=t256(:idel(t256))//' = '//Cislo(:idel(Cislo))
            endif
            if(ICDDBatch) then
              call FeLstWriteLine(t256,-1)
              if(LogLn.gt.0) write(LogLN,FormA) t256(:idel(t256))
            else
              write(ln,FormA) t256(:idel(t256))
            endif
          enddo
          ii=0
          do 2150i=1,nso
            do j=1,NSymm(KPhase)
              if(EqSymm(j).eq.i) go to 2150
            enddo
            write(t256,'(''  #'',i2,'' from the symbol ('')') i
            call CodeSymm(rm6o(1,i),s6o(1,i),SymmCP,0)
            do m=1,NDim(KPhase)
              t256=t256(:idel(t256))//SymmCP(m)(:idel(SymmCP(m)))
              if(m.ne.NDim(KPhase)) then
                t256=t256(:idel(t256))//','
              else
                t256=t256(:idel(t256))//')'
              endif
            enddo
            if(ii.eq.0) then
              p256='  Symmetry operations from the symbol not having '//
     1             'some correspondant:'
              if(ICDDBatch) then
                call FeLstWriteLine(p256,-1)
                if(LogLn.gt.0) write(LogLN,FormA) p256(:idel(p256))
              else
                write(ln,FormA) p256(:idel(p256))
              endif
            endif
            ii=ii+1
            if(ICDDBatch) then
              call FeLstWriteLine(t256,-1)
              if(LogLn.gt.0) write(LogLN,FormA) t256(:idel(t256))
            else
              write(ln,FormA) t256(:idel(t256))
            endif
2150      continue
        endif
        deallocate(EqSymm)
        if(Nesouhlas.ne.0) then
          if(ICDDBatch) then
            CIFFlag=1
          else
            call CloseIfOpened(ln)
            NInfo=2
            TextInfo(1)='These was a disagreement between symmetry '//
     1                  'operations as'
            TextInfo(2)='derived from the symbol and those read in '//
     1                  'from the CIF file.'
            t256=fln(:ifln)//'_list.tmp'
            if(FeYesNoHeader(-1.,-1.,'Do you want to see more details?',
     1         1)) call FeListView(t256,0)
            call DeleteFile(t256)
          endif
        endif
      endif
2200  if(allocated(rm6o)) deallocate(rm6o,s6o,vt6o)
2250  call CopyVek(CellPar(1,1,KPhase),CellParPom,6)
      if(CrSystem(KPhase).eq.CrSystemTriclinic.or.
     1   CrSystem(KPhase).eq.0) then
        call SetIntArrayTo(i90,3,0)
      else
        call SetIntArrayTo(i90,3,1)
      endif
      lpom=isPowder
      call FindSmbCentr(Lattice(KPhase),1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      isPowder=lpom
      call EM50CellSymmTest(0,ich)
      call FindSmbSg(Grupa(KPhase),ChangeOrderNo,1)
      call EM50MakeStandardOrder
      call CopyVek(CellParPom,CellPar(1,1,KPhase),6)
      call CIFGetReal(CifKey(51,10),LamAve(1),sa,t256,ich)
      if(ich.le.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.eq.-1) then
        if(ReadMagneticCIF) then
          LamAve(1)=1.
        else
          LamAve(1)=LamAveD(6)
        endif
      endif
      nlam=1
      call CIFGetSt(CifKey(48,10),sta,mm,t256,ich)
      if(ich.eq.-1)  then
        if(ReadMagneticCIF) then
          p256='neutron'
        else
          p256=' '
        endif
      else
        p256=sta(1)
      endif
      if(ich.le.-2) call CIFSyntaxError(CIFLastReadRecord)
      call mala(p256)
      if(EqIgCase(p256,'neutron')) then
        Radiation(KDatBlock)=NeutronRadiation
      else
        Radiation(KDatBlock)=XRayRadiation
        j=LocateInArray(LamAve(1),LamAveD,7,.0001)
        KLam(1)=j
        if(j.eq.0) then
          LamA1(1)=LamAve(1)
          LamA2(1)=LamAve(1)
          LamRat(1)=0.
          NAlfa(1)=1
        else
          LamA1(1)=LamA1D(j)
          LamA2(1)=LamA2D(j)
          LamRat(1)=LamRatD(j)
          NAlfa(1)=2
        endif
      endif
      call CIFGetReal(CifKey(4,5),pom,spom,t256,ich)
      NUnits(KPhase)=nint(pom)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.eq.-1) then
        NUnits(KPhase)=1
      endif
      FullMultiplicity=NSymmN(KPhase)*NLattVec(KPhase)
      pom=1./float(FullMultiplicity)
      if(allocated(AtMultP)) deallocate(AtMultP)
      allocate(AtMultP(333))
      call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(3,3),ia,Sta,
     1                AtMultP,spa,1,NAtFormulaP,t256,ich)
      if(ich.eq.-1) then
        p256=' '
        call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,p256,ia,Sta,
     1                  AtMultP,spa,1,NAtFormulaP,t256,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.-1) then
          call CIFGetSt(CifKey(23,6),sta,mm,p256,ich)
          if(ich.eq.-2)
     1      call CIFGetSt(CifKey(22,6),sta,mm,p256,ich)
          t256=sta(1)
          if(ich.eq.-1.or.t256.eq.'?') then
            call CIFGetSt(CifKey(23,6),sta,mm,p256,ich)
            t256=sta(1)
          endif
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          else if(ich.eq.-1) then

          else
            call PitChemFormula(t256,StLbl,AtMultP,NAtFormulaP)
            if(NAtFormulaP.lt.0) go to 9000
            pom=1.
          endif
        else
          call CIFGetSt(CifKey(23,6),sta,mm,p256,ich)
          t256=sta(1)
          if(ich.le.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(ich.eq.-1) then
            call CIFGetSt(CifKey(22,6),sta,mm,p256,ich)
            t256=sta(1)
          else
            if(ich.le.-2) call CIFSyntaxError(CIFLastReadRecord)
          endif
          if(ich.eq.0) then
            call PitChemFormula(t256,StLbl,AtMultP,NAtFormulaP)
            if(NAtFormulaP.lt.0) go to 9000
            pom=1.
          else
            do i=1,NAtFormulaP
              AtMultP(i)=FullMultiplicity
            enddo
          endif
        endif
      else if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      endif
      if(NPhase.le.1) then
        if(allocated(AtType))
     1    deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2               AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3               FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4               TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
        if(allocated(TypeCore))
     1    deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2               HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3               CoreValSource)
        n=NAtFormulaP
        if(n.le.0) n=15
        m=121
        i=max(1,NDatBlock)
        allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1           AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2           AtWeight(n,NPhase),AtRadius(n,NPhase),
     3           AtColor(n,NPhase),AtNum(n,NPhase),AtVal(n,NPhase),
     4           AtMult(n,NPhase),FFBasic(m,n,NPhase),
     5           FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6           FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7           FFra(n,NPhase,i),FFia(n,NPhase,i),
     8           FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9           TypeFFMag(n,NPhase),
     a           FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1           FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
        allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1           PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2           ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3           HNSlater(n,NPhase),HZSlater(n,NPhase),
     4           ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5           NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6           CoreValSource(n,NPhase))
      else
        call ReallocFormF(NAtFormulaP,NPhase,NDatBlock)
      endif
      FFCoreD=0.
      FFValD=0.
      CoreValSource='Default'
      ExistXRayData=.true.
      NAtFormula(KPhase)=NAtFormulaP
      call SetRealArrayTo(TypicalDist(1,1,KPhase),NAtFormulaP**2,-1.)
      do i=1,NAtFormula(KPhase)
        AtTypeFull(i,KPhase)=StLbl(i)
        AtTypeMag(i,KPhase)=' '
        AtTypeMagJ(i,KPhase)=' '
        CoreValSource(i,KPhase)='Default'
        call GetPureAtType(StLbl(i),AtType(i,KPhase))
        call uprat(AtTypeFull(i,KPhase))
        call uprat(AtType(i,KPhase))
        AtMult(i,KPhase)=AtMultP(i)*pom
        FFType(KPhase)=-62
      enddo
      call EM50ReadAllFormFactors
      call EM50FormFactorsSet
      if(Radiation(KDatBlock).eq.XRayRadiation) then
        call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(17,3),ia,
     1                  Sta,pa,spa,1,n,t256,ich)
        if(ich.le.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.0) then
          do i=1,n
            j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),
     1                            StLbl(i),IgnoreCaseYes)
            if(j.ne.0) ffra(j,KPhase,KDatBlock)=pa(i)
          enddo
        endif
        call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(16,3),ia,
     1                  Sta,pa,spa,1,n,t256,ich)
        if(ich.le.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.0) then
          do i=1,n
            j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),
     1                            StLbl(i),IgnoreCaseYes)
            if(j.ne.0) ffia(j,KPhase,KDatBlock)=pa(i)
          enddo
        endif
        k=1
        do i=1,9
          call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(i+6,3),ia,
     1                    Sta,pa,spa,1,n,t256,ich)
          if(ich.eq.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          else if(ich.eq.-1) then
            if(i.eq.1) then
              go to 2350
            else
              go to 9000
            endif
          endif
          do j=1,NAtFormula(KPhase)
            jj=ktat(StLbl,n,AtType(j,KPhase))
            if(jj.le.0) jj=j
            FFBasic(k,j,KPhase)=pa(jj)
          enddo
          if(i.eq.1) FFType(KPhase)=-9
          if(i.ne.4) then
            k=k+2
          else
            k=2
          endif
          if(k.gt.9) k=9
        enddo
      else if(Radiation(KDatBlock).eq.NeutronRadiation) then
        call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(18,3),ia,
     1                  Sta,pa,spa,1,n,t256,ich)
        if(ich.le.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.0) then
          do i=1,n
            j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),
     1                            StLbl(i),IgnoreCaseYes)
            if(j.ne.0) ffn(j,KPhase)=pa(i)
          enddo
        endif
        if(MagneticType(KPhase).ne.0) then
          call CIFGetLoop(CIFKey(21,3),StLbl,MxLoop,CIFKey(56,3),ia,
     1                    Sta,pa,spa,0,n,t256,ich)
          if(ich.le.-2) then
            call CIFSyntaxError(CIFLastReadRecord)
          else if(ich.eq.0) then
            do i=1,n
              j=LocateInStringArray(AtType(1,KPhase),NAtFormula(KPhase),
     1                              StLbl(i),IgnoreCaseYes)
              if(j.ne.0) then
                if(EqIgCase(sta(i),'.')) then
                  AtTypeMag(j,KPhase)=' '
                  AtTypeMagJ(j,KPhase)=' '
                else
                  AtTypeMag(j,KPhase)=sta(i)
                endif
              endif
            enddo
          endif
        endif
      endif
2350  call SetFormula(Formula(KPhase))
      lpom=isPowder
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      if(NPhase.gt.1) then
        deallocate(isa)
        allocate(isa(MaxNSymm,NAtAll))
        do i=1,NAtAll
          do j=1,MaxNSymm
            isa(j,i)=j
          enddo
        enddo
      endif
      isPowder=lpom
      ExistM50=.true.
      if(CIFFlag.ne.0.and.ICDDBatch) then
        p256='There were serious error in reading of that CIF file.'
        call FeLstWriteLine(p256,-1)
        if(LogLn.gt.0) write(LogLN,FormA) p256(:idel(p256))
        p256='Please make correction and submit the task again.'
        call FeLstWriteLine(p256,-1)
        if(LogLn.gt.0) write(LogLN,FormA) p256(:idel(p256))
        go to 9900
      endif
      if(KPhase.le.1) then
        ItfMax=0
        MagParMax=0
        IFrMax=0
      endif
      s256=CIFKey(33,1)
      do i=1,3
        p256=CIFKey(29+i,1)
        call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,nac,t256,
     1                  ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(i.eq.1) then
          if(NPhase.le.1) then
            call AllocateAtoms(nac)
          else
            call ReallocateAtoms(nac)
          endif
          call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+nac)
          call EM40UpdateAtomLimits
        endif
        if(nac.gt.0) then
          jj=NAtIndFrAll(KPhase)-1
          do j=1,nac
            if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
            jj=jj+1
            if(i.eq.1) call SetBasicKeysForAtom(jj)
            Atom(jj)=StLbl(j)
            call uprat(Atom(jj))
            if(KPhase.gt.1) Atom(jj)=Atom(jj)(:idel(Atom(jj)))//
     1                               StPh(:idel(StPh))
            do k=1,MaxNSymm
              isa(k,jj)=k
            enddo
            itf(jj)=1
            ifr(jj)=0
            itfMax=1
            lasmax(jj)=0
            x(i,jj)= pa(j)
            sx(i,jj)=spa(j)
            iswa(jj)=1
            if(jj.eq.1) then
              PrvniKiAtomu(jj)=ndoff
            else
              PrvniKiAtomu(jj)=PrvniKiAtomu(jj-1)+mxda
            endif
            DelkaKiAtomu(jj)=10
          enddo
          NAtIndLen(1,KPhase)=jj-NAtIndFrAll(KPhase)+1
        else
          go to 9999
        endif
        call EM40UpdateAtomLimits
      enddo
      if(NDimI(KPhase).gt.0) then
        do i=1,nac
          if(StLbl(i).eq.'?'.or.StLbl(i).eq.'0') cycle
          call qbyx(x(1,i),qcnt(1,i),1)
        enddo
      endif
      if(NComp(KPhase).gt.1) then
        call CIFGetLoop(CIFKey(33,1),StLbl,MxLoop,CIFKey(52,1),ia,
     1                  Sta,pa,spa,0,n,t256,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.0) then
          do 2400i=1,n
            do j=1,NComp(KPhase)
              if(EqIgCase(Sta(i),IdCode(j))) then
                iswa(i)=j
                call qbyx(x(1,i),qcnt(1,i),j)
                go to 2400
              endif
            enddo
            t256='the item "_atom_site_subsystem_code" for '//
     1           'the atom "'//Atom(i)(:idel(Atom(i)))//'"'
            p256='doesn''t correspond to any of composite subsystems.'
            call FeChybne(-1.,-1.,t256,p256,Warning)
2400      continue
        endif
      endif
      do i=1,NComp(KPhase)
        n=0
        do j=1,nac
          if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
          if(iswa(j).eq.i) n=n+1
        enddo
        NAtIndLen(i,KPhase)=n
      enddo
      do j=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        kswa(j)=KPhase
        call SetRealArrayTo(xp,3,0.)
        call SpecPos(x(1,j),xp,0,iswa(j),.01,nocc)
        ai(j)=1./float(nocc)
        sai(j)=0.
      enddo
      call EM40UpdateAtomLimits
      allocate(aip(nac),saip(nac))
      call SetRealArrayTo(aip,nac,1.)
      call SetRealArrayTo(saip,nac,0.)
      s256=CIFKey(33,1)
      p256=CIFKey(41,1)
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,n,t256,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.eq.-1) then
        call SetRealArrayTo(aip,nac,1.)
        call SetRealArrayTo(saip,nac,0.)
      else
        call CopyVek(pa,aip,nac)
        call CopyVek(spa,saip,nac)
      endif
      p256=CIFKey(41,1)
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.ne.-1) then
        jj=NAtIndFrAll(KPhase)-1
        do i=1,n
          jj=jj+1
          WyckoffSmb(jj)=Sta(i)
        enddo
      endif
      jj=NAtIndFrAll(KPhase)-1
      do i=1,nac
         jj=jj+1
         WyckoffMult(jj)=-1
         if(NDimI(KPhase).le.0) then
           ai(jj)=ai(jj)* aip(i)
          sai(jj)=ai(jj)*saip(i)
        endif
      enddo
      p256=CIFKey(3,4)
      ZShelx=.false.
      call CIFGetSt(p256,sta,mm,t256,ich)
      if(ich.eq.0)
     1 ZShelx=LocateSubstring(sta(1),'SHELX', .false.,.true.).gt.0
      jj=NAtIndFrAll(KPhase)-1
      do ii=1,3
        if(ii.eq.1) then
          p256=CIFKey(44,1)
        else if(ii.eq.2) then
          p256=CIFKey(106,1)
        else if(ii.eq.3) then
          p256=CIFKey(107,1)
        endif
        call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,n,t256,
     1                  ich)
        if(ich.eq.-1) then
          cycle
        else if(ich.eq.2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else
          j=0
          jj=NAtIndFrAll(KPhase)-1
          if(NDimI(KPhase).eq.1) then
            pom=float(NSymmN(KPhase))/float(NSymm(KPhase))
          else
            pom=1.
          endif
          do iat=1,n
            if(StLbl(iat).eq.'?'.or.StLbl(iat).eq.'0') cycle
            jj=jj+1
            WyckoffMult(jj)=nint(pa(iat))
            if(NDimI(KPhase).gt.0) cycle
!            ai(jj)=ai(jj)*aip(iat)
            if(ii.eq.2.or.(ii.eq.1.and..not.ZShelx)) then
               aip(iat)= aip(iat)* pa(iat)/float(FullMultiplicity)
              saip(iat)=saip(iat)*spa(iat)/float(FullMultiplicity)
            else
               aip(iat)= aip(iat)/pa(iat)
              saip(iat)=saip(iat)/pa(iat)**2*spa(iat)
            endif
            if(MagneticType(KPhase).ne.0) then
              aipi=aip(iat)*pom
            else
              aipi=aip(iat)
            endif
            if(abs(ai(jj)-aipi).gt..0001) then
              j=j+1
              if(j.eq.1) then
                t256=Atom(i)
                t256='the item "_atom_site_symmetry_multiplicity" '//
     1               'for the atom "'//Atom(jj)(:idel(Atom(jj)))//'"'
                p256='is not in accordance with the derived site '//
     1               'symmetry.'
                call FeChybne(-1.,-1.,t256,p256,Warning)
              endif
            endif
          enddo
          if(j.gt.0) then
            if(j.gt.1) then
              write(Cislo,FormI15) j
              call Zhusti(Cislo)
              t256='the previous error has occured '//
     1             Cislo(:idel(Cislo))//' times.'
            else
              t256='the previous error has occured just once.'
            endif
            p256='It will not have an influence to subsequest work.'
            call FeChybne(-1.,-1.,t256,p256,Warning)
          endif
          go to 2450
        endif
      enddo
2450  p256=CIFKey(46,1)
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,ich)
      if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
      if(ich.eq.-1) then
        jj=NAtIndFrAll(KPhase)-1
        do i=1,nac
          if(StLbl(i).eq.'?'.or.StLbl(i).eq.'0') cycle
          Sta(i)=' '
          jj=jj+1
          do j=1,idel(Atom(jj))
            znak=Atom(jj)(j:j)
            if(index(Cifry(1:10),znak).gt.0.or.Znak.eq.'('.or.
     1         Znak.eq.'_') go to 2490
            if(j.le.2) then
              Sta(i)(j:j)=znak
            else if(j.eq.3.and.EqIgCase(Sta(i),'wat')) then
              Sta(i)(j:j)='O'
              go to 2490
            else
              Sta(i)=' '
              go to 2490
            endif
          enddo
2490      if(idel(Sta(i)).le.0) Sta(i)=AtType(1,KPhase)
        enddo
      endif
      nfo=NAtFormula(KPhase)
      jj=NAtIndFrAll(KPhase)-1
      do i=1,nac
        if(StLbl(i).eq.'?'.or.StLbl(i).eq.'0') cycle
        jj=jj+1
        call SetRealArrayTo( beta(1,jj),6,0.)
        call SetRealArrayTo(sbeta(1,jj),6,0.)
        sta(i)=sta(i)(1:2)
        call uprat(sta(i))
        if(index(cifry(1:10),sta(i)(2:2)).gt.0.or.sta(i)(2:2).eq.'+'.or.
     1           sta(i)(2:2).eq.'-') sta(i)(2:)=' '
        do j=1,NAtFormula(KPhase)
          if(EqIgCase(sta(i),AtType(j,KPhase))) go to 2530
        enddo
        NAtFormula(KPhase)=NAtFormula(KPhase)+1
        MaxNAtFormula=max(MaxNAtFormula,NAtFormula(KPhase))
        call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
        AtTypeFull(NAtFormula(KPhase),KPhase)=sta(i)
        AtTypeMag(NAtFormula(KPhase),KPhase)=' '
        AtTypeMagJ(NAtFormula(KPhase),KPhase)=' '
        AtVal(NAtFormula(KPhase),KPhase)=0
        CoreValSource(NAtFormula(KPhase),KPhase)='Default'
        call SetRealArrayTo(TypicalDist(1,1,KPhase),
     1                      NAtFormula(KPhase)**2,-1.)
        call GetPureAtType(sta(i),AtType(NAtFormula(KPhase),KPhase))
        call uprat(AtTypeFull(NAtFormula(KPhase),KPhase))
        call uprat(AtType(NAtFormula(KPhase),KPhase))
        AtMult(NAtFormula(KPhase),KPhase)=1.
        call EM50ReadOneFormFactor(NAtFormula(KPhase))
        call EM50OneFormFactorSet(NAtFormula(KPhase))
        j=LocateInStringArray(atn,98,AtType(NAtFormula(KPhase),KPhase),
     1                        IgnoreCaseYes)
        if(j.gt.0) then
          AtNum(NAtFormula(KPhase),KPhase)=float(j)
        else
          AtNum(NAtFormula(KPhase),KPhase)=0.
        endif
        isf(jj)=NAtFormula(KPhase)
        cycle
2530    isf(jj)=j
      enddo
      if(NAtFormula(KPhase).ne.nfo) then
        call SetFormula(Formula(KPhase))
        lpom=isPowder
        call iom50(1,0,fln(:ifln)//'.m50')
        call iom50(0,0,fln(:ifln)//'.m50')
        isPowder=lpom
      endif
      MagPar(NAtIndFrAll(KPhase):NAtIndFrAll(KPhase))=0
      p256=CIFKey(21,1)
      call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,0,n,t256,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.ne.-1) then
        j=0
        do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
          j=j+1
          if(EqIgCase(Sta(j),'dum')) then
            ai(i)=0.
            sai(i)=0.
          endif
        enddo
      endif
      do m=1,2
        if(m.eq.1) then
          s256=CIFKey(48,1)
          pom=episq
        else
          s256=CIFKey(19,1)
          pom=1.
        endif
        call CIFGetLoop(CIFKey(33,1),StLbl,MxLoop,s256,ia,Sta,pa,spa,
     1                  1,n,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        jj=NAtIndFrAll(KPhase)-1
        do i=1,n
          jj=jj+1
          if(ich.eq.0) then
             beta(1,jj)= pa(i)*pom
            sbeta(1,jj)=spa(i)*pom
          endif
        enddo
      enddo
      l=idel(CIFKey(11,1))-2
      PomMx=0.
      n=0
      do m=1,2
        if(m.eq.1) then
          s256=CIFKey(11,1)(:l)
        else
          s256=CIFKey(2,1)(:l)
        endif
        do i=1,6
          if(m.eq.2) then
            if(KlicUsed.eq.ProHonzu) then
              pom=.0001
            else
              pom=1.
            endif
          endif
          call indext(i,j,k)
          write(p256,'(2i1)') j,k
          p256=s256(:l)//p256(1:2)
          call CIFGetLoop(CIFKey(8,1),StLbl,MxLoop,p256,ia,Sta,pa,
     1                    spa,1,nac,t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(KPhase.gt.1) then
            do j=1,nac
              if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
              StLbl(j)=StLbl(j)(:idel(StLbl(j)))//StPh(:idel(StPh))
            enddo
          endif
          do j=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            if(m.eq.1) pom=urcp(i,iswa(j),KPhase)
            do k=1,nac
              if(StLbl(k).eq.'?'.or.StLbl(k).eq.'0') cycle
              if(EqIgCase(StLbl(k),Atom(j))) then
                if(i.eq.1) then
                  itf(j)=2
                  itfMax=2
                endif
                 beta(i,j)= pa(k)*pom
                sbeta(i,j)=spa(k)*pom
                if(i.le.3) then
                  PomMx=PomMx+abs(beta(i,j)/urcp(i,iswa(j),KPhase))
                  n=n+1
                endif
              endif
            enddo
          enddo
        enddo
      enddo
      if(n.gt.0) PomMx=PomMx/float(n)
      if(PomMx.gt..2) then
        do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
          if(itf(i).gt.1) then
            itfMax=2
            do j=1,6
               beta(j,i)= beta(j,i)/episq
              sbeta(j,i)=sbeta(j,i)/episq
            enddo
          endif
        enddo
      endif
      call SetIntArrayTo(kmodn,7,0)
      call SetIntArrayTo(KModAMax,7,0)
      m=81
c      mm=82
      n=3
      do n=3,6
        mm=m+1
        m=mm+1
        do i=1,TRank(n)
          m=m+1
          call CIFGetLoop(CIFKey(mm,22),StLbl,MxLoop,CIFKey(m,22),ia,
     1                    Sta,pa,spa,1,nac,t256,ich)
          if(KPhase.gt.1) then
            do j=1,nac
              if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
              StLbl(j)=StLbl(j)(:idel(StLbl(j)))//StPh(:idel(StPh))
            enddo
          endif
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          do j=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            do k=1,nac
              if(StLbl(k).eq.'?'.or.StLbl(k).eq.'0') cycle
              if(EqIgCase(StLbl(k),Atom(j))) then
                if(i.eq.1) then
                  itf(j)=n
                  itfMax=max(itfMax,n)
                  call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,
     1                                      0)
                endif
                if(n.eq.3) then
                   c3(i,j)= pa(k)
                  sc3(i,j)=spa(k)
                else if(n.eq.4) then
                   c4(i,j)= pa(k)
                  sc4(i,j)=spa(k)
                else if(n.eq.5) then
                   c5(i,j)= pa(k)
                  sc5(i,j)=spa(k)
                else if(n.eq.6) then
                   c6(i,j)= pa(k)
                  sc6(i,j)=spa(k)
                endif
              endif
            enddo
          enddo
        enddo
      enddo
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        ifr(i)=0
        TypeModFun(i)=0
        call SetIntArrayTo(KFA  (1,i),7,0)
        call SetIntArrayTo(KModA(1,i),7,0)
        call SetIntArrayTo(KModAO(1,i),7,0)
        call SetIntArrayTo(KiA(1,i),mxda,0)
      enddo
      call SetIntArrayTo(KModMMax,3,0)
      if(NDimI(KPhase).le.0) go to 5000
      call SetRealArrayTo(qup,3*mxw,0.)
      n=1
      do i=1,3
        call CIFGetLoop(CIFKey(64,n),StLbl,MxLoop,CIFKey(64+i,n),ia,
     1                  Sta,pa,spa,1,nn,t256,ich)
        if(ich.eq.-1) then
          if(i.eq.1) then
            go to 3150
          else
            cycle
          endif
        else if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        endif
        jj=0
        do j=1,nn
          if(StLbl(j).eq.'?') cycle
          jj=jj+1
          StLbl(jj)=StLbl(j)
          qup(i,jj)=pa(j)
        enddo
        nn=jj
      enddo
      kwi=0
      imn(:NDimI(KPhase))=-mxw
      imn(NDimI(KPhase)+1:)=0
      imx(:NDimI(KPhase))= mxw
      imx(NDimI(KPhase)+1:)=0
      m=NDimI(KPhase)
      do 3100i=1,nn
        mrtl=999999
        mrto=999999
        do isw=1,NComp(KPhase)
          do i1=imn(1),imx(1)
            mrt(1)=i1
            do i2=imn(2),imx(2)
              mrt(2)=i2
              do 3050i3=imn(3),imx(3)
                mrt(3)=i3
                do j=1,3
                  pa(j)=-qup(j,i)
                  do k=1,NDimI(KPhase)
                    pa(j)=pa(j)+float(mrt(k))*qu(j,k,isw,KPhase)
                  enddo
                  if(abs(anint(pa(j))-pa(j)).gt..0001) go to 3050
                enddo
                do j=2,NLattVec(KPhase)
                  pom=ScalMul(pa,vt6(1,j,isw,KPhase))
                  if(abs(anint(pom)-pom).gt..0001) go to 3050
                enddo
                j=iabs(i1)+iabs(i2)+iabs(i3)
                if(j.gt.mrtl) go to 3050
                mrtl=j
                call CopyVekI(mrt,mrto,3)
3050          continue
            enddo
          enddo
        enddo
        if(mrtl.lt.999999) then
          k=0
          call StToInt(StLbl(i),k,ia,1,.false.,ich)
          if(ich.ne.0) then
            call CIFSyntaxError(CIFLastReadRecord)
            go to 3100
          endif
          do k=1,m
            if(eqiv(mrto,kw(1,k,KPhase),3)) then
              kwi(ia(1))=k
              go to 3100
            endif
            if(eqiv(mrto,kw(1,k,KPhase),3)) then
              kwi(ia(1))=k
              go to 3100
            else if(eqivm(mrto,kw(1,k,KPhase),3)) then
              kwi(ia(1))=-k
              go to 3100
            endif
          enddo
          m=m+1
          call CopyVekI(mrto,kw(1,m,KPhase),3)
          kwi(ia(1))=k
          go to 3100
        endif
        write(s256,'(3f10.4)') qup(1:3,i)
        call ZdrcniCisla(s256,3)
        do j=1,idel(s256)
          if(s256(j:j).eq.' ') s256(j:j)=','
        enddo
        s256='The wave vector ('//s256(:idel(s256))//') is not an '//
     1       'integer combination of modulation vectors'
        call FeChybne(-1.,-1.,s256,' ',SeriousError)
        CIFFlag=1
3100  continue
      go to 3200
3150  call SetIntArrayTo(kwp,3*mxw,0)
      kwi=0
      kk=0
3160  if(kk.eq.0) then
        s256=CifKey(64,n)
        ik=52
        im=22
      else
        ik=144
        im=1
      endif
      do i=1,NDimI(KPhase)
        call CIFGetLoop(s256,StLbl,MxLoop,CIFKey(ik+i,im),ia,
     1                  Sta,pa,spa,-1,nn,t256,ich)
        if(kk.eq.0) then
          kk=kk+1
          go to 3160
        endif
        if(ich.ne.0) then
          do j=1,mxw
            kwi(j)=j
          enddo
          go to 3200
        endif
        jj=0
        do j=1,nn
          if(StLbl(j).eq.'?') cycle
          jj=jj+1
          StLbl(jj)=StLbl(j)
          kwp(i,jj)=ia(j)
        enddo
        nn=jj
      enddo
      ii=0
      do i=1,nn
        k=0
        call kus(StLbl(i),k,Cislo)
        call posun(Cislo,0)
        read(Cislo,FormI15) k
        if(k.eq.0) cycle
        ii=ii+1
        do j=1,NDimI(KPhase)
          kw(j,k,KPhase)=kwp(j,i)
        enddo
        kwi(ii)=k
      enddo
3200  n=22
      do i=1,3
        call CIFGetLoop(CIFKey(49,n),StLbl,MxLoop,CIFKey(49+i,n),ia,
     1                  Sta,pa,spa,1,nn,t256,ich)
        if(i.eq.1) then
          norp=nn
          allocate(OrX40P(norp),OrDelP(norp),OrEpsP(norp))
          call SetRealArrayTo(OrX40P,norp,0.)
          call SetRealArrayTo(OrDelP,norp,1.)
          call SetRealArrayTo(OrEpsP,norp,.95)
          call CopyVek(pa,OrX40P,nn)
        else if(i.eq.2) then
          call CopyVek(pa,OrDelP,min(nn,norp))
        else if(i.eq.3) then
          call CopyVek(pa,OrEpsP,min(nn,norp))
        endif
      enddo
      do j=1,2
        call CIFGetLoop(CIFKey(103,1),StLbl,MxLoop,CIFKey(103+j,1),
     1                  ia,Sta,pa,spa,1,n,t256,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.-1) then
          go to 3320
        else
          do i=1,n
            if(KPhase.gt.1)
     1        StLbl(i)=StLbl(i)(:idel(StLbl(i)))//StPh(:idel(StPh))
            iat=ktat(atom,NAtCalc,StLbl(i))
            if(iat.le.0) cycle
            if(j.eq.1) kmodn(1)=max(KModA(1,iat),1)+1
            call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
            KModP=KModA(1,iat)
            if(j.eq.1) then
              ax(KModP+1,iat)=pa(i)
              ay(KModP+1,iat)=0.
              sax(KModP+1,iat)=0.
              say(KModP+1,iat)=0.
            else if(j.eq.2) then
              KFA(1,iat)=1
              KModA(1,iat)=KModP+1
               a0(iat)=pa(i)
              sa0(iat)=0.
            endif
          enddo
        endif
      enddo
3320  n=0
      call SetIntArrayTo(il,10,0)
      do i=1,9
        call CIFGetSt(CifKey(i+53,1),sta,mm,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(ich.gt.0)  then
          n=n+1
          il(ich)=i
        endif
      enddo
      call CIFBeginLoop(m,CIFKey(54,1),p256,t256,ich)
      if(ich.eq.-1) go to 3500
      call SetIntArrayTo(iwa,20,0)
3350  k=0
      it=0
      iw=0
      do 3400i=1,n
        call kus(p256,k,t256)
        if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 3500
        j=il(i)
        if(j.eq.5.or.j.eq.7.or.j.eq.8.or.j.eq.9) then
          call GetRealEsdFromSt(t256,pom,spom,ich)
          if(ich.ne.0) cycle
        endif
        if(j.eq.1) then
          call uprat(t256)
          if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,t256)
        else if(j.eq.2) then
          call mala(t256)
          do ix=1,3
            if(EqIgCase(t256,Smbx(ix))) go to 3400
          enddo
          ix=0
          if(t256(1:1).ne.'?') then
            ich=-2
            t256=p256
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        else if(j.eq.5) then
          pcos=pom
          scos=spom
          it=0
        else if(j.eq.7) then
          pmod=pom
          smod=spom
          it=1
        else if(j.eq.8) then
          pfaz=pom
          sfaz=spom
          it=1
        else if(j.eq.9) then
          psin=pom
          ssin=spom
          it=0
        else if(j.eq.4) then
          Cislo=t256
          call posun(Cislo,0)
          read(Cislo,FormI15,err=3360) iw
          if(iw.gt.0) then
            iw=kwi(iw)
          else
            iw=0
          endif
          cycle
3360      iw=-999
        endif
3400  continue
      if(ix.gt.0.and.iw.gt.-900) then
        iwabs=iabs(iw)
        kmodn(2)=max(KModA(2,iat),iwabs)+1
        call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
        KModA(2,iat)=kmodn(2)-1
        if(iwa(iwabs).le.0) then
          do i=1,NAtCalc
            call SetRealArrayTo( ux(1,iwabs,i),3,0.)
            call SetRealArrayTo( uy(1,iwabs,i),3,0.)
            call SetRealArrayTo(sux(1,iwabs,i),3,0.)
            call SetRealArrayTo(suy(1,iwabs,i),3,0.)
          enddo
          iwa(iwabs)=1
        endif
        call CIFSetMod(ux(ix,iwabs,iat),uy(ix,iwabs,iat),
     1                 sux(ix,iwabs,iat),suy(ix,iwabs,iat),
     2                 psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
        if(iw.lt.0) ux(ix,iw,iat)=-ux(ix,iw,iat)
      else if(iw.eq.0.and.iat.gt.0) then
        if(KFA(1,iat).eq.1) then
          if(TypeModFun(iat).ne.1) then
            DelejOrtho=.true.
            ifun=0
            do i=1,norp
              if(abs(a0(iat)-OrDelP(i)).lt..0001.and.
     1           abs(ax(1,iat)-OrX40P(i)).lt..0001) then
                ifun=i
                TypeModFun(iat)=1
                OrthoX40(iat)=OrX40P(ifun)
                OrthoDelta(iat)=OrDelP(ifun)
                OrthoEps(iat)=OrEpsP(ifun)
                exit
              endif
            enddo
          endif
          if(TypeModFun(iat).eq.1) then
            x(ix,iat)=x(ix,iat)+pcos
          endif
        endif
      else if(iw.lt.-900.and.iat.gt.0) then
        p256='The wave vector not properly defined'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      else if(ix.eq.0.and.iat.gt.0) then
        p256='Unknown modulation component'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      endif
      call CIFNewRecord(m,p256,*4300)
      go to 3350
3500  do 3600ityp=1,2
        if(ityp.eq.1) then
          ilm=4
          ilp=24
        else
          ilm=5
          ilp=35
        endif
        n=0
        call SetIntArrayTo(il,10,0)
        do i=1,ilm
          call CIFGetSt(CifKey(ilp+i-1,22),sta,mm,t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(ich.gt.0)  then
            n=n+1
            il(ich)=i
            if(ityp.eq.1.and.i.gt.2) il(ich)=il(ich)+1
          endif
        enddo
        call CIFBeginLoop(m,CIFKey(ilp,22),p256,t256,ich)
        if(ich.eq.-1) go to 3600
3550    k=0
        iwp=0
        ifun=0
        do 3590i=1,n
          call kus(p256,k,t256)
          if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 3600
          j=il(i)
          if(j.eq.1) then
            call uprat(t256)
            if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
            iat=ktat(atom,NAtCalc,t256)
          else if(j.eq.2) then
            do ix=1,3
              if(EqIgCase(t256,Smbx(ix))) go to 3590
            enddo
            ix=0
            if(t256(1:1).ne.'?') then
              ich=-2
              t256=p256
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          else if(j.eq.3) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=3560) ifun
            cycle
3560        ifun=0
          else if(j.eq.4) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=3570) iwp
            cycle
3570        iwp=0
          else if(j.eq.5) then
            call GetRealEsdFromSt(t256,pom,spom,ich)
            if(ich.ne.0) cycle
          endif
3590    continue
        if(ix.gt.0.and.iwp.gt.0) then
          iw=(iwp-1)/2+1
          kmodn(2)=max(KModA(2,iat),iw)+1
          call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
          KModA(2,iat)=kmodn(2)-1
          if(ityp.eq.1) then
            TypeModFun(iat)=2
          else
            TypeModFun(iat)=1
            OrthoX40(iat)=OrX40P(ifun)
            OrthoDelta(iat)=OrDelP(ifun)
            OrthoEps(iat)=OrEpsP(ifun)
          endif
          if(iwa(iw).le.0) then
            do i=1,NAtCalc
              call SetRealArrayTo( ux(1,iw,i),3,0.)
              call SetRealArrayTo( uy(1,iw,i),3,0.)
              call SetRealArrayTo(sux(1,iw,i),3,0.)
              call SetRealArrayTo(suy(1,iw,i),3,0.)
            enddo
            iwa(iw)=1
          endif
          if(mod(iwp,2).eq.1) then
             ux(ix,iw,iat)= pom
            sux(ix,iw,iat)=spom
          else
             uy(ix,iw,iat)= pom
            suy(ix,iw,iat)=spom
          endif
        endif
        call CIFNewRecord(m,p256,*4300)
        go to 3550
3600  continue
      call SetIntArrayTo(il,10,0)
      n=0
      do i=1,9
        call CIFGetSt(CifKey(i+87,1),sta,mm,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(ich.gt.0)  then
          n=n+1
          il(ich)=i
        endif
      enddo
      call CIFBeginLoop(m,CIFKey(88,1),p256,t256,ich)
      if(ich.eq.-1) go to 3900
      call SetIntArrayTo(iwa,20,0)
3750  k=0
      it=0
      iw=0
      do 3800i=1,n
        call kus(p256,k,t256)
        if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 3900
        j=il(i)
        if(j.eq.5.or.j.eq.7.or.j.eq.8.or.j.eq.9) then
          call GetRealEsdFromSt(t256,pom,spom,ich)
          if(ich.ne.0) cycle
        endif
        if(j.eq.1) then
          call uprat(t256)
          if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,t256)
        else if(j.eq.5) then
          pcos=pom
          scos=spom
          it=0
        else if(j.eq.7) then
          pmod=pom
          smod=spom
          it=1
        else if(j.eq.8) then
          pfaz=pom
          sfaz=spom
          it=1
        else if(j.eq.9) then
          psin=pom
          ssin=spom
          it=0
        else if(j.eq.3) then
          do ib=1,6
            if(EqIgCase(t256,Smbu(ib))) go to 3800
          enddo
          ib=0
          if(t256(1:1).ne.'?') then
            ich=-2
            t256=p256
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        else if(j.eq.4) then
          Cislo=t256
          call posun(Cislo,0)
          read(Cislo,FormI15,err=3760) iw
          if(iw.gt.0) then
            iw=kwi(iw)
          else
            iw=0
          endif
          cycle
3760      iw=-999
        endif
3800  enddo
      if(ib.gt.0.and.iw.gt.-900) then
        iwabs=iabs(iw)
        pom=urcp(ib,iswa(iat),KPhase)
        if(it.eq.0) then
          psin=psin*pom
          pcos=pcos*pom
          ssin=ssin*pom
          scos=scos*pom
        else
          pmod=pmod*pom
          smod=smod*pom
        endif
        kmodn(3)=max(KModA(3,iat),iwabs)+1
        call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
        KModA(3,iat)=kmodn(3)-1
        if(iwa(iwabs).le.0) then
          do i=1,NAtCalc
            call SetRealArrayTo( bx(1,iwabs,iat),6,0.)
            call SetRealArrayTo( by(1,iwabs,iat),6,0.)
            call SetRealArrayTo(sbx(1,iwabs,iat),6,0.)
            call SetRealArrayTo(sby(1,iwabs,iat),6,0.)
          enddo
          iwa(iwabs)=1
        endif
        call CIFSetMod(bx(ib,iwabs,iat),by(ib,iwabs,iat),
     1                 sbx(ib,iwabs,iat),sby(ib,iwabs,iat),
     2                 psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
        if(iw.lt.0) bx(ib,iwabs,iat)=-bx(ib,iwabs,iat)
      else if(iw.eq.0.and.iat.gt.0) then
        if(KFA(1,iat).eq.1) then
          if(TypeModFun(iat).ne.1) then
            DelejOrtho=.true.
            ifun=0
            do i=1,norp
              if(abs(a0(iat)-OrDelP(i)).lt..0001.and.
     1           abs(ax(1,iat)-OrX40P(i)).lt..0001) then
                ifun=i
                TypeModFun(iat)=1
                OrthoX40(iat)=OrX40P(ifun)
                OrthoDelta(iat)=OrDelP(ifun)
                OrthoEps(iat)=OrEpsP(ifun)
                exit
              endif
            enddo
          endif
          if(TypeModFun(iat).eq.1) then
            write(t256,'(i5,3f10.6)')
             beta(ib,iat)=beta(ib,iat)+pcos*urcp(ib,iswa(iat),KPhase)
          endif
        endif
      else if(iw.lt.-900.and.iat.gt.0) then
        p256='The wave vector not properly defined'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      else if(ib.eq.0.and.iat.gt.0) then
        p256='Unknown modulation component'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      endif
      call CIFNewRecord(m,p256,*4300)
      go to 3750
3900  do 4000ityp=1,2
        if(ityp.eq.1) then
          ilm=4
          ilp=31
        else
          ilm=5
          ilp=44
        endif
        n=0
        call SetIntArrayTo(il,10,0)
        do i=1,ilm
          call CIFGetSt(CifKey(ilp+i-1,22),sta,mm,t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(ich.gt.0)  then
            n=n+1
            il(ich)=i
            if(ityp.eq.1.and.i.gt.2) il(ich)=il(ich)+1
          endif
        enddo
        write(Cislo,'(i4)') n
        call CIFBeginLoop(m,CIFKey(ilp,22),p256,t256,ich)
        if(ich.eq.-1) go to 4100
3950    k=0
        iwp=0
        ifun=0
        do 3990i=1,n
          call kus(p256,k,t256)
          if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 4000
          j=il(i)
          if(j.eq.1) then
            call uprat(t256)
            if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
            iat=ktat(Atom,NAtCalc,t256)
          else if(j.eq.2) then
            do ib=1,6
              if(EqIgCase(t256,Smbu(ib))) go to 3990
            enddo
            ib=0
            if(t256(1:1).ne.'?') then
              ich=-2
              t256=p256
              call CIFSyntaxError(CIFLastReadRecord)
            endif
          else if(j.eq.3) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=3960) ifun
            cycle
3960        ifun=0
          else if(j.eq.4) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=3970) iwp
            cycle
3970        iwp=0
          else if(j.eq.5) then
            call GetRealEsdFromSt(t256,pom,spom,ich)
            if(ich.ne.0) cycle
          endif
3990    enddo
        if(ib.gt.0.and.iwp.gt.0) then
          iw=(iwp-1)/2+1
          kmodn(3)=max(KModA(3,iat),iw)+1
          call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
          KModA(3,iat)=kmodn(3)-1
          if(ityp.eq.1) then
            TypeModFun(iat)=2
          else
            TypeModFun(iat)=1
            OrthoX40(iat)=OrX40P(ifun)
            OrthoDelta(iat)=OrDelP(ifun)
            OrthoEps(iat)=OrEpsP(ifun)
          endif
          if(iwa(iw).le.0) then
            do i=1,NAtCalc
              call SetRealArrayTo( bx(1,iw,i),3,0.)
              call SetRealArrayTo( by(1,iw,i),3,0.)
              call SetRealArrayTo(sbx(1,iw,i),3,0.)
              call SetRealArrayTo(sby(1,iw,i),3,0.)
            enddo
            iwa(iw)=1
          endif
          pomp=urcp(ib,iswa(iat),KPhase)
          if(mod(iwp,2).eq.1) then
             bx(ib,iw,iat)= pom*pomp
            sbx(ib,iw,iat)=spom*pomp
          else
             by(ib,iw,iat)= pom*pomp
            sby(ib,iw,iat)=spom*pomp
          endif
        endif
        call CIFNewRecord(m,p256,*4300)
        go to 3950
4000  continue
4100  call CIFGetLoop(CIFKey(200,22),StLbl,MxLoop,CIFKey(201,22),
     1                  ia,Sta,pa,spa,1,n,t256,ich)
      if(ich.eq.-2) then
        call CIFSyntaxError(CIFLastReadRecord)
      else if(ich.eq.-1) then
        do i=1,NAtCalc
          if(KFA(1,i).le.0) a0(i)=-3333.
        enddo
        go to 4150
      else
        do i=1,n
          if(KPhase.gt.1)
     1      StLbl(i)=StLbl(i)(:idel(StLbl(i)))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,StLbl(i))
          if(iat.le.0) cycle
            a0(iat)= pa(i)
           sa0(iat)=spa(i)
        enddo
      endif
4150  n=0
      call SetIntArrayTo(il,10,0)
      do i=1,8
        call CIFGetSt(CifKey(i+67,1),sta,mm,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(ich.gt.0)  then
          n=n+1
          il(ich)=i
        endif
      enddo
      call CIFBeginLoop(m,CIFKey(68,1),p256,t256,ich)
      if(ich.eq.-1) go to 4300
      call SetIntArrayTo(iwa,20,0)
4200  k=0
      it=0
      iw=0
      do i=1,n
        call kus(p256,k,t256)
        if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 4300
        j=il(i)
        if(j.eq.4.or.j.eq.6.or.j.eq.7.or.j.eq.8) then
          call GetRealEsdFromSt(t256,pom,spom,ich)
          if(ich.ne.0) cycle
        endif
        if(j.eq.1) then
          call uprat(t256)
          if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,t256)
        else if(j.eq.4) then
          pcos=pom
          scos=spom
          it=0
        else if(j.eq.6) then
          pmod=pom
          smod=spom
          it=1
        else if(j.eq.7) then
          pfaz=pom
          sfaz=spom
          it=1
        else if(j.eq.8) then
          psin=pom
          ssin=spom
          it=0
        else if(j.eq.3) then
          Cislo=t256
          call posun(Cislo,0)
          read(Cislo,FormI15,err=4240) iw
          if(iw.gt.0) then
            iw=kwi(iw)
          else
            iw=0
          endif
          cycle
4240      iw=-999
        endif
      enddo
      if(iw.gt.-900) then
        iwabs=iabs(iw)
        kmodn(1)=max(KModA(1,iat),iwabs)+1
        call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
        KModA(1,iat)=kmodn(1)-1
        if(iwa(iw).le.0) then
          do i=1,NAtCalc
            if(KFA(1,i).ne.0) cycle
            call SetRealArrayTo( ax(iwabs,i),1,0.)
            call SetRealArrayTo( ay(iwabs,i),1,0.)
            call SetRealArrayTo(sax(iwabs,i),1,0.)
            call SetRealArrayTo(say(iwabs,i),1,0.)
          enddo
          iwa(iw)=1
        endif
        call CIFSetMod(ax(iwabs,iat),ay(iwabs,iat),
     1                 sax(iwabs,iat),say(iwabs,iat),
     1                 psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
        if(iw.lt.0) ax(iwabs,iat)=-ax(iwabs,iat)
      else if(iw.eq.0.and.iat.gt.0) then
        p256='The wave vector not properly defined'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      endif
      call CIFNewRecord(m,p256,*4300)
      go to 4200
4300  do j=1,5
        call CIFGetLoop(CIFKey(97,1),StLbl,MxLoop,CIFKey(97+j,1),ia,
     1                  Sta,pa,spa,1,n,t256,ich)
        if(ich.eq.-2) then
          call CIFSyntaxError(CIFLastReadRecord)
        else if(ich.eq.-1) then
          exit
        else
          do i=1,n
            if(KPhase.gt.1)
     1        StLbl(i)=StLbl(i)(:idel(StLbl(i)))//StPh(:idel(StPh))
            iat=ktat(atom,NAtCalc,StLbl(i))
            if(iat.le.0) cycle
            if(j.le.3) then
               ux(j,KModA(2,iat)+1,iat)=pa(i)
              sux(j,KModA(2,iat)+1,iat)=0.
            else if(j.eq.4) then
               uy(1,KModA(2,iat)+1,iat)=pa(i)
              sux(1,KModA(2,iat)+1,iat)=0.
            else if(j.eq.5) then
              KFA(2,iat)=1
              KModA(2,iat)=KModA(2,iat)+1
               uy(2,KModA(2,iat),iat)=pa(i)
              suy(2,KModA(2,iat),iat)=0.
            endif
          enddo
        endif
      enddo
      j=0
      do i=1,NAtCalc
        call SetRealArrayTo(xp,3,0.)
        k=0
        if(NDimI(KPhase).eq.1) then
          if(KFA(1,i).ne.0) then
            xp(1)=ax(KModA(1,i),i)
            k=1
          else if(KFA(2,i).ne.0) then
            xp(1)=uy(1,KModA(2,i),i)
            k=1
          endif
        endif
        call SpecPos(x(1,i),xp,k,iswa(i),.01,nocc)
        aipi=aip(i)
        ai(i)=1./float(nocc)*aip(i)
        sai(i)=1./float(nocc)*saip(i)
        if(WyckoffMult(i).le.0) WyckoffMult(i)=FullMultiplicity/nocc
        aip(i)=aip(i)*float(WyckoffMult(i))/float(FullMultiplicity)
        if(abs(ai(i)-aip(i)).gt..0001) then
          j=j+1
          if(j.eq.1) then
            t256=Atom(i)
            t256='the item "_atom_site_symmetry_multiplicity" for '//
     1           'the atom "'//Atom(i)(:idel(Atom(i)))//'"'
            p256='is not in accordance with the derived site '//
     1           'symmetry.'
            call FeChybne(-1.,-1.,t256,p256,Warning)
          endif
        endif
      enddo
      if(j.gt.0) then
        if(j.gt.1) then
          write(Cislo,FormI15) j
          call Zhusti(Cislo)
          t256='the previous error has occured '//Cislo(:idel(Cislo))//
     1         ' times.'
        else
          t256='the previous error has occured just once.'
        endif
        p256='It will not have an influence to subsequest work.'
        call FeChybne(-1.,-1.,t256,p256,Warning)
      endif
      ik=163
      do nn=3,6
        Znak=char(ichar('C')+nn-3)
        call SetIntArrayTo(il,10,0)
        n=0
        do i=1,9
          call CIFGetSt(CifKey(i+ik,22),sta,mm,t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(ich.gt.0)  then
            n=n+1
            il(ich)=i
          endif
        enddo
        call CIFBeginLoop(m,CIFKey(ik+1,22),p256,t256,ich)
        if(ich.eq.-1) cycle
        call SetIntArrayTo(iwa,20,0)
        ik=ik+9
4420    k=0
        it=0
        do 4500i=1,n
          call kus(p256,k,t256)
          if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 4600
          j=il(i)
          if(j.eq.5.or.j.eq.7.or.j.eq.8.or.j.eq.9) then
            call GetRealEsdFromSt(t256,pom,spom,ich)
            if(ich.ne.0) cycle
          endif
          if(j.eq.1) then
            call uprat(t256)
            if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
            iat=ktat(atom,NAtCalc,t256)
          else if(j.eq.5) then
            pcos=pom
            scos=spom
            it=0
          else if(j.eq.7) then
            pmod=pom
            smod=spom
            it=1
          else if(j.eq.8) then
            pfaz=pom
            sfaz=spom
            it=1
          else if(j.eq.9) then
            psin=pom
            ssin=spom
            it=0
          else if(j.eq.3) then
            if(.not.EqIgCase(t256(1:1),Znak)) then
              ib=0
              go to 4500
            endif
            ip=0
            do l=2,nn+1
              read(t256(l:l),102,err=4500) kk
              if(kk.lt.1.or.kk.gt.3) go to 4500
              ip=ip*4+kk
            enddo
            ib=nindc(ip,nn)
          else if(j.eq.4) then
            Cislo=t256
            call posun(Cislo,0)
            read(Cislo,FormI15,err=4440) iw
            if(iw.gt.0) then
              iw=kwi(iw)
            else
              iw=0
            endif
            cycle
4440        iw=-999
          endif
4500    continue
        if(ib.gt.0.and.iw.gt.-900) then
          iwabs=iabs(iw)
          kmodn(nn+1)=max(KModA(nn+1,iat),iwabs)+1
          call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,0)
          KModA(nn+1,iat)=kmodn(nn+1)-1
          if(iwa(iwabs).le.0) then
            do i=1,NAtCalc
              if(nn.eq.3) then
                call SetRealArrayTo( c3x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo( c3y(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc3x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc3y(1,iwabs,i),TRank(nn),0.)
              else if(nn.eq.4) then
                call SetRealArrayTo( c4x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo( c4y(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc4x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc4y(1,iwabs,i),TRank(nn),0.)
              else if(nn.eq.5) then
                call SetRealArrayTo( c5x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo( c5y(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc5x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc5y(1,iwabs,i),TRank(nn),0.)
              else if(nn.eq.6) then
                call SetRealArrayTo( c6x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo( c6y(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc6x(1,iwabs,i),TRank(nn),0.)
                call SetRealArrayTo(sc6y(1,iwabs,i),TRank(nn),0.)
              endif
            enddo
            iwa(iw)=1
          endif
          if(nn.eq.3) then
            call CIFSetMod( c3x(ib,iwabs,iat), c3y(ib,iwabs,iat),
     1                     sc3x(ib,iwabs,iat),sc3y(ib,iwabs,iat),
     2                     psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
            if(iw.lt.0) c3x(1,iwabs,i)=-c3x(1,iwabs,i)
          else if(nn.eq.4) then
            call CIFSetMod( c4x(ib,iwabs,iat), c4y(ib,iwabs,iat),
     1                     sc4x(ib,iwabs,iat),sc4y(ib,iwabs,iat),
     2                     psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
            if(iw.lt.0) c4x(1,iwabs,i)=-c4x(1,iwabs,i)
          else if(nn.eq.5) then
            call CIFSetMod( c5x(ib,iwabs,iat), c5y(ib,iwabs,iat),
     1                     sc5x(ib,iwabs,iat),sc5y(ib,iwabs,iat),
     2                     psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
            if(iw.lt.0) c5x(1,iwabs,i)=-c5x(1,iwabs,i)
          else if(nn.eq.6) then
            call CIFSetMod( c6x(ib,iwabs,iat), c6y(ib,iwabs,iat),
     1                     sc6x(ib,iwabs,iat),sc6y(ib,iwabs,iat),
     2                     psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
            if(iw.lt.0) c6x(1,iwabs,i)=-c6x(1,iwabs,i)
          endif
        else if(iw.eq.0.and.iat.gt.0) then
          p256='The wave vector not properly defined'
          call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
        else if(ib.eq.0.and.iat.gt.0) then
          p256='Unknown modulation component'
          call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
        endif
        call CIFNewRecord(m,p256,*5000)
        go to 4420
4600  enddo
      ik=107
      im=1
      il=0
      n=0
      do i=1,6
        call CIFGetSt(CifKey(ik+i,im),sta,mm,t256,ich)
        if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
        if(ich.gt.0)  then
          n=n+1
          il(ich)=i
        endif
      enddo
      call CIFBeginLoop(m,CIFKey(ik+1,im),p256,t256,ich)
      if(ich.eq.-1) go to 5000
      iwa=0
      kmodp=0
4620  k=0
      it=0
      do 4700i=1,n
        call kus(p256,k,t256)
        if(t256(1:1).eq.'_'.or.EqIgCase(t256,'loop_')) go to 5000
        j=il(i)
        if(j.eq.4.or.j.eq.5) then
          call GetRealEsdFromSt(t256,pom,spom,ich)
          if(ich.ne.0) cycle
        endif
        if(j.eq.1) then
          call uprat(t256)
          if(KPhase.gt.1) t256=t256(:idel(t256))//StPh(:idel(StPh))
          iat=ktat(atom,NAtCalc,t256)
        else if(j.eq.2) then
          do ix=1,3
            if(EqIgCase(t256,Smbx(ix))) go to 4700
          enddo
          ix=0
          if(t256(1:1).ne.'?') then
            ich=-2
            t256=p256
            call CIFSyntaxError(CIFLastReadRecord)
          endif
        else if(j.eq.3) then
          Cislo=t256
          call posun(Cislo,0)
          read(Cislo,FormI15,err=4640) iw
          if(iw.gt.0) then
            iw=kwi(iw)
          else
            iw=0
          endif
          cycle
4640      iw=-999
        else if(j.eq.4) then
          pcos=pom
          scos=spom
          it=0
        else if(j.eq.5) then
          psin=pom
          ssin=spom
          it=0
        endif
4700  continue
      if(ix.gt.0.and.iw.gt.-900) then
        iwabs=iabs(iw)
        kmodp=max(kmodp,iwabs+1)
        call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,kmodp)
        MagPar(iat)=max(MagPar(iat),iwabs+1)
        if(iwa(iw).le.0) then
          do i=1,NAtCalc
            call SetRealArrayTo( smx(1,iwabs,i),3,0.)
            call SetRealArrayTo( smy(1,iwabs,i),3,0.)
            call SetRealArrayTo(ssmx(1,iwabs,i),3,0.)
            call SetRealArrayTo(ssmy(1,iwabs,i),3,0.)
          enddo
          iwa(iw)=1
        endif
        call CIFSetMod( smx(ix,iwabs,iat), smy(ix,iwabs,iat),
     1                 ssmx(ix,iwabs,iat),ssmy(ix,iwabs,iat),
     2                 psin,pcos,ssin,scos,pmod,pfaz,smod,sfaz,it)
        pom=1./CellPar(ix,iswa(iat),KPhase)
         smx(ix,iwabs,iat)= smx(ix,iwabs,iat)*pom
         smy(ix,iwabs,iat)= smy(ix,iwabs,iat)*pom
        ssmx(ix,iwabs,iat)=ssmx(ix,iwabs,iat)*pom
        ssmy(ix,iwabs,iat)=ssmx(ix,iwabs,iat)*pom
        if(iw.lt.0) smx(ix,iwabs,iat)=-smx(ix,iwabs,iat)
      else if(iw.eq.0.and.iat.gt.0) then
        p256='The wave vector not properly defined'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      else if(ix.eq.0.and.iat.gt.0) then
        p256='Unknown modulation component'
        call FeChybne(-1.,-1.,CIFLastReadRecord,p256,SeriousError)
      endif
      call CIFNewRecord(m,p256,*5000)
      go to 4620
5000  if(DelejOrtho) then
        MaxUsedKwAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh.or.NDim(KPh).le.3) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
        enddo
        OrthoOrd=2*MaxUsedKwAll+1
        if(allocated(OrthoSel)) deallocate(OrthoSel)
        allocate(OrthoSel(OrthoOrd,NAtCalc))
        if(allocated(OrthoMat)) deallocate(OrthoMat,OrthoMatI)
        m=OrthoOrd**2
        allocate(OrthoMat(m,NAtCalc),OrthoMatI(m,NAtCalc))
        do i=1,NAtCalc
          if(kswa(i).ne.KPhase.or.NDim(KPhase).le.3.or.
     1       KFA(1,i).ne.1.or.TypeModFun(i).ne.1) cycle
          call UpdateOrtho(i,OrthoX40(i),OrthoDelta(i),OrthoEps(i),
     1                     OrthoSel(1,i),OrthoOrd)
          j=1
5100      if(OrthoSel(j,i).le.2*KModA(2,i)) then
            j=j+1
            go to 5100
          endif
          kmodao(2,i)=(j-1)/2
          j=1
5200      if(OrthoSel(j,i).le.2*KModA(3,i)) then
            j=j+1
            go to 5200
          endif
          kmodao(3,i)=(j-1)/2
          call MatOr(OrthoX40(i),OrthoDelta(i),OrthoSel(1,i),OrthoOrd,
     1               OrthoMat(1,i),OrthoMatI(1,i),i)
        enddo
        SwitchedToHarm=.true.
        call trortho(1)
      endif
      if(MagneticType(KPhase).gt.0) then
        s256=CIFKey(116,1)
        do i=1,3
          p256=CIFKey(i+122,1)
          call CIFGetLoop(s256,StLbl,MxLoop,p256,ia,Sta,pa,spa,1,nac,
     1                    t256,ich)
          if(ich.eq.-2) call CIFSyntaxError(CIFLastReadRecord)
          if(KPhase.gt.1) then
            do j=1,nac
              if(StLbl(j).eq.'?'.or.StLbl(j).eq.'0') cycle
              StLbl(j)=StLbl(j)(:idel(StLbl(j)))//StPh(:idel(StPh))
            enddo
          endif
          do j=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
            isfj=isf(j)
            do k=1,nac
              isw=iswa(j)
              if(StLbl(k).eq.'?'.or.StLbl(k).eq.'0') cycle

!              if(EqIgCase(StLbl(k),Atom(j))) then
!              Zatim !!!!

              if(EqIgCase(StLbl(k),Atom(j)).and.
     1           .not.EqIgCase(AtType(isfj,KPhase),'O').and.
     2           .not.EqIgCase(AtType(isfj,KPhase),'Ba').and.
     3           .not.EqIgCase(AtType(isfj,KPhase),'Cl')) then
                if(MagPar(j).le.0) then
                  MagPar(j)=1
                  MagParMax=1
                  call ReallocateAtomParams(NAtCalc,itfmax,IFrMax,kmodn,
     1                                      MagParMax)
                endif
                call EM50ReadMagneticFFLabels(AtType(isfj,KPhase),
     1                                        MenuJ,NMenuJ,ierr)
                if(ierr.eq.0) then
                  if(NMenuJ(1).gt.0) then
                    AtTypeMag(isfj,KPhase)=MenuJ(NMenuJ(1),1)
                    AtTypeMagJ(isfj,KPhase)='j0'
                    t256='Magnetic_formfactor_<j0>'
                    call MagFFFromAtomFile(AtType(isfj,KPhase),
     1                                     AtTypeMag(isfj,KPhase),t256,
     3                                     FFMag(1,isfj,KPhase),ich)
                  endif
                endif
                 sm0(i,j)= pa(k)/CellPar(i,isw,KPhase)
                ssm0(i,j)=spa(k)/CellPar(i,isw,KPhase)
              endif
            enddo
          enddo
        enddo
        call iom50(1,0,fln(:ifln)//'.m50')
      endif
      if(KPhase.ne.1) then
        nn=NMolecToAll(KPhase-1)
      else
        nn=0
      endif
      do isw=1,3
        NMolecFr(isw,KPhase)=nn+1
        NMolecTo(isw,KPhase)=nn
        NMolecLen(isw,KPhase)=0
      enddo
      NMolecFrAll(KPhase)=nn+1
      NMolecToAll(KPhase)=nn
      NMolecLenAll(KPhase)=0
      lite(KPhase)=0
      irot(KPhase)=1
      if(KPhase.eq.1) then
        call SetRealArrayTo(ec,12*MxDatBlock,0.)
        sc(1,1)=1.
      endif
      if(NDimI(KPhase).gt.0) then
        do i=1,NAtCalc
          do j=1,7
            KModAMax(j)=max(KModAMax(j),KModA(j,i))
          enddo
          if(KModA(1,i).gt.0) then
            if(KFA(1,i).ne.0) then
              xp(1)=ax(KModA(1,i),i)
              k=1
            else if(KFA(2,i).ne.0) then
              xp(1)=uy(1,KModA(2,i),i)
              k=1
            else
              xp(1)=0.
              k=0
            endif
            call SpecPos(x(1,i),xp,k,iswa(i),.01,nocc)
            pom=1./float(nocc)
            if(a0(i).gt.-3000.) then
              ai(i)=ai(i)/a0(i)
            else
              a0(i)=ai(i)/pom
              sa0(i)=sai(i)/pom
              ai(i)=pom
            endif
            sai(i)=0.
          endif
          phf(i)=0.
          sphf(i)=0.
        enddo
      endif
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,0),6)
      call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
      do i=1,NDimI(KPhase)
        call CopyVek(Qu(1,i,1,KPhase),QuRefBlock(1,i,0),3)
        call CopyVek(Qu(1,i,1,KPhase),QuPwd(1,i,KPhase),3)
      enddo
      if(isPowder.and.KPhase.le.1) then
        call SetBasicM41(KDatBlock)
        ExistPowder=.true.
      endif
      if(NComp(KPhase).gt.1) then
        if(2*NAtCalc.gt.MxAtAll) call ReallocateAtoms(2*NAtCalc-MxAtAll)
        do i=1,NAtCalc
          call AtCopy(i,i+NAtCalc)
        enddo
        nac=0
        do i=1,NComp(KPhase)
           do j=NAtCalc+1,2*NAtCalc
             if(iswa(j).eq.i) then
               nac=nac+1
               call AtCopy(j,nac)
             endif
           enddo
        enddo
      endif
      call iom40(1,0,fln(:ifln)//'.m40')
      if(KlicUsed.ne.5) call DeleteFile(PreviousM40)
      if(KPhase.gt.1) go to 9999
      if((ExistM90.or.ExistM95).and.KlicUsed.ne.5) then
        WaitTime=10000
        Ninfo=2
        TextInfo(1)='Please note possible inconsistency between'
        TextInfo(2)='imported structure and old reflection files.'
        call FeInfoOut(-1.,-1.,'WARNING','L')
      endif
      if(KlicUsed.gt.0) go to 7000
      if(isPowder) then
        call PwdBasicCIF(0,KlicUsed)
      else
        if(KlicUsed.eq.0) then
          call DRBasicRefCIF(0,1)
        else
          go to 7000
        endif
      endif
      if(ErrFlag.ne.0) then
        if(.not.BatchMode) then
          NInfo=1
          TextInfo(1)='CIF file does not contain any reflection block.'
          if(FeYesNoHeader(-1.,-1.,'Do you want to import data from '//
     1                     'file?',1)) CoDal=1
        endif
        ErrFlag=0
        isPowder=.false.
        ExistPowder=.false.
        t256=fln(:ifln)//'.m41'
        if(ExistFile(t256)) then
          call DeleteFile(t256)
          ExistM41=.false.
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
        endif
        go to 7000
      endif
      call CloseIfOpened(lni)
      call SetBasicM95(KRefBlock)
      SourceFileRefBlock(KRefBlock)=FileIn
      call FeGetFileTime(SourceFileRefBlock(KRefBlock),
     1                   SourceFileDateRefBlock(KRefBlock),
     2                   SourceFileTimeRefBlock(KRefBlock))
      call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,KRefBlock),6)
      do i=1,NDimI(KPhase)
        call CopyVek(Qu(1,i,1,KPhase),QuRefBlock(1,i,KRefBlock),3)
      enddo
      call UnitMat(TrMP,NDim(KPhase))
      CellReadIn(KRefBlock)=.true.
      CorrLp(KRefBlock)=-1
      CorrAbs(KRefBlock)=-1
      LamAveRefBlock(KRefBlock)=LamAve(1)
      DifCode(KRefBlock)=IdImportCIF
      if(Radiation(KDatBlock).eq.NeutronRadiation) then
        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else
        RadiationRefBlock(KRefBlock)=XRayRadiation
        AngleMonRefBlock(KRefBlock)=
     1   CrlMonAngle(MonCell(1,3),MonH(1,3),LamAveRefBlock(KRefBlock))
        AlphaGMonRefBlock(KRefBlock)=0.
        BetaGMonRefBlock(KRefBlock)=0.
      endif
      TempRefBlock(KRefBlock)=293
      UseTrRefBlock(KRefBlock)=.false.
      ITwRead(KRefBlock)=1
      if(isPowder) then
        PolarizationRefBlock(KRefBlock)=PolarizedMonoPar
        DifCode(KRefBlock)=IdDataCIF
      else
        if(Radiation(KDatBlock).eq.NeutronRadiation) then
          PolarizationRefBlock(KRefBlock)=PolarizedLinear
        else
          PolarizationRefBlock(KRefBlock)=PolarizedMonoPer
        endif
        DifCode(KRefBlock)=IdImportCIF
      endif
      RefBlockFileName=fln(:ifln)//'.l01'
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      if(ErrFlag.ne.0) call CIFSyntaxError(CIFLastReadRecord)
      t256='000000'
      call FeTabsAdd(FeTxLength(t256),UseTabs,IdLeftTab,' ')
      call FeTabsAdd(FeTxLengthSpace(t256(:6)//' '),UseTabs,IdRightTab,
     1               ' ')
      if(isPowder) then
        t256=Tabulator//'     0'//Tabulator//'records already read'
        if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
        if(allocated(YcPwd)) deallocate(YcPwd,YbPwd,YfPwd,YcPwdInd)
        nalloc=10000
        allocate(XPwd(nalloc),YoPwd(nalloc),YsPwd(nalloc),YiPwd(nalloc))
        NPnts=0
      else
        t256=Tabulator//'     0'//Tabulator//'reflections already read'
        write(t256(2:7),100) 0
        tbar=0.
        call SetRealArrayTo(uhly,4,0.)
        call SetRealArrayTo(dircos,6,0.)
        call SetRealArrayTo(corrf,2,1.)
        iq=1
        KProf=0
        NProf=0
        DRlam=0.
      endif
      call FeTxOut(-1.,-1.,t256)
6000  if(isPowder) then
        ipointA=0
        iorderA=0
        ipointI=0
        iorderI=0
        call PwdReadCIF(0,0,ik)
      else
        call DRReadRefCIF(0,0,ik)
      endif
      if(ErrFlag.ne.0) call CIFSyntaxError(CIFLastReadRecord)
      if(ik.ne.0) go to 6100
      if(isPowder) then
        NRef95(KRefBlock)=NRef95(KRefBlock)+1
        if(NPnts.ge.nalloc) call PwdImportReallocate(nalloc)
        NPnts=NPnts+1
        XPwd(NPnts)=float(ih(1))/1000000.
        YoPwd(NPnts)=ri
        YiPwd(NPnts)=0.
        YsPwd(NPnts)=rs
      else
        NRef95(KRefBlock)=NRef95(KRefBlock)+1
        no=NRef95(KRefBlock)
        expos=float(NRef95(KRefBlock))*.1
        iflg(1)=iq
        if(iflg(2).lt.0) HKLF5RefBlock(KRefBlock)=1
        call DRPutReflectionToM95(95,n)
        NLines95(KRefBlock)=NLines95(KRefBlock)+2
      endif
      if(mod(NRef95(KRefBlock),50).eq.0) then
        write(t256(2:7),100) NRef95(KRefBlock)
        call FeTxOutCont(t256)
      endif
      go to 6000
6100  write(t256(2:7),100) NRef95(KRefBlock)
      call FeTxOutCont(t256)
      call FeTxOutEnd
      if(isPowder) then
        call PwdMakeFormat92
        call PwdPutRecordToM95(95)
      endif
      call CloseIfOpened(95)
      call iom95(1,fln(:ifln)//'.m95')
      call CompleteM95(0)
      ExistM95=.true.
7000  if(KlicUsed.eq.5) then
        i=2
      else
        i=0
      endif
      call FinalStepOfImport(i,'CIF')
      go to 9999
9000  if(ich.eq.-1) then
        call FeChybne(-1.,-1.,'the crutial information is missing.',
     1                t256,SeriousError)
      else if(ich.eq.-2) then
        call FeChybne(-1.,-1.,'CIF syntax problem.',CIFLastReadRecord,
     1                SeriousError)
      else if(ich.eq.-3) then
        call FeChybne(-1.,-1.,'number of variables does not match.',
     1                t256,SeriousError)
      else
        call FeChybne(-1.,-1.,'neco navic.',t256,SeriousError)
      endif
9900  ErrFlag=1
9999  call CloseIfOpened(lni)
      call CloseIfOpened(91)
      call CloseIfOpened(95)
      call DeleteFile('cif.pom')
      if(KlicUsed.ne.5) then
        call DeleteFile(fln(:ifln)//'.z40')
        call DeleteFile(fln(:ifln)//'.z50')
      endif
      if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      if(allocated(AtMultP)) deallocate(AtMultP)
      if(allocated(XPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
      if(allocated(CIFArray)) deallocate(CIFArray)
      if(allocated(aip)) deallocate(aip,saip)
      if(allocated(IdCode)) deallocate(IdCode)
      if(allocated(OrX40P)) deallocate(OrX40P,OrDelP,OrEpsP)
      if(allocated(pa)) deallocate(pa,spa,ia,StLbl,sta,stap)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
100   format(i6)
101   format(f15.0)
102   format(i1)
103   format(i6)
104   format(3f9.4,3f9.2)
      end
