      subroutine AtCheck(at,ich,iatm)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) at
      ich=0
      iatm=0
      if(index(at,'#').ne.0.or.index(at,'*').ne.0.or.
     1   index(at,'?').ne.0.or.index(at,'[').ne.0.or.index(at,']').ne.0)
     2      ich=1
      iatm=ktatmol(at)
      if(iatm.ne.0) ich=2
      return
      end
