      subroutine UnpackHKL(hkl,ih,mxh,mxd,n)
      include 'fepc.cmn'
      dimension ih(n),mxh(n),mxd(n)
      integer hkl
      do i=1,n
        if(i.eq.1) then
          ih(i)=hkl/mxd(i)-mxh(i)
        else
          ih(i)=mod(hkl,mxd(i-1))/mxd(i)-mxh(i)
        endif
      enddo
      return
      end
