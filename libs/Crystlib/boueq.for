      subroutine boueq(beta,sbeta,liteac,bizo,sbizo,isw)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension beta(6),sbeta(6)
      bizo=MetTens(1,isw,KPhase)*beta(1)+MetTens(5,isw,KPhase)*beta(2)+
     1     MetTens(9,isw,KPhase)*beta(3)+
     1 2.*(MetTens(4,isw,KPhase)*beta(4)+MetTens(7,isw,KPhase)*beta(5)+
     3     MetTens(8,isw,KPhase)*beta(6))
      sbizo=(MetTens(1,isw,KPhase)*sbeta(1))**2+
     1       (MetTens(5,isw,KPhase)*sbeta(2))**2+
     2       (MetTens(9,isw,KPhase)*sbeta(3))**2+
     3   4.*((MetTens(4,isw,KPhase)*sbeta(4))**2+
     4       (MetTens(7,isw,KPhase)*sbeta(5))**2+
     5       (MetTens(8,isw,KPhase)*sbeta(6))**2)
      bizo=bizo*1.333333333
      sbizo=sqrt(sbizo)*1.333333333
      if(liteac.eq.0) then
         bizo= bizo/episq
        sbizo=sbizo/episq
      endif
      return
      end
