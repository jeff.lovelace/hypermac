      subroutine CrlChangeL51(OldFln,NewFln,Klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) OldFln,NewFln
      character*256 Veta
      character*10 Labels(4)
      logical EqIgCase
      data Labels/'refine','fourier','dist','contour'/
      lni=NextLogicNumber()
      lno=0
      call OpenFile(lni,OldFln(:idel(OldFln))//'.l51','formatted','old')
      if(ErrFlag.ne.0) go to 5000
      lno=NextLogicNumber()
      call OpenFile(lno,NewFln(:idel(NewFln))//'.l51','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 5000
1000  read(lni,FormA,end=5000) Veta
      write(lno,FormA) Veta(:idel(Veta))
      k=0
      call kus(Veta,k,Cislo)
      do i=1,4
        if(EqIgCase(Cislo,Labels(i))) go to 1400
      enddo
      go to 1000
1400  if(Klic.eq.0) then
        read(lni,FormA,end=5000) Veta
      else
        write(lno,FormA)
      endif
      go to 1000
5000  call CloseIfOpened(lno)
      call CloseIfOpened(lni)
      return
      end
