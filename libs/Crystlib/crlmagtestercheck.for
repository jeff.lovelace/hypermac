      subroutine CrlMagTesterCheck
      use Atoms_mod
      use Basic_mod
      use Refine_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      character*10 t10
      dimension rmx(9),ShSgOrg(6)
      integer SbwItemPointerQuest,SbwLnQuest
      logical CrwLogicQuest
      ShSgOrg(1:NDim(KPhase))=ShSg(1:NDim(KPhase),KPhase)
      EpiSel=SbwItemPointerQuest(nSbwGroup)
      EpiSel=EpiSel+EpiSelFr-1
      EpiSel=OrdEpi(EpiSel)
      if(nGrpMenuLast.ne.EpiSel) then
        nGrpMenuLast=EpiSel
        if(FromRepAnalysis) then
          Grupa(KPhase)=GrpEpi(EpiSel)
          ShSg(1:NDim(KPhase),KPhase)=ShSgEpi(1:NDim(KPhase),EpiSel)
          rmx=0.
          if(QPul) then
            NLattVec(KPhase)=NLattVecEpiComm(EpiSel)
            do j=1,NLattVec(KPhase)
              call CopyVek(vt6EpiComm(1,j,EpiSel),vt6(1,j,1,KPhase),3)
            enddo
            do j=1,NSymm(KPhase)
              call CopyMat(rmEpiComm(1,j,EpiSel),rm6(1,j,1,KPhase),3)
              call CopyMat(rmEpiComm(1,j,EpiSel),rm (1,j,1,KPhase),3)
              call CopyVek(s6EpiComm(1,j,EpiSel), s6(1,j,1,KPhase),3)
              ZMag(j,1,KPhase)=ZMagEpiComm(j,EpiSel)
            enddo
          else
            ns=0
            do j=1,NSymmS
              if(BratSymmEpi(j,EpiSel)) then
                ns=ns+1
                call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
                call CopyMat(rmr (1,j),rm (1,ns,1,KPhase),3)
                ZMag(ns,1,KPhase)=ZMagEpi(j,EpiSel)
                call CopyVek(s6Epi(1,j,EpiSel),s6(1,ns,1,KPhase),
     1                       NDim(KPhase))
              endif
            enddo
            NSymmN(KPhase)=ns
            MaxNSymN=NSymmN(KPhase)
            NSymm(KPhase)=ns
          endif
          if(NDimI(KPhase).eq.1) then
            if(ubound(rm6,2).lt.2*NSymm(KPhase))
     1        call ReallocSymm(NDim(KPhase),2*NSymm(KPhase),
     2                         NLattVec(KPhase),NComp(KPhase),NPhase,0)
            ns=ns+1
            call UnitMat(rm6(1,ns,1,KPhase),NDim(KPhase))
            call UnitMat(rm(1,ns,1,KPhase),3)
            call SetRealArrayTo(s6(1,ns,1,KPhase),NDim(KPhase),0.)
            s6(4,ns,1,KPhase)=.5
            ZMag(ns,1,KPhase)=-1.
            NSymm(KPhase)=ns
            call CrlMakeRMag(ns,1)
            call CompleteSymm(0,ich)
            if(Allocated(KWSym)) deallocate(KwSym)
            allocate(KWSym(MaxUsedKwAll,NSymm(KPhase),NComp(KPhase),
     1           NPhase))
            call SetSymmWaves
          endif
          MaxNSym=NSymm(KPhase)
        else
          do j=1,NSymm(KPhase)
            ZMag(j,1,KPhase)=ZMagGen(j,nGrpMenuLast)
            call CopyVek(s6Gen(1,j,nGrpMenuLast),s6(1,j,1,KPhase),
     1                   NDim(KPhase))
          enddo
          Grupa(KPhase)=GrpMenu(nGrpMenuLast)
        endif
        do j=1,NSymm(KPhase)
          call CrlMakeRMag(j,1)
        enddo
        call CloseIfOpened(SbwLnQuest(nSbwAtoms))
        call DeleteFile(fln(:ifln)//'_atoms.tmp')
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_atoms.tmp','formatted','unknown')
        call OpenFile(lst,fln(:ifln)//'.l00','formatted','unknown')
        NAtomMenuLast=0
        do 1500i=1,NAtCalc
          if(kswa(i).ne.KPhase) go to 1500
          Veta=Atom(i)
          if(MagPar(i).ne.0) then
            call CrlMagTesterSetAtom(i,2.,.true.)
            neq=0
            call AtSpec(i,i,0,0,0)
            call SetRealArrayTo(rmx,3,0.)
            do j=1,NSymm(KPhase)
              call Cultm(RMag(1,j,1,KPhase),sm0(1,i),rmx,3,3,1)
            enddo
            if(NDimI(KPhase).eq.0) then
              kk=2
            else
              kk=2+2*min(NDimI(KPhase),2)
            endif
            do k=1,kk
              t10='('
              do j=1,3
                if(k.eq.1) then
                  pom=sm0(j,i)
                else if(k.eq.2) then
                  pom=rmx(j)
                else
                  if(mod(k-2,2).eq.1) then
                    pom=smx(j,(k-1)/2,i)
                  else
                    pom=smy(j,(k-1)/2,i)
                  endif
                endif
                if(abs(pom).lt..0001) then
                  t10=t10(:idel(t10))//'0,'
                else
                  t10=t10(:idel(t10))//'M,'
                endif
              enddo
              j=idel(t10)
              Veta=Veta(:idel(Veta))//Tabulator//'|'//Tabulator//
     1             t10(:idel(t10)-1)//')'
            enddo
            Veta=Veta(:idel(Veta))//Tabulator//'|'
            if(NDimI(KPhase).eq.1.and.NCommQProduct(1,KPhase).eq.2) then
              write(Cislo,'(f8.3)') PhaseX4(i)
              Veta=Veta(:idel(Veta))//Tabulator//Cislo(:idel(Cislo))
            endif
            write(ln,FormA) Veta(:idel(Veta))
          endif
1500    continue
        close(lst,status='delete')
        call CloseIfOpened(ln)
        call FeQuestSbwMenuOpen(nSbwAtoms,fln(:ifln)//'_atoms.tmp')
      endif
      ShSg(1:NDim(KPhase),KPhase)=ShSgOrg(1:NDim(KPhase))
      return
      end
