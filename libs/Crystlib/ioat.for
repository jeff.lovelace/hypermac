      subroutine ioat(i1,i2,isw,nm,Klic,tisk)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension px(6),kmodp(7)
      character*80 Veta
      integer tisk
      logical PolMag,EqIgCase
      do i=i1,i2
        kip=1
        if(Klic.eq.0) then
          PrvniKiAtomu(i)=PrvniKi
          call SetBasicKeysForAtom(i)
          read(m40,100,err=9000,end=9000) atom(i),isf(i),itff,lasmax(i),
     1      ai(i),(x(j,i),j=1,3),(KFA(j,i),j=1,3),(KModA(j,i),j=1,3)
          sai(i)=0.
          call SetRealArrayTo(sx(1,i),3,0.)
          if(isf(i).le.0.or.isf(i).gt.NAtFormula(KPhase)) then
            write(Cislo,FormI15) isf(i)
            call Zhusti(Cislo)
            call FeChybne(-1.,-1.,'incorrect flag for atomic type (='//
     1                    Cislo(:idel(Cislo))//
     2                    ') of the atom '//atom(i),' ',SeriousError)
            go to 9100
          else if(itff.le.0.and.nm.le.0) then
            write(Cislo,FormI15) itff
            call Zhusti(Cislo)
            call FeChybne(-1.,-1.,'incorrect ADP flag (='//
     1                    Cislo(:idel(Cislo))//
     2                    ') of the atom '//atom(i),' ',SeriousError)
            go to 9100
          endif
          ifr(i)=itff/10
          IFrMax=max(IFrMax,ifr(i))
          itfi=mod(itff,10)
          itf(i)=itfi
          if(NDim(KPhase).le.3) then
            if(ChargeDensities) then
              lasmaxm=max(lasmax(i),lasmaxm)
              MagPar(i)=0
            else
              MagPar(i)=lasmax(i)
              lasmax(i)=0
              lasmaxm=0
            endif
          else
            MagPar(i)=lasmax(i)
            lasmax(i)=0
            if(itfi.gt.2)
     1        read(m40,104,err=9000,end=9000) Cislo,(KFA(j,i),j=4,7),
     2                                        (KModA(j,i),j=4,7)
            if(NDimI(KPhase).gt.0.and.NonModulated(KPhase)) then
              do j=1,itfi+1
                NonModulated(KPhase)=NonModulated(KPhase).and.
     1                               KModA(j,i).le.0
              enddo
              NonModulated(KPhase)=NonModulated(KPhase).and.
     1                             MagPar(i).le.1
            endif
             phf(i)=0.
            sphf(i)=0.
          endif
          do j=1,7
            if(KModA(j,i).le.0) KFA(j,i)=0
            kmodp(j)=KModA(j,i)
          enddo
          call ReallocateAtomParams(i-1,itf(i),ifr(i),kmodp,MagPar(i))
          read(m40,105,err=9000,end=9000)(beta(j,i),j=1,6),
     1                                   (kia(j,i),j=1,10)
          call SetRealArrayTo(sbeta(1,i),6,0.)
          if(itf(i).eq.1) call SetRealArrayTo(beta(2,i),5,0.)
          iswa(i)=isw
          kswa(i)=KPhase
          if(NAtFormula(KPhase).gt.0) then
            if(EqIgCase(AtType(isf(i),KPhase),'H').or.
     1         EqIgCase(AtType(isf(i),KPhase),'D')) then
              NAtH(KPhase)=NAtH(KPhase)+1
            endif
          endif
          kmol(i)=nm
          if(tisk.eq.1) then
            call newln(2)
            if(NDim(KPhase).eq.3) then
              write(lst,102) atom(i),isf(i),itff,ai(i),(x(j,i),j=1,3),
     1                       (beta(j,i),j=1,6),
     2                       (max(kia(j,i),0),j=1,10)
            else
              write(lst,101) atom(i),isf(i),itff,ai(i),(x(j,i),j=1,3),
     1                       (KFA(j,i),j=1,3),(KModA(j,i),j=1,3),
     2                       (beta(j,i),j=1,6),
     3                       (max(kia(j,i),0),j=1,10)
            endif
          endif
        else
          kip=1
          itfi=itf(i)
          itff=itfi+ifr(i)*10
          if(lite(KPhase).eq.0) then
            if(itfi.ne.1) then
              do j=1,6
                beta(j,i)=beta(j,i)/urcp(j,isw,KPhase)
              enddo
            else
              beta(1,i)=beta(1,i)/episq
            endif
          else if(itfi.eq.1) then
            call SetRealArrayTo(beta(2,i),5,0.)
          endif
          if(NDim(KPhase).eq.3) then
            if(ChargeDensities) then
              write(m40,100) atom(i),isf(i),itff,lasmax(i),ai(i),
     1          (x(j,i),j=1,3)
            else if(MagPar(i).gt.0) then
              write(m40,100) atom(i),isf(i),itff,MagPar(i),ai(i),
     1          (x(j,i),j=1,3)
            else
              write(m40,103) atom(i),isf(i),itff,ai(i),(x(j,i),j=1,3)
            endif
          else
            do j=itfi+2,7
              KModA(j,i)=0
            enddo
            do j=1,7
              if(KModA(j,i).le.0) KFA(j,i)=0
            enddo
            if(MagPar(i).gt.0) then
              write(m40,100) atom(i),isf(i),itff,MagPar(i),ai(i),
     1                       (x(j,i),j=1,3),
     1                       (KFA(j,i),j=1,3),(KModA(j,i),j=1,3)
            else
              write(m40,103) atom(i),isf(i),itff,ai(i),(x(j,i),j=1,3),
     1                       (KFA(j,i),j=1,3),(KModA(j,i),j=1,3)
            endif
            if(itfi.gt.2) write(m40,104) atom(i),(KFA(j,i),j=4,7),
     1                                   (KModA(j,i),j=4,7)
          endif
          write(m40,105)(beta(j,i),j=1,6),
     1                  (max(kia(j,i),0),j=1,10)
        endif
        if(lite(KPhase).eq.0) then
          if(itfi.ne.1) then
            do j=1,6
              beta(j,i)=beta(j,i)*urcp(j,isw,KPhase)
            enddo
          else
            beta(1,i)=beta(1,i)*episq
          endif
        endif
        kip=kip+10
        call uprat(atom(i))
        if(NDimI(KPhase).gt.0) then
          call SetRealArrayTo(qcnt(1,i),3,0.)
          call qbyx(x(1,i),qcnt(1,i),isw)
        endif
        itfmax=max(itfmax,itfi)
        if(itfi.gt.2) then
          call iost(m40,c3(1,i),kia(kip,i),10,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc3(1,i),10,0.)
          kip=kip+10
        endif
        if(itfi.gt.3) then
          call iost(m40,c4(1,i),kia(kip,i),15,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc4(1,i),15,0.)
          kip=kip+15
        endif
        if(itfi.gt.4) then
          call iost(m40,c5(1,i),kia(kip,i),21,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc5(1,i),21,0.)
          kip=kip+21
        endif
        if(itfi.gt.5) then
          call iost(m40,c6(1,i),kia(kip,i),28,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc6(1,i),28,0.)
          kip=kip+28
        endif
        if(ifr(i).gt.0) then
          call iost(m40,xfr(i),kia(kip,i),1,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          kip=kip+1
        endif
        if(NDimI(KPhase).le.0) then
          if(ChargeDensities.and.Klic.le.0) then
            SmbPGAt(i)='1'
            NPGAt(i)=1
            call UnitMat(RPGAt(1,1,i),3)
            LocAtSystSt(1,i)='***'
            LocAtSystSt(2,i)='***'
            call SetRealArrayTo(LocAtSystX(1,1,i),6,0.)
            LocAtSystX(1,1,i)=1.
            LocAtSystX(2,2,i)=1.
            LocAtSystAx(i)='xy'
            LocAtSense(i)=' '
          endif
          if(lasmax(i).gt.0) then
            if(lasmax(i).gt.1) then
              j=4
            else
              j=3
            endif
            if(Klic.gt.0) then
              px(1)=popc(i)
              px(2)=popv(i)
              px(3)=kapa1(i)
              if(j.eq.4) px(4)=kapa2(i)
            endif
            call iost(m40,px,kia(kip,i),j,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            kip=kip+j
            if(Klic.le.0) then
              popc(i)=px(1)
              popv(i)=px(2)
              kapa1(i)=px(3)
              if(j.eq.4) then
                kapa2(i)=px(4)
              else
                kapa2(i)=1.
              endif
            endif
            kk=lasmax(i)-1
            if(kk.le.0) go to 1700
            if(Klic.le.0) then
              read(m40,107,err=9000,end=9000)(LocAtSystSt(j,i),j=1,2),
     1          LocAtSystAx(i),LocAtSense(i),SmbPGAt(i)
              call mala(LocAtSystAx(i))
              call mala(SmbPGAt(i))
            else
              if(LocAtSense(i).ne.'-') LocAtSense(i)=' '
              do j=1,2
                if(LocAtSystSt(j,i)(1:1).ne.'-'.and.
     1             LocAtSystSt(j,i)(1:1).ne.' ')
     2            LocAtSystSt(j,i)=' '//
     3              LocAtSystSt(j,i)(:idel(LocAtSystSt(j,i)))
              enddo
              write(m40,107)(LocAtSystSt(j,i),j=1,2),LocAtSystAx(i),
     1                       LocAtSense(i),SmbPGAt(i)
            endif
            kk=kk**2
            call iost(m40,popas(1,i),kia(kip,i),kk,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            kip=kip+kk
          endif
        endif
1700    if(KModA(1,i).gt.0) then
          if(Klic.eq.0) then
            read(m40,FormA) Veta
            k=0
            call StToReal(Veta,k,a0(i),1,.false.,ich)
            if(ich.ne.0) go to 9000
            if(k.lt.60) then
              call Kus(Veta,k,Cislo)
              if(EqIgCase(Cislo,'Ortho')) then
                TypeModFun(i)=1
                call StToReal(Veta,k,OrthoEps(i),1,.false.,ich)
                if(ich.ne.0) go to 9100
                OrthoDelta(i)=a0(i)
              else if(EqIgCase(Cislo,'Legendre')) then
                TypeModFun(i)=2
              else if(EqIgCase(Cislo,'XHarm')) then
                TypeModFun(i)=3
              endif
            else
              TypeModFun(i)=0
            endif
            call StToInt(Veta,k,kia(kip,i),1,.false.,ich)
            sa0(i)=0.
          else
            if(TypeModFun(i).eq.1) then
              Cislo='Ortho'
            else if(TypeModFun(i).eq.2) then
             Cislo='Legendre'
            else if(TypeModFun(i).eq.3) then
              Cislo='XHarm'
            else
              Cislo=' '
            endif
            if(TypeModFun(i).eq.1) then
              write(Veta,109) a0(i),Cislo(1:8),OrthoEps(i),kia(kip,i)
            else
              write(Veta,108) a0(i),Cislo(1:8),kia(kip,i)
            endif
            write(m40,FormA) Veta(:idel(Veta))
          endif
          kip=kip+1
          if(KFA(1,i).ne.0) SoucinAy=1.
          do j=1,KModA(1,i)
            if(Klic.ne.0) then
              px(1)=ax(j,i)
              px(2)=ay(j,i)
            endif
            call iost(m40,px,kia(kip,i),2,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            if(TypeModFun(i).eq.1.and.j.eq.KModA(1,i).and.Klic.eq.0)
     1        OrthoX40(i)=px(1)
            kip=kip+2
            if(ErrFlag.ne.0) go to 9999
            if(Klic.eq.0) then
               ax(j,i)=px(1)
               ay(j,i)=px(2)
              sax(j,i)=0.
              say(j,i)=0.
            endif
            if(KFA(1,i).ne.0.and.j.le.NDimI(KPhase).and.
     1         NDimI(KPhase).gt.1) SoucinAy=SoucinAy*ay(j,i)
          enddo
          if(KFA(1,i).ne.0.and.KModA(1,i).ge.NDimI(KPhase)) then
            if(NDim(KPhase).gt.4) then
              if(abs(SoucinAy).lt..0001) then
                call SetRealArrayTo(ay(KModA(1,i)-NDim(KPhase)-2,i),
     1            NDimI(KPhase),a0(i)**(1./float(NDimI(KPhase))))
              else
                 a0(i)=SoucinAy
                sa0(i)=0.
              endif
            else if(NDim(KPhase).eq.4) then
              ay(KModA(1,i),i)=0.
            endif
          endif
        else
           a0(i)=1.
          sa0(i)=0.
        endif
        do j=1,KModA(2,i)
          if(Klic.ne.0) then
            call CopyVek(ux(1,j,i),px   ,3)
            call CopyVek(uy(1,j,i),px(4),3)
          endif
          call iost(m40,px,kia(kip,i),6,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          kip=kip+6
          if(Klic.eq.0) then
            call CopyVek(px   ,ux(1,j,i),3)
            call CopyVek(px(4),uy(1,j,i),3)
            if(Klic.eq.0) then
              call SetRealArrayTo(sux(1,j,i),3,0.)
              call SetRealArrayTo(suy(1,j,i),3,0.)
            endif
          endif
        enddo
        do j=1,KModA(3,i)
          if(Klic.ne.0.and.lite(KPhase).eq.0) then
            do l=1,6
              bx(l,j,i)=bx(l,j,i)/urcp(l,isw,KPhase)
              by(l,j,i)=by(l,j,i)/urcp(l,isw,KPhase)
            enddo
          endif
          call iost(m40,bx(1,j,i),kia(kip,i),6,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sbx(1,j,i),6,0.)
          kip=kip+6
          call iost(m40,by(1,j,i),kia(kip,i),6,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sby(1,j,i),6,0.)
          kip=kip+6
          if(lite(KPhase).eq.0) then
            do l=1,6
              bx(l,j,i)=bx(l,j,i)*urcp(l,isw,KPhase)
              by(l,j,i)=by(l,j,i)*urcp(l,isw,KPhase)
            enddo
          endif
        enddo
        do j=1,KModA(4,i)
          call iost(m40,c3x(1,j,i),kia(kip,i),10,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc3x(1,j,i),10,0.)
          kip=kip+10
          call iost(m40,c3y(1,j,i),kia(kip,i),10,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc3y(1,j,i),10,0.)
          kip=kip+10
        enddo
        do j=1,KModA(5,i)
          call iost(m40,c4x(1,j,i),kia(kip,i),15,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc4x(1,j,i),15,0.)
          kip=kip+15
          call iost(m40,c4y(1,j,i),kia(kip,i),15,Klic,tisk)
          if(Klic.eq.0) call SetRealArrayTo(sc4y(1,j,i),15,0.)
          kip=kip+15
        enddo
        do j=1,KModA(6,i)
          call iost(m40,c5x(1,j,i),kia(kip,i),21,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc5x(1,j,i),21,0.)
          kip=kip+21
          call iost(m40,c5y(1,j,i),kia(kip,i),21,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc5y(1,j,i),21,0.)
          kip=kip+21
        enddo
        do j=1,KModA(7,i)
          call iost(m40,c6x(1,j,i),kia(kip,i),28,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc6x(1,j,i),28,0.)
          kip=kip+28
          call iost(m40,c6y(1,j,i),kia(kip,i),28,Klic,tisk)
          if(ErrFlag.ne.0) go to 9999
          if(Klic.eq.0) call SetRealArrayTo(sc6y(1,j,i),28,0.)
          kip=kip+28
        enddo
        do j=1,7
          if(KModA(j,i).gt.0) then
            call iost(m40,phf(i),kia(kip,i),1,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            if(Klic.eq.0) sphf(i)=0.
            kip=kip+1
            exit
          endif
        enddo
        if(MagneticType(KPhase).gt.0) then
          PolMag=ktat(NamePolar,NPolar,Atom(i)).gt.0
          do j=1,MagPar(i)
            if(j.eq.1) then
              if(Klic.gt.0) then
                if(PolMag) then
                  call CopyVek(sm0(1,i),px,3)
                else
                  do k=1,3
                    px(k)=sm0(k,i)*CellPar(k,isw,KPhase)
                  enddo
                endif
              endif
              n=3
            else
              if(Klic.gt.0) then
                if(PolMag) then
                  call CopyVek(smx(1,j-1,i),px(1),3)
                  call CopyVek(smy(1,j-1,i),px(4),3)
                else
                  do k=1,3
                    px(k  )=smx(k,j-1,i)*CellPar(k,isw,KPhase)
                    px(k+3)=smy(k,j-1,i)*CellPar(k,isw,KPhase)
                  enddo
                endif
              endif
              n=6
            endif
            call iost(m40,px,kia(kip,i),n,Klic,tisk)
            if(ErrFlag.ne.0) go to 9999
            if(Klic.le.0) then
              if(j.eq.1) then
                if(PolMag) then
                  call CopyVek(px,sm0(1,i),3)
                else
                  do k=1,3
                    sm0(k,i)=px(k)/CellPar(k,isw,KPhase)
                  enddo
                endif
              else
                if(PolMag) then
                  call CopyVek(px(1),smx(1,j-1,i),3)
                  call CopyVek(px(4),smy(1,j-1,i),3)
                else
                  do k=1,3
                    smx(k,j-1,i)=px(k  )/CellPar(k,isw,KPhase)
                    smy(k,j-1,i)=px(k+3)/CellPar(k,isw,KPhase)
                  enddo
                endif
              endif
            endif
            kip=kip+n
          enddo
        endif
        if(Klic.eq.0) then
          if(kip.le.mxda) call SetIntArrayTo(KiA(kip,i),mxda-kip+1,0)
          PrvniKi=PrvniKi+kip-1
          DelkaKiAtomu(i)=kip-1
        endif
      enddo
      NAtCalc=NAtInd
      if(Klic.eq.0.and.ChargeDensities) then
        do i=i1,i2
          kk=lasmax(i)
          call CopyMat(TrToOrtho (1,1,KPhase),TrAt (1,i),3)
          call CopyMat(TrToOrthoI(1,1,KPhase),TriAt(1,i),3)
          call UnitMat(TroAt (1,i),3)
          call UnitMat(TroiAt(1,i),3)
          if(kk.le.1) cycle
          call CrlMakeTrMatToLocal(x(1,i),atom(i),LocAtSystAx(i),
     1      LocAtSystSt(1,i),LocAtSense(i),LocAtSystX(1,1,i),TrAt(1,i),
     2      TriAt(1,i),0,ich)
          if(ich.ne.0) go to 9100
          call Multm(TrAt(1,i),TrToOrthoI(1,1,KPhase),TroAt(1,i),3,3,3)
          call MatInv(TroAt(1,i),TroiAt(1,i),pom,3)
        enddo
      endif
      go to 9999
9000  ErrFlag=1
      go to 9999
9050  call FeChybne(-1.,-1.,'in the local coordinate system for : '//
     1              atom(i)(:idel(atom(i))),' ',SeriousError)
9100  ErrFlag=2
9999  return
100   format(a8,3i3,1x,4f9.6,6x,3i1,3i3)
101   format(1x,a8,2i3,4x,4f9.6,3x,'>>',3i1,3i3,'<<'/
     1       1x,6f9.6,3x,'=>',10i1,'<=')
102   format(1x,a8,2i3,4x,4f9.6/1x,6f9.6,3x,'=>',10i1,'<=')
103   format(a8,2i3,4x,4f9.6,6x,3i1,3i3)
104   format(a8,52x,4i1,4i3)
105   format(6f9.6,6x,10i1)
106   format(7(i2,f7.3))
107   format(2a27,2x,a2,a1,1x,a8)
108   format(f9.6,10x,a8,33x,i1)
109   format(f9.6,10x,a8,f9.6,24x,i1)
      end
