      logical function CIFEq(String1,String2)
      character*(*) String1,String2
      character*256 String1P,String2P
      logical EqIgCase
      String1P=String1
      String2P=String2
      do i=1,idel(String1P)
        if(String1P(i:i).eq.'.') then
          String1P(i:i)='_'
        else
          String1P(i:i)=String1P(i:i)
        endif
      enddo
      do i=1,idel(String2P)
        if(String2P(i:i).eq.'.') then
          String2P(i:i)='_'
        else
          String2P(i:i)=String2P(i:i)
        endif
      enddo
      CIFEq=EqIgCase(String1P,String2P)
      return
      end
