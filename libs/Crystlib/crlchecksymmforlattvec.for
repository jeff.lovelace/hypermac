      subroutine CrlCheckSymmForLattVec(isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension si(:),sj(:)
      integer Flag(:),CrlMagInver
      logical EqRV,EqRV0,MatRealEqUnitMat
      allocatable si,sj,Flag
      allocate(si(NDim(KPhase)),sj(NDim(KPhase)),Flag(NSymm(KPhase)))
      n=NLattVec(KPhase)
      do 1100i=1,NSymm(KPhase)
        if(MatRealEqUnitMat(RM6(1,i,isw,KPhase),NDim(KPhase),.0001).and.
     1     ZMag(i,isw,KPhase).gt.0.) then
          if(EQRV0(s6(1,i,isw,KPhase),NDim(KPhase),.0001)) go to 1100
          n=n+1
          call ReallocSymm(NDim(KPhase),NSymm(KPhase),n,NComp(KPhase),
     1                     NPhase,0)
          call CopyVek(s6(1,i,isw,KPhase),vt6(1,n,isw,KPhase),
     1                 NDim(KPhase))
        endif
1100  continue
      if(n.eq.NLattVec(KPhase)) go to 9000
      NLattVec(KPhase)=n
      call SetIntArrayTo(Flag,NSymm(KPhase),1)
      n=0
      do 1200i=1,NSymm(KPhase)
        if(Flag(i).ne.1) go to 1200
        call CopyVek(s6(1,i,isw,KPhase),si,NDim(KPhase))
        call NormCentr(si)
        do 1150j=i+1,NSymm(KPhase)
          if(Flag(j).ne.1) go to 1150
          if(EqRV(RM6(1,i,isw,KPhase),RM6(1,j,isw,KPhase),NDimQ(KPhase),
     1            .0001)) then
            call CopyVek(s6(1,j,isw,KPhase),sj,NDim(KPhase))
            call NormCentr(sj)
            if(EqRV(si,sj,NDim(KPhase),.0001)) Flag(j)=0
          endif
1150    continue
        n=n+1
        call CopyVek(si,s6(1,i,isw,KPhase),NDim(KPhase))
        call CopySymmOperator(i,n,isw)
1200  continue
      NSymm(KPhase)=n
      if(CrlMagInver().gt.0) then
        NSymmN(KPhase)=n/2
      else
        NSymmN(KPhase)=n
      endif
9000  deallocate(si,sj,Flag)
      return
      end
