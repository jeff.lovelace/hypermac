      subroutine ReadCompMat(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension FMat(36),FMatInv(36),ZSigP(9),ZSigIP(9),PomMat(9),
     1          smp(9),CellOut(6),VSig(9),tpoma(0:6),xpoma(6)
      character*80 Veta
      character*2  nty
      logical FeYesNoHeader
      real MTPom(9),MTIPom(9)
      dpom=50.
      xqd=dpom+FeTxLength('XXXX')+10.
      xqd=xqd*float(NDim(KPhase))+FeTxLength('XXX')+10.
      tpoma(0)=5.
      do n=2,NComp(KPhase)
        write(Veta,'(i2,a2,'' composite matrix'')') n,nty(n)
        il=NDim(KPhase)+1
        id=NextQuestId()
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,1,LightGray,0,0)
        il=0
        k=0
        do i=1,NDim(KPhase)
          il=il+1
          Veta=Indices(i)//'''='
          call FeQuestLblMake(id,tpoma(0),il,Veta,'L','N')
          do j=1,NDim(KPhase)
            if(i.eq.1) then
              xpoma(j)=tpoma(j-1)+FeTxLength('XXXX')+5.
              tpoma(j)=xpoma(j)+dpom+5.
            endif
            Veta='*'//Indices(j)
            if(j.ne.NDim(KPhase)) Veta=Veta(:idel(Veta))//'+'
            call FeQuestEdwMake(id,tpoma(j),il,xpoma(j),il,Veta,'L',
     1                          dpom,EdwYd,0)
            if(i.eq.1.and.j.eq.1) nEdwMatFirst=EdwLastMade
            k=k+1
            call FeQuestRealEdwOpen(EdwLastMade,ZV(k,n,KPhase),.false.,
     1                              .true.)
          enddo
        enddo
        il=il+1
        Veta='Matrix %calculator'
        dpomp=FeTxLengthUnder(Veta)+10.
        tpom=(xqd-dpomp)*.5
        call FeQuestButtonMake(id,tpom,il,dpomp,ButYd,Veta)
        nButtMatCalc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
          nEdw=nEdwMatFirst
          k=0
          do i=1,NDim(KPhase)
            do j=1,NDim(KPhase)
              k=k+1
              call FeQuestRealFromEdw(nEdw,FMat(k))
              nEdw=nEdw+1
            enddo
          enddo
          call matinv(FMat,FMatInv,pom,NDim(KPhase))
          if(abs(pom).le..001) then
            call FeChybne(-1.,-1.,'The matrix is singular - try again.',
     1                    ' ',SeriousError)
            go to 1800
          else
            m=0
            do j=1,3
              do k=1,3
                m=m+1
                pom=FMat(k+(j-1)*NDim(KPhase))
                do l=4,NDim(KPhase)
                  pom=pom+FMat(k+(l-1)*NDim(KPhase))*qu(j,l-3,1,KPhase)
                enddo
                ZSigP(m)=pom
              enddo
            enddo
            call matinv(ZSigP,ZSigIP,pom,3)
            if(abs(pom).le.0.) then
              call FeChybne(-1.,-1.,'3x3 Z-block is singular - try '//
     1                      'again.',' ',SeriousError)
              go to 1800
            endif
            call trmat(ZSigP,PomMat,3,3)
            call multm(ZSigP,MetTensI(1,1,KPhase),smp,3,3,3)
            call multm(smp,PomMat,MTIPom,3,3,3)
            call matinv(MTIPom,MTPom,pom,3)
            if(pom.le.0.) then
              call FeChybne(-1.,-1.,'matric tensor is singular - try '//
     1                      'again.',' ',SeriousError)
              go to 1800
            endif
            m=1
            do j=1,3
              CellOut(j)=sqrt(MTPom(m))
              m=m+4
            enddo
            do j=4,6
              call indext(10-j,l,k)
              m=l+(k-1)*3
              CellOut(j)=MTPom(m)/(CellOut(l)*CellOut(k))
              if(abs(CellOut(j)).le..000001) CellOut(j)=0.
            enddo
            Vol=sqrt(1.-CellOut(4)**2-CellOut(5)**2-CellOut(6)**2+
     1               2.*CellOut(4)*CellOut(5)*CellOut(6))*
     2          CellOut(1)*CellOut(2)*CellOut(3)
            do j=4,6
              CellOut(j)=acos(CellOut(j))/torad
            enddo
            m=0
            do j=1,3
              do k=4,NDim(KPhase)
                m=m+1
                pom=FMat(k+(j-1)*NDim(KPhase))
                do l=4,NDim(KPhase)
                  pom=pom+FMat(k+(l-1)*NDim(KPhase))*qu(j,l-3,1,KPhase)
                enddo
                VSig(m)=pom
              enddo
            enddo
            call multm(VSig,ZSigIP,PomMat,NDimI(KPhase),3,3)
            Ninfo=NDim(KPhase)
            TextInfo(1)='Induced cell parameters:'
            TextInfoFlags(1)='CN'
            write(TextInfo(2),100) CellOut,Vol
            TextInfoFlags(2)='CN'
            TextInfo(3)='Induced modulation vector(s):'
            TextInfoFlags(3)='CN'
            do i=1,NDimI(KPhase)
              write(TextInfo(3+i),101)(PomMat(i+(j-1)*NDimI(KPhase)),
     1                                 j=1,3)
              TextInfoFlags(3+i)='CN'
            enddo
            if(.not.FeYesNoHeader(-1.,YBottomMessage,
     1                            'Do you want to accept the '//
     2                            'composite matrix?',0)) go to 1800
            call CopyMat(FMat,ZV(1,n,KPhase),NDim(KPhase))
            call CopyMat(FMatInv,ZVI(1,n,KPhase),NDim(KPhase))
            call CopyVek(PomMat,qu(1,1,n,KPhase),3*NDimI(KPhase))
            QuestCheck(id)=0
            go to 1500
          endif
1800      call FeQuestButtonOff(ButtonOK-ButtonFr+1)
          go to 1500
        else if(CheckNumber.eq.nButtMatCalc) then
          NRow=NDim(KPhase)
          NCol=NDim(KPhase)
          nEdw=nEdwMatFirst
          do i=1,NDim(KPhase)
            k=i
            do j=1,NDim(KPhase)
              call FeQuestRealFromEdw(nEdw,FMat(k))
              nEdw=nEdw+1
              k=k+NDim(KPhase)
            enddo
          enddo
          call MatrixCalculatorInOut(FMat,NRow,NCol,ichp)
          if(ichp.eq.0) then
            nEdw=nEdwMatFirst
            do i=1,NDim(KPhase)
              k=i
              do j=1,NDim(KPhase)
                call FeQuestRealEdwOpen(nEdw,FMat(k),.false.,.true.)
                k=k+NDim(KPhase)
                nEdw=nEdw+1
              enddo
            enddo
          endif
          go to 1500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        call FeQuestRemove(id)
      enddo
9999  return
100   format(3f9.3,3f9.2,'     Volume:',f12.3)
101   format(3f9.4)
      end
