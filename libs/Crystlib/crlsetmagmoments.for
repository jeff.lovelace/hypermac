      subroutine CrlSetMagMoments(NQMagIn)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(NQMagIn.gt.0) then
        MagParA=2
      else
        MagParA=1
      endif
      do i=1,NAtAll
        if(AtTypeMag(isf(i),KPhase).eq.' ') cycle
        MagPar(i)=MagParA
        call ReallocateAtomParams(NAtAll,itfMax,IFrMax,
     1                            KModAMax,MagParA)
        if(NQMagIn.gt.0) then
          call SetRealArrayTo( sm0(1,i),3,.0)
          call SetRealArrayTo(ssm0(1,i),3,.0)
          call SetRealArrayTo(ssmx(1,1,i),3,.0)
          call SetRealArrayTo(ssmy(1,1,i),3,.0)
          do j=1,3
            smx(j,1,i)=.1/CellPar(j,1,KPhase)
            smy(j,1,i)=.1/CellPar(j,1,KPhase)
          enddo
        else
          call SetRealArrayTo(ssm0(1,i),3,.0)
          do j=1,3
            sm0(j,i)=.1/CellPar(j,1,KPhase)
          enddo
        endif
      enddo
      return
      end
