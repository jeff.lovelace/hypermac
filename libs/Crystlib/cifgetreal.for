      subroutine CIFGetReal(CifKeySearch,a,sa,ErrSt,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256,p256
      character*(*) CifKeySearch,ErrSt
      logical loop,EqIgCase,CIFEq
      ich=0
      ErrSt=' '
      loop=.false.
      n=0
1000  call CIFNewRecord(n,t256,*9000)
      k=0
      call kus(t256,k,p256)
      if(EqIgCase(p256,'loop_')) then
        loop=.true.
        go to 1000
      endif
      if(loop) then
        if(p256(1:1).eq.'_') then
          ich=ich+1
        else
          loop=.false.
          ich=0
          go to 1000
        endif
      endif
      if(.not.CIFEq(p256,CifKeySearch)) go to 1000
      if(loop) go to 9999
      call kus(t256,k,p256)
      if(p256.eq.'?'.or.p256.eq.'''?''') then
        a=0.
c        ErrSt='?'
        go to 9000
      endif
      call GetRealEsdFromSt(p256,a,sa,ich)
      if(ich.ne.0) go to 9100
      ErrSt=' '
      go to 9999
9000  ich=-1
      ErrSt=CifKeySearch
      go to 9999
9100  ich=-2
      ErrSt=t256
9999  return
      end
