      logical function ReflFulfilsCond(ih,GroupMatrix,GroupVector,
     1                                CondIncl,ModIncl,EqualIncl,
     2                                CondExcl,ModExcl,EqualExcl)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer GroupMatrix(*),CondIncl(*),ModIncl,EqualIncl,
     1        GroupVector(*),CondExcl(*),ModExcl,EqualExcl,
     2                       ih(*),ihp(6),ihq(6)
      logical EqIV,EqIVM,EqIV0
      ReflFulfilsCond=.false.
      if(NPhase.eq.1) then
        ns=NSymm(1)
      else
        ns=1
      endif
      do is=1,ns
        if(NPhase.eq.1) then
          call MultmIRI(ih,rm6(1,is,1,1),ihp,1,NDim(1),NDim(1))
        else
          ihp=ih(1:maxNDim)
        endif
        kkk=0
1100    call MultmI(ihp,GroupMatrix,ihq,1,maxNDim,maxNDim)
        call AddVekI(ihq,GroupVector,ihq,maxNDim)
        if(.not.EqIV(ihp,ihq,maxNDim)) then
          if(kkk.gt.0) cycle
          kkk=kkk+1
          do i=1,maxNDim
            ihp(i)=-ihp(i)
          enddo
          go to 1100
        endif
        ipp=-EqualIncl
        do j=1,maxNDim
          ipp=ipp+ihp(j)*CondIncl(j)
        enddo
        if(ModIncl.ne.0) then
          if(mod(iabs(ipp),ModIncl).ne.0) cycle
        else
          if(ipp.ne.0) cycle
        endif
        if(EqIV0(CondExcl,maxNDim)) go to 1500
        ipp=-EqualExcl
        do j=1,maxNDim
          ipp=ipp+ihp(j)*CondExcl(j)
        enddo
        if(ModExcl.ne.0) then
          if(mod(iabs(ipp),ModExcl).eq.0) cycle
        else
          if(ipp.eq.0) cycle
        endif
        go to 1500
      enddo
      go to 9999
1500  ReflFulfilsCond=.true.
9999  return
      end
