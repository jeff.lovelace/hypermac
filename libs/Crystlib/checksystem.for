      subroutine CheckSystem(Cell,MonoclinicP,CrSystemP)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Cell(6)
      integer CrSystemP
      j=0
      MonoclinicP=0
      do i=1,3
        if(abs(Cell(i+3)-90.).lt..001) then
          j=j+1
        else
          MonoclinicP=i
        endif
      enddo
      if(j.ne.2) then
        MonoclinicP=0
      else
        if(MonoclinicP.ne.0) then
          if(abs(Cell(MonoclinicP+3)-120.).lt..001) then
            CrSystemP=CrSystemHexagonal
          else
            CrSystemP=MonoclinicP*10+CrSystemMonoclinic
          endif
        endif
        go to 2000
      endif
      CrSystemP=CrSystemTriclinic
      if(j.eq.3) then
        if(abs(Cell(1)-Cell(2)).gt..0001) then
          CrSystemP=CrSystemOrthorhombic
        else if(abs(Cell(1)-Cell(3)).gt..0001) then
          CrSystemP=CrSystemTetragonal
        else
          CrSystemP=CrSystemCubic
        endif
      else if(j.eq.0) then
        do i=2,3
          if(abs(Cell(1)-Cell(i)).gt..0001) go to 2000
          if(abs(Cell(4)-Cell(i+3)).gt..0001) go to 2000
        enddo
        CrSystemP=-CrSystemTrigonal
      endif
2000  return
      end
