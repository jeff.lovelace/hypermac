      subroutine ReadPDB
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension TrMat(3,3),TrShift(3),xp(5)
      character*256 Veta,FileIn
      character*60  itxti,GrupaHM,GrupaHMShort
      character*11  SpGrp
      character*8   GrupaI
      character*4   At
      character*3   Skupina
      character*2   AtTp
      character*1   AltLoc,ChainId,ICode
      integer ResSeq
      logical StructureExists,FeYesNo,ExistFile,EqIgCase
      ln=0
      lni=0
      if(StructureExists(fln)) then
        if(.not.FeYesNo(-1.,-1.,'The structure "'//fln(:ifln)//
     1                  '" already exists, rewrite it?',0)) go to 9999
      endif
      id=NextQuestId()
      xqd=300.
      Veta='Define input PDB file'
      FileIn=fln(:ifln)//'.pdb'
1120  call FeFileManager(Veta,FileIn,'*.pdb',0,.true.,ich)
      if(ich.ne.0) go to 9999
      if(.not.ExistFile(FileIn)) then
        call FeChybne(-1.,YBottomMessage,'the file "'//
     1                FileIn(:idel(FileIn))//
     2                '" does not exist, try again.',' ',SeriousError)
        go to 1120
      endif
      call CheckEOLOnFile(FileIn,2)
      if(ErrFlag.ne.0) go to 9999
      lni=NextLogicNumber()
      call OpenFile(lni,FileIn,'formatted','old')
      call SetBasicM40(.true.)
      call SetBasicM50
      call UnitMat(TrMat,3)
      TrShift(:)=0.
      SpGrp='P 1'
      call AllocateSymm(96,96,1,1)
      if(allocated(AtType))
     1  deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2             AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3             FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4             TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
      if(allocated(TypeCore))
     1  deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2             HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3             CoreValSource)
      n=20
      m=121
      i=1
      allocate(AtType(n,NPhase),AtTypeFull(n,NPhase),
     1         AtTypeMag(n,NPhase),AtTypeMagJ(n,NPhase),
     2         AtWeight(n,NPhase),AtRadius(n,NPhase),AtColor(n,NPhase),
     3         AtNum(n,NPhase),AtVal(n,NPhase),AtMult(n,NPhase),
     4         FFBasic(m,n,NPhase),
     5         FFCore(m,n,NPhase),FFCoreD(m,n,NPhase),
     6         FFVal(m,n,NPhase),FFValD(m,n,NPhase),
     7         FFra(n,NPhase,i),FFia(n,NPhase,i),
     8         FFn(n,NPhase),FFni(n,NPhase),FFMag(7,n,NPhase),
     9         TypeFFMag(n,NPhase),
     a         FFa(4,m,n,NPhase),FFae(4,m,n,NPhase),fx(n),fm(n),
     1         FFEl(m,n,NPhase),TypicalDist(n,n,NPhase))
      allocate(TypeCore(n,NPhase),TypeVal(n,NPhase),
     1         PopCore(28,n,NPhase),PopVal(28,n,NPhase),
     2         ZSlater(8,n,NPhase),NSlater(8,n,NPhase),
     3         HNSlater(n,NPhase),HZSlater(n,NPhase),
     4         ZSTOA(7,7,40,n,NPhase),CSTOA(7,7,40,n,NPhase),
     5         NSTOA(7,7,40,n,NPhase),NCoefSTOA(7,7,n,NPhase),
     6         CoreValSource(n,NPhase))
      FFCoreD=0.
      FFValD=0.
      CoreValSource='Default'
1200  read(lni,FormA,err=9000,end=1300) Veta
      k=0
      call Kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'REMARK')) then
        go to 1200
      else if(EqIgCase(Cislo,'ATOM')) then
        go to 1300
      else if(EqIgCase(Cislo,'TER')) then
        go to 1300
      else if(LocateSubstring(Veta,'CRYST',.false.,.true.).eq.1) then
        read(Veta(6:6),100,err=9000) i
        if(i.ne.1) go to 1200
        read(Veta,101)(CellPar(i,1,KPhase),i=1,6),SpGrp,NUnits(KPhase)
      else if(LocateSubstring(Veta,'SCALE',.false.,.true.).eq.1) then
        read(Veta(6:6),100,err=9000) i
        call StToReal(Veta,k,xp,4,.false.,ich)
        if(ich.ne.0) go to 9000
        TrShift(i)=xp(4)
        do j=1,3
          TrMat(i,j)=xp(j)
        enddo
      endif
      go to 1200
1300  ln=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1        'spgroup.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
      endif
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      read(ln,FormA) Veta
      if(Veta(1:1).ne.'#') rewind ln
1500  read(ln,FormA,err=9100,end=1600) Veta
      read(Veta,FormSG,err=9100) igi,ipgi,idli,GrupaI,itxti,
     1                           GrupaHM,GrupaHM,GrupaHMShort
      if(EqIgCase(GrupaHM,SpGrp).or.EqIgCase(GrupaHMShort,SpGrp)) then
        Grupa(KPhase)=GrupaI
        go to 1610
      endif
      go to 1500
1600  Grupa(KPhase)='P1'
1610  call CloseIfOpened(ln)
      call EM50GenSym(RunForFirstTimeNo,AskForDeltaNo,xp,ich)
      MaxNSymm=max(MaxNSymm,NSymm(KPhase))
      MaxNSymmN=max(MaxNSymmN,NSymmN(KPhase))
      nac=0
      call AllocateAtoms(1000)
      rewind lni
2000  read(lni,FormA,err=9000,end=2200) Veta
      k=0
      call Kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'END')) then
        go to 2200
      else if(EqIgCase(Cislo,'ATOM')) then
        read(Veta,102) n,At,AltLoc,Skupina,ChainId,ResSeq,ICode,xp,
     1                 AtTp
        call Zhusti(AtTp)
        if(nac.ge.MxAtAll) call ReallocateAtoms(1000)
        nac=nac+1
        call SetBasicKeysForAtom(nac)
        isf(nac)=KtAt(AtType(1,KPhase),NAtFormula(KPhase),AtTp)
        if(isf(nac).le.0) then
          NAtFormula(KPhase)=NAtFormula(KPhase)+1
          isf(nac)=NAtFormula(KPhase)
          AtMult(isf(nac),KPhase)=0.
          MaxNAtFormula=max(MaxNAtFormula,NAtFormula(KPhase))
          call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
          AtTypeFull(NAtFormula(KPhase),KPhase)=AtTp
          AtTypeMag(NAtFormula(KPhase),KPhase)=' '
          AtTypeMagJ(NAtFormula(KPhase),KPhase)=' '
          call Uprat(AtTypeFull(NAtFormula(KPhase),KPhase))
          call GetPureAtType(AtTypeFull(NAtFormula(KPhase),KPhase),
     1                       AtType(NAtFormula(KPhase),KPhase))
          FFType(KPhase)=-62
          call EM50ReadOneFormFactor(NAtFormula(KPhase))
          call EM50OneFormFactorSet(NAtFormula(KPhase))
          j=LocateInStringArray(atn,98,
     1      AtType(NAtFormula(KPhase),KPhase),IgnoreCaseYes)
          if(j.gt.0) then
            AtNum(NAtFormula(KPhase),KPhase)=float(j)
          else
            AtNum(NAtFormula(KPhase),KPhase)=0.
          endif
        endif
        write(Cislo,FormI15) n
        call Zhusti(Cislo)
        Atom(nac)=AtTp(:idel(AtTp))//Cislo(:idel(Cislo))
        kswa(nac)=KPhase
        AtSiteMult(nac)=1
        call uprat(atom(nac))
        call AddVek(xp,TrShift,xp,3)
        call MultM(TrMat,xp,x(1,nac),3,3,1)
        call SpecPos(x(1,nac),xp,0,1,.01,i)
        pom=1./float(i)
        beta(1,nac)=xp(5)
        ai(nac)=pom*xp(4)
        AtMult(isf(nac),KPhase)=AtMult(isf(nac),KPhase)+ai(nac)
        if(nac.gt.1) then
          PrvniKiAtomu(nac)=PrvniKiAtomu(nac-1)+mxda
        else
          PrvniKiAtomu(nac)=ndoff+1
        endif
        DelkaKiAtomu(nac)=10
        call SetIntArrayTo(KiA(1,nac),mxda,0)
        if(KPhase.gt.1) then
          NAtIndLen(1,KPhase)=nac-NAtIndToAll(KPhase-1)
        else
          NAtIndLen(1,KPhase)=nac
        endif
        call EM40UpdateAtomLimits
      endif
      go to 2000
2200  call SetFormula(Formula(KPhase))
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom40(1,0,fln(:ifln)//'.m40')
      go to 9999
9000  call FeReadError(lni)
      go to 9999
9100  call FeReadError(ln)
      go to 9999
9999  call CloseIfOpened(lni)
      call CloseIfOpened(ln)
      return
100   format(i1)
101   format(6x,3f9.3,3f7.2,1x,a11,i4)
102   format(6x,i5,1x,a4,a1,a3,1x,a1,i4,a1,3x,3f8.3,2f6.2,10x,2a2)
      end
