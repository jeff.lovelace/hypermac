      subroutine SetOrM(x40,delta,gor,n,natp)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension gor(*)
      x1=x40-.5*delta
      x2=x40+.5*delta
      jip=1
      do i=1,n
        if(i.eq.1) then
          ii=1
        else
          ni=mod(i,2)
          nj=i/2
          ii=nj*2+ni
        endif
        ij=i
        ji=jip
        do j=1,i
          if(j.eq.1) then
            jj=1
          else
            ni=mod(j,2)
            nj=j/2
            jj=nj*2+ni
          endif
          pom=csprod(ii,jj,x1,x2,natp)/delta
          if(i.ne.j) gor(ij)=pom
          gor(ji)=pom
          ji=ji+1
          ij=ij+n
        enddo
        jip=jip+n
      enddo
      return
      end
