      subroutine CrlExportXD
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 Veta,OutputFile,EdwStringQuest
      integer Exponent10
      logical FeYesNo,ExistFile,SystExtRef
      real so(3,2),xp(3),MT(3,3),MTI(3,3),e1(3),e2(3)
      lni=0
      lno=0
      id=NextQuestId()
      il=1
      xqd=300.
      Veta='Specify output file:'
      OutputFile=' '
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      bpom=50.
      xpom=5.
      tpom=5.
      dpom=xqd-bpom-20.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,OutputFile)
      Veta='%Browse'
      xpom=tpom+dpom+10.
      call FeQuestButtonMake(id,xpom,il,bpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        OutputFile=EdwStringQuest(nEdwName)
        call FeFileManager('Select output structure',OutputFile,'*.hkl',
     1                      0,.true.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwName,OutputFile)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        OutputFile=EdwStringQuest(nEdwName)
        if(ExistFile(OutputFile)) then
          if(.not.FeYesNo(-1.,YBottomMessage,'The file "'//
     1       OutputFile(:idel(OutputFile))//
     2       '" already exists, overwrite it?',0)) then
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)

            go to 1500
          endif
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      MT(1,1)=1.
      MT(2,2)=1.
      MT(3,3)=1.
      MT(1,2)=cos(CellPar(6,1,KPhase)*ToRad)
      MT(2,1)=MT(1,2)
      MT(1,3)=cos(CellPar(5,1,KPhase)*ToRad)
      MT(3,1)=MT(1,3)
      MT(2,3)=cos(CellPar(4,1,KPhase)*ToRad)
      MT(3,2)=MT(2,3)
      call MatInv(MT,MTi,det,3)
      call iom95(0,fln(:ifln)//'.m95')
      KDatBlock=1
      lni=NextLogicNumber()
      call OpenRefBlockM95(lni,KDatBlock,fln(:ifln)//'.m95')
      lno=NextLogicNumber()
      call OpenFile(lno,OutputFile,'formatted','unknown')
      RiMax=0.
      RsMax=0.
2000  call DRGetReflectionFromM95(lni,iend,ich)
      if(iend.ne.0.or.ich.ne.0) go to 2500
      if(SystExtRef(ih,.false.,1)) go to 2000
      pom1=corrf(1)*corrf(2)
      if(RunScN(KRefBlock).gt.0) then
        ikde=RunScGrN(RunCCD,KRefBlock)
        pom1=pom1*ScFrame(ikde,KRefBlock)
      endif
      Ri=Ri*pom1
      Rs=Rs*pom1
      RiMax=max(Ri,RiMax)
      RsMax=max(Rs,RsMax)
      go to 2000
2500  pom=max(RiMax/9999999.99,RsMax/99999.9)
      k=Exponent10(pom)+2
      if(k.gt.0) then
        scp=1./10**k
      else
        scp=1.
      endif
      call CloseIfOpened(lni)
      call OpenRefBlockM95(lni,KDatBlock,fln(:ifln)//'.m95')
      Veta=fln
      Veta(ifln+10:)='F^2        NDAT   13'
      write(lno,FormA) Veta(:idel(Veta))
3000  call DRGetReflectionFromM95(lni,iend,ich)
      if(SystExtRef(ih,.false.,1)) go to 3000
      pom1=corrf(1)*corrf(2)*scp
      if(RunScN(KRefBlock).gt.0) then
        ikde=RunScGrN(RunCCD,KRefBlock)
        pom1=pom1*ScFrame(ikde,KRefBlock)
      endif
      Ri=Ri*pom1
      Rs=Rs*pom1
      if(iend.ne.0.or.ich.ne.0) go to 9999
      do i=1,3
        xp(i)=rcp(i,1,KPhase)*CellPar(i,1,KPhase)
      enddo
      do j=1,2
        if(j.eq.1) then
          pom=-1.
        else
          pom=1.
        endif
        do k=1,3
          so(k,j)=pom*DirCos(k,j)*xp(k)
        enddo
      enddo
      call VecMul(so(1,1),so(1,2),xp)
      call MultM(MTi,xp,e1,3,3,1)
      call VecNor(e1,xp)
      call VecMul(e1,so(1,1),xp)
      call MultM(MTi,xp,e2,3,3,1)
      call VecNor(e2,xp)
      write(lno,'(3i4,i2,f10.2,f7.1,1x,f5.4,6(1x,f7.4))') ih(1:3),1,
     1  ri,rs,tbar,e1,e2
      go to 3000
9999  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      return
      end
