      integer function CrlSubSystemNumber(ih)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ih(6),ihp(6)
      if(NDimI(KPhase).gt.0) then
        CrlSubSystemNumber=0
        do 2000i=1,NComp(KPhase)
          call indtr(ih,zvi(1,i,KPhase),ihp,NDim(KPhase))
          do j=4,NDim(KPhase)
            if(iabs(ihp(j)).gt.0) go to 2000
          enddo
          if(CrlSubSystemNumber.gt.0) then
            CrlSubSystemNumber=-1
          else
            CrlSubSystemNumber=i
          endif
2000    continue
      else
        CrlSubSystemNumber=1
      endif
9999  return
      end
