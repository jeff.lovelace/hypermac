      subroutine StrToStandSetting
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(6)
      do 1000i=1,NAtCalc
        if(kswa(i).ne.KPhase) go to 1000
        call AddVek(x(1,i),ShSg(1,KPhase),x(1,i),3)
1000  continue
      do i=1,NSymm(KPhase)
        call Multm(rm6(1,i,1,KPhase),ShSg(1,KPhase),xp,NDim(KPhase),
     1             NDim(KPhase),1)
        do j=1,NDim(KPhase)
          s6(j,i,1,KPhase)=s6(j,i,1,KPhase)-xp(j)+ShSg(j,KPhase)
        enddo
      enddo
9999  return
      end
