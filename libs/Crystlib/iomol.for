      subroutine iomol(klic,tisk)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension px(9),rpgp(9,48),trp(9),trpi(9),XCentr(3)
      dimension pp(3),pt(3),uoa(3,2)
      character*80 Veta,s80
      character*27 t27(2)
      character*8 at
      character*3 t3
      integer tisk,PrvniKiM,OldM40
      logical eqrv,EqIgCase,StareOrtho
      save PrvniKiM,iak,iakp
      if(KPhase.le.1) then
        if(klic.eq.0) PrvniKiM=1
        iak=NAtMolFr(1,1)-1
        iakp=iak
      endif
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        if(i.ge.NMolecFr(1,KPhase).and.i.le.NMolecFr(1,KPhase)) then
          isw=1
        else if(i.ge.NMolecFr(2,KPhase).and.i.le.NMolecFr(2,KPhase))
     1    then
          isw=2
        else if(i.ge.NMolecFr(3,KPhase).and.i.le.NMolecFr(3,KPhase))
     1    then
          isw=3
        endif
        iswmol(i)=isw
        kswmol(i)=KPhase
        iap=iak+1
        if(klic.eq.0) then
          NPoint(i)=1
          KPoint(i)=1
          iak=iak+iam(i)
          read(m40,100,err=9100,end=9100) molname(i),RefPoint(i),
     1                                    SmbPGMol(i),t27(1)
          call uprat(molname(i))
          call zhusti(SmbPGMol(i))
          if(SmbPGMol(i).eq.' ') SmbPGMol(i)='1'
          StRefPoint(i)=' '
          read(t27(1),103,err=1010)(xm(k,i),k=1,3)
          go to 1050
1010      m=0
          call kus(t27(1),m,at)
          call uprat(at)
          StRefPoint(i)=at
1050      UsePGSyst(i)=.false.
          LocPGSystAx(i)='xy'
          LocPGSystSt(1,i)=' 1.000000 0.000000 0.000000'
          LocPGSystSt(2,i)=' 0.000000 1.000000 0.000000'
          call SetRealArrayTo(LocPGSystX(1,1,i),6,0.)
          LocPGSystX(1,1,i)=1.
          LocPGSystX(2,2,i)=1.
          if(SmbPGMol(i).ne.' ') then
            j=idel(SmbPGMol(i))
            if(SmbPGMol(i)(j:j).eq.'*') then
              SmbPGMol(i)(j:j)=' '
              UsePGSyst(i)=.true.
              read(m40,107,err=9100,end=9100)
     1          (LocPGSystSt(m,i),m=1,2),LocPGSystAx(i)
            endif
          endif
          if(tisk.eq.1) then
            call newln(1)
            write(lst,100) molname(i),RefPoint(i),SmbPGMol(i),t27(1)
          endif
        else
          iak=iak+iam(i)/KPoint(i)
          if(StRefPoint(i).eq.' ') then
            write(t27(1),103)(xm(j,i),j=1,3)
          else
            t27(1)=' '//StRefPoint(i)
          endif
          At=SmbPGMol(i)
          if(UsePGSyst(i)) then
            j=idel(At)
            At=At(:j)//'*'
          endif
          write(m40,100) molname(i),RefPoint(i),At,t27(1)
          if(UsePGSyst(i))
     1      write(m40,107)(LocPGSystSt(m,i),m=1,2),LocPGSystAx(i)
        endif
        call ioat(iap,iak,isw,(i-1)*mxp+1,klic,tisk)
        if(ErrFlag.ne.0) go to 9100
        if(klic.eq.0) then
          if(StRefPoint(i).ne.' ') then
            m=0
            call kus(StRefPoint(i),m,at)
            StRefPoint(i)=at
            k=ktat(atom(iap),iam(i),at)
            if(k.le.0) go to 9000
            k=k+iap-1
            call CopyVek(x(1,k),xm(1,i),3)
          endif
        endif
        if(SmbPGMol(i).ne.' ') then
          call CrlMakeTrMatToLocal(xm(1,i),molname(i),
     1                     LocPGSystAx(i),LocPGSystSt(1,i),' ',
     2                     LocPGSystX(1,1,i),
     3                     TrPG(1,i),TriPG(1,i),i,ich)
          if(ich.ne.0) go to 9100
          do j=1,2
            if(LocPGSystSt(j,i)(1:1).ne.'-')
     1        LocPGSystSt(j,i)=' '//
     2                         LocPGSystSt(j,i)(:idel(LocPGSystSt(j,i)))
          enddo
          call GenPg(SmbPGMol(i),rpoint(1,1,i),npoint(i),ich)
          if(ich.ne.0) then
            j=0
            Veta='unknown point group symbol : '//At(:j)
            go to 9090
          endif
          do l=1,npoint(i)
            call multm(TriPG(1,i),rpoint(1,l,i),px,3,3,3)
            call multm(px,TrPG(1,i),rpoint(1,l,i),3,3,3)
          enddo
        endif
        KTLS(i)=0
        do j=iap,iak
          if(itf(j).eq.0.and.ai(j).ne.0.) KTLS(i)=KTLS(i)+1
        enddo
        do j=1,mam(i)
          ji=j+mxp*(i-1)
          call CopyVek(TrToOrtho(1,isw,KPhase),TrMol(1,ji),9)
          call CopyVek(TrToOrthoI(1,isw,KPhase),TriMol(1,ji),9)
          if(klic.eq.0) then
            if(MaxNDimI.gt.0) then
              OrthoX40Mol(ji)=0.
              OrthoDeltaMol(ji)=1.
              OrthoEpsMol(ji)=.95
            endif
            PrvniKiMolekuly(ji)=PrvniKiM
            read(m40,105,err=9100,end=9100) Veta(1:8),RotSign(ji),t3,
     1        aimol(ji),(KFM(k,ji),k=1,3),(KModM(k,ji),k=1,3)
            if(NDimI(KPhase).le.0) then
              call SetIntArrayTo(KFM(1,ji),3,0)
              call SetIntArrayTo(KModM(1,ji),3,0)
            endif
            call ReallocateMoleculeParams(i,j-1,KTLS(i),KModM(1,ji))
            call SetIntArrayTo(KiMol(1,ji),mxdm,0)
            read(m40,106,err=9100,end=9100)
     1        (euler(k,ji),k=1,3),t27(1),(KiMol(k,ji),k=1,7)
            if(t3.eq.' ') then
              OldM40=1
            else
              OldM40=0
              read(t3,'(i3)') LocMolSystType(ji)
            endif
            if(RotSign(ji).ge.0) then
              RotSign(ji)=1
            else
              RotSign(ji)=-1
            endif
            if(NDim(KPhase).gt.3.and.NonModulated(KPhase)) then
              NonModulated(KPhase)=NonModulated(KPhase).and.
     1                             KModM(1,ji).le.0
              NonModulated(KPhase)=NonModulated(KPhase).and.
     1                             KModM(2,ji).le.0
              NonModulated(KPhase)=NonModulated(KPhase).and.
     1                             KModM(3,ji).le.0
            endif
            AtTrans(ji)=' '
            read(t27(1),103,err=1210)(trans(k,ji),k=1,3)
            go to 1230
1210        m=0
            call kus(t27(1),m,at)
            call uprat(at)
            k=ktat(Atom,NAtInd,at)
            if(k.le.0) then
              go to 9000
            else
              if(kswa(k).ne.KPhase) go to 9000
              AtTrans(ji)=at
            endif
1230        if(tisk.eq.1) then
              call newln(2)
              write(lst,102) Veta(1:8),RotSign(ji),LocMolSystType(ji),
     1                       aimol(ji),(KFM(k,ji),k=1,3),
     2                       (KModM(k,ji),k=1,3),
     3                       (euler(k,ji),k=1,3),t27(1),
     4                       (KiMol(k,ji),k=1,7)
            endif
            LocMolSystAx(1,ji)='zy'
            LocMolSystSt(1,1,ji)=' 0.000000 0.000000 1.000000'
            LocMolSystSt(2,1,ji)=' 0.000000 1.000000 0.000000'
            call SetRealArrayTo(LocMolSystX(1,1,1,ji),12,0.)
            LocMolSystX(1,1,1,ji)=1.
            LocMolSystX(2,2,1,ji)=1.
            LocMolSystAx(2,ji)=LocMolSystAx(1,ji)
            LocMolSystSt(1,2,ji)=LocMolSystSt(1,1,ji)
            LocMolSystSt(2,2,ji)=LocMolSystSt(2,1,ji)
            LocMolSystX(1,1,2,ji)=1.
            LocMolSystX(2,2,2,ji)=1.
            if(OldM40.eq.1) then
              RefPoint(i)=0
              read(m40,FormA,err=9100,end=9100) Veta
              call mala(Veta)
              if(tisk.eq.1) then
                call newln(1)
                write(lst,FormA) '=>'//Veta(:idel(Veta))//'<='
              endif
              t27(1)=Veta(:27)
              t27(2)=Veta(28:54)
              LocMolSystType(ji)=0
              do l=1,2
                LocMolSystAx(l,ji)=' '
                read(t27(l),103,err=1250)(uoa(k,l),k=1,3)
                LocMolSystSt(1,l,ji)=t27(l)
                go to 1300
1250            m=0
                LocMolSystAx(l,ji)='zx'
                do kk=1,2
                  call kus(t27(l),m,at)
                  call uprat(at)
                  if(l.eq.1) then
                    k=ktat(atom(iap),iam(i),at)+iap-1
                  else
                    k=ktat(Atom,NAtInd,at)
                  endif
                  if(k.le.0) then
                    go to 9000
                  else
                    if(kswa(k).ne.KPhase) go to 9000
                  endif
                  if(kk.eq.1) then
                    LocMolSystSt(1,l,ji)=' '//At(:idel(At))
                    call CopyVek(x(1,k),uoa(1,l),3)
                  else
                    LocMolSystSt(1,l,ji)=
     1                LocMolSystSt(1,l,ji)(:idel(LocMolSystSt(1,l,ji)))
     2                //' - '//at(:idel(at))
                    do m=1,3
                      uoa(m,l)=uoa(m,l)-x(m,k)
                    enddo
                  endif
                enddo
1300            if(abs(uoa(1,l))+abs(uoa(2,l))+abs(uoa(3,l)).gt.0.) then
                  call transtr(uoa(1,l),TrToOrtho(1,isw,KPhase),trp)
                  if(LocMolSystAx(l,ji).eq.' ') LocMolSystAx(l,ji)='zx'
                  call matinv(trp,trpi,pom,3)
                  pp(1)=1.
                  pp(2)=0.
                  pp(3)=0.
                  call multm(trpi,pp,pt,3,3,1)
                  pom=0.
                  do m=1,3
                    aptm=abs(pt(m))
                    if(aptm.gt.pom) pom=aptm
                  enddo
                  do m=1,3
                    pt(m)=pt(m)/pom
                  enddo
                  write(Veta,103) pt
                  LocMolSystSt(2,l,ji)=Veta
                  LocMolSystType(ji)=l
                endif
              enddo
            else if(OldM40.eq.0) then
              if(LocMolSystType(ji).gt.0) then
                read(m40,107,err=9100,end=9100)
     1           (LocMolSystSt(m,1,ji),m=1,2),LocMolSystAx(1,ji)
                if(LocMolSystType(ji).gt.1) then
                  read(m40,107,err=9100,end=9100)
     1              (LocMolSystSt(m,2,ji),m=1,2),LocMolSystAx(2,ji)
                else
                  LocMolSystAx(2,ji)=LocMolSystAx(1,ji)
                  LocMolSystSt(1,2,ji)=LocMolSystSt(1,1,ji)
                  LocMolSystSt(2,2,ji)=LocMolSystSt(2,1,ji)
                endif
              endif
            endif
            go to 2000
          else
            write(Veta,'(''pos#'',i2)') j
            call zhusti(Veta)
            if(AtTrans(ji).eq.' ') then
              write(t27(1),103)(trans(k,ji),k=1,3)
            else
              write(t27(1),FormA) ' '//
     1          AtTrans(ji)(:idel(AtTrans(ji)))
            endif
            if(NDimI(KPhase).gt.0) then
              write(m40,101) Veta(1:8),RotSign(ji),LocMolSystType(ji),
     1                       aimol(ji),(KFM(k,ji),k=1,3),
     2                       (KModM(k,ji),k=1,3)
            else
              write(m40,101) Veta(1:8),RotSign(ji),LocMolSystType(ji),
     1                       aimol(ji)
            endif
            write(m40,106)(euler(k,ji),k=1,3),t27(1),(KiMol(k,ji),k=1,7)
            do l=1,LocMolSystType(ji)
              write(m40,107)(LocMolSystSt(m,l,ji),m=1,2),
     1                       LocMolSystAx(l,ji)
            enddo
          endif
2000      kip=8
          if(AtTrans(ji).ne.' ') then
            m=0
            call kus(AtTrans(ji),m,at)
            AtTrans(ji)=at
            k=ktat(atom,NAtInd,at)
            if(k.le.0) go to 9000
            do l=1,3
              trans(l,ji)=x(l,k)-xm(l,i)
            enddo
          endif
          s80=molname(i)(:idel(molname(i)))
          write(Cislo,'(i2)') j
          call zhusti(Cislo)
          s80=s80(:idel(s80))//'#'//Cislo(:idel(Cislo))
          do l=1,2
            if(l.eq.1) then
              call CopyVek(xm(1,i),XCentr,3)
            else
              call AddVek(XCentr,trans(1,ji),XCentr,3)
            endif
            if(LocMolSystType(ji).ge.l) then
              if(l.eq.1) then
                im=i
              else
                im=0
              endif
              call CrlMakeTrMatToLocal(XCentr,s80,LocMolSystAx(l,ji),
     1                    LocMolSystSt(1,l,ji),' ',
     2                    LocMolSystX(1,1,l,ji),trp,trpi,im,ich)
              if(ich.ne.0) go to 9099
              if(l.eq.1) then
                call CopyMat(trp,TrMol(1,ji),3)
                call CopyMat(trpi,TriMol(1,ji),3)
              else
                call CopyMat(trpi,TriMol(1,ji),3)
              endif
            endif
          enddo
          if(KTLS(i).gt.0) then
            if(Klic.eq.1) call CrlSetTraceS(ts(1,ji),isw)
            call iost(m40,tt(1,ji),KiMol(kip,ji),6,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+6
            call iost(m40,tl(1,ji),KiMol(kip,ji),6,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+6
            call iost(m40,ts(1,ji),KiMol(kip,ji),9,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+8
            if(Klic.eq.0) call CrlSetTraceS(ts(1,ji),isw)
            KiMol(kip,ji)=0
            kip=kip+1
          endif
          if(KModM(1,ji).gt.0) then
            StareOrtho=.false.
            if(Klic.eq.0) then
              read(m40,FormA) Veta
              k=0
              call StToReal(Veta,k,a0m(ji),1,.false.,ich)
              if(ich.ne.0) go to 9100
              if(k.lt.60) then
                call Kus(Veta,k,Cislo)
                OrthoEpsMol(ji)=.95
                if(EqIgCase(Cislo,'Ortho')) then
                  TypeModFunMol(ji)=1
                  call StToReal(Veta,k,OrthoEpsMol(ji),1,.false.,ich)
                  if(ich.ne.0) go to 9100
                else if(EqIgCase(Cislo,'Legendre')) then
                  TypeModFunMol(ji)=2
                else if(EqIgCase(Cislo,'XHarm')) then
                  TypeModFunMol(ji)=3
                else
                  TypeModFunMol(ji)=0
                endif
              else
                TypeModFunMol(ji)=0
                do kk=1,nor
                  if(EqIgCase(s80,ora(kk))) then
                    TypeModFunMol(ji)=1
                    OrthoX40Mol(ji)=orx40(kk)
                    OrthoDeltaMol(ji)=ordel(kk)
                    OrthoEpsMol(ji)=oreps(kk)
                    StareOrtho=.true.
                    exit
                  endif
                enddo
              endif
              call StToInt(Veta,k,kiMol(kip,ji),1,.false.,ich)
              if(.not.StareOrtho) OrthoDeltaMol(ji)=a0m(ji)
              sa0m(ji)=0.
            else
              if(TypeModFunMol(ji).eq.1) then
                Cislo='Ortho'
              else if(TypeModFunMol(ji).eq.2) then
                Cislo='Legendre'
              else if(TypeModFunMol(ji).eq.3) then
                Cislo='XHarm'
              else
                Cislo=' '
              endif
              if(TypeModFunMol(ji).eq.1) then
                write(Veta,109) a0m(ji),Cislo(1:8),OrthoEpsMol(ji),
     1            KiMol(kip,ji)
              else
                write(Veta,108) a0m(ji),Cislo(1:8),KiMol(kip,ji)
              endif
              write(m40,FormA) Veta(:idel(Veta))
            endif
            kip=kip+1
            if(KFM(1,ji).ne.0) SoucinAy=1.
            do k=1,KModM(1,ji)
              if(klic.ne.0) then
                px(1)=axm(k,ji)
                px(2)=aym(k,ji)
              endif
              call iost(m40,px,KiMol(kip,ji),2,klic,tisk)
              if(k.eq.KModM(1,ji).and.Klic.eq.0.and..not.StareOrtho)
     1           OrthoX40Mol(ji)=px(1)
              if(ErrFlag.ne.0) go to 9100
              kip=kip+2
              if(klic.eq.0) then
                axm(k,ji)=px(1)
                aym(k,ji)=px(2)
                saxm(k,ji)=0.
                saym(k,ji)=0.
              endif
              if(KFM(1,ji).ne.0.and.k.le.NDimI(KPhase).and.
     1           NDimI(KPhase).gt.1) SoucinAy=SoucinAy*aym(k,ji)
            enddo
            if(KFM(1,ji).ne.0.and.KModM(1,ji).ge.NDimI(KPhase)) then
              if(NDim(KPhase).gt.4) then
                if(abs(SoucinAy).lt..0001) then
                  call SetRealArrayTo(aym(KModM(1,ji)-NDim(KPhase)-2,
     1                                                            ji),
     2              NDimI(KPhase),a0m(ji)**(1./float(NDimI(KPhase))))
                else
                  a0m(ji)=SoucinAy
                endif
              else if(NDim(KPhase).eq.4) then
                aym(KModM(1,ji),ji)=0.
              endif
            endif
          else
            TypeModFunMol(ji)=0
            a0m(ji)=1.
            sa0m(ji)=0.
          endif
          do k=1,KModM(2,ji)
            if(klic.ne.0) then
              do l=1,3
                px(l)=utx(l,k,ji)
                px(l+3)=uty(l,k,ji)
              enddo
            endif
            call iost(m40,px,KiMol(kip,ji),6,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+6
            if(klic.eq.0) then
              do l=1,3
                utx(l,k,ji)=px(l)
                uty(l,k,ji)=px(l+3)
                sutx(l,k,ji)=0.
                suty(l,k,ji)=0.
              enddo
            endif
          enddo
          do k=1,KModM(2,ji)
            if(klic.ne.0) then
              do l=1,3
                px(l)=urx(l,k,ji)
                px(l+3)=ury(l,k,ji)
              enddo
            endif
            call iost(m40,px,KiMol(kip,ji),6,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+6
            if(klic.eq.0) then
              do l=1,3
                urx(l,k,ji)=px(l)
                ury(l,k,ji)=px(l+3)
                surx(l,k,ji)=0.
                sury(l,k,ji)=0.
              enddo
            endif
          enddo
          do k=1,KModM(3,ji)
            call iost(m40,ttx(1,k,ji),KiMol(kip,ji),6,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+6
            call iost(m40,tty(1,k,ji),KiMol(kip,ji),6,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+6
            call SetRealArrayTo(sttx(1,k,ji),6,0.)
            call SetRealArrayTo(stty(1,k,ji),6,0.)
          enddo
          do k=1,KModM(3,ji)
            call iost(m40,tlx(1,k,ji),KiMol(kip,ji),6,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+6
            call iost(m40,tly(1,k,ji),KiMol(kip,ji),6,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            kip=kip+6
            call SetRealArrayTo(stlx(1,k,ji),6,0.)
            call SetRealArrayTo(stly(1,k,ji),6,0.)
          enddo
          do k=1,KModM(3,ji)
            if(klic.eq.1) then
              call CrlSetTraceS(tsx(1,k,ji),isw)
              call CrlSetTraceS(tsy(1,k,ji),isw)
            endif
            call iost(m40,tsx(1,k,ji),KiMol(kip,ji),9,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            KiMol(kip+8,ji)=0
            kip=kip+9
            call iost(m40,tsy(1,k,ji),KiMol(kip,ji),9,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            KiMol(kip+8,ji)=0
            kip=kip+9
            if(klic.eq.0) then
              call CrlSetTraceS(tsx(1,k,ji),isw)
              call CrlSetTraceS(tsy(1,k,ji),isw)
            endif
            call SetRealArrayTo(stsx(1,k,ji),9,0.)
            call SetRealArrayTo(stsy(1,k,ji),9,0.)
          enddo
          if(KModM(1,ji).gt.0.or.KModM(2,ji).gt.0.or.
     1       KModM(3,ji).gt.0) then
            call iost(m40,phfm(ji),KiMol(kip,ji),1,klic,tisk)
            if(ErrFlag.ne.0) go to 9100
            sphfm(ji)=0.
            kip=kip+1
          else if(NDimI(KPhase).gt.0) then
             phfm(ji)=0.
            sphfm(ji)=0.
          endif
          call CrlSetRotMat(ji)
          if(klic.eq.0) then
            PrvniKiM=PrvniKiM+kip-1
            DelkaKiMolekuly(ji)=kip-1
          endif
          if(SmbPGMol(i).ne.' '.and.j.eq.1) then
            if(klic.eq.0) then
              imp=(i-1)*mxp+1
              call AddVek(xm(1,i),trans(1,ji),px,3)
              call SpecPg(px,rpgp,isw,n)
              call SetIntArrayTo(ipoint(1,i),npoint(i),1)
              do k=2,n
                call multm(rpgp(1,k),RotMol(1,imp),px,3,3,3)
                call multm(RotIMol(1,imp),px,rpgp(1,k),3,3,3)
                do 2800l=1,npoint(i)
                  if(ipoint(l,i).le.0) cycle
                  call multm(rpgp(1,k),rpoint(1,l,i),px,3,3,3)
                  do m=2,npoint(i)
                    if(ipoint(m,i).le.0) cycle
                    if(eqrv(px,rpoint(1,m,i),9,.0001)) then
                      ipoint(m,i)=0
                      go to 2800
                    endif
                  enddo
2800            continue
              enddo
              k=0
              do l=1,npoint(i)
                if(ipoint(l,i).gt.0) k=k+1
                call srotb(rpoint(1,l,i),rpoint(1,l,i),tpoint(1,l,i))
                call multm(rpoint(1,l,i),xm(1,i),spoint(1,l,i),3,3,1)
                do m=1,3
                  spoint(m,l,i)=xm(m,i)-spoint(m,l,i)
                enddo
              enddo
              kpoint(i)=k
              iam(i)=iam(i)*k
            endif
          endif
        enddo
        if(klic.eq.0) then
          do j=mam(i)+1,mxp
            ji=j+mxp*(i-1)
            PrvniKiMolekuly(ji)=PrvniKiM
            DelkaKiMolekuly(ji)=0
          enddo
          PosledniKiMol=PrvniKiM-1
        else
          iak=iak+iam(i)/KPoint(i)*(KPoint(i)-1)
        endif
      enddo
      iak=iakp
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        iap=iak+1
        iak=iak+iam(i)/KPoint(i)
        if(SmbPGMol(i).eq.'1') cycle
        if(i.ge.NMolecFr(1,KPhase).and.i.le.NMolecFr(1,KPhase)) then
          isw=1
        else if(i.ge.NMolecFr(2,KPhase).and.i.le.NMolecFr(2,KPhase))
     1    then
          isw=2
        else if(i.ge.NMolecFr(3,KPhase).and.i.le.NMolecFr(3,KPhase))
     1    then
          isw=3
        endif
        n=iak
        nd=0
        kid=0
        j=0
        NAtCalcIn=NAtCalc
        PrvniKi=PrvniKiAtomu(n)+DelkaKiAtomu(n)
        do k=2,npoint(i)
          if(ipoint(k,i).le.0) cycle
          j=j+1
          write(at,'(''/'',i3)') j
          call zhusti(at)
          id=idel(at)
          do l=iap,iak
            n=n+1
            if(Klic.eq.0) then
              nd=nd+1
              if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
              NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+1
              call AtSun(n,NAtAll,n+1)
              call EM40UpdateAtomLimits
              NAtCalc=NAtCalcIn
            endif
            call CopyBasicKeysForAtom(l,n)
            atom(n)=atom(n)(:min(idel(atom(n)),7-id))//at(:id)
            call SetIntArrayTo(KiA(1,n),mxda,0)
            if(klic.eq.0) then
              DelkaKiAtomu(n)=DelkaKiAtomu(l)
              PrvniKiAtomu(n)=PrvniKi
              PrvniKi=PrvniKi+DelkaKiAtomu(l)
              kid=kid+DelkaKiAtomu(l)
            endif
            do m=1,3
              px(m)=x(m,l)-xm(m,i)
              x(m,n)=xm(m,i)
            enddo
            call cultm(rpoint(1,k,i),px,x(1,n),3,3,1)
            if(itf(n).gt.1) then
              call multm (tpoint(1,k,i), beta(1,l), beta(1,n),6,6,1)
            else
               beta(1,n)= beta(1,l)
            endif
            if(KModA(1,n).gt.0) then
              do m=1,KModA(1,n)
                ax(m,n)=ax(m,l)
                ay(m,n)=ay(m,l)
              enddo
            endif
            do m=1,KModA(2,n)
              call multm(rpoint(1,k,i),ux(1,m,l),ux(1,m,n),3,3,1)
              call multm(rpoint(1,k,i),uy(1,m,l),uy(1,m,n),3,3,1)
            enddo
            do m=1,KModA(3,n)
              call multm(tpoint(1,k,i),bx(1,m,l),bx(1,m,n),6,6,1)
              call multm(tpoint(1,k,i),by(1,m,l),by(1,m,n),6,6,1)
            enddo
            if(KModA(1,n).gt.0.or.KModA(2,n).gt.0.or.KModA(3,n).gt.0)
     1         phf(n)=phf(l)
          enddo
        enddo
        if(nd.gt.0) then
          do k=iak+nd+1,NAtAll
            PrvniKiAtomu(k)=PrvniKiAtomu(k)+kid
          enddo
          nn=nd*mam(i)
          if(NAtAll+nn.gt.MxAtAll) call ReallocateAtoms(nn)
          NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+nn
          call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+nn)
          call EM40UpdateAtomLimits
          NAtCalc=NAtCalcIn
        else
          nn=0
        endif
        if(RefPoint(i).gt.0) then
          Veta=StRefPoint(i)
          if(Veta.eq.' ') write(Veta,103)(xm(m,i),m=1,3)
          call CrlDefMolRefPoint(i,Veta,ich)
          if(ich.gt.0) go to 9100
        endif
        iak=iap+iam(i)+nn-1
      enddo
      if(klic.eq.0) PosledniKiAtMol=PrvniKi-1
      go to 9999
8500  ErrFlag=2
      go to 9099
9000  Veta='atom "'//at(:idel(at))//'" isn''t of the file M40'
      go to 9090
9040  Veta='the singular transformation matrix for '//
     1    SmbPGMol(i)(1:idel(SmbPGMol(i)))
9090  s80='It happended for "'//molname(i)(:idel(molname(i)))
      if(j.gt.0) then
        write(Cislo,'(i2)') j
        call zhusti(Cislo)
        s80=s80(:idel(s80))//'#'//Cislo(:idel(Cislo))
      endif
      s80=s80(:idel(s80))//'"'
      call FeChybne(-1.,-1.,Veta,s80,SeriousError)
9099  ErrFlag=2
      go to 9999
9100  ErrFlag=1
9999  return
100   format(a8,i3,1x,a8,7x,a27)
101   format(a8,2i3,4x,f9.6,33x,3i1,3i3)
102   format(a8,2i3,4x,f9.6,30x,'>>',3i1,3i3,'<<'/
     1       1x,3f9.3,a27,3x,'=>',7i1,'<=')
103   format(6f9.6)
104   format(2a27)
105   format(a8,i3,a3,4x,f9.6,33x,3i1,3i3)
106   format(3f9.3,a27,6x,7i1)
107   format(2a27,2x,a2)
108   format(f9.6,10x,a8,33x,i1)
109   format(f9.6,10x,a8,f9.6,24x,i1)
      end
