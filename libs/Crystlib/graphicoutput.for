      subroutine GraphicOutput(Klic,FileNameIn,ich)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      real, allocatable :: vt6p(:,:),xyz(:,:),xyzo(:,:),sngc(:,:,:),
     1                     csgc(:,:,:)
      real :: x0(6),x1(6),x2(6),x3(6),x4(6),xd(6),xdc(6),xf(6),RPom(9),
     1        uxi(3,mxw),uyi(3,mxw),xt(6),smt(36),bxi(6,mxw),byi(6,mxw),
     2        bd(6),bp(6),smtc(36,3),bf(6),GammaIntInv(9),x40(3),x4p(3),
     3        oi(1),sh(6),shc(3),xg(3),rm6p(36),s6p(6),rmp(36),zsigp(9),
     4        ZVIp(36),TrX(3,3),TrXP(3,3),TrXI(3,3),TrB(36),sm0i(3),
     5        smxi(3,mxw),smyi(3,mxw),sm(3),CellParNew(6),olim=.5,
     6        DminP=1.,dx4(3)=(/0.,0.,0./),Bohr2Ang=1.,xmezc(2,3),
     7        xmez(2,3,3) = reshape ((/-333.,-333.,-333.,
     8                                 -333.,-333.,-333.,
     9                                 -333.,-333.,-333.,
     a                                 -333.,-333.,-333.,
     1                                 -333.,-333.,-333.,
     2                                -333.,-333.,-333./),shape(xmez)),
     3        trezg(3,3)=reshape((/0.,0.,0.,0.,0.,0.,0.,0.,0./),
     4                           shape(trezg))
      character*(*) FileNameIn
      character*256 FileName
      character*80 :: Veta,t80,
     1                Label='The following positions discarded to '//
     2                      'avoid short disances:'
      character*40 t40
      character*9 ch9
      character*8 ch8
      character*8, allocatable :: AtName(:)
      integer :: EdwStateQuest,nmez(2,3,3),natp(20),ish(3),ishc(3),
     1           DrawMode=0,KPhaseUsed=-1,nx4(3)=(/1,1,1/),IDrawMag=-1
      logical, allocatable :: BratAtom(:),BratAtomO(:)
      logical :: EqIgCase,IdenticalAtoms,eqrv,KeepNames,PolMag,
     1           CrwLogicQuest,NonStandard,EqRV0,MameVestu1,MameVestu2,
     2           CheckBorders=.false.
      save BratAtom,NPeak,NFilter,NonStandard
      FileName=FileNameIn
      if(KPhase.ne.KPhaseUsed) then
        if(.not.allocated(BratAtom)) allocate(BratAtom(NAtCalc))
        call SetLogicalArrayTo(BratAtom,NAtCalc,.true.)
!        do i=1,NAtCalc-1
!          if(kswa(i).ne.KPhase.or..not.BratAtom(i)) cycle
!          do j=i+1,NAtCalc
!            if(kswa(j).ne.KPhase) cycle
!            if(IdenticalAtoms(i,j,.001)) BratAtom(j)=.false.
!          enddo
!        enddo
      endif
      KPhaseUsed=KPhase
      go to 1100
      entry GraphicOutputProlog
      if(allocated(BratAtom)) deallocate(BratAtom)
      allocate(BratAtom(NAtCalc))
      call SetLogicalArrayTo(BratAtom,NAtCalc,.true.)
      KPhaseOld=-1
      call SetRealArrayTo(xmez(1,1,1),6,-333.)
      trezg=0.
      if(NDimI(KPhase).gt.0) then
        olim=.5
        if(kcommen(KPhase).gt.0)
     1     trezg(1:NDimI(KPhase),1)=trez(1:NDimI(KPhase),1,KPhase)
      else
        olim=.0
      endif
      dx4=0.
      nx=1
      DrawMode=0
      CheckBorders=.false.
      NFilter=0
      NPeak=0
      NonStandard=EqIgCase(Grupa(KPhase)(1:1),'x').or.
     1            EqIgCase(Grupa(KPhase)(1:1),'?').or.
     2            Grupa(KPhase).eq.' '.or.
     3            index(Grupa(KPhase),'[').gt.0.or.
     4            (EqIgCase(Grupa(KPhase)(1:1),'r').and.
     5             CrSystem(KPhase).ne.CrSystemTrigonal).or.
     6            (.not.EqIgCase(Grupa(KPhase)(1:1),'p').and.
     7             CrSystem(KPhase).eq.CrSystemTriclinic).or.
     8            (EqIgCase(Grupa(KPhase)(1:1),'f').and.
     9             mod(CrSystem(KPhase),10).eq.CrSystemMonoclinic).or.
     a             MagneticType(KPhase).gt.0
      if(.not.NonStandard.and.
     1    mod(CrSystem(KPhase),10).eq.CrSystemMonoclinic) then
        NonStandard=Monoclinic(KPhase).ne.2.and.
     1              EqIgCase(Grupa(KPhase)(1:1),'c')
      endif
      go to 9999
1100  allocate(vt6p(NDim(KPhase),NLattVec(KPhase)))
      call UnitMat(TrX ,3)
      call UnitMat(TrXP,3)
      call UnitMat(TrXI,3)
      KPhaseIn=KPhase
      NDatBlockIn=NDatBlock
      ich=0
      if(OrthoOrd.gt.0) call trortho(0)
      if(.not.BatchMode) then
        id=NextQuestId()
        xqd=450.
        if(xmez(1,1,1).lt.-300.) then
          if(NDimI(KPhase).gt.0.and.KCommen(KPhase).gt.0) then
            do i=1,3
              xmez(1,i,1)=0.
              xmez(2,i,1)=NCommen(i,1,KPhase)
            enddo
          else
            do i=1,3
              xmez(1,i,1)=0.
              xmez(2,i,1)=1.
            enddo
          endif
        endif
        if(NDimI(KPhase).le.0) then
          if(NonStandard.or.MagneticType(KPhase).gt.0) then
            il=6
          else
            il=3
          endif
        else
          il=17
        endif
        if(MagneticType(KPhase).gt.0) then
          il=il+4
          MameVestu1=LocateSubstring(Call3dMaps,'Vesta.exe',.false.,
     1                              .true.).gt.0
          if(MameVestu1) il=il+1
          MameVestu2=LocateSubstring(CallGraphic,'Vesta.exe',.false.,
     1                              .true.).gt.0
        endif
        call FeQuestCreate(id,-1.,-1.,xqd,il,' ',1,LightGray,-1,-1)
        il=-12
        dpom=105.
        xpom=xqd*.5-1.5*dpom-10.
        Veta='Ato%m filter'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtFilter=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+10.
        Veta='Add F%ourier peaks'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtIncludePeaks=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+10.
        Veta='R%eset selection'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtReset=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il-10
        write(Cislo,'(i5,''/'',i5)') NFilter,NPeak
        call Zhusti(Cislo)
        Veta=Cislo(:idel(Cislo))//' ... number of excluded atoms/'//
     1       'number of included peaks'
        xpom=5.
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','N')
        nLblInfo=LblLastMade
        il=2
        call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
        if(ErrFlag.ne.0) go to 1250
1200    read(m40,FormA,end=1250) Veta
        if(Veta(1:10).ne.'----------'.or.
     1     LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2    go to 1200
        read(m40,'(2i5)',end=1250) isw,KPh
        if(KPh.ne.KPhase) go to 1250
        go to 1300
1250    call FeQuestButtonDisable(nButtIncludePeaks)
1300    close(m40)
        xpom=5.
        tpom=xpom+CrwgXd+5.
        ichk=0
        if(NDimI(KPhase).le.0) then
          nCrwAverage=0
          nCrwSupercell=0
          nCrwModulated=0
          DrawMode=1
          nEdwLimitFirst=0
          nEdwLimitLast=0
          nCrwSmallStep=0
          nCrwLargeStep=0
          nEdwCutOcc=0
          nEdwTzero=0
          nEdwDmin=0
          nCrwCheckBorders=0
          nButtAddStep=0
        else
          il=il+1
          call FeQuestLinkaMake(id,il)
          ichk=ichk+1
          if(DrawMode.eq.0) then
            if(KCommen(KPhase).gt.0) then
              DrawMode=2
            else
              DrawMode=3
            endif
          endif
          do i=1,3
            il=il+1
            if(i.eq.1) then
              Veta='Draw %average structure'
            else if(i.eq.2) then
              Veta='Draw %supercell structure'
            else if(i.eq.3) then
              Veta='Draw a%pproximant structure'
            endif
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,ichk)
            call FeQuestCrwOpen(CrwLastMade,i.eq.DrawMode)
            if(i.eq.1) then
              nCrwAverage=CrwLastMade
            else if(i.eq.2) then
              nCrwSupercell=CrwLastMade
            else if(i.eq.3) then
              nCrwModulated=CrwLastMade
            endif
          enddo
          if(KCommen(KPhase).le.0)
     1      call FeQuestCrwDisable(nCrwSupercell)
          tpom=55.
          xpom=tpom+5.
          dpom=50.
          spom=xpom+dpom+10.
          do i=1,3
            il=il+1
            call FeQuestLblMake(id,15.,il,smbx(i),'C','N')
            call FeQuestEudMake(id,tpom,il,xpom,il,'from','R',dpom,
     1                          EdwYd,1)
            if(i.eq.1) then
              nEdwLimitFirst=EdwLastMade
              nLblLimitFirst=LblLastMade
            endif
            call FeQuestRealEdwOpen(EdwLastMade,xmez(1,i,1),.false.,
     1                              .false.)
            call FeQuestEudOpen(EdwLastMade,0,0,0,-100.,xmez(2,i,1),
     1                          1.)
            call FeQuestEudMake(id,tpom+spom,il,xpom+spom,il,'to','R',
     1                          dpom,EdwYd,1)
            if(i.eq.3) nEdwLimitLast=EdwLastMade
            call FeQuestRealEdwOpen(EdwLastMade,xmez(2,i,1),.false.,
     1                              .false.)
            call FeQuestEudOpen(EdwLastMade,0,0,0,xmez(1,i,1),100.,1.)
          enddo
          xpom=xpom+dpom+spom+35.
          tpom=xpom+5.+CrwXd
          ilp=il
          il=il-3
          ichk=ichk+1
          do i=1,2
            il=il+1
            if(i.eq.1) then
              Veta='step 0%.1'
            else
              Veta='step %1'
            endif
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,ichk)
            if(i.eq.1) then
              nCrwSmallStep=CrwLastMade
            else
              nCrwLargeStep =CrwLastMade
            endif
            call FeQuestCrwOpen(CrwLastMade,i.eq.2)
          enddo
          il=il+1
          Veta='Expand volume by %0.1'
          dpom=FeTxLengthUnder(Veta)+10.
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          nButtAddStep=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          pom=1.
          il=ilp+1
          t80='%Cutoff occupancy'
          tpom=5.
          xpom=FeTxLengthUnder(t80)+10.
          dpom=50.
          call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
          nEdwCutOcc=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,olim,.false.,.false.)
          il=il+1
          t80='%tzero'
          call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
          nEdwTzero=EdwLastMade
          call FeQuestRealAEdwOpen(EdwLastMade,trezg(1,1),NDimI(KPhase),
     1                             .false.,.false.)
          il=il+1
          xpomp=5.
          tpomp=xpomp+5.+CrwXd
          t80='C%heck short distances in the approximant'
          call FeQuestCrwMake(id,tpomp,il,xpomp,il,t80,'L',CrwXd,
     1                        CrwXd,1,0)
          nCrwCheckBorders=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,CheckBorders)
          il=il+1
          t80='Min. %distance'
          call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
          nEdwDmin=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,DminP,.false.,.false.)
        endif
        if(MagneticType(KPhase).gt.0) then
          il=il+1
          call FeQuestLinkaMake(id,il)
          il=il+1
          tpom=5.
          t80='One %Bohr magneton corresponds to'
          xpom=tpom+FeTxLengthUnder(t80)+10.
          dpom=50.
          call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
          nEdwBohr2Ang=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,Bohr2Ang,.false.,.false.)
          xpomp=xpom+dpom+10.
          call FeQuestLblMake(id,xpomp,il,'Ang.','L','N')
          ichk=ichk+1
          if(MameVestu1) then
            NDrawMag=3
          else
            NDrawMag=2
          endif
          if(IDrawMag.lt.0) IDrawMag=NDrawMag
          UseVestaForMagDraw=IDrawMag.eq.3.or.MameVestu2
          xpom=5.
          tpom=xpom+CrwgXd+5.
          Veta='C%reate "inp" file for ATOMS'
          do i=1,NDrawMag
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,ichk)
            call FeQuestCrwOpen(CrwLastMade,i.eq.IDrawMag)
            if(i.eq.1) then
              Veta='Use %your default drawing program'
              nCrwDrawMagFirst=CrwLastMade
            else if(i.eq.2) then
              Veta='Use %Vesta to draw the magnetic structure'
            endif
          enddo
          nCrwDrawMagLast=CrwLastMade
        else
          nEdwBohr2Ang=0
          nCrwMagDrawFirst=0
          nCrwMagDrawLast=0
        endif
        if(NDimI(KPhase).gt.0..or.NonStandard.or.
     1     MagneticType(KPhase).ne.0) then
          il=il+1
          call FeQuestLinkaMake(id,il)
          xpom=5.
          tpom=xpom+5.+CrwXd
          ichk=ichk+1
          do i=1,2
            il=il+1
            if(i.eq.1) then
              t80='%Keep atom names (duplicity could appear)'
            else
              t80='Chan%ge atom names to "atom_type"+number'
            endif
            call FeQuestCrwMake(id,tpom,il,xpom,il,t80,'L',CrwgXd,
     1                          CrwgYd,0,ichk)
            if(i.eq.1) then
              nCrwKeepNames=CrwLastMade
            else
              nCrwChangeNames=CrwLastMade
            endif
            call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          enddo
        else
          nCrwKeepNames=0
          nCrwChangeNames=0
        endif
        il=-il*10-12
        if(Klic.eq.0) then
          Veta='OK'
        else
          Veta='%Quit'
        endif
        dpom=FeTxLengthUnder(Veta)+20.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtQuit=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(Klic.ne.0) then
          Veta='Draw+retur%n'
          dpom=FeTxLengthUnder(Veta)+20.
          tpom=xpom-dpom-15.
          call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
          nButtReturn=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          Veta='Draw%+continue'
          tpom=xqd-xpom+15.
          call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
          nButtContinue=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        else
          nButtReturn=0
          nButtContinue=0
        endif
1380    if(nEdwLimitFirst.gt.0) then
          nEdw=nEdwLimitFirst
          nLbl=nLblLimitFirst
          if(DrawMode.eq.1.or.DrawMode.eq.2) then
            do i=1,6
              call FeQuestEdwDisable(nEdw)
              if(i.le.3) call FeQuestLblDisable(nLbl)
              nEdw=nEdw+1
              nLbl=nLbl+1
            enddo
            call FeQuestCrwDisable(nCrwSmallStep)
            call FeQuestCrwDisable(nCrwLargeStep)
            call FeQuestButtonDisable(nButtAddStep)
          endif
          if(DrawMode.eq.1) then
            call FeQuestEdwDisable(nEdwCutOcc)
            if(KCommen(KPhase).le.0) then
              call FeQuestEdwDisable(nEdwTzero)
              call FeQuestCrwDisable(nCrwCheckBorders)
              call FeQuestEdwDisable(nEdwDmin)
            endif
            if(.not.NonStandard) then
              call FeQuestCrwDisable(nCrwKeepNames)
              call FeQuestCrwDisable(nCrwChangeNames)
            endif
          else if(DrawMode.eq.2) then
            call FeQuestRealEdwOpen(nEdwCutOcc,olim,.false.,.false.)
            call FeQuestEdwDisable(nEdwTzero)
            call FeQuestCrwDisable(nCrwCheckBorders)
            call FeQuestEdwDisable(nEdwDmin)
            call FeQuestCrwOpen(nCrwKeepNames,.true.)
            call FeQuestCrwOpen(nCrwChangeNames,.false.)
          else if(DrawMode.eq.3) then
            do i=1,3
              call FeQuestRealEdwOpen(nEdw,xmez(1,i,1),.false.,
     1                                .false.)
              call FeQuestEudOpen(nEdw,0,0,0,-100.,xmez(2,i,1),1.)
              nEdw=nEdw+1
              call FeQuestLblOn(nLbl)
              nLbl=nLbl+1
              call FeQuestRealEdwOpen(nEdw,xmez(2,i,1),.false.,
     1                                .false.)
              call FeQuestEudOpen(nEdw,0,0,0,xmez(1,i,1),100.,1.)
              nEdw=nEdw+1
            enddo
            call FeQuestCrwOpen(nCrwSmallStep,.false.)
            call FeQuestCrwOpen(nCrwLargeStep,.true.)
            call FeQuestButtonOff(nButtAddStep)
            call FeQuestRealEdwOpen(nEdwCutOcc,olim,.false.,.false.)
            if(KCommen(KPhase).le.0) then
              call FeQuestRealAEdwOpen(nEdwTzero,trezg(1,1),
     1                                 NDimI(KPhase),.false.,.false.)
              call FeQuestCrwOpen(nCrwCheckBorders,CheckBorders)
              if(CheckBorders) then
                call FeQuestRealEdwOpen(nEdwDmin,DminP,.false.,
     1                                  .false.)
              else
                call FeQuestEdwDisable(nEdwDmin)
              endif
            else
              call FeQuestEdwDisable(nEdwTzero)
              call FeQuestCrwDisable(nCrwCheckBorders)
              call FeQuestEdwDisable(nEdwDmin)
            endif
            call FeQuestCrwOpen(nCrwKeepNames,.true.)
            call FeQuestCrwOpen(nCrwChangeNames,.false.)
          endif
        endif
1385    if(MagneticType(KPhase).gt.0) then
          if(UseVestaForMagDraw) then
            call FeQuestEdwDisable(nEdwBohr2Ang)
          else
            call FeQuestRealEdwOpen(nEdwBohr2Ang,Bohr2Ang,.false.,
     1                              .false.)
          endif
        endif
1390    write(Cislo,'(i5,''/'',i5)') NFilter,NPeak
        call Zhusti(Cislo)
        Veta=Cislo(:idel(Cislo))//' ... number of excluded atoms/'//
     1       'number of included peaks'
        call FeQuestLblChange(nLblInfo,Veta)
1400    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton.and.
     1     (CheckNumber.eq.nButtQuit.and.Klic.ne.0)) then
          ich=1
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtContinue.or.
     2           CheckNumber.eq.nButtReturn.or.
     3           (CheckNumber.eq.nButtQuit.and.Klic.eq.0))) then
          if(CheckNumber.eq.nButtContinue.or.CheckNumber.eq.nButtQuit)
     1       then
            ich=0
          else
            ich=-10
          endif
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtFilter) then
          call SelAtoms('Select atoms exported for drawing',Atom,
     1                  BratAtom,isf,NAtCalc,ichp)
          NFilter=0
          do i=1,NAtCalc
            if(.not.BratAtom(i)) NFilter=NFilter+1
          enddo
          go to 1390
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtIncludePeaks) then
          NPeak=0
          call iom40Only(0,0,fln(:ifln)//'.m40')
          NAtCalcO=NAtCalc
          allocate(AtName(NAtCalc))
          do i=1,NAtCalc
            AtName(i)=Atom(i)
          enddo
          call IncludePeaks
          if(NAtCalc.gt.NAtCalcO) then
            allocate(BratAtomO(NAtCalcO))
            do i=1,NAtCalcO
              BratAtomO(i)=BratAtom(i)
            enddo
            deallocate(BratAtom)
            allocate(BratAtom(NAtCalc))
            do i=1,NAtCalcO
              BratAtom(i)=BratAtomO(i)
            enddo
            do i=NAtCalcO+1,NAtCalc
              BratAtom(i)=.true.
            enddo
            deallocate(BratAtomO)
            NPeak=NPeak+NAtCalc-NAtCalcO
          endif
          deallocate(AtName)
          go to 1390
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtReset) then
          call CopyFile(fln(:ifln)//'.l40',fln(:ifln)//'.m40')
          call iom40(0,0,fln(:ifln)//'.m40')
          NPeak=0
          NFilter=0
          go to 1390
        else if(CheckType.eq.EventCrw.and.
     1          (CheckNumber.eq.nCrwAverage.or.
     2           CheckNumber.eq.nCrwSuperCell.or.
     3           CheckNumber.eq.nCrwModulated)) then
          DrawMode=CheckNumber-nCrwAverage+1
          go to 1380
        else if(CheckType.eq.EventCrw.and.
     1          (CheckNumber.ge.nCrwDrawMagFirst.and.
     2           CheckNumber.le.nCrwDrawMagLast)) then
          nCrw=nCrwDrawMagFirst
          do i=1,NDrawMag
            if(CrwLogicQuest(nCrw)) then
              IDrawMag=i
              exit
            endif
            nCrw=nCrw+1
          enddo
          UseVestaForMagDraw=IDrawMag.eq.3.or.MameVestu2
          go to 1385
        else if(CheckType.eq.EventEdw.and.
     1          CheckNumber.le.nEdwLimitLast) then
          i=mod(CheckNumber-nEdwLimitFirst+1,2)
          if(i.eq.1) then
            j=CheckNumber+1
            if(EdwRealQuest(2,j).ne.EdwRealQuest(1,CheckNumber)) then
              call FeQuestEudOpen(j,0,0,0,EdwRealQuest(1,CheckNumber),
     1                            EdwRealQuest(3,j),EdwRealQuest(4,j))
              if(EdwRealQuest(1,j).lt.
     2           EdwRealQuest(2,j)+EdwRealQuest(4,j)*.5.or.
     3           EdwRealQuest(1,j).lt.pom+EdwRealQuest(4,j)*.5) then
                call FeCheckEud(CheckNumber,0)
              endif
            endif
          else if(i.eq.0) then
            j=CheckNumber-1
            if(EdwRealQuest(3,j).ne.EdwRealQuest(1,CheckNumber)) then
              call FeQuestEudOpen(j,0,0,0,EdwRealQuest(2,j),
     1                                    EdwRealQuest(1,CheckNumber),
     2                                    EdwRealQuest(4,j))
              if(EdwRealQuest(1,j).gt.
     1           EdwRealQuest(3,j)-EdwRealQuest(4,j)*.5.or.
     2           EdwRealQuest(1,j).gt.pom-EdwRealQuest(4,j)*.5) then
                call FeCheckEud(CheckNumber,0)
              endif
            endif
          endif
          go to 1400
        else if(CheckType.eq.EventCrw.and.
     1          (CheckNumber.eq.nCrwLargeStep.or.
     2           CheckNumber.eq.nCrwSmallStep)) then
          if(CheckNumber.eq.nCrwLargeStep) then
            pom=1.
          else
            pom=.1
          endif
          nEdw=nEdwLimitFirst
          do i=1,3
            do j=1,2
              call FeQuestEudOpen(nEdw,0,0,0,EdwRealQuest(2,nEdw),
     1                                       EdwRealQuest(3,nEdw),
     2                                       pom)
              nEdw=nEdw+1
            enddo
          enddo
          go to 1400
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtAddStep) then
          nEdw=nEdwLimitFirst
          do i=1,3
            do j=1,2
              call FeQuestRealFromEdw(nEdw,pom)
              if(j.eq.1) then
                pom=pom-.1
              else
                pom=pom+.1
              endif
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)

              nEdw=nEdw+1
            enddo
          enddo
          go to 1400
        else if(CheckType.eq.EventCrw.and.
     1          CheckNumber.eq.nCrwCheckBorders) then
          CheckBorders=CrwLogicQuest(CheckNumber)
          if(CheckBorders) then
            call FeQuestRealEdwOpen(nEdwDmin,DminP,.false.,.false.)
          else
            call FeQuestEdwDisable(nEdwDmin)
          endif
          go to 1400
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1400
        endif
        if(ich.le.0) then
          if(nEdwLimitFirst.gt.0.and.
     1       EdwStateQuest(nEdwLimitFirst).eq.EdwOpened) then
            nEdw=nEdwLimitFirst
            do i=1,3
              do j=1,2
                call FeQuestRealFromEdw(nEdw,xmez(j,i,1))
                nEdw=nEdw+1
              enddo
            enddo
          else
            if(DrawMode.eq.1) then
              do i=1,3
                xmez(1,i,1)=0.
                xmez(2,i,1)=1.
              enddo
            else if(DrawMode.eq.2) then
              do i=1,3
                xmez(1,i,1)=0.
                xmez(2,i,1)=NCommen(i,1,KPhase)
              enddo
            endif
          endif
          if(nEdwCutOcc.gt.0) call FeQuestRealFromEdw(nEdwCutOcc,olim)
          if(nCrwKeepNames.gt.0) KeepNames=CrwLogicQuest(nCrwKeepNames)
          if(nEdwTzero.ne.0) then
            call FeQuestRealAFromEdw(nEdwTzero,trezg(1,1))
            call FeQuestRealFromEdw(nEdwDmin,DminP)
          endif
          if(MagneticType(KPhase).gt.0.and..not.UseVestaForMagDraw)
     1      call FeQuestRealFromEdw(nEdwBohr2Ang,Bohr2Ang)
        endif
        call FeQuestRemove(id)
        if(ich.gt.0) go to 9000
      else
        DrawMode=3
        KeepNames=.true.
        if(xmez(1,1,1).lt.-300.) then
          do i=1,3
            xmez(1,i,1)=0.
            xmez(2,i,1)=1.
          enddo
        endif
        olim=.5
1520    read(BatchLN,FormA,err=1550,end=1550) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo(2:),'lim')) then
          do i=1,3
            if(EqIgCase(Cislo(1:1),smbx(i))) then
              call StToReal(Veta,k,xmez(1,i,1),2,.false.,ichp)
              cycle
            endif
          enddo
        else if(EqIgCase(Cislo,'ocutoff')) then
          call StToReal(Veta,k,x1,1,.false.,ichp)
          olim=x1(1)
        else if(EqIgCase(Cislo,'output')) then
          FileName=Veta(k:)
        else
          if(Cislo.ne.'end') backspace BatchLN
          go to 1550
        endif
        go to 1520
      endif
1550  do i=1,3
        nmez(1,i,1)=xmez(1,i,1)-1.
        nmez(2,i,1)=xmez(2,i,1)+1.
        TrXP(i,i)=1./(xmez(2,i,1)-xmez(1,i,1))
        xmezc(1,i)=int(xmez(1,i,1))
        xmezc(2,i)=int(xmez(2,i,1))
        TrX(i,i)=1./(xmezc(2,i)-xmezc(1,i))
      enddo
      call UnitMat(smtc,6)
      do i=2,NComp(KPhase)
        do j=1,3
          xmez(1,j,i)= 9999.
          xmez(2,j,i)=-9999.
        enddo
        do k1=1,2
          x0(1)=xmez(k1,1,1)
          do k2=1,2
            x0(2)=xmez(k2,2,1)
            do k3=1,2
              x0(3)=xmez(k3,3,1)
              call qbyx(x0,x0(4),1)
              do isym=1,NSymm(KPhase)
                call MultM(ZV(1,i,KPhase),s6(1,isym,1,KPhase),x1,
     1                     NDim(KPhase),NDim(KPhase),1)
                call MultM(ZV(1,i,KPhase),rm6(1,isym,1,KPhase),rmp,
     1                     NDim(KPhase),NDim(KPhase),NDim(KPhase))
                call MultM(rmp,x0,x2,NDim(KPhase),NDim(KPhase),1)
                do j=1,3
                  xmez(1,j,i)=min(x1(j)+x2(j),xmez(1,j,i))
                  xmez(2,j,i)=max(x1(j)+x2(j),xmez(2,j,i))
                enddo
              enddo
            enddo
          enddo
        enddo
        do k=1,3
          nmez(1,k,i)=xmez(1,k,i)-1.
          nmez(2,k,i)=xmez(2,k,i)+1.
        enddo
        call srotb(zsigi(1,i,KPhase),zsigi(1,i,KPhase),smtc(1,i))
        do j=4,NDim(KPhase)
          do k=4,NDim(KPhase)
            pom=zv(k+(j-1)*NDim(KPhase),i,KPhase)
            do l=1,3
              pom=pom-qu(l,j-3,i,KPhase)*
     1                zv(l+(k-1)*NDim(KPhase),i,KPhase)
            enddo
            zsigp(k-3+(j-4)*NDimI(KPhase))=pom
          enddo
        enddo
        call multm(zsigp,trezg(1,1),trezg(1,i),NDimI(KPhase),
     1             NDimI(KPhase),1)
      enddo
      if(DrawMode.eq.1.and..not.NonStandard.and.
     1   MagneticType(KPhase).eq.0) go to 4100
      call CopyVek(CellPar(1,1,KPhase),CellParNew,6)
      call MatInv(TrX,TrXI,pom,3)
      call SrotB(TrX,TrX,TrB)
      call UnitMat(rmp,NDim(KPhase))
      call DRMatTrCell(TrXI,CellParNew,rmp)
2000  if(UseVestaForMagDraw.and.MagneticType(KPhase).ne.0.and.
     1  NDimI(KPhase).eq.0) then
        if(NDimI(KPhase).eq.0) go to 4100
      else if(IDrawMag.eq.1) then
        LnInp=NextLogicNumber()
        call OpenFile(LnInp,fln(:ifln)//'.inp','formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        write(LnInp,FormA) 'TITL '//fln(:ifln)
        write(LnInp,'(''CELL '',6f10.3)')(CellParNew(i),i=1,6)
        write(LnInp,FormA) 'SPGP P 1'
        write(LnInp,FormA) 'FIELDS LAB COO TYP VEC'
      else
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_m40.tmp','formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
      endif
      nalloc=10000
      allocate(xyz(3,nalloc))
      nac=0
      naci=0
      nav=0
      nahi=0
      if(isfh(KPhase).le.0.and.MagneticType(KPhase).gt.0.and.
     1   .not.UseVestaForMagDraw) then
        call ReallocFormF(NAtFormula(KPhase)+1,NPhase,NDatBlock)
        NAtFormula(KPhase)=NAtFormula(KPhase)+1
        Formula(KPhase)=Formula(KPhase)(:idel(Formula(KPhase)))//' H'
        isfh(KPhase)=NAtFormula(KPhase)
        AtTypeFull(isfh(KPhase),KPhase)='H'
        AtType(isfh(KPhase),KPhase)='H'
        AtTypeMag(isfh(KPhase),KPhase)='H'
        call EM50ReadAllFormFactors
        call EM50FormFactorsSet
      endif
      call SetIntArrayTo(natp,NAtFormula(KPhase),0)
      NInfo=0
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase.or..not.BratAtom(i).or.ai(i).le.0.) cycle
        isw=iswa(i)
        kk=0
        if(KFA(1,i).ne.0.and.KModA(1,i).ne.0) then
          xg(1)=ax(KModA(1,i),i)
          kk=1
        else if(KFA(2,i).ne.0.and.KModA(2,i).ne.0) then
          xg(1)=uy(1,KModA(2,i),i)
          kk=1
        endif
        call SpecPos(x(1,i),xg,kk,isw,.001,nocc)
        pomo=ai(i)*float(nocc)
        if(DrawMode.eq.1) then
          MezD1=-1
          MezH1= 1
          MezD2=-1
          MezH2= 1
          MezD3=-1
          MezH3= 1
        else
          MezD1=nmez(1,1,isw)
          MezH1=nmez(2,1,isw)
          MezD2=nmez(1,2,isw)
          MezH2=nmez(2,2,isw)
          MezD3=nmez(1,3,isw)
          MezH3=nmez(2,3,isw)
        endif
        itfi=min(itf(i),2)
        MagParI=MagPar(i)
        if(MagParI.gt.0) then
          if(KUsePolar(i).gt.0) then
            call ShpCoor2Fract(sm0(1,i),smt)
            do k=1,MagParI-1
              call ShpCoor2Fract(smx(1,k,i),smt)
              call ShpCoor2Fract(smy(1,k,i),smt)
            enddo
          endif
        endif
        isfi=isf(i)
        call CopyVek(x(1,i),x0,3)
        call SetRealArrayTo(x0(4),NDimI(KPhase),0.)
        js=0
        natn=0
        nacp=nac+1
        do 3900isym=1,NSymm(KPhase)
          js=js+1
          if(isa(js,i).le.0) go to 3900
          iswp=iswSymm(isym,isw,KPhase)
          if(iswp.eq.isw) then
            call CopyMat(rm6(1,isym,isw,KPhase),rm6p,NDim(KPhase))
            call CopyVek( s6(1,isym,isw,KPhase), s6p,NDim(KPhase))
            call CopyMat(ZVi(1,isw,KPhase),ZVip,NDim(KPhase))
            do ivt=1,NLattVec(KPhase)
              call CopyVek(vt6(1,ivt,isw,KPhase),vt6p(1,ivt),
     1                     NDim(KPhase))
            enddo
          else
            call MultM(ZV(1,iswp,KPhase),ZVi(1,isw,KPhase),ZVip,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MultM(ZVip,rm6(1,isym,isw,KPhase),rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MultM(ZVip,s6(1,isym,isw,KPhase),s6p,NDim(KPhase),
     1                 NDim(KPhase),1)
            do ivt=1,NLattVec(KPhase)
              call MultM(ZVip,vt6(1,ivt,isw,KPhase),vt6p(1,ivt),
     1                   NDim(KPhase),NDim(KPhase),1)
            enddo
          endif
          call MatBlock3(rm6p,rmp,NDim(KPhase))
          call GetGammaIntInv(rm6p,GammaIntInv)
          call multm(rm6p,x0,x1,NDim(KPhase),NDim(KPhase),1)
          do j=1,KModA(2,i)
            call multm(rmp,ux(1,j,i),uxi(1,j),3,3,1)
            if(KFA(2,i).eq.0.or.j.ne.KModA(2,i)) then
              call multm(rmp,uy(1,j,i),uyi(1,j),3,3,1)
            else
              call CopyVek(uy(1,j,i),uyi(1,j),3)
            endif
          enddo
          call srotb(rmp,rmp,smt)
          if(itfi.ge.2) then
            call multm(smt,beta(1,i),bp,6,6,1)
            do j=1,KModA(3,i)
              call multm(smt,bx(1,j,i),bxi(1,j),6,6,1)
              call multm(smt,by(1,j,i),byi(1,j),6,6,1)
            enddo
          else
            bp=beta(1:6,i)
          endif
          if(MagParI.gt.0) then
            call Multm(RMag(1,isym,iswp,KPhase),sm0(1,i),sm0i,3,3,1)
            do j=1,MagParI-1
              call Multm(RMag(1,isym,iswp,KPhase),smx(1,j,i),smxi(1,j),
     1                   3,3,1)
              call Multm(RMag(1,isym,iswp,KPhase),smy(1,j,i),smyi(1,j),
     1                   3,3,1)
            enddo
          else
            sm0i=0.
          endif
          do j=1,NDim(KPhase)
            x2(j)=x1(j)+s6p(j)
          enddo
          do 3700ivt=1,NLattVec(KPhase)
            do jvt=1,ivt-1
              if(eqrv(vt6p(1,ivt),vt6p(1,jvt),3,.0001)) go to 3700
            enddo
            call AddVek(x2,vt6p(1,ivt),x3,NDim(KPhase))
            call AddVek(s6p,vt6p(1,ivt),sh,NDim(KPhase))
            call od0do1(x3,x4,NDim(KPhase))
            do j=1,3
              sh(j)=sh(j)+x4(j)-x3(j)
              ish(j)=nint(x4(j)-x3(j))
            enddo
            if(NDimI(KPhase).gt.0) then
              call qbyx(sh,xt(4),iswp)
              do j=4,NDim(KPhase)
                xt(j)=xt(j)-sh(j)
              enddo
              call CopyVek(qcnt(1,i),x40,NDimI(KPhase))
              call Cultm(GammaIntInv,xt(4),x40,NDimI(KPhase),
     1                   NDimI(KPhase),1)
            endif
            ip=0
            do ic3=MezD3,MezH3
              xt(3)=ic3
              ishc(3)=ish(3)+ic3
              do ic2=MezD2,MezH2
                xt(2)=ic2
                ishc(2)=ish(2)+ic2
                do 3400ic1=MezD1,MezH1
                  xt(1)=ic1
                  ishc(1)=ish(1)+ic1
                  call AddVek(sh,xt,shc,3)
                  if(NDimI(KPhase).eq.0) then
                    oi(1)=ai(i)
                    sm(1:3)=sm0i(1:3)
                    do j=1,3
                      xf(j)=x4(j)+xt(j)
                    enddo
                    bd=bp
                  else
                    call qbyx(xt,xt(4),iswp)
                    call AddVek(xt(4),trezg(1,iswp),xt(4),NDimI(KPhase))
                    call Multm(GammaIntInv,xt(4),x4p,NDimI(KPhase),
     1                         NDimI(KPhase),1)
                    if(DrawMode.eq.1.or.(KModA(1,i).le.0.and.
     1                                   KModA(2,i).le.0)) then
                      oi(1)=a0(i)
                    else
                      if(TypeModFun(i).le.1.or.KModA(1,i).le.1) then
                        call MakeOccMod(oi,x40,x4p,nx4,dx4,a0(i),
     1                    ax(1,i),ay(1,i),KModA(1,i),KFA(1,i),
     2                    GammaIntInv)
                        if(KFA(2,i).gt.0.and.KModA(2,i).gt.0)
     1                    call MakeOccModSawTooth(oi,x40,x4p,nx4,dx4,
     2                      uyi(1,KModA(2,i)),uyi(2,KModA(2,i)),
     3                      KFA(2,i),GammaIntInv)
                      else
                        call MakeOccModPol(oi,x40,x4p,nx4,dx4,ax(1,i),
     1                    ay(1,i),KModA(1,i)-1,ax(KModA(1,i),i),
     2                    a0(i)*.5,GammaIntInv,TypeModFun(i))
                      endif
                    endif
                    oi(1)=oi(1)*pomo
!                    if(oi(1).lt.olim.and.MagPar(i).le.0) go to 3400
                    if(oi(1).lt.olim.and.NDimI(KPhase).gt.0.and.
     1                 DrawMode.gt.1) go to 3400
                    if(DrawMode.eq.1) then
                      call SetRealArrayTo(xd,3,0.)
                    else
                      if(TypeModFun(i).le.1) then
                        call MakePosMod(xd,x40,x4p,nx4,dx4,uxi,uyi,
     1                    KModA(2,i),KFA(2,i),GammaIntInv)
                      else
                        call MakePosModPol(xd,x40,x4p,nx4,dx4,uxi,uyi,
     1                                     KModA(2,i),ax(1,i),a0(i)*.5,
     2                                     GammaIntInv,TypeModFun(i))
                      endif
                    endif
                    if(MagParI.gt.0) then
!                      if(TypeModFun(i).le.1) then
                        call MakeMagMod(sm,x40,x4p,nx4,dx4,sm0i,smxi,
     1                                  smyi,MagParI-1,GammaIntInv)
!                      else
!                        call MakeMagModPol(sm,x40,x4p,nx4,dx4,sm0i,smxi,
!     1                                     smyi,MagParI-1,ax(1,i),
!     2                                     a0(i)*.5,GammaIntInv)
!                      endif
                      if(InvMag(KPhase).lt.0) then
                        pom=ScalMul(xt,s6(1,-InvMag(KPhase),iswp,KPhase)
     1                              )
                        if(abs(pom-anint(pom)).gt..001) sm=-sm
                      endif
                    else
                      call SetRealArrayTo(sm,3,0.)
                    endif
                    do j=1,3
                      xd(j)=x4(j)+xt(j)+xd(j)
                    enddo
                    if(itfi.ge.2) then
                      call SetRealArrayTo(bd,6,0.)
                      if(DrawMode.ne.1) then
                        if(TypeModFun(i).le.1) then
                          call MakeBetaMod(bd,x40,x4p,nx4,dx4,bxi,byi,
     1                                     KModA(3,i),KFA(3,i),
     2                                     GammaIntInv)
                        else
                          call MakeBetaModPol(bd,x40,x4p,nx4,dx4,bxi,
     1                                        byi,KModA(3,i),ax(1,i),
     2                                        a0(i)*.5,GammaIntInv,
     3                                        TypeModFun(i))
                        endif
                      endif
                      call AddVek(bd,bp,bd,6)
                    else
                      bd=bp
                    endif
                    call qbyx(xd,xd(4),iswp)
                    call AddVek(xd(4),trezg(1,iswp),xd(4),NDimI(KPhase))
                    call multm(ZVi(1,iswp,KPhase),xd,xf,NDim(KPhase),
     1                         NDim(KPhase),1)
                  endif
                  write(Veta,'(3i3)') ic1,ic2,ic3
                  call ZdrcniCisla(Veta,3)
                  Veta='('//Veta(:idel(Veta))//')'
                  do j=1,idel(Veta)
                    if(Veta(j:j).eq.' ') Veta(j:j)=','
                  enddo
                  if(itfi.ge.2) call multm(smtc(1,iswp),bd,bf,6,6,1)
                  do j=1,3
                    xdc(j)=(xf(j)-xmezc(1,j))*TrX (j,j)
                    xd(j) =(xf(j)-xmez (1,j,1))*TrXP(j,j)
                  enddo
                  do j=1,3
                    sm(j)=Bohr2Ang*sm(j)
                    if(xd(j).lt.-0.0001.or.xd(j).gt.1.0001) go to 3400
                  enddo
                  if(itfi.ge.2) call MultM(TrB,bf,bd,6,6,1)
                  ip=ip+1
                  if(KeepNames) then
                    natn=natn+1
                    write(Cislo,'(''-'',i5)') natn
                    call Zhusti(Cislo)
                    Veta=Atom(i)(:idel(Atom(i)))//
     1                   Cislo(:idel(Cislo))
                  else
                    natp(isfi)=natp(isfi)+1
                    write(Veta,'(a2,i4)') AtType(isfi,KPhase),
     1                                    natp(isfi)
                    call zhusti(Veta)
                  endif
                  call scode(isym,ivt,shc,ishc,iswp,t80,t40)
                  do j=nacp,nac
                    if(eqrv(xdc,xyz(1,j),3,.0001)) go to 3400
                    if(CheckBorders.and.DrawMode.ne.2) then
                      do k=1,3
                        xf(k)=xdc(k)-xyz(k,j)
2910                    if(xf(k).gt..5) then
                          xf(k)=xf(k)-1.
                          go to 2910
                        endif
2920                    if(xf(k).le.-.5) then
                          xf(k)=xf(k)+1.
                          go to 2920
                        endif
                        xf(k)=xf(k)/TrX(k,k)
                      enddo
                      call multm(MetTens(1,1,KPhase),xf,xg,3,3,1)
                      if(sqrt(scalmul(xf,xg)).lt.DminP) then
                        NInfo=NInfo+1
                        TextInfo(NInfo)=atom(i)
                        if(t40.ne.' ')
     1                    TextInfo(NInfo)=
     2                      TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     3                      '#'//t40(:idel(t40))
                        if(NInfo.ge.15) then
                          call FeInfoOut(-1.,-1.,Label,'L')
                          NInfo=0
                        endif
                        go to 3400
                      endif
                    endif
                  enddo
                  magn=0
3100              nac=nac+1
                  if(nac.gt.nalloc) then
                    allocate(xyzo(3,nalloc))
                    call CopyVek (xyz,xyzo,3*nalloc)
                    nalloco=nalloc
                    nalloc=nalloc+10000
                    deallocate(xyz)
                    allocate(xyz(3,nalloc))
                    call CopyVek (xyzo,xyz,3*nalloco)
                    deallocate(xyzo)
                  endif
                  if(magn.gt.0) then
                    isfp=isfh(KPhase)
                  else
                    isfp=isf(i)
                  endif
                  call CopyVek(xdc,xyz(1,nac),3)
                  if(UseVestaForMagDraw.and.MagneticType(KPhase).ne.0)
     1              then
                    if(MagParI.gt.0) then
                      MagParP=1
                    else
                      MagParP=0
                    endif
                  else if(IDrawMag.ne.1) then
                    MagParP=0
                  else
                    MagParP=-1
                  endif
                  if(MagParP.ge.0) then
                    write(ln,100) Veta(:8),isfp,itfi,MagParP,oi(1),
     1                            (xdc(j),j=1,3)
                    if(itfi.ge.2.or.(bd(1).gt.-9.9.and.bd(1).lt.99.))
     1                then
                      write(ln,101) bd
                    else
                      write(ln,102) bd
                    endif
                  endif
                  do ii=1,nac-1
                    if(EqRV(xyz(1,ii),xyz(1,nac),3,.00001)) then
                      naci=ii
                      go to 3120
                    endif
                  enddo
                  naci=naci+1
3120              if(MagneticType(KPhase).gt.0.and.magn.eq.0.and.
     1               MagParI.gt.0) then
                    if(EqRV0(sm,3,.0001)) then
                      Del=0.
                    else
                      call MultM(MetTens(1,1,KPhase),sm,xf,3,3,1)
                      Del=sqrt(scalmul(sm,xf))
                    endif
                    if(UseVestaForMagDraw) then
                      write(ln,105)(sm(k)*CellPar(k,isw,KPhase),k=1,3)
                    else if(IDrawMag.eq.1) then
                      if(Del.gt.0.)
     1                  write(LnInp,104) Veta(:8),(xdc(k),k=1,3),isfi,
     2                    (sm(k)*CellPar(k,isw,KPhase),k=1,3),Del
                    else
                      if(Del.gt.0.) then
                        magn=magn+1
                        call MultM(TrX,sm,xg,3,3,1)
                        call AddVek(xg,xdc,xdc,3)
                        Veta='H/'//Veta(1:6)
                        call SetRealArrayTo(bd,6,0.)
                        go to 3100
                      endif
                    endif
                  else
                    if(IDrawMag.eq.1.and.magn.le.0)
     1                write(LnInp,104) Veta(:8),(xdc(k),k=1,3),isfi,
     2                                 1.,(0.,k=1,2),0.5
                  endif
3400            continue
              enddo
            enddo
3700      continue
3900    continue
        if(MagParI.gt.0) then
          if(KUsePolar(i).gt.0) then
            call Fract2ShpCoor(sm0(1,i))
            do k=1,MagParI-1
              call Fract2ShpCoor(smx(1,k,i))
              call Fract2ShpCoor(smy(1,k,i))
            enddo
          endif
        endif
      enddo
      if(NInfo.gt.0) call FeInfoOut(-1.,-1.,Label,'L')
      nacp=nac
      NPhase=1
      KPhase=1
      grupa(KPhase)='P1'
      ngrupa(KPhase)=1
      CrSystem(KPhase)=1
      Lattice(KPhase)='P'
      NSymm(KPhase)=1
      NSymmN(KPhase)=1
      NDim(KPhase)=3
      NDimQ(KPhase)=9
      NDimI(KPhase)=0
      NComp(KPhase)=1
      NDatBlock=1
      itwin=1
      call UnitMat(rm6(1,1,1,KPhase),3)
      call SetRealArrayTo(s6(1,1,1,KPhase),NDim(KPhase),0.)
      call CodeSymm(rm6(1,1,1,KPhase),s6(1,1,1,KPhase),
     1              symmc(1,1,1,KPhase),0)
      call CopyVek(CellParNew,CellPar(1,1,KPhase),3)
      call SetMet(0)
      go to 4500
4100  if(DrawMode.eq.1.and.KCommen(KPhase).gt.0) then
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        call SetLogicalArrayTo(BratAtom,NAtCalc,.true.)
        do i=1,NAtCalc-1
          if(kswa(i).ne.KPhase.or..not.BratAtom(i)) cycle
          do j=i+1,NAtCalc
            if(kswa(j).ne.KPhase) cycle
            if(IdenticalAtoms(i,j,.001)) BratAtom(j)=.false.
          enddo
        enddo
      endif
      if(NDimI(KPhase).eq.1) then
        i=index(Grupa(KPhase),'(')
        if(i.gt.0) Grupa(1)=Grupa(KPhase)(:i-1)
      endif
      NPhase=1
      ndold=NDim(KPhase)
      nq=NDimQ(KPhase)
      NDim(1)=3
      NDimQ(1)=9
      NDimI(1)=0
      NComp(1)=1
      NSymm(1)=NSymm(KPhase)
      NSymmN(1)=NSymmN(KPhase)
      MagneticType(1)=MagneticType(KPhase)
      KPhase=1
      NDatBlock=1
      itwin=1
      do i=1,NSymm(1)
        if(UseVestaForMagDraw.and.MagneticType(KPhase).ne.0) then
          x0(1:ndold)=ShSg(1:ndold,KPhaseIn)
        else
          call Multm(rm6(1,i,1,KPhaseIn),ShSg(1,KPhaseIn),x0,ndold,
     1               ndold,1)
        endif
        call SetRealArrayTo(rm6(1,i,1,1),ndold,0.)
        call CopyMat(rm(1,i,1,KPhaseIn),rm6(1,i,1,1),3)
        do j=1,3
          s6(j,i,1,1)=s6(j,i,1,KPhaseIn)-x0(j)+ShSg(j,KPhaseIn)
        enddo
        call CodeSymm(rm6(1,i,1,1),s6(1,i,1,1),symmc(1,i,1,1),0)
        if(MagneticType(1).gt.0) then
          ZMag(i,1,1)=ZMag(i,1,KPhase)
          call CopyMat(rmag(1,i,1,KPhaseIn),rmag(1,i,1,1),3)
        endif
      enddo
      Lattice(1)=Lattice(KPhaseIn)
      NLattVec(1)=NLattVec(KPhaseIn)
      do i=1,NLattVec(1)
        call CopyVek(vt6(1,i,1,KPhaseIn),vt6(1,i,1,1),3)
      enddo
      NUnits(1)=NUnits(KPhaseIn)
      FFType(1)=FFType(KPhaseIn)
      Formula(1)=Formula(KPhaseIn)
      NAtFormula(1)=NAtFormula(KPhaseIn)
      nt=iabs(FFType(KPhaseIn))
      do i=1,NAtFormula(1)
        AtTypeFull(i,1)=AtTypeFull(i,KPhaseIn)
        AtType(i,1)=AtType(i,KPhaseIn)
        AtRadius(i,1)=AtRadius(i,KPhaseIn)
        AtColor(i,1)=AtColor(i,KPhaseIn)
        call CopyVek(FFBasic(1,i,KPhaseIn),FFBasic(1,i,1),nt)
      enddo
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_m40.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9900
      nacp=0
      do i=1,NAtCalc
        if(kswa(i).eq.KPhaseIn.and.BratAtom(i).and.ai(i).gt.0.) then
          itfi=min(itf(i),2)
          nacp=nacp+1
          if(UseVestaForMagDraw.and.MagneticType(KPhase).ne.0) then
            x0(1:3)=x(1:3,i)
          else
            call AddVek(x(1,i),ShSg(1,KPhaseIn),x0,3)
          endif
          write(ln,100) Atom(i),isf(i),itfi,MagPar(i),ai(i),
     1                  (x0(j),j=1,3)
          if(itfi.le.1) then
            write(ln,101)(beta(j,i),j=1,6)
          else
            if(beta(1,i).gt.-9.9.and.beta(1,i).lt.99.) then
              write(ln,101)(beta(j,i),j=1,6)
            else
              write(ln,102)(beta(j,i),j=1,6)
            endif
          endif
          if(MagPar(i).gt.0) then
            PolMag=ktat(NamePolar,NPolar,Atom(i)).gt.0
            if(PolMag) call ShpCoor2Fract(sm0(1,i),RPom)
            write(ln,105)(sm0(j,i)*CellPar(j,1,KPhaseIn),j=1,3)
            if(PolMag) call Fract2ShpCoor(sm0(1,i))
          endif
        endif
      enddo
      do i=1,6
        CellPar(i,1,1)=CellPar(i,1,KPhaseIn)
      enddo
4500  if(IDrawMag.eq.1) then
        close(LnInp)
      else
        ParentStructure=.false.
        call iom50(1,0,'#tmp#.m50')
        call iom50(0,0,'#tmp#.m50')
        rewind(ln)
        call OpenFile(m40,'#tmp#.m40','formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        if(nacp.ge.100000) then
          write(m40,'(i6,3i5)') nacp,0,1,1
        else
          write(m40,'(4i5)') nacp,0,1,1
        endif
        n=4
        if(KAnRef(KPhase).gt.0) n=n+NAtFormula(KPhase)
        pom=1.
        do i=1,n
          write(m40,'(f9.6)') pom
          pom=0.
        enddo
4600    read(ln,FormA,end=4650) Veta
        write(m40,FormA) Veta(:idel(Veta))
        go to 4600
4650    close(ln,status='delete')
      endif
      write(Veta,'(3f10.3)')(1./TrX(i,i),i=1,3)
      call ZdrcniCisla(Veta,3)
      do i=1,idel(Veta)
        if(Veta(i:i).eq.' ') Veta(i:i)='x'
      enddo
      Veta=Veta(:idel(Veta))//' supercell'
5000  call FeDateAndTime(ch9,ch8)
      Veta=Veta(:idel(Veta))//' '//ch9
      if(StructureName.ne.' ') Veta=StructureName(:idel(StructureName))
     1                              //' : '//Veta(:idel(Veta))
      if((UseVestaForMagDraw.or.MagneticType(KPhase).eq.0).and.
     1    IDrawMag.ne.1) then
        close(m40)
        call iom50(0,0,'#tmp#.m50')
        call iom40only(0,0,'#tmp#.m40')
        kk=0
        do i=1,NAtCalc-1
          if(ai(i).le.0.) cycle
          do j=i+1,NAtCalc
            if(isf(i).ne.isf(j)) cycle
            k=koinc(x(1,i),x(1,j),1,1,.001,dpom,iswa(i),iswa(j))
            if(k.gt.0) then
              kk=kk+1
              ai(j)=0.
            endif
          enddo
        enddo
        if(kk.gt.0) then
          call iom40only(1,0,'#tmp#.m40')
          call iom40only(0,0,'#tmp#.m40')
        endif
        if(Klic.eq.-7) then
          call GetExtentionOfFileName(FileName,Veta)
          if(.not.EqIgCase(Veta,'cif'))
     1      FileName=FileName(:idel(FileName))//'.cif'
        endif
        call TrM4050(Klic,FileName,Veta,ichp)
        if(Klic.eq.-7.and.
     1     LocateSubstring(CallGraphic,'Diamond.exe',.false.,.true.)
     2       .gt.0) call UpravGrupuProDiamond(FileName)
        call DeleteFile('#tmp#.m40')
        call DeleteFile('#tmp#.m50')
        call DeleteFile('#tmp#.l51')
      endif
      NDatBlock=NDatBlockIn
      if(IDrawMag.eq.1) ich=ich-1
      go to 9900
9000  if(OrthoOrd.gt.0) call trortho(1)
9900  call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9990
      call iom40Only(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.ne.0) go to 9990
9990  if(allocated(xyz)) deallocate(xyz)
      if(allocated(vt6p)) deallocate(vt6p)
      if(allocated(sngc)) deallocate(sngc,csgc)
      NPeak=0
      KPhase=KPhaseIn
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
9999  return
100   format(a8,3i3,1x,4f9.6)
101   format(6f9.6)
102   format(f9.2,5f9.6)
103   format(i4,1x,a2,2x,a8,f8.4,3f11.6,2a6/25x,3f11.6,f6.2)
104   format(a8,3f10.5,i4/8x,4f10.5)
105   format(3f9.4)
      end
