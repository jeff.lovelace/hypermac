      subroutine SpGroupAlias(Grp)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Grp
      character*256 Veta
      character*8   GrpBad
      logical EqIgCase
      lni=NextLogicNumber()
      if(NDimI(KPhase).eq.1) then
        idl=index(Grp,'(')
        if(idl.le.0) idl=idel(Grp)
      else
        idl=idel(Grp)
      endif
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1       'spgroup_alias.dat'
      else
        Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup_alias.dat'
      endif
      call OpenFile(lni,Veta,'formatted','old')
      read(lni,FormA) Veta
      if(Veta(1:1).ne.'#') rewind lni
1000  read(lni,FormA,end=9999) Veta
      k=0
      call kus(Veta,k,GrpBad)
      if(EqIgCase(GrpBad,Grp(:idl))) then
        call kus(Veta,k,GrpBad)
        Grp=GrpBad(:idel(GrpBad))//Grp(idl+1:)
        go to 9999
      else
        go to 1000
      endif
9999  call CloseIfOpened(lni)
      return
      end
