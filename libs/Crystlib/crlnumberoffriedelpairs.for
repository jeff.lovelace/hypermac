      integer function CrlNumberOfFriedelPairs()
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iha(:,:),nr(:),ih(6),ihp(6)
      logical EqIVM
      allocatable iha,nr
      CrlNumberOfFriedelPairs=0
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m83','formatted','old')
      if(ErrFlag.ne.0) go to 9000
      n=0
1100  read(ln,format83e,end=1200,err=9999) i
      n=n+1
      go to 1100
1200  allocate(iha(6,n),nr(n))
      call FeFlowChartOpen(-1.,-1.,max(nint(float(n)*.005),10),n,
     1  'Counting of used Friedel pairs',' ',' ')
      m=0
      rewind ln
      k=0
1500  do 1900i=1,n
        call FeFlowChartEvent(k,ie)
        read(ln,format83e,end=2000,err=9999)(ihp(j),j=1,NDim(KPhase)),
     1                                       p,p,p,Cislo(1:1),j
        if(j.lt.0) go to 1900
        do ns=1,NSymmN(KPhase)
          call MultMIRI(ihp,rm6(1,ns,1,KPhase),ih,1,NDim(KPhase),
     1                  NDim(KPhase))
          do j=1,m
            if(EqIVM(ih,iha(1,j),NDim(KPhase))) then
              nr(j)=nr(j)+1
              go to 1900
            endif
          enddo
        enddo
        m=m+1
        call CopyVekI(ihp,iha(1,m),NDim(KPhase))
        nr(m)=0
1900  continue
      call FeFlowChartRemove
2000  do i=1,m
        if(nr(i).gt.0) CrlNumberOfFriedelPairs=CrlNumberOfFriedelPairs+1
      enddo
9000  if(allocated(iha)) deallocate(iha,nr)
      call CloseIfOpened(ln)
9999  return
      end
