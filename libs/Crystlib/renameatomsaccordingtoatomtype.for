      subroutine RenameAtomsAccordingToAtomType(namax)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*2 atp(20),at
      dimension   natp(20)
      logical EqIgCase
      n=0
      do KPh=1,NPhase
        do 1100i=1,NAtFormula(KPh)
          do j=1,n
            if(EqIgCase(AtType(i,KPhase),atp(j))) go to 1100
          enddo
          n=n+1
          atp(n)=AtType(i,KPhase)
1100    continue
      enddo
      call SetIntArrayTo(natp,n,0)
      do 1200i=1,namax
        if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
        j=isf(i)
        at=AtType(isf(i),kswa(i))
        do k=1,n
          if(EqIgCase(at,atp(k))) then
            natp(k)=natp(k)+1
            write(Atom(i),'(a2,i4)') atp(k),natp(k)
            call zhusti(Atom(i))
            go to 1200
          endif
        enddo
1200  continue
      return
      end
