      subroutine iom41(klic,tisk,FileName)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension Vek001(3),xp(6),kip(6)
      integer tisk
      character*(*) FileName
      character*256 t256,PureFileName
      character*80  Radka,t80
      logical eqrv,poprve,EqIgCase,PwdCheckTOFInD
      character*8 nazev,IdP
      character*9 IdPP
      equivalence (t80,t256)
      data Vek001/0.,0.,1./
      KPhaseIn=KPhase
      call GetPureFileName(FileName,PureFileName)
      if(klic.le.0) then
        call OpenFile(m41,FileName,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        call SetBasicM41(0)
        ln=NextLogicNumber()
        do i=1,NIdM41
          KPhase=0
          KDatB=0
          IZdvih=0
          rewind m41
1000      poprve=.true.
1005      k=80
1010      if(k.eq.80) then
1015        read(m41,FormA,err=9000,end=9010) Radka
            t80=Radka
            if(Radka(1:1).eq.'*'.or.Radka(1:1).eq.'#'.or.
     1         idel(Radka).le.0.or.Radka(1:2).eq.'--') go to 1015
            if(EqIgCase(Radka,'end')) cycle
            call Vykric(Radka)
            if(Radka(1:1).eq.'!') Radka(1:1)=' '
            k=0
          endif
          call kus(Radka,k,Nazev)
          j=islovo(Nazev,idM41,NIdM41)
          if(j.eq.IdM41DatBlock) then
1100        KDatB=KDatB+1
            if(iabs(DataType(KDatB)).ne.2) then
              if(KDatB.ge.NDatBlock) then
                cycle
              else
                IZdvih=IZdvih+NParRecPwd
                go to 1100
              endif
            endif
            KPhase=0
            go to 1000
          else if(j.eq.IdM41Phase) then
            KPhase=KPhase+1
            go to 1000
          else if(j.ne.i) then
            go to 1010
          else
            KDatB=max(KDatB,1)
            KPh=max(KPhase,1)
            if(NAtIndFrAll(KPh).le.0) SatFrMod(KPh,KDatB)=.false.
            if(i.eq.IdM41LamFile) then
              KUseLamFile(KDatB)=1
              call kus(Radka,k,LamFile(KDatB))
              go to 1010
            else if(i.eq.IdM41BroadHKL) then
              if(NBroadHKLPwd(KPh,KDatB).lt.10) then
                n=NBroadHKLPwd(KPh,KDatB)+1
                call Kus(Radka,k,t80)
                StGBroadHKLPwd(n,KPh,KDatB)=t80
                call EqStringToIntMatrix(t80,hklmnp(:maxNDim),maxNDim,
     1                                   HMBroadHKLPwd(1,n,KPh,KDatB),
     2                                   HMABroadHKLPwd(1,n,KPh,KDatB),
     3                                   ich)
                if(ich.ne.0) go to 9000
                call Kus(Radka,k,t80)
                if(t80.eq.':') call Kus(Radka,k,t80)
                StVBroadHKLPwd(n,KPh,KDatB)=t80
                call EqStringToModEq(t80,hklmnp(:maxNDim),maxNDim,
     1                               HVBroadHKLPwd(1,n,KPh,KDatB),
     2                               EqVBroadHKLPwd(n,KPh,KDatB),
     3                               ModVBroadHKLPwd(n,KPh,KDatB),ich)
                if(ich.ne.0) go to 9000
                if(k.eq.len(Radka)) then
                  StNBroadHKLPwd(n,KPh,KDatB)=' '
                  HNBroadHKLPwd(1:6,n,KPh,KDatB)=0
                  EqNBroadHKLPwd(n,KPh,KDatB)=0
                  ModNBroadHKLPwd(n,KPh,KDatB)=0
                  NBroadHKLPwd(KPh,KDatB)=n
                  go to 1010
                endif
                call kus(Radka,k,t80)
                if(EqIgCase(t80,'except')) call kus(Radka,k,t80)
                call EqStringToModEq(t80,hklmnp(:maxNDim),maxNDim,
     1                               HNBroadHKLPwd(1,n,KPh,KDatB),
     2                               EqNBroadHKLPwd(n,KPh,KDatB),
     3                               ModNBroadHKLPwd(n,KPh,KDatB),ich)
                if(ich.ne.0) go to 9000
                NBroadHKLPwd(KPh,KDatB)=n
              endif
              go to 1010
            endif
            call kus(Radka,k,Cislo)
            call Posun(Cislo,1)
            read(Cislo,104,err=9000) Value
            IValue=nint(Value)
            if(i.ne.IdM41SkipFrTo.and..not.poprve) go to 1200
            if(i.eq.IdM41KBackg) then
              KBackg(KDatB)=IValue
            else if(i.eq.IdM41NBackg) then
              NBackg(KDatB)=IValue
            else if(i.eq.IdM41KUseInvX) then
              KUseInvX(KDatB)=IValue
            else if(i.eq.IdM41KManBackg) then
              KManBackg(KDatB)=IValue
            else if(i.eq.IdM41KWleBail) then
              KWleBail(KDatB)=IValue
            else if(i.eq.IdM41KKleBail) then
              KKleBail(KDatB)=IValue
            else if(i.eq.IdM41KAbsor) then
              KAbsor(KDatB)=max(IValue,0)
            else if(i.eq.IdM41Mir) then
              MirPwd(KDatB)=Value
            else if(i.eq.IdM41KUseTOFJason) then
              KUseTOFJason(KDatB)=IValue
            else if(i.eq.IdM41TOFKey) then
              TOFKey(KDatB)=IValue
            else if(i.eq.IdM41SkipFrTo) then
              NSkipPwd(KDatB)=NSkipPwd(KDatB)+1
              n=NSkipPwd(KDatB)
              if(n.ge.MxSkipPwd) then
                if(n.eq.MxSkipPwd)
     1            call FeChybne(-1.,-1.,'number of skip regions '//
     2                        'exceeds the limit.',' ',SeriousError)
                go to 1000
              endif
              SkipPwdFr(n,KDatB)=Value
              call kus(Radka,k,Cislo)
              call posun(cislo,1)
              read(cislo,104,err=9000) SkipPwdTo(n,KDatB)
              if(t80(1:1).eq.'!') then
                SkipPwdFr(n,KDatB)=-SkipPwdFr(n,KDatB)-200.
                SkipPwdTo(n,KDatB)=-SkipPwdTo(n,KDatB)-200.
              endif
              go to 1000
            else if(i.eq.IdM41KProfPwd) then
              KProfPwd(KPh,KDatB)=IValue
            else if(i.eq.IdM41KAsym) then
              KAsym(KDatB)=IValue
            else if(i.eq.IdM41KPartBroad) then
              KPartBroad(KPh,KDatB)=IValue
            else if(i.eq.IdM41KStrain) then
              KStrain(KPh,KDatB)=IValue
            else if(i.eq.IdM41KPref) then
              KPref(KPh,KDatB)=IValue
            else if(i.eq.IdM41SPref) then
              SPref(KPh,KDatB)=Value
            else if(i.eq.IdM41PCutOff) then
              PCutOff(KPh,KDatB)=Value
            else if(i.eq.IdM41DirPref) then
              DirPref(1,KPh,KDatB)=Value
              call StToReal(Radka,k,DirPref(2,KPh,KDatB),2,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM41DirBroad) then
              DirBroad(1,KPh,KDatB)=Value
              call StToReal(Radka,k,DirBroad(2,KPh,KDatB),2,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdM41MMax) then
              MMaxPwd(1,KPh,KDatB)=IValue
              if(NDimI(KPh).gt.1) then
                call StToInt(Radka,k,MMaxPwd(2,KPh,KDatB),NDimI(KPh)-1,
     1                       .false.,ich)
                if(ich.ne.0) go to 9000
              endif
            else if(i.eq.IdM41SatFrMod) then
              if(NAtIndFrAll(KPh).gt.0) SatFrMod(KPh,KDatB)=IValue.eq.1
            else if(i.eq.IdM41KRefLam) then
              KRefLam(KDatB)=IValue
            else if(i.eq.IdM41SkipFrdl) then
              SkipFriedel(KPh,KDatB)=IValue.eq.1
            else if(i.eq.IdM41KRough) then
              KRough(KDatB)=IValue
            else if(i.eq.IdM41RadPrim) then
              RadPrim(KDatB)=Value
            else if(i.eq.IdM41RadSec) then
              RadSec(KDatB)=Value
            else if(i.eq.IdM41KUseRSW) then
              KUseRSW(KDatB)=IValue
            else if(i.eq.IdM41KUseFDSA.or.i.eq.IdM41KUseDS) then
              KUseDS(KDatB)=IValue
            else if(i.eq.IdM41KUsePSoll) then
              KUsePSoll(KDatB)=IValue
            else if(i.eq.IdM41KUseSSoll) then
              KUseSSoll(KDatB)=IValue
            else if(i.eq.IdM41CutOffMn) then
              CutOffMinPwd=Value
              UseCutOffPwd=.true.
            else if(i.eq.IdM41CutOffMx) then
              CutOffMaxPwd=Value
              UseCutOffPwd=.true.
            else if(i.eq.IdM41UseTOFAbs) then
              KUseTOFAbs(KDatB)=IValue
            else if(i.eq.IdM41KIllum) then
              KIllum(KDatB)=IValue
            else if(i.eq.IdM41KFocusBB) then
              KFocusBB(KDatB)=IValue
            else if(i.eq.IdM41KUseHS) then
              KUseHS(KDatB)=IValue
            endif
          endif
          poprve=.false.
          go to 1010
1200      call FeChybne(-1.,-1.,'duplicate occurrence of "'//
     1                  nazev(:idel(nazev))//'"',
     2                  'remember that the first occurrence will be '//
     3                  'accepted',Warning)
        enddo
        call CloseIfOpened(ln)
        do KDatB=1,NDatBlock
          call PwdSetTOF(KDatB)
          if(isTOF) then
            if(KAsym(KDatB).eq.IdPwdAsymTOFNone) then
              NAsym(KDatB)=0
            else if(KAsym(KDatB).eq.IdPwdAsymTOF1) then
              NAsym(KDatB)=4
            else if(KAsym(KDatB).eq.IdPwdAsymTOF2) then
              NAsym(KDatB)=8
            endif
          else
            if(KAsym(KDatB).eq.IdPwdAsymNone) then
              NAsym(KDatB)=0
            else if(KAsym(KDatB).eq.IdPwdAsymSimpson) then
              NAsym(KDatB)=1
            else if(mod(KAsym(KDatB),100).eq.IdPwdAsymBerar) then
              NAsym(KDatB)=KAsym(KDatB)/100
              if(NAsym(KDatB).eq.0) NAsym(KDatB)=4
              KAsym(KDatB)=mod(KAsym(KDatB),100)
            else if(KAsym(KDatB).eq.IdPwdAsymDivergence) then
              NAsym(KDatB)=2
            else if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
              NAsym(KDatB)=7
              if(KUseLamFile(KDatB).eq.1) then
                if(OpSystem.le.0) then
                  t80=HomeDir(:idel(HomeDir))//'lam'//ObrLom//
     1                LamFile(KDatB)(:idel(LamFile(KDatB)))
                else
                  t80=HomeDir(:idel(HomeDir))//'lam/'//
     1                LamFile(KDatB)(:idel(LamFile(KDatB)))
                endif
                call OpenFile(ln,t80,'formatted','old')
                if(ErrFlag.ne.0) go to 1330
                n=0
1320            read(ln,FormA,end=1330) t80
                k=0
                call StToReal(t80,k,xp,3,.false.,ich)
                if(ich.ne.0) then
                  call FeChybne(-1.,-1.,'wrong record on '//
     1              LamFile(KDatB)(:idel(LamFile(KDatB)))//' file.',t80,
     2              SeriousError)
                  n=0
                  go to 1330
                endif
                n=n+1
                LamSpectRel (n,KDatB)=xp(1)
                LamSpectLen (n,KDatB)=xp(2)
                LamSpectLife(n,KDatB)=xp(3)
                go to 1320
1330            call CloseIfOpened(ln)
                NLamSpect(KDatB)=n
                call PwdSetLamProfile(KDatB)
              endif
            else if(KAsym(KDatB).eq.IdPwdAsymDebyeInt) then
              NAsym(KDatB)=6
            endif
            if(PwdMethod(KDatB).eq.IdPwdMethodDS) then
              KIllum(KDatB)=0
              KFocusBB(KDatB)=0
            else if(PwdMethod(KDatB).eq.IdPwdMethodBBVDS) then
              KIllum(KDatB)=0
              KFocusBB(KDatB)=1
            else if(PwdMethod(KDatB).eq.IdPwdMethodBBFDS) then
              KIllum(KDatB)=2
              KFocusBB(KDatB)=1
            endif
          endif
          if(KAbsor(KDatB).eq.IdPwdAbsorbCylinder.and.
     1       MirPwd(KDatB).le.0.) then
            KAbsor(KDatB)=IdPwdAbsorbNone
          endif
        enddo
      else
        call PwdResetVoigt
        call OpenFile(m41,FileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        if(UseCutOffPwd) then
          write(Radka,100) CutOffMinPwd,IdM41(IdM41CutOffMx),
     1                     CutOffMaxPwd
          k=0
          call WriteLabeledRecord(m41,IdM41(IdM41CutOffMn),Radka,k)
        endif
        do KDatB=1,NDatBlock
          if(iabs(DataType(KDatB)).ne.2) cycle
          call PwdSetTOF(KDatB)
          if(NDatBlock.gt.1) then
            write(Radka,'(a)') DatBlockName(KDatB)
            k=0
            call WriteLabeledRecord(m41,IdM41(IdM41DatBlock),Radka,k)
          endif
          if(KAsym(KDatB).eq.IdPwdAsymBerar.and..not.isTOF) then
            i=KAsym(KDatB)+100*NAsym(KDatB)
          else
            i=KAsym(KDatB)
          endif
          write(t256,101)  KBackg(KDatB),
     1                     IdM41(IdM41NBackg),NBackg(KDatB),
     2                     IdM41(IdM41KManBackg),KManBackg(KDatB),
     3                     IdM41(IdM41KUseInvX),KUseInvX(KDatB),
     4                     IdM41(IdM41KWleBail),KWleBail(KDatB),
     5                     IdM41(IdM41KKleBail),KKleBail(KDatB),
     6                     IdM41(IdM41KAsym),i,
     7                     IdM41(IdM41KUseHS),KUseHS(KDatB)
          k=0
          call WriteLabeledRecord(m41,IdM41(IdM41KBackg),t256,k)
          if(.not.isTOF.and..not.isED) then
            t256=' '
            if(PwdMethod(KDatB).eq.IdPwdMethodBBFDS.or.
     1         PwdMethod(KDatB).eq.IdPwdMethodBBVDS) then
              write(t256,101) KRough(KDatB)
            else if(PwdMethod(KDatB).eq.IdPwdMethodUnknown) then
              write(t256,101) KRough(KDatB),
     1                        IdM41(IdM41KIllum),KIllum(KDatB),
     2                        IdM41(IdM41KFocusBB),KFocusBB(KDatB)
            endif
            if(t256.ne.' ') then
              k=0
              call WriteLabeledRecord(m41,IdM41(IdM41KRough),t256,k)
            endif
          endif
          Nazev=' '
          idl=0
          if(KRefLam(KDatB).eq.1) then
            Nazev=IdM41(IdM41KRefLam)
            write(Radka,101) KRefLam(KDatB)
            idl=idel(Radka)
          else if(KUseLamFile(KDatB).eq.1.and.
     1            KAsym(KDatB).eq.IdPwdAsymFundamental) then
            Nazev=IdM41(IdM41LamFile)
            Radka=LamFile(KDatB)
            idl=idel(LamFile(KDatB))
          endif
          if(isTOF) then
            if(idl.le.0) then
              Nazev=IdM41(IdM41TOFKey)
              write(Radka,101) TOFKey(KDatB),
     1                         IdM41(IdM41UseTOFAbs),
     2                         KUseTOFAbs(KDatB),
     3                         IdM41(IdM41KUseTOFJason),
     4                         KUseTOFJason(KDatB)
            else
              write(Radka(idl+2:),111) IdM41(IdM41TOFKey),
     1                                 TOFKey(KDatB),
     2                                 IdM41(IdM41UseTOFAbs),
     3                                 KUseTOFAbs(KDatB),
     4                                 IdM41(IdM41KUseTOFJason),
     5                                 KUseTOFJason(KDatB)
            endif
            idl=idel(Radka)
          else
            if(KAbsor(KDatB).gt.0) then
              if(idl.le.0) then
                Nazev=IdM41(IdM41KAbsor)
                write(Radka,103) KAbsor(KDatB)
              else
                write(Radka(idl+2:),111) IdM41(IdM41KAbsor),
     1                                   KAbsor(KDatB)
              endif
              idl=idel(Radka)
              write(Radka(idl+2:),112) IdM41(IdM41Mir),MirPwd(KDatB)
              idl=idel(Radka)
            endif
          endif
          if(idl.gt.0) then
            k=0
            call WriteLabeledRecord(m41,Nazev,Radka,k)
          endif
          if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
            write(t256,102) RadPrim(KDatB)
            idl=idel(t256)
            write(t256(idl+2:),112) IdM41(IdM41RadSec),RadSec(KDatB)
            idl=idel(t256)
            write(t256(idl+2:),111)
     1        IdM41(IdM41KUseRSW),KUseRSW(KDatB),
     2        IdM41(IdM41KUseDS),KUseDS(KDatB),
     3        IdM41(IdM41KUsePSoll),KUsePSoll(KDatB),
     4        IdM41(IdM41KUseSSoll),KUseSSoll(KDatB)
            k=0
            call WriteLabeledRecord(m41,IdM41(IdM41RadPrim),t256,k)
          endif
          IdP=IdM41(IdM41SkipFrTo)
          idl=idel(IdP)
          do j=1,NSkipPwd(KDatB)
            if(SkipPwdFr(j,KDatB).gt.-180.) then
              write(Radka,FormatSkipFrTo(KDatB)) SkipPwdFr(j,KDatB),
     1                                           SkipPwdTo(j,KDatB)
              IdPP=IdP
            else
              write(Radka,FormatSkipFrTo(KDatB))
     1          -SkipPwdFr(j,KDatB)-200.,-SkipPwdTo(j,KDatB)-200.
              IdPP='!'//IdP(:idl)
            endif
            k=0
            call WriteLabeledRecord(m41,IdPP,Radka,k)
          enddo
          do KPh=1,NPhase
            if(NPhase.gt.1) then
              t80=IdM41(IdM41Phase)(:idel(IdM41(IdM41Phase)))//' '//
     1            PhaseName(KPh)(:idel(PhaseName(KPh)))
              write(m41,FormA) t80(:idel(t80))
            endif
            write(Radka,101) KProfPwd(KPh,KDatB),IdM41(IdM41KPartBroad),
     1                       KPartBroad(KPh,KDatB),IdM41(IdM41KStrain),
     2                       KStrain(KPh,KDatB)
            idl=idel(Radka)
            write(Radka(idl+2:),110)
     1        IdM41(IdM41PCutOff),PCutOff(KPh,KDatB)
            k=0
            call WriteLabeledRecord(m41,IdM41(IdM41KProfPwd),Radka,k)
            if(KPref(KPh,KDatB).ne.IdPwdPrefNone) then
              write(Radka,101) KPref(KPh,KDatB)
              idl=idel(Radka)+1
              write(Radka(idl+2:),111)
     1          IdM41(IdM41SPref),SPref(KPh,KDatB)
              k=0
              call WriteLabeledRecord(m41,IdM41(IdM41KPref),Radka,k)
            endif
            if(.not.eqrv(DirPref(1,KPh,KDatB),Vek001,3,.0001).and.
     1         KPref(KPh,KDatB).ne.IdPwdPrefNone) then
              write(Radka,102)(DirPref(i,KPh,KDatB),i=1,3)
              k=0
              call WriteLabeledRecord(m41,IdM41(IdM41DirPref),Radka,k)
            endif
            if(.not.eqrv(DirBroad(1,KPh,KDatB),Vek001,3,.0001).and.
     1         (KStrain(KPh,KDatB).eq.IdPwdStrainAxial.or.
     2          abs(LorentzPwd(2,KPh,KDatB)).gt.0.)) then
              write(Radka,102)(DirBroad(i,KPh,KDatB),i=1,3)
              k=0
              call WriteLabeledRecord(m41,IdM41(IdM41DirBroad),Radka,k)
            endif
            if(SkipFriedel(KPh,KDatB)) then
              i=1
            else
              i=0
            endif
            write(Radka,103) i
            k=0
            call WriteLabeledRecord(m41,IdM41(IdM41SkipFrdl),Radka,k)
            if(NDimI(KPh).gt.0.or.NQMag(KPh).gt.0) then
              if(SatFrMod(KPh,KDatB).and.NAtIndFrAll(KPh).gt.0) then
                i=1
              else
                SatFrMod(KPh,KDatB)=.false.
                i=0
              endif
              write(Radka,103) i
              k=0
              call WriteLabeledRecord(m41,IdM41(IdM41SatFrMod),Radka,k)
              if(.not.SatFrMod(KPh,KDatB)) then
                write(Radka,103)
     1            (MMaxPwd(i,KPh,KDatB),i=1,max(NDimI(KPh),1))
                k=0
                call WriteLabeledRecord(m41,IdM41(IdM41MMax),Radka,k)
              endif
            endif
            do i=1,NBroadHKLPwd(KPh,KDatB)
              Radka=StGBroadHKLPwd(i,KPh,KDatB)
     1              (:idel(StGBroadHKLPwd(i,KPh,KDatB)))// ' : '//
     2              StVBroadHKLPwd(i,KPh,KDatB)
     3              (:idel(StVBroadHKLPwd(i,KPh,KDatB)))
              t80=StNBroadHKLPwd(i,KPh,KDatB)
              if(t80.ne.' ')
     1          Radka=Radka(:idel(Radka))//' except '//t80(:idel(t80))
              k=0
              call WriteLabeledRecord(m41,IdM41(IdM41BroadHKL),Radka,k)
            enddo
          enddo
        enddo
        write(m41,'(''end'')')
      endif
      IZdvih=0
      do kk=1,2
        if(Klic.eq.0) then
          if(kk.eq.2) read(m41,FormA,err=9000,end=3000) t80
        else
          if(kk.eq.1) then
            write(m41,'(79(''*''))')
          else
            write(m41,'(28(''-''),''   s.u. block   '',35(''-''))')
          endif
        endif
        do KDatB=1,NDatBlock
          if(iabs(DataType(KDatB)).ne.2) go to 2400
          call PwdSetTOF(KDatB)
          if(DataType(KDatB).ne.2.and.KUseTOFJason(KDatB).ge.1) then
            nsh=5
          else
            nsh=3
          endif
          if(klic.le.0) then
            call SkipComments(m41)
            if(ErrFlag.ne.0.and.kk.eq.1) then
              go to 9000
            else
              ErrFlag=0
            endif
          else
            if(NDatBlock.gt.1) then
              write(t80,'(''## datblock '',a)') DatBlockName(KDatB)
              write(m41,FormA) t80(:idel(t80))
            endif
            if(DataType(KDatB).eq.2.or.
     1        Radiation(KDatB).eq.NeutronRadiation) then
              if(DataType(KDatB).eq.2) then
                t80='# Shift parameters - zero, sycos, sysin'
              else
                if(KUseTOFJason(KDatB).le.0) then
                  t80='# TOF parameters - zero, difc, difa'
                else
                  t80='# TOF parameters - zerot, ct, at, zeroe, ce'
                  nsh=5
                endif
              endif
              write(m41,FormA) t80(:idel(t80))
            endif
          endif
          if(kk.eq.1) then
            call iost(m41,ShiftPwd(1,KDatB),kipwd(IShiftPwd+IZdvih),nsh,
     1                klic,tisk)
            if(ErrFlag.ne.0) go to 9000
          else
            call iosts(m41,ShiftPwdS(1,KDatB),nsh,klic)
          endif
          if(isTOF.and.KUseTOFJason(KDatB).ge.1) then
            if(klic.le.0) then
              call SkipComments(m41)
              if(ErrFlag.ne.0.and.kk.eq.1) then
                go to 9000
              else
                ErrFlag=0
              endif
            else
              t80='# TOF parameters - WCross, TCross'
              write(m41,FormA) t80(:idel(t80))
            endif
            if(kk.eq.1) then
              call iost(m41,ShiftPwd(nsh+1,KDatB),
     1                  kipwd(IShiftPwd+IZdvih+nsh),2,klic,tisk)

              if(ErrFlag.ne.0) go to 9000
            else
              call iosts(m41,ShiftPwdS(nsh+1,KDatB),2,klic)
            endif
          endif
          if(NBackg(KDatB).gt.0) then
            if(klic.le.0) then
              call SkipComments(m41)
              if(ErrFlag.ne.0.and.kk.eq.1) then
                go to 9000
              else
                ErrFlag=0
              endif
            else
              write(m41,'(''# Background parameters'')')
            endif
            if(kk.eq.1) then
              call iost(m41,BackgPwd(1,KDatB),kiPwd(IBackgPwd+IZdvih),
     1                  NBackg(KDatB),klic,tisk)
              if(ErrFlag.ne.0) go to 9000
            else
              call iosts(m41,BackgPwdS(1,KDatB),NBackg(KDatB),klic)
            endif
          endif
          if(KRough(KDatB).gt.0) then
            if(klic.le.0) then
              call SkipComments(m41)
              if(ErrFlag.ne.0.and.kk.eq.1) then
                go to 9000
              else
                ErrFlag=0
              endif
            else
              write(m41,'(''# Roughness parameters'')')
            endif
            if(kk.eq.1) then
              call iost(m41,RoughPwd(1,KDatB),kiPwd(IRoughPwd+IZdvih),2,
     1                  klic,tisk)
              if(ErrFlag.ne.0) go to 9000
            else
              call iosts(m41,RoughPwdS(1,KDatB),2,klic)
            endif
          endif
          if(KRefLam(KDatB).gt.0) then
            if(klic.le.0) then
              call SkipComments(m41)
              if(ErrFlag.ne.0.and.kk.eq.1) then
                go to 9000
              else
                ErrFlag=0
              endif
            else
              write(m41,'(''# Wave length'')')
            endif
            if(kk.eq.1) then
              call iost(m41,LamPwd(1,KDatB),kiPwd(ILamPwd+IZdvih),
     1                  Nalfa(KDatB),klic,tisk)
              LamA1(KDatB)=LamPwd(1,KDatB)
              if(Nalfa(KDatB).gt.1) then
                LamA2(KDatB)=LamPwd(2,KDatB)
                LamAve(KDatB)=(LamAve(KDatB)+LamRat(KDatB)*LamA2(KDatB))
     1                        /(1.+LamRat(KDatB))
              else
                LamAve(KDatB)=LamPwd(1,KDatB)
              endif
              LorentzToCrySize(KDatB)=10.*LamAvePwd(KDatB)/ToRad
              GaussToCrySize(KDatB)=sqrt8ln2*LorentzToCrySize(KDatB)
              if(ErrFlag.ne.0) go to 9000
            else
              call iosts(m41,LamPwdS(1,KDatB),Nalfa(KDatB),klic)
            endif
          endif
          if((KAsym(KDatB).ne.IdPwdAsymNone.and..not.isTOF).or.
     1       (KAsym(KDatB).ne.IdPwdAsymTOFNone.and.isTOF)) then
            j=IAsymPwd+IZdvih
            if(klic.le.0) then
              call SkipComments(m41)
              if(ErrFlag.ne.0.and.kk.eq.1) then
                go to 9000
              else
                ErrFlag=0
              endif
            else
              if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                write(m41,'(''# Fundamental parameters'')')
                do i=1,7
                  if(i.eq.2.or.i.eq.6.or.i.eq.7) then
                    if(kk.eq.1) then
                      AsymPwd(i,KDatB)=AsymPwd(i,KDatB)/ToRad
                    else
                      AsymPwdS(i,KDatB)=AsymPwdS(i,KDatB)/ToRad
                    endif
                  endif
                enddo
              else
                write(m41,'(''# Asymmetry parameters'')')
              endif
            endif
            if(kk.eq.1) then
              call iost(m41,AsymPwd(1,KDatB),kiPwd(j),NAsym(KDatB),klic,
     1                  tisk)
              if(ErrFlag.ne.0) go to 9000
            else
              call iosts(m41,AsymPwdS(1,KDatB),NAsym(KDatB),klic)
            endif
            if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
              do i=1,7
                if(i.eq.6.and.KUsePSoll(KDatB).eq.0.or.
     1             i.eq.7.and.KUseSSoll(KDatB).eq.0) KiPwd(j+i-1)=0
                if(i.eq.2.or.i.eq.6.or.i.eq.7) then
                  if(kk.eq.1) then
                    AsymPwd(i,KDatB)=AsymPwd(i,KDatB)*ToRad
                  else
                    AsymPwdS(i,KDatB)=AsymPwdS(i,KDatB)*ToRad
                  endif
                endif
              enddo
            else if(isTOF.and.KAsym(KDatB).eq.IdPwdAsymTOF1.and.
     1              Klic.eq.0) then
              if(TOFKey(KDatB).eq.0) then
                do i=1,4
                  if(kk.eq.1) then
                    AsymPwd(i,KDatB)=AsymPwd(i,KDatB)*.001
                  else
                    AsymPwdS(i,KDatB)=AsymPwdS(i,KDatB)*.001
                  endif
                enddo
              endif
            endif
          endif
          if(isTOF.and.KUseTOFAbs(KDatB).ne.0) then
            j=ITOFAbsPwd+IZdvih
            if(klic.le.0) then
              call SkipComments(m41)
              if(ErrFlag.ne.0.and.kk.eq.1) then
                go to 9000
              else
                ErrFlag=0
              endif
            else
              write(m41,'(''# TOF absorption parameter'')')
            endif
            if(kk.eq.1) then
              call iost(m41,TOFAbsPwd(KDatB),kiPwd(j),1,klic,tisk)
              if(ErrFlag.ne.0) go to 9000
            else
              call iosts(m41,TOFAbsPwdS(KDatB),1,klic)
            endif
          endif
2400      IZdvih=IZdvih+NParRecPwd
        enddo
        IZdvihP=0
        do KPh=1,NPhase
          if(NPhase.gt.1.and.Klic.gt.0) then
            t80='### '//IdM41(IdM41Phase)(:idel(IdM41(IdM41Phase)))//
     1          ' '//PhaseName(KPh)(:Idel(PhaseName(KPh)))
            write(m41,FormA) t80(:idel(t80))
          endif
          if(klic.le.0) then
            call SkipComments(m41)
            if(ErrFlag.ne.0.and.kk.eq.1) then
              go to 9000
            else
              ErrFlag=0
            endif
          else
            write(m41,'(''# Cell parameters - a,b,c,alpha,beta,'',
     1                  ''gamma'')')
          endif
          j=ICellPwd+IZdvihP
          if(kk.eq.1) then
            call iost(m41,CellPwd(1,KPh),kiPwd(j),6,klic,tisk)
          else
            call iosts(m41,CellPwdS(1,KPh),6,klic)
          endif
          if(NDimI(KPh).gt.0.or.NQMag(KPh).gt.0) then
            if(klic.le.0) then
              call SkipComments(m41)
              if(ErrFlag.ne.0.and.kk.eq.1) then
                go to 9000
              else
                ErrFlag=0
              endif
            else
              write(m41,'(''# Modulation vector(s)'')')
            endif
            j=IQuPwd+IZdvihP
            do i=1,Max(NDimI(KPh),1)
              if(kk.eq.1) then
                call iost(m41,QuPwd(1,i,KPh),kiPwd(j),3,klic,tisk)
              else
                call iosts(m41,QuPwdS(1,i,KPh),3,klic)
              endif
              if(NQMag(KPh).gt.0) then
                call CopyVek(QuPwd(1,i,KPh),QMag(1,1,KPh),3)
              else
                call CopyVek(QuPwd(1,i,KPh),Qu(1,i,1,KPh),3)
              endif
              j=j+3
            enddo
          endif
          IZdvih=IZdvihP
          do KDatB=1,NDatBlock
            if(iabs(DataType(KDatB)).ne.2) go to 2700
            call PwdSetTOF(KDatB)
            if(NDatBlock.gt.1.and.Klic.gt.0) then
              write(t80,'(''## datblock '',a)') DatBlockName(KDatB)
              write(m41,FormA) t80(:idel(t80))
            endif
            if(KProfPwd(KPh,KDatB).eq.IdPwdProfGauss.or.
     1         KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
              j=IGaussPwd+IZdvih
              if(klic.le.0) then
                call SkipComments(m41)
                if(ErrFlag.ne.0.and.kk.eq.1) then
                  go to 9000
                else
                  ErrFlag=0
                endif
              else
                if(isTOF) then
                write(m41,'(''# Gaussian parameters - sig0,sig1,'',
     1                        ''sig2'')')
                else
                  if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                    t80='CSizeG,CSizeGA,StrainG,StrainGA'
                  else
                    t80='U,V,W,P'
                  endif
                  write(m41,'(''# Gaussian parameters - '',a)')
     1              t80(:idel(t80))
                endif
                if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  GaussPwd(1,KPh,KDatB)=abs(GaussPwd(1,KPh,KDatB))
                endif
              endif
              if(isTOF) then
                i=3
              else
                i=4
              endif
              if(kk.eq.1) then
                call iost(m41,GaussPwd(1,KPh,KDatB),kiPwd(j),i,klic,
     1                    tisk)
                if(klic.le.0) then
                  if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                    GaussPwd(1,KPh,KDatB)=abs(GaussPwd(1,KPh,KDatB))
                  endif
                  if(isTOF.and.TOFKey(KDatB).eq.0) then
                    do i=1,3
                      GaussPwd(i,KPh,KDatB)=100.*GaussPwd(i,KPh,KDatB)
                    enddo
                  endif
                endif
                if(ErrFlag.ne.0) go to 9000
              else
                call iosts(m41,GaussPwdS(1,KPh,KDatB),i,klic)
              endif
            endif
            if(KProfPwd(KPh,KDatB).eq.IdPwdProfLorentz.or.
     1         KProfPwd(KPh,KDatB).eq.IdPwdProfModLorentz.or.
     2         KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
              j=ILorentzPwd+IZdvih
              if(klic.le.0) then
                call SkipComments(m41)
                if(ErrFlag.ne.0.and.kk.eq.1) then
                  go to 9000
                else
                  ErrFlag=0
                endif
              else
                if(isTOF) then
                  Radka='# Lorentzian parameters - gam0,gam1,gam2'
                  if(KStrain(KPh,KDatB).eq.IdPwdStrainAxial)
     1              Radka=Radka(:idel(Radka))//',gam1e,gam2e'
                else
                  if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                    t80='CSizeL,CSizeLA,StrainL,StrainLA'
                  else
                    t80='LX,LXe,LY,LYe'
                  endif
                  Radka='# Lorentzian parameters - '//t80(:idel(t80))
                endif
                if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor.and.
     1             KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt)
     2             Radka=Radka//',zeta'
                write(m41,FormA) Radka(:idel(Radka))
                if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  LorentzPwd(1,KPh,KDatB)=abs(LorentzPwd(1,KPh,KDatB))
                endif
              endif
              if(isTOF) then
                if(KStrain(KPh,KDatB).eq.IdPwdStrainAxial) then
                  n=5
                else
                  n=3
                endif
              else
                n=4
              endif
              m=n
              if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor) m=n+1
              if(Klic.eq.1) then
                if(kk.eq.1) then
                  call CopyVek (LorentzPwd(1,KPh,KDatB),xp,n)
                  call CopyVekI(KiPwd(j),kip,n)
                  if(m.gt.n) then
                    xp(m)=ZetaPwd(KPh,KDatB)
                    kip(m)=KiPwd(IZetaPwd+IZdvih)
                  endif
                else
                  call CopyVek (LorentzPwdS(1,KPh,KDatB),xp,n)
                  if(m.gt.n) xp(m)=ZetaPwdS(KPh,KDatB)
                endif
              endif
              if(kk.eq.1) then
                call iost(m41,xp,kip,m,klic,tisk)
                if(ErrFlag.ne.0) go to 9000
                if(Klic.eq.0) then
                  call CopyVek (xp,LorentzPwd(1,KPh,KDatB),n)
                  call CopyVekI(kip,KiPwd(j),n)
                  if(m.gt.n) then
                    ZetaPwd(KPh,KDatB)=xp(m)
                    KiPwd(IZetaPwd+IZdvih)=kip(m)
                  endif
                  if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                    LorentzPwd(1,KPh,KDatB)=abs(LorentzPwd(1,KPh,KDatB))
                  endif
                  if(isTOF.and.TOFKey(KDatB).eq.0) then
                    do i=1,3
                      LorentzPwd(i,KPh,KDatB)=
     1                  10.*LorentzPwd(i,KPh,KDatB)
                    enddo
                  endif
                endif
              else
                call iosts(m41,xp,m,klic)
                if(ErrFlag.ne.0) go to 9000
                if(Klic.eq.0) then
                  call CopyVek (xp,LorentzPwdS(1,KPh,KDatB),n)
                  if(m.gt.n) then
                    ZetaPwdS(KPh,KDatB)=xp(m)
                  endif
                  if(KAsym(KDatB).eq.IdPwdAsymFundamental) then
                    LorentzPwdS(1,KPh,KDatB)=LorentzPwdS(1,KPh,KDatB)
                  endif
                  if(isTOF.and.TOFKey(KDatB).eq.0) then
                    do i=1,3
                      LorentzPwdS(i,KPh,KDatB)=
     1                  10.*LorentzPwdS(i,KPh,KDatB)
                    enddo
                  endif
                endif
              endif
            endif
            if(KStrain(KPh,KDatB).eq.IdPwdStrainTensor) then
              j=IStPwd+IZdvih
              if(klic.le.0) then
                call SkipComments(m41)
                if(ErrFlag.ne.0.and.kk.eq.1) then
                  go to 9000
                else
                  ErrFlag=0
                endif
              else
                write(m41,'(''# Strain parameters'')')
              endif
              if(NDimI(KPh).eq.1.or.NQMag(KPh).gt.0) then
                 n=35
              else
                 n=15+NDimI(KPh)
              endif
              if(kk.eq.1) then
                call iost(m41,StPwd(1,KPh,KDatB),kiPwd(j),n,klic,tisk)
                if(ErrFlag.ne.0) go to 9000
              else
                call iosts(m41,StPwdS(1,KPh,KDatB),n,klic)
              endif
            endif
            if(NBroadHKLPwd(KPh,KDatB).gt.0) then
              j=IBroadHKLPwd+IZdvih
              if(klic.le.0) then
                call SkipComments(m41)
                if(ErrFlag.ne.0.and.kk.eq.1) then
                  go to 9000
                else
                  ErrFlag=0
                endif
              else
                write(m41,'(''# Broadening HKL'')')
              endif
              if(kk.eq.1) then
                call iost(m41,BroadHKLPwd(1,KPh,KDatB),kiPwd(j),
     1                    NBroadHKLPwd(KPh,KDatB),klic,tisk)
                if(ErrFlag.ne.0) go to 9000
              else
                call iosts(m41,BroadHKLPwdS(1,KPh,KDatB),
     1                     NBroadHKLPwd(KPh,KDatB),klic)
              endif
            endif
            if(KPref(KPh,KDatB).ne.IdPwdPrefNone) then
              j=IPrefPwd+IZdvih
              if(klic.le.0) then
                call SkipComments(m41)
                if(ErrFlag.ne.0.and.kk.eq.1) then
                  go to 9000
                else
                  ErrFlag=0
                endif
              else
                write(m41,'(''# Preferred orientation'')')
              endif
              if(kk.eq.1) then
                call iost(m41,PrefPwd(1,KPh,KDatB),kiPwd(j),2,klic,tisk)
                if(ErrFlag.ne.0) go to 9000
              else
                call iosts(m41,PrefPwdS(1,KPh,KDatB),2,klic)
              endif
            endif
2700        IZdvih=IZdvih+NParProfPwd
          enddo
          IZdvihP=IZdvihP+NParCellProfPwd
        enddo
        if(Klic.eq.1.and.kk.eq.2) write(m41,'(79(''-''))')
      enddo
      call iom50(0,0,PureFileName(:idel(PureFileName))//'.m50')
      if(ErrFlag.ne.0) go to 9900
3000  if(Klic.le.0) then
        do KDatB=1,NDatBlock
          call PwdSetTOF(KDatB)
          if(DataType(KDatB).eq.-2.and.
     1       Radiation(KDatB).eq.NeutronRadiation) TOFKey(KDatB)=1
          do KPh=1,NPhase
            IZdvihCell=(KPh-1)*NParCellProfPwd
            IZdvihProf=IZdvihCell+(KDatB-1)*NParProfPwd
            if(KPartBroad(KPh,KDatB).lt.0) then
              KPartBroad(KPh,KDatB)=0
              if(.not.isTOF) then
                j=ILorentzPwd+IZdvihProf+1
                if(abs(LorentzPwd(2,KPh,KDatB)).gt.0.or.
     1            kiPwd(j).ne.0) KPartBroad(KPh,KDatB)=1
                if(KProfPwd(KPh,KDatB).le.0.and.
     1             KAsym(KDatB).eq.IdPwdAsymFundamental) then
                  j=IGaussPwd+IZdvihProf+1
                  if(abs(GaussPwd(2,KPh,KDatB)).gt.0.or.
     1               kiPwd(j).ne.0) KPartBroad(KPh,KDatB)=1
                endif
              endif
            endif
          enddo
        enddo
      else
        do i=1,NPhase
          call CopyVek(CellPwd(1,i),CellPar(1,1,i),6)
          call CopyVek(CellPwds(1,i),CellParSU(1,1,i),6)
          if(NQMag(i).gt.0) then
            call CopyVek(QuPwd(1,1,i),QMag(1,1,i),3)
          else
            call CopyVek(QuPwd(1,1,i),Qu(1,1,1,i),3*(NDim(i)-3))
          endif
        enddo
        call iom50(1,0,PureFileName(:idel(PureFileName))//'.m50')
        if(ErrFlag.ne.0) go to 9900
        call CloseIfOpened(m50)
        if(ErrFlag.ne.0) go to 9900
      endif
      if(Klic.eq.0) call PwdResetVoigt
      go to 9999
9000  call FeReadError(m41)
      go to 9900
9010  t80='Unexpected end of file'
      call FeChybne(-1.,-1.,'wrong record on M41 file',t80,SeriousError)
      go to 9900
9900  ErrFlag=1
9999  call CloseIfOpened(m41)
      KPhase=KPhaseIn
      return
100   format(f12.6,5(1x,a10,f12.6))
101   format(i5,7(1x,a10,i5))
102   format(3f10.5)
103   format(3i5)
104   format(f15.0)
110   format(5(a10,f12.6,1x))
111   format(5(a10,i5,1x))
112   format(a10,3f8.3)
      end
