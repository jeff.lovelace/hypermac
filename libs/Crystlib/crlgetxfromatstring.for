      subroutine CrlGetXFromAtString(LocSystSt,imol,XAt,ErrSt,ich)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) LocSystSt,ErrSt
      character*80 Veta,p80,t80
      character*1 t1
      logical Jediny
      dimension XAt(*),xx(3),py(3)
      ich=0
      ErrSt=' '
      Veta=LocSystSt
      call mala(Veta)
      l=0
      call StToReal(Veta,l,XAt,1,.false.,ich)
      if(ich.eq.0) then
        call StToReal(Veta,l,XAt(2),2,.false.,ich)
        if(ich.eq.0) then
          write(Veta,'(3f9.6)')(XAt(i),i=1,3)
          ich=-1
          LocSystSt=Veta
          go to 9999
        else
          if(ich.eq.1) then
            ErrSt='"'//LocSystSt(:idel(LocSystSt))//
     1            '" contains a non-numerical character.'
          else
            ErrSt='"'//LocSystSt(:idel(LocSystSt))//
     1            '" does not contain the requested number of items.'
          endif
          go to 9000
        endif
      else
        ich=0
      endif
      l=0
      call kus(Veta,l,p80)
      call UprAt(p80)
      Jediny=l.ge.len(Veta)
      if(imol.gt.0) then
        ipc=NAtMolFr(1,1)
        do k=1,imol-1
          ipc=ipc+iam(k)
        enddo
        k=ktat(Atom(ipc),iam(imol),p80)
        if(k.le.0) then
          go to 8000
        else
          k=k+ipc-1
          call CopyVek(x(1,k),XAt,3)
        endif
      else
        call atsym(p80,k,XAt,xx,xx,isym,ich)
        if(ich.eq.2) then
          go to 8000
        else if(ich.eq.3) then
          go to 8100
        endif
        ich=0
      endif
      t80=p80
      if(.not.Jediny) then
        call kus(Veta,l,Cislo)
        if(Cislo.ne.'^'.and.Cislo.ne.'-') then
          ErrSt='incorrect separator "'//Cislo(:idel(Cislo))//
     1          '" in string "'//LocSystSt(:idel(LocSystSt))
          go to 9000
        endif
        t1=Cislo
        call kus(Veta,l,p80)
        call UprAt(p80)
        if(imol.gt.0) then
          k=ktat(atom(ipc),iam(imol)/KPoint(imol),p80)
          if(k.le.0) then
            go to 8000
          else
            k=k+ipc-1
            call CopyVek(x(1,k),py,3)
          endif
        else
          call atsym(p80,k,py,xx,xx,isym,ich)
          if(ich.eq.2) then
            go to 8000
          else if(ich.eq.3) then
            go to 8100
          endif
          ich=0
        endif
        LocSystSt=t80(:idel(t80))//' '//t1//' '//p80(:idel(p80))
        if(t1.eq.'^') then
          do k=1,3
            XAt(k)=(XAt(k)+py(k))*.5
          enddo
        else
          do k=1,3
            XAt(k)=XAt(k)-py(k)
          enddo
          ich=-2
        endif
      else
        LocSystSt=t80(:idel(t80))
      endif
      go to 9999
8000  ErrSt='atom "'//p80(:idel(p80))
      if(.not.Jediny)
     1  ErrSt=ErrSt(:idel(ErrSt))//'" in string "'//
     2        LocSystSt(:idel(LocSystSt))
        ErrSt=ErrSt(:idel(ErrSt))//'" does not exist.'
      go to 9000
8100  ErrSt='error in the symmetry part of "'//p80(:idel(p80))
      if(.not.Jediny)
     1  ErrSt=ErrSt(:idel(ErrSt))//'" in string "'//
     2        LocSystSt(:idel(LocSystSt))
        ErrSt=ErrSt(:idel(ErrSt))//'".'
9000  ich=1
9999  return
      end
