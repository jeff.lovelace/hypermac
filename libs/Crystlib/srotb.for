      subroutine srotb(a,b,c)
      include 'fepc.cmn'
      dimension a(3,3),b(3,3),c(36)
      do i1=1,6
        ind=i1-6
        call indext(i1,i,l)
        do i2=1,6
          ind=ind+6
          call indext(i2,j,m)
          p=a(i,j)*b(l,m)
          if(j.ne.m) p=p+a(i,m)*b(l,j)
          c(ind)=p
        enddo
      enddo
      return
      end
