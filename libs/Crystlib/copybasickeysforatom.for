      subroutine CopyBasicKeysForAtom(n,m)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      Atom(m)=Atom(n)
      isf(m)=isf(n)
      itf(m)=itf(n)
      ifr(m)=ifr(n)
      iswa(m)=iswa(n)
      kswa(m)=kswa(n)
      kmol(m)=kmol(n)
      MagPar(m)=MagPar(n)
      lasmax(m)=lasmax(n)
      TypeModFun(m)=TypeModFun(n)
      KUsePolar(m)=KUsePolar(n)
      AtSiteMult(m)=AtSiteMult(n)
      xfr(m)=xfr(n)
      sxfr(m)=sxfr(n)
      if(MagPar(n).gt.0) KUsePolar(m)=KUsePolar(n)
      call CopyVekI(isa(1,n),isa(1,m),MaxNSymm)
      call CopyVekI(KFA(1,n),KFA(1,m),7)
      call CopyVekI(KModA(1,n),KModA(1,m),7)
      call CopyVekI(KModAO(1,n),KModAO(1,m),7)
      ai(m)=ai(n)
      sai(m)=sai(n)
      a0(m)=a0(n)
      sa0(m)=sa0(n)
      call CopyVek( x(1,n), x(1,m),3)
      call CopyVek(sx(1,n),sx(1,m),3)
      call CopyVek( beta(1,n), beta(1,m),6)
      call CopyVek(sbeta(1,n),sbeta(1,m),6)
      if(NMolec.gt.0) call CopyVek(durdr(1,n),durdr(1,m),9)
      call CopyVekI(KiA(1,n),KiA(1,m),mxda)
      if(NDimI(KPhase).gt.0) then
        call CopyVek(qcnt(1,n),qcnt(1,m),NDimI(KPhase))
         phf(m)= phf(n)
        sphf(m)=sphf(n)
        OrthoX40(m)=OrthoX40(n)
        OrthoDelta(m)=OrthoDelta(n)
        OrthoEps(m)=OrthoEps(n)
      endif
      return
      end
