      subroutine BetaU
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical BetaIn
      entry BetaToU(Klic)
      BetaIn=.true.
      go to 1000
      entry UToBeta(Klic)
      BetaIn=.false.
1000  KPhaseIn=KPhase
      do i=1,NAtAll
        if(i.gt.NAtCalc.and.i.lt.NAtMolFr(1,1)) cycle
        if(Klic.eq.0) then
         KPhase=kswa(i)
        else
          if(kswa(i).ne.Klic) cycle
        endif
        if(lite(KPhase).ne.0) cycle
        isw=iswa(i)
        if(itf(i).ge.2) then
          do j=1,6
            if(BetaIn) then
              pom=1./urcp(j,isw,KPhase)
            else
              pom=urcp(j,isw,KPhase)
            endif
            Beta(j,i)=Beta(j,i)*pom
            if(NDim(KPhase).gt.4) then
              do k=1,KModA(3,i)
                bx(j,k,i)=bx(j,k,i)*pom
                by(j,k,i)=by(j,k,i)*pom
              enddo
            endif
          enddo
        else
          if(BetaIn) then
            pom=1./episq
          else
            pom=episq
          endif
          beta(1,i)=beta(1,i)*pom
        endif
      enddo
      if(Klic.eq.0) KPhase=KPhaseIn
      return
      end
