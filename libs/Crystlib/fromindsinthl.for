      subroutine FromIndSinthl(ih,h,sinthl,sinthlq,isw,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ih(*),h(3)
      if(klic.eq.0) then
        do i=1,3
          h(i)=ih(i)
          do j=1,NDimI(KPhase)
            h(i)=h(i)+qu(i,j,isw,KPhase)*float(ih(j+3))
          enddo
        enddo
      endif
      sinthlq=h(1)**2*prcp(1,isw,KPhase)+h(2)**2*prcp(2,isw,KPhase)+
     1        h(3)**2*prcp(3,isw,KPhase)+
     2        2.*(h(1)*h(2)*prcp(4,isw,KPhase)+
     3            h(1)*h(3)*prcp(5,isw,KPhase)+
     4            h(2)*h(3)*prcp(6,isw,KPhase))
      sinthl=sqrt(sinthlq)
      return
      end
