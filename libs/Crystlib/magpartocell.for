      subroutine MagParToCell(n)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension VecN(3)
      klic=0
      go to 1100
      entry MagParToBohrMag(n)
      klic=1
1100  if(n.eq.0) then
        i1=1
        i2=NAtCalc
      else
        i1=n
        i2=n
      endif
      do i=i1,i2
        KPh=kswa(i)
        if(MagneticType(KPh).le.0) cycle
        if(MagPar(i).le.0.or.ktat(NamePolar,NPolar,Atom(i)).gt.0) cycle
        isw=iswa(i)
        do k=1,3
          if(klic.eq.0) then
            VecN(k)=1./CellPar(k,isw,KPh)
          else
            VecN(k)=CellPar(k,isw,KPh)
          endif
        enddo
        do j=1,MagPar(i)
          if(j.eq.1) then
            do k=1,3
              sm0(k,i)=sm0(k,i)*VecN(k)
            enddo
          else
            do k=1,3
              smx(k,j-1,i)=smx(k,j-1,i)*VecN(k)
              smy(k,j-1,i)=smy(k,j-1,i)*VecN(k)
            enddo
          endif
        enddo
      enddo
      return
      end
