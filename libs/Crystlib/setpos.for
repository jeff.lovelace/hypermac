      subroutine SetPos(im,ip,ji,j1,j2,isw,tisk)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension pa(9),pb(9),pc(9),pad(9),pbd(9),pcd(9),px(36),xp(3),
     1          pxx(9),rotgi(9),qcntp(3),betap(6),sbetap(6),spa(3),
     2          spb(3),spc(3),sxp(3),KModP(7),xpp(3)
      equivalence (px(10),pxx(1)),(px(19),pa(1)),(px(28),pb(1))
      character*1 znak
      integer tisk
      znak=char(ichar('a')+ip-1)
      klic=0
      go to 500
      entry CrLSetRotMat(ji)
      klic=1
500   NAtCalcIn=NAtCalc
      fi=ToRad*euler(1,ji)
      chi=ToRad*euler(2,ji)
      psi=ToRad*euler(3,ji)
      snf=sin(fi)
      csf=cos(fi)
      snc=sin(chi)
      csc=cos(chi)
      snp=sin(psi)
      csp=cos(psi)
      pc(1)=csf
      pc(2)=snf
      pc(3)=0.
      pc(4)=-snf
      pc(5)=csf
      pc(6)=0.
      pc(7)=0.
      pc(8)=0.
      pc(9)=1.
      pcd(1)=-snf
      pcd(2)=csf
      pcd(3)=0.
      pcd(4)=-csf
      pcd(5)=-snf
      pcd(6)=0.
      pcd(7)=0.
      pcd(8)=0.
      pcd(9)=0.
      if(irot(KPhase).eq.0) then
        pa(1)=csp
        pa(2)=snp
        pa(3)=0.
        pa(4)=-snp
        pa(5)=csp
        pa(6)=0.
        pa(7)=0.
        pa(8)=0.
        pa(9)=1.
        pad(1)=-snp
        pad(2)=csp
        pad(3)=0.
        pad(4)=-csp
        pad(5)=-snp
        pad(6)=0.
        pad(7)=0.
        pad(8)=0.
        pad(9)=0.
        pb(1)=1.
        pb(2)=0.
        pb(3)=0.
        pb(4)=0.
        pb(5)=csc
        pb(6)=snc
        pb(7)=0.
        pb(8)=-snc
        pb(9)=csc
        pbd(1)=0.
        pbd(2)=0.
        pbd(3)=0.
        pbd(4)=0.
        pbd(5)=-snc
        pbd(6)=csc
        pbd(7)=0.
        pbd(8)=-csc
        pbd(9)=-snc
      else
        pa(1)=1.
        pa(2)=0.
        pa(3)=0.
        pa(4)=0.
        pa(5)=csp
        pa(6)=snp
        pa(7)=0.
        pa(8)=-snp
        pa(9)=csp
        pad(1)=0.
        pad(2)=0.
        pad(3)=0.
        pad(4)=0.
        pad(5)=-snp
        pad(6)=csp
        pad(7)=0.
        pad(8)=-csp
        pad(9)=-snp
        pb(1)=csc
        pb(2)=0.
        pb(3)=-snc
        pb(4)=0.
        pb(5)=1.
        pb(6)=0.
        pb(7)=snc
        pb(8)=0.
        pb(9)=csc
        pbd(1)=-snc
        pbd(2)=0.
        pbd(3)=-csc
        pbd(4)=0.
        pbd(5)=0.
        pbd(6)=0.
        pbd(7)=csc
        pbd(8)=0.
        pbd(9)=-snc
      endif
      call multm5(RotMol(1,ji),TriMol(1,ji),pc,pb,pa,TrMol(1,ji),
     1            RotSign(ji))
      call matinv(RotMol(1,ji),RotiMol(1,ji),pom,3)
      if(klic.eq.1) go to 9999
      call multm5(drotf(1,ji),TriMol(1,ji),pcd,pb,pa,TrMol(1,ji),
     1            RotSign(ji))
      call multm5(drotc(1,ji),TriMol(1,ji),pc,pbd,pa,TrMol(1,ji),
     1            RotSign(ji))
      call multm5(drotp(1,ji),TriMol(1,ji),pc,pb,pad,TrMol(1,ji),
     1            RotSign(ji))
      do i=1,9
        drotf(i,ji)=torad*drotf(i,ji)
        drotc(i,ji)=torad*drotc(i,ji)
        drotp(i,ji)=torad*drotp(i,ji)
      enddo
      call srotb(RotMol(1,ji),RotMol(1,ji),rotb(1,ji))
      call srotb(RotMol(1,ji),drotf(1,ji),drotbf(1,ji))
      call srotb(drotf(1,ji),RotMol(1,ji),px)
      call AddVek(drotbf(1,ji),px,drotbf(1,ji),36)
      call srotb(RotMol(1,ji),drotc(1,ji),drotbc(1,ji))
      call srotb(drotc(1,ji),RotMol(1,ji),px)
      call AddVek(drotbc(1,ji),px,drotbc(1,ji),36)
      call srotb(RotMol(1,ji),drotp(1,ji),drotbp(1,ji))
      call srotb(drotp(1,ji),RotMol(1,ji),px)
      call AddVek(drotbp(1,ji),px,drotbp(1,ji),36)
      do i=1,36
        drotbf(i,ji)=torad*drotbf(i,ji)
        drotbc(i,ji)=torad*drotbc(i,ji)
        drotbp(i,ji)=torad*drotbp(i,ji)
      enddo
      call multm(RotMol(1,ji),MetTensI(1,isw,KPhase),rotgi,3,3,3)
      do j=1,KModM(2,ji)
        pa(1)=0.
        pa(2)= urx(3,j,ji)*CellVol(isw,KPhase)
        pa(3)=-urx(2,j,ji)*CellVol(isw,KPhase)
        call multm(MetTensI(1,isw,KPhase),pa,durdx(1,j,ji),3,3,1)
        pa(2)= ury(3,j,ji)*CellVol(isw,KPhase)
        pa(3)=-ury(2,j,ji)*CellVol(isw,KPhase)
        call multm(MetTensI(1,isw,KPhase),pa,durdx(4,j,ji),3,3,1)
        pa(1)=-urx(3,j,ji)*CellVol(isw,KPhase)
        pa(2)=0.
        pa(3)= urx(1,j,ji)*CellVol(isw,KPhase)
        call multm(MetTensI(1,isw,KPhase),pa,durdx(7,j,ji),3,3,1)
        pa(1)=-ury(3,j,ji)*CellVol(isw,KPhase)
        pa(3)= ury(1,j,ji)*CellVol(isw,KPhase)
        call multm(MetTensI(1,isw,KPhase),pa,durdx(10,j,ji),3,3,1)
        pa(1)= urx(2,j,ji)*CellVol(isw,KPhase)
        pa(2)=-urx(1,j,ji)*CellVol(isw,KPhase)
        pa(3)=0.
        call multm(MetTensI(1,isw,KPhase),pa,durdx(13,j,ji),3,3,1)
        pa(1)= ury(2,j,ji)*CellVol(isw,KPhase)
        pa(2)=-ury(1,j,ji)*CellVol(isw,KPhase)
        call multm(MetTensI(1,isw,KPhase),pa,durdx(16,j,ji),3,3,1)
      enddo
      call AddVek(xm(1,im),trans(1,ji),xp,3)
      call qbyx(xp,qcntp,isw)
      i=NAtCalcIn+(j2-j1+1)*(ip-1)
      do j=j1,j2
        i=i+1
        call CopyBasicKeysForAtom(j,i)
        TypeModFun(i)=TypeModFunMol(ji)
        if(TypeModFun(i).eq.1) then
          OrthoX40(i)=OrthoX40Mol(ji)
          OrthoDelta(i)=OrthoDeltaMol(ji)
          OrthoEps(i)=OrthoEpsMol(ji)
        endif
        call SetRealArrayTo(durdr(1,i),9,0.)
        if(NDimI(KPhase).gt.0) then
          call CopyVek(qcntp,qcnt(1,i),NDimI(KPhase))
          if(ip.eq.1) call CopyVek(qcntp,qcnt(1,j),NDimI(KPhase))
        endif
        atom(i)=atom(j)(1:min(idel(atom(j)),7))//znak
        iswa(i)=isw
        kmol(i)=ji
        ai(i)=ai(j)*aimol(ji)
        sai(i)=sqrt((sai(j)*aimol(ji))**2+(ai(j)*saimol(ji))**2)
        pomsx=0.
        do k=1,3
          pomsx=pomsx+sx(k,j)
        enddo
        do k=1,3
          xp(k)= x(k,j)-xm(k,im)
          if(pomsx.ge.0) then
            sxp(k)=sx(k,j)
          else
            sxp(k)=0.
          endif
        enddo
        call multm(RotMol(1,ji),xp,x(1,i),3,3,1)
        if(pomsx.ge.0.) then
          call multmq(RotMol(1,ji),sx(1,j),sx(1,i),3,3,1)
          call multmq(drotf(1,ji),xp,xpp,3,3,1)
          do k=1,3
            sx(k,i)=sx(k,i)**2+(xpp(k)*seuler(1,ji))**2
          enddo
          call multmq(drotc(1,ji),xp,xpp,3,3,1)
          do k=1,3
            sx(k,i)=sx(k,i)+(xpp(k)*seuler(2,ji))**2
          enddo
          call multmq(drotp(1,ji),xp,xpp,3,3,1)
          do k=1,3
            sx(k,i)=sqrt(sx(k,i)+(xpp(k)*seuler(3,ji))**2)
          enddo
        endif
        do k=1,3
          x(k,i)=x(k,i)+trans(k,ji)+xm(k,im)
          if(pomsx.ge.0.) then
            sx(k,i)=sqrt(sx(k,i)**2+strans(k,ji)**2)
          else
            sx(k,i)=-.1
          endif
        enddo
        px(1)=0.
        px(5)=0.
        px(9)=0.
        px(2)=-xp(3)
        px(4)=-px(2)
        px(3)= xp(2)
        px(7)=-px(3)
        px(6)=-xp(1)
        px(8)=-px(6)
        call multm(MetTensI(1,isw,KPhase),px,pxx,3,3,3)
        do k=1,9
          px(k)=CellVol(isw,KPhase)*pxx(k)
        enddo
        jmmxa=j-NAtMolFr(1,1)+1
        call srotb(px,px,tztl(1,jmmxa))
        call srots(px,px,tzts(1,jmmxa))
        if(itf(j).eq.0) then
          itf(i)=2
          call AddVek(beta(1,j),tt(1,ji),betap,6)
          call CopyVek(stt(1,ji),sbetap,6)
          call cultm (tztl(1,jmmxa), tl(1,ji), betap,6,6,1)
          call cultmq(tztl(1,jmmxa),stl(1,ji),sbetap,6,6,1)
          call cultm (tzts(1,jmmxa), ts(1,ji), betap,6,9,1)
          call cultmq(tzts(1,jmmxa),sts(1,ji),sbetap,6,9,1)
        else if(itf(j).eq.1) then
          if(KModM(3,ji).le.0) then
            itf(i)=1
            call SetRealArrayTo( beta(1,i),6,0.)
            call SetRealArrayTo(sbeta(1,i),6,0.)
             beta(1,i)= beta(1,j)
            sbeta(1,i)=sbeta(1,j)
          else
            itf(i)=2
             pom= beta(1,j)
            spom=sbeta(1,j)
            do m=1,6
               betap(m)= pom*prcp(m,isw,KPhase)
              sbetap(m)=spom*prcp(m,isw,KPhase)
            enddo
          endif
        else
          itf(i)=itf(j)
          call CopyVek( beta(1,j), betap,6)
          call CopyVek(sbeta(1,j),sbetap,6)
        endif
        if(itf(i).ne.1) then
          call multm (rotb(1,ji), betap, beta(1,i),6,6,1)
          call multmq(rotb(1,ji),sbetap,sbeta(1,i),6,6,1)
        endif
        call CopyVekI(KModA(1,j),KModP,7)
        call CopyVekI(KModAO(1,j),KModAO(1,i),7)
        do k=1,3
          KFA(k,i)=max(KFA(k,i),KFM(k,ji))
          KModP(k)=max(KModP(k),KModM(k,ji))
        enddo
        call ReallocateAtomParams(NAtAll,itfmax,IFrMax,KModP,0)
        call SetIntArrayTo(KiA(1,i),mxda,0)
        NAtCalc=i
        call CopyVekI(KModP,KModA(1,i),7)
        if(KModA(1,i).gt.0) then
          a0(i)=a0(j)
         sa0(i)=sa0(j)
        else
          a0(i)=1.
         sa0(i)=0.
        endif
        do k=1,KModA(1,i)
          if(k.le.KModA(1,j)) then
            ax(k,i)=ax(k,j)
            ay(k,i)=ay(k,j)
            sax(k,i)=sax(k,j)
            say(k,i)=say(k,j)
          else
            ax(k,i)=0.
            ay(k,i)=0.
            sax(k,i)=0.
            say(k,i)=0.
          endif
        enddo
        if(KModM(1,ji).gt.0) then
          a0(i)=a0(i)*a0m(ji)
          call AddVek(ax(1,i),axm(1,ji),ax(1,i),KModM(1,ji))
          call AddVek(ay(1,i),aym(1,ji),ay(1,i),KModM(1,ji))
        endif
        do k=1,KModA(2,i)
          if(k.le.KModA(2,j)) then
            call multm (RotMol(1,ji), ux(1,k,j), ux(1,k,i),3,3,1)
            call multm (RotMol(1,ji), uy(1,k,j), uy(1,k,i),3,3,1)
            call multmq(RotMol(1,ji),sux(1,k,j),sux(1,k,i),3,3,1)
            call multmq(RotMol(1,ji),suy(1,k,j),suy(1,k,i),3,3,1)
          else
            call SetRealArrayTo( ux(1,k,i),3,0.)
            call SetRealArrayTo( uy(1,k,i),3,0.)
            call SetRealArrayTo(sux(1,k,i),3,0.)
            call SetRealArrayTo(suy(1,k,i),3,0.)
          endif
        enddo
        if(KModM(2,ji).gt.0) then
          pa(1)=0.
          pa(2)=-xp(3)*CellVol(isw,KPhase)
          pa(3)= xp(2)*CellVol(isw,KPhase)
          call multm(MetTensI(1,isw,KPhase),pa,durdr(1,i),3,3,1)
          pa(1)= xp(3)*CellVol(isw,KPhase)
          pa(2)=0.
          pa(3)=-xp(1)*CellVol(isw,KPhase)
          call multm(MetTensI(1,isw,KPhase),pa,durdr(4,i),3,3,1)
          pa(1)=-xp(2)*CellVol(isw,KPhase)
          pa(2)= xp(1)*CellVol(isw,KPhase)
          pa(3)=0.
          call multm(MetTensI(1,isw,KPhase),pa,durdr(7,i),3,3,1)
          do k=1,KModM(2,ji)
            call CopyVek(utx(1,k,ji),pb,3)
            call CopyVek(uty(1,k,ji),pc,3)
            call CopyVek(sutx(1,k,ji),spb,3)
            call CopyVek(suty(1,k,ji),spc,3)
            pa(1)=-xp(2)*urx(3,k,ji)+xp(3)*urx(2,k,ji)
            pa(2)=-xp(3)*urx(1,k,ji)+xp(1)*urx(3,k,ji)
            pa(3)=-xp(1)*urx(2,k,ji)+xp(2)*urx(1,k,ji)
            spa(1)=( xp(2)*surx(3,k,ji))**2+( xp(3)*surx(2,k,ji))**2+
     1             (sxp(2)* urx(3,k,ji))**2+(sxp(3)* urx(2,k,ji))**2
            spa(2)=( xp(3)*surx(1,k,ji))**2+( xp(1)*surx(3,k,ji))**2+
     1             (sxp(3)* urx(1,k,ji))**2+(sxp(1)* urx(3,k,ji))**2
            spa(3)=( xp(1)*surx(2,k,ji))**2+( xp(2)*surx(1,k,ji))**2+
     1             (sxp(1)* urx(2,k,ji))**2+(sxp(2)* urx(1,k,ji))**2
            do l=1,3
              pa(l)=pa(l)*CellVol(isw,KPhase)
              spa(l)=sqrt(spa(l))*CellVol(isw,KPhase)
            enddo
            call cultm (MetTensI(1,isw,KPhase), pa, pb,3,3,1)
            call cultmq(MetTensI(1,isw,KPhase),spa,spb,3,3,1)
            pa(1)=-xp(2)*ury(3,k,ji)+xp(3)*ury(2,k,ji)
            pa(2)=-xp(3)*ury(1,k,ji)+xp(1)*ury(3,k,ji)
            pa(3)=-xp(1)*ury(2,k,ji)+xp(2)*ury(1,k,ji)
            spa(1)=( xp(2)*sury(3,k,ji))**2+( xp(3)*sury(2,k,ji))**2+
     1             (sxp(2)* ury(3,k,ji))**2+(sxp(3)* ury(2,k,ji))**2
            spa(2)=( xp(3)*sury(1,k,ji))**2+( xp(1)*sury(3,k,ji))**2+
     1             (sxp(3)* ury(1,k,ji))**2+(sxp(1)* ury(3,k,ji))**2
            spa(3)=( xp(1)*sury(2,k,ji))**2+( xp(2)*sury(1,k,ji))**2+
     1             (sxp(1)* ury(2,k,ji))**2+(sxp(2)* ury(1,k,ji))**2
            do l=1,3
              pa(l)=pa(l)*CellVol(isw,KPhase)
              spa(l)=sqrt(spa(l))*CellVol(isw,KPhase)
            enddo
            call cultm (MetTensI(1,isw,KPhase), pa, pc,3,3,1)
            call cultmq(MetTensI(1,isw,KPhase),spa,spc,3,3,1)
            call cultm (RotMol(1,ji), pb, ux(1,k,i),3,3,1)
            call cultmq(RotMol(1,ji),spb,sux(1,k,i),3,3,1)
            if(k.lt.KModM(2,ji).or.KFM(2,ji).eq.0) then
              call cultm (RotMol(1,ji), pc, uy(1,k,i),3,3,1)
              call cultmq(RotMol(1,ji),spc,suy(1,k,i),3,3,1)
            else
              call CopyVek( uty(1,k,ji), uy(1,k,i),3)
              call CopyVek(suty(1,k,ji),suy(1,k,i),3)
            endif
          enddo
        endif
        do k=1,KModA(3,i)
          if(k.le.KModA(3,j)) then
            call multm (rotb(1,ji), bx(1,k,j), bx(1,k,i),6,6,1)
            call multm (rotb(1,ji), by(1,k,j), by(1,k,i),6,6,1)
            call multmq(rotb(1,ji),sbx(1,k,j),sbx(1,k,i),6,6,1)
            call multmq(rotb(1,ji),sby(1,k,j),sby(1,k,i),6,6,1)
          else
            call SetRealArrayTo( bx(1,k,i),6,0.)
            call SetRealArrayTo( by(1,k,i),6,0.)
            call SetRealArrayTo(sbx(1,k,i),6,0.)
            call SetRealArrayTo(sby(1,k,i),6,0.)
          endif
        enddo
        do k=1,KModM(3,ji)
          do l=1,6
            pb(l)=ttx(l,k,ji)
            pc(l)=tty(l,k,ji)
          enddo
          call cultm(tztl(1,jmmxa),tlx(1,k,ji),pb,6,6,1)
          call cultm(tztl(1,jmmxa),tly(1,k,ji),pc,6,6,1)
          call cultm(tzts(1,jmmxa),tsx(1,k,ji),pb,6,9,1)
          call cultm(tzts(1,jmmxa),tsy(1,k,ji),pc,6,9,1)
          call multm(rotb(1,ji),pb,bx(1,k,i),6,6,1)
          call multm(rotb(1,ji),pc,by(1,k,i),6,6,1)
        enddo
        if(KModM(1,ji).ne.0.or.KModM(2,ji).ne.0.or.KModM(3,ji).ne.0.)
     1    then
          phf(i)=phfm(ji)
          sphf(i)=0.
        else if(NDimI(KPhase).gt.0) then
          phf(i)=phf(j)
          sphf(i)=0.
        endif
        if(lite(KPhase).eq.0) then
          if(itf(i).ne.1) then
            do k=1,6
              beta(k,i)=beta(k,i)/urcp(k,isw,KPhase)
          enddo
          else
            beta(1,i)=beta(1,i)/episq
          endif
        endif
        if(tisk.eq.1) then
          write(lst,101) atom(i),isf(i),itf(i),ai(i),(x(k,i),k=1,3),
     1                   (KFA(k,i),k=1,3),(KModA(k,i),k=1,3),
     2                   (beta(k,i),k=1,6)
          if(KModA(1,i).gt.0) then
            call newln(1)
            write(lst,102) a0(i)
            do k=1,KModA(1,i)
              call newln(1)
              write(lst,102) ax(k,i),ay(k,i)
            enddo
          endif
          do k=1,KModA(2,i)
            call newln(1)
            write(lst,102)(ux(l,k,i),l=1,3),(uy(l,k,i),l=1,3)
          enddo
          do k=1,KModA(3,i)
            call newln(1)
            write(lst,102)(bx(l,k,i),l=1,6)
            call newln(1)
            write(lst,102)(by(l,k,i),l=1,6)
          enddo
          if(KModA(1,i).gt.0.or.KModA(2,i).gt.0.or.KModA(3,i).gt.0) then
            call newln(1)
            write(lst,102) phf(i)
          endif
        endif
        if(lite(KPhase).eq.0) then
          if(itf(i).ne.1) then
            do k=1,6
              beta(k,i)=beta(k,i)*urcp(k,isw,KPhase)
            enddo
          else
            beta(1,i)=beta(1,i)*episq
          endif
        endif
      enddo
9999  NAtCalc=NAtCalcIn
      NAtCalcBasic=NAtCalc
      return
101   format(a8,2i3,4x,4f9.6,3x,'>>',3i1,3i3,'<<'/1x,6f9.6)
102   format(6f9.6)
      end
