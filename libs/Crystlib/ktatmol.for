      function KtAtMol(at)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) at
      character*8 atp
      atp=at
      k=KtAt(Atom,NAtInd+NAtPos,atp)
      if(k.le.0.and.NMolec.gt.0) then
        k=ktat(Atom(NAtMolFr(1,1)),NAtMol,atp)
        if(k.le.0) then
          i=index(at,'#')-1
          if(i.gt.0) then
            atp=at(:i)
            cislo=at(i+2:)
            call posun(cislo,0)
            read(cislo,FormI15,err=9000) i
          else
            atp=at(1:idel(at))
            i=1
          endif
          k=ktat(MolName,NMolec,atp)
          if(k.le.0.or.i.gt.mam(k)) go to 9000
          k=-i-mxp*(k-1)
        else
          k=k+NAtMolFr(1,1)-1
        endif
      endif
      go to 9999
9000  k=0
9999  KtAtMol=k
      return
      end
