      subroutine IndFromIndReal(hi,MMaxIn,difi,ih,itwp,isw,reduce,
     1                          CheckExtRefIn)
      include 'fepc.cmn'
      include 'basic.cmn'
      logical CheckExtRefIn
      dimension hi(3),hii(6),hp(6),h(6),ihc(6,3),dif(3),ih(6),difi(3),
     1          MMin(3),MMax(3)
      kmn=999999
      ksn=999999
      isw=0
      itwp=0
      KPhaseIn=KPhase
      MMax=0
      do i=1,NDimI(KPhase)
        MMax(i)=MMaxIn
      enddo
      MMin=-MMax
      do it=1,NTwin
        KPhase=KPhaseTwin(it)
        hii=0.
        call multm(hi,rtw(1,it),hii,1,3,3)
!        do 1900i=1,NComp(KPhase)
        do 1900i=1,1
          if(reduce.gt.0..and.NTwin.gt.1) then
            if(sctw(it,KDatBlock).lt.reduce/float(NTwin)) go to 1900
          endif
          call multm(hii,zvi(1,i,KPhase),hp,1,NDim(KPhase),NDim(KPhase))
          h=0.
          do j=1,3
            h(j)=hp(j)
            do k=4,NDim(KPhase)
              h(j)=h(j)+hp(k)*qu(j,k-3,i,KPhase)
            enddo
          enddo
          call ChngInd(h,ihc(1,i),i,dif,MMin,MMax,1,CheckExtRefIn)
          do j=1,3
            if(abs(dif(j)).gt.difi(j)) go to 1900
          enddo
          if(NComp(KPhase).gt.1) then
            if(i.ne.1) call indtr(ihc(1,i),zv(1,i,KPhase),ihc,
     1                                           NDim(KPhase))
            do j=2,NComp(KPhase)
              call indtr(ihc,zvi(1,j,KPhase),ihc(1,j),NDim(KPhase))
            enddo
          endif
          km=999999
          ks=0
          do k=1,NComp(KPhase)
            kmp=0
            if(ihc(1,k).gt.900) cycle
            do j=4,NDim(KPhase)
              m=iabs(ihc(j,k))
              kmp=kmp+m
              ks=ks+m
            enddo
            km=min(km,kmp)
          enddo
          if(km.lt.kmn.or.(km.eq.kmn.and.ks.lt.ksn)) then
            itwp=it
            isw=1
            kmn=km
            ksn=ks
            call CopyVekI(ihc,ih,NDim(KPhase))
          endif
1900    continue
      enddo
      KPhase=KPhaseIn
      return
      end
