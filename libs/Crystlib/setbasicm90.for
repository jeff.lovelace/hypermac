      subroutine SetBasicM90(KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      LamAve(KDatB)=-1.
      LamA1(KDatB)=-1.
      LamA2(KDatB)=-1.
      NAlfa(KDatB)=1
      LamRat(KDatB)=.5
      LpFactor(KDatB)=1
      AngleMon(KDatB)=0.
      AlphaGMon(KDatB)=0.
      BetaGMon(KDatB)=0.
      FractPerfMon(KDatB)=.5
      PwdMethod(KDatB)=0
      TOFJason(KDatB)=0
      TOFTTh(KDatB)=-3333.
      TOFDifc(KDatB)=-3333.
      TOFDifa(KDatB)=-3333.
      TOFZero(KDatB)=-3333.
      TOFCe(KDatB)=-3333.
      TOFZeroe(KDatB)=-3333.
      TOFTCross(KDatB)=-3333.
      TOFWCross(KDatB)=-3333.
      XDDirCos(KDatB)=0
      call SetRealArrayTo(TOFSigma(1,KDatB),3,-3333.)
      call SetRealArrayTo(TOFGamma(1,KDatB),3,-3333.)
      call SetRealArrayTo(TOFAlfbe(1,KDatB),4,-3333.)
      call SetRealArrayTo(TOFAlfbt(1,KDatB),4,-3333.)
      EDSlope(KDatB)=-3333.
      EDOffset(KDatB)=-3333.
      EDTTh(KDatB)=-3333.
      Radiation(KDatB)=XRayRadiation
      DatCollTemp(KDatB)=293.
      DataType(KDatB)=1
      NRef90(KDatB)=0
      FLimPrint(KDatB)=5.
      FLimCull(KDatB)=-1.
      IndSlowest(KDatB)=3
      IndFastest(KDatB)=1
      DataAve(KDatB)=1
      AveSigWt(KDatBlock)=0
      EM9ObsLim(KDatB)=3.
      EM9ScaleLim(KDatB)=10.
      EM9ScToM90(KDatB)=1.
      AddCentrSymm(KDatB)=0
      SigIMethod(KDatB)=1
      MultAve(KDatB)=1
      DiffScales(KDatB)=1
      NoRefItems(KDatB)=1
      UseIncSpect(KDatB)=0
      do i=1,MxDatBlock
        ScNoScheme(i,KDatB)=i
      enddo
      HKLF5(KDatB)=0
      MagFieldDir(1:3,KDatB)=-333.
      MagPolFlag(KDatB)=0
      MagPolPlus(KDatB)=-333.
      MagPolMinus(KDatB)=-333.
      EM9Instab(KDatB)=-999.
      return
      end
