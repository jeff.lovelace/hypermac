      subroutine NactiCifKeys(CifKey,CifKeyFlag,Klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta
      character*(*) CifKey(400,40)
      integer CifKeyFlag(400,40)
      logical ExistFile
      if(klic.eq.0) then
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'cif'//ObrLom//'cif.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'cif/cif.dat'
        endif
      else
        Veta='cif.dat'
      endif
      if(.not.ExistFile(Veta)) then
        call FeChybne(-1.,-1.,'the dictionary file "'//
     1    Veta(:idel(Veta))//'" does not exist.',
     2    'The action cannot be performed',SeriousError)
        ErrFlag=1
        go to 9999
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','unknown')
      read(ln,FormA) Veta
      if(Veta(1:1).ne.'#') rewind ln
      if(ErrFlag.ne.0) go to 9000
      call SetIntArrayTo(CifKeyFlag,16000,0)
      call SetStringArrayTo(CifKey,16000,' ')
1500  read(ln,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1500
      k=0
      call kus(Veta,k,Cislo)
      call posun(Cislo,0)
      read(Cislo,FormI15) j
      call kus(Veta,k,Cislo)
      call posun(Cislo,0)
      read(Cislo,FormI15) i
      call kus(Veta,k,CifKey(j,i))
      go to 1500
9000  call CloseIfOpened(ln)
9999  return
      end
