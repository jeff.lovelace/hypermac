      subroutine CrlAtomNamesAddNewString(StringIn,Position,AddChar,
     1                                    NewAtomIndex,StringOut,ich)
      use Atoms_mod
      character*(*) StringIn,StringOut
      character*80 t80
      character*1  AddChar
      integer Position
      logical EqIgCase
      ich=0
      call kus(StringIn,Position,t80)
      j=LocateAtom(AtomRenameOld,NRenameAt,t80,jj)
      if(j.le.0) then
        StringOut=StringOut(:idel(StringOut))//AddChar//t80(:idel(t80))
        go to 9999
      endif
      if(AtomRenameNew(j,NewAtomIndex).eq.' ') then
        StringOut=StringOut(:idel(StringOut))//AddChar//t80(:idel(t80))
      else if(EqIgCase(AtomRenameNew(j,NewAtomIndex),'#delete#')) then
        ich=-1
      else
        StringOut=StringOut(:idel(StringOut))//AddChar//
     1            AtomRenameNew(j,NewAtomIndex)
     2            (:idel(AtomRenameNew(j,NewAtomIndex)))
        if(jj.gt.0) StringOut=StringOut(:idel(StringOut))//
     1                        t80(jj:idel(t80))
      endif
9999  return
      end
