      subroutine CrlCommenCheck
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmp(36),xr(9)
      integer AddGen(:)
      logical EqRV
      allocatable AddGen
      allocate(AddGen(NSymm(KPhase)))
      call SetIntArrayTo(AddGen,NSymm(KPhase),0)
      do 1500k=1,NSymm(KPhase)
        do j=1,3
          if(abs(rm6(j*4,k,1,KPhase)).gt..001) go to 1500
        enddo
        AddGen(k)=1
1500  continue
      do k=1,NSymm(KPhase)
        if(AddGen(k).gt.0) cycle
        AddGen(k)=1
        do i=1,NSymm(KPhase)
          if(AddGen(i).eq.0) cycle
          do j=1,NSymm(KPhase)
            if(AddGen(j).eq.0) cycle
            call MultM(rm6(1,i,1,KPhase),rm6(1,j,1,KPhase),rmp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MatBlock3(rmp,xr,NDim(KPhase))
            do m=1,NSymm(KPhase)
              if(AddGen(m).ne.0) cycle
              if(EqRV(rm(1,m,1,KPhase),xr,9,.01)) then
                if(AddGen(m).eq.0)
     1            call CopyMat(rmp,rm6(1,m,1,KPhase),NDim(KPhase))
                exit
              endif
            enddo
          enddo
        enddo
      enddo
      deallocate(AddGen)
9999  return
      end
