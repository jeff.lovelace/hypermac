      subroutine atsymi(at,i,x3,x4dif,tdif,is,ich,*)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x3(3),x4dif(3),tdif(3)
      character*80 ch80
      character*(*) at
      call atsym(at,i,x3,x4dif,tdif,is,ich)
      if(ich.eq.1) then
        call FeChybne(-1.,YBottomMessage,'String "'//at(:idel(at))//
     1                '" contains non-numerical character, try again.',
     2                ' ',SeriousError)
        return 1
      else if(ich.eq.2) then
        i=index(at,'#')-1
        if(i.le.0) i=idel(at)
        ch80='Atom "'//at(:i)//'" doesn''t exist'
        if(NAtCalc.eq.NAtInd.and.NMolec.gt.0)
     1    ch80=ch80(:idel(ch80))//' in atomic part'
        ch80=ch80(:idel(ch80))//', try again.'
        call FeChybne(-1.,YBottomMessage,ch80,' ',SeriousError)
        i=0
        return 1
      else if(ich.eq.3) then
        call FeChybne(-1.,YBottomMessage,'The symmetry part of "'//
     1                at(:idel(at))//
     2                '" isn''t correct, try again.',' ',SeriousError)
        return 1
      endif
      return
      end
