      subroutine ShpCoor2Fract(xx,RM)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xx(*),xp(3),RM(*),RMP(9)
      R=xx(1)
      cosp=cos(xx(2)*ToRad)
      sinp=sin(xx(2)*ToRad)
      cost=cos(xx(3)*ToRad)
      sint=sin(xx(3)*ToRad)
      xp(1) = R*cosp*sint
      xp(2) = R*sinp*sint
      xp(3) = R*cost
      RMP(1)=   cosp*sint
      RMP(2)=   sinp*sint
      RMP(3)=        cost
      RMP(4)=-R*sinp*sint*ToRad
      RMP(5)= R*cosp*sint*ToRad
      RMP(6)= 0.
      RMP(7)= R*cosp*cost*ToRad
      RMP(8)= R*sinp*cost*ToRad
      RMP(9)=-R*     sint*ToRad
      call MultM(TrToOrthoI(1,1,KPhase),xp,xx,3,3,1)
      call MultM(TrToOrthoI(1,1,KPhase),RMP,RM,3,3,3)
      return
      end
