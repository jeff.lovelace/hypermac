      subroutine UpdateOrtho(ia,x40,delta,eps,isel,nsel)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Gor(:,:),p(:),pp(:),isel(nsel),iselp(:),Gd(:),hh(1)
      allocatable Gor,p,pp,Gd,iselp
      nn=20
1100  if(allocated(Gor)) deallocate(Gor,p,pp,Gd,iselp)
      allocate(Gor(nn,nn),p(nn),pp(nn),Gd(nn*(nn+1)),iselp(nn))
      if(eps.lt..999) then
        call SetOrM(x40,delta,Gor,nn,ia)
        do i=1,nn
          p(i)=sqrt(Gor(i,i))
        enddo
        do i=1,nn
          do j=1,nn
            Gor(i,j)=Gor(i,j)/(p(i)*p(j))
          enddo
        enddo
        iselp=0
        gd(1)=1.
        ngd=1
        iselp(1)=1
        i=1
        do j=2,nn
          n=0
          do k=1,i
            if(iselp(k).eq.0) cycle
            n=n+1
            p(n)=Gor(j,k)
          enddo
          call nasob(gd,p,pp,ngd)
          call multm(pp,p,hh,1,ngd,1)
          if(hh(1).le.eps**2) then
            iselp(j)=1
            ngd=ngd+1
            n=0
            i=j
            do ii=1,i
              if(iselp(ii).eq.0) cycle
              do jj=1,ii
                if(iselp(jj).eq.0) cycle
                n=n+1
                gd(n)=Gor(ii,jj)
              enddo
            enddo
            call smi(gd,ngd,ising)
          endif
        enddo
        m=0
        do i=1,nn
          if(iselp(i).ne.0) then
            m=m+1
            if(m.gt.nsel) then
              go to 9999
            else
              isel(m)=i-1
            endif
           endif
        enddo
        if(m.lt.nsel) then
          nn=nn*2
          go to 1100
        endif
      else
        do i=1,nsel
          isel(i)=i-1
        enddo
      endif
9999  if(allocated(Gor)) deallocate(Gor,p,pp,Gd,iselp)
      return
      end
