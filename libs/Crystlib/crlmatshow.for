      subroutine CrlMatShow(xm,ym,ild,Decimal,RMat,nd)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension RMat(*)
      character*80 Veta
      character*15 FormR
      integer :: id=0,UseTabsIn,Decimal
      logical WizardModeIn
      save UseTabsIn,UseTabsP,LastActiveQuestSave
      XPosOld=XPos
      YPosOld=YPos
      WizardModeIn=WizardMode
      WizardMode=.false.
      if(Decimal.gt.0) then
        dpom=60.
        xqd=dpom*float(nd)+20.
        write(FormR,'(''(f15.'',i2,'')'')') Decimal
      else
        dpom=40.
        xqd=dpom*float(nd-1)+50.
      endif
      id=NextQuestId()
      call FeQuestCreate(id,xm,ym,xqd,ild,' ',0,0,-1,-1)
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      UseTabsP=UseTabs
      if(Decimal.gt.0) then
        tpom=5.
        xpom=20.
      else
        tpom=5.
        xpom=25.
      endif
      do i=1,nd
        if(Decimal.gt.0) then
          call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        else
          call FeTabsAdd(xpom,UseTabs,IdLeftTab,' ')
        endif
        xpom=xpom+dpom
      enddo
      il=1
      tpom=5.
      do i=1,nd
        Veta=' '
        k=i
        do j=1,nd
          if(Decimal.gt.0) then
            write(Cislo,FormR) RMat(k)
            call Zhusti(Cislo)
          else
            call ToFract(RMat(k),Cislo,15)
          endif
          Veta=Veta(:idel(Veta))//Tabulator//Cislo(:idel(Cislo))
          k=k+nd
        enddo
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        il=il+1
      enddo
      il=1
      call FeQuestSvisliceFromToMake(id,tpom,il,il+nd-1,1)
      tpom=xqd-5.
      call FeQuestSvisliceFromToMake(id,tpom,il,il+nd-1,1)
      call FeFrToRefresh(QuestCalledFrom(id))
      LastActiveQuest=QuestCalledFrom(id)
      LastActiveQuestSave=LastActiveQuest
      go to 9000
      entry CrlMatHide
      if(id.eq.0) go to 9999
      XPosOld=XPos
      YPosOld=YPos
      LastActiveQuest=id
      UseTabs=UseTabsP
      WizardModeIn=WizardMode
      WizardMode=.false.
      call FeFrToRefresh(id)
      call FeQuestActiveObjOn
      call FeQuestRemove(id)
      LastActiveQuest=LastActiveQuestSave
      call FeTabsReset(UseTabs)
9000  UseTabs=UseTabsIn
      WizardMode=WizardModeIn
      call FeMoveMouseTo(XPosOld,YPosOld)
      go to 9999
      entry CrlMatReset
      id=0
9999  return
      end
