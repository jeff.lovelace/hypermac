      subroutine CtiLatt(tisk)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer tisk
      call velka(lattice(KPhase))
      cislo=lattice(KPhase)(2:4)
      call posun(cislo,0)
      read(cislo,FormI15) icp
      latt=index(smbc,lattice(KPhase)(1:1))
      if(latt.le.0.or.latt.gt.7) go to 9000
1010  go to (1050,1020,1020,1020,1020,1030,1040),latt
1020  call SetRealArrayTo(vt6(1,2,1,KPhase),3,.5)
      if(latt.ne.5) vt6(latt-1,2,1,KPhase)=0.
      NLattVec(KPhase)=2
      n=10**(NDim(KPhase)-4)
      do i=4,NDim(KPhase)
        if(mod(icp/n,10).eq.1) then
          vt6(i,2,1,KPhase)=.5
        else
          vt6(i,2,1,KPhase)=.0
        endif
        n=n/10
      enddo
      go to 1060
1030  NLattVec(KPhase)=3
      vt6(1,2,1,KPhase)=.666666666
      vt6(2,2,1,KPhase)=.333333333
      vt6(3,2,1,KPhase)=.333333333
      vt6(1,3,1,KPhase)=.333333333
      vt6(2,3,1,KPhase)=.666666666
      vt6(3,3,1,KPhase)=.666666666
      do j=2,3
        do i=4,NDim(KPhase)
          vt6(i,j,1,KPhase)=0.
        enddo
      enddo
      go to 1060
1040  NLattVec(KPhase)=4
      do j=2,4
        do i=1,3
          if(i.ne.j-1) then
            vt6(i,j,1,KPhase)=.5
          else
            vt6(i,j,1,KPhase)=0.
          endif
        enddo
        n=10**(NDim(KPhase)-4)
        do i=4,NDim(KPhase)
          k=mod(icp/n,10)
          if(k.eq.0.or.k.eq.j-1) then
            vt6(i,j,1,KPhase)=0.
          else
            vt6(i,j,1,KPhase)=.5
          endif
          n=n/10
        enddo
      enddo
      go to 1060
1050  NLattVec(KPhase)=1
1060  do i=1,NDim(KPhase)
        vt6(i,1,1,KPhase)=0.
      enddo
      go to 9999
9000  call FeChybne(-1.,-1.,'wrong lattice specification : '//
     1              lattice(KPhase),' ',SeriousError)
      ErrFlag=1
9999  return
      end
