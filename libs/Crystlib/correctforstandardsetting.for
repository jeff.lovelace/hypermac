      subroutine CorrectForStandardSetting
      include 'fepc.cmn'
      include 'basic.cmn'
      character*8  PosunuteGrupy(41)
      logical EqIgCase
      data PosunuteGrupy/'Pnnn','Pban','Pncb','Pcna','Pmmn','Pnmm',
     1                   'Pmnm','Ccca','Abaa','Bbcb','Cccb','Acaa',
     2                   'Bbab','Ccce','Aeaa','Bbeb','Fddd',
     3                   'P4/n','P42/n','I41/a','-I4ad','P4/mcc',
     4                   'P4/nnc','P4/nmm','P4/ncc','P42/nbc','P42/nnm',
     5                   'P42/nmc','P42/ncm','I41/amd','I41/acd',
     6                   'Pn-3','Fm-3','Fd-3','Pn-3n','Pm-3n','Pn-3m',
     7                   'Fm-3m','Fm-3c','Fd-3m','Fd-3c'/
      if(NDim(KPhase).eq.4) then
        i=index(Grupa(KPhase),':')
        if(i.gt.0) then
          Cislo=Grupa(KPhase)(i+1:)
          i=index(Cislo,':')
          if(i.gt.0) then
            Cislo(i:)=' '
          else
            go to 9999
          endif
        else
          i=index(Grupa(KPhase),'(')
          if(i.gt.0) Cislo=Grupa(KPhase)(:i-1)
        endif
      else
        Cislo=Grupa(KPhase)
      endif
      do i=1,41
        if(EqIgCase(Cislo,PosunuteGrupy(i))) then
          call FindSmbSg(Grupa(KPhase),ChangeOrderNo,1)
          go to 9999
        endif
      enddo
9999  return
      end
