      subroutine AtomDistLimitsReadCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/AtomDist/ nRolMenuFirst,nEdwDMin,nEdwDMax,DMin,DMax,
     1                 AtNames(2)
      dimension xp(2)
      character*80 t80
      character*2  AtNames
      character*(*) Command
      save /AtomDist/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'distlim'.and.t80.ne.'!distlim') go to 8010
      do i=1,2
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        j=ktat(AtType(1,KPhase),NAtFormula(KPhase),t80)
        if(j.le.0) then
          t80='the atom type "'//t80(:idel(t80))//'" doesn''t exist'
          go to 8100
        endif
        AtNames(i)=AtType(j,KPhase)
      enddo
      if(k.ge.lenc) go to 8000
      kk=k
      call StToReal(Command,k,xp,2,.false.,ich)
      if(ich.eq.0) then
        DMin=xp(1)
        DMax=xp(2)
      else
        go to 8000
      endif
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8020  t80='incorrect integer "'//t80(:idel(t80))//'"'
      go to 8100
8030  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
