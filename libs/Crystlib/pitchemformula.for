      subroutine PitChemFormula(ChemFormula,AtomNames,AtomMult,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*2 at
      character*(*) ChemFormula
      character*(*) AtomNames(*)
      character*256 EdwStringQuest
      character*80 Veta
      character*1  Znak
      logical PropFontIn,EqIgCase
      dimension AtomMult(*),ZavorkaMult(10)
      if(ChemFormula(1:1).eq.'?') then
        n=0
        go to 9999
      endif
1000  iz=0
      i=0
      id=idel(ChemFormula)
1100  i=i+1
      if(i.gt.id) go to 2000
      if(ChemFormula(i:i).eq.'(') then
        iz=iz+1
        izp=iz
        j=i
1200    j=j+1
        if(j.gt.id) go to 9999
        if(ChemFormula(j:j).eq.'(') then
          izp=izp+1
          go to 1200
        else if(ChemFormula(j:j).eq.')') then
          izp=izp-1
          if(izp.eq.iz-1) then
            Cislo=' '
            k=0
1300        k=k+1
            j=j+1
            if(j.gt.id) go to 1400
            Znak=ChemFormula(j:j)
            if(index(Cifry(1:11),Znak).gt.0) then
              Cislo(k:k)=Znak
              go to 1300
            else if(Znak.eq.'.') then
              go to 1300
            endif
1400        if(Cislo.eq.' ') then
              ZavorkaMult(iz)=1.
            else
              call Posun(Cislo,1)
              read(Cislo,'(f15.0)',err=9000) ZavorkaMult(iz)
            endif
            go to 1100
          endif
        endif
        go to 1200
      endif
      go to 1100
2000  iz=0
      i=0
      iat=0
      Cislo=' '
      At=' '
      k=0
      AllMult=1.
      n=0
3000  i=i+1
      if(i.le.id) then
        Znak=ChemFormula(i:i)
        ind=index(Cifry(1:10),Znak)
      endif
      if(Znak.eq.'('.or.Znak.eq.')'.or.Znak.eq.' '.or.
     1   i.eq.1.or.i.gt.id) then
        if(iat.gt.0) then
          call uprat(at)
          ia=ktat(atn,98,at)
          if(ia.le.0) then
            if(EqIgCase(at,'D')) then
              ia=1
            else
              go to 9000
            endif
          endif
          if(Cislo.eq.' ') then
            pom=AllMult
          else
            call Posun(Cislo,1)
            read(Cislo,'(f15.0)',err=9000) pom
            pom=pom*AllMult
          endif
          ia=ktat(AtomNames,n,At)
          if(ia.gt.0) then
            AtomMult(ia)=AtomMult(ia)+pom
          else
            n=n+1
            AtomNames(n)=At
            AtomMult(n)=pom
          endif
          iat=0
          Cislo=' '
          At=' '
          k=0
        endif
      endif
      if(i.gt.id) go to 9999
      if(Znak.eq.'(') then
        iz=iz+1
        AllMult=AllMult*ZavorkaMult(iz)
        izp=iz
      else if(Znak.eq.')') then
        AllMult=AllMult/ZavorkaMult(izp)
        izp=izp-1
      else if(Znak.eq.' ') then
        go to 3000
      else if(ind.gt.0.or.Znak.eq.'.') then
        if(iat.gt.0) then
          k=k+1
          Cislo(k:k)=Znak
        endif
        go to 3000
      else
        iat=iat+1
        if(iat.gt.2) go to 9000
        At(iat:iat)=Znak
      endif
      go to 3000
9000  PropFontIn=PropFont
      if(.not.PropFont) call FeSetPropFont
      Veta='A syntax error in the formula, please correct it:'
      ich=0
      id=NextQuestId()
      xqd=300.
      il=3
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,
     1                   0,0)
      il=1
      Veta='Chemical formula:'
      tpom=5.
      xpom=FeTxLength(Veta)+tpom+5.
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestStringEdwOpen(EdwLastMade,ChemFormula)
      nEdwFormula=EdwLastMade
      il=-10*il-15
      Veta='Please note that chemical elements should be separated'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il-5
      Veta='by at least one space: e.g. Na2 C O3, e.t.c.'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
9500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 9500
      endif
      if(ich.eq.0) ChemFormula=EdwStringQuest(nEdwFormula)
      call FeQuestRemove(id)
      if(.not.PropFontIn) call FeSetFixFont
      if(ich.eq.0) go to 1000
9999  return
      end
