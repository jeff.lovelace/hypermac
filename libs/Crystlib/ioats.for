      subroutine ioats(ias,iak,klic,*)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical PolMag
      do i=ias,iak
        itfi=itf(i)
        isw=iswa(i)
        if(klic.eq.0) then
          read(m40,100,end=2500,err=2500) cislo(1:8),sai(i),
     1                    (sx(j,i),j=1,3),(sbeta(j,i),j=1,6)
        else
          if(lite(KPhase).eq.0) then
            if(itfi.ge.2) then
              do j=1,6
                sbeta(j,i)=sbeta(j,i)/urcp(j,isw,KPhase)
              enddo
            else if(itfi.eq.1) then
              sbeta(1,i)=sbeta(1,i)/episq
              call SetRealArrayTo(sbeta(2,i),5,0.)
            endif
          endif
          if(itfi.eq.0) call SetRealArrayTo(sbeta(1,i),6,0.)
          write(m40,100) atom(i),sai(i),(sx(j,i),j=1,3),
     1                   (sbeta(j,i),j=1,6)
        endif
        if(lite(KPhase).eq.0) then
          if(itfi.ge.2) then
            do j=1,6
              sbeta(j,i)=sbeta(j,i)*urcp(j,isw,KPhase)
            enddo
          else if(itfi.eq.1) then
            sbeta(1,i)=sbeta(1,i)*episq
          endif
        endif
        if(itfi.gt.2) then
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sc3(j,i),j=1,10)
          else
            write(m40,101)(sc3(j,i),j=1,10)
          endif
        endif
        if(itfi.gt.3) then
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sc4(j,i),j=1,15)
          else
            write(m40,101)(sc4(j,i),j=1,15)
          endif
        endif
        if(itfi.gt.4) then
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sc5(j,i),j=1,21)
          else
            write(m40,101)(sc5(j,i),j=1,21)
          endif
        endif
        if(itfi.gt.5) then
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sc6(j,i),j=1,28)
          else
            write(m40,101)(sc6(j,i),j=1,28)
          endif
        endif
        if(ifr(i).gt.0) then
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500) sxfr(i)
          else
            write(m40,101) sxfr(i)
          endif
        endif
        if(NDimI(KPhase).le.0) then
          if(lasmax(i).gt.0) then
            j=lasmax(i)
            if(klic.eq.0) then
              if(j.gt.1) then
                read(m40,101,end=2500,err=2500) spopc(i),spopv(i),
     1                                          skapa1(i),skapa2(i)
              else
                read(m40,101,end=2500,err=2500) spopc(i),spopv(i),
     1                                          skapa1(i)
                skapa2(i)=0.
              endif
            else
              if(j.gt.1) then
                write(m40,101) spopc(i),spopv(i),skapa1(i),skapa2(i)
              else
                write(m40,101) spopc(i),spopv(i),skapa1(i)
              endif
            endif
            k=(j-1)**2
            if(k.le.0) go to 1800
            if(klic.eq.0) then
              read(m40,101,end=2500,err=2500)(spopas(j,i),j=1,k)
            else
              write(m40,101)(spopas(j,i),j=1,k)
            endif
          endif
          go to 1800
        endif
        if(KModA(1,i).gt.0) then
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500) sa0(i)
          else
            write(m40,101) sa0(i)
          endif
        endif
        do j=1,KModA(1,i)
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500) sax(j,i),say(j,i)
          else
            write(m40,101) sax(j,i),say(j,i)
          endif
        enddo
        do j=1,KModA(2,i)
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sux(k,j,i),k=1,3),
     1                                     (suy(k,j,i),k=1,3)
          else
            write(m40,101)(sux(k,j,i),k=1,3),(suy(k,j,i),k=1,3)
          endif
        enddo
        do j=1,KModA(3,i)
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sbx(k,j,i),k=1,6),
     1                                     (sby(k,j,i),k=1,6)
          else
            if(lite(KPhase).eq.0) then
              do k=1,6
                sbx(k,j,i)=sbx(k,j,i)/urcp(k,isw,KPhase)
                sby(k,j,i)=sby(k,j,i)/urcp(k,isw,KPhase)
              enddo
            endif
            write(m40,101)(sbx(k,j,i),k=1,6),(sby(k,j,i),k=1,6)
          endif
          if(lite(KPhase).eq.0) then
            do k=1,6
              sbx(k,j,i)=sbx(k,j,i)*urcp(k,isw,KPhase)
              sby(k,j,i)=sby(k,j,i)*urcp(k,isw,KPhase)
            enddo
          endif
        enddo
        do j=1,KModA(4,i)
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sc3x(k,j,i),k=1,10)
            read(m40,101,end=2500,err=2500)(sc3y(k,j,i),k=1,10)
          else
            write(m40,101)(sc3x(k,j,i),k=1,10)
            write(m40,101)(sc3y(k,j,i),k=1,10)
          endif
        enddo
        do j=1,KModA(5,i)
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sc4x(k,j,i),k=1,15)
            read(m40,101,end=2500,err=2500)(sc4y(k,j,i),k=1,15)
          else
            write(m40,101)(sc4x(k,j,i),k=1,15)
            write(m40,101)(sc4y(k,j,i),k=1,15)
          endif
        enddo
        do j=1,KModA(6,i)
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sc5x(k,j,i),k=1,21)
            read(m40,101,end=2500,err=2500)(sc5y(k,j,i),k=1,21)
          else
            write(m40,101)(sc5x(k,j,i),k=1,21)
            write(m40,101)(sc5y(k,j,i),k=1,21)
          endif
        enddo
        do j=1,KModA(7,i)
          if(klic.eq.0) then
            read(m40,101,end=2500,err=2500)(sc6x(k,j,i),k=1,28)
            read(m40,101,end=2500,err=2500)(sc6y(k,j,i),k=1,28)
          else
            write(m40,101)(sc6x(k,j,i),k=1,28)
            write(m40,101)(sc6y(k,j,i),k=1,28)
          endif
        enddo
        do j=1,7
          if(KModA(j,i).gt.0) then
            if(klic.eq.0) then
              read(m40,101,end=2500,err=2500) sphf(i)
            else
              write(m40,101) sphf(i)
            endif
            exit
          endif
        enddo
1800    if(MagneticType(KPhase).gt.0) then
          PolMag=ktat(NamePolar,NPolar,Atom(i)).gt.0
          do j=1,MagPar(i)
            if(j.eq.1) then
              if(klic.eq.0) then
                read(m40,101,end=2500,err=2500)(ssm0(k,i),k=1,3)
              else
                if(.not.PolMag) then
                  do k=1,3
                    ssm0(k,i)=ssm0(k,i)*CellPar(k,isw,KPhase)
                  enddo
                endif
                write(m40,101)(ssm0(k,i),k=1,3)
              endif
                if(.not.PolMag) then
                do k=1,3
                  ssm0(k,i)=ssm0(k,i)/CellPar(k,isw,KPhase)
                enddo
              endif
            else
              if(klic.eq.0) then
                read(m40,101,end=2500,err=2500)
     1           (ssmx(k,j-1,i),k=1,3),(ssmy(k,j-1,i),k=1,3)
              else
                if(.not.PolMag) then
                  do k=1,3
                    ssmx(k,j-1,i)=ssmx(k,j-1,i)*CellPar(k,isw,KPhase)
                    ssmy(k,j-1,i)=ssmy(k,j-1,i)*CellPar(k,isw,KPhase)
                  enddo
                endif
                write(m40,101)
     1           (ssmx(k,j-1,i),k=1,3),(ssmy(k,j-1,i),k=1,3)
              endif
              if(.not.PolMag) then
                do k=1,3
                  ssmx(k,j-1,i)=ssmx(k,j-1,i)/CellPar(k,isw,KPhase)
                  ssmy(k,j-1,i)=ssmy(k,j-1,i)/CellPar(k,isw,KPhase)
                enddo
              endif
            endif
          enddo
        endif
      enddo
      go to 9999
2500  return 1
9999  return
100   format(a8,10x,4f9.6/6f9.6)
101   format(6f9.6)
      end
