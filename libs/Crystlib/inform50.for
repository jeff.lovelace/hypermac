      subroutine inform50
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*128 ven
      character*80 t80,p80
      character*60 Grupa1
      character*22 FormatLS
      character*21 format
      character*4  Lattice1
      dimension ip(MxDatBlock),xp(6)
      integer CrlCentroSymm,CrSystem1
      logical EqIgCase
      data FormatLS/'(9x,3f11.4,11x,f11.4)'/
      write(FormatLS(5:5),'(i1)') NDim(KPhase)
      if(KPhase.eq.1.and.
     1   (EqIgCase(RunningProgram,'refine').or.
     2    EqIgCase(RunningProgram,'Create M90'))) then
        call TitulekVRamecku('Radiation')
        il=0
        do 1000KDatB=1,max(NDatBlock,1)
          if(EqIgCase(RunningProgram,'Create M90').and.
     1       KDatB.ne.KDatBlock) go to 1000
          if(NDatBlock.gt.1.and..not.
     1       EqIgCase(RunningProgram,'Create M90')) then
            if(il.gt.0) then
              call newln(il)
              write(lst,FormA)
            endif
            il=1
            call TitulekPodtrzeny(DatBlockName(KDatB),'-')
            call newln(1)
            write(lst,FormA)
          endif
          call newln(1)
          if(Radiation(KDatB).eq.XRayRadiation) then
            if(KLam(KDatB).le.0) then
              if(LpFactor(KDatB).eq.3) then
                t80='X-rays synchrotron'
              else
                t80='X-rays'
              endif
            else
              t80='X-ray tube'
            endif
          else if(Radiation(KDatB).eq.NeutronRadiation) then
            t80='Neutrons'
            if(DataType(KDatB).eq.-2)
     1        t80=t80(:idel(t80))//' - time of flight'
          else if(Radiation(KDatB).eq.ElectronRadiation) then
            t80='Electrons'
          endif
          t80='Radiation type: '//t80(:idel(t80))
          write(lst,FormA) t80(:idel(t80))
          if(DataType(KDatB).ne.-2) then
            call newln(1)
            if(Radiation(KDatB).eq.XRayRadiation.and.
     1        (LpFactor(KDatB).eq.1.or.LpFactor(KDatB).eq.2)) then
              if(NAlfa(KDatB).eq.2) then
                write(lst,'(''Wave length - average: '',f7.5,
     1                      '', KAlpha1: ''f7.5,'', KAlpha2='',f7.5,
     2                      '', I(KAlpa2)/I(KAlpa1): '',f5.3)')
     3            LamAve(KDatB),LamA1(KDatB),LamA2(KDatB),LamRat(KDatB)
              else
                if(KRefLam(KDatB).eq.0) then
                  pom=LamAve(KDatB)
                else
                  pom=LamPwd(1,KDatB)
                endif
                write(lst,105) pom
              endif
              if(LpFactor(KDatB).eq.1) then
                t80='perpendicular'
              else
                t80='parallel'
              endif
              t80='Monochromator - '//t80(:idel(t80))//
     1            ' setting, glancing angle:'
              write(Cislo,'(f5.2)') AngleMon(KDatB)
              t80=t80(:idel(t80)+1)//Cislo(:idel(Cislo))
              call newln(1)
              write(lst,FormA) t80(:idel(t80))
            else  if(iabs(DataType(KDatB)).ne.-2) then
              if(KRefLam(KDatB).eq.0) then
                pom=LamAve(KDatB)
              else
                pom=LamPwd(1,KDatB)
              endif
              write(lst,105) pom
            endif
          endif
1000    continue
      endif
      if(KPhase.ne.KPhaseBasic.and.
     1   .not.EqIgCase(RunningProgram,'refine').and.
     2   .not.EqIgCase(RunningProgram,'Create M90')) go to 9999
      t80='Structure data'
      if(NPhase.gt.1) t80=t80(:idel(t80))//' - '//
     1                    PhaseName(KPhase)(:idel(PhaseName(KPhase)))
      call TitulekVRamecku(t80)
      format='(9x,a7,''|'',.f7.3,''|'')'
      write(format(12:12),100) NDim(KPhase)
      do i=1,NComp(KPhase)
        ic=i
        if(NComp(KPhase).gt.1) then
          call newln(3+NDim(KPhase)+1)
          write(lst,100)
          write(lst,'(''Composite part #'',i1/)') i
          do j=1,NDim(KPhase)
            if(j.eq.NDim(KPhase)/2) then
              t80='W(.) = '
              write(t80(3:3),100) i
            else
              t80=' '
            endif
            write(lst,format)
     1        t80(1:7),(zv(j+(k-1)*NDim(KPhase),i,KPhase),
     2                  k=1,NDim(KPhase))
          enddo
          write(lst,100)
        endif
        call newln(2)
        write(lst,'(''Cell parameters       : '',3f10.4,3f10.3,
     1              ''    Volume : '',f10.1)')
     2              (CellPar(j,i,KPhase),j=1,6),CellVol(i,KPhase)
        write(lst,'(''Reciprocal parameters : '',3f10.6,3f10.3,
     1            ''    Volume : '',f10.6)')
     2            (rcp(j,i,KPhase),j=1,3),
     3              (acos(rcp(j,i,KPhase))/ToRad,j=4,6),
     4              1./CellVol(i,KPhase)
        call newln(1)
        write(lst,100)
        if(NDim(KPhase).gt.3) then
          call newln(NDim(KPhase)-2)
          write(lst,'(''Modulation vector q('',i1,''): '',3f10.6)')
     1      (j,(qu(k,j,i,KPhase),k=1,3),j=1,NDimI(KPhase))
          write(lst,100)
        endif
        if(i.eq.1) then
          t80=grupa(KPhase)
          Grupa1=t80
          Lattice1=Lattice(KPhase)
          NGrupa1=NGrupa(KPhase)
          CrSystem1=CrSystem(KPhase)
          Monoclinic1=Monoclinic(KPhase)
        else
          call FindSmbSg(t80,ChangeOrderNo,ic)
        endif
        ven='Centrosymmetric super-space group: '//t80(:idel(t80))
        if(NDimI(KPhase).le.0) ven(18:)=ven(24:)
        if(CrlCentroSymm().le.0) ven='Non-c'//ven(2:)
        if(NGrupa(KPhase).gt.0) then
          write(Cislo,FormI15) NGrupa(KPhase)
          call Zhusti(Cislo)
          Ven(idel(Ven)+3:)='Number : '//Cislo(:idel(Cislo))
        else
          Ven(idel(Ven)+3:)='Number not specified - non-standard '//
     1                      'setting'
        endif
        call newln(1)
        write(lst,FormA) ven(:idel(ven))
        call newln(3)
        write(lst,'(/''List of centring vectors:''/)')
        do j=1,NLattVec(KPhase)
          call newln(1)
          write(lst,'(6f10.6)')(vt6(k,j,i,KPhase),k=1,NDim(KPhase))
        enddo
        call newln(3)
        write(lst,'(/''Symmetry operators:''/)')
        n=0
        do j=1,NSymm(KPhase)
          call CodeSymm(rm6(1,j,i,KPhase),s6(1,j,i,KPhase),
     1                  symmc(1,j,i,KPhase),0)
          do k=1,NDim(KPhase)
            kk=idel(symmc(k,j,i,KPhase))+1
            if(symmc(k,j,i,KPhase)(1:1).ne.'-') kk=kk+1
            n=max(n,kk)
          enddo
        enddo
        if(i.eq.1) then
          if(allocated(StSymmCard)) deallocate(StSymmCard)
          allocate(StSymmCard(NSymm(KPhase)))
        endif
        if(n.lt.5) n=5
        do j=1,NSymm(KPhase)
          t80=' '
          k=5
          StSymmCard(j)='('
          do l=1,NDim(KPhase)
            p80=symmc(l,j,i,KPhase)
            if(p80(1:1).eq.'-') then
              kk=k
            else
              kk=k+1
            endif
            t80(kk:)=p80
            k=k+n
            StSymmCard(j)=StSymmCard(j)(:idel(StSymmCard(j)))//
     1                    p80(:idel(p80))//','
          enddo
          idl=idel(StSymmCard(j))
          StSymmCard(j)(idl:idl)=')'
          if(MagneticType(KPhase).ne.0.and..not.ParentStructure) then
            if(zmag(j,i,KPhase).gt.0) then
              t80(k+1:k+1)='m'
            else
              t80(k:k+1)='-m'
            endif
          endif
          call newln(1)
          write(lst,FormA) t80(:idel(t80))
        enddo
        if(i.eq.1.and..not.ParentStructure) then
          call FindSmbSGTr(t80,xp,p80,NGrTr,TrMatStd,xp,ic,ii)
          if(.not.EqIgCase(t80,p80)) then
            if(NDimI(KPhase).gt.0) then
              Cislo='superspace'
            else
              Cislo='space'
            endif
            ven='Standard '//Cislo(:idel(Cislo))//' group symbol : '//
     1          p80(:idel(p80))
            if(NGrupa(KPhase).gt.0) then
              write(Cislo,FormI15) NGrTr
              call Zhusti(Cislo)
              Ven(idel(Ven)+3:)='Number : '//Cislo(:idel(Cislo))
            endif
            call NewLn(2)
            write(lst,FormA)
            write(lst,FormA) ven(:idel(ven))
            call NewLn(3)
            call TrMat2String(TrMatStd,NDim(KPhase),t80)
            write(lst,'(/''Transformation matrix to the standard '',
     1                   ''setting : ''a/)') t80(:idel(t80))
          endif
        endif
        if(NSymmL(KPhase).gt.0.and..not.ParentStructure) then
          call newln(3)
          write(lst,'(/''Local symmetry:''/)')
          do j=1,NSymmL(KPhase)
            call newln(NDim(KPhase)+1)
            write(ven,'(''Operator#'',i3)') j
            call Zhusti(ven)
            ven=ven(:idel(ven))//'           Transformation matrix:'
            ven(11*NDim(KPhase)+20:)='Translation vector:'
            write(lst,FormA) ven(:idel(ven))
            do k=1,NDim(KPhase)
              write(lst,FormatLS)
     1          (rm6l(k+(l-1)*NDim(KPhase),j,i,KPhase),
     2           l=1,NDim(KPhase)),s6l(k,j,i,KPhase)
            enddo
          enddo
        endif
      enddo
      do i=1,NAtFormula(KPhase)
        n=0
        do KDatB=1,max(NDatBlock,1)
          if(Radiation(KDatB).eq.XRayRadiation) then
            if(n.eq.0) then
              n=KDatB
            else
              n=-1
            endif
          else if(Radiation(KDatB).eq.NeutronRadiation) then
            ffra(i,KPhase,KDatB)=0.
            ffia(i,KPhase,KDatB)=0.
          endif
        enddo
      enddo
      if(NAtFormula(KPhase).le.0) go to 9000
      if(ExistXRayData) then
        call newln(3)
        p80='Source and type of used form-factors'
        if(n.gt.0) then
          t80='f''      f"'
        else
          t80=' '
        endif
        write(lst,'(/12x,110a1)')
     1        (' ',i=1,15),(p80(i:i),i=1,idel(p80))
        write(lst,FormA)
        do i=1,NAtFormula(KPhase)
          if(FFType(KPhase).gt.0) then
            p80='     Atomic scattering tables - in steps 0.05'
          else if(FFType(KPhase).eq.-9) then
            p80='Atomic scattering coefficients - as in IT, vol.C'
          else
            p80='Atomic scattering tables - in steps as in IT, vol.C'
          endif
          if(ChargeDensities) then
            Cislo='free'
          else
            Cislo=' '
          endif
          call newln(1)
          if(n.gt.0) then
            write(lst,'(a7,2f8.3,5x,a4,1x,80a1)')
     1        AtTypeFull(i,KPhase),ffra(i,KPhase,n),ffia(i,KPhase,n),
     2        Cislo,(p80(j:j),j=1,idel(p80))
          else
            write(lst,'(a7,21x,a4,1x,80a1)')
     1        AtTypeFull(i,KPhase),Cislo,(p80(j:j),j=1,idel(p80))
          endif
          if(FFType(KPhase).ne.-9) then
            jk=0
            nt=iabs(FFType(KPhase))
2200        jp=jk+1
            jk=min(jp+7,nt)
            call newln(1)
            if(n.gt.0) then
              write(lst,102)(FFBasic(j,i,KPhase),j=jp,jk)
            else
              write(lst,103)(FFBasic(j,i,KPhase),j=jp,jk)
            endif
            if(jk.lt.nt) go to 2200
            if(ChargeDensities) then
              call newln(2)
              write(lst,FormA)
              if(n.gt.0) then
                write(lst,'(28x,''core '',8f9.4)')
     1            (FFCore(j,i,KPhase),j=1,8)
              else
                write(lst,'(10x,''core '',8f9.4)')
     1            (FFCore(j,i,KPhase),j=1,8)
              endif
              jk=8
2300          jp=jk+1
              jk=min(jp+7,nt)
              call newln(1)
              if(n.gt.0) then
                write(lst,102)(FFCore(j,i,KPhase),j=jp,jk)
              else
                write(lst,103)(FFCore(j,i,KPhase),j=jp,jk)
              endif
              if(jk.lt.nt) go to 2300
              call newln(2)
              write(lst,FormA)
              if(n.gt.0) then
                write(lst,'(25x,''valence '',8f9.4)')
     1            (FFVal(j,i,KPhase),j=1,8)
              else
                write(lst,'(7x,''valence '',8f9.4)')
     1            (FFVal(j,i,KPhase),j=1,8)
              endif
              jk=8
2400          jp=jk+1
              jk=min(jp+7,nt)
              call newln(1)
              if(n.gt.0) then
                write(lst,102)(FFVal(j,i,KPhase),j=jp,jk)
              else
                write(lst,103)(FFVal(j,i,KPhase),j=jp,jk)
              endif
              if(jk.lt.nt) go to 2400
            endif
            call newln(1)
            write(lst,FormA)
          else
            call newln(2)
            write(lst,'(25x,9f10.6/)')(FFBasic(j,i,KPhase),j=1,9)
          endif
        enddo
        if(n.eq.-1) then
          n=0
          ven=' '
          j=20
          do KDatB=1,max(NDatBlock,1)
            if(Radiation(KDatB).eq.XRayRadiation) then
              n=n+1
              ip(n)=KDatB
              ven(j:)=DatBlockName(KDatB)
              j=j+20
            endif
          enddo
          call newln(NAtFormula(KPhase)+2)
          write(lst,FormA) ven(:idel(ven))
          write(lst,'(''Atom   '',5(7x,a2,8x,a2,1x))')('f''','f"',i=1,n)
          do i=1,NAtFormula(KPhase)
            write(lst,'(a7,1x,5(2f10.3))') AtTypeFull(i,KPhase),
     1        (ffra(i,KPhase,ip(j)),ffia(i,KPhase,ip(j)),j=1,n)
          enddo
        endif
      endif
      if(ExistNeutronData) then
        call newln(3)
        write(lst,'(/''Neutron scattering lengths''/)')
        do i=1,NAtFormula(KPhase)
          call newln(1)
          write(lst,104) AtTypeFull(i,KPhase),ffn (i,KPhase),
     1                                        ffni(i,KPhase)
        enddo
        call newln(1)
        write(lst,FormA)
        if(MagneticType(KPhase).eq.1) then
          call newln(2)
          write(lst,'(''Magnetic form factors''/)')
          do i=1,NAtFormula(KPhase)
            if(AtTypeMag(i,KPhase).ne.' ') then
              call newln(1)
              if(TypeFFMag(i,KPhase).ne.0) then
                t80='  x (sin(th)/lambda)^2'
              else
                t80=' '
              endif
              write(lst,104) AtTypeMag(i,KPhase),
     1                       (FFMag(j,i,KPhase),j=1,7),t80(:idel(t80))
            endif
          enddo
          call newln(1)
          write(lst,FormA)
        endif
      endif
      if(ExistElectronData) then
        call newln(3)
        p80='Source and type of used form-factors'
        write(lst,'(/12x,110a1)')
     1        (' ',i=1,15),(p80(i:i),i=1,idel(p80))
        write(lst,FormA)
        do i=1,NAtFormula(KPhase)
          p80='Atomic scattering amplitudes for electrons  - in steps'//
     1        ' as in IT, vol.C'
          call newln(1)
          write(lst,'(a7,27x,a)') AtTypeFull(i,KPhase),p80(:idel(p80))
          jk=0
          nt=62
3300      jp=jk+1
          jk=min(jp+7,nt)
          call newln(1)
          write(lst,103)(FFEl(j,i,KPhase),j=jp,jk)
          if(jk.lt.nt) go to 3300
          call newln(1)
          write(lst,103)
        enddo
      endif
9000  Grupa(KPhase)=Grupa1
      Lattice(KPhase)=Lattice1
      NGrupa(KPhase)=NGrupa1
      CrSystem(KPhase)=CrSystem1
      Monoclinic(KPhase)=Monoclinic1
9999  return
100   format(i1)
101   format(a7,2f8.3,5x,a4,1x,8f9.4/6(33x,8f9.4/))
102   format(33x,8f9.4)
103   format(15x,8f9.4)
104   format(a7,5x,7f8.3,a)
105   format('Wave length: ',f9.5)
      end
