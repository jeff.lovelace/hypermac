      subroutine GoToSubGr
      use Basic_mod
      use Refine_mod
      use EditM40_mod
      use GoToSubGr_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      include 'datred.cmn'
      dimension rmp(36),TrMatPom(36),TrShift(6),s6o(6),xp(6),xpp(6)
      real ScTwIn(*)
      character*(*) FileMag
      character*256 EdwStringQuest,t256
      character*80 Veta,Grp,FlnOld,FlnNew
      character*12, allocatable :: Names(:)
      character*8  AtKmen
      character*8, allocatable :: AtomOld(:)
      logical Eqrv,FeYesNoHeader,MakeTwin,ExistFile,FeYesNo,
     1        StructureExists,CrwLogicQuest,MuzeBytTwin,EqIgCase,
     2        MatRealEqUnitMat,EqRV0,SaveSuperSymm,GenRestrictions,
     3        Change,ForMagnetic,CrlAcceptTransformation
      logical, allocatable :: AtomAlreadyUsed(:)
      integer UseTabsIn,CrlMagInver
      UseTabsIn=UseTabs
      nss=NSymm(KPhase)
      nvtp=1
      IgnoreE=.true.
      call CompleteSymm(1,ich)
      IgnoreE=.false.
      if(ich.ne.0.or.nss.ne.NSymm(KPhase)) then
        call FeChybne(-1.,-1.,'the set of symmetry operators is not '//
     1                'complete or does not','make a group. Correct '//
     2                'it in Editm50 and try again.',SeriousError)
        go to 9999
      endif
      n=NSymm(KPhase)*NLattVec(KPhase)
      allocate(ips(n),ipr(n),rm6s(NDimQ(KPhase),n),s6s(NDim(KPhase),n),
     1         ipri(n),iopc(n),itrl(n),StSym(n),BratSymmSub(n),
     2         vt6s(NDim(KPhase),n),io(3,n),OpSmb(n),ios(3,n),OpSmbS(n),
     3         IswSymms(n),ZMagS(n),RMagS(9,n))
      call ReallocSymm(NDim(KPhase),n,NLattVec(KPhase),NComp(KPhase),
     1                 NPhase,0)
      call SetLogicalArrayTo(BratSymmSub,NSymm(KPhase),.true.)
      IPocatek=0
2100  call GoToSubGrSelOp(0,IPocatek,n,TrMatPom,Grp,TrShift,ich)
      nss=NSymm(KPhase)
      if(ich.ne.0) go to 9000
2900  id=NextQuestId()
      Veta='Specify the output structure'
      xqd=WizardLength
      call FeQuestTitleMake(id,Veta)
      il=1
      tpom=5.
      Veta='Name of %structure'
      xpom=FeTxLengthUnder(Veta)+10.
      dpom=xqd-xpom-5.-100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      xpom=xpom+dpom+10.
      dpom=xqd-xpom-10.
      Veta='%Browse'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta=' '
      call FeQuestStringEdwOpen(EdwLastMade,Veta)
      MuzeBytTwin=.false.
      if(.not.isPowder) then
        do 2920i=2,indc
          do j=1,nss
            k=ipr(j)
            if(ips(k).ne.i) then
              cycle
            else if(MatRealEqUnitMat(rm6(1,k,1,KPhase),NDim(KPhase),
     1              .001)) then
              go to 2920
            endif
          enddo
          MuzeBytTwin=.true.
          exit
2920    continue
      endif
      xpom=7.
      tpom=xpom+CrwXd+10.
      if(MuzeBytTwin) then
        il=il+1
        Veta='Make as %twinned structure'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwTwin=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.true.)
      else
        nCrwTwin=0
      endif
      il=il+1
      Veta='Save the selected %coset representative as local operators'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwSaveSuperSymm=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,.false.)
      il=il+1
      xpom=tpom
      tpom=xpom+CrwXd+10.
      Veta='Generate restriction commands for subsequest refinement'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwGenRestrictions=CrwLastMade
      QuestCheck(id)=1
3000  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        FlnNew=EdwStringQuest(nEdwName)
        iFlnNew=idel(FlnNew)
        if(FlnNew.ne.' ') then
          if(StructureExists(FlnNew)) then
            if(FeYesNo(-1.,-1.,'The structure "'//
     1                 FlnNew(:iFlnNew)//
     2                  '" already exists, rewrite it?',0))
     3        QuestCheck(id)=0
          else
            QuestCheck(id)=0
          endif
        else
          call FeChybne(-1.,-1.,'you must define the name of '//
     1                  'structure.',' ',SeriousError)
        endif
        go to 3000
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwSaveSuperSymm) then
        if(CrwLogicQuest(nCrwSaveSuperSymm)) then
          call FeQuestCrwOpen(nCrwGenRestrictions,.true.)
        else
          call FeQuestCrwClose(nCrwGenRestrictions)
        endif
        go to 3000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        WizardMode=.false.
        t256=EdwStringQuest(nEdwName)
        call FeFileManager('Define name of the output structure',
     1                     t256,' ',1,.false.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwName,t256)
        WizardMode=.true.
        go to 3000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 3000
      endif
      if(ich.eq.0) then
        if(MuzeBytTwin) then
          MakeTwin=CrwLogicQuest(nCrwTwin)
        else
          MakeTwin=.false.
        endif
        SaveSuperSymm=CrwLogicQuest(nCrwSaveSuperSymm)
        if(SaveSuperSymm) then
          GenRestrictions=CrwLogicQuest(nCrwGenRestrictions)
        else
          GenRestrictions=.false.
        endif
      else if(ich.gt.0) then
        go to 2100
      else
        call FeQuestRemove(id)
        go to 9000
      endif
      NTwinO=NTwin
      if(NTwinO.eq.1) sctw(1,KDatBlock)=1.
      ntrans=indc-1
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      if(SaveSuperSymm) then
        if(allocated(rm6l)) deallocate(rm6l,rml,s6l,ISwSymmL,KwSymL,rtl,
     1                                 rc3l,rc4l,rc5l,rc6l,GammaIntL,
     2                                 LXYZMode,XYZMode,NXYZMode,
     3                                 MXYZMode)
        NSymmL(KPhase)=indc-1
        MaxNSymmL=max(indc-1,MaxNSymmL)
        if(MaxNSymmL.gt.0) then
          allocate(rm6l     (MaxNDim**2,MaxNSymmL  ,MaxNComp,NPhase),
     1             rml      (9         ,MaxNSymmL  ,MaxNComp,NPhase),
     2             s6l      (MaxNDim   ,MaxNSymmL  ,MaxNComp,NPhase),
     3             rtl      (36        ,MaxNSymmL  ,MaxNComp,NPhase),
     4             rc3l     (100       ,MaxNSymmL  ,MaxNComp,NPhase),
     5             rc4l     (225       ,MaxNSymmL  ,MaxNComp,NPhase),
     6             rc5l     (441       ,MaxNSymmL  ,MaxNComp,NPhase),
     7             rc6l     (784       ,MaxNSymmL  ,MaxNComp,NPhase),
     8             GammaIntL(9         ,MaxNSymmL  ,MaxNComp,NPhase),
     9             KwSymL  (mxw       ,MaxNSymmL   ,MaxNComp,NPhase),
     a             ISwSymmL           (MaxNSymmL   ,MaxNComp,NPhase),
     1             LXYZMode(3*(MaxNSymmL+1),NPhase),
     2             XYZMode (3*(MaxNSymmL+1),3*(MaxNSymmL+1),NPhase),
     3             NXYZMode(NPhase),MXYZMode(NPhase))
          call SetIntArrayTo(NXYZMode,NPhase,0)
          call SetIntArrayTo(MXYZMode,NPhase,0)
        endif
      endif
      do i=1,indc-1
        j=itrl(i+1)
        do 3100isw=1,NComp(KPhase)
          call CopyMat(rm6(1,j,isw,KPhase),TransMat(1,i,isw),
     1                 NDim(KPhase))
          call CopyVek( s6(1,j,isw,KPhase),TransVec(1,i,isw),
     1                 NDim(KPhase))
          TransZM(i)=ZMag(j,isw,KPhase)
          if(SaveSuperSymm) then
            call CopyMat(rm6(1,j,isw,KPhase),rm6l(1,i,isw,KPhase),
     1                   NDim(KPhase))
            call CopyVek( s6(1,j,isw,KPhase), s6l(1,i,isw,KPhase),
     1                   NDim(KPhase))
          endif
          if(isw.eq.1.and.MakeTwin) then
            do k=1,nss
              if(ips(k).le.i.and.
     1           Eqrv(rm6(1,j,1,KPhase),rm6(1,k,1,KPhase),NDimQ(KPhase),
     2                .001)) go to 3100
            enddo
            call MatBlock3(TransMat(1,i,isw),rmp,NDim(KPhase))
            do k=1,NTwinO
              NTwin=NTwin+1
              sctw(NTwin,KDatBlock)=sctw(k,KDatBlock)
              call multm(rtw(1,k),rmp,rtw(1,NTwin),3,3,3)
            enddo
          endif
3100    continue
      enddo
      Grupa(KPhase)=Grp
      if(Grp(1:1).ne.'?') Lattice(KPhase)=Grp(1:1)
      call CopyVek(TrShift,ShSg(1,KPhase),3)
      m=0
      do i=1,NSymm(KPhase)
        if(ips(i).eq.1) then
          m=m+1
          call CopySymmOperator(i,m,1)
        endif
      enddo
      NSymm(KPhase)=m
      if(CrlMagInver().gt.0) then
        NSymmN(KPhase)=m/2
      else
        NSymmN(KPhase)=m
      endif
      call CrlCheckSymmForLattVec(1)
      ForMagnetic=.false.
      go to 3200
      entry GoToSubGrMag(FileMag,indm,ScTwIn,NTwinIn)
      NTwinO=NTwinIn
      ForMagnetic=.true.
      FlnNew=FileMag
      iFlnNew=idel(FlnNew)
      MakeTwin=.not.isPowder
      GenRestrictions=.true.
      ind=indm
      indc=indm
3200  if(allocated(AtomOld)) deallocate(AtomOld)
      allocate(AtomOld(NAtAll))
      j=0
      do i=1,NAtInd
        if(kswa(i).ne.KPhase) cycle
        j=j+1
        AtomOld(j)=Atom(i)
      enddo
      if(ForMagnetic) then
        call CopyFile(fln(:ifln)//'.m44',FlnNew(:iFlnNew)//'.m40')
        if(isPowder)
     1    call CopyFile(fln(:ifln)//'.m41',FlnNew(:iFlnNew)//'.m41')
c        call iom50(0,0,FlnNew(:iFlnNew)//'.m50')
        call iom40Only(0,0,FlnNew(:iFlnNew)//'.m40')
        sctw(1:NTwin,KDatBlock)=sctwIn(1:NTwin)
      endif
      NAtOld=j
      call EM40SetTr(0)
      call EM40ExpandAll(ich)
      if(ich.ne.0) go to 9999
      if(MakeTwin) then
        pom=float(NTwinO)/float(NTwin)
        j=mxscu
3300    if(sc(j,1).le.0.) then
          j=j-1
          if(j.gt.1) go to 3300
        endif
        mxscutw=max(j+NTwin-1,6)
        mxscu=mxscutw-NTwin+1
        do i=1,NTwin
          sctw(i,KDatBlock)=sctw(i,KDatBlock)*pom
        enddo
      endif
      call CopyFile(fln(:ifln)//'.l51',FlnNew(:iFlnNew)//'.l51')
      call FindSmbSg(Grp,ChangeOrderNo,1)
      call iom50(1,0,FlnNew(:iFlnNew)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      call iom50(0,0,FlnNew(:iFlnNew)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      n=NAtInd+NAtMol
      allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
      NAtActive=0
      do i=1,NAtInd
        if(kswa(i).eq.KPhase) then
          NAtActive=NAtActive+1
          LAtActive(NAtActive)=.true.
          IAtActive(NAtActive)=i
        endif
      enddo
      if(NMolec.gt.0) then
        nn=0
        if(allocated(Names)) deallocate(Names)
        allocate(Names(NMolec*mxp))
        iak=NAtMolFrAll(KPhase)-1
        do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          iap=iak+1
          iak=iap+iam(im)/KPoint(im)-1
          do ia=iap,iak
            NAtActive=NAtActive+1
            LAtActive(NAtActive)=.true.
            IAtActive(NAtActive)=ia
          enddo
          iak=iak+(KPoint(im)-1)*(iak-iap+1)
          do j=1,mam(im)
            k=j+mxp*(im-1)
            nn=nn+1
            write(Cislo,FormI15) j
            call Zhusti(Cislo)
            Names(nn)=Molname(im)(:idel(Molname(im)))//'#'//
     1                Cislo(:idel(Cislo))
          enddo
        enddo
      endif
      call EM40MergeAtomsRun(.1,ich)
      call SetIntArrayTo(KiA(1,1),mxda*NAtInd,0)
      if(MagneticType(KPhase).gt.0) then
        NPolar=0
        do i=1,NAtInd
          if(kswa(i).eq.KPhase) then
            if(KUsePolar(i).gt.0) then
              NPolar=NPolar+1
              NamePolar(NPolar)=Atom(i)
            endif
          endif
        enddo
      endif
      call iom40(1,0,FlnNew(:iFlnNew)//'.m40')
      if(ExistFile(fln(:ifln)//'.m95')) then
        call CopyFile(fln(:ifln)//'.m95',FlnNew(:iFlnNew)//'.m95')
        call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
      endif
      flnOld=fln
      call DeletePomFiles
      fln=FlnNew
      ifln=iFlnNew
      call DeletePomFiles
      call ChangeUSDFile(fln,'opened','*')
      call iom50(0,0,fln(:ifln)//'.m50')
      Change=.false.
      if(GenRestrictions) then
        call RefOpenCommands
        lni=NextLogicNumber()
        open(lni,file=fln(:ifln)//'_restric.tmp')
        lno=NextLogicNumber()
        open(lno,file=fln(:ifln)//'_restric_new.tmp')
3400    read(lni,FormA,end=3420) t256
        write(lno,FormA) t256(:idel(t256))
        go to 3400
3420    call CloseIfOpened(lni)
      endif
      jto=0
      allocate(AtomAlreadyUsed(NAtCalc))
      call SetLogicalArrayTo(AtomAlreadyUsed,NAtCalc,.false.)
      do i=1,NAtOld
        AtKmen=AtomOld(i)
        idlKmen=idel(AtKmen)
        jfr=jto+1
        jto=jto+1
3450    if(EqIgCase(AtKmen,Atom(jto)(:idlKmen)).and.
     1     Atom(jto)(idlKmen+1:idlKmen+1).eq.'_') then
          jto=jto+1
          if(jto.le.NAtInd) go to 3450
        endif
        jto=jto-1
        if(jfr.eq.jto) then
          Atom(jfr)=AtKmen
          Change=.true.
        endif
        do j=jfr,jto
          if(AtomAlreadyUsed(j)) cycle
          if(jfr.ne.jto) call CrlAtomNamesSave(AtKmen,Atom(j),0)
          isw=iswa(j)
          do l=1,NSymmL(KPhase)
            xp=0.
            xp(1:3)=x(1:3,j)
            if(NDimI(KPhase).eq.1) then
              if(KFA(1,j).ne.0.and.KModA(1,j).ne.0) then
                xp(4)=ax(KModA(1,j),j)
              else if(KFA(2,j).ne.0.and.KModA(2,j).ne.0) then
                xp(4)=uy(1,KModA(2,j),j)
              endif
            endif
            call MultM(rm6l(1,l,isw,KPhase),xp,xpp,NDim(KPhase),
     1                 NDim(KPhase),1)
            call AddVek(s6l(1,l,isw,KPhase),xpp,xp,NDim(KPhase))
            k=j
3500        k=koinc(xp,x,k,jto,.1,dpom,isw,iswa)
            if(k.le.0) cycle
            if(NDimI(KPhase).eq.1) then
              x4p=0.
              if(KFA(1,k).ne.0.and.KModA(1,k).ne.0) then
                x4p=ax(KModA(1,k),k)
              else if(KFA(2,k).ne.0.and.KModA(2,k).ne.0) then
                x4p=uy(1,KModA(2,k),k)
              endif
              pom=xp(4)-x4p
              if(abs(pom-anint(pom)).gt..001) then
                k=k+1
                go to 3500
              endif
            endif
            write(Cislo,FormI15) l
            call Zhusti(Cislo)
            t256='restric '//Atom(j)(:idel(Atom(j)))//' 11 ls#'//
     1           Cislo(:idel(Cislo))
            if(k.ne.j) t256=t256(:idel(t256))//' '//
     1                      Atom(k)(:idel(Atom(k)))
            write(lno,FormA) t256(:idel(t256))
            AtomAlreadyUsed(j)=.true.
            AtomAlreadyUsed(k)=.true.
          enddo
        enddo
      enddo
      if(Allocated(AtomAlreadyUsed)) deallocate(AtomAlreadyUsed)
      if(Change) then
        if(MagneticType(KPhase).gt.0) then
          NPolar=0
          do i=1,NAtInd
            if(kswa(i).eq.KPhase) then
              if(KUsePolar(i).gt.0) then
                NPolar=NPolar+1
                NamePolar(NPolar)=Atom(i)
              endif
            endif
          enddo
        endif
        if(nor.gt.0) then
          do i=1,nor
            k=ktat(Atom,NAtInd,ora(i))
            if(k.le.0) then
              k=ktat(Atom,NAtInd,ora(i)(:idel(ora(i))-1))
              if(k.gt.0) ora(i)=Atom(k)
            endif
          enddo
        endif
        call iom40(1,0,Fln(:iFln)//'.m40')
      endif
      if(GenRestrictions) then
        close(lno)
        call MoveFile(fln(:ifln)//'_restric_new.tmp',
     1                fln(:ifln)//'_restric.tmp')
        call RefRewriteCommands(1)
        call iom40(1,0,Fln(:iFln)//'.m40')
      endif
      call CrlAtomNamesApply
      if(allocated(AtomOld)) deallocate(AtomOld)
      if(ForMagnetic) return
      if(ExistFile(fln(:ifln)//'.m95')) then
        call FeFillTextInfo('gotosubgr3.txt',0)
        call FeWizardTextInfo('INFORMATION',1,-1,1,ich)
        call FeQuestRemove(id)
        ExistM90=.false.
        call EM9CreateM90(0,ich)
        if(ich.ne.0) then
          call FeQuestRemove(id)
          go to 9999
        endif
      else
        call FeQuestRemove(id)
      endif
      call MatBlock3(TrMatPom,rmp,NDim(KPhase))
      if(.not.MatRealEqUnitMat(rmp,3,.001).or.
     1   .not.EqRV0(ShSg(1,KPhase),3,.0001)) then
        Veta='The following transformation can bring the structure '//
     1        'to the standard setting:'
        if(CrlAcceptTransformation(Veta,rmp,ShSg(1,KPhase),3,SmbABC,1))
     1    then
          if(.not.MatRealEqUnitMat(rmp,3,.001))
     1      call DRCellDefinedTrans(TrMatPom)
          if(.not.EqRV0(ShSg(1,KPhase),3,.0001)) then
            call CopyVek(ShSg(1,KPhase),TrShift,NDim(KPhase))
            call OriginDefinedShift(TrShift)
            if(NSymmL(KPhase).gt.0) then
              do i=1,NSymmL(KPhase)
                call CopyVek(s6l(1,i,1,KPhase),s6o,NDim(KPhase))
                call multm(rm6l(1,i,1,KPhase),TrShift,xp,NDim(KPhase),
     1                     NDim(KPhase),1)
                do j=1,NDim(KPhase)
                  s6l(j,i,1,KPhase)=s6l(j,i,1,KPhase)-xp(j)+TrShift(j)
                enddo
                call od0do1(s6l(1,i,1,KPhase),s6l(1,i,1,KPhase),
     1                      NDim(KPhase))
              enddo
              call iom50(1,0,Fln(:iFln)//'.m50')
            endif
          endif
        endif
      endif
      fln=FlnOld
      ifln=idel(fln)
      call ContinueWithNewStructure(FlnNew)
      go to 9999
9000  call DeletePomFiles
9999  deallocate(ips,ipr,rm6s,s6s,ipri,iopc,itrl,StSym,BratSymmSub,vt6s,
     1           io,OpSmb,ios,OpSmbS,IswSymms,ZMagS,RMagS)
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(allocated(TransMat)) call DeallocateTrans
      if(allocated(SelSubGr)) deallocate(SelSubGr,GrpSubGr,SysSubGr,
     1                                   NGrpSubGr,PorGrpSubGr)
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      UseTabs=UseTabsIn
      return
100   format(i1)
      end
