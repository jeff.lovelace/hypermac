      subroutine NormCentr(x)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(*),xp(6),xopt(6)
      call SetRealArrayTo(xopt,6,9999.)
      do 2000i=1,NLattVec(KPhase)
        call AddVek(x,vt6(1,i,1,KPhase),xp,NDim(KPhase))
        call od0do1(xp,xp,NDim(KPhase))
        do 1000j=1,NDim(KPhase)
          pom=xp(j)-xopt(j)
          if(abs(pom).lt..0001) then
            go to 1000
          else if(pom.lt.0.) then
            go to 1500
          else
            go to 2000
          endif
1000    continue
        go to 2000
1500    call CopyVek(xp,xopt,NDim(KPhase))
2000  continue
      call CopyVek(xopt,x,NDim(KPhase))
      return
      end
