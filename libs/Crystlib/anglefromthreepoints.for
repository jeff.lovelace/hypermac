      function AngleFromThreePoints(xc,xb1,xb2)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xc(3),xb1(3),xb2(3),u(3),ug(3),v(3),vg(3),w(3),wg(3)
      do i=1,3
        u(i)=xb1(i)-xc(i)
        v(i)=xb2(i)-xc(i)
        w(i)=xb2(i)-xb1(i)
      enddo
      call multm(MetTens(1,1,KPhase),u,ug,3,3,1)
      call multm(MetTens(1,1,KPhase),v,vg,3,3,1)
      call multm(MetTens(1,1,KPhase),w,wg,3,3,1)
      uu=scalmul(u,ug)
      vv=scalmul(v,vg)
      uv=scalmul(u,vg)
      if(abs(uv).gt..001) then
        AngleFromThreePoints=uv/sqrt(uu*vv)
        if(abs(AngleFromThreePoints).lt.1.) then
          AngleFromThreePoints=acos(AngleFromThreePoints)/ToRad
        else
          AngleFromThreePoints=180.
        endif
      else
        AngleFromThreePoints=90.
      endif
      return
      end
