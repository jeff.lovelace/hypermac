      subroutine TrSuper(VzitNovouIn,FlnNewIn)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'refine.cmn'
      include 'dist.cmn'
      include 'powder.cmn'
      include 'editm9.cmn'
      character*(*) FlnNewIn
      character*256 Veta,EdwStringQuest,FlnNew,FlnOld
      character*80  FormatOut,FormatIn
      character*8 atoms
      character*4 t4
      integer CrlMagCenter,VzitNovouIn,VzitNovou,UseTabsIn
      logical FeYesNo,StructureExists,EqIgCase,ExistFile,ForMagnetic,
     1        WizardModeOld
      dimension fc(3),xp(6),xpp(6),xs(6),betas(6),snwp(:),cswp(:),
     1          h(6),hp(6),c3s(10),c4s(15),c5s(21),c6s(28),
     2          NSupPom(3),hpp(6),difi(3),RPom(9),x4center(3),delta(3),
     3          sm0s(3),PomMat(36),sngc(:,:,:),csgc(:,:,:),src2(:,:),
     4          src3(:,:),src4(:,:),src5(:,:),src6(:,:),FPolA(:)
      allocatable sngc,csgc,src2,src3,src4,src5,src6,snwp,cswp,FPolA
      data difi/3*.01/
      ForMagnetic=.false.
      go to 1100
      entry TrSuperMag(VzitNovouIn,FlnNewIn)
      ForMagnetic=.true.
1100  UseTabsIn=UseTabs
      Klic=0
      VzitNovou=VzitNovouIn
      n=NComp(KPhase)
      allocate(src2(36,n))
      if(itfMax.gt.2) allocate(src3(100,n))
      if(itfMax.gt.3) allocate(src4(225,n))
      if(itfMax.gt.4) allocate(src5(441,n))
      if(itfMax.gt.5) allocate(src6(784,n))
      allocate(snwp(MaxUsedKwAll),cswp(MaxUsedKwAll),
     1         FPolA(2*MaxUsedKwAll+1))
      call srotb(RCommenI(1,1,KPhase),RCommenI(1,1,KPhase),src2(1,1))
      if(itfmax.gt.2) then
        call srotc(RCommenI(1,1,KPhase),3,src3(1,1))
        if(itfmax.gt.3) then
          call srotc(RCommenI(1,1,KPhase),4,src4(1,1))
          if(itfmax.gt.4) then
            call srotc(RCommenI(1,1,KPhase),5,src5(1,1))
            if(itfmax.gt.5) then
              call srotc(RCommenI(1,1,KPhase),6,src6(1,1))
            endif
          endif
        endif
      endif
      if(NComp(KPhase).gt.1) then
        do i=2,NComp(KPhase)
          call srotb(zsigi(1,i,KPhase),zsigi(1,i,KPhase),src2(1,i))
          if(itfmax.gt.2) then
            call srotc(zsigi(1,i,KPhase),3,src3(1,i))
            if(itfmax.gt.3) then
              call srotc(zsigi(1,i,KPhase),4,src4(1,i))
              if(itfmax.gt.4) then
                call srotc(zsigi(1,i,KPhase),5,src5(1,i))
                if(itfmax.gt.5) then
                  call srotc(zsigi(1,i,KPhase),6,src6(1,i))
                endif
              endif
            endif
          endif
        enddo
      endif
      go to 1300
      entry GoTo3d(FlnNewIn)
      Klic=1
      VzitNovou=2
      ForMagnetic=.false.
      go to 1300
      entry FromAmplimodes(FlnNewIn)
      Klic=2
      VzitNovou=2
      ForMagnetic=.false.
1300  FlnNew=FlnNewIn
      if(FlnNew.ne.' ') then
        IFlnNew=idel(FlnNew)
        go to 1420
      endif
1350  id=NextQuestId()
      il=1
      xqd=300.
      if(Klic.eq.0) then
        Veta='Name of the supercell structure'
      else if(Klic.eq.1) then
        Veta='Name of the 3d average structure'
      else
        Veta='Name of the transformed structure'
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      bpom=50.
      xpom=5.
      tpom=5.
      dpom=xqd-bpom-20.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FlnNew)
      Veta='%Browse'
      xpom=tpom+dpom+10.
      call FeQuestButtonMake(id,xpom,il,bpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1400  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        FlnNew=EdwStringQuest(nEdwName)
        call FeFileManager('Select output structure',FlnNew,'*',1,
     1                     .true.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwName,FlnNew)
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1400
      endif
      if(ich.eq.0) then
        FlnNew=EdwStringQuest(nEdwName)
        IFlnNew=idel(FlnNew)
        if(StructureExists(FlnNew)) then
          if(.not.FeYesNo(-1.,YBottomMessage,'The structure "'//
     1                    FlnNew(:IFlnNew)//
     2                    '" already exists, rewrite it?',0)) then
            call FeQuestRemove(id)
            go to 1350
          endif
        else if(FlnNew.eq.' ') then
          call FeChybne(-1.,YBottomMessage,'The empty string isn''t '//
     1                  'acceptable, try again.',' ',SeriousError)
          call FeQuestRemove(id)
          go to 1350
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
1420  if(Klic.eq.0) then
        call OpenFile(44,fln(:ifln)//'_m40.tmp','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        ScSupPom=1./DetRCommen(KPhase)
        i=NCommQProduct(1,KPhase)
        ScCommQ=1./(ScSupPom*float(i))
        ScSupPom=1./float(i)
        n=1
1450    if(i.ge.10) then
          n=n+1
          i=i/10
          go to 1450
        endif
        call trortho(0)
        call comsym(0,0,ich)
        if(ich.ne.0) go to 9100
        if(ngc(KPhase).gt.0) then
          allocate(sngc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc),
     1             csgc(MaxUsedKw(KPhase),ngc(KPhase),NAtCalc))
          call comexp(sngc,csgc,MaxUsedKw(KPhase),ngc(KPhase),NAtCalc)
        endif
        if(ErrFlag.ne.0) go to 9999
        idm=0
        do i=1,NAtCalc
          if(kswa(i).eq.KPhase) idm=max(idm,idel(atom(i)))
        enddo
        idm=min(idm,8-n)
        mc=0
        do i=1,NAtCalc
          if(ai(i).eq.0..or.kswa(i).ne.KPhase) cycle
          isw=iswa(i)
          itfi=itf(i)
          kmodsi=KModA(1,i)
          kmodxi=KModA(2,i)
          MagParI=MagPar(i)
          if(MagParI.gt.0) then
            MagParP=1
          else
            MagParP=0
          endif
          if(KFA(1,i).ne.0.and.kmodsi.ne.0) then
            kfsi=KFA(1,i)
          else
            kfsi=0
          endif
          if(KFA(2,i).ne.0.and.kmodxi.ne.0) then
            kfxi=KFA(2,i)
          else
            kfxi=0
          endif
          mxmod=0
          do j=1,itfi+1
            mxmod=max(mxmod,KModA(j,i))
          enddo
          if(MagParI.gt.0) mxmod=max(mxmod,MagParI-1)
          ic=0
          if(kfsi.ne.0) then
            k=kmodsi-NDimI(KPhase)
            do j=1,NDimI(KPhase)
              k=k+1
              x4center(j)=ax(k,i)
              if(NDimI(KPhase).gt.1) then
                delta(j)=ay(k,i)*.5
              else
                delta(j)=a0(i)*.5
              endif
            enddo
          else if(kfxi.ne.0) then
            x4center(1)=uy(1,kmodxi,i)
            delta(1)=uy(2,kmodxi,i)*.5
          endif
          call CopyVekI(NCommQ(1,isw,KPhase),NSupPom,3)
          do ica=1,NCommAdd(1,isw,KPhase)
            do ic3=1,NSupPom(3)
              if(NDimI(KPhase).ne.1) fc(3)=ic3-1
              do ic2=1,NSupPom(2)
                if(NDimI(KPhase).ne.1) fc(2)=ic2-1
                do ic1=1,NSupPom(1)
                  if(NDimI(KPhase).eq.1) then
                    pom=ic1-1
                    do j=1,3
                      fc(j)=VCommQ(j,1,isw,KPhase)*pom+
     1                      VCommAdd(j,1,isw,KPhase)*float(ica-1)
                    enddo
                  else
                    fc(1)=ic1-1
                  endif
                  mc=mc+1
                  ic=ic+1
                  do j=1,3
                    xs(j)=x(j,i)+fc(j)
                  enddo
                  call qbyx(fc,xs(4),isw)
                  call AddVek(qcnt(1,i),xs(4),xs(4),NDimI(KPhase))
                  call AddVek(xs(4),trez(1,isw,KPhase),xs(4),
     1                        NDimI(KPhase))
                  kk=1
                  do k=1,mxmod
                    if(TypeModFun(i).le.1) then
                      arg=0.
                      do n=1,NDimI(KPhase)
                        arg=arg+float(kw(n,k,KPhase))*xs(n+3)
                      enddo
                      arg=arg*pi2
                      snwp(k)=sin(arg)
                      cswp(k)=cos(arg)
                    else
                      if(k.eq.1) then
                        pom=xs(4)-x4center(1)
                        j=pom
                        if(pom.lt.0.) j=j-1
                        pom=pom-float(j)
                        if(pom.gt..5) pom=pom-1.
                        pom=pom/delta(1)
                        call GetFPol(pom,FPolA,2*mxmod+1,TypeModFun(i))
                      endif
                      kk=kk+1
                      snwp(k)=FPolA(kk)
                      kk=kk+1
                      cswp(k)=FPolA(kk)
                    endif
                  enddo
                  occx=1.
                  do k=1,kmodxi
                    if(kfxi.gt.0.and.k.eq.kmodxi) then
                      pom=xs(4)-x4center(1)
                      j=pom
                      if(pom.lt.0.) j=j-1
                      pom=pom-float(j)
                      if(pom.gt..5) pom=pom-1.
                      if(kfxi.eq.1) then
                        znak=pom/delta(1)
                        if(pom.ge.-delta(1).and.pom.le.delta(1)) then
                          occx=1.
                        else
                          occx=0.
                        endif
                      else
                        if(pom.ge.-delta(1).and.pom.le.delta(1)) then
                          if(kfxi.eq.2) then
                            znak=pom/delta(1)
                            occx=1.
                          else
                            znak=1.
                          endif
                        else
                          if(kfxi.eq.2) then
                            pom=pom-.5
                            if(pom.lt.-.5) pom=pom+1.
                            if(pom.ge.-delta(1).and.pom.le.delta(1))
     1                        then
                              znak=-pom/delta(1)
                            else
                              occx=0.
                            endif
                          else
                            znak=-1.
                          endif
                        endif
                      endif
                      do j=1,3
                        xs(j)=xs(j)+ux(j,k,i)*znak
                      enddo
                    else
                      sn=snwp(k)
                      cs=cswp(k)
                      do j=1,3
                        xs(j)=xs(j)+ux(j,k,i)*sn+uy(j,k,i)*cs
                      enddo
                    endif
                  enddo
                  occo=0.
                  if(kmodsi.gt.0) then
                    if(kfsi.eq.0) occo=a0(i)-occx
                    kk=0
                    do k=1,kmodsi
                      if(kfsi.ne.0.and.k.gt.kmodsi-NDimI(KPhase)) then
                        kk=kk+1
                        pom=xs(kk+3)-x4center(kk)
                        j=pom
                        if(pom.lt.0.) j=j-1
                        pom=pom-float(j)
                        if(pom.gt..5) pom=pom-1.
                        if(pom.ge.-delta(kk)-.0001.and.
     1                     pom.le. delta(kk)+.0001) then
                          occx=1.
                        else
                          occx=0.
                          go to 1550
                        endif
                      else
                        sn=snwp(k)
                        cs=cswp(k)
                        occo=occo+ax(k,i)*sn+ay(k,i)*cs
                      endif
                    enddo
                  endif
1550              if(occx.gt.0.) then
                    ais=ai(i)*(occx+occo)
                  else
                    ais=0.
                    cycle
                  endif
                  itfip=itfi
                  if(MagParI.gt.0) itfip=itfip+1
                  do n=2,itfip
                    if(n.gt.itfi) then
                      nrank=3
                      kmodp=MagParI-1
                    else
                      nrank=TRank(n)
                      kmodp=KModA(n+1,i)
                    endif
                    if(n.gt.itfi) then
                      if(KUsePolar(i).gt.0)
     1                  call ShpCoor2Fract(sm0(1,i),RPom)
                      call CopyVek(sm0(1,i),sm0s,nrank)
                      if(KUsePolar(i).gt.0) call Fract2ShpCoor(sm0(1,i))
                    else if(n.eq.2) then
                      call CopyVek(beta(1,i),betas,nrank)
                    else if(n.eq.3) then
                      call CopyVek(c3(1,i),c3s,nrank)
                    else if(n.eq.4) then
                      call CopyVek(c4(1,i),c4s,nrank)
                    else if(n.eq.5) then
                      call CopyVek(c5(1,i),c5s,nrank)
                    else
                      call CopyVek(c6(1,i),c6s,nrank)
                    endif
                    do k=1,kmodp
                      sn=snwp(k)
                      cs=cswp(k)
                      do j=1,nrank
                        if(n.gt.itfi) then
                          if(KUsePolar(i).gt.0) then
                            call ShpCoor2Fract(smx(1,k,i),RPom)
                            call ShpCoor2Fract(smy(1,k,i),RPom)
                          endif
                          sm0s(j)=sm0s(j)+smx(j,k,i)*sn+smy(j,k,i)*cs
                          if(KUsePolar(i).gt.0) then
                            call Fract2ShpCoor(smx(1,k,i))
                            call Fract2ShpCoor(smy(1,k,i))
                          endif
                        else if(n.eq.2) then
                          betas(j)=betas(j)+bx(j,k,i)*sn+by(j,k,i)*cs
                        else if(n.eq.3) then
                          c3s(j)=c3s(j)+c3x(j,k,i)*sn+c3y(j,k,i)*cs
                        else if(n.eq.4) then
                          c4s(j)=c4s(j)+c4x(j,k,i)*sn+c4y(j,k,i)*cs
                        else if(n.eq.5) then
                          c5s(j)=c5s(j)+c5x(j,k,i)*sn+c5y(j,k,i)*cs
                        else
                          c6s(j)=c6s(j)+c6x(j,k,i)*sn+c6y(j,k,i)*cs
                        endif
                      enddo
                    enddo
                  enddo
                  if(itfi.le.1) then
                    betas(1)=beta(1,i)
                    call SetRealArrayTo(betas(2),5,0.)
                  endif
                  if(isw.eq.1) then
                    call MultM(RCommenI(1,1,KPhase),xs,xpp,3,3,1)
                  else
                    call qbyx(xs,xs(4),isw)
                    do k=4,NDim(KPhase)
                      xs(k)=xs(k)+trez(k-3,isw,KPhase)
                    enddo
                    call multm(ZVi(1,isw,KPhase),xs,xp,NDim(KPhase),
     1                         NDim(KPhase),1)
                    call MultM(RCommenI(1,1,KPhase),xp,xpp,3,3,1)
                  endif
                  call od0do1(xpp,xpp,3)
                  do n=2,itfip
                    if(n.gt.itfi) then
                      nrank=3
                    else
                      nrank=TRank(n)
                    endif
                    if(isw.ne.1) then
                      if(n.eq.2) then
                        call multm(src2(1,isw),betas,PomMat,nrank,
     1                             nrank,1)
                        call CopyVek(PomMat,betas,nrank)
                      else if(n.eq.3) then
                        call multm(src3(1,isw),c3s,PomMat,nrank,nrank,
     1                             1)
                        call CopyVek(PomMat,c3s,nrank)
                      else if(n.eq.4) then
                        call multm(src4(1,isw),c4s,PomMat,nrank,nrank,
     1                             1)
                        call CopyVek(PomMat,c4s,nrank)
                      else if(n.eq.5) then
                        call multm(src5(1,isw),c5s,PomMat,nrank,nrank,
     1                             1)
                        call CopyVek(PomMat,c5s,nrank)
                      else if(n.eq.6) then
                        call multm(src6(1,isw),c6s,PomMat,nrank,nrank,
     1                             1)
                        call CopyVek(PomMat,c6s,nrank)
                      endif
                    endif
                    if(n.gt.itfi) then
                      call MultM(RCommenI(1,1,KPhase),sm0s,PomMat,nrank,
     1                           nrank,1)
                      call CopyVek(PomMat,sm0s,nrank)
                    else if(n.eq.2) then
                      call multm(src2(1,1),betas,PomMat,nrank,nrank,1)
                      call CopyVek(PomMat,betas,nrank)
                    else if(n.eq.3) then
                      call multm(src3(1,1),c3s,PomMat,nrank,nrank,1)
                      call CopyVek(PomMat,c3s,nrank)
                    else if(n.eq.4) then
                      call multm(src4(1,1),c4s,PomMat,nrank,nrank,1)
                      call CopyVek(PomMat,c4s,nrank)
                    else if(n.eq.5) then
                      call multm(src5(1,1),c5s,PomMat,nrank,nrank,1)
                      call CopyVek(PomMat,c5s,nrank)
                    else if(n.eq.6) then
                      call multm(src6(1,1),c6s,PomMat,nrank,nrank,1)
                      call CopyVek(PomMat,c6s,nrank)
                    endif
                  enddo
                  write(t4,'(''-'',i3)') ic
                  call zhusti(t4)
                  atoms=atom(i)(:min(idel(atom(i)),idm))//t4(:idel(t4))
                  write(44,100) atoms,isf(i),itfi,MagParP,KUsePolar(i),
     1                          ais,(xpp(j),j=1,3),betas
                  if(itfi.gt.2) then
                    write(44,102) c3s
                    if(itfi.gt.3) then
                      write(44,102) c4s
                      if(itfi.gt.4) then
                        write(44,102) c5s
                        if(itfi.gt.5) write(44,102) c6s
                      endif
                    endif
                  endif
                  if(MagParP.gt.0) write(44,102) sm0s
                enddo
              enddo
            enddo
          enddo
        enddo
        if(NMolecLenAll(KPhase).gt.0) then
          call FeChybne(-1.,-1.,'atoms of molecule(s) will be '//
     1                  'transformed to atomic part.',' ',Warning)
          call MolSun(NMolecToAll(KPhase)+1,NMolec,NMolecFrAll(KPhase))
          call SetIntArrayTo(NMolecLen(1,KPhase),3,0)
          call EM40UpdateMolecLimits
          call AtSun(NAtMolToAll(KPhase)+1,NAtAll,NAtMolFrAll(KPhase))
          call SetIntArrayTo(NAtIndLen(1,KPhase),3,0)
          call SetIntArrayTo(NAtPosLen(1,KPhase),3,0)
          call SetIntArrayTo(NAtMolLen(1,KPhase),3,0)
          call EM40UpdateAtomLimits
        endif
      else if(Klic.eq.1) then
        do j=1,NComp(KPhase)
          do i=NAtIndFr(j,KPhase),NAtMolTo(j,KPhase)
            if(i.gt.NAtIndTo(j,KPhase).and.i.lt.NAtMolFr(j,KPhase))
     1        cycle
            if(KModA(1,i).gt.0) ai(i)=ai(i)*a0(i)
            call SetIntArrayTo(KModA(1,i),7,0)
            call SetIntArrayTo(KFA(1,i),7,0)
            MagPar(i)=min(MagPar(i),1)
          enddo
        enddo
        do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          do j=1,mam(i)
            ji=j+mxp*(i-1)
            if(KModM(1,ji).gt.0) aimol(ji)=aimol(ji)*a0m(ji)
            call SetIntArrayTo(KModM(1,ji),3,0)
            call SetIntArrayTo(KFM(1,ji),3,0)
          enddo
        enddo
      else
        NAtXYZMode=0
        NXYZAMode=0
        NAtMagMode=0
        NMagAMode=0
      endif
      if(.not.ExistM95) go to 2500
      FormatOut=Format95
      FormatIn =Format95
      FormatOut(5:5)='3'
      call iom95(0,fln(:ifln)//'.m95')
      NDimOld=NDim(KPhase)
      do KRefB=1,NRefBlock
        call OpenRefBlockM95(95,KRefB,fln(:ifln)//'.m95')
        if(ErrFlag.ne.0) go to 2090
        write(Cislo,'(''.l'',i2)') KRefB
        if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
        RefBlockFileName=FlnNew(:idel(FlnNew))//Cislo(:idel(Cislo))
        call OpenFile(96,RefBlockFileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 2090
        if(DifCode(KRefB).le.100) then
          NRef95(KRefB)=0
          NLines95(KRefB)=0
2010      NDim(KPhase)=NDimOld
          NDimQ(KPhase)=NDim(KPhase)**2
          NDimI(KPhase)=NDim(KPhase)-3
          MaxNDim=NDimOld
          call DRGetReflectionFromM95(95,iend,ich)
          if(ich.ne.0) go to 2090
          if(iend.ne.0) go to 2090
          iatw=iabs(iflg(2))
          if(Klic.eq.0) then
            do i=1,NDim(KPhase)
              hp(i)=ih(i)
            enddo
            call Multm(hp,trmp,h,1,NDim(KPhase),NDim(KPhase))
            mmax=1
            do isw=1,NComp(KPhase)
              call Multm(h,zvi(1,isw,KPhase),hpp,1,NDim(KPhase),
     1                   NDim(KPhase))
              do i=4,NDim(KPhase)
                mmax=max(iabs(nint(hpp(i))),mmax)
              enddo
            enddo
            do i=1,NDimI(KPhase)
              ihp(i)=nint(h(i))
            enddo
            do i=1,3
              do j=1,NDimI(KPhase)
                h(i)=h(i)+qu(i,j,1,KPhase)*h(j+3)
              enddo
            enddo
            call multm(h,rtwi(1,iatw),hp,1,3,3)
            call IndFromIndReal(hp,mmax,difi,ih,itw,iswp,-1.,
     1                          CheckExtRefNo)
            if(iswp.le.0) go to 2010
            do i=1,3
              h(i)=ih(i)
              do j=1,NDimI(KPhase)
                h(i)=h(i)+qu(i,j,1,KPhase)*float(ih(j+3))
              enddo
            enddo
            iflg(2)=itw
            call MultM(h,RCommen(1,1,KPhase),hp,1,3,3)
            do j=1,3
              ih(j)=nint(hp(j))
              if(abs(hp(j)-float(ih(j))).gt..001) then
                if(NInfo.lt.10) then
                  Ninfo=Ninfo+1
                  TextInfo(Ninfo)='Rounding error for reflection : '
                  write(TextInfo(Ninfo)(idel(TextInfo(Ninfo))+2:),
     1                  '(6i4)') (ih(i),i=1,NDim(KPhase))
                else if(Ninfo.eq.10) then
                  Ninfo=11
                  TextInfo(Ninfo)='                   ... and more ... '
                endif
                go to 2010
              endif
            enddo
          else if(Klic.eq.1) then
            call MultMIRI(ih,trmp,ihp,1,NDim(KPhase),NDim(KPhase))
            do i=4,NDim(KPhase)
              if(ihp(i).ne.0) go to 2010
            enddo
            call CopyVekI(ihp,ih,NDim(KPhase))
          endif
          pom1=corrf(1)*corrf(2)
          ri=ri*pom1
          rs=rs*pom1
          corrf(1)=1.
          corrf(2)=1.
          NProf=0
          NDim(KPhase)=3
          NDimI(KPhase)=0
          NDimQ(KPhase)=9
          maxNDim=3
          Format95=FormatOut
          call DRPutReflectionToM95(96,nl)
          NRef95(KRefB)=NRef95(KRefB)+1
          NLines95(KRefB)=NLines95(KRefB)+nl
          NDim(KPhase)=NDimOld
          Format95=FormatIn
          go to 2010
        else
2070      read(95,FormA,end=2090) Veta
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo,'data')) go to 2090
          write(96,FormA) Veta(:idel(Veta))
          NRef95(KDatBlock)=NRef95(KDatBlock)+1
          go to 2070
        endif
2090    call CloseIfOpened(95)
        call CloseIfOpened(96)
        Veta='?The strucure created from "'//fln(:ifln)//'"'
        if(Klic.eq.0) then
          SourceFileRefBlock(KRefB)=Veta(:idel(Veta))//' as a supercell'
        else if(Klic.eq.1) then
          SourceFileRefBlock(KRefB)=Veta(:idel(Veta))//' as 3d average'
        endif
        if(Klic.ne.2) then
          SourceFileDateRefBlock(KRefB)='?'
          SourceFileTimeRefBlock(KRefB)='?'
          UseTrRefBlock(KRefB)=.false.
          NDim95(KRefB)=3
          CellReadIn(KRefB)=.false.
        endif
        if(NLines95(KRefB).lt.0) NLines95(KRefB)=NRef95(KRefB)
      enddo
2500  if(Klic.eq.0) then
        call SuperSGToSuperCellSG(ich)
        if(ich.ne.0) go to 9100
        call UnitMat(PomMat,NDim(KPhase))
        call DRMatTrCell(RCommen(1,1,KPhase),CellPar(1,1,KPhase),PomMat)
        if(ExistM95)
     1    call DRMatTrCell(RCommen(1,1,KPhase),CellRefBlock(1,0),PomMat)
        if(isPowder)
     1    call DRMatTrCell(RCommen(1,1,KPhase),CellPwd(1,KPhase),PomMat)
        NUnits(KPhase)=NUnits(KPhase)*NCommQProduct(1,KPhase)
        if(NTwin.gt.1) then
          do i=1,NTwin
            call MultM(RCommenI(1,1,KPhase),rtw(1,i),PomMat,3,3,3)
            call MultM(PomMat,RCommen(1,1,KPhase),rtw(1,i),3,3,3)
          enddo
        endif
      else if(Klic.eq.1) then
        do i=1,NSymm(KPhase)
          call CopyMat(rm(1,i,1,KPhase),rm6(1,i,1,KPhase),3)
        enddo
      endif
      NDim(KPhase)=3
      NDimQ(KPhase)=9
      NDimI(KPhase)=0
      KCommen(KPhase)=0
      ICommen(KPhase)=0
      KCommenMax=0
      NComp(KPhase)=1
      call CrlOrderMagSymmetry
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      call CopyFile(fln(:ifln)//'.l51',FlnNew(:IFlnNew)//'.l51')
      call iom50(1,0,flnNew(:idel(flnNew))//'.m50')
      call iom50(0,0,flnNew(:idel(flnNew))//'.m50')
      if(MagneticType(KPhase).gt.0.and.CrlMagCenter().gt.0) then
        aiCorr=.5
      else
        aiCorr=1.
      endif
      if(Klic.eq.0) then
        rewind 44
        do j=1,NDatBlock
          if(iabs(DataType(j)).eq.2) then
            n=1
          else
            n=mxscu
          endif
          do i=1,n
            sc(i,j)=sc(i,j)*ScSupPom
          enddo
        enddo
        n=NAtIndFrAll(KPhase)-1
        nl=NAtIndToAll(KPhase)
        if(n.gt.0) then
          PrvniKi=PrvniKiAtomu(n)+mxda
        else
          PrvniKi=1
        endif
        nn=0
3100    read(44,100,end=3200) atoms,isfn,itfn,MagParN,KUsePolarN,ais,
     1                        (xs(i),i=1,3),betas
        nn=nn+1
!   Vubec nevim proc to tam bylo !!!!!
!        ais=ais*ScCentrGC*aiCorr
        ais=ais*aiCorr
        i=1
3170    i=koinc(xs,x,i,n,.1,dist,1,iswa)
        if(i.gt.0) then
          if(isf(i).ne.isfn) then
            if(i.lt.n) then
              i=i+1
              go to 3170
            else
              i=0
            endif
          endif
        endif
        if(i.gt.0) then
          ai(i)=ai(i)+ais
          if(itfn.gt.2) then
            read(44,102) c3s
            if(itfn.gt.3) then
              read(44,102) c4s
              if(itfn.gt.4) then
                read(44,102) c5s
                if(itfn.gt.5) then
                  read(44,102) c6s
                endif
              endif
            endif
          endif
          if(MagParN.gt.0) read(44,102)
        else
          if(n.ge.nl) then
            if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
            call AtSun(n+1,NAtAll,n+2)
            NAtIndLen(1,KPhase)=NAtIndLen(1,KPhase)+1
            call EM40UpdateAtomLimits
          endif
          n=n+1
          PrvniKiAtomu(n)=PrvniKi
          DelkaKiAtomu(n)=max(TRankCumul(itfn),10)
          PrvniKi=PrvniKi+DelkaKiAtomu(n)
          call SetIntArrayTo(KiA(1,n),DelkaKiAtomu(n),0)
          write(t4,'(''-'',i3)') ic
          call zhusti(t4)
          call SetBasicKeysForAtom(n)
          atom(n)=atoms
          isf(n)=isfn
          itf(n)=itfn
          kswa(n)=KPhase
          MagPar(n)=MagParN
          KUsePolar(n)=KUsePolarN
          ai(n)=ais
          call CopyVek(xs,x(1,n),3)
          k=0
          if(NDimI(KPhase).eq.1) then
            if(KFA(1,n).ne.0) then
              xp(1)=ax(KModA(1,n),n)
              k=1
            else
              xp(1)=uy(1,KModA(2,n),n)
              k=1
            endif
          endif
          call SpecPos(x(1,n),xp,k,iswa(n),.01,i)
          call CopyVek(betas,beta(1,n),6)
          if(itfn.gt.2) then
            read(44,102)(c3(i,n),i=1,10)
            call SetRealArrayTo(sc3(1,n),10,0.)
            if(itfn.gt.3) then
              read(44,102)(c4(i,n),i=1,15)
              call SetRealArrayTo(sc4(1,n),15,0.)
              if(itfn.gt.4) then
                read(44,102)(c5(i,n),i=1,21)
                call SetRealArrayTo(sc5(1,n),21,0.)
                if(itfn.gt.5) then
                  read(44,102)(c6(i,n),i=1,28)
                  call SetRealArrayTo(sc6(1,n),28,0.)
                endif
              endif
            endif
          endif
          if(MagParN.gt.0) then
            read(44,102)(sm0(i,n),i=1,3)
            call SetRealArrayTo(ssm0(1,n),3,0.)
          endif
        endif
        go to 3100
3200    close(44,status='delete')
        if(n.gt.0) then
          NAtIndFr(1,KPhase)=1
          NAtIndLen(1,KPhase)=n
          NAtIndTo(1,KPhase)=n
          NAtIndLen(2,KPhase)=0
          NAtIndLen(3,KPhase)=0
          nor=0
          NPolynom=0
          if(MagneticType(KPhase).gt.0) then
           NPolar=0
           do i=1,NAtInd
             if(kswa(i).eq.KPhase) then
               if(KUsePolar(i).gt.0) then
                 NPolar=NPolar+1
                 NamePolar(NPolar)=Atom(i)
                 call Fract2ShpCoor(sm0(1,i))
                endif
             endif
            enddo
          endif
        else
          call DeleteFile(FlnNew(:IFlnNew)//'.m40')
          call SetBasicM40(.true.)
        endif
        call iom40(1,0,FlnNew(:IFlnNew)//'.m40')
      else
        nor=0
        NPolynom=0
        if(MagneticType(KPhase).gt.0) NPolar=0
        call iom40(1,0,FlnNew(:IFlnNew)//'.m40')
      endif
      call iom40(0,0,FlnNew(:IFlnNew)//'.m40')
      Veta=Fln
      Fln=FlnNew
      ifln=idel(Fln)
      if(ExistFile(fln(:ifln)//'.m40')) then
        call RefOpenCommands
        j=NactiInt+NactiReal+21
        if(KLic.ne.2) then
          do i=j,j+8
            call DeleteFile(Fln(:ifln)//'_'//
     1                      NactiKeywords(i)(:idel(NactiKeywords(i)))//
     2                      '.tmp')
          enddo
          NacetlInt(nCmdkim)=DefIntRefine(nCmdkim)
          NacetlInt(nCmdkic)=DefIntRefine(nCmdkic)
          NacetlInt(nCmdmetoda)=DefIntRefine(nCmdmetoda)
          NacetlInt(nCmdngrid)=DefIntRefine(nCmdngrid)
          NacetlInt(nCmdiover)=DefIntRefine(nCmdiover)
          NacetlReal(nCmdDifBess)=DefRealRefine(nCmdDifBess)
          NacetlReal(nCmdOverDif)=DefRealRefine(nCmdOverDif)
          call RefRewriteCommands(1)
          allocate(xdst(3,NAtCalc),BetaDst(6,NAtCalc),dxm(NAtCalc),
     1             BratPrvni(NAtCalc),BratDruhyATreti(NAtCalc),
     2             aselPrvni(NAtCalc),aselDruhyATreti(NAtCalc))
          call DistOpenCommands
          if(ErrFlag.ne.0) go to 9999
          do i=nCmdTorsion,nCmdbondval
            call DeleteFile(Fln(:IFln)//'_'//
     1                      NactiKeywords(i)(:idel(NactiKeywords(i)))//
     2                      '.tmp')
          enddo
          call SetLogicalArrayTo(BratPrvni,NAtCalc,.true.)
          call SetLogicalArrayTo(BratDruhyATreti,NAtCalc,.true.)
          call DistRewriteCommands(1)
          deallocate(xdst,BetaDst,dxm,BratPrvni,BratDruhyATreti,
     1               aselPrvni,aselDruhyATreti)
        endif
      endif
      if(ExistM95) then
        FlnOld=Fln
        Fln=FlnNew
        iFln=idel(Fln)
        if(Klic.ne.2) then
          call CopyVek(CellPar(1,1,KPhase),CellRefBlock(1,0),6)
          call UnitMat(TrMP,3)
        endif
        SilentRun=NRefBlock.le.1.or.ForMagnetic
        WizardModeOld=WizardMode
        WizardMode=.false.
        call iom95(1,fln(:ifln)//'.m95')
        call CompleteM95(0)
        ExistM90=.false.
        call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
        UpdateAnomalous=.false.
        if(ParentM90.ne.' '.and.ForMagnetic) call iom90(0,ParentM90)
        call EM9CreateM90(0,ich)
        call iom90(0,fln(:ifln)//'.m90')
        UpdateAnomalous=.true.
        SilentRun=.false.
        WizardMode=WizardModeOld
        Fln=FlnOld
        iFln=idel(Fln)
      endif
      if(VzitNovou.eq.1) then
        call DeletePomFiles
        call ExtractDirectory(FlnNew,Veta)
        i=FeChdir(Veta)
        call ExtractFileName(FlnNew,fln)
        ifln=idel(fln)
        call FeGetCurrentDir
        call FeBottomInfo(' ')
        call DeletePomFiles
        call ChangeUSDFile(fln,'opened','*')
      else
        Fln=Veta
        ifln=idel(Fln)
        if(VzitNovou.eq.2)
     1    call ContinueWithNewStructure(FlnNew)
      endif
      go to 9999
9100  call DeleteFile(FlnNew(:idel(FlnNew))//'.m40')
      call DeleteFile(FlnNew(:idel(FlnNew))//'.m41')
      call DeleteFile(FlnNew(:idel(FlnNew))//'.m50')
      call DeleteFile(FlnNew(:idel(FlnNew))//'.m90')
      call DeleteFile(FlnNew(:idel(FlnNew))//'.m95')
9999  call DeleteFile(fln(:ifln)//'_m40.tmp')
      ErrFlag=0
      if(allocated(sngc)) deallocate(sngc,csgc)
      if(allocated(src2)) deallocate(src2)
      if(allocated(src3)) deallocate(src3)
      if(allocated(src4)) deallocate(src4)
      if(allocated(src5)) deallocate(src5)
      if(allocated(src6)) deallocate(src6)
      if(allocated(snwp)) deallocate(snwp,cswp,FPolA)
      call DeleteFile(PreviousM40)
      if(ExistPowder) call DeleteFile(PreviousM41)
      call DeleteFile(PreviousM50)
      UseTabs=UseTabsIn
      return
100   format(a8,4i4,4f9.6/6f9.6)
102   format(6f9.6)
      end
