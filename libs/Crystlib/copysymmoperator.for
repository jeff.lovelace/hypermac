      subroutine CopySymmOperator(i,j,isw)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      call CopyMat(rm6(1,i,isw,KPhase),rm6(1,j,isw,KPhase),NDim(KPhase))
      call CopyVek(s6(1,i,isw,KPhase),s6(1,j,isw,KPhase),NDim(KPhase))
      call MatBlock3(rm6(1,j,isw,KPhase),rm(1,j,isw,KPhase),
     1               NDim(KPhase))
      ISwSymm(j,isw,KPhase)=ISwSymm(i,isw,KPhase)
      ZMag(j,isw,KPhase)=ZMag(i,isw,KPhase)
      call CopyMat(RMag(1,i,isw,KPhase),RMag(1,j,isw,KPhase),3)
      call CodeSymm(rm6(1,j,isw,KPhase),s6(1,j,isw,KPhase),
     1              symmc(1,j,isw,KPhase),0)
      return
      end
