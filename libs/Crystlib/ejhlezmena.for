      subroutine EjhleZmena(p1,p2,ia,m,ip,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 t80,s80
      character*12 At,Param
      ich=1
      call kdoco(ip,At,Param,0,pom,spom)
      if(lite(KPhase).eq.0) then
        call EM40SwitchBetaU(0,ia)
        call EM40SwitchBetaU(0,m)
      endif
      t80=Param(:idel(Param))//'['//At(:idel(At))//']'
      write(s80,'(''The value : '',f9.6,'' should be changed to : '',
     1            f9.6)') p1,p2
      call FeChybne(-1.,-1.,'the parameter "'//t80(:idel(t80))//
     1              '" contradicts to the site symmetry.',s80,
     2              SeriousError)
      if(lite(KPhase).eq.0) then
        call EM40SwitchBetaU(1,ia)
        call EM40SwitchBetaU(1,m)
      endif
      return
      end
