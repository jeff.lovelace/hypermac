      subroutine CyclicRefGraph
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      character*17 :: Labels(5) =
     1              (/'%Quit     ','%Print    ','%Save     ',
     2                '%New graph','%Options  '/)
      character*6 :: MenuC(7) = (/'a     ',
     1                            'b     ',
     2                            'c     ',
     3                            'alpha ',
     4                            'beta  ',
     5                            'gamma ',
     6                            'volume'/)
      integer :: NCellPar=1
      logical GraphExists,CrwLogicQuest
      real, allocatable  :: XGrP(:),YGrP(:)
      real xpp(3),xo(3)
      call GetPureFileName(CyclicRefMasterFile,fln)
      ifln=idel(fln)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(120.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,5
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtNew=ButtonLastMade
        else if(i.eq.5) then
          nButtOptions=ButtonLastMade
        endif
        if(i.eq.3.or.i.eq.5) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      GraphExists=.false.
      go to 1200
1100  if(allocated(XGrP)) deallocate(XGrP,YGrP)
      allocate(XGrP(NCyclicRefFile),YGrP(NCyclicRefFile))
      NGrP=0
      call FeFlowChartOpen(-1.,-1.,1,NCyclicRefFile,'Reading files',' ',
     1                     ' ')
      iz=0
      do i=1,NCyclicRefFile
        call FeFlowChartEvent(iz,is)
        if(is.ne.0) then
          call FeBudeBreak
          if(ErrFlag.ne.0) then
            ErrFlag=0
            call FeClearGrWin
            GraphExists=.false.
            go to 1200
          endif
        endif
        if(CyclicRefUse(i).le.0) cycle
        call GetPureFileName(CyclicRefFile(i),Veta)
        call iom50(0,0,Veta(:idel(Veta))//'.m50')
        NGrP=NGrP+1
        XGrP(NGrP)=CyclicRefParam(i)
        if(NCellPar.le.6) then
          YGrP(NGrP)=CellPar(NCellPar,1,KPhase)
        else if(NCellPar.eq.7) then
          csa=cos(CellPar(4,1,KPhase)*ToRad)
          csb=cos(CellPar(5,1,KPhase)*ToRad)
          csg=cos(CellPar(6,1,KPhase)*ToRad)
          YGrP(NGrP)=CellPar(1,1,KPhase)*CellPar(2,1,KPhase)*
     1               CellPar(3,1,KPhase)*
     2               sqrt(1.-csa**2-csb**2-csg**2+2.*csa*csb*csg)
        endif
        if(NGrP.eq.1) then
          XMin=XGrP(NGrP)
          XMax=XGrP(NGrP)
          YMin=YGrP(NGrP)
          YMax=YGrP(NGrP)
        else
          XMin=min(XMin,XGrP(NGrP))
          XMax=max(XMax,XGrP(NGrP))
          YMin=min(YMin,YGrP(NGrP))
          YMax=max(YMax,YGrP(NGrP))
        endif
      enddo
      if(abs(YMax-YMin).le.0.) then
        YMin=YMin*0.99
        YMax=YMax*1.01
      endif
      call FeFlowChartRemove
      HardCopy=0
1150  call FeHardCopy(HardCopy,'open')
      if(HardCopy.eq.HardCopyNum) then
        do i=1,NGrP
          write(85,'(2e15.5)') XGrP(i),YGrP(i)
        enddo
      else if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                             HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        xomn=XMin
        xomx=XMax
        yomn=YMin
        yomx=YMax
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        if(CyclicRefType.eq.1) then
          Veta='Temp'
        else if(CyclicRefType.eq.2) then
          Veta='P'
        else if(CyclicRefType.eq.3) then
          Veta='Time'
        endif
        Veta=Veta(:idel(Veta)+1)//'['//
     1       CyclicRefTypeUnits(:idel(CyclicRefTypeUnits))//']'
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,Veta)
        if(NParType.eq.1) then
          Veta=MenuC(NCellPar)
          if(NCellPar.le.3) then
            Veta=Veta(:idel(Veta)+1)//'[ang]'
          else if(NCellPar.le.6) then
            Veta=Veta(:idel(Veta)+1)//'[deg]'
        else
            Veta=Veta(:idel(Veta)+1)//'[Ang^3]'
          endif
        else
          Veta='????'
        endif
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,Veta)
        xpp(3)=0.
        do i=1,NGrP
          xpp(1)=XGrP(i)
          xpp(2)=YGrP(i)
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,White)
        enddo
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
      GraphExists=.true.
1200  do nButt=nButtQuit,nButtBasTo
        if(GraphExists.or.nButt.eq.nButtQuit.or.nButt.eq.nButtNew) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButt,j)
      enddo
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 2800
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ichp)
          if(ichp.eq.0) then
            go to 1150
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',7,1)
          if(HardCopy.gt.0) then
            go to 1150
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtNew) then
          xqdp=450.
          il=7
          idp=NextQuestId()
          Veta='Define parameter to be drawn:'
          call FeQuestCreate(idp,-1.,-1.,xqdp,il,Veta,0,LightGray,0,0)
          il=0
          Veta='%Cell parameter'
          xpom=5.
          tpom=xpom+CrwgXd+10.
          il=1
          do i=1,4
            call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,1)
            call FeQuestCrwOpen(CrwLastMade,i.eq.1)
            if(i.ne.1) call FeQuestCrwDisable(CrwLastMade)
            if(i.eq.1) then
              Veta='%Structural paramater'
              nCrwCell=CrwLastMade
            else if(i.eq.2) then
              Veta='%Profile paramater'
              nCrwStructure=CrwLastMade
            else if(i.eq.3) then
              Veta='%R factor'
              nCrwProfile=CrwLastMade
            else if(i.eq.4) then
              nCrwR=CrwLastMade
            endif
            il=il+1
          enddo
          xpom=xpom+xqdp*.5
          tpom=tpom+xqdp*.5
          il=1
          do i=1,7
            call FeQuestCrwMake(idp,tpom,il,xpom,il,MenuC(i),'L',CrwgXd,
     1                          CrwgYd,0,2)
            if(i.eq.1) then
              nCrwCellFirst=CrwLastMade
            else if(i.eq.7) then
              nCrwCellLast=CrwLastMade
            endif
            call FeQuestCrwOpen(CrwLastMade,i.eq.NCellPar)
            il=il+1
          enddo
2700      call FeQuestEvent(idp,ichp)
          if(CheckType.eq.EventCrw) then
            go to 2700
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 2700
          endif
          if(ichp.eq.0) then
            if(CrwLogicQuest(nCrwCell)) then
              NParType=1
              nCrw=nCrwCellFirst
              do i=1,7
                if(CrwLogicQuest(nCrw)) then
                  NCellPar=i
                  exit
                endif
                nCrw=nCrw+1
              enddo
            else
              NParType=0
            endif
          endif
          call FeQuestRemove(idp)
          if(ichp.ne.0.or.NParType.eq.0) then
            go to 2500
          else
            go to 1100
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
2800  call FeQuestRemove(id)
      if(allocated(XGrP)) deallocate(XGrP,YGrP)
      return
      end
