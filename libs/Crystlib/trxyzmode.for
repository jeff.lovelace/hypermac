      subroutine TrXYZMode(Klic)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      dimension XYZAtom(:,:)
      integer XYZAtomFlag(:)
      allocatable XYZAtom,XYZAtomFlag
      save XYZAtom,XYZAtomFlag
      if(Klic.lt.0) then
        XYZModeInAtoms=.false.
        go to 9999
      endif
      if(XYZModeInAtoms.eqv.Klic.eq.0) then
        if(VasekTest.ne.0) then
          if(Klic.eq.0) then
            Veta='chce rozpocitavat mody, ale ono je jiz rozpocitano'
          else
            Veta='chce obnovovat vychozi polohy atomu, ale ono je to '//
     1           'jiz obnoveno'
          endif
          call FeWinMessage(Veta,' ')
        endif
        go to 9999
      endif
      if(Klic.eq.0) then
        if(allocated(XYZAtom)) deallocate(XYZAtom,XYZAtomFlag)
        allocate(XYZAtom(3,NAtCalc),XYZAtomFlag(NAtCalc))
        call SetIntArrayTo(XYZAtomFlag,NAtCalc,0)
        do i=1,NAtXYZMode
          l=0
          do j=1,MAtXYZMode(i)
            ia=IAtXYZMode(j,i)
            if(XYZAtomFlag(ia).le.0) then
              call CopyVek(x(1,ia),XYZAtom(1,ia),3)
              XYZAtomFlag(ia)=1
            endif
            do k=1,NMAtXYZMode(i)
              kt=KAtXYZMode(k,i)
              do m=1,3
                x(m,ia)=x(m,ia)+
     1                  FAtXYZMode(k,i)*AtXYZMode(k,i)*XYZAMode(l+m,kt)
              enddo
            enddo
            l=l+3
          enddo
        enddo
        XYZModeInAtoms=.true.
        NAtCalcUsed=NAtCalc
      else if(Klic.eq.1) then
        do i=1,NAtCalcUsed
          if(XYZAtomFlag(ia).gt.0)
     1      call CopyVek(XYZAtom(1,i),x(1,i),3)
        enddo
        XYZModeInAtoms=.false.
      endif
9999  return
      end
