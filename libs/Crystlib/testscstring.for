      subroutine TestScString(StSc,ErrString)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) StSc,ErrString
      logical EqIgCase
      ErrString=' '
      i=index(StSc,'%')
      if(i.gt.0) then
        Cislo=StSc(:i-1)
        if(ktat(PhaseName,NPhase,Cislo).le.0) go to 9000
        Cislo=StSc(i+1:)
        go to 2000
      endif
      Cislo=StSc
      if(ktat(PhaseName,NPhase,Cislo).gt.0) go to 9999
      i=index(Cislo,'#')
      if(i.gt.0) then
        if(EqIgCase(Cislo(:4),'zone')) go to 9999
      endif
2000  if(.not.EqIgCase(Cislo(:5),'block')) go to 9100
      go to 9999
9000  ErrString='phase "'//Cislo(:idel(Cislo))//'" does not exist.'
      go to 9999
9100  ErrString='incorrect BlockName: "'//Cislo(:idel(Cislo))//'"'
      go to 9999
9200  ErrString='incorrect zone label : "'//Cislo(:idel(Cislo))//'"'
9999  return
      end
