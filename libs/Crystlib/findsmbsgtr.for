      subroutine FindSmbSgTr(Grp,Shift,GrpTr,NGrpTr,TrMat,TrShift,
     1                       CrSystemNew,CrSystemNewTr)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension TrMat(*),TrShift(*),iop(3,3),TrPom(3,3),TrPomI(3,3),
     1          CellParS(6),ShSgS(6),Qus(3,3),rm6s(:,:),rms(:,:),
     2          s6s(:,:),IswSymmS(:),ZMagS(:),RMagS(:,:),vt6s(:,:),
     3          xp(6),rmp(36),TrPom6(36),TrPom6I(36),TrMatI(36),hp(3),
     4          QuiS(3,3),QuiRS(3,3),xop(3,3),ip(3),xopt(3,3),xpt(3),
     5          Shift(*),CellP(6)
      integer io(:,:),CrSystemS,CrSystemP,CrSystemNew,CrSystemNewTr,
     1        CelkemNenul,CrSystemFromCell
      integer :: ioHexa(3,6)=
     1           reshape((/1,0,0,1,1,0,0,1,0,-1,-2,0,1,-1,0,2,1,0/),
     2                   shape(ioHexa))
      character*(*) GrpTr,Grp
      character*256 Veta
      character*80  GrpSearch
      character*40  itxt
      character*20  GrupaS
      character*8   GrupaR,GrupaP
      character*7 :: OpSmbTwoFold=('2abcnmd')
      character*4   LatticeS
      character*3 :: abc=('abc'),OpSmb(:)
      character*2 t2m
      real :: Vt6Zle(3)=(/.3333333,.6666667,.3333333/)
      allocatable OpSmb,io,rm6s,rms,s6s,IswSymmS,ZMagS,RMagS,vt6s
      logical EqIgCase,EqIV,EqRV,EqRVM,EqIVM,MatRealEqUnitMat,Preskok
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      lni=0
      nss=NSymm(KPhase)
      nssn=NSymmN(KPhase)
      nvts=NLattVec(KPhase)
      CrSystemS=CrSystem(KPhase)
      CrSystemP=CrSystemFromCell(CellPar(1,1,KPhase),.001,.05)
      call CopyVek(ShSg(1,KPhase),ShSgS,min(NDim(KPhase),4))
      LatticeS=Lattice(KPhase)
      call CopyVek(CellPar(1,1,KPhase),CellParS,6)
      call CopyVek(Qu(1,1,1,KPhase),Qus,3*NDimI(KPhase))
      call CopyVek(Qui(1,1,KPhase),QuiS,3*NDimI(KPhase))
      call CopyVek(QuiR(1,1,KPhase),QuiRS,3*NDimI(KPhase))
      call UnitMat(TrMat ,NDim(KPhase))
      call UnitMat(TrMatI,NDim(KPhase))
      allocate(rm6s(36,nss),rms(9,nss),s6s(6,nss),IswSymmS(nss),
     1         ZMagS(nss),RMagS(9,nss),vt6s(6,nvts))
      do i=1,nss
        call CopyMat(rm6(1,i,1,KPhase),rm6s(1,i),NDim(KPhase))
        call CopyMat(rm(1,i,1,KPhase),rms(1,i),3)
        call CopyVek(s6(1,i,1,KPhase),s6s(1,i),NDim(KPhase))
        IswSymmS(i)=ISwSymm(i,1,KPhase)
        ZMagS(i)=ZMag(i,1,KPhase)
        call CopyMat(RMag(1,i,1,KPhase),RMagS(1,i),3)
      enddo
      do i=1,nvts
        call CopyVek(vt6(1,i,1,KPhase),vt6s(1,i),NDim(KPhase))
      enddo
      allocate(io(3,nss),OpSmb(nss))
      do i=1,nss
        call SmbOp(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,OpSmb(i),t2m,
     1             io(1,i),n,det)
      enddo
      call FindSmbSg(Grp,ChangeOrderNo,1)
      call CopyVek(ShSg(1,KPhase),Shift,min(NDim(KPhase),4))
      CrSystemNew=CrSystem(KPhase)
      i3=0
      i4=0
      i6=0
      KdeDve=3
      call UnitMatI(iop,3)
      do i=1,nss
        if(EqIgCase(OpSmb(i)(1:1),'3').or.EqIgCase(OpSmb(i),'-3')) then
          if(i3.le.0) then
            i3=1
            call CopyVekI(io(1,i),iop(1,3),3)
            if(iop(1,3).lt.0)
     1        call IntVectorToOpposite(iop(1,3),iop(1,3),3)
          else
            if(.not.EqIV (io(1,i),iop(1,3),3).and.
     1         .not.EqIVM(io(1,i),iop(1,3),3)) then
              i3=i3+1
            endif
          endif
        endif
        if(EqIgCase(OpSmb(i)(1:1),'6').or.EqIgCase(OpSmb(i),'-6')) then
          if(i6.le.0) then
            i6=1
            call CopyVekI(io(1,i),iop(1,3),3)
            do j=1,3
              if(iop(j,3).lt.0) then
                call IntVectorToOpposite(iop(1,3),iop(1,3),3)
                exit
              endif
            enddo
          else
            if(.not.EqIV (io(1,i),iop(1,3),3).and.
     1         .not.EqIVM(io(1,i),iop(1,3),3)) then
              i6=i6+1
            endif
          endif
        endif
        if(EqIgCase(OpSmb(i)(1:1),'4').or.EqIgCase(OpSmb(i),'-4')) then
          if(i4.le.0) then
            i4=1
            call CopyVekI(io(1,i),iop(1,3),3)
            do j=1,3
              if(iop(j,3).lt.0) then
                call IntVectorToOpposite(iop(1,3),iop(1,3),3)
                exit
              endif
            enddo
          else
            if(.not.EqIV (io(1,i),iop(1,3),3).and.
     1         .not.EqIVM(io(1,i),iop(1,3),3)) then
              i4=i4+1
            endif
          endif
        endif
      enddo
      if(i6.eq.1.or.i4.eq.1) then
        NNeNul=0
        KNeNul=0
        do i=1,3
          if(iop(i,3).ne.0) then
            NNeNul=NNeNul+1
            KNeNul=i
          endif
        enddo
        if(NNeNul.gt.1.or.NNeNul.le.0.or.KNeNul.eq.3) go to 2100
        call SetIntArrayTo(iop,6,0)
        iop(KNeNul,3)=1
        iop(mod(KNeNul  ,3)+1,1)=1
        iop(mod(KNeNul+1,3)+1,2)=1
      else if(i3.eq.1) then
        if(iabs(iop(1,3)).eq.1.and.iabs(iop(2,3)).eq.1.and.
     1     iabs(iop(3,3)).eq.1) then
          iop(1,1)=1
          iop(2,1)=-iop(1,3)*iop(2,3)
          iop(2,2)= iop(1,3)*iop(2,3)
          iop(3,2)=-iop(1,3)*iop(3,3)
        endif
      else if(i3.gt.1) then
        call UnitMatI(iop,3)
      endif
      if(i3.le.0.and.i4.le.0.and.i6.le.0) then
        i2=0
        CelkemNenul=0
        KolikNenul=0
        KdeNenul=0
        call SetIntArrayTo(iop,9,0)
        do 2050i=1,nss
          if(index(OpSmbTwoFold,OpSmb(i)(1:1)).gt.0) then
            do j=1,min(i2,3)
              if(EqIV (io(1,i),iop(1,j),3).or.
     1           EqIVM(io(1,i),iop(1,j),3)) go to 2050
            enddo
            i2=i2+1
            if(i2.le.3) then
              call CopyVekI(io(1,i),iop(1,i2),3)
              if(i2.eq.1) then
                do j=1,3
                  if(iop(j,1).ne.0) then
                    KolikNenul=KolikNenul+1
                    KdeNenul=j
                  endif
                enddo
              endif
              do j=1,3
                CelkemNenul=CelkemNenul+iabs(iop(j,i2))
                if(iop(j,i2).lt.0) then
                  call IntVectorToOpposite(iop(1,i2),iop(1,i2),3)
                  exit
                endif
              enddo
            endif
          endif
2050    continue
        if(i2.lt.3.and.i2.gt.0) then
          call CopyVekI(iop(1,1),iop(1,2),3)
          xop=0.
          do i=1,3
            xop(i,2)=iop(i,2)
          enddo
          ip1=0
          ip2=0
          do ipp=1,3,2
            dmin=999999.
            imn=99999
            do j1=0,3
              xp(1)=j1
              ip(1)=j1
              if(j1.eq.0) then
                j2p=0
              else
                j2p=-3
              endif
              do j2=j2p,3
                xp(2)=j2
                ip(2)=j2
                if(j1.eq.0.and.j2.eq.0) then
                  j3p=0
                else
                  j3p=-3
                endif
                do j3=j3p,3
                  if(j1.eq.0.and.j2.eq.0.and.j3.eq.0) cycle
                  xp(3)=j3
                  ip(3)=j3
                  call MinMultMaxFract(ip,3,MinMult,MaxFract)
                  if(MaxFract.gt.1) cycle
                  if(ipp.eq.3.and.(EqRV (xop(1,1),xp,3,.001).or.
     1                             EqRVM(xop(1,1),xp,3,.001))) cycle
                  pom=CrlAngle(xop(1,2),xp,1,KPhase,d1,d2)
                  if(abs(abs(pom)-90.).lt..1) then
                    imnp=nint(abs(xp(1)))+nint(abs(xp(2)))+
     1                   nint(abs(xp(3)))
                    if(d2.lt.dmin+.001.and.imnp.lt.imn) then
                      call CopyVek(xp,xop(1,ipp),3)
                      if(ipp.eq.1) then
                        ip1=1
                      else if(ipp.eq.3) then
                        ip2=1
                      endif
                      dmin=d2
                      imn=imnp
                    endif
                  endif
                enddo
              enddo
            enddo
          enddo
          if(ip1.ne.0.and.ip2.ne.0) then
            do j=1,3
              do i=1,3
                iop(i,j)=nint(xop(i,j))
              enddo
            enddo
          else
            call UnitMatI(iop,3)
          endif
        else if(i2.ne.3.or.(i2.eq.3.and.CelkemNenul.eq.3)) then
          call UnitMatI(iop,3)
        endif
        if(i2.eq.3) then
          KdeDve=0
          KteryNenulMax=0
          do j=1,3
            n=0
            do i=1,3
              if(iop(i,j).eq.0) then
                n=n+1
              else
                KteryNenul=i
              endif
            enddo
            if(n.eq.2) then
              if(KteryNenul.gt.KteryNenulMax) then
                KteryNenulMax=KteryNenul
                KdeDve=j
              endif
            endif
          enddo
        endif
      endif
2100  do j=1,3
        jj=mod(j-KdeDve+2,3)+1
        do i=1,3
          TrPom(i,jj)=iop(i,j)
        enddo
      enddo
      call MatInv(TrPom,TrPomI,VolRatio,3)
      if(VolRatio.lt.0.) then
        if(i2.gt.1.or.i3.gt.0.or.i4.gt.0.or.i6.gt.0) then
          ii=2
        else
          ii=3
        endif
        do i=1,3
          pom=TrPom(i,ii)
          TrPom(i,ii)=TrPom(i,1)
          TrPom(i,1)=pom
        enddo
        if(i3.eq.1.and.i6.eq.0)
     1    call RealVectorToOpposite(TrPom,TrPom,6)
        call MatInv(TrPom,TrPomI,VolRatio,3)
      endif
      call DRMatTrCell(TrPom,CellPar(1,1,KPhase),TrMat)
      do i=1,NDimI(KPhase)
        call CopyVek(Qu(1,i,1,KPhase),xp,3)
        call multm(xp,TrPom,Qu(1,i,1,KPhase),1,3,3)
        call CopyVek(QuiR(1,i,KPhase),xp,3)
        call multm(xp,TrPom,QuiR(1,i,KPhase),1,3,3)
      enddo
      call MatInv(TrMat,TrMatI,VolRatio,NDim(KPhase))
      nvtt=0
      do 2200i=1,nvts
        call multm(TrMatI,vt6s(1,i),rmp,NDim(KPhase),
     1             NDim(KPhase),1)
        call od0do1(rmp,xp,NDim(KPhase))
        do j=1,nvtt
          if(eqrv(xp,vt6(1,j,1,KPhase),NDim(KPhase),.001)) go to 2200
        enddo
        nvtt=nvtt+1
        call CopyVek(xp,vt6(1,nvtt,1,KPhase),NDim(KPhase))
2200  continue
      if(VolRatio.gt.1.5) then
        i=nint(VolRatio)
        call SetRealArrayTo(xp,6,0.)
        do i3=-i,i
          xp(3)=i3
          do i2=-i,i
            xp(2)=i2
            do 2210i1=-i,i
              xp(1)=i1
              call multm(TrMatI,xp,rmp,NDim(KPhase),NDim(KPhase),1)
              call od0do1(rmp,hp,3)
              do j=1,nvtt
                if(eqrv(hp,vt6(1,j,1,KPhase),3,.001)) go to 2210
              enddo
              nvtt=nvtt+1
              call ReallocSymm(NDim(KPhase),2*nss,nvtt,
     1                         NComp(KPhase),NPhase,0)
              call CopyVek(hp,vt6(1,nvtt,1,KPhase),3)
              NLattVec(KPhase)=nvtt
              if(NDimI(KPhase).gt.0)
     1          call SetRealArrayTo(vt6(4,nvtt,1,KPhase),
     2                              NDimI(KPhase),0.)
2210        continue
          enddo
        enddo
      endif
      NLattVec(KPhase)=nvtt
      call ReallocSymm(NDim(KPhase),2*nss,1000,NComp(KPhase),NPhase,0)
      call EM50CompleteCentr(0,vt6(1,1,1,KPhase),ubound(vt6,1),nvtt,
     1                       1000,ich)
      NLattVec(KPhase)=nvtt
      call ReallocSymm(NDim(KPhase),2*nss,nvtt,NComp(KPhase),NPhase,0)
      do i=1,NSymm(KPhase)
        call multm(TrMatI,rm6(1,i,1,KPhase),rmp,NDim(KPhase),
     1             NDim(KPhase),NDim(KPhase))
        call multm(rmp,TrMat,rm6(1,i,1,KPhase),NDim(KPhase),
     1             NDim(KPhase),NDim(KPhase))
        call MatBlock3(rm6(1,i,1,KPhase),rm(1,i,1,KPhase),NDim(KPhase))
        call multm(TrMatI,s6(1,i,1,KPhase),xp,NDim(KPhase),
     1             NDim(KPhase),1)
        call CopyVek(xp,s6(1,i,1,KPhase),NDim(KPhase))
      enddo
      call CrlCheckSymmForLattVec(1)
      call FindSmbSg(GrpTr,ChangeOrderNo,1)
      NGrpTr=NGrupa(KPhase)
      call UnitMat(TrPom,3)
      call CopyVek(ShSg(1,KPhase),TrShift,min(NDim(KPhase),4))
      iprichod=0
      izpet=0
      if(GrpTr(1:1).ne.'?'.and.GrpTr(1:1).ne.'X'.and.
     1   CrSystem(KPhase).eq.CrSystemOrthorhombic) then
        GrpSearch=' '
        j=0
        Preskok=.false.
        do i=1,idel(GrpTr)
          if(GrpTr(i:i).eq.'''') cycle
          if(GrpTr(i:i).eq.'('.or.GrpTr(i:i).eq.'.') exit
          if(GrpTr(i:i).eq.'[') then
            Preskok=.true.
          else if(GrpTr(i:i).eq.']') then
            Preskok=.false.
            cycle
          endif
          if(Preskok) cycle
          j=j+1
          GrpSearch(j:j)=GrpTr(i:i)
        enddo
        lni=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1         'spgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
        endif
        call OpenFile(lni,Veta,'formatted','old')
        read(lni,FormA) Veta
        if(Veta(1:1).ne.'#') rewind lni
        igp=0
2230    read(lni,FormSG,end=3000) ig,ipg,idl,GrupaR,itxt,GrupaS
        if(ig.eq.39.or.ig.eq.41.or.ig.eq.64.or.ig.eq.67.or.ig.eq.68)
     1    then
          Cislo=GrupaR
          call mala(Cislo)
          if(index(Cislo,'e').le.0) go to 2230
        endif
        if(igp.ne.ig) then
          igp=ig
          GrupaP=GrupaR
        endif
        if(.not.EqIgCase(GrupaR,GrpSearch)) go to 2230
        call CloseIfOpened(lni)
        idl=index(GrupaS,'#')
        if(.not.EqIgCase(GrpSearch,GrupaP).and.idl.gt.0) then
          call SetRealArrayTo(TrPom,9,0.)
          i=0
          j=idl
          im=idel(GrupaS)
2235      izn=1
          i=i+1
          if(i.gt.3) then
            if(j.eq.im) then
              go to 2240
            else
              iprichod=1
              go to 3000
            endif
          endif
          do while(j<=im)
            j=j+1
            k=index(abc,GrupaS(j:j))
            if(k.ge.1) then
              TrPom(i,k)=izn
              go to 2235
            else if(GrupaS(j:j).eq.'-') then
              izn=-1.
            else if(GrupaS(j:j).ne.',') then
              iprichod=2
              go to 3000
            endif
          enddo
          iprichod=3
          go to 3000
        endif
      endif
2240  if(.not.MatRealEqUnitMat(TrPom,3,.001)) then
        call MatInv(TrPom,TrPomI,pom,3)
        k=0
        do i=1,NDim(KPhase)
          do j=1,NDim(KPhase)
            k=k+1
            if(i.le.3.and.j.le.3) then
              TrPom6 (k)=TrPom (j,i)
              TrPom6I(k)=TrPomI(j,i)
            else if(j.eq.i) then
              TrPom6 (k)=1.
              TrPom6I(k)=1.
            else
              TrPom6 (k)=0.
              TrPom6I(k)=0.
            endif
          enddo
        enddo
        if(NDimI(KPhase).eq.1) then
!          do i=1,3
!            TrPom6(i*4)=xpt(i)
!          enddo
          call MatInv(TrPom6,TrPom6I,pom,4)
        endif
        do i=1,NSymm(KPhase)
          call multm(TrPom6I,rm6(1,i,1,KPhase),rmp,NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          call multm(rmp,TrPom6,rm6(1,i,1,KPhase),NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
          call MatBlock3(rm6(1,i,1,KPhase),rm(1,i,1,KPhase),
     1                   NDim(KPhase))
          call multm(TrPom6I,s6(1,i,1,KPhase),xp,NDim(KPhase),
     1               NDim(KPhase),1)
          call od0do1(xp,xp,NDim(KPhase))
          call CopyVek(xp,s6(1,i,1,KPhase),NDim(KPhase))
        enddo
        nvtt=0
        do 2300i=1,NLattVec(KPhase)
          call multm(TrPom6I,vt6(1,i,1,KPhase),xp,NDim(KPhase),
     1               NDim(KPhase),1)
          call od0do1(xp,xp,NDim(KPhase))
          do j=1,nvtt
            if(eqrv(xp,vt6(1,j,1,KPhase),NDim(KPhase),.001)) go to 2300
          enddo
          nvtt=nvtt+1
          call CopyVek(xp,vt6(1,nvtt,1,KPhase),NDim(KPhase))
2300    continue
        NLattVec(KPhase)=nvtt
        call DRMatTrCell(TrPom,CellPar(1,1,KPhase),TrMat)
        do i=1,NDimI(KPhase)
          call CopyVek(Qu(1,i,1,KPhase),xp,3)
          call multm(xp,TrPom,Qu(1,i,1,KPhase),1,3,3)
          call CopyVek(QuiR(1,i,KPhase),xp,3)
          call multm(xp,TrPom,QuiR(1,i,KPhase),1,3,3)
!          if(NDimI(KPhase).eq.1) then
!            do j=1,3
!              Qu(j,i,1,KPhase)=Qu(j,i,1,KPhase)-xpt(j)
!              QuiR(j,i,KPhase)=QuiR(j,i,KPhase)-xpt(j)
!            enddo
!          endif
        enddo
        call MatInv(TrMat,TrMatI,VolRatio,NDim(KPhase))
        call FindSmbSg(GrpTr,ChangeOrderNo,1)
        NGrpTr=NGrupa(KPhase)
        call CopyVek(ShSg(1,KPhase),TrShift,min(NDim(KPhase),4))
!        if(NDimI(KPhase).eq.1) then
!          call GetQiQr(qu(1,1,1,KPhase),xp,hp,NDimI(KPhase),1)
!          call UnitMat(TrPom,3)
!          i0=0
!          ii=0
!          n=0
!          do i=1,3
!            if(xp(i).lt.-.001) then
!              n=n+1
!              TrPom(i,i)=-1.
!            else if(xp(i).gt..001) then
!              TrPom(i,i)= 1.
!            else if(abs(Qu(i,1,1,KPhase)).lt..001) then
!              i0=i
!            else
!              ii=i
!            endif
!          enddo
!          if(n.gt.0) then
!            if(n.eq.1) then
!              if(i0.gt.0) then
!                if(ii.gt.0.and.Qu(ii,1,1,KPhase).lt.-.001) then
!                  TrPom(ii,ii)=-1.
!                else
!                  TrPom(i0,i0)=-1.
!                endif
!              else
!                TrPom(ii,ii)=-1.
!              endif
!            endif
!            go to 2240
!          endif
!        endif
      endif
3000  call CloseIfOpened(lni)
      call UnitMat(TrPom,3)
      if(izpet.eq.0) then
        do i=1,NLattVec(KPhase)
          NNul=0
          KNenul=0
          do j=1,NDim(KPhase)
            if(abs(vt6(j,i,1,KPhase)).lt..001) then
              NNul=NNul+1
            else
              KNenul=j
            endif
          enddo
          if(NNul.eq.NDim(KPhase)-1.and.KNenul.le.3) then
            TrPom(KNenul,KNenul)=vt6(KNenul,i,1,KPhase)
            izpet=-1
          endif
        enddo
        if(izpet.lt.0) then
          izpet=0
          go to 2240
        endif
        if(CrSystem(KPhase).eq.CrSystemTriclinic) then
          if(.not.EqIgCase(Lattice(KPhase),'P')) izpet=1
          if(EqIgCase(GrpTr(1:1),'A')) then
            TrPom(2,2)=.5
            TrPom(3,2)=.5
          else if(EqIgCase(Lattice(KPhase),'B')) then
            TrPom(1,1)=.5
            TrPom(3,1)=.5
          else if(EqIgCase(Lattice(KPhase),'C')) then
            TrPom(1,1)=.5
            TrPom(2,1)=.5
          else if(EqIgCase(Lattice(KPhase),'I')) then
            TrPom(1,1)=.5
            TrPom(2,1)=.5
            TrPom(3,1)=.5
          else if(EqIgCase(Lattice(KPhase),'F')) then
            TrPom(1,1)=.5
            TrPom(3,1)=.5
            TrPom(1,2)=.5
            TrPom(2,2)=.5
          else if(EqIgCase(Lattice(KPhase),'R')) then
            TrPom(1,1)= .6666667
            TrPom(1,2)=-.3333333
            TrPom(1,3)=-.3333333
            TrPom(2,1)= .3333333
            TrPom(2,2)= .3333333
            TrPom(2,3)=-.6666667
            TrPom(3,1)= .3333333
            TrPom(3,2)= .3333333
            TrPom(3,3)= .3333333
          else if(EqIgCase(Lattice(KPhase),'X')) then
            if(NLattVec(KPhase).eq.3) then
              TrPom(1,1)=vt6(1,2,1,KPhase)
              TrPom(2,1)=vt6(2,2,1,KPhase)
              TrPom(3,1)=vt6(3,2,1,KPhase)
              TrPom(1,2)=vt6(1,3,1,KPhase)
              TrPom(2,2)=vt6(2,3,1,KPhase)
              TrPom(3,2)=vt6(3,3,1,KPhase)
              TrPom(1,3)=0.
              TrPom(2,3)=0.
              TrPom(3,3)=1.
            endif
          endif
        else if(CrSystem(KPhase).eq.20+CrSystemMonoclinic) then
          if(EqIgCase(Lattice(KPhase),'A')) then
            TrPom(1,1)=0.
            TrPom(3,1)=1.
            TrPom(1,3)=1.
            TrPom(3,3)=0.
            TrPom(2,2)=-1.
            izpet=1
          else if(EqIgCase(Lattice(KPhase),'B')) then
            TrPom(1,1)=.5
            TrPom(3,1)=.5
            izpet=1
          else if(EqIgCase(Lattice(KPhase),'I')) then
            TrPom(3,1)=1.
            izpet=1
          else if(EqIgCase(Lattice(KPhase),'F').or.
     1            (EqIgCase(LatticeS(1:1),'F').and.GrpTr(1:1).eq.'?'))
     2      then
            TrPom(1,3)=.5
            TrPom(3,3)=.5
            izpet=1
          else if(EqIgCase(Lattice(KPhase),'X')) then
            call UnitMat(TrPomI,3)
            TrPomI(1,1)=1.
            TrPomI(3,3)=3.
            do 3200k=-1,1,2
              TrPomI(1,3)=k
              do i=1,NLattVec(KPhase)
                call Multm(TrPomI,vt6(1,i,1,KPhase),xp,3,3,1)
                call od0do1(xp,xp,3)
                do j=1,3
                  if(abs(anint(2.*xp(j))-2.*xp(j)).gt..0001) go to 3200
                enddo
              enddo
              izpet=1
              call MatInv(TrPomI,TrPom,VolRatio,3)
              go to 2240
3200        continue
          endif
        else if(CrSystem(KPhase).eq.CrSystemTetragonal) then
          if(EqIgCase(Lattice(KPhase),'F')) then
            TrPom(1,1)= .5
            TrPom(1,2)= .5
            TrPom(2,1)=-.5
            TrPom(2,2)= .5
            izpet=1
          endif
        else if(CrSystem(KPhase).eq.CrSystemTrigonal) then
          call UnitMat(TrPomI,3)
          if(EqIgCase(Lattice(KPhase),'X')) then
            do i=1,NLattVec(KPhase)
              nnul=0
              npul=0
              kpul=0
              do j=1,3
                if(abs(vt6(j,i,1,KPhase)   ).lt..0001) nnul=nnul+1
                if(abs(vt6(j,i,1,KPhase)-.5).lt..0001) then
                  npul=npul+1
                  kpul=j
                endif
              enddo
              if(npul.eq.1.and.nnul.eq.2) then
                TrPomI(kpul,kpul)=2.
                izpet=1
              endif
            enddo
            do i=1,NLattVec(KPhase)
              call Multm(TrPomI,vt6(1,i,1,KPhase),xp,3,3,1)
              call od0do1(xp,xp,3)
              if(EqRV(xp,vt6Zle,3,.0001)) then
                TrPomI(1,1)=-TrPomI(1,1)
                TrPomI(2,2)=-TrPomI(2,2)
                izpet=1
              endif
            enddo
            if(izpet.ne.0) call MatInv(TrPomI,TrPom,VolRatio,3)
          endif
        endif
        if(izpet.ne.0) go to 2240
      endif
      if(izpet.le.1) then
        if(CrSystem(KPhase).eq.CrSystemTriclinic) then
          CellP(1:6)=CellPar(1:6,1,KPhase)
          call UnitMat(TrPom6,NDim(KPhase))
          call DRCellReduction(CellP,TrPom6)
          call MatBlock3(TrPom6,TrPom,NDim(KPhase))
          if(.not.MatRealEqUnitMat(TrPom,3,.001)) izpet=2
        else if(CrSystem(KPhase).eq.20+CrSystemMonoclinic) then
          i=index(GrpTr,'(')
          if(i.gt.0.and.NDimI(KPhase).eq.1) then
            i=i-1
          else
            i=idel(GrpTr)
          endif
          Veta=GrpTr(:i)
          i1=index(Veta,'[')
          i2=index(Veta,']')
          if(i1.gt.0.and.i2.gt.0.and.i2.gt.i1)
     1      Veta=Veta(1:i1-1)//Veta(i2+1:)
          call mala(Veta)
          if(index(Veta(2:),'a').gt.0) then
            TrPom(1,1)=0.
            TrPom(3,1)=1.
            TrPom(1,3)=1.
            TrPom(3,3)=0.
            TrPom(2,2)=-1.
            izpet=2
          else if(index(Veta(2:),'n').gt.0) then
            TrPom(1,3)=1.
            TrPom(3,3)=1.
            izpet=2
          endif
        endif
        if(izpet.eq.2) go to 2240
      endif
      if(izpet.le.2) then
        if(CrSystem(KPhase).eq.20+CrSystemMonoclinic) then
          if(CellPar(5,1,KPhase).lt.89.999) then
            TrPom(1,1)=-1.
            TrPom(2,2)=-1.
            izpet=3
          endif
          if(izpet.eq.3) go to 2240
        endif
      endif
      CrSystemNewTr=CrSystem(KPhase)
      do i=1,nss
        call CopyMat(rm6s(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
        call CopyMat(rms(1,i),rm(1,i,1,KPhase),3)
        call CopyVek(s6s(1,i),s6(1,i,1,KPhase),NDim(KPhase))
        call codesymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        ISwSymm(i,1,KPhase)=IswSymmS(i)
        ZMag(i,1,KPhase)=ZMagS(i)
        call CopyMat(RMagS(1,i),RMag(1,i,1,KPhase),3)
      enddo
      do i=1,nvts
        call CopyVek(vt6s(1,i),vt6(1,i,1,KPhase),NDim(KPhase))
      enddo
      NSymm(KPhase)=nss
      NSymmN(KPhase)=nssn
      NLattVec(KPhase)=nvts
      CrSystem(KPhase)=CrSystemS
      call CopyVek(ShSgS,ShSg(1,KPhase),min(NDim(KPhase),4))
      Lattice(KPhase)=LatticeS
      call CopyVek(CellParS,CellPar(1,1,KPhase),6)
      call CopyVek(Qus,Qu(1,1,1,KPhase),3*NDimI(KPhase))
      call CopyVek(QuiRS,QuiR(1,1,KPhase),3*NDimI(KPhase))
      call CopyVek(QuiS,Qui(1,1,KPhase),3*NDimI(KPhase))
      if(allocated(io)) deallocate(io,OpSmb)
      if(allocated(rm6s)) deallocate(rm6s,rms,s6s,IswSymmS,ZMagS,RMagS,
     1                               vt6s)
      call ResetIgnoreW
      call ResetIgnoreE
      return
      end
