      subroutine GetShSg(rx,px,n,nx,vt,ndvt,nvt,ShSg)
      include 'fepc.cmn'
      dimension rx(2*n,n),px(2*n),ShSg(n),pxi(6),vt(ndvt,nvt),Sol0(6),
     1          SolDel(6,6),ShSgV(6),ShSgP(6),fn(6),im(6)
      if(nx.le.0) go to 9999
      difc=0.
      nposc=0
      call SetIntArrayTo(im,6,0)
      do i=1,n
        difc=difc+abs(ShSg(i))
        if(ShSg(i).ge.0.) nposc=nposc+1
      enddo
      call SetRealArrayTo(SolDel,36,0.)
      do l=1,nx
        call SetRealArrayTo(pxi,nx,0.)
        pxi(l)=1.
        sum=0.
        do 1300i=nx,1,-1
          do j=1,n
            if(abs(rx(i,j)).gt..0001) then
              pom=pxi(i)
              do k=j+1,n
                pom=pom-rx(i,k)*SolDel(k,l)
              enddo
              SolDel(j,l)=pom/rx(i,j)
              call od0do1(SolDel(j,l),SolDel(j,l),1)
              sum=sum+abs(SolDel(j,l))
              go to 1300
            endif
          enddo
1300    continue
        if(sum.gt..0001) then
          im(l)=1
        else
          im(l)=0
        endif
      enddo
      call SetRealArrayTo(Sol0,6,0.)
      do 1700i=nx,1,-1
        do j=1,n
          if(abs(rx(i,j)).gt..0001) then
            pom=px(i)
            do k=j+1,n
              pom=pom-rx(i,k)*Sol0(k)
            enddo
            Sol0(j)=pom/rx(i,j)
            go to 1700
          endif
        enddo
1700  continue
      do i1=-im(1),im(1)
        fn(1)=i1
        do i2=-im(2),im(2)
          fn(2)=i2
          do i3=-im(3),im(3)
            fn(3)=i3
            do i4=-im(4),im(4)
              fn(4)=i4
              do i5=-im(5),im(5)
                fn(5)=i5
                do i6=-im(6),im(6)
                  fn(6)=i6
                  call CopyVek(Sol0,ShSgP,n)
                  do i=1,nx
                    fni=fn(i)
                    do j=1,n
                      ShSgP(j)=ShSgP(j)+fni*SolDel(j,i)
                    enddo
                  enddo
                  do 2400ivt=1,nvt
                    call AddVek(ShSgP,vt(1,ivt),ShSgV,n)
                    call od0do1(ShSgV,ShSgV,n)
                    difp=0.
                    npos=0
                    do j=1,n
                      if(ShSgV(j).gt..5) ShSgV(j)=ShSgV(j)-1.
                      pom=ShSgV(j)
                      difp=difp+abs(pom)
                      if(difp.gt.difc) go to 2400
                      if(ShSgV(j).ge.0.) npos=npos+1
                    enddo
                    if((difp.eq.difc.and.npos.gt.nposc).or.
     1                  difp.lt.difc) then
                      difc=difp
                      call CopyVek(ShSgV,ShSg,n)
                      nposc=npos
                      if(difc.le..0001) go to 9999
                    endif
2400              continue
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo
      if(difc.gt.900000.) call SetRealArrayTo(ShSg,n,0.)
9999  return
      end
