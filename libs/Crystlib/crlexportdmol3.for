      subroutine CrlExportDMol3
      use Atoms_mod
      use Basic_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xyz(:,:),xp(3),xo(3),TrC(3,3),TrCT(3,3),xs(3,3),xss(3,3)
      integer   AtTypeN(:)
      character*256 FileName,Veta
      character*10  t10
      logical ExistFile,FeYesNo,Export
      allocatable xyz,AtTypeN
      Export=.true.
      go to 1100
      entry CrlImportDMol3
      Export=.false.
1100  allocate(xyz(3,NSymm(KPhase)*NLattVec(KPhase)),
     1         AtTypeN(NAtFormula(KPhase)))
      ln=0
      ic=LocateSubstring(SmbC,Lattice(KPhase)(:idel(Lattice(KPhase))),
     1                   .false.,.true.)
      call UnitMat(TrC,3)
      if(ic.eq.2) then
        TrC(2,3)=.5
        TrC(3,3)=.5
      else if(ic.eq.3) then
        TrC(1,1)=.5
        TrC(3,1)=.5
      else if(ic.eq.4) then
        TrC(1,2)=.5
        TrC(2,2)=.5
      else if(ic.eq.5) then
        TrC(1,3)=.5
        TrC(2,3)=.5
        TrC(3,3)=.5
      else if(ic.eq.6) then
        call SetRealArrayTo(TrC,9,0.)
        TrC(1,1)= .666666667
        TrC(1,2)=-.333333333
        TrC(1,3)=-.333333333
        TrC(2,1)= .333333333
        TrC(2,2)= .333333333
        TrC(2,3)=-.666666667
        TrC(3,1)= .333333333
        TrC(3,2)= .333333333
        TrC(3,3)= .333333333
      else if(ic.eq.7) then
        call SetRealArrayTo(TrC,9,0.)
        TrC(1,1)=.5
        TrC(3,1)=.5
        TrC(1,2)=.5
        TrC(2,2)=.5
        TrC(2,3)=.5
        TrC(3,3)=.5
      else if(ic.eq.8) then
        call FeChybne(-1.,-1.,'a non-standard X cell centering '//
     1                'occured.',
     2                'The import to the DMol3 format cannot be '//
     3                'performed.',SeriousError)
        go to 9999
      endif
      call TrMat(TrC,TRCT,3,3)
      FileName=fln(:ifln)//'_DMol3.txt'
      if(Export) then
        Cislo='out'
      else
        Cislo='in'
      endif
1200  call FeFileManager('Select '//Cislo(:idel(Cislo))//'put file',
     1                   FileName,'*.txt',0,.true.,ich)
      if(ich.ne.0.or.FileName.eq.' ') go to 9999
      if(Export) then
        if(ExistFile(FileName)) then
          if(.not.FeYesNo(-1.,-1.,'The file "'//
     1                    FileName(:idel(FileName))//
     2                    '" already exists, rewrite it?',0)) go to 9999
        endif
      else
        if(.not.ExistFile(FileName)) then
          call FeChybne(-1.,-1.,'The input doesn''t exist, try'//
     1                  'again.',' ',SeriousError)
          go to 1200
        endif
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,FileName(:idel(FileName)),'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(.not.Export) go to 5000
      write(ln,FormA) '$cell vectors'
      do j=1,3
        do i=1,3
          xs(j,i)=TrToOrtho(i+(j-1)*3,1,KPhase)
        enddo
      enddo
      call Multm(TrCT,xs,xss,3,3,3)
      do j=1,3
        write(ln,100) ' ',(dble(xss(j,i))/dble(BohrRad),i=1,3)
      enddo
      write(ln,FormA) '$coordinates'
      do 2200i=1,NAtCalc
        if(iswa(i).ne.1.or.kswa(i).ne.KPhase) go to 2200
        t10=AtType(isf(i),KPhase)
        call CopyVek(x(1,i),xyz,3)
        call od0do1(xyz,xyz,3)
        n=1
        do 2100isym=1,NSymmN(KPhase)
          call multm(rm(1,isym,1,KPhase),x(1,i),xo,3,3,1)
          call AddVek(xo,s6(1,isym,1,KPhase),xo,3)
          do ivt=1,NLattVec(KPhase)
            call AddVek(xo,vt6(1,ivt,1,KPhase),xp,3)
            do 2050k=1,n
              do m=1,3
                pom=xp(m)-xyz(m,k)
                if(abs(anint(pom)-pom).gt..0001) go to 2050
              enddo
              go to 2100
2050        continue
          enddo
          n=n+1
          call od0do1(xp,xp,3)
          call CopyVek(xp,xyz(1,n),3)
2100    continue
        do j=1,n
          call Multm(TrToOrtho(1,1,KPhase),xyz(1,j),xp,3,3,1)
          write(ln,100) t10,(dble(xp(k))/dble(BohrRad),k=1,3)
        enddo
2200  continue
      write(ln,FormA) '$end'
      go to 9999
5000  read(ln,FormA,end=9000) Veta
      if(LocateSubstring(Veta,'$cell vectors',.false.,.true.).le.0)
     1  go to 5000
      do j=1,3
        read(ln,100,end=9000,err=9100) t10,(xss(j,i),i=1,3)
        do i=1,3
          xss(j,i)=xss(j,i)*BohrRad
        enddo
      enddo
5100  read(ln,FormA,end=9000) Veta
      if(LocateSubstring(Veta,'$coordinates',.false.,.true.).le.0)
     1  go to 5100
      call SetBasicM40(.true.)
      nac=0
      call SetIntArrayTo(AtTypeN,NAtFormula(KPhase),0)
5200  read(ln,FormA,end=5500) Veta
      if(Veta.eq.' ') go to 5200
      if(LocateSubstring(Veta,'$end',.false.,.true.).gt.0) go to 5500
      read(Veta,100,end=9000,err=9100) t10,(xp(k),k=1,3)
      call Multm(TrToOrthoI(1,1,KPhase),xp,xyz(1,1),3,3,1)
      do i=1,3
        xp(i)=xyz(i,1)*BohrRad
      enddo
      i=Koinc(xp,x,1,nac,.1,dist,1,iswa)
      if(i.gt.0) go to 5200
      nac=nac+1
      call CopyVek(xp,x(1,nac),3)
      ai(nac)=1.
      itf(nac)=1
      iswa(nac)=1
      i=max(ktat(AtType(1,KPhase),NAtFormula(KPhase),t10),1)
      AtTypeN(i)=AtTypeN(i)+1
      isf(nac)=i
      write(Cislo,FormI15) AtTypeN(i)
      call Zhusti(Cislo)
      Atom(nac)=t10(:idel(t10))//Cislo(:idel(Cislo))
      call SetRealArrayTo(beta(1,nac),6,0.)
      call SetRealArrayTo(sbeta(1,nac),6,0.)
      sai(nac)=0.
      call SetRealArrayTo(sx(1,nac),3,0.)
      go to 5200
5500  if(KPhase.ne.1) then
        nn=NMolecToAll(KPhase-1)
      else
        nn=0
      endif
      do isw=1,3
        NMolecFr(isw,KPhase)=nn+1
        NMolecTo(isw,KPhase)=nn
        NMolecLen(isw,KPhase)=0
      enddo
      NMolecFrAll(KPhase)=nn+1
      NMolecToAll(KPhase)=nn
      NMolecLenAll(KPhase)=0
      NAtIndLen(1,KPhase)=nac
      call EM40UpdateAtomLimits
      itfmax=2
      IFrMax=0
      centr(KPhase)=0
      call SetRealArrayTo(ec,12*MxDatBlock,0.)
      OverAllB(KPhase)=0.
      call iom40(1,0,fln(:ifln)//'.m40')
      go to 9999
9000  call FeChybne(-1.,-1.,'EOF has been read from the inupt file '//
     1              'befor expeced.',' ',SeriousError)
      go to 9999
9100  call FeReadError(ln)
9999  call CloseIfOpened(ln)
      if(allocated(xyz)) deallocate(xyz,AtTypeN)
      return
100   format(a10,3f20.14)
      end
