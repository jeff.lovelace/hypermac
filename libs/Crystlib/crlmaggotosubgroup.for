      subroutine CrlMagGoToSubgroup
      use Basic_mod
      use RepAnal_mod
      use GoToSubGr_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xr(9)
      character*80 FlnOld
      integer CrlCentroSymm
      logical EqRV
      allocate(PerTabOrg(NSymm(KPhase),NSymm(KPhase)),
     1         AddSymmOrg(NSymm(KPhase)))
      NSymmP=NSymm(KPhase)
      do i=1,NSymmP
        do j=1,NSymmP
          call MultM(rm(1,i,1,KPhase),rm(1,j,1,KPhase),xr,3,3,3)
          do k=1,NSymmP
            if(EqRV(xr,rm(1,k,1,KPhase),9,.01)) then
              PerTabOrg(i,j)=k
              exit
            endif
          enddo
        enddo
      enddo
      invcOrg=CrlCentroSymm()
      call iom40Only(0,0,fln(:ifln)//'.m40')
      NSymmS=NSymm(KPhase)
      allocate(ips(NSymmS),ipr(NSymmS),rm6s(NDimQ(KPhase),NSymmS),
     1         s6s(NDim(KPhase),NSymmS),ipri(NSymmS),iopc(NSymmS),
     2         itrl(NSymmS),StSym(NSymmS),BratSymmSub(NSymmS),
     3         vt6s(NDim(KPhase),NSymmS),io(3,NSymmS),OpSmb(NSymmS),
     4         ios(3,NSymmS),OpSmbS(NSymmS),IswSymms(NSymmS),
     5         ZMagS(NSymmS),RMagS(9,NSymmS))
      if(allocated(BratSymmKer))
     1  deallocate(BratSymmKer,ZMagKer,s6Ker,GrpKer,ShSgKer,TrMatKer)
      m=1
      allocate(BratSymmKer(NSymmS,m),ZMagKer(NSymmS,m),
     1         s6Ker(6,NSymmS,m),GrpKer(m),ShSgKer(6,m),
     2         TrMatKer(NDim(KPhase),NDim(KPhase),m))
      il=15
      id=NextQuestId()
      WizardId=id
      WizardMode=.true.
      WizardTitle=.true.
      WizardLength=650.
      WizardLines=il
      call FeQuestCreate(id,-1.,-1.,WizardLength,il,'Wizard',0,
     1                   LightGray,0,0)
      call SetLogicalArrayTo(BratSymmSub,NSymmS,.true.)
      IPocatek=0
1100  call GoToSubGrSelOp(1,IPocatek,NSymmS,TrMatKer,GrpKer(1),
     1                    ShSgKer(1,1),ich)
      if(ich.ne.0) then
        call FeQuestRemove(id)
        go to 9999
      endif
      do i=1,NSymmS
        BratSymmKer(i,1)=BratSymmSub(i)
        call CopyVek(s6(1,i,1,KPhase),s6Ker(1,i,1),NDim(KPhase))
        ZMagKer(i,1)=ZMag(i,1,KPhase)
      enddo
      if(allocated(BratSymmEpi))
     1  deallocate(BratSymmEpi,ZMagEpi,OrdEpi,s6Epi,GrpEpi,ShSgEpi,
     2             TrMatEpi,rm6r,rmr,s6r,ZMagR)
      m=1
      allocate(BratSymmEpi(NSymmS,m),ZMagEpi(NSymmS,m),OrdEpi(10),
     1         s6Epi(6,NSymmS,m),GrpEpi(m),ShSgEpi(6,m),
     2         TrMatEpi(NDim(KPhase),NDim(KPhase),m),
     3         rm6r(36,NSymmS),rmr(9,NSymmS),s6r(6,NSymmS),
     4         ZMagR(NSymmS))
      GrpEpi(1)=GrpKer(1)
      NEpiMax=1
      NEpi=1
      OrdEpi(1)=1
      do i=1,NSymmS
        BratSymmEpi(i,1)=BratSymmKer(i,1)
        call CopyVek(s6Ker(1,i,1),s6Epi(1,i,1),NDim(KPhase))
        ZMagEpi(i,1)=ZMagKer(i,1)
        call CopyMat(rm6(1,i,1,KPhase),rm6r(1,i),NDim(KPhase))
        call CopyMat(rm (1,i,1,KPhase),rmr (1,i),3)
        call CopyVek(s6 (1,i,1,KPhase),s6r (1,i),NDim(KPhase))
        ZMagR(i)=ZMag(i,1,KPhase)
      enddo
      FlnOld=Fln
      iFlnOld=iFln
      call CrlMagGoToSubgroupFinish(ich)
      if(ich.ne.0) then
         Fln= FlnOld
        iFln=iFlnOld
        call iom50(0,0,fln(:ifln)//'.m50')
        if(ich.gt.0) then
          call iom40Only(0,0,fln(:ifln)//'.m40')
          go to 1100
        endif
        call iom40Only(0,0,fln(:ifln)//'.m40')
      endif
c      call FeQuestRemove(id)
c      WizardMode=.false.
9999  if(allocated(BratSymmKer))
     1  deallocate(BratSymmKer,ZMagKer,s6Ker,GrpKer,ShSgKer,TrMatKer)
      if(allocated(BratSymmEpi))
     1  deallocate(BratSymmEpi,ZMagEpi,s6Epi,GrpEpi,ShSgEpi,TrMatEpi,
     2             rm6r,rmr,s6r,ZMagR,OrdEpi)
      if(allocated(ips))
     1  deallocate(ips,ipr,rm6s,s6s,ipri,iopc,itrl,StSym,BratSymmSub,
     2             vt6s,io,OpSmb,ios,OpSmbS,IswSymms,ZMagS,RMagS)
      if(allocated(PerTabOrg)) deallocate(PerTabOrg,AddSymmOrg)
      return
      end
