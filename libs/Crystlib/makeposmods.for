      subroutine MakePosModS(sxd,x40,tzero,nt,dt,ux,uy,sux,suy,kmod,kf,
     1                       Gamma)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension sxd(3,*),ux(3,*),uy(3,*),sux(3,*),suy(3,*),x40(3),
     1          tzero(3),nt(3),dt(3),x4i(3),x4(3),nd(3),Gamma(9),d4(3)
      if(kf.ne.0) then
        x4s=uy(1,kmod)
        delta=uy(2,kmod)*.5
      endif
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      call SetRealArrayTo(sxd,3*ntt,0.)
      do i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        do k=1,kmod
          arg=0.
          do j=1,NDimI(KPhase)
            arg=arg+x4(j)*float(kw(j,k,KPhase))
          enddo
          arg=pi2*arg
          if(kf.eq.0.or.k.ne.kmod) then
            do l=1,3
              sxd(l,i)=sxd(l,i)+sux(l,k)*sin(arg)**2+
     1                          suy(l,k)*cos(arg)**2
            enddo
          else
            x4p=x4(1)-x4s
            ix4p=x4p
            if(x4p.lt.0.) ix4p=ix4p-1
            x4p=x4p-float(ix4p)
            if(x4p.gt..5) x4p=x4p-1.
            if(kf.eq.1) then
              znak=x4p/delta
            else
              if(x4p.ge.-delta.and.x4p.le.delta) then
                if(kf.eq.2) then
                  znak=x4p/delta
                else
                  znak=1.
                endif
              else
                if(kf.eq.2) then
                  x4p=x4p-.5
                  ix4p=x4p
                  if(x4p.lt.0.) ix4p=ix4p-1
                  x4p=x4p-float(ix4p)
                  if(x4p.gt..5) x4p=x4p-1.
                  if(x4p.ge.-delta.and.x4p.le.delta) then
                    znak=-x4p/delta
                  else
                    znak=-1.
                  endif
                endif
              endif
            endif
            do l=1,3
              sxd(l,i)=sxd(l,i)+znak**2*sux(l,k)
            enddo
          endif
        enddo
      enddo
      return
      end
