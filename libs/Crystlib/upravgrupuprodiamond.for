      subroutine UpravGrupuProDiamond(FileName)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) FileName
      character*256 Veta
      character*7 :: SymbolNew(24) = (/'A e m 2','B 2 e m','C m 2 e',
     1                                 'B m e 2','C 2 m e','A e 2 m',
     2                                 'A e a 2','B 2 e b','C c 2 e',
     3                                 'B b e 2','C 2 c e','A e 2 a',
     4                                 'C m c e','A e m a','B b e m',
     5                                 'C c m e','A e a m','B m e b',
     6                                 'C m m e','A e m m','B m e m',
     7                                 'C c c e','A e a a','B b e b'/)
      character*7 :: SymbolOld(24) = (/'A b m 2','B 2 c m','C m 2 a',
     1                                 'B m a 2','C 2 m b','A c 2 m',
     2                                 'A b a 2','B 2 c b','C c 2 a',
     3                                 'B b a 2','C 2 c b','A c 2 a',
     4                                 'C m c a','A b m a','B b c m',
     5                                 'C c m b','A c a m','B m a b',
     6                                 'C m m a','A b m m','B m c m',
     7                                 'C c c a','A b a a','B b c b'/)
      call CopyFile(FileName,'#pom#.cif')
      lni=NextLogicNumber()
      open(lni,file='#pom#.cif')
      lno=NextLogicNumber()
      open(lno,file=FileName)
1100  read(lni,FormA,end=2000) Veta
      if(LocateSubstring(Veta,'_symmetry_space_group_name_H-M',.false.,
     1   .true.).gt.0) then
        do i=1,24
          j=LocateSubstring(Veta,SymbolNew(i),.false.,.true.)
          if(j.gt.0) then
            Veta(j:j+6)=SymbolOld(i)
            exit
          endif
        enddo
      endif
      write(lno,FormA) Veta(:idel(Veta))
      go to 1100
2000  close(lni,status='delete')
      close(lno)
      return
      end
