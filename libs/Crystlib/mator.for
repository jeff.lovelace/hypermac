      subroutine MatOr(x40,delta,orsel,nd,orm,ormi,natp)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer orsel(*)
      dimension orm(*),ormi(*),go(:)
      allocatable go
      allocate(go(nd**2))
      x1=x40-.5*delta
      x2=x40+.5*delta
      jip=1
      isel=0
      do i=1,nd
        ij=i
        ji=jip
        jsel=0
        do j=1,i
          poms=csprod(OrSel(i)+1,OrSel(j)+1,x1,x2,natp)
          if(KCommen(KPhase).eq.0) then
            poms=poms/delta
          else
            if(i.eq.1.and.j.eq.1) deltac=poms
            poms=poms/deltac
          endif
          if(i.ne.j) then
            go(ij)=poms
          endif
          go(ji)=poms
          ji=ji+1
          ij=ij+nd
        enddo
        jip=jip+nd
      enddo
      nn=1
      knp=1
      do n=1,nd
        ni=n
        do i=1,nd
          orm(ni)=0.
          ni=ni+nd
        enddo
        do j=1,n-1
          aj=0.
          jk=j
          kn=knp
          do k=1,j
            aj=aj-orm(jk)*go(kn)
            kn=kn+1
            jk=jk+nd
          enddo
          nk=n
          jk=j
          do k=1,j
            orm(nk)=orm(nk)+aj*orm(jk)
            nk=nk+nd
            jk=jk+nd
          enddo
        enddo
        orm(nn)=1.
        pom=0.
        ni=n
        do i=1,n
          ij=i
          nj=n
          do j=1,n
            pom=pom+orm(ni)*orm(nj)*go(ij)
            ij=ij+nd
            nj=nj+nd
          enddo
          ni=ni+nd
        enddo
        if(pom.gt.0.) then
          pom=sqrt(1./pom)
        else
          pom=0.
        endif
        ni=n
        do i=1,n
          if(i.ne.n.or.pom.ne.0.) then
            orm(ni)=orm(ni)*pom
          else
            orm(ni)=1.
          endif
          ni=ni+nd
        enddo
        knp=knp+nd
        nn=nn+nd+1
      enddo
      call matinv(orm,ormi,pp,nd)
      deallocate(go)
      return
      end
