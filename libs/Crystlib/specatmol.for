      subroutine SpecAtMol
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(6),x0(6),x1(6),x2(6),x3(6)
      integer, allocatable :: isaOld(:,:)
      integer ISymmPhase(MxPhases)
      KPhaseIn=KPhase
      if(ISymmBasic.gt.0) then
        call CrlCleanSymmetry
        ISymmBasic=0
      endif
      ISymmPhase=0
      do KPhase=1,NPhase
        call CrlStoreSymmetry(ISymmPhase(KPhase))
      enddo
      KPhase=KPhaseIn
      MaxNSymmMol=MaxNSymm
      call CrlStoreSymmetry(ISymmBasic)
      if(allocated(ISymmMolec)) deallocate(ISymmMolec)
      if(NMolec.gt.0) then
        allocate(ISymmMolec(NMolec))
        ISymmMolec=0
      else
        go to 9999
      endif
      MaxNSymmOld=MaxNSymm
      do im=1,NMolec
        isw=iswmol(im)
        KPhase=kswmol(im)
        call ReallocSymm(MaxNDim,NPoint(im),MaxNLattVec,MaxNComp,NPhase,
     1                   0)
        do i=1,NPoint(im)
          call MatFromBlock3(rpoint(1,i,im),rm6(1,i,isw,KPhase),
     1                       NDim(KPhase))
          call CopyVek(spoint(1,i,im),s6(1,i,isw,KPhase),3)
        enddo
        NSymm(KPhase)=NPoint(im)
        call CrlStoreSymmetry(ISymmMolec(im))
        MaxNSymm=max(MaxNSymm,NPoint(im))
        MaxNSymmMol=MaxNSymm
      enddo
      if(MaxNSymm.gt.MaxNSymmOld) then
        allocate(isaOld(MaxNSymmOld,NAtAll))
        do i=1,NAtAll
          do j=1,MaxNSymmOld
            isaOld(j,i)=isa(j,i)
          enddo
        enddo
        deallocate(isa)
        allocate(isa(MaxNSymm,NAtAll))
        do i=1,NAtAll
          do j=1,MaxNSymmOld
            isa(j,i)=isaOld(j,i)
          enddo
          do j=MaxNSymmOld+1,MaxNSymm
            isa(j,i)=j
          enddo
        enddo
        deallocate(isaOld)
      endif
      iak=NAtMolFr(1,1)-1
      do im=1,NMolec
        call CrlRestoreSymmetry(ISymmMolec(im))
        iap=iak+1
        iak=iak+iam(im)
        do ia=iap,iak
          isw=iswa(ia)
          KPhase=kswa(ia)
          AtSiteMult(ia)=1
          do j=1,NSymm(KPhase)
            isa(j,ia)=j
          enddo
          call CopyVek(x(1,ia),xp,3)
          NDimp=NDim(KPhase)
          if(NDimP.gt.3) then
            if(KFA(1,ia).gt.0.and.KModA(1,ia)-NDimP+4.gt.0) then
              call CopyVek(ax(KModA(1,ia)-NDimP+4,ia),xp(4),
     1                     NDimI(KPhase))
            else if(KFA(2,ia).eq.1.and.KModA(2,ia).gt.0) then
              xp(4)=uy(1,KModA(2,ia),ia)
            else
              call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
              NDimp=3
            endif
          endif
          do isym=1,NSymm(KPhase)
            call multm(rm6(1,isym,isw,KPhase),xp,x0,NDim(KPhase),
     1                 NDim(KPhase),1)
            call AddVek(x0,s6(1,isym,isw,KPhase),x1,NDim(KPhase))
            if(InvMag(KPhase).gt.0.and.NDim(KPhase).gt.3)
     1        x1(4)=x1(4)-s6(4,isym,isw,KPhase)
            do 1500jsym=isym,NSymm(KPhase)
              if(jsym.le.isym.or.isa(jsym,ia).lt.0) go to 1500
              call multm(rm6(1,jsym,isw,KPhase),xp,x2,NDim(KPhase),
     1                 NDim(KPhase),1)
              call AddVek(x2,s6(1,jsym,isw,KPhase),x3,NDim(KPhase))
              do m=1,NDimp
                pom=x1(m)-x3(m)
                if(InvMag(KPhase).gt.0.and.m.gt.3)
     1            pom=pom-s6(m,jsym,isw,KPhase)
                if(abs(pom-anint(pom)).gt..00001) go to 1500
              enddo
              isa(jsym,ia)=-isym
              if(isym.eq.1) AtSiteMult(ia)=AtSiteMult(ia)+1
              exit
1500        continue
          enddo
        enddo
      enddo
9999  do KPhase=1,NPhase
        if(ISymmPhase(KPhase).le.0) cycle
        call CrlRestoreSymmetry(ISymmPhase(KPhase))
        ISymmPhase(KPhase)=0
      enddo
      call CrlCleanSymmetry
      KPhase=KPhaseIn
      return
      end
