      subroutine CIFEditCrystal
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,sta(1),ErrSt,EdwStringQuest
      save nEdwShape,nEdwColor,nEdwMoietyFormula,nEdwSumFormula
      entry CIFEditCrystalMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      il=1
      tpom=5.
      Veta='Crystal %shape:'
      xpom=tpom+FeTxLengthUnder(Veta)+20.
      dpom=150.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwShape=EdwLastMade
      m=13
      n=11
      call CIFGetSt(CifKey(m,n),sta,mm,ErrSt,ich)
      if(sta(1).eq.'?'.or.sta(1).eq.'''?''') sta(1)=' '
      call FeQuestStringEdwOpen(EdwLastMade,sta)
      il=il+1
      Veta='Crystal %color:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwColor=EdwLastMade
      m=8
      call CIFGetSt(CifKey(m,n),sta,mm,ErrSt,ich)
      if(sta(1).eq.'?'.or.sta(1).eq.'''?''') sta(1)=' '
      call FeQuestStringEdwOpen(EdwLastMade,sta)
      il=1
      tpom=xqd*.5+5.
      Veta='%Moiety formula:'
      xpom=tpom+FeTxLengthUnder(Veta)+20.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMoietyFormula=EdwLastMade
      il=il+1
      m=21
      n=6
      call CIFGetSt(CifKey(m,n),sta,mm,ErrSt,ich)
      if(sta(1).eq.'?'.or.sta(1).eq.'''?''') sta(1)=' '
      call FeQuestStringEdwOpen(EdwLastMade,sta)
      Veta='%Sum formula:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwSumFormula=EdwLastMade
      m=23
      call CIFGetSt(CifKey(m,n),sta,mm,ErrSt,ich)
      if(sta(1).eq.'?'.or.sta(1).eq.'''?''') sta(1)=' '
      call FeQuestStringEdwOpen(EdwLastMade,sta)
      go to 9999
      entry CIFEditCrystalCheck
      go to 9999
      entry CIFEditCrystalUpdate
      Veta=EdwStringQuest(nEdwShape)
      m=13
      n=11
      l=1
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),
     1                                 ''''//Veta(:idel(Veta))//''''
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      Veta=EdwStringQuest(nEdwColor)
      m=8
      l=1
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),
     1                                 ''''//Veta(:idel(Veta))//''''
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      Veta=EdwStringQuest(nEdwMoietyFormula)
      m=21
      n=6
      l=1
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),
     1                                 ''''//Veta(:idel(Veta))//''''
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      Veta=EdwStringQuest(nEdwSumFormula)
      m=23
      l=1
      write(CIFArrayUpdate(1),FormCIF) CifKey(m,n),
     1                                 ''''//Veta(:idel(Veta))//''''
      call CIFUpdateRecord(CifKey(m,n),'CRYSTAL DATA',l)
      go to 9999
9999  return
      end
