      subroutine UpdateNumberOfCommands(n)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 t256
      ln=NextLogicNumber()
      t256=fln(:ifln)//'_'//NactiKeywords(n)(:idel(NactiKeywords(n)))//
     1     '.tmp'
      call OpenFile(ln,t256,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      NactiActive(n)=0
      NactiPassive(n)=0
1000  read(ln,FormA256,end=9000) t256
      if(t256(1:1).eq.'!') then
        NactiPassive(n)=NactiPassive(n)+1
      else if(t256(1:1).ne.'#') then
        NactiActive(n)=NactiActive(n)+1
      endif
      go to 1000
9000  call CloseIfOpened(ln)
9999  return
      end
