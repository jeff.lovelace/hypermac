      subroutine CrlCleanOldCommands
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do i=1,NAtAll
        call CrlAtomNamesSave(Atom(i),'#delete#',1)
      enddo
      call CrlAtomNamesApply
      call CrlAtomNamesIni
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      return
      end
