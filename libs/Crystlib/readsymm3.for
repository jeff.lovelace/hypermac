      subroutine ReadSymm3(Veta,rmp,sp)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmp(*),sp(*)
      character*80 t80
      character*(*) Veta
      call mala(Veta)
      call SetRealArrayTo(rmp,9,0.)
      k=0
      ij=0
      do 5000i=1,3
        ij=i-3
        call kus(veta,k,t80)
        idl=idel(t80)
        if(idl.le.0) go to 9000
        do j=1,idl
          if(t80(j:j).eq.'*') t80(j:j)=' '
        enddo
        call zhusti(t80)
        sp(i)=0.
        do 2000j=1,3
          ij=ij+3
          kk=index(t80,smbx(j))
          if(kk.eq.0) then
            rmp(ij)=0.
            go to 2000
          endif
          if(kk.gt.1) then
            kp=kk-1
1100        if(t80(kp:kp).ne.'+'.and.t80(kp:kp).ne.'-') then
              if(kp.eq.1) go to 1150
              kp=kp-1
              go to 1100
            endif
          else
            kp=kk
          endif
1150      if(kp.ne.kk) then
            cislo=t80(kp:kk-1)
            if(cislo.eq.'+') then
              l=1
            else if(cislo.eq.'-') then
              l=-1
            else
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) l
            endif
          else
            l=1
          endif
          rmp(ij)=l
          do l=kp,kk
            t80(l:l)=' '
          enddo
          call zhusti(t80)
          if(idel(t80).le.0) go to 5000
2000    continue
        sp(i)=fract(t80,ich)
        if(ich.ne.0) go to 9000
5000  continue
      go to 9999
9000  call FeChybne(-1.,-1.,'a syntactic error in the following '//
     1              'symmetry operator.',veta,SeriousError)
      ErrFlag=1
9999  return
      end
