      subroutine ReadFFFromM50(ffin,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Radka
      dimension ffin(*)
      j1=1
      j2=iabs(n)
      if(n.lt.0) ffin(1)=0.
      k=80
      do j=j1,j2
        if(k.ge.80) then
          read(m50,FormA,err=9000,end=9010) Radka
          k=0
        endif
        call kus(Radka,k,Cislo)
        call posun(Cislo,1)
        read(Cislo,101,err=9000) ffin(j)
      enddo
      go to 9999
9000  ErrFlag=1
      go to 9999
9010  ErrFlag=1
9999  return
101   format(f15.0)
      end
