      subroutine CrlMakeDCRED
      use Atoms_mod
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      character*256 Veta
      integer ihitw(6,18)
      logical Ranover
      UseEFormat91=.false.
      if(UseEFormat91) then
        Format91='(3i4,2e15.6,3i4,8f8.4, e15.6,i15)'
      else
        Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
      endif
      write(Format91(2:2),'(i1)') NDim(KPhase)
      call GenerQuest(-1,0)
      call iom50(0,0,fln(:ifln)//'.m50')
      if(allocated(KFixOrigin)) deallocate(KFixOrigin,AtFixOrigin,
     1                                     AtFixX4,PromAtFixX4,KFixX4)
      allocate(KFixOrigin(NPhase),AtFixOrigin(NPhase),AtFixX4(NPhase),
     1         PromAtFixX4(NPhase),KFixX4(NPhase))
      if(allocated(ScSup)) deallocate(ScSup,NSuper)
      allocate(ScSup(3,NPhase),NSuper(3,3,NPhase))
      call RefDefault
      IgnoreW=.true.
      IgnoreE=.true.
      call RefReadCommands
      IgnoreW=.false.
      IgnoreE=.false.
      if(TwMax.lt.TwDiff) TwMax=TwDiff
      Veta=fln(:ifln)//'.l91'
      call MoveFile(DCREDFile,Veta)
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      lno=NextLogicNumber()
      call OpenFile(lno,DCREDFile,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1500  read(ln,100,end=9999) ih(1:NDim(KPhase))
      if(ih(1).gt.900) go to 9999
      ihitw=0
      ihitw(1,1:NTwin)=999
      call settw(ih,ihitw,1,Ranover)
      do i=1,NTwin
        if(ihitw(1,i).lt.900) then
          write(lno,101) ih(1:NDim(KPhase))
          go to 1500
        endif
      enddo
      go to 1500
9999  call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      return
100   format(6i4)
101   format(6(i10,1x))
      end
