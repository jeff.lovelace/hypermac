      subroutine ComExp(sngc,csgc,n1,n2,n3)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension sngc(n1,n2,n3),csgc(n1,n2,n3)
      dimension xp(6),xpp(6),sxp(6),sxpp(6),kwz(3),kwp(3),kmodxp(3),
     1          phi(3),px(28),py(28),spx(28),spy(28)
      logical eqiv
      KPhaseIn=KPhase
      if(NAtCalc.le.0) go to 9999
      n=NAtCalcBasic
      if(NMolec.le.0) NAtAll=n
      NAtLastPom=0
      do KPh=1,NPhase
        KPhase=KPh
        nagcOff(KPh)=n
        nagcLen(KPh)=0
        do j=1,ngc(KPh)
          call SetIntArrayTo(kmodxp,3,1)
          do i=1,NAtCalcBasic
            if(kswa(i).ne.KPh) cycle
            n=n+1
            if(NMolec.gt.0) then
              if(n.ge.NAtMolFr(1,1)) then
                call ReallocateAtoms(100)
                call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+100)
                NAtLastPom=NAtMolFr(1,1)+99
                do k=NAtMolFr(1,1),NAtLastPom
                  call SetBasicKeysForAtom(k)
                enddo
                do k=1,NPhase
                  do l=1,NComp(k)
                    NAtMolFr(l,k)=NAtMolFr(l,k)+100
                    NAtMolTo(l,k)=NAtMolTo(l,k)+100
                  enddo
                  NAtMolFrAll(k)=NAtMolFrAll(k)+100
                  NAtMolToAll(k)=NAtMolToAll(k)+100
                enddo
                NAtAll=NAtAll+100
              endif
            else
              if(n.gt.MxAtAll) call ReallocateAtoms(100)
            endif
            NAtCalc=NAtCalc+1
            if(NMolec.le.0) NAtAll=NAtAll+1
            if(j.eq.1) nagcLen(KPh)=nagcLen(KPh)+1
            call CopyBasicKeysForAtom(i,n)
            write(atom(n)(7:8),'(i2)') j
            call zhusti(atom(n))
            PrvniKiAtomu(n)=PrvniKiAtomu(n-1)+DelkaKiAtomu(n-1)
            DelkaKiAtomu(n)=DelkaKiAtomu(i)
            if(NMolec.gt.0) then
              l=PrvniKiAtomu(n)+DelkaKiAtomu(n)
              do k=n+1,NAtLastPom
                PrvniKiAtomu(k)=l
              enddo
            endif
            isw=iswa(i)
            jsw=iswGC(j,isw,KPhase)
            iswa(n)=jsw
            kswa(n)=KPh
            kmodmx=0
            do k=1,7
              kmodmx=max(kmodmx,KModA(k,i))
            enddo
            call SetIntArrayTo(KiA(1,n),mxda,0)
            kmodmx=max(kmodmx,MagPar(i)-1)
            if(kmol(i).gt.0) im=(kmol(i)-1)/mxp+1
            phf(n)=phf(i)
            sphf(n)=sphf(i)
            MagPar(n)=MagPar(i)
            if(kmodmx.gt.0) then
              do 1100k=kmodxp(isw),kmodmx
                call MultMI(kw(1,k,KPh),GammaIntGC(1,j,isw,KPh),kwp,1,
     1                      NDimI(KPh),NDimI(KPh))
                do m=1,NDimI(KPh)
                  kwz(m)=-kwp(m)
                enddo
                do m=1,mxw
                  if(eqiv(kw(1,m,KPh),kwp,NDimI(KPh))) then
                    KwSymGC(k,j,isw,KPh)= m
                    go to 1100
                  else if(eqiv(kw(1,m,KPh),kwz,NDimI(KPh))) then
                    KwSymGC(k,j,isw,KPh)=-m
                    go to 1100
                  endif
                enddo
                call FeChybne(-1.,-1.,'some of modulation waves are not'
     1                      //' defined.','The transformation cannot be'
     2                      //' performed.',SeriousError)
                ErrFlag=1
                go to 9999
1100          continue
              kmodxp(isw)=kmodmx
            endif
            call CopyVek( x(1,i), xp,3)
            call CopyVek(sx(1,i),sxp,3)
            call SetRealArrayTo( xp(4),NDimI(KPh),0.)
            call SetRealArrayTo(sxp(4),NDimI(KPh),0.)
            call MultM (rm6gc(1,j,isw,KPh),xp,xpp,NDim(KPh),NDim(KPh),1)
            call MultMQ(rm6gc(1,j,isw,KPh),sxp,sxpp,NDim(KPh),NDim(KPh),
     1                  1)
            do k=1,NDim(KPh)
              xp(k)=xpp(k)+s6gc(k,j,isw,KPh)
              if(k.lt.4) then
                x(k,n)=xp(k)
                sx(k,n)=sxpp(k)
              else
                phi(k-3)=-xp(k)*pi2
              endif
            enddo
            if(kmol(n).le.0) then
              call qbyx(x(1,n),qcnt(1,n),jsw)
            else
              do k=1,NDim(KPh)
                if(k.le.3) then
                  xp(k)=xm(k,im)+trans(k,kmol(n))
                else
                  xp(k)=0.
                endif
              enddo
              call MultM(rm6gc(1,j,isw,KPh),xp,xpp,NDim(KPh),NDim(KPh),
     1                   1)
              call AddVek(xpp,s6gc(1,j,isw,KPh),xp,NDim(KPh))
              call qbyx(xp,qcnt(1,n),jsw)
            endif
            do k=1,kmodmx
              if(TypeModFun(i).le.1) then
                l=iabs(KwSymGC(k,j,isw,KPh))
                pom=0.
                do m=1,NDimI(KPh)
                  pom=pom+float(kw(m,l,KPh))*phi(m)
                enddo
                sngc(k,j,i)=sin(pom)
                csgc(k,j,i)=cos(pom)
              else
                sngc(k,j,i)=0.
                csgc(k,j,i)=1.
              endif
            enddo
            if(itf(n).le.1) then
              call CopyVek(beta(1,i),beta(1,n),6)
              call CopyVek(sbeta(1,i),sbeta(1,n),6)
            endif
            if(MagPar(i).le.0) then
              itfim=itf(n)
            else
              itfim=itf(n)+1
            endif
            do nn=0,itfim
              if(nn.gt.itf(n)) then
                nrank=3
                kfap=0
                kmodp=MagPar(i)-1
              else
                nrank=TRank(nn)
                kfap=KFA(nn+1,i)
                kmodp=KModA(nn+1,i)
              endif
              if(nn.gt.itf(n)) then
                call MultM (RMagGC(1,j,isw,KPh), sm0(1,i), sm0(1,n),
     1                      nrank,nrank,1)
                call MultMQ(RMagGC(1,j,isw,KPh),ssm0(1,i),ssm0(1,n),
     1                      nrank,nrank,1)
              else if(nn.eq.0) then
                 ai(n)= ai(i)
                sai(n)=sai(i)
                a0 (n)= a0(i)
                sa0(n)=sa0(i)
              else if(nn.eq.2) then
                call MultM (rtgc(1,j,isw,KPh), beta(1,i), beta(1,n),
     1                      nrank,nrank,1)
                call MultMQ(rtgc(1,j,isw,KPh),sbeta(1,i),sbeta(1,n),
     1                      nrank,nrank,1)
                call SetRealArrayTo(sbeta(1,n),6,0.)
              else if(nn.eq.3) then
                call MultM (rc3gc(1,j,isw,KPh), c3(1,i), c3(1,n),nrank,
     1                     nrank,1)
                call MultMQ(rc3gc(1,j,isw,KPh),sc3(1,i),sc3(1,n),nrank,
     1                     nrank,1)

              else if(nn.eq.4) then
                call MultM (rc4gc(1,j,isw,KPh), c4(1,i), c4(1,n),nrank,
     1                     nrank,1)
                call MultMQ(rc4gc(1,j,isw,KPh),sc4(1,i),sc4(1,n),nrank,
     1                     nrank,1)
              else if(nn.eq.5) then
                call MultM (rc5gc(1,j,isw,KPh), c5(1,i), c5(1,n),nrank,
     1                     nrank,1)
                call MultMQ(rc5gc(1,j,isw,KPh),sc5(1,i),sc5(1,n),nrank,
     1                     nrank,1)
              else if(nn.eq.5) then
                call MultM (rc6gc(1,j,isw,KPh), c6(1,i), c6(1,n),nrank,
     1                     nrank,1)
                call MultMQ(rc6gc(1,j,isw,KPh),sc6(1,i),sc6(1,n),nrank,
     1                     nrank,1)
              endif
              do k=1,kmodp
                iw=iabs(KwSymGC(k,j,isw,KPh))
                snp=sngc(k,j,i)
                csp=csgc(k,j,i)
                epsp=isign(1,KwSymGC(k,j,isw,KPh))
                if(nn.gt.itf(n)) then
                  call MultM (RMagGC(1,j,isw,KPh), smx(1,k,i), px,nrank,
     1                        nrank,1)
                  call MultMQ(RMagGC(1,j,isw,KPh),ssmx(1,k,i),spx,nrank,
     1                        nrank,1)
                  call MultM (RMagGC(1,j,isw,KPh), smy(1,k,i), py,nrank,
     1                        nrank,1)
                  call MultMQ(RMagGC(1,j,isw,KPh),ssmy(1,k,i),spy,nrank,
     1                        nrank,1)
                else if(nn.eq.0) then
                   px(1)= ax(k,i)
                   py(1)= ay(k,i)
                  spx(1)=sax(k,i)
                  spy(1)=say(k,i)
                else if(nn.eq.1) then
                  call MultM (rmgc(1,j,isw,KPh), ux(1,k,i), px,nrank,
     1                        nrank,1)
                  call MultMQ(rmgc(1,j,isw,KPh),sux(1,k,i),spx,nrank,
     1                        nrank,1)
                  if(kfap.eq.0.or.k.ne.kmodp) then
                    call MultM (rmgc(1,j,isw,KPh), uy(1,k,i), py,nrank,
     1                          nrank,1)
                    call MultMQ(rmgc(1,j,isw,KPh),suy(1,k,i),spy,nrank,
     1                          nrank,1)
                  endif
                else if(nn.eq.2) then
                  call MultM (rtgc(1,j,isw,KPh), bx(1,k,i), px,nrank,
     1                        nrank,1)
                  call MultMQ(rtgc(1,j,isw,KPh),sbx(1,k,i),spx,nrank,
     1                        nrank,1)
                  if(kfap.eq.0.or.k.ne.kmodp) then
                    call MultM (rtgc(1,j,isw,KPh), by(1,k,i), py,nrank,
     1                          nrank,1)
                    call MultMQ(rtgc(1,j,isw,KPh),sby(1,k,i),spy,nrank,
     1                          nrank,1)
                  endif
                else if(nn.eq.3) then
                  call MultM (rc3gc(1,j,isw,KPh), c3x(1,k,i), px,nrank,
     1                        nrank,1)
                  call MultMQ(rc3gc(1,j,isw,KPh),sc3x(1,k,i),spx,nrank,
     1                        nrank,1)
                  call MultM (rc3gc(1,j,isw,KPh), c3y(1,k,i), py,nrank,
     1                        nrank,1)
                  call MultMQ(rc3gc(1,j,isw,KPh),sc3y(1,k,i),spy,nrank,
     1                        nrank,1)
                else if(nn.eq.4) then
                  call MultM (rc4gc(1,j,isw,KPh), c4x(1,k,i), px,nrank,
     1                        nrank,1)
                  call MultMQ(rc4gc(1,j,isw,KPh),sc4x(1,k,i),spx,nrank,
     1                        nrank,1)
                  call MultM (rc4gc(1,j,isw,KPh), c4y(1,k,i), py,nrank,
     1                        nrank,1)
                  call MultMQ(rc4gc(1,j,isw,KPh),sc4y(1,k,i),spy,nrank,
     1                        nrank,1)
                else if(nn.eq.5) then
                  call MultM (rc5gc(1,j,isw,KPh), c5x(1,k,i), px,nrank,
     1                        nrank,1)
                  call MultMQ(rc5gc(1,j,isw,KPh),sc5x(1,k,i),spx,nrank,
     1                        nrank,1)
                  call MultM (rc5gc(1,j,isw,KPh), c5y(1,k,i), py,nrank,
     1                        nrank,1)
                  call MultMQ(rc5gc(1,j,isw,KPh),sc5y(1,k,i),spy,nrank,
     1                        nrank,1)
                else if(nn.eq.5) then
                  call MultM (rc6gc(1,j,isw,KPh), c6x(1,k,i), px,nrank,
     1                        nrank,1)
                  call MultMQ(rc6gc(1,j,isw,KPh),sc6x(1,k,i),spx,nrank,
     1                        nrank,1)
                  call MultM (rc6gc(1,j,isw,KPh), c6y(1,k,i), py,nrank,
     1                        nrank,1)
                  call MultMQ(rc6gc(1,j,isw,KPh),sc6y(1,k,i),spy,nrank,
     1                        nrank,1)
                endif
                if(kfap.eq.0.or.k.ne.kmodp) then
                  do m=1,nrank
                    ppx=epsp*csp*px(m)-snp*py(m)
                    ppy=epsp*snp*px(m)+csp*py(m)
                    sppx=sqrt((epsp*csp*spx(m))**2+(snp*spy(m))**2)
                    sppy=sqrt((epsp*snp*spx(m))**2+(csp*spy(m))**2)
                    if(nn.gt.itf(n)) then
                       smx(m,iw,n)= ppx
                       smy(m,iw,n)= ppy
                      ssmx(m,iw,n)=sppx
                      ssmy(m,iw,n)=sppy
                    else if(nn.eq.0) then
                       ax(iw,n)= ppx
                       ay(iw,n)= ppy
                      sax(iw,n)=sppx
                      say(iw,n)=sppy
                    else if(nn.eq.1) then
                       ux(m,iw,n)= ppx
                       uy(m,iw,n)= ppy
                      sux(m,iw,n)=sppx
                      suy(m,iw,n)=sppy
                    else if(nn.eq.2) then
                       bx(m,iw,n)= ppx
                       by(m,iw,n)= ppy
                      sbx(m,iw,n)=sppx
                      sby(m,iw,n)=sppy
                    else if(nn.eq.3) then
                       c3x(m,iw,n)= ppx
                       c3y(m,iw,n)= ppy
                      sc3x(m,iw,n)=sppx
                      sc3y(m,iw,n)=sppy
                    else if(nn.eq.4) then
                       c4x(m,iw,n)= ppx
                       c4y(m,iw,n)= ppy
                      sc4x(m,iw,n)=sppx
                      sc4y(m,iw,n)=sppy
                    else if(nn.eq.5) then
                       c5x(m,iw,n)= ppx
                       c5y(m,iw,n)= ppy
                      sc5x(m,iw,n)=sppx
                      sc5y(m,iw,n)=sppy
                    else
                       c6x(m,iw,n)= ppx
                       c6y(m,iw,n)= ppy
                      sc6x(m,iw,n)=sppx
                      sc6y(m,iw,n)=sppy
                    endif
                  enddo
                else
                  if(nn.eq.0) then
                    call CopyVek(x(1,i),xp,3)
                    xp(4)=ax(k,i)
                    call MultM(rm6gc(1,j,isw,KPh),xp,xpp,NDim(KPh),
     1                         NDim(KPh),1)
                    call AddVek(xpp,s6gc(1,j,isw,KPh),xp,NDim(KPh))
                    pom=xp(4)
                    m=pom
                    if(pom.lt.0.) m=m-1
                    ax(k,n)=pom-float(m)
                    ay(k,n)=ay(k,i)
                    sax(k,n)=sax(k,i)
                    say(k,n)=say(k,i)
                  else if(nn.eq.1) then
                    call MultM(rmgc(1,j,isw,KPh),ux(1,k,i),px,3,3,1)
                    do m=1,3
                      ux(m,k,n)=epsp*px(m)
                    enddo
                    call CopyVek(x(1,i),xp,3)
                    xp(4)=uy(1,k,i)
                    call MultM(rm6gc(1,j,isw,KPh),xp,xpp,NDim(KPh),
     1                         NDim(KPh),1)
                    call AddVek(xpp,s6gc(1,j,isw,KPh),xp,NDim(KPh))
                    pom=xp(4)
                    m=pom
                    if(pom.lt.0.) m=m-1
                    uy(1,k,n)=pom-float(m)
                    uy(2,k,n)=uy(2,k,i)
                    suy(1,k,n)=suy(1,k,i)
                    suy(2,k,n)=suy(2,k,i)
                  else if(nn.eq.2) then
                    call MultM(rtgc(1,j,isw,KPh),bx(1,k,i),px,6,6,1)
                    do m=1,3
                      bx(m,k,n)=epsp*px(m)
                    enddo
                    pom=epsp*by(1,k,i)+xp(4)
                    m=pom
                    if(pom.lt.0.) m=m-1
                    by(1,k,n)=pom-float(m)
                    by(2,k,n)=by(2,k,i)
                    sby(1,k,n)=sby(1,k,i)
                    sby(2,k,n)=sby(2,k,i)
                  endif
                endif
              enddo
            enddo
          enddo
        enddo
      enddo
      if(allocated(OrthoSel)) deallocate(OrthoSel)
      do iat=1,NAtAll
        if(TypeModFun(iat).eq.1) then
          if(.not.allocated(OrthoSel))
     1      allocate(OrthoSel(OrthoOrd,NAtAll+1))
          call UpdateOrtho(iat,OrthoX40(iat),OrthoDelta(iat),
     1                     OrthoEps(iat),OrthoSel(1,iat),
     2                     OrthoOrd)
        endif
      enddo
      if(OrthoOrd.gt.0) then
        if(allocated(OrthoMat)) deallocate(OrthoMat,OrthoMatI)
        m=OrthoOrd**2
        allocate(OrthoMat(m,NAtAll+1),OrthoMatI(m,NAtAll+1))
        do iat=1,NAtAll
          if(TypeModFun(iat).eq.1) then
            call MatOr(OrthoX40(iat),OrthoDelta(iat),
     1                 OrthoSel(1,iat),OrthoOrd,OrthoMat(1,iat),
     2                 OrthoMatI(1,iat),iat)
          else
            call SetIntArrayTo(OrthoSel(1,iat),OrthoOrd,1)
            call UnitMat(OrthoMat (1,iat),OrthoOrd)
            call UnitMat(OrthoMatI(1,iat),OrthoOrd)
          endif
        enddo
      endif
      PosledniKiAtPos=PrvniKiAtomu(n)+DelkaKiAtomu(n)-1
      call SpecAt
      if(NMolec.gt.0) then
        do i=NAtMolFr(1,1),NAtAll
          if(i.eq.NAtMolFr(1,1)) then
            PrvniKiAtomu(i)=PosledniKiAtPos+1
            PrvniKiAtMol=PrvniKiAtomu(i)
          else
            PrvniKiAtomu(i)=PrvniKiAtomu(i-1)+DelkaKiAtomu(i-1)
          endif
        enddo
        PosledniKiAtMol=PrvniKiAtomu(NAtAll)+DelkaKiAtomu(NAtAll)-1
        PrvniKiMol=PosledniKiAtMol+1
        do i=1,NMolec
          do j=1,mam(i)
            ji=j+(i-1)*mxp
            if(ji.eq.1) then
              PrvniKiMolekuly(ji)=PrvniKiMol
            else
              PrvniKiMolekuly(ji)=PosledniKiMol+1
            endif
            PosledniKiMol=PrvniKiMolekuly(ji)+DelkaKiMolekuly(ji)-1
          enddo
        enddo
      else
        PrvniKiMol=PosledniKiAtPos+1
        PosledniKiMol=PrvniKiMol-1
      endif
      PrvniKiAtXYZMode=PosledniKiMol+1
      PosledniKiAtXYZMode=PosledniKiMol
      PrvniKiAtMagMode=PosledniKiMol+1
      PosledniKiAtMagMode=PosledniKiMol
9999  KPhase=KPhaseIn
      return
      end
