      subroutine CrlCompIntSymm(NDimNew,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension pp(3),pq(3),mpp(3)
      ich=0
      if(NDimNew.le.3) go to 9999
      if(NDimNew.gt.3) then
        k1=0
        k2=0
        n1=0
        n2=0
        if(NDimNew.gt.4) then
          k1=-2
          k2= 2
          if(NDimNew.gt.5) then
            n1=-2
            n2= 2
          endif
        endif
      endif
      do is=1,NSymm(KPhase)
        call MatFromBlock3(rm(1,is,1,KPhase),rm6(1,is,1,KPhase),NDimNew)
        do 1600i=1,NDimNew-3
          call MultM(qu(1,i,1,KPhase),rm(1,is,1,KPhase),pp,1,3,3)
          jknMn=999999
          do n=n1,n2
            do k=k1,k2
              do 1560j=-4,4
                mmpa=0
                do l=1,3
                  pq(l)=float(j)*qu(l,1,1,KPhase)-pp(l)
                  if(NDimNew.gt.4)
     1              pq(l)=pq(l)+float(k)*qu(l,2,1,KPhase)
                  if(NDimNew.gt.5)
     1              pq(l)=pq(l)+float(n)*qu(l,3,1,KPhase)
                  mpp(l)=nint(pq(l))
                  mmpa=mmpa+iabs(mpp(l))
                  if(abs(pq(l)-float(mpp(l))).gt..0005) go to 1560
                enddo
                do ivt=1,NLattVec(KPhase)
                  pom=ScalMul(vt6(1,ivt,1,KPhase),pq)
                  if(abs(pom-anint(pom)).gt..0005) go to 1560
                enddo
                jkn=iabs(j)+iabs(k)+iabs(n)+mmpa
                if(jkn.lt.jknMn) then
                  ip3=i+3
                  do l=1,3
                    rm6(ip3+NDimNew*(l-1),is,1,KPhase)=-mpp(l)
                  enddo
                  rm6(ip3+NDimNew*3,is,1,KPhase)=j
                  if(NDimNew.gt.4)
     1              rm6(ip3+NDimNew*4,is,1,KPhase)=k
                  if(NDimNew.gt.5)
     1              rm6(ip3+NDimNew*5,is,1,KPhase)=n
                  jknMn=jkn
                endif
1560          continue
            enddo
          enddo
          if(jknMn.ge.999999) then
            call FeChybne(-1.,-1.,'inconsisteny between average '//
     1                    'symmetry and modulation vector.',' ',
     2                    SeriousError)
            ich=1
            go to 9999
          endif
1600    continue
        call SetRealArrayTo(s6(4,is,1,KPhase),NDimNew-3,0.)
      enddo
      do ivt=1,NLattVec(KPhase)
        call SetRealArrayTo(VT6(4,ivt,1,KPhase),NDimNew-3,0.)
      enddo
9999  return
      end
