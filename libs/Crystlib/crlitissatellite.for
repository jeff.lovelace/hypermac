      logical function CrlItIsSatellite(ih)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ih(6),ihp(6)
      if(NDim(KPhase).gt.3) then
        CrlItIsSatellite=.true.
        do 2000i=1,NComp(KPhase)
          call indtr(ih,zvi(1,i,KPhase),ihp,NDim(KPhase))
          do j=4,NDim(KPhase)
            if(iabs(ihp(j)).gt.0) go to 2000
          enddo
          CrlItIsSatellite=.false.
          go to 9999
2000    continue
      else
        CrlItIsSatellite=.false.
      endif
9999  return
      end
