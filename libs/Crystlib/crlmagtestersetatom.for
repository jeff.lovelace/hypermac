      subroutine CrlMagTesterSetAtom(ia,Value,Randomly)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(3)
      integer Seed(1)
      logical Randomly
      logical :: Prvne = .true.
      ValueLimit=Value*.1
      xp=1.
      if(Prvne.and.Randomly) then
        call Random_Seed()
        Prvne=.false.
      endif
      isw=iswa(ia)
      if(NDimI(KPhase).le.0) then
1100    if(Randomly) then
          do j=1,3
            call Random_Number(xp(j))
            xp(j)=2.*xp(j)-1.
          enddo
          call VecOrtNorm(xp,3)
        endif
        do j=1,3
          sm0(j,ia)=Value*xp(j)
          if(Randomly.and.abs(sm0(j,ia)).lt.ValueLimit) go to 1100
          sm0(j,ia)=sm0(j,ia)/CellPar(j,1,KPhase)
        enddo
      else
        n=(MagPar(ia)-1)*6+3
        kif=DelkaKiAtomu(ia)-n+1
        call SetIntArrayTo(KiA(kif,ia),n,0)
1200    if(Randomly) then
          do j=1,3
            call Random_Number(xp(j))
            xp(j)=2.*xp(j)-1.
          enddo
          call VecOrtNorm(xp,3)
        endif
        do j=1,3
          sm0(j,ia)=Value*xp(j)
          if(Randomly.and.abs(sm0(j,ia)).lt.ValueLimit) go to 1200
          sm0(j,ia)=sm0(j,ia)/CellPar(j,1,KPhase)
        enddo
        call SetIntArrayTo(KiA(kif,ia),3,1)
        kip=kif
        do k=1,MagPar(ia)-1
          kip=kip+3
1300      if(Randomly) then
            do j=1,3
              call Random_Number(xp(j))
              xp(j)=2.*xp(j)-1.
            enddo
            call VecOrtNorm(xp,3)
          endif
          do j=1,3
            smx(j,k,ia)=Value*xp(j)
            if(Randomly.and.abs(smx(j,k,ia)).lt.ValueLimit) go to 1300
            smx(j,k,ia)=smx(j,k,ia)/CellPar(j,isw,KPhase)
          enddo
          call SetIntArrayTo(KiA(kip,ia),3,1)
          kip=kip+3
1400      do j=1,3
            call Random_Number(xp(j))
          enddo
          call VecOrtNorm(xp,3)
          do j=1,3
            smy(j,k,ia)=Value*xp(j)
            if(Randomly.and.abs(smy(j,k,ia)).lt.ValueLimit) go to 1400
            smy(j,k,ia)=smy(j,k,ia)/CellPar(j,isw,KPhase)
          enddo
          call SetIntArrayTo(KiA(kip,ia),3,1)
        enddo
      endif
      return
      end
