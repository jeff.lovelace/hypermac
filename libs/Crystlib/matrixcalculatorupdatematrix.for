      subroutine MatrixCalculatorUpdateMatrix(RMat,NRow,NCol,
     1                                        RMatNew,NRowNew,NColNew,
     2                                        ich)
      include 'fepc.cmn'
      logical FeYesNoHeader
      dimension RMat(*),RMatNew(*)
      character*80 Veta
      ich=0
      if(NRow.ne.NRowNew.or.NCol.ne.NColNew) then
        NInfo=2
        write(Cislo,100) NRowNew,NColNew
        TextInfo(1)='The imported matrix has dimension '//
     1              Cislo(:idel(Cislo))//
     2              ' whereas the edited matrix'
        write(Cislo,100) NRow,NCol
        call Zhusti(Cislo)
        TextInfo(1)=TextInfo(1)(:idel(TextInfo(1))+1)//
     1              Cislo(:idel(Cislo))//'.'
        TextInfo(2)='The recall action will rewrite the '//
     1              'elements corresponing to the saved matrix.'
        Veta='Do you want read to recall the saved matrix anyhow?'
        if(.not.FeYesNoHeader(-1.,YBottomMessage,Veta,1)) then
          ich=1
          go to 9999
        endif
      endif
      do i=1,NRow
        if(i.gt.NRowNew) cycle
        do j=1,NCol
          if(j.gt.NColNew) cycle
          RMat(i+(j-1)*NRow)=RMatNew(i+(j-1)*NRowNew)
        enddo
      enddo
9999  return
100   format('[',i1,',',i1,']')
      end
