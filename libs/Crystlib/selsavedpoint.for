      subroutine SelSavedPoint(Text,Ktery,xs)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Text
      dimension xs(*)
      character*80 Veta
      integer UseTabsIn,SbwItemPointerQuest
      Ktery=0
      if(NSavedPoints.eq.1) then
        call CopyVek(XSavedPoint,xs,3)
        Ktery=1
        go to 9999
      endif
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      xpom=80.
      do i=1,3
        call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        xpom=xpom+70.
      enddo
      id=NextQuestId()
      xqd=330.
      il=max(NSavedPoints+1,7)
      call FeQuestCreate(id,-1.,-1.,xqd,il,Text,0,LightGray,0,0)
      il=1
      xpom=10.
      call FeQuestLblMake(id,xpom,il,'Label','L','N')
      xpom=100.
      do i=1,3
        call FeQuestLblMake(id,xpom,il,SmbX(i),'L','N')
        xpom=xpom+70.
      enddo
      il=NSavedPoints+1
      xpom=7.
      dpom=xqd-14.-SbwPruhXd
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextNone,
     1                    SbwVertical)
      nSbw=SbwLastMade
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_points.tmp','formatted','unknown')
      do i=1,NSavedPoints
        write(Veta,'(a8,3(a1,f10.6))') StSavedPoint(i),
     1    (Tabulator,XSavedPoint(j,i),j=1,3)
        call Zhusti(Veta)
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_points.tmp')
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        Ktery=SbwItemPointerQuest(nSbw)
        call CopyVek(XSavedPoint(1,Ktery),xs,3)
      endif
      call FeQuestRemove(id)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
9999  return
      end
