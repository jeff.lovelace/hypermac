      subroutine CrlCorrectAtomNames(ich)
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*80 t80
      character*8 At
      character*2 nty
      logical EqIgCase,WizardModeIn
      WizardModeIn=WizardMode
      WizardMode=.false.
      namax=NAtInd+NAtMol
      xqd=500.
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,11,'Atom names '//
     1                   ' are not unique, please make corrections:',1,
     2                   LightGray,0,0)
      iap=1
      iak=min(NAtInd,30)
      dpom=FeTxLength('XXXXXXXXX')+10.
      if(namax.gt.30) then
        xpom=xqd-dpom-15.
        il=1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Next')
        nButtNext=ButtonLastMade
        il=10
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Previous')
        nButtPrevious=ButtonLastMade
      endif
      tpom=45.
      xpom=50.
      il=0
      dpom=dpom+2.*EdwMarginSize
      do i=1,30
        write(t80,100) i,nty(i)
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,t80,'R',dpom,EdwYd,1)
        if(i.eq.1) nEdwAtomFirst=EdwLastMade
        if(il.eq.10) then
          il=0
          xpom=xpom+55.+dpom
          tpom=tpom+55.+dpom
        endif
      enddo
      il=11
      t80='%Rename to "atom_type"+number'
      at='R%eset'
      dpom=FeTxLengthUnder(t80)+10.
      tpom=FeTxLengthUnder(At)+10.
      xpom=(xqd-dpom-tpom-10.)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,t80)
      nButtRename=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call FeQuestButtonMake(id,xpom+dpom+10.,il,tpom,ButYd,at)
      nButtReset=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1220  nEdw=nEdwAtomFirst
      do i=iap,iap+29
        if(i.le.NAtInd) then
          j=i
        else
          j=NAtMolFr(1,1)-1+i-NAtInd
        endif
        if(i.le.namax) then
          call FeQuestStringEdwOpen(nEdw,Atom(j))
        else
          call FeQuestEdwClose(nEdw)
        endif
        nEdw=nEdw+1
      enddo
1250  if(namax.gt.30) then
        if(iak.lt.namax) then
          call FeQuestButtonOff(nButtNext)
        else
          call FeQuestButtonDisable(nButtNext)
        endif
        if(iap.gt.1) then
          call FeQuestButtonOff(nButtPrevious)
        else
          call FeQuestButtonDisable(nButtPrevious)
        endif
      endif
      do j=iap,iak
        if(j.le.NAtInd) then
          k=j
        else
          k=j-NAtInd+NAtMolFr(1,1)-1
        endif
        At=Atom(k)
        write(t80,100) j,nty(j)
        do i=1,j-1
          if(i.le.NAtInd) then
            l=i
          else
            l=i-NAtInd+NAtMolFr(1,1)-1
          endif
          if(EqIgCase(At,Atom(l))) then
            do k=1,idel(t80)
              if(t80(k:k).ne.' ') then
                t80='**'//t80(k:idel(t80))
                go to 1305
              endif
            enddo
            go to 1305
          endif
        enddo
1305    call FeQuestEdwLabelChange(j+nEdwAtomFirst-iap,t80)
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do j=1,namax
          if(j.le.NAtInd) then
            k=j
          else
            k=j-NAtInd+NAtMolFr(1,1)-1
          endif
          At=Atom(k)
          do i=1,j-1
            if(i.le.NAtInd) then
              l=i
            else
              l=i-NAtInd+NAtMolFr(1,1)-1
            endif
            if(EqIgCase(At,Atom(l))) then
              call FeChybne(-1.,-1.,'problem was not yet fully solved.',
     1                      ' ',SeriousError)
              go to 1500
            endif
          enddo
        enddo
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventEdw) then
        i=CheckNumber-nEdwAtomFirst+iap
        if(i.le.NAtInd) then
          k=i
        else
          k=i-NAtInd+NAtMolFr(1,1)-1
        endif
        At=EdwStringQuest(CheckNumber)
        call UprAt(At)
        call FeQuestStringEdwOpen(CheckNumber,At)
        if(EqIgCase(At,Atom(k))) then
          go to 1500
        else
          Atom(k)=At
          go to 1250
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPrevious.or.
     2         CheckNumber.eq.nButtNext)) then
        if(CheckNumber.eq.nButtNext) then
          iap=iap+30
        else
          iap=iap-30
        endif
        iap=max(iap,1)
        iap=min(iap,namax)
        iak=min(iap+29,namax)
        go to 1220
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRename)
     1  then
        call RenameAtomsAccordingToAtomType(NAtAll)
        go to 1220
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        call iom40(0,0,fln(:ifln)//'.m40')
        go to 1220
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      if(ich.eq.0) then
        call iom40(1,0,fln(:ifln)//'.m40')
      else
        call iom40(0,0,fln(:ifln)//'.m40')
      endif
      WizardMode=WizardModeIn
      return
100   format(i5,a2)
      end
