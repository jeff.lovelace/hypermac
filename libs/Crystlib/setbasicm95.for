      subroutine SetBasicM95(KRefB)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      NRuns(KRefB)=0
      RunScGr(KRefB)=0
      RunScN(KRefB)=0
      RunCCDMax(KRefB)=0
      SpHEven(KRefB)=0
      SpHOdd(KRefB)=0
      ScFrMethod(KRefB)=ScFrMethodStepLike
      SourceFileRefBlock(KRefB)=' '
      SourceFileTimeRefBlock(KRefB)=' '
      SourceFileDateRefBlock(KRefB)=' '
      InsParFileRefBlock(KRefB)=' '
      IncSpectFileRefBlock(KRefB)=' '
      FormatRefBlock(KRefB)='(3i4,2f8.2)'
      RefDatCorrespond(KRefB)=0
      CorrLp(KRefB)=-1
      CorrAbs(KRefB)=-1
      NRef95(KRefB)=0
      NLines95(KRefB)=0
      NFaces(KRefB)=0
      DelMi(KRefB)=0.
      RadiusRefBlock(KRefB)=0.
      AmiRefBlock(KRefB)=-1.
      TempRefBlock(KRefB)=293.
      RadiusRefBlock(KRefB)=0.1
      ImportOnlySatellites(KRefB)=.false.
      if(KRefB.gt.1) then
        DifCode(KRefB)=DifCode(KRefB-1)
      else
        DifCode(KRefB)=IdKumaCCD
      endif
      call SetRealArrayTo(CellRefBlock(1,KRefB),3,10.)
      call SetRealArrayTo(CellRefBlock(4,KRefB),3,90.)
      call SetRealArrayTo(CellRefBlockSU(1,KRefB),6,0.)
      call SetIntArrayTo(GaussRefBlock(1,KRefB),3,10)
      MMaxRefBlock(KRefB)=4
      call SetRealArrayTo(DiffSatRefBlock(1,KRefB),3,.001)
      RadiationRefBlock(KRefB)=XRayRadiation
      PolarizationRefBlock(KRefB)=PolarizedMonoPer
      FractPerfMonRefBlock(KRefB)=.5
      AngleMonRefBlock(KRefB)=0.
      AlphaGMonRefBlock(KRefB)=0.
      BetaGMonRefBlock(KRefB)=0.
      LamAveRefBlock(KRefB)=-1.
      LamA1RefBlock(KRefB)=-1.
      LamA2RefBlock(KRefB)=-1.
      LamRatRefBlock(KRefB)=.5
      PwdMethodRefBlock(KRefB)=0
      TOFJasonRefBlock(KRefB)=0
      TOFTThRefBlock(KRefB)=-3333.
      TOFDifcRefBlock(KRefB)=-3333.
      TOFDifaRefBlock(KRefB)=-3333.
      TOFZeroRefBlock(KRefB)=-3333.
      TOFCeRefBlock(KRefB)=-3333.
      TOFZeroeRefBlock(KRefB)=-3333.
      TOFTCrossRefBlock(KRefB)=-3333.
      TOFWCrossRefBlock(KRefB)=-3333.
      call SetRealArrayTo(TOFSigmaRefBlock(1,KRefB),3,-3333.)
      call SetRealArrayTo(TOFGammaRefBlock(1,KRefB),3,-3333.)
      call SetRealArrayTo(TOFAlfbeRefBlock(1,KRefB),4,-3333.)
      call SetRealArrayTo(TOFAlfbtRefBlock(1,KRefB),4,-3333.)
      EDSlopeRefBlock(KRefB)=-3333.
      EDOffsetRefBlock(KRefB)=-3333.
      EDTThRefBlock(KRefB)=-3333.
      KLamRefBlock(KRefB)=0
      NAlfaRefBlock(KRefB)=1
      ScaleRefBlock(KRefB)=1.
      XDDirCosRefBlock(KRefB)=0
      if(KRefB.eq.1.and..not.ExistM50) then
        NDim95(KRefB)=3
      else
        NDim95(KRefB)=NDim(KPhase)
      endif
      ITwRead(KRefB)=1
      ScMaxRead(KRefB)=1
      CellRefBlock(1,KRefB)=-1.
      CellReadIn(KRefB)=.false.
      LamAveRefBlock(KRefB)=-1.
      call SetRealArrayTo(QuRefBlock(1,1,KRefB),9,-333.)
      UseTrRefBlock(KRefB)=.false.
      call UnitMat(TrRefBlock(1,KRefB),NDim(KPhase))
      call SetRealArrayTo(ub(1,1,KRefB),9,-333.)
      HKLF5RefBlock(KRefB)=0
      CutMinAve(KRefB)=3.
      CutMaxAve(KRefB)=-333.
      FLimCullAve(KRefB)=-333.
      NRefMaxAve(KRefB)=-333
      PGAve(KRefB)=' '
      UseExpCorr(KRefB)=1
      MagFieldDirRefBlock(1:3,KRefB)=-333.
      MagPolFlagRefBlock(KRefB)=0
      MagPolPlusRefBlock(KRefB)=-333.
      MagPolMinusRefBlock(KRefB)=-333.
      return
      end
