      subroutine CrlMagTesterAction(Action,SvFile,AtMag,StMag,NAtMag,
     1                              NMagPar)
      use Atoms_mod
      use Basic_mod
      use Refine_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*(*) SvFile
      character*256 Veta
      character*80 t80
      character*80 StMag(6,0:NMagPar,NAtMag)
      character*8  At,At1,AtMag(NAtMag)
      integer Action,UseTabsIn
      logical EqIgCase,RefineEnd,SilentRunIn
      UseTabsIn=UseTabs
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      do i=1,NAtCalc
        if(kswa(i).ne.KPhase) cycle
        if(MagPar(i).ne.0)
     1    call CrlMagTesterSetAtom(i,2.,.true.)
      enddo
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom40Only(1,0,fln(:ifln)//'.m40')
      if(FromRepAnalysis.or.FromGoToSub) WizardMode=.false.
      ShowInfoOnScreen=.false.
      SilentRunIn=SilentRun
      SilentRun=.false.
      call RefOpenCommands
      lni=NextLogicNumber()
      open(lni,file=fln(:ifln)//'_fixed.tmp')
      lno=NextLogicNumber()
      open(lno,file=fln(:ifln)//'_fixed_new.tmp')
1200  read(lni,FormA,end=1220) Veta
      write(lno,FormA) Veta(:idel(Veta))
      go to 1200
1220  call CloseIfOpened(lni)
      Veta='fixed all *'
      write(lno,FormA) Veta(:idel(Veta))
      close(lno)
      call MoveFile(fln(:ifln)//'_fixed_new.tmp',
     1              fln(:ifln)//'_fixed.tmp')
      NacetlInt(nCmdncykl)=1
      NacetlReal(nCmdtlum)=1.
      call RefRewriteCommands(1)
      IgnoreW=.true.
      IgnoreE=.true.
      call RefDeallocate(.false.,0)
      if(Action.eq.0) then
        call Refine(0,RefineEnd)
        if(ErrFlag.eq.0) then
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
          call CopyFile(fln(:ifln)//'.m50',PreviousM50)
          call CopyFile(fln(:ifln)//'.m40',PreviousM40)
          if(ExistPowder)
     1      call CopyFile(fln(:ifln)//'.m41',PreviousM41)
        endif
        call PwdPrf(0)
      else
        call Refine(1,RefineEnd)
        if(ErrFlag.eq.0) then
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
          call CopyFile(fln(:ifln)//'.m50',PreviousM50)
          call CopyFile(fln(:ifln)//'.m40',PreviousM40)
          if(ExistPowder)
     1      call CopyFile(fln(:ifln)//'.m41',PreviousM41)
        endif
        call NewPg(1)
        call OpenFile(lst,fln(:ifln)//'.rep','formatted','unknown')
        LstOpened=.true.
        Uloha='Details for selected space group'
        call inform50
        ln=NextLogicNumber()
        do i=1,NAtMag
          do j=1,MagParMax
            write(Cislo,'(i1)') j-1
            do k=1,6
              t80='m'//SmbX(mod(k-1,3)+1)
              if(j.gt.1) then
                if(k.le.3) then
                  t80=t80(:idel(t80))//'sin'
                else
                  t80=t80(:idel(t80))//'cos'
                endif
              endif
              StMag(k,j-1,i)=t80(:idel(t80))//Cislo(1:1)//' ... free'
            enddo
          enddo
        enddo
        call OpenFile(ln,fln(:ifln)//'.ref','formatted','unknown')
1600    read(ln,FormA,end=1650) Veta
        if(LocateSubstring(Veta,'mx',.false.,.true.).eq.1.or.
     1     LocateSubstring(Veta,'my',.false.,.true.).eq.1.or.
     2     LocateSubstring(Veta,'mz',.false.,.true.).eq.1) then
          At=' '
          j= 1
          k=-1
          l= 0
          t80=' '
          m=0
          do i=1,idel(Veta)
            if(Veta(i:i).eq.'[') then
              k=0
            else if(Veta(i:i).eq.']') then
              if(j.eq.1) then
                At1=At
              else
                if(.not.EqIgCase(At,At1)) go to 1600
              endif
              At=' '
              j=j+1
              k=-1
            else if(k.ge.0) then
              k=k+1
              At(k:k)=Veta(i:i)
            else
              m=m+1
              t80(m:m)=Veta(i:i)
            endif
          enddo
          ia=ktat(AtMag,NAtMag,At1)
          if(ia.gt.0) then
            if(EqIgCase(t80(1:2),'mx')) then
              ix=1
            else if(EqIgCase(t80(1:2),'my')) then
              ix=2
            else if(EqIgCase(t80(1:2),'mz')) then
              ix=3
            else
              go to 1600
            endif
            if(EqIgCase(t80(3:3),'0')) then
              iy=0
              iw=0
            else if(EqIgCase(t80(3:3),'s')) then
              iy=1
              iw=0
            else if(EqIgCase(t80(3:3),'c')) then
              iy=1
              iw=3
            else
              go to 1600
            endif
          endif
          StMag(ix+iw,iy,ia)=t80
        endif
        go to 1600
1650    call CloseIfOpened(ln)
        Veta='List of relationships between magnetic moments of '//
     1        'individual atoms'
        call TitulekVRamecku(Veta)
        do i=1,NAtMag
          call NewLn(2+4)
          write(Veta,'(''Atom: '',a)') AtMag(i)
          j=idel(Veta)
          write(lst,FormA) Veta(:j)
          write(lst,FormA1)('=',k=1,j)
          do k=1,3
            if(NDimI(KPhase).eq.0) then
              write(lst,'(3a30)') StMag(k,0,i)
            else
              write(lst,'(3a30)') StMag(k,0,i),
     1                            StMag(k,1,i),
     2                            StMag(k+3,1,i)
            endif
          enddo
          write(lst,FormA)
        enddo
        call CloseListing
        HCFileName=SvFile
        call FeSaveImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile)
        call FeListView(fln(:ifln)//'.rep',0)
        call FeLoadImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile,0)
        call DeleteFile(SvFile)
        HCFileName=' '
      endif
      IgnoreW=.false.
      IgnoreE=.false.
      call DeleteFile(fln(:ifln)//'.ref')
      call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
      call CopyFile(fln(:ifln)//'.z50',fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      call OpenFile(lst,fln(:ifln)//'.ref','formatted','unknown')
      LstOpened=.true.
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      IStore=0
      call CrlStoreSymmetry(IStore)
      call RefPrelim(0)
      call CrlRestoreSymmetry(IStore)
      call CrlCleanSymmetry
      call SetIgnoreWTo(.false.)
      call SetIgnoreETo(.false.)
      call CloseListing
      call DeleteFile(fln(:ifln)//'.ref')
      if(FromRepAnalysis.or.FromGoToSub) WizardMode=.true.
      mxe=100
      mxep=5
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      allocate(lnp(mxe),pnp(mxep,mxe),npa(mxe),pko(mxep,mxe),
     1         lat(mxe),pat(mxep,mxe),lpa(mxe),ppa(mxep,mxe),
     2         lnpo(mxe),pab(mxe),eqhar(mxe))
      UseTabs=UseTabsIn
      SilentRun=SilentRunIn
      return
      end
