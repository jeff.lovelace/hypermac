      function CrlAngle(x1,x2,isw,KPh,Length1,Length2)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real x1(3),x2(3),x1g(3),x2g(3),Length1,Length2
      call multm(MetTens(1,isw,KPh),x1,x1g,3,3,1)
      call multm(MetTens(1,isw,KPh),x2,x2g,3,3,1)
      x1x2=scalmul(x1,x2g)
      x1x1=scalmul(x1,x1g)
      x2x2=scalmul(x2,x2g)
      Length1=sqrt(x1x1)
      Length2=sqrt(x2x2)
      CrlAngle=x1x2/sqrt(x1x1*x2x2)
      if(abs(CrlAngle).gt..999999) then
        CrlAngle=90.-sign(90.,CrlAngle)
      else if(abs(CrlAngle).lt..0001) then
        CrlAngle=90.
      else
        CrlAngle=acos(CrlAngle)/ToRad
      endif
9999  return
      end
