      subroutine MakeMagMod(sm,x40,tzero,nt,dt,sm0,smx,smy,kmod,Gamma)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension sm(3,*),sm0(3),smx(3,*),smy(3,*),x40(3),tzero(3),nt(3),
     1          dt(3),x4i(3),x4(3),nd(3),Gamma(9),d4(3)
      call AddVek(x40,tzero,x4i,NDimI(KPhase))
      ntt=1
      do i=1,NDimI(KPhase)
        ntt=ntt*nt(i)
      enddo
      do i=1,ntt
        call RecUnpack(i,nd,nt,NDimI(KPhase))
        do j=1,NDimI(KPhase)
          x4(j)=(nd(j)-1)*dt(j)
        enddo
        call multm(Gamma,x4,d4,NDimI(KPhase),NDimI(KPhase),1)
        call AddVek(x4i,d4,x4,NDimI(KPhase))
        call CopyVek(sm0,sm(1,i),3)
        do k=1,kmod
          arg=0.
          do j=1,NDimI(KPhase)
            arg=arg+x4(j)*float(kw(j,k,KPhase))
          enddo
          arg=pi2*arg
          do l=1,3
            sm(l,i)=sm(l,i)+smx(l,k)*sin(arg)+smy(l,k)*cos(arg)
          enddo
        enddo
      enddo
      return
      end
