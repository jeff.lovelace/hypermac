      subroutine Peaks2Atoms(ich)
      use Atoms_mod
      use Molec_mod
      use Basic_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real, allocatable :: XNew(:,:),aiNew(:),Charge(:),uxNew(:,:,:),
     1                     uyNew(:,:,:)
      real xp(3)
      integer, allocatable :: isfNew(:),nf(:),KModNew(:,:),iswp(:)
      integer kmodap(7),nc(3)
      character*8 At
      character*8, allocatable :: AtomNew(:)
      character*80 Veta
      logical EqIgCase,ExistFile
      ich=0
      if(allocated(AtomNew))
     1  deallocate(AtomNew,XNew,isfNew,aiNew,Charge,nf,KModNew,iswp)
      if(allocated(uxNew)) deallocate(uxNew,uyNew)
      n=0
      nmod=0
      nc=0
      do i=1,NComp(KPhase)
        if(NComp(KPhase).gt.1) then
          write(Cislo,FormI15) i
          call Zhusti(Cislo)
          Veta=fln(:ifln)//'.m40_'//Cislo(:idel(Cislo))
          if(ExistFile(Veta)) then
            call OpenFile(m40,Veta,'formatted','old')
            iven=0
          else
            call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
            iven=1
          endif
        else
          call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
          iven=0
        endif
        if(ErrFlag.ne.0) go to 9999
1100    read(m40,FormA,end=9999) Veta
        if(Veta(1:10).ne.'----------'.or.
     1     LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2    go to 1100
1110    read(m40,FormA,end=9999) Veta
        if(Veta(1:10).eq.'----------') go to 1110
        read(Veta,100) IComp,KPh
        MaxUsedKw(KPh)=0
        call SetIntArrayto(kmodap,7,0)
1120    read(m40,FormA,end=1130) Veta
        if(Veta(1:10).eq.'----------'.or.Veta.eq.' ') go to 1130
        read(Veta,'(63x,3i3)')(kmodap(j),j=1,3)
        n=n+1
        nc(IComp)=nc(IComp)+1
        do j=1,7
          MaxUsedKw(KPh)=max(MaxUsedKw(KPh),kmodap(j))
        enddo
        k=1
        if(kmodap(1).gt.0) k=kmodap(1)+1
        if(kmodap(2).gt.0) then
          k=k+kmodap(2)
          nmod=max(kmodap(2),nmod)
        endif
        if(kmodap(3).gt.0) k=k+kmodap(3)*2
        if(k.gt.1) k=k+1
        read(m40,FormA)(Veta,j=1,k)
        go to 1120
1130    call CloseIfOpened(m40)
        if(iven.eq.1) exit
      enddo
      if(n.le.0) go to 9999
      allocate(AtomNew(n),XNew(3,n),isfNew(n),aiNew(n),Charge(n),
     1         nf(NAtFormula(KPhase)),KModNew(7,n),iswp(n))
      if(nmod.gt.0) allocate(uxNew(3,nmod,n),uyNew(3,nmod,n))
      n=0
      do i=1,NComp(KPhase)
        if(NComp(KPhase).gt.1) then
          write(Cislo,FormI15) i
          call Zhusti(Cislo)
          Veta=fln(:ifln)//'.m40_'//Cislo(:idel(Cislo))
          if(ExistFile(Veta)) then
            call OpenFile(m40,Veta,'formatted','old')
            iven=0
          else
            call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
            iven=1
          endif
        else
          call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
        endif
        if(ErrFlag.ne.0) go to 9999
1200    read(m40,FormA,end=9999) Veta
        if(Veta(1:10).ne.'----------'.or.
     1     LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2    go to 1200
        read(m40,100) IComp,KPh
        do ii=1,nc(IComp)
          n=n+1
          read(m40,102) At,isfp,itfp,pom,(XNew(j,n),j=1,3),
     1                  (kmodap(j),j=1,3),pom,Charge(n)
          do j=1,3
            XNew(j,n)=anint(XNew(j,n)*1000.)/1000.
          enddo
          call od0do1(XNew(1,n),XNew(1,n),3)
          iswp(n)=IComp
          if(kmodap(1).gt.0) read(m40,FormA)(Veta,j=1,kmodap(1)+1)
          if(kmodap(2).gt.0) then
            read(m40,101)((uxNew(j,k,n),j=1,3),(uyNew(j,k,n),j=1,3),
     1                    k=1,kmodap(2))
          endif
          if(kmodap(3).gt.0) read(m40,FormA)(Veta,j=1,kmodap(3)*2)
          if(kmodap(1).gt.0.or.kmodap(2).gt.0.or.kmodap(3).gt.0)
     1      read(m40,FormA) Veta
          call SetIntArrayTo(KModNew(1,n),7,0)
          call CopyVekI(kmodap,KModNew(1,n),3)
          isfNew(n)=0
          call SetRealArrayTo(xp,3,0.)
          call SpecPos(XNew(1,n),xp,0,IComp,.5,nocc)
!         Podruhe pro jistotu
          call SpecPos(XNew(1,n),xp,0,IComp,.5,nocc)
          aiNew(n)=1./float(nocc)
          itfMax=max(itfp,itfMax)
        enddo
        call CloseIfOpened(m40)
        if(iven.eq.1) exit
      enddo
      SfMin=10000.
      do k=1,NAtFormula(KPhase)
        if(.not.EqIgCase(AtType(k,KPhase),'H'))
     1    SfMin=min(SfMin,AtNum(k,KPhase))
      enddo
      kopt=0
      AcceptedChargeOpt=0.
      SumDOpt=0.
      do k=1,NAtFormula(KPhase)
        if(EqIgCase(AtType(k,KPhase),'H')) cycle
        Scp=AtNum(k,KPhase)/Charge(1)
        nn=0
        SumDOld=0.
1320    Sum1=0.
        Sum2=0.
        SumD=0.
        m=0
        AcceptedCharge=0.
        do i=1,n
          pom=Charge(i)*Scp
          if(pom.lt..5*SfMin) cycle
          m=m+1
          AcceptedCharge=AcceptedCharge+Charge(i)
          Diff=10000.
          do j=1,NAtFormula(KPhase)
            if(EqIgCase(AtType(j,KPhase),'H')) cycle
            DiffP=abs(pom-AtNum(j,KPhase))/AtNum(j,KPhase)
            if(DiffP.lt.Diff) then
              jopt=j
              Diff=DiffP
            endif
          enddo
          Sum1=Sum1+Charge(i)*AtNum(jopt,KPhase)
          Sum2=Sum2+Charge(i)**2
          SumD=SumD+(pom-AtNum(jopt,KPhase))**2
        enddo
        if(m.eq.0) cycle
        Scp=Sum1/Sum2
        if(abs(SumD-SumDOld).gt.0..or.nn.eq.0) then
          nn=nn+1
          SumDOld=SumD
          if(nn.le.5) go to 1320
        endif
        if(AcceptedCharge.gt.AcceptedChargeOpt.or.
     1     (AcceptedCharge.eq.AcceptedChargeOpt.and.SumD.lt.SumDOpt))
     2    then
          AcceptedChargeOpt=AcceptedCharge
          ScpOpt=Scp
          SumDOpt=SumD
        endif
      enddo
      Scp=ScpOpt
      call SetIntArrayTo(nf,NAtFormula(KPhase),0)
c      do i=1,NAtInd
      do i=1,NAtAll
        if(kswa(i).eq.KPhase) isf(i)=0
      enddo
      call EM40CleanAtoms
      nac=0
      do isw=1,NComp(KPhase)
        do i=1,n
          if(iswp(i).ne.isw) cycle
          pom=Charge(i)*Scp
          if(pom.lt..5*SfMin) cycle
          Diff=10000.
          do j=1,NAtFormula(KPhase)
            if(EqIgCase(AtType(j,KPhase),'H')) cycle
            DiffP=abs(pom-AtNum(j,KPhase))/AtNum(j,KPhase)
            if(DiffP.lt.Diff) then
              jopt=j
              Diff=DiffP
            endif
          enddo
          nac=nac+1
          if(NAtAll.ge.MxAtAll) call ReallocateAtoms(1000)
          call AtSun(nac,NAtAll,nac+1)
          NAtIndLen(isw,KPhase)=nac-NAtIndFr(isw,KPhase)+1
          call EM40UpdateAtomLimits
          call ReallocateAtomParams(nac-1,itfMax,IFrMax,KModNew(1,i),0)
          call SetBasicKeysForAtom(nac)
          iswa(nac)=isw
          nf(jopt)=nf(jopt)+1
          write(Atom(nac),'(a2,i3)') AtType(jopt,KPhase),nf(jopt)
          call Zhusti(Atom(nac))
          isf(nac)=jopt
          itf(nac)=itfp
          itfMax=max(itf(nac),itfMax)
          call CopyVekI(KModNew(1,i),KModA(1,nac),7)
          call CopyVek(XNew(1,i),x(1,nac),3)
          k=KModNew(2,nac)
          if(k.gt.0) then
            call CopyVek(uxNew(1,1,i),ux(1,1,nac),3*k)
            call CopyVek(uyNew(1,1,i),uy(1,1,nac),3*k)
            call SetRealArrayTo(sux(1,1,nac),3*k,0.)
            call SetRealArrayTo(suy(1,1,nac),3*k,0.)
          endif
          ai(nac)=aiNew(i)
          call SetRealArrayTo(beta(1,nac),6,0.)
          beta(1,nac)=3.
          if(nac.gt.1) then
            PrvniKiAtomu(nac)=PrvniKiAtomu(nac-1)+mxda
          else
            PrvniKiAtomu(nac)=ndoff+1
          endif
          if(k.gt.0) then
            DelkaKiAtomu(nac)=11+6*k
          else
            DelkaKiAtomu(nac)=10
          endif
          call SetIntArrayTo(KiA(1,nac),DelkaKiAtomu(nac),0)
        enddo
        call CloseIfOpened(m40)
!        nac=nac+NAtIndLen(isw,KPhase)
      enddo
      call iom40(1,0,fln(:ifln)//'.m40')
      if(MaxNDim.gt.3) then
        mxe=100
        mxep=5
        if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                                lnpo,pab,eqhar)
        allocate(lnp(mxe),pnp(mxep,mxe),npa(mxe),pko(mxep,mxe),
     1           lat(mxe),pat(mxep,mxe),lpa(mxe),ppa(mxep,mxe),
     2           lnpo(mxe),pab(mxe),eqhar(mxe))
        call iom40(0,0,fln(:ifln)//'.m40')
        MaxUsedKwAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh.or.NDim(KPh).le.3) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
        enddo
        if(Allocated(KWSym)) deallocate(KwSym)
        allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
        call SetSymmWaves
        nvai=0
        neq=0
        neqs=0
        LstOpened=.true.
        uloha='Symmetry restrictions'
        call OpenFile(lst,Veta,'formatted','unknown')
        call NewPg(1)
        im=0
        ji=0
        nap=0
        call atspec(1,NAtInd,ji,im,nap)
        call RefAppEq(0,0,0)
        call CloseListing
        LstOpened=.false.
        call DeleteFile(Veta)
        call iom40(1,0,fln(:ifln)//'.m40')
        if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                                lnpo,pab,eqhar)
      endif
      go to 9999
9000  ich=1
9999  if(allocated(AtomNew))
     1  deallocate(AtomNew,XNew,isfNew,aiNew,Charge,nf,KModNew,iswp)
      if(allocated(uxNew)) deallocate(uxNew,uyNew)
      call CloseIfOpened(m40)
      return
100   format(2i5)
101   format(6f9.6)
102   format(a8,2i3,4x,4f9.6,9x,3i3/2f9.3)
      end
