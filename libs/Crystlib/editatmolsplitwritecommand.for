      subroutine EditAtMolSplitWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EditAtMolSplitC/ nEdwAtMol,nCrwAtoms,nCrwMolecules,
     1                        AtMolString
      character*256 Veta,AtMolString,EdwStringQuest
      character*(*) Command
      logical CrwLogicQuest
      save EditAtMolSplitC
      ich=0
      Command=EdwStringQuest(nEdwAtMol)
      if(CrwLogicQuest(nCrwAtoms)) then
        call TestAtomString(Command,IdWildNo,IdAtMolYes,IdMolNo,
     1                      IdSymmYes,IdAtMolMixNo,Veta)
        Command='atsplit '//Command(:idel(Command))
      else
        call TestMolString(Command,IdWildNo,IdSymmYes,Veta)
        Command='molsplit '//Command(:idel(Command))
      endif
      if(Veta.ne.' ') go to 8100
      go to 9999
8100  ich=1
      if(Veta.ne.'Jiz oznameno')
     1  call FeChybne(-1.,-1.,Veta,'Edit or delete the relevant '//
     2                'command.',SeriousError)
9999  return
      end
