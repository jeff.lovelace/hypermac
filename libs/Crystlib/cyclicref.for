      subroutine CyclicRef(Klic)
      use Basic_mod
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*256 :: FlnOld,FlnStart,Veta,CurrentDirO,RInfo(10),
     1                 CyclicRefMasterPureFile
      character*20, allocatable :: ParString(:)
      integer :: FeChDir,ip(1),Use,Start,SbwLnQuest,SbwItemPointerQuest,
     1           IFile=1,UseTabsIn
      logical RefineEnd,ExistFile,StructureExists,EqIgCase,RunImport,
     1        OnlyOneDirection
      real RFac(5)
      real, allocatable :: XManBackgOld(:),YManBackgOld(:)
      ln=0
      UseTabsIn=UseTabs
      FlnOld=Fln
      if(.not.allocated(CifKey)) then
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
      endif
      if(CyclicBack) then
        CyclicBack=.false.
        go to 1100
      endif
      CurrentDirO=CurrentDir
      if(Klic.eq.0) then
        call FeFileManager('Select the master file for cyclic '//
     1                     'refinement',CyclicRefMasterFile,'*.cref',0,
     2                     .false.,ich)
        if(ich.ne.0) go to 9999
      endif
      call GetPureFileName(CyclicRefMasterFile,CyclicRefMasterPureFile)
1100  ln=NextLogicNumber()
      call IOCyclicRef(0)
      if(ErrFlag.ne.0) go to 9999
      if(allocated(ParString)) deallocate(ParString)
      allocate(ParString(NCyclicRefFile))
      n1=0
      n2=0
      do i=1,NCyclicRefFile
        if(CyclicRefUpDown(i).gt.0) then
          n1=n1+1
        else
          n2=n2+1
        endif
      enddo
      OnlyOneDirection=.not.(n1.gt.0.and.n2.gt.0)
      do i=1,NCyclicRefFile
        write(Cislo,'(f10.2)') CyclicRefParam(i)
        call ZdrcniCisla(Cislo,1)
        Veta=Cislo(:idel(Cislo))//CyclicRefTypeUnits
        if(.not.OnlyOneDirection) then
          if(CyclicRefUpDown(i).gt.0) then
            Cislo='+'
          else
            Cislo='-'
          endif
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        if(CyclicRefStart(i).gt.0) then
          IFileStart=i
          Veta=Veta(:idel(Veta))//'*'
        endif
        ParString(i)=Veta
      enddo
      id=NextQuestId()
      il=16
      xqd=550.
      Veta='Actions with individual item:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
      il=1
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=5
      ild=5
      call FeQuestSbwMake(id,xpom,il,dpom,ild,8,CutTextFromRight,
     1                    SbwHorizontal)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      SbwRightClickAllowed=.false.
      nSbwSel=SbwLastMade
      lns=NextLogicNumber()
      idl=idel(CyclicRefMasterPureFile)
      Veta=CyclicRefMasterPureFile(:idl)//'_list.tmp'
      call OpenFile(lns,Veta,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NCyclicRefFile
        write(lns,FormA) ParString(i)(:idel(ParString(i)))
      enddo
      call CloseIfOpened(lns)
      call FeQuestSbwMenuOpen(nSbwSel,Veta)
      call FeQuestSbwItemOff(nSbwSel,1)
      call FeQuestSbwItemOn(nSbwSel,ICyclicRefFile)
      Veta='Run le %Bail wizard'
      dpom=FeTxLengthUnder(Veta)+60.
      xpom=xqd*.5-dpom*1.5-20.
      il=-il*10-13
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          Veta='Run %Rietveld wizard'
          nButtLeBail=ButtonLastMade
        else if(i.eq.2) then
          Veta='Go to the basic %Jana window'
          nButtRietveld=ButtonLastMade
        else if(i.eq.3) then
          nButtJana=ButtonLastMade
        endif
        xpom=xpom+dpom+20.
      enddo
      il=il-8
      call FeQuestLinkaMake(id,il)
      il=il-8
      Veta='Cyclic actions:'
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      dpom=FeTxLengthUnder(Veta)+60.
      xpom=(xqd-dpom)*.5
      il=il-8
      Veta='Run %cyclic refinement'
      xpom=xqd*.5-dpom-10.
      do i=1,2
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          Veta='%Graph'
          nButtCyclic=ButtonLastMade
        else if(i.eq.2) then
          nButtGraph=ButtonLastMade
        endif
        xpom=xpom+dpom+20.
      enddo
      il=il-10
      UseTabs=NextTabs()
      xpom=90.
      do i=1,3
        call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
        if(i.eq.2) then
          xpom=xpom+35.
        else
          xpom=xpom+40.
        endif
        call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        xpom=xpom+90.
      enddo
      xpom=5.
      do i=1,10
        call FeQuestLblMake(id,xpom,il,' ','L','N')
        if(i.eq.1) nLblInfo=LblLastMade
        il=il-6
      enddo
      il=16
      Veta='Close'
      dpom=FeTxLength(Veta)+20.
      xpom=(xqd-xpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtClose=ButtonLastMade
1500  call GetPureFileName(CyclicRefFile(IFileStart),FlnStart)
      if(ExistFile(FlnStart(:idel(FlnStart))//'.m95')) then
        call FeQuestButtonOff(nButtCyclic)
      else
        call FeQuestButtonDisable(nButtCyclic)
      endif
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtClose) then
        go to 8000
      else if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtLeBail.or.
     2    CheckNumber.eq.nButtRietveld.or.
     2    CheckNumber.eq.nButtJana)) then
        ICyclicRefFile=SbwItemPointerQuest(nSbwSel)
        call GetPureFileName(CyclicRefFile(ICyclicRefFile),fln)
        ifln=idel(fln)
        do i=1,9
          ExistMFile(i)=ExistFile(fln(:ifln)//ExtMFile(i))
        enddo
        if(.not.ExistM95) then
          call SetBasicM40(.true.)
          call SetBasicM50
          CyclicWizardHotovo=.true.
          call EM9ImportWizard(0)
          CyclicWizardHotovo=.false.
        endif
        call iom40(0,0,fln(:ifln)//'.m40')
        call iom50(0,0,fln(:ifln)//'.m50')
        if(CheckNumber.eq.nButtLeBail) then
          call CrlLeBailMaster(ich)
          go to 1500
        else if(CheckNumber.eq.nButtRietveld) then
          call CrlRietveldMaster(ich)
          go to 1500
        else if(CheckNumber.eq.nButtJana) then
          CyclicBack=.true.
          go to 8000
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCyclic)
     1  then

        nLbl=nLblInfo
        do i=1,10
          call FeQuestLblChange(nLbl,' ')
          nLbl=nLbl+1
        enddo
        NInfoP=0
        IFile=0
        RInfo=' '
        NManBackgOld=-1
        do i=1,NCyclicRefFile
          if(CyclicRefUse(i).le.0) cycle
          call GetPureFileName(CyclicRefFile(i),fln)
          ifln=idel(fln)
          SilentRun=.true.
          CyclicRefMode=.true.
          RunImport=.true.
          if(NManBackgOld.lt.0) then
            call PwdM92Nacti
            NManBackgOld=NManBackg(KDatBlock)
            if(NManBackgOld.gt.0) then
              if(allocated(XManBackgOld))
     1          deallocate(XManBackgOld,YManBackgOld)
              allocate(XManBackgOld(NManBackgOld),
     1                 YManBackgOld(NManBackgOld))
              do j=1,NManBackgOld
                XManBackgOld(j)=XManBackg(j,KDatBlock)
                YManBackgOld(j)=YManBackg(j,KDatBlock)
              enddo
            endif
          endif
          if(IFile.ne.IFileStart) then
            call CopyFile(FlnStart(:idel(FlnStart))//'.m40',
     1                    fln(:ifln)//'.m40')
            call CopyFile(FlnStart(:idel(FlnStart))//'.m41',
     1                    fln(:ifln)//'.m41')
            call CopyFile(FlnStart(:idel(FlnStart))//'.m50',
     1                    fln(:ifln)//'.m50')
            ExistM40=ExistFile(fln(:ifln)//'.m40')
            ExistM41=ExistFile(fln(:ifln)//'.m41')
            ExistM50=ExistFile(fln(:ifln)//'.m50')
            ExistM95=ExistFile(fln(:ifln)//'.m95')
            if(ExistM95) then
              call iom95(0,fln(:ifln)//'.m95')
              ExistM90=ExistFile(fln(:ifln)//'.m90')
              RunImport=.not.EqIgCase(SourceFileRefBlock(1),
     1                                CyclicRefFile(i)).or.
     2                  .not.ExistM90
            endif
          endif
          if(RunImport) then
            call CopyFile(FlnStart(:idel(FlnStart))//'.m95',
     1                    fln(:ifln)//'.m95')
            call CopyFile(FlnStart(:idel(FlnStart))//'.m90',
     1                    fln(:ifln)//'.m90')
          endif
          ExistM40=.true.
          ExistM41=.true.
          ExistM50=.true.
          ExistM90=.true.
          ExistM95=.true.
          call iom90(0,fln(:ifln)//'.m90')
          call iom40(0,0,fln(:ifln)//'.m40')
          call iom50(0,0,fln(:ifln)//'.m50')
          if(RunImport) then
            ICyclicRefFile=i
            call EM9ImportWizard(0)
            call PwdM92Nacti
            if(NManBackgOld.gt.0) then
              NManBackg(KDatBlock)=NManBackgOld
              if(allocated(XManBackg)) deallocate(XManBackg,YManBackg)
              allocate(XManBackg(NManBackgOld,NDatBlock),
     1                 YManBackg(NManBackgOld,NDatBlock))
              do j=1,NManBackgOld
                XManBackg(j,KDatBlock)=XManBackgOld(j)
                YManBackg(j,KDatBlock)=YManBackgOld(j)
              enddo
              write(Cislo,'(''.l'',i2)') KRefBlock
              if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
              RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
              call DeleteFile(RefBlockFileName)
              call OpenFile(95,RefBlockFileName,'formatted','unknown')
              call PwdPutRecordToM95(95)
              ModifiedRefBlock(KRefBlock)=.true.
              call CloseIfOpened(95)
              call CompleteM95(0)
              call EM9CreateM90Powder(ich)
              call CompleteM90
            endif
          endif
          call NewPg(1)
          SilentRun=.false.
          Veta=fln(:ifln)//'.m70'
          call DeleteFile(Veta)
          call Refine(0,RefineEnd)
          if(ErrFlag.eq.0) then
            call UpdateSummary
          else
            ErrFlag=0
            if(RefineEnd) exit
          endif
          if(ExistFile(Veta)) then
            lns=NextLogicNumber()
            call OpenFile(lns,Veta,'formatted','unknown')
            if(ErrFlag.ne.0) go to 1860
1820        read(lns,FormA,end=1840) Veta
            m=12
            n=15
            nRFac=3
            do jj=1,nRFac
              k=LocateSubstring(Veta,CIFKey(m,n)(:idel(CIFKey(m,n))),
     1                          .false.,.true.)
              if(k.gt.0) then
                l=k+idel(CIFKey(m,n))
                call StToReal(Veta,l,RFac(jj),1,.false.,ichp)
                if(ichp.ne.0) RFac(jj)=.9999
                if(jj.ne.1) RFac(jj)=RFac(jj)*100.
                go to 1820
              endif
              if(jj.eq.1) then
                m=142
                n=20
              else if(jj.eq.2) then
                m=143
              endif
            enddo
            go to 1820
1840        call CloseIfOpened(lns)
          endif
          if(RFac(1).gt.90.) then
            write(Veta,101)(Tabulator,Tabulator,jj=1,nRFac)
          else
            write(Veta,100)(Tabulator,Tabulator,RFac(jj),jj=1,nRFac)
          endif
          Veta=ParString(i)//Veta(:idel(Veta))
          if(NInfoP.ge.10) then
            RInfo(1:9)=RInfo(2:10)
          else
            NInfoP=NInfoP+1
          endif
          RInfo(NInfoP)=Veta
          nLbl=nLblInfo
          call FeDeferOutput
          do jj=1,NInfoP
            call FeQuestLblChange(nLbl,RInfo(jj))
            nLbl=nLbl+1
          enddo
          call FeReleaseOutput
          call FeDeferOutput
1860      call DeleteFile(fln(:ifln)//'_Refine.l70')
          call DeleteFile(fln(:ifln)//'.s40')
          call DeleteFile(fln(:ifln)//'.s41')
          call DeleteFile(fln(:ifln)//'.m70')
          call DeleteFile(fln(:ifln)//'.m80')
          call DeleteFile(fln(:ifln)//'.m83')
          call DeleteFile(fln(:ifln)//'.m85')
          call DeleteFile(fln(:ifln)//'.m91')
          call DeletePomFiles
          call DeleteFile(fln(:ifln)//'.usd')
          FlnStart=Fln
          CyclicRefMode=.false.
        enddo
        if(NInfoP.ge.10) then
          RInfo(1:9)=RInfo(2:10)
        else
          NInfoP=NInfoP+1
        endif
        RInfo(NInfoP)='                                            '//
     1                '             --- End of cyclic refinment ---'
        nLbl=nLblInfo
        do i=1,NInfoP
          call FeQuestLblChange(nLbl,RInfo(i))
          nLbl=nLbl+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtGraph)
     1  then
        call CyclicRefGraph
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
8000  call FeQuestRemove(id)
9999  if(.not.CyclicBack) then
        i=FeChDir(CurrentDirO)
        call FeGetCurrentDir
        call CloseIfOpened(ln)
        Fln=FlnOld
        iFln=idel(Fln)
        call DeleteFile(PreviousM40)
        call DeleteFile(PreviousM50)
      endif
      call CloseIfOpened(ln)
      idl=idel(CyclicRefMasterPureFile)
      Veta=CyclicRefMasterPureFile(:idl)//'_list.tmp'
      call DeleteFile(Veta)
      if(allocated(CifKey)) deallocate(CifKey,CifKeyFlag)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(allocated(XManBackgOld)) deallocate(XManBackgOld,YManBackgOld)
      return
100   format(a1,'GOF=',a1,f6.2,a1,'Rp=',a1,f6.2,a1,'Rwp=',a1,f6.2)
101   format(a1,'GOF=',a1,'---',a1,'Rp='a1,'---',a1,'Rwp=',a1,'---')
      end
