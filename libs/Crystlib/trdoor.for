      subroutine trdoor(x,ux,uy,n,kfx,kmodx,kmodxo,ormi,orsel,ord)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(n),ux(n,*),uy(n,*),ormi(*)
      real, allocatable :: orx(:),orxo(:)
      integer orsel(*),ord
      allocate(orx(ord),orxo(ord))
      if(kfx.eq.0) then
        jk=2*kmodxo+1
        kmodxp=kmodx
      else
        jk=2*kmodxo-1
        kmodxp=kmodx-1
      endif
      if(kmodxp.le.0) then
        kmodxo=kmodx
        go to 9999
      endif
      call SetRealArrayTo(orx,ord,0.)
      do i=1,n
        orx(1)=x(i)
        do j=2,2*kmodxo+1
          m=(OrSel(j)+1)/2
          if(mod(OrSel(j),2).eq.1) then
            orx(j)=ux(i,m)
          else
            orx(j)=uy(i,m)
          endif
        enddo
        call MultM(orx,ormi,orxo,1,ord,ord)
        x(i)=orxo(1)
        m=1
        do j=1,kmodxo
          m=m+1
          ux(i,j)=orxo(m)
          m=m+1
          uy(i,j)=orxo(m)
        enddo
        if(kfx.ne.0) then
          j=kmodxo+1
          m=m+1
          ux(i,j)=orx(m)
          m=m+1
          uy(i,j)=orx(m)
        endif
      enddo
      kmodx=kmodxo
9999  deallocate(orx,orxo)
      return
      end
