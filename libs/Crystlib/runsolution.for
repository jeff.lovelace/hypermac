      subroutine RunSolution
      use Atoms_mod
      use Basic_mod
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'fourier.cmn'
      include 'memexport.cmn'
      include 'datred.cmn'
      real CellParP(6),tr(3,3),xp(6),trp(36),TrMPIn(36),TrShelxt(36,15),
     1     ShiftSol(3)
      real, allocatable :: Fa(:),sFa(:),Fap(:),sFap(:),s6p(:,:)
      integer CrlCentroSymm,EdwStateQuest,CrwStateQuest,StTime,UseTabsT,
     1        FeGetSystemTime,RolMenuSelectedQuest,EDAtomsDef,UseTabsIn,
     2        TypeOutFile,PocetPrgm,ITrP(3,3),ITrvP(3),NatType(20),
     3        itr(3,3),ihitw(6,18),isymfac(6),mxsymfac(6),UseTabsS
      integer, allocatable :: iha(:,:),ihap(:,:)
      integer :: NPrgmO=-1,OutFileXYZ=0,OutFilePowder=-1,OutFile1=1,
     1           OutFile2=2,LastSolution=0
      logical :: isPowderO=.false.,MatRealEqUnitMat,EqRV
      character*256 DirSIRA,CallSIRA,Veta,EdwStringQuest,CommandFile,
     1              FileName,FlnSir
      character*80 Radka,LabelG,FormulaOld,t80,s80,flnOld,ShelxtName,
     1             ShelxtSol(15),Orientation,Raw(3),SirSol,SuperflipSol
      character*60 GrupaOld
      character*40 GrupaH
      character*20 GrupaS
      character*10 Grupap
      character*8  GrupaI,At
      character*3  abc
      integer FeChdir,ButtonStateQuest
      integer :: NSIR=0,NExpo=0
      logical FeSystemCommandFinished,UseOldInflip,EqIgCase,ExistFile,
     1        FeYesNo,eqiv,ranover,CrwLogicQuest,FeYesNoHeader,
     2        ManualEdit,Scitat,UzJdouAtomy,UseOldSolution,UseOldInflipO
      logical :: WasPowder=.false.
      equivalence (IdNumbers(1),IdSIR97,IdExpo),
     2            (IdNumbers(2),IdSIR2002,IdExpo2004),
     3            (IdNumbers(3),IdSIR2004,IdExpo2009),
     4            (IdNumbers(4),IdSIR2008,IdExpo2013),
     5            (IdNumbers(5),IdSIR2011,IdExpo2014),
     6            (IdNumbers(6),IdSIR2014),
     7            (IdNumbers(7),IdSuperflip),
     8            (IdNumbers(8),IdShelxt)
      data abc/'abc'/,Residual/15./,NPeakSearch/4/
      UseTabsS=0
      TrMPIn(1:NDimQ(KPhase))=TrMp(1:NDimQ(KPhase))
      NShelxt=0
      NShelxtSel=0
      ShelxtName=fln(:ifln)//'_shelxt'
      iShelxtName=idel(ShelxtName)
      call DeleteAllFiles(ShelxtName(:iShelxtName)//'*')
      SirSol=fln(:ifln)//'_sir'
      iSirSol=idel(SirSol)
      call DeleteAllFiles(SirSol(:iSirSol)//'*')
      SuperflipSol=fln(:ifln)//'_Superflip'
      iSuperflipSol=idel(SuperflipSol)
      Veta=fln(:ifln)//'.sflog'
      if(ExistFile(Veta)) then
        call CopyFile(Veta,SuperflipSol(:iSuperflipSol)//'.sflog')
        call CopyFile(fln(:ifln)//'.m80',
     1                SuperflipSol(:iSuperflipSol)//'.m80')
        call CopyFile(fln(:ifln)//'.m81',
     1                SuperflipSol(:iSuperflipSol)//'.m81')
      endif
      Veta=fln(:ifln)//'.inflip'
      if(ExistFile(Veta))
     1  call CopyFile(Veta,SuperflipSol(:iSuperflipSol)//'.inflip')
      LastSolution=0
      id=0
      if(isPowder.neqv.isPowderO) PrgmO=-1
      isPowderO=isPowder
      j=0
      do i=1,6
        if(CallSIR(i).ne.' ') then
          j=i
          SIRDefined(i)=1
        else
          SIRDefined(i)=0
        endif
      enddo
      if(NSIR.le.0) NSIR=j
      j=0
      do i=1,5
        if(CallExpo(i).ne.' ') then
          j=i
          ExpoDefined(i)=1
        else
          ExpoDefined(i)=0
        endif
      enddo
      if(NExpo.le.0) NExpo=j
      UseOldSolution=.false.
      NAtFormulaOld=NAtFormula(KPhase)
      if(isPowder.and.NPhase.gt.1) then
        KKleBailIn=KKleBail(KDatBlock)
        KPhaseSolve=KPhase
      endif
1050  ln=0
      lnr=0
      Scitat=.false.
      ManualEdit=.false.
      Biso=0.
      SuperflipKeepTerminal=.false.
      SuperflipLocalNorm=.false.
      MaxCycles1=2000
      MaxCycles2=300000
      SuperflipRepeatMax=10
      SuperflipDeltaDef=.9
      SuperflipRandomSeed=111
      UseSuperflipRandomSeed=.false.
      SuperflipStartModel=1
      EDAtoms=-1
      if(Radiation(1).eq.ElectronRadiation) then
        SuperflipDelta=SuperflipDeltaDef
        SuperflipRepeatMode=1
        SuperflipAlgorithm=3
      else
        SuperflipDelta=0.
        SuperflipRepeatMode=0
        SuperflipAlgorithm=1
      endif
      if(ExistFile(fln(:ifln)//'.inflip')) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.inflip','formatted','unknown')
1100    read(ln,FormA,end=1150) Radka
        k=0
        call kus(Radka,k,t80)
        if(EqIgCase(t80,'biso')) then
          call kus(Radka,k,Cislo)
          call Posun(Cislo,1)
          read(Cislo,'(f15.5)',err=1100,end=1100) Biso
        else if(EqIgCase(t80,'delta')) then
          call kus(Radka,k,Cislo)
          if(.not.EqIgCase(Cislo,'AUTO')) then
            call Posun(Cislo,1)
            read(Cislo,'(f15.5)',err=1100,end=1100) SuperflipDeltaDef
            SuperflipDelta=SuperflipDeltaDef
          endif
        else if(EqIgCase(t80,'maxcycles')) then
          call kus(Radka,k,Cislo)
          call Posun(Cislo,0)
          read(Cislo,FormI15,err=1100,end=1100) MaxCycles
        else if(EqIgCase(t80,'randomseed')) then
          call kus(Radka,k,Cislo)
          if(EqIgCase(Cislo,'AUTO')) then
            UseSuperflipRandomSeed=.false.
          else
            call Posun(Cislo,0)
            read(Cislo,FormI15,err=1100,end=1100) SuperflipRandomSeed
            UseSuperflipRandomSeed=.true.
          endif
        else if(EqIgCase(t80,'repeatmode')) then
          if(k.lt.len(Radka)) then
            call kus(Radka,k,Cislo)
            if(EqIgCase(Cislo,'nosuccess')) then
              SuperflipRepeatMode=-1
            else
              call Posun(Cislo,0)
              read(Cislo,FormI15,err=1100,end=1100) SuperflipRepeatMax
              SuperflipRepeatMode= 1
            endif
          endif
        else if(EqIgCase(t80,'perform')) then
          call kus(Radka,k,Cislo)
          if(EqIgCase(Cislo,'CF')) then
            SuperflipAlgorithm=1
          else if(EqIgCase(Cislo,'LDE')) then
            SuperflipAlgorithm=2
            SuperflipAlgorithm=3
          endif
        else if(EqIgCase(t80,'modelfile')) then
          call kus(Radka,k,Cislo)
          if(EqIgCase(Cislo,'superposition')) SuperflipStartModel=2
        else if(EqIgCase(t80,'numberofatoms')) then
          call StToInt(Radka,k,ih,1,.false.,ich)
          if(ich.eq.0) then
            EdAtoms=ih(1)
          else
            EdAtoms=-1
          endif
        endif
        go to 1100
1150    call CloseIfOpened(ln)
        if(SuperflipRepeatMode.eq.0) then
          MaxCycles2=MaxCycles
        else
          MaxCycles1=MaxCycles
        endif
      endif
      FormulaOld=Formula(KPhase)
      nzOld=NUnits(KPhase)
      GrupaOld=Grupa(KPhase)
      if(NUnits(KPhase).le.0)
     1  NUnits(KPhase)=NLattVec(KPhase)*NSymm(KPhase)
1200  ich=0
      id=NextQuestId()
      xqd=600.
      ilm=19
      if(isPowder) then
        if(isTOF) then
          SinTOFThUse=sin(.5*TOFTTh(KDatBlock)*ToRad)
        else if(.not.isED) then
          LPTypeCorrUse=LpFactor(KDatBlock)
          FractPerfMonUse=FractPerfMon(KDatBlock)
          if(LPTypeCorrUse.eq.PolarizedGuinier) then
            snm=sin(AlphaGMon(KDatBlock)*ToRad)
            BetaGMonUse=BetaGMon(KDatBlock)*ToRad
          else
            snm=sin(AngleMon(KDatBlock)*ToRad)
          endif
          Cos2ThMon =1.-2.*snm**2
          Cos2ThMonQ=Cos2ThMon**2
        endif
        ilm=ilm+1
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,'Structure solution',0,
     1                   LightGray,-1,-1)
      il=0
      xpom=5.
      tpom=xpom+CrwgXd+3.
      nCrwProgramFirst=0
      if(isPowder) then
        Veta='use %Expo'
        PocetPrgm=3
      else
        Veta='use S%IR'
        PocetPrgm=3
      endif
      do i=1,PocetPrgm
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,1)
        call FeQuestCrwOpen(CrwLastMade,.false.)
        if(nCrwProgramFirst.eq.0) then
          nCrwProgramFirst=CrwLastMade
        else
          nCrwProgramLast=CrwLastMade
        endif
        if(i.eq.1) then
          if((isPowder.and.NExpo.le.0).or.(.not.isPowder.and.NSIR.le.0))
     1      then
            call FeQuestCrwDisable(CrwLastMade)
            nRolMenu=0
          else
            xpomp=tpom+48.
            if(isPowder) xpomp=xpomp+3.
            dpom=50.
            call FeQuestRolMenuMake(id,xpomp,il,xpomp,il,' ','L',dpom,
     1                            EdwYd,1)
            nRolMenu=RolMenuLastMade
            if(isPowder) then
              call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1          ExpoName,ExpoDefined,5,NExpo)
            else
              call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1          SIRName,SIRDefined,6,NSIR)
            endif
          endif
          Veta='use Super%flip'
        else if(i.eq.2) then
          Veta='use Shelx%t'
        else if(i.eq.3) then
          if(CallShelxt.eq.' ') call FeQuestCrwDisable(CrwLastMade)
        endif
      enddo
      if(isPowder.eqv.WasPowder) then
        if(NPrgmO.le.0) then
          NPrgm=IdSuperflip
          nCrw=nCrwProgramFirst+1
        else
          NPrgm=NPrgmO
        endif
      else
        NPrgm=IdSuperflip
        nCrw=nCrwProgramFirst+1
      endif
      WasPowder=isPowder
      if(NPrgm.lt.IdSuperflip) then
        nCrw=nCrwProgramFirst
      else if(NPrgm.eq.IdSuperflip) then
        nCrw=nCrwProgramFirst+1
      else
        nCrw=nCrwProgramLast
      endif
      call FeQuestCrwOn(nCrw)
      il=0
      tpom=200.
      xpom=tpom+FeTxLength('XXXXXXXXXXXXXX')+3.
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='F%ormula'
          dpom=xqd-xpom-5.
        else
          Veta='Formula %units'
          dpom=50.
        endif
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwFormula=EdwLastMade
          call FeQuestStringEdwOpen(EdwLastMade,Formula(KPhase))
        else
          call FeQuestIntEdwOpen(EdwLastMade,NUnits(KPhase),.false.)
          nEdwZ=EdwLastMade
        endif
      enddo
      xpomb=xpom+dpom+40.
      Veta='%Calculate density'
      dpom=FeTxLengthUnder(Veta)+40.
      call FeQuestButtonMake(id,xpomb,il,dpom,ButYd,Veta)
      nButtCalculateDensity=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      idl=idel(Grupa(KPhase))
      if(NDim(KPhase).eq.4) then
        i=index(Grupa(KPhase),'(')
        if(i.gt.0) idl=i-1
      endif
      LabelG='Actual space group: '//Grupa(KPhase)(:idl)
      call FeQuestLblMake(id,tpom,il,LabelG,'L','N')
      nLblSG=LblLastMade
      Veta='C%hange the space group'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpomb,il,dpom,ButYd,Veta)
      nButtNewSG=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      if(isPowder) then
        xpom=5.
        tpom=xpom+CrwgXd+10.
        Veta='use in le Bail decomposition structure information for '//
     1       'already identified phases'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      0,0)
        nCrwUseKnownPhases=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,KKleBail(KDatBlock).gt.0)
        if(NPhase.le.1) call FeQuestCrwDisable(CrwLastMade)
        il=il+1
      endif
      call FeQuestLinkaMake(id,il)
      il=il+1
      xpom=5.
      tpom=xpom+CrwXd+10.
      Veta='%allow manual editing of the command file before start'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwEdit=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,ManualEdit)
      il=il+1
      Veta='%Shelxt commands:'
      tpomp=5.
      xpomp=tpomp+FeTxLengthUnder(Veta)+5.
      dpom=xqd-xpomp-5.
      call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwCommandLineShelxt=EdwLastMade
!      il=il+1
      ilp=il
      il=il+1
      Veta=Tabulator//'Space group'//Tabulator//'Orientation'//
     1     Tabulator// 'R1'//Tabulator//'Rweak'//Tabulator//'Alpha'//
     2     Tabulator//'Formula'
      tpomp=5.
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      UseTabsT=UseTabs
      call FeTabsAdd( 20.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(150.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(255.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(295.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(345.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(395.,UseTabs,IdRightTab,' ')
      call FeQuestLblMake(id,tpomp,il,Veta,'L','N')
      call FeQuestLblOff(LblLastMade)
      UseTabs=UseTabsIn
      nLblShelxt=LblLastMade
      il=il+1
      il=-10*il
      xpomp=5.
      tpomp=xpomp+CrwXd+10.
      do i=1,15
        call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,2)
        if(i.eq.1) nCrwShelxtSolution=CrwLastMade
        il=il-7
      enddo
      il=ilp
      Veta='use pre%viously prepared input file for Superflip'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseOldInflip=CrwLastMade
      UseOldInflip=ExistFile(fln(:ifln)//'.inflip')
      call FeQuestCrwOpen(CrwLastMade,UseOldInflip)
      il=il+1
      Veta='use old solution and r'
      if(isPowder) then
        Veta=Veta(:idel(Veta))//'e%i'
      else
        Veta=Veta(:idel(Veta))//'%ei'
      endif
      Veta=Veta(:idel(Veta))//'nterpret it'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseOldSolution=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,UseOldSolution)
      UseOldInflipO=UseOldInflip
      if(.not.ExistFile(fln(:ifln)//'.m81').or..not.UseOldInflip)
     1  call FeQuestCrwDisable(CrwLastMade)
      xpoma=tpom+FeTxLengthUnder(Veta)+200.
      Veta='%Biso:'
      dpom=80.
      call FeQuestEdwMake(id,xpoma-10.,il,xpoma,il,Veta,'R',dpom,EdwYd,
     1                    0)
      nEdwBiso=EdwLastMade
      call FeQuestRealEdwOpen(nEdwBiso,Biso,.false.,.false.)
      il=il+1
      Veta='%Repeat Superflip: Until the convergence detected'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwRepeatConv=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SuperflipRepeatMode.eq.-1)
      Veta='Ma%xcycles:'
      call FeQuestEdwMake(id,xpoma-10.,il,xpoma,il,Veta,'R',dpom,EdwYd,
     1                    0)
      nEdwMaxCycles=EdwLastMade
      if(SuperflipRepeatMode.eq.0) then
        i=MaxCycles2
      else
        i=MaxCycles1
      endif
      call FeQuestIntEdwOpen(EdwLastMade,i,.false.)
      il=il+1
      Veta='Repeat %Superflip: Number of runs'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwRepeatTimes=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SuperflipRepeatMode.eq.1)
      tpome=tpom+FeTxLengthUnder(Veta)+5.
      xpome=tpome+23.
      dpom=80.
      Veta='=>'
      call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwRepeatTimes=EdwLastMade
      if(SuperflipRepeatMode.eq.1)
     1  call FeQuestIntEdwOpen(EdwLastMade,SuperflipRepeatMax,.false.)
      il=il+1
      Veta='Use local normali%zation'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwLocalNorm=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SuperflipLocalNorm)
      il=il+1
      Veta='Use a specific ra%ndom seed'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseRandomSeed=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,UseSuperflipRandomSeed)
      tpome=tpom+FeTxLengthUnder(Veta)+5.
      xpome=tpome+23.
      dpom=80.
      Veta='=>'
      call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwRandomSeed=EdwLastMade
      if(UseSuperflipRandomSeed)
     1  call FeQuestIntEdwOpen(nEdwRandomSeed,SuperflipRandomSeed,
     2                         .false.)
      il=il+1
      Veta='Define explicitl%y delta value'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwDefDelta=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SuperflipDelta.gt.0.)
      tpome=tpom+FeTxLengthUnder(Veta)+5.
      xpome=tpome+23.
      dpom=80.
      Veta='=>'
      call FeQuestEdwMake(id,tpome,il,xpome,il,Veta,'L',dpom,EdwYd,0)
      nEdwDefDelta=EdwLastMade
      if(SuperflipDelta.gt.0.)
     1  call FeQuestRealEdwOpen(EdwLastMade,SuperflipDelta,.false.,
     2                          .false.)
      il=il+1
      ill=il
      tpoml=5.
      Veta='Iteration scheme:'
      call FeQuestLblMake(id,tpoml,il,Veta,'L','N')
      nLblAlgorithm=LblLastMade
      call FeQuestLblOff(LblLastMade)
      xpom=tpoml+FeTxLength(Veta)+5.
      tpom=xpom+CrwgXd+10.
      Veta='CF'
      il=-il*10
      do i=1,3
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      0,3)
        if(i.eq.1) then
          Veta='LDE'
          nCrwCF=CrwLastMade
        else if(i.eq.2) then
          Veta='AAR'
          nCrwLDE=CrwLastMade
        else if(i.eq.3) then
          nCrwAAR=CrwLastMade
        endif
        il=il-7
      enddo
      Veta='Starting model:'
      il=il-3
      call FeQuestLblMake(id,tpoml,il,Veta,'L','N')
      nLblModel=LblLastMade
      call FeQuestLblOff(LblLastMade)
      Veta='Random phases'
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      0,4)
        if(i.eq.1) then
          Veta='Patterson superposition map'
          nCrwRandomPhases=CrwLastMade
        else if(i.eq.2) then
          nCrwPatterson=CrwLastMade
        endif
        il=il-7
      enddo
      il=-10*ill
      tpom=250.
      Veta='For peak search use:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblSeachUse=LblLastMade
      call FeQuestLblOff(LblLastMade)
      xpom=tpom+FeTxLength(Veta)+15.
      tpom=xpom+CrwgXd+10.
      Veta='E%DMA - fixed composition'
      do i=1,5
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,5)
        if(i.eq.1) then
          Veta='EDMA - fixed nu%mber of atoms'
          nCrwEDMA1=CrwLastMade
        else if(i.eq.2) then
          xpome=tpom+FeTxLengthUnder(Veta)+8.
          dpome=xqd-xpome-5.
          call FeQuestEdwMake(id,tpom,il,xpome,il,' ','L',dpome,EdwYd,0)
          nEdwEDAtoms=EdwLastMade
          Veta='EDMA - peak interpretation by Jana%2006'
          nCrwEDMA2=CrwLastMade
        else if(i.eq.3) then
          Veta='%Peaks from Jana2006'
          nCrwEDMA3=CrwLastMade
        else if(i.eq.4) then
          Veta='Pea%ks from Jana2006 but first run Fourier'
          nCrwPeaks=CrwLastMade
        else
          nCrwFourier=CrwLastMade
        endif
        il=il-8
      enddo
      il=ilp
      tpom=5.
      Veta='%Stop when R% is less then:'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwResidual=EdwLastMade
      il=ilm-1
      dpom=100.
      xpom=.5*xqd-2.*dpom-15.
      Veta='Run solution'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRun=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+10.
      Veta='Open the listing'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtListing=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+10.
      Veta='Draw structure'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDraw=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+10.
      Veta='Draw 3d map'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButt3dMap=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      xpom=.5*xqd-1.*dpom-5.
      Veta='Accept last solution'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAccept=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+10.
      Veta='Quit'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtQuit=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1300  nCrw=nCrwProgramFirst
      do i=1,PocetPrgm
        if(CrwStateQuest(nCrw).ne.CrwDisabled) then
          if(CrwLogicQuest(nCrw)) then
            NPrgm=i
            exit
          endif
        endif
        nCrw=nCrw+1
      enddo
      if(NPrgm.eq.1) then
        if(isPowder) then
          NExpo=RolMenuSelectedQuest(nRolMenu)
          NPrgm=IdExpo+NExpo-1
        else
          NSIR=RolMenuSelectedQuest(nRolMenu)
          NPrgm=IdSIR97+NSIR-1
        endif
      else if(NPrgm.eq.2) then
        NPrgm=IdSuperflip
      else if(NPrgm.eq.3) then
        NPrgm=IdShelxt
      endif
      if((isPowder.or.NPrgm.le.IdSIR97.or.NPrgm.eq.IdSuperflip.or.
     1    NPrgm.eq.IdShelxt).and.
     3    EdwStateQuest(nEdwResidual).eq.EdwOpened) then
        call FeQuestRealFromEdw(nEdwResidual,Residual)
        call FeQuestEdwClose(nEdwResidual)
      endif
      if(NPrgm.ne.IdShelxt.and.
     1   EdwStateQuest(nEdwCommandLineShelxt).eq.EdwOpened) then
        CommandLineShelxt=EdwStringQuest(nEdwCommandLineShelxt)
        call FeQuestEdwClose(nEdwCommandLineShelxt)
        UseTabsIn=UseTabs
        UseTabs=UseTabsT
        call FeQuestLblOff(nLblShelxt)
        nCrw=nCrwShelxtSolution
        if(CrwStateQuest(nCrw).eq.CrwOff.or.
     1     CrwStateQuest(nCrw).eq.CrwOn) then
          UseTabs=UseTabsS
          do i=1,15
            call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
          UseTabs=UseTabsIn
        endif
      endif
      if(NPrgm.ne.IdSuperflip) then
        if(nLblSeachUse.gt.0) then
          call FeQuestCrwClose(nCrwUseOldInflip)
          SuperflipLocalNorm=CrwLogicQuest(nCrwLocalNorm)
          call FeQuestCrwClose(nCrwLocalNorm)
          UseSuperflipRandomSeed=CrwLogicQuest(nCrwUseRandomSeed)
          if(UseSuperflipRandomSeed)
     1      call FeQuestIntFromEdw(nEdwRandomSeed,SuperflipRandomSeed)
          if(EdwStateQuest(nEdwRepeatTimes).eq.EdwOpened) then
            call FeQuestIntFromEdw(nEdwMaxCycles,i)
            if(SuperflipRepeatMode.eq.0) then
              MaxCycles2=i
            else
              MaxCycles1=i
            endif
          endif
          if(CrwLogicQuest(nCrwRepeatConv)) then
            SuperflipRepeatMode=-1
          else if(CrwLogicQuest(nCrwRepeatTimes)) then
            SuperflipRepeatMode= 1
          else
            SuperflipRepeatMode= 0
          endif
          UseOldSolution=CrwLogicQuest(nCrwUseOldSolution)
          call FeQuestCrwClose(nCrwUseOldSolution)
          call FeQuestCrwClose(nCrwRepeatConv)
          call FeQuestCrwClose(nCrwRepeatTimes)
          if(EdwStateQuest(nEdwRepeatTimes).eq.EdwOpened)
     1      call FeQuestIntFromEdw(nEdwRepeatTimes,SuperflipRepeatMax)
          call FeQuestEdwClose(nEdwRepeatTimes)
          call FeQuestCrwClose(nCrwUseRandomSeed)
          call FeQuestEdwClose(nEdwRandomSeed)
          call FeQuestCrwClose(nCrwDefDelta)
          if(EdwStateQuest(nEdwDefDelta).eq.EdwOpened)
     1      call FeQuestRealFromEdw(nEdwDefDelta,SuperflipDeltaDef)
          call FeQuestEdwClose(nEdwDefDelta)
          call FeQuestEdwClose(nEdwMaxCycles)
          call FeQuestRealFromEdw(nEdwBiso,Biso)
          call FeQuestEdwClose(nEdwBiso)
          call FeQuestLblOff(nLblAlgorithm)
          nCrw=nCrwCF
          do i=1,3
            if(CrwStateQuest(nCrw).ne.CrwClosed.and.
     1         CrwStateQuest(nCrw).ne.CrwDisabled) then
              if(CrwLogicQuest(nCrw)) SuperflipAlgorithm=i
            endif
            call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
          call FeQuestLblOff(nLblSeachUse)
          nCrw=nCrwRandomPhases
          do i=1,2
            if(CrwStateQuest(nCrw).ne.CrwClosed.and.
     1         CrwStateQuest(nCrw).ne.CrwDisabled) then
              if(CrwLogicQuest(nCrw)) SuperflipStartModel=i
            endif
            call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
          call FeQuestLblOff(nLblModel)
          nCrw=nCrwEDMA1
          do i=1,5
            call FeQuestCrwClose(nCrw)
            nCrw=nCrw+1
          enddo
          call FeQuestEdwClose(nEdwEDAtoms)
        endif
        if(NPrgm.ne.IdSIR97.and.NPrgm.ne.IdShelxt.and..not.isPowder)
     1    then
          call FeQuestRealEdwOpen(nEdwResidual,Residual,.false.,.false.)
          if(CrwStateQuest(nCrwEdit).ne.CrwOn.and.
     1       CrwStateQuest(nCrwEdit).ne.CrwOff)
     2      call FeQuestCrwOpen(nCrwEdit,ManualEdit)
        endif
      else if(NPrgm.eq.IdSuperflip) then
        call FeQuestCrwOpen(nCrwUseOldSolution,UseOldSolution)
        if(ExistFile(fln(:ifln)//'.inflip')) then
          call FeQuestCrwOpen(nCrwUseOldInflip,UseOldInflip)
        else
          call FeQuestCrwDisable(nCrwUseOldInflip)
          UseOldInflip=.false.
        endif
        call FeQuestLblOn(nLblSeachUse)
        nCrw=nCrwEDMA1
        if(NDimI(KPhase).eq.1.and.NPeakSearch.eq.4) NPeakSearch=5
        do i=1,5
          call FeQuestCrwOpen(nCrw,i.eq.NPeakSearch)
          if(NDimI(KPhase).eq.1.and.i.eq.4)
     1      call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
        if(NPeakSearch.eq.2) then
          if(EDAtoms.le.0) then
            pom=0.
            ppp=NLattVec(KPhase)*NSymm(KPhase)
            do i=1,NAtFormula(KPhase)
              if((EqIgCase(AtType(i,KPhase),'H').or.
     1            EqIgCase(AtType(i,KPhase),'D')).and.
     2            Radiation(KDatBlock).ne.NeutronRadiation) cycle
              pom=pom+AtMult(i,KPhase)
            enddo
            EDAtoms=max(nint(pom*ppp),1)
          endif
          call FeQuestIntEdwOpen(nEdwEDAtoms,EDAtoms,EDAtoms.le.0)
        else
          call FeQuestEdwDisable(nEdwEDAtoms)
        endif
      endif
      if(NPrgm.eq.IdShelxt) then
        if(isPowder) then
          if(CrwStateQuest(nCrwEdit).ne.CrwOn.and.
     1       CrwStateQuest(nCrwEdit).ne.CrwOff)
     2      call FeQuestCrwOpen(nCrwEdit,ManualEdit)
        else if(CrwStateQuest(nCrwEdit).eq.CrwOn.or.
     1     CrwStateQuest(nCrwEdit).eq.CrwOff) then
          ManualEdit=CrwLogicQuest(nCrwEdit)
          call FeQuestCrwClose(nCrwEdit)
        endif
        if(EdwStateQuest(nEdwCommandLineShelxt).ne.EdwOpened)
     1    call FeQuestStringEdwOpen(nEdwCommandLineShelxt,
     2                              CommandLineShelxt)
      else
        if(CrwStateQuest(nCrwEdit).ne.CrwOn.and.
     1     CrwStateQuest(nCrwEdit).ne.CrwOff)
     2    call FeQuestCrwOpen(nCrwEdit,ManualEdit)
      endif
      if(NPrgm.ne.IdSuperflip) go to 1500
1400  if(UseOldInflip.or.UseOldSolution) then
        SuperflipLocalNorm=CrwLogicQuest(nCrwLocalNorm)
        if(CrwLogicQuest(nCrwRepeatConv)) then
          SuperflipRepeatMode=-1
        else if(CrwLogicQuest(nCrwRepeatTimes)) then
          SuperflipRepeatMode= 1
        else
          SuperflipRepeatMode= 0
        endif
        if(.not.ExistFile(fln(:ifln)//'.m81').or.
     1     .not.ExistFile(fln(:ifln)//'.inflip')) then
          call FeQuestCrwDisable(nCrwUseOldSolution)
        else
          call FeQuestCrwOpen(nCrwUseOldSolution,UseOldSolution)
          UseOldInflipO=UseOldInflip
        endif
        if(UseSuperflipRandomSeed)
     1    call FeQuestIntFromEdw(nEdwRandomSeed,SuperflipRandomSeed)
        call FeQuestCrwDisable(nCrwRepeatConv)
        if(EdwStateQuest(nEdwRepeatTimes).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwRepeatTimes,SuperflipRepeatMax)
          call FeQuestEdwClose(nEdwRepeatTimes)
        endif
        call FeQuestCrwDisable(nCrwDefDelta)
        if(EdwStateQuest(nEdwDefDelta).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwDefDelta,SuperflipDeltaDef)
          call FeQuestEdwClose(nEdwDefDelta)
        endif
        call FeQuestCrwDisable(nCrwRepeatTimes)
        call FeQuestCrwDisable(nCrwLocalNorm)
        call FeQuestRealFromEdw(nEdwBiso,Biso)
        call FeQuestEdwDisable(nEdwBiso)
        if(EdwStateQuest(nEdwRepeatTimes).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwMaxCycles,i)
          if(SuperflipRepeatMode.eq.0) then
            MaxCycles2=i
          else
            MaxCycles1=i
          endif
        endif
        call FeQuestCrwDisable(nCrwUseRandomSeed)
        call FeQuestEdwClose(nEdwRandomSeed)
        call FeQuestEdwDisable(nEdwMaxCycles)
        call FeQuestLblDisable(nLblAlgorithm)
        nCrw=nCrwCF
        do i=1,3
          if(CrwStateQuest(nCrw).ne.CrwClosed.and.
     1       CrwStateQuest(nCrw).ne.CrwDisabled) then
            if(CrwLogicQuest(nCrw)) SuperflipAlgorithm=i
          endif
          call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
        call FeQuestLblDisable(nLblModel)
        nCrw=nCrwRandomPhases
        do i=1,2
          if(CrwStateQuest(nCrw).ne.CrwClosed.and.
     1       CrwStateQuest(nCrw).ne.CrwDisabled) then
            if(CrwLogicQuest(nCrw)) SuperflipStartModel=i
          endif
          call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
      else
        if(CrwStateQuest(nCrwDefDelta).ne.CrwOn.and.
     1     CrwStateQuest(nCrwDefDelta).ne.CrwOff) then
c          SuperflipDelta=SuperflipDeltaDef
          call FeQuestCrwOpen(nCrwDefDelta,SuperflipDelta.gt.0.)
        endif
        call FeQuestCrwOpen(nCrwRepeatConv ,SuperflipRepeatMode.eq.-1)
        call FeQuestCrwOpen(nCrwRepeatTimes,SuperflipRepeatMode.eq. 1)
        call FeQuestRealEdwOpen(nEdwBiso,Biso,.false.,.false.)
        call FeQuestCrwOpen(nCrwLocalNorm,SuperflipLocalNorm)
        if(SuperflipRepeatMode.eq.0) then
          i=MaxCycles2
        else
          i=MaxCycles1
        endif
        call FeQuestIntEdwOpen(nEdwMaxCycles,i,.false.)
        call FeQuestCrwOpen(nCrwUseRandomSeed,UseSuperflipRandomSeed)
        if(UseSuperflipRandomSeed)
     1    call FeQuestIntEdwOpen(nEdwRandomSeed,SuperflipRandomSeed,
     2                           .false.)
        if(CrwLogicQuest(nCrwDefDelta)) then
          if(EdwStateQuest(nEdwDefDelta).ne.EdwOpened) then
            SuperflipDelta=SuperflipDeltaDef
            call FeQuestRealEdwOpen(nEdwDefDelta,SuperflipDelta,.false.,
     1                              .false.)
          endif
        else
          call FeQuestEdwClose(nEdwDefDelta)
        endif
        if(CrwLogicQuest(nCrwRepeatTimes)) then
          if(EdwStateQuest(nEdwRepeatTimes).ne.EdwOpened) then
            call FeQuestIntEdwOpen(nEdwRepeatTimes,SuperflipRepeatMax,
     1                             .false.)
          endif
        else
          call FeQuestEdwClose(nEdwRepeatTimes)
        endif
        call FeQuestLblOn(nLblAlgorithm)
        nCrw=nCrwCF
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.SuperflipAlgorithm)
          nCrw=nCrw+1
        enddo
        call FeQuestLblOn(nLblModel)
        nCrw=nCrwRandomPhases
        do i=1,2
          call FeQuestCrwOpen(nCrw,i.eq.SuperflipStartModel)
          nCrw=nCrw+1
        enddo
      endif
      call FeQuestButtonDisable(nButtListing)
      call FeQuestButtonDisable(nButt3dMap)
1500  if((NPrgm.eq.IdShelxt.and.NShelxt.gt.0).or.
     1   (NPrgm.eq.IdSuperflip.and.
     2    ExistFile(SuperflipSol(:iSuperflipSol)//'.m40')).or.
     3    (NPrgm.ne.IdShelxt.and.NPrgm.ne.IdSuperflip.and.
     4    ExistFile(SirSol(:iSirSol)//'.m40'))) then
        if(ButtonStateQuest(nButtAccept).ne.ButtonOff)
     1    call FeQuestButtonOff(nButtAccept)
        if(ButtonStateQuest(nButtDraw).ne.ButtonOff)
     1    call FeQuestButtonOff(nButtDraw)
      else
        if(ButtonStateQuest(nButtAccept).ne.ButtonDisabled)
     1    call FeQuestButtonDisable(nButtAccept)
        if(ButtonStateQuest(nButtDraw).ne.ButtonDisabled)
     1    call FeQuestButtonDisable(nButtDraw)
      endif
      if((NPrgm.eq.IdShelxt.and.
     1    ExistFile(ShelxtName(:iShelxtName)//'.lxt')).or.
     2   (NPrgm.eq.IdSuperflip.and.
     3    ExistFile(SuperflipSol(:iSuperflipSol)//'.sflog'))) then
        if(ButtonStateQuest(nButtListing).ne.ButtonOff)
     1    call FeQuestButtonOff(nButtListing)
      else
        if(ButtonStateQuest(nButtListing).ne.ButtonDisabled)
     1    call FeQuestButtonDisable(nButtListing)
      endif
      call FeQuestEvent(id,ich)
      if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtRun).or.
     1   (CheckType.eq.EventButton.and.
     2                      CheckNumber.eq.nButtCalculateDensity)) then
        if(EdwStateQuest(nEdwResidual).eq.EdwOpened)
     1    call FeQuestRealFromEdw(nEdwResidual,Residual)
        call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
        Formula(KPhase)=EdwStringQuest(nEdwFormula)
        if(Formula(KPhase).eq.' ') then
          call FeChybne(-1.,-1.,'The formula isn''t defined.',' ',
     1                  SeriousError)
          go to 1500
        else
          call PitFor(1,ichp)
          if(ichp.ne.0) go to 1520
          call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
          if(NAtFormulaOld.le.0) FFType(KPhase)=-62
          call PitFor(0,ichp)
          if(ichp.ne.0) go to 1520
        endif
        call FeQuestIntFromEdw(nEdwZ,NUnits(KPhase))
        if(NUnits(KPhase).le.0) then
          call FeChybne(-1.,-1.,'The number of formula units has '//
     1                  'to be a positive number.',' ',SeriousError)
          go to 1500
        endif
        if(CheckNumber.eq.nButtCalculateDensity) then
          NInfo=0
          call EM50CalcDenAbs(ichp)
          if(ichp.ne.0) then
            TextInfo(1)='Error during calculation of density and '//
     1                  'absorption coefficient'
          endif
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          go to 1500
        else
          go to 1550
        endif
1520    call FeChybne(-1.,-1.,'The formula has a syntax error.',' ',
     1                SeriousError)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAccept)
     1  then
        if(NPrgm.eq.IdShelxt) then
          FlnOld=ShelxtSol(NShelxtSel)
          iflnOld=idel(FlnOld)
          call CopyMat(TrShelxt(1,NShelxtSel),trp,NDim(KPhase))
        else if(NPrgm.eq.IdSuperflip) then
          FlnOld=SuperflipSol
          call CopyMat(TrMPIn,trp,NDim(KPhase))
        else
          FlnOld=SirSol
          call CopyMat(TrMPIn,trp,NDim(KPhase))
        endif
        iflnOld=idel(FlnOld)
        call CopyFile(FlnOld(:iFlnOld)//'.m40',fln(:ifln)//'.m40')
        call CopyFile(FlnOld(:iFlnOld)//'.m50',fln(:ifln)//'.m50')
        if(NPrgm.eq.IdSuperflip) then
          call CopyFile(FlnOld(:iFlnOld)//'.m80',fln(:ifln)//'.m80')
          call CopyFile(FlnOld(:iFlnOld)//'.m81',fln(:ifln)//'.m81')
          call CopyFile(FlnOld(:iFlnOld)//'.inflip',
     1                  fln(:ifln)//'.inflip')
          call CopyFile(FlnOld(:iFlnOld)//'.sflog',fln(:ifln)//'.sflog')
        else
          call DeleteFile(fln(:ifln)//'.m80')
          call DeleteFile(fln(:ifln)//'.m81')
          call DeleteFile(fln(:ifln)//'.inflip')
          call DeleteFile(fln(:ifln)//'.sflog')
        endif
        call iom50(0,0,fln(:ifln)//'.m50')
        if(isPowder) then
          call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
          call iom41(1,0,fln(:ifln)//'.m41')
        endif
        call iom40(0,0,fln(:ifln)//'.m40')
        if(.not.EqRV(trp,TrMPIn,NDimQ(KPhase),.0001)) then
          call CopyMat(TrShelxt(1,NShelxtSel),TrMP,NDim(KPhase))
          call CompleteM95(0)
          ExistM90=.false.
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          SilentRun=.true.
          call EM9CreateM90(0,ich)
          SilentRun=.false.
        endif
        go to 9999
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)
     1  then
        if(LastSolution.gt.0) then
          if(.not.FeYesNo(-1.,-1.,
     1       'Do you really want to discard the last solution?',0))
     2      go to 1500
        endif
        call CopyFile(PreviousM40,fln(:ifln)//'.m40')
        call CopyFile(PreviousM50,fln(:ifln)//'.m50')
        if(isPowder) call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        go to 9999
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDraw)
     1  then
        FlnOld=Fln
        iFlnOld=ifln
        if(NPrgm.eq.IdShelxt) then
          Fln=ShelxtSol(NShelxtSel)
        else if(NPrgm.eq.IdSuperflip) then
          Fln=SuperFlipSol
        else
          Fln=SirSol
        endif
        ifln=idel(Fln)
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        call CrlPlotStructure
        Fln=FlnOld
        ifln=iFlnOld
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButt3dMap)
     1  then
        FlnOld=Fln
        iFlnOld=ifln
        if(NPrgm.eq.IdShelxt) then
          Fln=ShelxtSol(NShelxtSel)
        else if(NPrgm.eq.IdSuperflip) then
          Fln=SuperFlipSol
        else
          Fln=SirSol
        endif
        ifln=idel(Fln)
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        call CrlDraw3dMap
        Fln=FlnOld
        ifln=iFlnOld
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtListing)
     1  then
        if(NPrgm.eq.IdShelxt) then
          call FeListView(ShelxtName(:iShelxtName)//'.lxt',1)
        else
          call FeListView(SuperflipSol(:iSuperflipSol)//'.sflog',1)
        endif
        go to 1500
      endif
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.ge.nCrwProgramFirst.and.
     2    CheckNumber.le.nCrwProgramLast)) then
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.ge.nCrwEDMA1.and.
     2         CheckNumber.le.nCrwFourier)) then
        NPeakSearch=CheckNumber-nCrwEDMA1+1
        if(NPeakSearch.eq.2) then
          if(EDAtoms.le.0) then
            pom=0.
            ppp=NLattVec(KPhase)*NSymm(KPhase)
            do i=1,NAtFormula(KPhase)
              if((EqIgCase(AtType(i,KPhase),'H').or.
     1            EqIgCase(AtType(i,KPhase),'D')).and.
     2            Radiation(KDatBlock).ne.NeutronRadiation) cycle
              pom=pom+AtMult(i,KPhase)
            enddo
            EDAtoms=max(nint(pom*ppp),1)
          endif
          call FeQuestIntEdwOpen(nEdwEDAtoms,EDAtoms,EDAtoms.le.0)
        else
          if(EdwStateQuest(nEdwEDAtoms).eq.EdwOpened) then
            call FeQuestIntFromEdw(nEdwEDAtoms,EDAtoms)
            call FeQuestEdwDisable(nEdwEDAtoms)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenu) then
        nCrw=nCrwProgramFirst
        do i=1,2
          call FeQuestCrwOpen(nCrw,i.eq.1)
          nCrw=nCrw+1
        enddo
        go to 1300
      else if(CheckType.eq.EventCrw.and.
     1        (CheckNumber.ge.nCrwShelxtSolution.and.
     2         CheckNumber.le.nCrwShelxtSolution+14)) then
        NShelxtSel=CheckNumber-nCrwShelxtSolution+1
        LastSolution=NShelxtSel
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwUseOldInflip) then
        UseOldInflip=CrwLogicQuest(nCrwUseOldInflip)
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwUseOldSolution) then
        UseOldSolution=CrwLogicQuest(nCrwUseOldSolution)
        if(UseOldSolution) then
          UseOldInflipO=UseOldInflip
          UseOldInflip=.false.
          call FeQuestCrwDisable(nCrwUseOldInflip)
        else
          UseOldInflip=UseOldInflipO
          call FeQuestCrwOpen(nCrwUseOldInflip,UseOldInflip)
        endif
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwLocalNorm) then
        SuperflipLocalNorm=CrwLogicQuest(nCrwLocalNorm)
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwUseRandomSeed) then
        UseSuperflipRandomSeed=CrwLogicQuest(nCrwUseRandomSeed)
        if(UseSuperflipRandomSeed) then
          call FeQuestIntEdwOpen(nEdwRandomSeed,SuperflipRandomSeed,
     1                           .false.)
        else
          call FeQuestIntFromEdw(nEdwRandomSeed,SuperflipRandomSeed)
          call FeQuestEdwClose(nEdwRandomSeed)
        endif
        go to 1400
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDefDelta)
     1  then
        if(CrwLogicQuest(nCrwDefDelta)) then
          SuperflipDelta=SuperflipDeltaDef
        else
          call FeQuestRealFromEdw(nEdwDefDelta,SuperflipDeltaDef)
          SuperflipDelta=0.
        endif
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwRepeatConv) then
        if(CrwLogicQuest(nCrwRepeatConv)) then
          SuperflipRepeatMode=-1
        else
          SuperflipRepeatMode= 0
        endif
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwRepeatTimes) then
        if(CrwLogicQuest(nCrwRepeatTimes)) then
          SuperflipRepeatMode= 1
        else
          SuperflipRepeatMode= 0
        endif
        go to 1400
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtCalculateDensity) then
        NInfo=0
        call EM50CalcDenAbs(ichp)
        if(ichp.ne.0) then
          TextInfo(1)='Error during calculation of density and '//
     1                'absorption coefficient'
        endif
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNewSG)
     1  then
        GrupaOld=Grupa(KPhase)
        call EM50DefSpaceGroup(0,ich)
        if(Grupa(KPhase).ne.GrupaOld) then
          Veta=fln(:ifln)//'.inflip'
          if(ExistFile(Veta)) call DeleteFile(Veta)
          if(ExistSingle) then
            ModifyDatBlock(KDatBlock)=.true.
            call EM9CreateM90(0,ich)
            GrupaOld=Grupa(KPhase)
            call iom90(0,fln(:ifln)//'.m90')
          endif
          LabelG='Actual space group: '//
     1           Grupa(KPhase)(:idel(Grupa(KPhase)))
          call FeQuestLblChange(nLblSG,LabelG)
        endif
        go to 1300
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
1550  NPrgmO=NPrgm
      if(isPowder.and.NPhase.gt.1) then
        if(CrwLogicQuest(nCrwUseKnownPhases)) then
          KKleBail(KDatBlock)=1
        else
          KKleBail(KDatBlock)=0
        endif
      endif
      ManualEdit=CrwLogicQuest(nCrwEdit)
      if(NPrgm.eq.IdSuperflip) then
        flnOld=fln
        iflnOld=ifln
        fln=SuperflipSol
        ifln=iSuperflipSol
        call CopyFile(flnOld(:iflnOld)//'.m40',fln(:ifln)//'.m40')
        if(isPowder)
     1    call CopyFile(flnOld(:iflnOld)//'.m41',fln(:ifln)//'.m41')
        call CopyFile(flnOld(:iflnOld)//'.m50',fln(:ifln)//'.m50')
        call CopyFile(flnOld(:iflnOld)//'.m90',fln(:ifln)//'.m90')
        nCrw=nCrwCF
        do i=1,3
          if(CrwStateQuest(nCrw).ne.CrwClosed.and.
     1       CrwStateQuest(nCrw).ne.CrwDisabled) then
            if(CrwLogicQuest(nCrw)) then
              SuperflipAlgorithm=i
              exit
            endif
          endif
          nCrw=nCrw+1
        enddo
        call FeQuestLblDisable(nLblModel)
        nCrw=nCrwRandomPhases
        do i=1,2
          if(CrwStateQuest(nCrw).ne.CrwClosed.and.
     1       CrwStateQuest(nCrw).ne.CrwDisabled) then
            if(CrwLogicQuest(nCrw)) then
              SuperflipStartModel=i
              exit
            endif
          endif
          nCrw=nCrw+1
        enddo
        nCrw=nCrwEDMA1
        do i=1,5
          if(CrwLogicQuest(nCrw)) then
            NPeakSearch=i
            exit
          endif
          nCrw=nCrw+1
        enddo
        if(EdwStateQuest(nEdwEDAtoms).eq.EdwOpened) then
          call FeQuestIntFromEdw(nEdwEDAtoms,EDAtoms)
        else
          EDAtoms=-1
        endif
        if(index(fln(:ifln),' ').gt.0.and.NPeakSearch.le.3) then
          call FeChybne(-1.,-1.,'the EDMA program cannot handle a '//
     1                  'file name with spaces',' ',SeriousError)
          go to 1500
        endif
        if(ExistFile(fln(:ifln)//'.inflip'))
     1    UseOldInflip=CrwLogicQuest(nCrwUseOldInflip)
        if(.not.UseOldInflip) then
          if(CrwLogicQuest(nCrwRepeatConv)) then
            SuperflipRepeatMode=-1
          else if(CrwLogicQuest(nCrwRepeatTimes)) then
            SuperflipRepeatMode= 1
          else
            SuperflipRepeatMode= 0
          endif
          call FeQuestRealFromEdw(nEdwBiso,Biso)
          call FeQuestIntFromEdw(nEdwMaxCycles,i)
          if(SuperflipRepeatMode.eq.0) then
            MaxCycles2=i
          else
            MaxCycles1=i
          endif
          SuperflipMaxCycles=i
          if(CrwLogicQuest(nCrwRepeatTimes)) then
            call FeQuestIntFromEdw(nEdwRepeatTimes,SuperflipRepeatMax)
          endif
          UseSuperflipRandomSeed=CrwLogicQuest(nCrwUseRandomSeed)
          if(UseSuperflipRandomSeed) then
            call FeQuestIntFromEdw(nEdwRandomSeed,SuperflipRandomSeed)
            MEMCFRS=SuperflipRandomSeed
            AutoRS=.false.
          else
!            SuperflipRandomSeed=0
            MEMCFRS=1000
            AutoRS=.true.
          endif
          if(CrwLogicQuest(nCrwDefDelta)) then
            call FeQuestRealFromEdw(nEdwDefDelta,SuperflipDelta)
          else
            SuperflipDelta=0.
          endif
        endif
      else if(NPrgm.eq.IdShelxt) then
        CommandLineShelxt=EdwStringQuest(nEdwCommandLineShelxt)
      endif
      do i=1,NAtFormula(KPhase)
        AtTypeFull(i,KPhase)=AtType(i,KPhase)
        FFType(KPhase)=-62
        CoreValSource='Default'
      enddo
      call EM50ReadAllFormFactors
      call EM50FormFactorsSet
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      call CrlAtomNamesIni
      if(NPrgm.eq.IdShelxt) then
        call DeleteAllFiles(ShelxtName(:iShelxtName)//'*')
        Radka=ShelxtName(:iShelxtName)//'.ins'
        ns=NSymm(KPhase)
        allocate(s6p(NDim(KPhase),NSymm(KPhase)))
        do i=1,ns
          s6p(1:NDim(KPhase),i)=s6(1:NDim(KPhase),i,1,KPhase)
          s6(1:NDim(KPhase),i,1,KPhase)=0.
        enddo
        if(CrlCentroSymm().le.0) then
          call ReallocSymm(NDim(KPhase),2*ns,NLattVec(KPhase),
     1                     NComp(KPhase),NPhase,0)
          do i=1,ns
            rm6(1:NDimQ(KPhase),ns+i,1,KPhase)=
     1        -rm6(1:NDimQ(KPhase),i,1,KPhase)
            rm(1:9,ns+i,1,KPhase)=-rm(1:9,i,1,KPhase)
            s6(1:NDim(KPhase),ns+i,1,KPhase)=0.
          enddo
          NSymm(KPhase)=2*ns
        endif
        call Trm4050(5,Radka,' ',ich)
        do i=1,ns
          s6(1:NDim(KPhase),i,1,KPhase)=s6p(1:NDim(KPhase),i)
        enddo
        deallocate(s6p)
        NSymm(KPhase)=ns
        if(isPowder) then
          scp=sc(2,KDatBlock)
          call SetBasicM40(.true.)
          sc(2,KDatBlock)=scp
          call PwdLeBail(1,.true.)
          call DeleteAllFiles(fln(:ifln)//'.l??')
          call iom50(0,0,fln(:ifln)//'.m50')
          call FeFlowChartRemove
          Veta=fln(:ifln)//'.rfl'
          if(.not.ExistFile(Veta).or.ErrFlag.ne.0) go to 9999
          ln=NextLogicNumber()
          call OpenFile(ln,Veta,'formatted','unknown')
          write(FormRfl(2:2),'(i1)') NDim(KPhase)
          FormatShelx(2:2)=FormRfl(2:2)
          lno=NextLogicNumber()
          Veta=ShelxtName(:iShelxtName)//'.hkl'
          call OpenFile(lno,Veta,'formatted','unknown')
          write(FormRfl(2:2),'(i1)') NDim(KPhase)
          n0=0
          n=0
1600      read(ln,FormRfl,end=1610,err=1610)(ih(i),i=1,NDim(KPhase)),
     1                                      F,pom
          if(F.ne.0.) then
            n0=0
          else
            n0=n0+1
          endif
          n=n+1
          go to 1600
1610      m=n-n0
          rewind ln
          n=0
1620      read(ln,FormRfl,end=1650,err=1650)(ih(i),i=1,NDim(KPhase)),
     1                                      F,pom
          n=n+1
          if(n.gt.m) go to 1650
          if(F.gt.0.) then
            pom=sqrt(F)
          else
            pom=1.
          endif
          write(lno,formatshelx)(ih(i),i=1,NDim(KPhase)),F,pom,1
          go to 1620
1650      call CloseIfOpened(ln)
          call CloseIfOpened(lno)
          if(ManualEdit) call FeEdit(Veta)
        else
          call DRExport(-1,ShelxtName)
        endif
        Veta=CallShelxt(:idel(CallShelxt))//' '//
     1       ShelxtName(:iShelxtName)//' '//
     2       CommandLineShelxt(:idel(CommandLineShelxt))
        if(WineKey.le.0) then
          call FeSystemCommand(Veta,0)
          call FeWaitInfo('end of Shelxt.')
        else
          call FeSystem(Veta)
        endif
        UseTabsIn=UseTabs
        if(UseTabsS.gt.0) then
          UseTabs=UseTabsS
        else
          UseTabs=NextTabs()
          UseTabsS=UseTabs
          call FeTabsAdd(150.,UseTabs,IdCenterTab,' ')
          call FeTabsAdd(230.,UseTabs,IdCharTab,'.')
          call FeTabsAdd(280.,UseTabs,IdCharTab,'.')
          call FeTabsAdd(330.,UseTabs,IdCharTab,'.')
        endif
        ln=NextLogicNumber()
        call OpenFile(ln,ShelxtName(:iShelxtName)//'.lxt','formatted',
     1                'unknown')
1700    read(ln,FormA,end=1750) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'R1')) go to 1700
        call kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'Rweak')) go to 1700
        call kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'Alpha')) go to 1700
        NShelxt=0
1720    read(ln,FormA,end=1750) Veta
        if(Veta.eq.' ') go to 1720
        k=0
        call kus(Veta,k,t80)
        if(EqIgCase(t80,'assign').or.NShelxt.ge.15) go to 1750
        NShelxt=NShelxt+1
        read(Veta,'(3f6.3)',err=1750) Pom1,Pom2,Pom3
        write(Radka,'(3(a1,f8.3))') Tabulator,Pom1,Tabulator,Pom2,
     1                              Tabulator,Pom3
        k=18
        call kus(Veta,k,Orientation)
        call kus(Veta,k,s80)
        Orientation=Orientation(:idel(Orientation))//' '//s80
        if(EqIgCase(Orientation,'as input')) then
          TrShelxt(1:NDimQ(KPhase),NShelxt)=TrMPIn(1:NDimQ(KPhase))
        else
          call kus(Veta,k,s80)
          Orientation=Orientation(:idel(Orientation))//' '//s80
          kk=0
          do i=1,3
            call kus(Orientation,kk,s80)
            idp=idel(s80)
            if(s80(idp:idp).eq.',') s80(idp:idp)=' '
            j=index(abc,s80(1:1))
            l=index(s80,'=')
            Raw(j)=s80(l+1:)
          enddo
          s80=Raw(1)(:idel(Raw(1)))//','//Raw(2)(:idel(Raw(2)))//','//
     1        Raw(3)(:idel(Raw(3)))
          call EqStringToIntMatrix(s80,abc,3,ITrp,ITrvp,ich)
          call UnitMat(trp,NDim(KPhase))
          do i=1,3
            do j=1,3
              trp(i+(j-1)*NDim(KPhase))=ITrp(i,j)
            enddo
          enddo
          call Multm(TrMP,trp,TrShelxt(1,NShelxt),NDim(KPhase),
     1               NDim(KPhase),NDim(KPhase))
        endif
        call kus(Veta,k,t80)
        Radka=t80(:idel(t80))//Tabulator//
     1        Orientation(:idel(Orientation))//Radka(:idel(Radka))
        i=LocateSubstring(Veta,ShelxtName(:iShelxtName),.false.,.true.)
        if(i.gt.0) then
          Veta=Veta(i+iShelxtName:)
          k=0
          call kus(Veta,k,Cislo)
          t80=Veta(k:)
          ShelxtSol(NShelxt)=ShelxtName(:iShelxtName)//
     1                       Cislo(:idel(Cislo))
          Radka=Radka(:idel(Radka))//'   '//t80(:idel(t80))
          FlnOld=Fln
          iFlnOld=ifln
          Fln=ShelxtSol(NShelxt)
          ifln=idel(Fln)
          call DeleteFile(Fln(:ifln)//'.hkl')
          call ReadSHELX(1,ich)
          if(isPowder) then
            call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
            call iom41(1,0,fln(:ifln)//'.m41')
          endif
          Fln=FlnOld
          iFln=iflnOld
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
        else
          NShelxt=NShelxt-1
          go to 1720
        endif
        if(NShelxt.eq.1) call FeQuestLblOn(nLblShelxt)
        nCrw=nCrwShelxtSolution+NShelxt-1
        call FeQuestCrwLabelChange(nCrw,Radka)
        call FeQuestCrwOpen(nCrw,NShelxt.eq.1)
        go to 1720
1750    call CloseIfOpened(ln)
        UseTabs=UseTabsIn
        NShelxtSel=1
        LastSolution=1
        go to 1500
      else if(NPrgm.ne.IdSuperflip) then
        call DeleteAllFiles(SirSol(:iSirSol)//'.*')
        if(isPowder) then
          DirSIRA=DirExpo(NPrgm)
          CallSIRA=CallExpo(NPrgm)
          if(NPrgm.eq.1) then
            Cislo='Expo'
          else
            Cislo='Expo'//ExpoName(NPrgm)(:idel(ExpoName(NPrgm)))
          endif
        else
          DirSIRA=DirSIR(NPrgm)
          CallSIRA=CallSIR(NPrgm)
          Cislo='SIR'//SirName(NPrgm)(:idel(SirName(NPrgm)))
        endif
        if(DirSIRA.eq.' '.or..not.ExistFile(DirSIRA(:idel(DirSIRA))//
     1     CallSIRA)) then
          call FeChybne(-1.,-1.,'the path to '//Cislo(:idel(Cislo))//
     1                  ' program is not correct.',
     2                  'Please re-define it.',SeriousError)
          call FeSetPrograms
          go to 1050
        endif
        if(isPowder) then
          flnSir=fln(:ifln)//'_expo_temp'
        else
          flnSir=fln(:ifln)//'_sir_temp'
        endif
        iflnSir=idel(flnSir)
        Radka=flnSir(:iflnSir)//'.res'
        if(ExistFile(Radka)) call DeleteFile(Radka)
        Ranover=.false.
        UzJdouAtomy=.false.
        ln=NextLogicNumber()
        CommandFile=CurrentDir(:idel(CurrentDir))//flnSir(:iflnSir)
        if(isPowder) then
          CommandFile=CommandFile(:idel(CommandFile))//'.exp'
        else
          CommandFile=CommandFile(:idel(CommandFile))//'.sir'
        endif
        call OpenFile(ln,CommandFile,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        Radka=Grupa(KPhase)
        if(NDimI(KPhase).eq.1) then
          i=index(Radka,':')
          if(i.gt.0) then
            Radka=Radka(i+1:)
            i=index(Radka,':')
            if(i.gt.0) Radka(i:)=' '
            go to 2100
          endif
          i=index(Radka,'(')
          if(i.gt.0) Radka(i:)=' '
        endif
2100    call mala(Radka)
        lns=NextLogicNumber()
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1         'spgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/spgroup.dat'
        endif
        call OpenFile(lns,Veta,'formatted','old')
        if(ErrFlag.ne.0) go to 9200
        read(lns,FormA) Veta
        if(Veta(1:1).ne.'#') rewind lns
        i=0
2200    i=i+1
        read(lns,FormSG,err=9100,end=9000) igi,igi,igi,GrupaI,GrupaH,
     1                                     GrupaS
        if(GrupaS.ne.' ') then
          if(EqIgCase(Radka,GrupaI).and.
     1       (Monoclinic(KPhase).eq.0.or.mod(i,3)+1.eq.
     2        Monoclinic(KPhase))) go to 2300
        endif
        go to 2200
2300    call CloseIfOpened(lns)
        idg=index(GrupaS,'#')
        if(idg.le.0) then
          idg=idel(GrupaS)
          call UnitMat (tr, 3)
          call UnitMatI(itr,3)
          go to 2350
        endif
        idg=idg-1
        call SetIntArrayTo(itr,9,0)
        call SetRealArrayTo(tr,9,0.)
        i=0
        j=idg+1
        im=idel(GrupaS)
2310    izn=1
        i=i+1
        if(i.gt.3) then
          if(j.eq.im) then
            go to 2350
          else
            go to 9000
          endif
        endif
2320    j=j+1
        if(j.gt.im) go to 9000
        k=index(abc,GrupaS(j:j))
        if(k.ge.1) then
          itr (i,k)=izn
          tr(i,k)=izn
          go to 2310
        else if(GrupaS(j:j).eq.'-') then
          izn=-1.
        else if(GrupaS(j:j).ne.',') then
          go to 9000
        endif
        go to 2320
2350    call SetRealArrayTo(ShiftSol,3,0.)
        if(CrlCentroSymm().le.0) go to 2380
        k=index(GrupaH,'#')
        if(k.gt.0) then
          Veta=GrupaH(k+1:)
          do j=1,idel(Veta)
            if(Veta(j:j).eq.',') Veta(j:j)=' '
          enddo
        else
          go to 2380
        endif
        k=0
        call StToReal(Veta,k,ShiftSol,3,.false.,ich)
2380    call RealVectorToOpposite(shsg(1,KPhase),xp,3)
        call AddVek(ShiftSol,shsg(1,KPhase),ShiftSol,3)
        Grupap=GrupaS(:idg)
        Radka='%structure '//flnSir(:iflnSir)
        write(ln,FormA) Radka(:idel(Radka))
        Radka='%init'
        write(ln,FormA) Radka(:idel(Radka))
        Radka='%job Solution of '//flnSir(:iflnSir)
        write(ln,FormA) Radka(:idel(Radka))
        Radka='%data'
        write(ln,FormA) Radka(:idel(Radka))
        call CopyVek(CellPar(1,1,KPhase),xp,6)
        call UnitMat(trp,NDim(KPhase))
        call DRMatTrCell(tr,xp,trp)
        write(Radka,'(''cell '',3f10.4,3f10.3)') xp
        call ZdrcniCisla(Radka,7)
        Radka='  '//Radka(:idel(Radka))
        write(ln,FormA) Radka(:idel(Radka))
        Radka='  space '//Grupap(:idel(Grupap))
        write(ln,FormA) Radka(:idel(Radka))
        Radka='  cont'
        do i=1,NAtFormula(KPhase)
          j=idel(Radka)
          write(Cislo,'(f8.2)') AtMult(i,KPhase)*float(NUnits(KPhase))
          Radka=Radka(:j)//' '//AtType(i,KPhase)//' '//Cislo
          call ZdrcniCisla(Radka(j+2:),2)
        enddo
        write(ln,FormA) Radka(:idel(Radka))
        if(Radiation(1).eq.NeutronRadiation) then
          write(ln,'(''  neutron'')')
        else if(Radiation(1).eq.ElectronRadiation) then
          write(ln,'(''  electrons'')')
        endif
        if(isPowder) then
          lnr=0
          write(Radka,'(''wave '',f10.6)') abs(LamA1(1))
          call ZdrcniCisla(Radka,2)
          Radka='  '//Radka(:idel(Radka))
          write(ln,FormA) Radka(:idel(Radka))
          Radka=flnSir(:iflnSir)//'.rfl'
          Radka='  ref2 '//Radka(:idel(Radka))
          write(ln,FormA) Radka(:idel(Radka))
          call PwdLeBail(1,.true.)
          call DeleteAllFiles(fln(:ifln)//'.l??')
          call iom50(0,0,fln(:ifln)//'.m50')
          call FeFlowChartRemove
          if(.not.ExistFile(fln(:ifln)//'.rfl').or.ErrFlag.ne.0)
     1      go to 9999
          call MoveFile(fln(:ifln)//'.rfl',flnSir(:iflnSir)//'.m91')
          call OpenFile(90,flnSir(:iflnSir)//'.m91','formatted','old')
          write(FormRfl(2:2),'(i1)') NDim(KPhase)
          lnr=NextLogicNumber()
          call OpenFile(lnr,flnSir(:iflnSir)//'.rfl','formatted',
     1                  'unknown')
2400      read(90,FormRfl,end=2450,err=2450)(ih(i),i=1,NDim(KPhase)),
     1                                      F,pom
          do i=4,NDim(KPhase)
            if(ih(i).ne.0) go to 2400
          enddo
          call Multmi(ih,itr,ihp,1,3,3)
          write(lnr,101)(ihp(i),i=1,3),pom,F
          go to 2400
2450      close(90,status='delete')
          close(lnr)
        else
          if(NDimI(KPhase).eq.1.and.
     1       (.not.isPowder.or.MMaxPwd(1,KPhase,KDatBlock).gt.0)) then
            Scitat=FeYesNo(-1.,-1.,'Do you want to combine main '//
     1                     'reflections and satellites?',1)
          endif
          write(ln,'(''  format (3i4,2f9.1)'')')
          write(ln,'(''  fosquare'')')
          call OpenDatBlockM90(90,KDatBlock,fln(:ifln)//'.m90')
          if(ErrFlag.ne.0) go to 9999
          Veta=flnSir(:iflnSir)//'.hkl'
          lnr=NextLogicNumber()
          call OpenFile(lnr,Veta,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          Radka='  refl '//Veta
          write(ln,FormA) Radka(:idel(Radka))
          n=0
          if(Scitat.and..not.allocated(iha)) then
            nalloc=10000
            allocate(iha(3,nalloc),Fa(nalloc),sFa(nalloc))
          endif
2500      read(90,Format91,end=2600,err=2600)(ih(i),i=1,NDim(KPhase)),
     1                                        F,sF,iq,nxx,itwr
          if(NTwin.gt.1) then
            call settw(ih,ihitw,itwr,ranover)
          else
            call CopyVekI(ih,ihitw,NDim(KPhase))
          endif
          if(ihitw(1,1).gt.900) go to 2500
          pom=F
          do i=2,NTwin
            if(ihitw(1,i).lt.900) F=F-sctw(i,KDatBlock)*pom
          enddo
          if(Scitat) then
            do i=4,NDim(KPhase)
              if(ih(i).ne.0) go to 2500
            enddo
            n=n+1
            if(n.gt.nalloc) then
              allocate(ihap(3,nalloc),Fap(nalloc),sFap(nalloc))
              call CopyVekI(iha,ihap,3*nalloc)
              call CopyVek ( Fa, Fap,  nalloc)
              call CopyVek (sFa,sFap,  nalloc)
              nallocp=nalloc
              nalloc=nalloc+10000
              deallocate(iha,Fa,sFa)
              allocate(iha(3,nalloc),Fa(nalloc),sFa(nalloc))
              call CopyVekI(ihap,iha,3*nallocp)
              call CopyVek ( Fap, Fa,  nallocp)
              call CopyVek (sFap,sFa,  nallocp)
              deallocate(ihap,Fap,sFap)
            endif
            call CopyVekI(ih,iha(1,n),3)
            Fa(n)=F
            sFa(n)=sF**2
            go to 2500
          else
            if(NDim(KPhase).gt.3) then
              do i=4,NDim(KPhase)
                if(ih(i).ne.0) go to 2500
              enddo
            endif
            call CopyVekI(ih(4),ihp(4),NDimI(KPhase))
            call Multmi(ih,itr,ihp,1,3,3)
            write(lnr,100)(ihp(i),i=1,3),F,sF
          endif
          go to 2500
2600      if(Scitat) then
            call CloseIfOpened(90)
            call OpenDatBlockM90(90,KDatBlock,fln(:ifln)//'.m90')
            if(ErrFlag.ne.0) go to 5000
2700        read(90,Format91,end=2800,err=2800)(ih(i),i=1,NDim(KPhase)),
     1                                         F,sF
            do i=4,NDim(KPhase)
              if(ih(i).ne.0) go to 2750
            enddo
            go to 2700
2750        do is=1,NSymmN(KPhase)
              call IndTr(ih,rm6(1,is,1,KPhase),ihp,NDim(KPhase))
              do i=1,n
                if(eqiv(ihp,iha(1,i),3)) then
                  Fa(i)=Fa(i)+max(F,0.)
                  sFa(i)=sFa(i)+sF**2
                  go to 2700
                endif
              enddo
            enddo
            go to 2700
2800        do j=1,n
              call Multmi(iha(1,j),itr,ih,1,3,3)
              write(lnr,100)(ih(i),i=1,3),Fa(j),sqrt(sFa(j))
            enddo
          endif
          call CloseIfOpened(90)
          call CloseIfOpened(lnr)
        endif
        if(NPrgm.ge.2.and..not.isPowder) then
          write(ln,'(''%phase'')')
          write(Cislo,'(f15.3)') Residual
          call ZdrcniCisla(Cislo,1)
          write(ln,'(''  residual '',a)') Cislo(:idel(Cislo))
        endif
        write(ln,'(''%continue'')')
        write(ln,'(''%end'')')
        close(ln)
        if(ManualEdit) call FeEdit(CommandFile)
        Veta=DirSIRA(:idel(DirSirA))//CallSIRA(:idel(CallSIRA))//' "'//
     1       CommandFile(:idel(CommandFile))//'"'
        if(isPowder) then
          if(NPrgm.eq.1) then
            Cislo='Expo'
          else
            Cislo='Expo'//ExpoName(NPrgm)(:idel(ExpoName(NPrgm)))
          endif
        else
          Cislo='SIR'//SirName(NPrgm)(:idel(SirName(NPrgm)))
        endif
        if(WineKey.le.0) then
          call FeSystemCommand(Veta,0)
          call FeWaitInfo('end of '//Cislo(:idel(Cislo))//'.')
        else
          call FeSystem(Veta)
        endif
        call FeFillTextInfo('runsolution2.txt',0)
        if(ExistFile(flnSir(:iflnSir)//'.xyz').or.
     1     ExistFile(flnSir(:iflnSir)//'.out')) then
          call CrlCleanOldCommands
          ln=NextLogicNumber()
          if(ExistFile(flnSir(:iflnSir)//'.xyz')) then
            Cislo='.xyz'
            TypeOutFile=OutFileXYZ
          else
            Cislo='.out'
            if(isPowder) then
              if(NPrgm.eq.IdExpo.or.NPrgm.eq.IdExpo2004) then
                TypeOutFile=OutFile2
              else
                TypeOutFile=OutFilePowder
              endif
            else
              TypeOutFile=OutFile1
            endif
          endif
          call OpenFile(ln,flnSir(:iflnSir)//Cislo,'formatted',
     1                  'unknown')
          call SetIntArrayTo(NAtType,NAtFormula(KPhase),0)
          if(MxAtAll.le.0) then
            call AllocateAtoms(1)
            nac=0
            nak=-1
          else
            nac=NAtIndFr(1,KPhase)-1
            nak=NAtIndTo(1,KPhase)
          endif
          sc(1,KDatBlock)=1.
          nn=0
2900      read(ln,FormA,end=3000) Veta
          if(Veta.eq.' ') go to 2900
          if(UzJdouAtomy) then
            k=0
            if(NAtAll.ge.MxAtAll) call ReallocateAtoms(1)
            if(nac.ge.nak.and.nak.ge.0) call AtSun(nac+1,NAtAll,nac+2)
            call kus(Veta,k,Cislo)
            if(TypeOutFile.ne.OutFileXYZ) call kus(Veta,k,Cislo)
            call UprAt(Cislo)
            if(EqIgCase(Cislo(1:1),'q')) go to 2900
            nac=nac+1
            nn=nn+1
            do i=1,NAtFormula(KPhase)
              do j=idel(Cislo),1,-1
                if(Index(Cifry(1:10),Cislo(j:j)).le.0) exit
              enddo
              if(EqIgCase(Cislo(:j),AtType(i,KPhase))) then
                call SetBasicKeysForAtom(nac)
                isf(nac)=i
                itf(nac)=1
                NAtType(i)=NAtType(i)+1
                write(Atom(nac),'(i4)') NAtType(i)
                Atom(nac)=Cislo(:idel(Cislo))//Atom(nac)(1:4)
                call Zhusti(Atom(nac))
                go to 2920
              endif
            enddo
            nac=nac-1
            if(nac.ge.nak.and.nak.gt.0)
     1        call AtSun(nac+2,NAtAll+1,nac+1)
            NAtIndLen(1,KPhase)=nac
            call EM40UpdateAtomLimits
            go to 3000
2920        if(TypeOutFile.gt.0) then
              call kus(Veta,k,Cislo)
              if(TypeOutFile.eq.OutFile1) call kus(Veta,k,Cislo)
            endif
            call StToReal(Veta,k,x(1,nac),3,.false.,ich)
            if(ich.ne.0) go to 2900
            if(TypeOutFile.eq.OutFileXYZ) then
              m=2
            else if(TypeOutFile.gt.0) then
              m=1
            else
              m=0
            endif
            do i=1,m
              call StToReal(Veta,k,ai(nac),1,.false.,ich)
              if(ich.ne.0) go to 2900
            enddo
            call SetRealArrayTo(beta(2,nac),5,0.)
            call StToReal(Veta,k,beta(1,nac),1,.false.,ich)
            if(nac.gt.1) then
              PrvniKiAtomu(nac)=PrvniKiAtomu(nac-1)+
     1                          DelkaKiAtomu(nac-1)
            else
              PrvniKiAtomu(nac)=ndoff+1
            endif
            DelkaKiAtomu(nac)=mxda
            call SetIntArrayTo(KiA(1,nac),DelkaKiAtomu(nac),0)
            NAtIndLen(1,KPhase)=nac
            if(TypeOutFile.lt.0) then
              call StToReal(Veta,k,ai(nac),1,.false.,ich)
              if(ich.ne.0) go to 2900
              xp=0.
              call SpecPos(x(1,nac),xp,1,1,.2,nocc)
              ai(nac)=ai(nac)/float(nocc)
            endif
            call EM40UpdateAtomLimits
          else
            k=0
            call kus(Veta,k,Cislo)
            if(TypeOutFile.eq.OutFileXYZ) then
              if(.not.EqIgCase(Cislo,'type')) go to 2900
            else if(TypeOutFile.gt.0) then
              if(.not.EqIgCase(Cislo,'serial')) go to 2900
              call Kus(Veta,k,Cislo)
              if(.not.EqIgCase(Cislo,'atom')) go to 2900
              call Kus(Veta,k,Cislo)
              if(.not.EqIgCase(Cislo,'label').and.
     1           .not.EqIgCase(Cislo(1:6),'height')) go to 2900
              read(ln,FormA,end=3000) Veta
            else if(TypeOutFile.lt.0) then
              if(.not.EqIgCase(Cislo,'final')) go to 2900
              call Kus(Veta,k,Cislo)
              if(.not.EqIgCase(Cislo,'model')) go to 2900
              read(ln,FormA,end=3000) Veta
            endif
            UzJdouAtomy=.true.
            if(NAtAll.le.0) call SetBasicM40(.true.)
          endif
          go to 2900
3000      call CloseIfOpened(ln)
          if(nn.le.0) go to 3200
        else
          if(ExistFile(flnSir(:iflnSir)//'.ins').or.
     1       ExistFile(flnSir(:iflnSir)//'.res')) then
            call CrlCleanOldCommands
            NPhaseOld=NPhase
            call ReadSHELX(1,ich)
            write(Veta,'(3f10.6)')(x(i,1),i=1,3)
            NPhase=NPhaseOld
            call iom40only(1,0,SirSol(:iSirSol)//'.m40')
          else
            go to 3200
          endif
        endif
        go to 3300
3200    call FeFillTextInfo('runsolution3.txt',0)
        if(FeYesNoHeader(-1.,-1.,'Do you want to repeat the process?',
     1                   0)) then
          go to 1550
        endif
3300    call DeleteAllFiles(flnSir(:iflnSir)//'*')
        if(UzJdouAtomy) then
          call iom40(1,0,SirSol(:iSirSol)//'.m40')
          if(nac.gt.0) LastSolution=LastSolution+1
        endif
        do i=1,NAtInd
          j=1
3400      if(index(Cifry(1:10),Atom(i)(j:j)).le.0) then
            j=j+1
            go to 3400
          endif
          Cislo=Atom(i)(:j-1)
          do j=1,NAtFormula(KPhase)
            if(EqIgCase(Cislo,AtType(j,KPhase))) then
              isf(i)=j
              exit
            endif
          enddo
          call multm(tr,x(1,i),xp,3,3,1)
          call AddVek(xp,ShiftSol,x(1,i),3)
        enddo
        call iom40Only(1,0,SirSol(:iSirSol)//'.m40')
        call CopyFile(fln(:ifln)//'.m50',SirSol(:iSirSol)//'.m50')
      else
        EDm40file=fln(:ifln)//'_tmp.m40'
        if(UseOldInflip.or.UseOldSolution) then
          call CopyFile(fln(:ifln)//'.inflip',fln(:ifln)//'.inflip_tmp')
          lnr=NextLogicNumber()
          call OpenFile(lnr,fln(:ifln)//'.inflip_tmp','formatted',
     1                  'unknown')
          ln=NextLogicNumber()
          call OpenFile(ln,fln(:ifln)//'.inflip','formatted','unknown')
3500      read(lnr,FormA,end=3600) Radka
          k=0
          call kus(Radka,k,t80)
          if(EqIgCase(t80,'numberofatoms')) then
            if(EDAtoms.gt.0) then
              write(Cislo,FormI15) EDAtoms
              call Zhusti(Cislo)
            else
              Cislo='composition'
            endif
            Radka='numberofatoms '//Cislo(:idel(Cislo))
          else if(EqIgCase(t80,'composition')) then
            t80=' '
            do i=1,NAtFormula(KPhase)
              write(Cislo,FormI15)
     1          nint(AtMult(i,KPhase)*float(NUnits(KPhase)))
              call Zhusti(Cislo)
              t80=t80(:idel(t80))//' '//
     1            AtType(i,KPhase)(:idel(AtType(i,KPhase)))//
     2            Cislo(:idel(Cislo))
            enddo
            Radka='composition '//t80(:idel(t80))
          endif
          write(ln,FormA) Radka(:idel(Radka))
          go to 3500
3600      close(lnr,status='delete')
          close(ln)
        else
          call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
          if(isPowder) then
            scp=sc(2,KDatBlock)
            call SetBasicM40(.true.)
            sc(2,KDatBlock)=scp
            call PwdLeBail(1,.true.)
            call DeleteAllFiles(fln(:ifln)//'.l??')
            call iom50(0,0,fln(:ifln)//'.m50')
            call FeFlowChartRemove
            if(.not.ExistFile(fln(:ifln)//'.rfl').or.ErrFlag.ne.0)
     1        go to 9999
          endif
          call MoveFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
          MEMTitle=fln(:ifln)
          PerformCode=PerformCF
          IncludeRefs=.true.
          RefsSourceCode=Refsm90
          IncludeMEM=.false.
          IncludeCF=.true.
          IncludeEDMA=.true.
          IncludePrior=.false.
          MEMCFDelta=1.
          MEMCFRelDelta=.false.
          AutoDelta=.true.
          MEMCFWeak=0.
          CFSymCode=AverageSym
          MEMAim=1.
          MEMLambda=0.
          MEMRate=1.
          MEMName=fln(:ifln)//'.inflip'
          MEMOutfile='"'//fln(:ifln)//'.m81" "'//fln(:ifln)//'.m80"'
          MEMInEDMA=fln(:ifln)//'.m81'
          MEMOutEDMA=fln(:ifln)
          EDmaxima=all
          EDtolerance=0.15
          EDposition=absol
          EDcharge=.true.
          EDwrm40=.true.
          if(NAtFormula(KPhase).gt.0) then
            minnel=100
            maxnel=0
            do i=1,NAtFormula(KPhase)
              minnel=min(minnel,nint(AtNum(i,KPhase)))
              maxnel=max(maxnel,nint(AtNum(i,KPhase)))
            enddo
            EDchlimlist=0.3*minnel/maxnel
          else
            EDchlimlist=0.03
          endif
          AbsChlim=.false.
          EDchlimint=0.25
          EDplim=0.3
          EDscale=fract
          EDprojection=.true.
          EDaddb=0.2
          EDfullcell=.false.
          do icv=1,NLattVec(KPhase)
            do is=1,NSymmN(KPhase)
              do i=1,NDim(KPhase)
                sh=vt6(i,icv,1,KPhase)+s6(i,is,1,KPhase)
                isymfac(i)=0
                mxsymfac(i)=0
4000            isymfac(i)=isymfac(i)+1
                dummy=sh*float(isymfac(i))
                if(abs(dummy-nint(dummy)).ge..00001) go to 4000
                if(isymfac(i).gt.mxsymfac(i)) mxsymfac(i)=isymfac(i)
              enddo
            enddo
          enddo
          do i=1,3
            ddiv=CellPar(i,1,KPhase)/.2
            mindif=ddiv+2.
            n2max=int(log(ddiv)/log(2.))+1
            n3max=int(log(ddiv)/log(3.))+1
            do j2=0,n2max
              do j3=0,n3max
                dummy=float(2**j2*3**j3*mxsymfac(i))
                if(abs(dummy-ddiv).lt.mindif) then
                  mindif=abs(dummy-ddiv)
                  n2b=j2
                  n3b=j3
                endif
              enddo
            enddo
            MEMDivision(i)=2**n2b*3**n3b*mxsymfac(i)
          enddo
          do i=4,NDim(KPhase)
            MEMDivision(i)=16
          enddo
          call WriteMEMFile
        endif
        if(ManualEdit) call FeEdit(fln(:ifln)//'.inflip')
        Veta='"'//HomeDir(:idel(HomeDir))//'Superflip'//ObrLom//
     1       'superflip.exe" "'//fln(:ifln)//'.inflip"'
        if(NPeakSearch.le.3.and.WineKey.le.0) then
          if(UseOldSolution) then
            Veta='"'//HomeDir(:idel(HomeDir))//'Superflip'//ObrLom//
     1           'edma.exe" "'//fln(:ifln)//'.inflip"'
          else
            Veta='cmd /c "'//Veta(:idel(Veta))//'&&"'//
     1           HomeDir(:idel(HomeDir))//'Superflip'//
     2           ObrLom//'edma.exe" "'//fln(:ifln)//'.inflip""'
          endif
        endif
        if(UseOldSolution) then
          if(NPeakSearch.gt.3) go to 4200
        else
          call DeleteFile(fln(:ifln)//'.m80')
          call DeleteFile(fln(:ifln)//'.m81')
        endif
        if(WineKey.le.0) then
          call FeSystemCommand(Veta,0)
          idp=NextQuestId()
          xqd=350.
          il=2
          call FeQuestCreate(idp,-1.,-1.,xqd,il,' ',0,LightGray,-1,-1)
          Veta='Superflip is running - use interrupt if necessary.'
          il=1
          call FeQuestLblMake(idp,xqd*.5,il,Veta,'C','B')
          nLbl=LblLastMade
          il=il+1
          Veta='Interrupt'
          dpom=FeTxLengthUnder(Veta)+10.
          call FeQuestButtonMake(idp,(xqd-dpom)*.5,il,dpom,ButYd,Veta)
          nButtInterrupt=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeReleaseOutput
          call FeDeferOutput
          StTime=FeGetSystemTime()
4100      call FeEvent(1)
          call FeMouseShape(0)
          if(EventType.eq.EventButton.and.
     1       EventNumber.eq.nButtInterrupt) then
            call FeQuestButtonOn(nButtInterrupt)
            call FeReleaseOutput
            call FeDeferOutput
            ln=NextLogicNumber()
            call OpenFile(ln,fln(:ifln)//'.sfcom','formatted','unknown')
            write(ln,'(''stop'')')
            close(ln)
            call FeWait(1.)
            call FeQuestButtonOff(nButtInterrupt)
            call FeReleaseOutput
            call FeDeferOutput
            EventType=0
            go to 4100
          endif
          if(.not.FeSystemCommandFinished()) then
            i=FeGetSystemTime()-StTime
            if(i.gt.100) then
              call FeReleaseOutput
              call FeUpdateDisplay(0,0,0,0)
              call FeDeferOutput
              StTime=FeGetSystemTime()
            endif
            go to 4100
          endif
          call FeQuestRemove(idp)
          call FeListView(fln(:ifln)//'.sflog',-1)
        else
          if(.not.UseOldSolution) call FeSystem(Veta)
          if(NPeakSearch.le.2) then
            Veta='"'//HomeDir(:idel(HomeDir))//'Superflip'//
     1           ObrLom//'edma.exe" "'//fln(:ifln)//'.inflip"'
            call FeSystem(Veta)
          endif
          call FeFillTextInfo('runsolution2.txt',0)
          call CrlCleanOldCommands
        endif
        if(ExistFile(SuperflipSol(:iSuperflipSol)//'.sflog')) then
          call FeQuestButtonOff(nButtListing)
        else
          call FeQuestButtonDisable(nButtListing)
        endif
        if(ExistFile(fln(:ifln)//'.m81')) then
          call FeQuestButtonOff(nButt3dMap)
        else
          call FeQuestButtonDisable(nButt3dMap)
        endif
        call DeleteFile(fln(:ifln)//'.sfcom')
4200    if(NPeakSearch.le.2) then
          call iom40Only(0,0,fln(:ifln)//'.m40')
          do i=1,NAtAll
            if(kswa(i).eq.KPhase) isf(i)=0
          enddo
          call EM40CleanAtoms
          lnr=NextLogicNumber()
          call OpenFile(lnr,EDm40file,'formatted','unknown')
          read(lnr,103,end=5000) NMax
          do i=1,4
            read(lnr,FormA) Radka
          enddo
          nn=0
          nac=NAtIndFr(1,KPhase)-1
          do i=1,NMax
            read(lnr,102,end=4230) At,isfp,itfp,aip,(xp(j),j=1,3),Rho,
     1                             Charge
            nac=nac+1
            if(NAtAll.ge.MxAtAll) call ReallocateAtoms(1000)
            call AtSun(nac,NAtAll,nac+1)
            NAtIndLen(1,KPhase)=nac-NAtIndFr(1,KPhase)+1
            call EM40UpdateAtomLimits
            call SetBasicKeysForAtom(nac)
            Atom(nac)=At
            isf(nac)=isfp
            itf(nac)=1
            x(1:3,nac)=xp(1:3)
            beta(1,nac)=3.
            xp=0.
            call SpecPos(x(1,nac),xp,0,1,.5,nocc)
            ai(nac)=1./float(nocc)
            if(nac.ne.1) then
              PrvniKiAtomu(nac)=PrvniKiAtomu(nac-1)+mxda
            else
              PrvniKiAtomu(nac)=ndoff+1
            endif
            DelkaKiAtomu(nac)=10
            call SetIntArrayTo(KiA(1,nac),DelkaKiAtomu(nac),0)
          enddo
4230      call CloseIfOpened(lnr)
          call iom40Only(1,0,fln(:ifln)//'.m40')
          call iom40Only(0,0,fln(:ifln)//'.m40')
        else if(NPeakSearch.eq.3) then
          call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
          if(ErrFlag.ne.0) go to 5000
          ln=NextLogicNumber()
          call OpenFile(ln,fln(:ifln)//'.z40','formatted','unknown')
          if(ErrFlag.ne.0) go to 5000
4250      read(m40,FormA,end=4300) Radka
          if((Radka(1:10).ne.'----------'.or.
     1        (LocateSubstring(Radka,'Fourier maxima',.false.,.true.)
     2           .le.0.and.
     3         LocateSubstring(Radka,'Fourier minima',.false.,.true.)
     4           .le.0)).and.Radka.ne.' ') then
            write(ln,FormA) Radka(:idel(Radka))
            go to 4250
          endif
4300      call CloseIfOpened(m40)
          lnr=NextLogicNumber()
          call OpenFile(lnr,EDm40file,'formatted','unknown')
          read(lnr,'(i5)',end=5000) NMax
          do i=1,4
            read(lnr,FormA) Radka
          enddo
          write(ln,109)
          write(ln,'(2i5)') 1,KPhase
          do i=1,NMax
            read(lnr,102,end=4400) At,isfp,itfp,aip,(xp(j),j=1,3),Rho,
     1                             Charge
            if(Charge.le.0.) Charge=Rho
            write(At,'(''max'',i3)') i
            call Zhusti(At)
            write(ln,102) At,isfp,itfp,aip,(xp(j),j=1,3),Rho,Charge
          enddo
4400      write(ln,'(72(''-''))')
          close(lnr,status='delete')
          call CloseIfOpened(ln)
          call MoveFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
          call iom40Only(0,0,fln(:ifln)//'.m40')
          call DeleteFile(fln(:ifln)//'.coo')
        else if(NPeakSearch.eq.4) then
          if(ExistFile(fln(:ifln)//'.m81')) then
            Uloha='Program for n-dimensional Fourier synthesis'
            call OpenFile(lst,fln(:ifln)//'_peaks.lst','formatted',
     1                    'unknown')
            call DefaultFourier
            npeaks(1)=3000
            npeaks(2)=0
            kharm=1
            YMinFlowChart=-1.
            call FouPeaks(0,0)
            close(lst,status='delete')
            LastSolution=LastSolution+1
          endif
        else if(NPeakSearch.eq.5) then
          call CopyFile(fln(:ifln)//'.l51',fln(:ifln)//'.z51')
          do i=1,NComp(KPhase)
            call DefaultFourier
            call FouRewriteCommands(1)
            call FouOpenCommands
            ScopeType=1
            NacetlInt(nCmdUseWeight)=1
            NacetlInt(nCmdmapa)=4
            NacetlInt(nCmdPPeaks)=500
            NacetlInt(nCmdNPeaks)=0
            NacetlInt(nCmdkharm)=1
            NacetlInt(nCmdnsubs)=i
            NacetlInt(nCmdIndUnit)=0
            call SetRealArrayTo(xrmn,NDim(KPhase),-333.)
            xrmn(4)=.0
            xrmx(4)=.9
            dd(4)=.1
            call FouRewriteCommands(1)
            SolutionCallFourier=.true.
            call Fourier
            SolutionCallFourier=.false.
            if(NComp(KPhase).gt.1) then
              write(Cislo,FormI15) i
              call Zhusti(Cislo)
              call CopyFile(fln(:ifln)//'.m40',
     1                      fln(:ifln)//'.m40_'//Cislo(:idel(Cislo)))
            endif
          enddo
          call CopyFile(fln(:ifln)//'.z51',fln(:ifln)//'.l51')
          call iom50(1,0,fln(:ifln)//'.m50')
        endif
        if(NPeakSearch.gt.2) then
          call iom40Only(0,0,fln(:ifln)//'.m40')
          call Peaks2Atoms(ich)
          if(ich.eq.0) LastSolution=LastSolution+1
        endif
        if(NComp(KPhase).gt.1) then
          do i=1,NComp(KPhase)
            write(Cislo,FormI15) i
            call Zhusti(Cislo)
            Veta=fln(:ifln)//'.m40_'//Cislo(:idel(Cislo))
            call DeleteFile(Veta)
          enddo
        endif
      endif
      if(NPrgm.ne.IdShelxt.and.NPrgm.ne.IdSuperflip) then
        flnOld=fln
        iflnOld=ifln
        fln=SirSol
        ifln=iSirSol
      endif
      call EM40MakeMotifsAuto
      if(NPrgm.eq.IdSuperflip.or.NPrgm.ne.IdShelxt) then
        fln=flnOld
        ifln=iflnOld
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40Only(0,0,fln(:ifln)//'.m40')
      endif
      go to 1500
5000  if(NPrgm.eq.IdShelxt) then
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      go to 1500
9000  call FeChybne(-1.,-1.,'SIR programs require the standard '//
     1              'setting of space group.',
     2              'For this case you can use only Superfip.',
     3              SeriousError)
      call CloseIfOpened(lns)
      go to 1050
9100  call FeReadError(lns)
      call CloseIfOpened(lns)
      go to 9999
9200  call FeGrQuit
      call DeletePomFiles
      call FeTmpFilesDelete
      stop
9999  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
      call CloseIfOpened(lnr)
      call CloseIfOpened(91)
      if(allocated(iha) ) deallocate(iha ,Fa ,sFa)
      if(allocated(ihap)) deallocate(ihap,Fap,sFap)
      if(isPowder.and.NPhase.gt.1) then
        KKleBail(KDatBlock)=KKleBailIn
        KPhaseSolve=0
      endif
      call FeTabsReset(UseTabsT)
      call FeTabsReset(UseTabsS)
      call DeleteAllFiles(ShelxtName(:iShelxtName)//'*')
      call DeleteAllFiles(SirSol(:iSirSol)//'*')
      call DeleteAllFiles(SuperflipSol(:iSuperflipSol)//'*')
      return
100   format(3i4,2f9.1)
101   format(3i4,2f15.4)
102   format(a8,2i3,4x,4f9.6/2f9.3)
103   format(i5)
104   format(a8,2i3,4x,4f9.6/f9.6)
109   format(26('-'),'   Fourier maxima   ',26('-'))
110   format(f15.5)
      end
