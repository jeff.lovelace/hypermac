      subroutine ReallocateCIFArrayUpdate(nCIFArrayUpdateNew)
      use Basic_mod
      character*128 CIFArrayUpdateP(:)
      allocatable CIFArrayUpdateP
      if(nCIFArrayUpdate.gt.nCIFArrayUpdateNew) go to 9999
      allocate(CIFArrayUpdateP(nCIFArrayUpdate))
      do i=1,nCIFArrayUpdate
        CIFArrayUpdateP(i)=CIFArrayUpdate(i)
      enddo
      deallocate(CIFArrayUpdate)
      allocate(CIFArrayUpdate(nCIFArrayUpdateNew))
      do i=1,nCIFArrayUpdate
        CIFArrayUpdate(i)=CIFArrayUpdateP(i)
      enddo
      deallocate(CIFArrayUpdateP)
      nCIFArrayUpdate=nCIFArrayUpdateNew
9999  return
      end
