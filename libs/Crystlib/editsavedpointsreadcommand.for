      subroutine EditSavedPointsReadCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EditSavedPointsC/ nEdwLabel,nEdwXYX,LabelString,xs
      dimension xs(3)
      character*8 LabelString
      character*(*) Command
      save /EditSavedPointsC/
      ich=0
      if(Command.eq.' ')go to 9999
      k=0
      call kus(Command,k,LabelString)
      call StToReal(Command,k,xs,3,.false.,ich)
      if(ich.ne.0) go to 8100
      go to 9999
8100  ich=1
      call FeChybne(-1.,-1.,Command,'Inacceptable numerical string.',
     1              SeriousError)
9999  return
      end
