      integer function CrlCentroSymm()
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      logical MatRealEqMinusUnitMat
      CrlCentroSymm=0
      do i=1,NSymm(KPhase)
        if(MatRealEqMinusUnitMat(rm6(1,i,1,KPhase),NDim(KPhase),.0001))
     1    then
          CrlCentroSymm=i
          exit
        endif
      enddo
      return
      end
