      subroutine ReallocateCIFArray(nCIFArrayNew)
      use Basic_mod
      character*128 CIFArrayP(:)
      allocatable CIFArrayP
      if(nCIFArray.gt.nCIFArrayNew) go to 9999
      allocate(CIFArrayP(nCIFArray))
      do i=1,nCIFArray
        CIFArrayP(i)=CIFArray(i)
      enddo
      deallocate(CIFArray)
      allocate(CIFArray(nCIFArrayNew))
      do i=1,nCIFArray
        CIFArray(i)=CIFArrayP(i)
      enddo
      deallocate(CIFArrayP)
      nCIFArray=nCIFArrayNew
9999  return
      end
