      subroutine OpenDatBlockM90(ln,KDatB,FileName)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*(*) FileName
      character*256 t256
      logical EqIgCase
      call OpenFile(ln,FileName,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      n=0
1500  read(ln,FormA,end=2000) t256
      k=0
      call kus(t256,k,Cislo)
      if(EqIgCase(Cislo,'data')) then
        n=n+1
        if(n.eq.KDatB) go to 9999
      endif
      go to 1500
2000  ErrFlag=1
9999  if(ErrFlag.ne.0) call CloseIfOpened(ln)
      return
      end
