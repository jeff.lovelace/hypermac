      subroutine TrMat2String(TrMat,n,String)
      include 'fepc.cmn'
      dimension TrMat(n,n)
      character*(*) String
      String='('
      do i=1,3
        do j=1,3
          call ToFract(TrMat(j,i),Cislo,6)
          String=String(:idel(String))//Cislo(:idel(Cislo))
          if(j.ne.3) String=String(:idel(String))//','
        enddo
        if(i.eq.3) then
          String=String(:idel(String))//')'
        else
          String=String(:idel(String))//'|'
        endif
      enddo
      return
      end
