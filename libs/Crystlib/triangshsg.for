      subroutine TriangShSg(a,b,n,m)
      include 'fepc.cmn'
      dimension a(n,m),b(n)
      jp=1
      do 2000i=1,m
        aopt=999999.
        jopt=0
        do j=jp,n
          absaji=abs(a(j,i))
          if(absaji.gt.0.0001) then
            if(absaji.lt.aopt) then
              jopt=j
              aopt=absaji
            endif
          else
            a(j,i)=0.
          endif
        enddo
        if(jopt.le.0) then
          go to 2000
        else if(jopt.ne.jp) then
          do k=i,m
            p=a(jopt,k)
            a(jopt,k)=a(jp,k)
            a(jp,k)=p
          enddo
          p=b(jopt)
          b(jopt)=b(jp)
          b(jp)=p
        endif
        p1=a(jp,i)
        do l=jp+1,n
          p=a(l,i)
          a(l,i)=0.
          pp=CommMult(p1,p)
          if(p.ne.0.) then
            do k=i+1,m
              a(l,k)=(p1*a(l,k)-a(jp,k)*p)/pp
            enddo
            b(l)=(p1*b(l)-b(jp)*p)/pp
          endif
        enddo
        jp=jp+1
        if(jp.gt.m) go to 2500
2000  continue
2500  do 5000i=1,n
        do j=1,m
          if(abs(a(i,j)).gt..0001) then
            if(a(i,j).lt.0.) then
              do k=j,m
                a(i,k)=-a(i,k)
              enddo
              b(i)=-b(i)
            endif
            go to 5000
          endif
        enddo
5000  continue
9999  return
      end
