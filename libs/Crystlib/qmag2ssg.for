      subroutine QMag2SSG(FileName,Klic)
      use Basic_mod
      use EditM40_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      logical EqRV,ExistFile
      character*(*) FileName
      character*60 GrupaO,Veta
      character*3  t3
      character*2  t2m
      real rmp(36),xr(9),xq1(3),xq(3)
      real, allocatable :: rm6o(:,:),rmo(:,:),s6o(:,:)
      integer io(3),ioa(3,3),ia(3),IGenP(5),CrlCentroSymm,IRac(3),
     1        Opakovat
      integer, allocatable :: AddGen(:),PerTab(:,:),IswSymmO(:,:)
      logical, allocatable :: AddSymm(:),BratSymmO(:)
      save rm6o,rmo,s6o,AddSymm,AddGen,PerTab,NSymmS,GrupaO,BratSymmO,
     1     IswSymmO,NGrupaO
      BratSymm(1:NSymm(KPhase),KPhase)=.true.
      call ReallocSymm(NQMag(KPhase)+3,NSymm(KPhase),NLattVec(KPhase),
     1                 NComp(KPhase),NPhase,0)
      idl=idel(FileName)
      if(ExistFile(FileName(:idl)//'.m40')) then
        call CopyFile(FileName(:idl)//'.m40',FileName(:idl)//'.m44')
        call iom40Only(0,0,FileName(:idl)//'.m44')
      endif
      if(NQMag(KPhase).le.0) then
        go to 9999
      else
        if(NDimI(KPhase).gt.0) then
          if(VasekTest.ne.0)
     1      call FeWinMessage('Chce prepinat do SSG, ale je jiz '//
     2                        'prepnuto',' ')
          go to 9999
        endif
      endif
      if(ExistFile(FileName(:idl)//'.m40')) then
        call CopyFile(FileName(:idl)//'.m40',FileName(:idl)//'.m44')
        call iom40Only(0,0,FileName(:idl)//'.m44')
      endif
      do i=1,NLattVec(KPhase)
        vt6(4:NQMag(KPhase)+3,i,1,KPhase)=0.
      enddo
      BratSymm(1:NSymm(KPhase),KPhase)=.true.
      call ReallocSymm(NQMag(KPhase)+3,NSymm(KPhase),NLattVec(KPhase),
     1                 NComp(KPhase),NPhase,0)
      if(NQMag(KPhase).eq.1) then
        idl=idel(FileName)
        QMagIrr(1:3,1,KPhase)=QMag(1:3,1,KPhase)
        Opakovat=0
1100    NeBrat=0
        IRac=0
        IRacS=0
        ia=0
        ioa=0
        do i=1,NSymm(KPhase)
          call CopyMat(rm6(1,i,1,KPhase),rmp,3)
          call MultM(QMagIrr(1,1,KPhase),rmp,xr,1,3,3)
          rm6(1:16,i,1,KPhase)=0.
          call MatFromBlock3(rmp,rm6(1,i,1,KPhase),4)
          izp=0
          xqm=9999.
          do 2100iz=-1,1,2
            pom=0.
            do j=1,3
              xq1(j)=xr(j)-float(iz)*QMagIrr(j,1,KPhase)
              if(abs(anint(xq1(j))-xq1(j)).gt..001) go to 2100
              pom=pom+abs(xq1(j))
            enddo
            do ivt=1,NLattVec(KPhase)
              poms=scalmul(xq1,vt6(1,ivt,1,KPhase))
!              if(abs(anint(poms)-poms).le..001) go to 2000
              if(abs(anint(poms)-poms).gt..001) go to 2100
            enddo
!            go to 2100
2000        if(iz.lt.0) then
              xqm=pom
              call CopyVek(xq1,xq,3)
              izp=iz
            else
              if(pom.lt.xqm) then
                call CopyVek(xq1,xq,3)
                izp=iz
              endif
            endif
2100      continue
          if(izp.eq.0) then
            NeBrat=NeBrat+1
            BratSymm(i,KPhase)=.false.
            rm6(16,i,1,KPhase)=1.
            do j=1,3
              rm6(j*4,i,1,KPhase)=0.
            enddo
          else
2150        rm6(16,i,1,KPhase)=izp
            do j=1,3
              rm6(j*4,i,1,KPhase)=xq(j)
            enddo
            BratSymm(i,KPhase)=.true.
            call SmbOp(rm(1,i,1,KPhase),s6(1,i,1,KPhase),1,1.,t3,t2m,io,
     1                 n,pom)
            if(n.gt.2.and.n.le.6) then
              n=n-2
              if(n.eq.4) n=n-1
              if(ia(n).gt.0) then
                if(ioa(1,n)+ioa(2,n)+ioa(3,n).lt.io(1)+io(2)+io(3))
     1            ioa(1:3,n)=io(1:3)
              else
                ia(n)=i
                ioa(1:3,n)=io(1:3)
              endif
            endif
          endif
          do j=1,3
            if(abs(xq(j)).gt..001) then
              IRacS=IRacS+1
              IRac(j)=1
            endif
          enddo
        enddo
        if(Opakovat.eq.1) go to 2200
        Opakovat=0
        do i=3,1,-1
          if(ia(i).eq.0) cycle
          do j=1,3
            QMagIrr(j,1,KPhase)=QMagIrr(j,1,KPhase)+
     1                          1./sqrt(3.)*float(ioa(j,i))
          enddo
          Opakovat=1
          go to 2190
        enddo
        if(IRacS.eq.0) then
          do i=1,3
            if(abs(QMagIrr(i,1,KPhase)).gt..001) then
              QMagIrr(i,1,KPhase)=QMagIrr(i,1,KPhase)/sqrt(3.)
              Opakovat=1
            endif
          enddo
        else
          do i=1,3
            if(IRac(i).eq.0.) then
              QMagIrr(i,1,KPhase)=QMagIrr(i,1,KPhase)/sqrt(3.)
              Opakovat=1
            endif
          enddo
        endif
2190    if(Opakovat.eq.1) then
          do i=1,NSymm(KPhase)
            call CopyMat(rm(1,i,1,KPhase),rm6(1,i,1,KPhase),3)
          enddo
          go to 1100
        endif
      else if(NQMag(KPhase).gt.1) then
        n=NQMag(KPhase)+3
        do i=1,NSymm(KPhase)
          call CopyMat(rm6(1,i,1,KPhase),rmp,3)
          rm6(1:n**2,i,1,KPhase)=0.
          call MatFromBlock3(rmp,rm6(1,i,1,KPhase),n)
        enddo
      endif
2200  NDim(KPhase)=NQMag(KPhase)+3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      MaxNDim=NDim(KPhase)
      MaxNDimI=MaxNDim-3
      call CopyVek(QMag,qu (1,1,1,KPhase),9)
      call CopyVek(QMag,qui(1,1,KPhase),9)
      call CopyVek(QMag,QuRefBlock(1:3,1,0),9)
      call SetRealArrayTo(quir(1,1,KPhase),3,0.)
      call UnitMat(ZV (1,1,KPhase),NDim(KPhase))
      call UnitMat(ZVi(1,1,KPhase),NDim(KPhase))
      if(.not.allocated(qcnt)) then
        n=NAtInd
        allocate(qcnt(3,n),phf(n),sphf(n),OrthoX40(n),OrthoDelta(n),
     1           OrthoEps(n))
        do i=1,n
          call qbyx(x(1,i),qcnt(1,i),iswa(i))
          phf(i)=0.
          sphf(i)=0.
          OrthoX40(i)=0.
          OrthoDelta(i)=0.
          OrthoEps(i)=0.
        enddo
        if(NDimI(KPhase).gt.0) then
          call SetIntArrayTo(kw(1,1,KPhase),3*mxw,0)
          if(NDimI(KPhase).eq.1) then
            j=mxw
          else
            j=NDimI(KPhase)
          endif
          do i=1,j
            kw(mod(i-1,NDimI(KPhase))+1,i,KPhase)=
     1                                       (i-1)/NDimI(KPhase)+1
          enddo
        endif
      endif
      n=NSymm(KPhase)
      invc=CrlCentroSymm()
      if(allocated(rm6o)) deallocate(rm6o,rmo,s6o,AddSymm,AddGen,
     1                               PerTab,BratSymmO,IswSymmO)
      allocate(rm6o(36,n),rmo(9,n),s6o(6,n),AddSymm(n),AddGen(n),
     1         PerTab(n,n),BratSymmO(n),IswSymmO(n,NComp(KPhase)))
      NSymmS=n
      ns=0
      do i=1,NSymmS
        s6(4,i,1,KPhase)=0.
        call CopyMat(rm6(1,i,1,KPhase),rm6o(1,i),NDim(KPhase))
        call CopyMat(rm (1,i,1,KPhase),rmo (1,i),3)
        call CopyVek(s6 (1,i,1,KPhase),s6o (1,i),NDim(KPhase))
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        BratSymmO(i)=BratSymm(i,KPhase)
        if(BratSymmO(i)) ns=ns+1
        do j=1,NComp(KPhase)
          IswSymmO(i,j)=IswSymm(i,j,KPhase)
        enddo
        do j=1,NSymmS
          call MultM(rmo(1,i),rm(1,j,1,KPhase),xr,3,3,3)
          do k=1,NSymmS
            if(EqRV(xr,rm(1,k,1,KPhase),9,.01)) then
              PerTab(i,j)=k
              exit
            endif
          enddo
        enddo
      enddo
      GrupaO=Grupa(KPhase)
      NGrupaO=NGrupa(KPhase)
      if(NQMag(KPhase).eq.1) then
        if(Klic.gt.0) then
          call CrlCommenCheck
          go to 9999
        endif
        NSymmOrg=NSymm(KPhase)
2300    ns=0
        do i=1,NSymmOrg
          if(BratSymmO(i)) then
            ns=ns+1
            call CopyMat(rm6o(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
            call CopyMat(rmo (1,i),rm (1,ns,1,KPhase),3)
            call CopyVek(s6o (1,i),s6 (1,ns,1,KPhase),NDim(KPhase))
          endif
        enddo
        do i=1,NSymmS
          BratSymmO(i)=.false.
          do j=1,NSymm(KPhase)
            if(EqRV(rmo(1,i),rm(1,j,1,KPhase),9,.01)) then
              BratSymmO(i)=.true.
              exit
            endif
          enddo
        enddo
        NSymm (KPhase)=ns
        NSymmN(KPhase)=ns
        call SetLogicalArrayTo(AddSymm,NSymmS,.false.)
        call SetIntArrayTo(AddGen,NSymmS,0)
        indc=0
        do i=1,NSymmS
          if(BratSymmO(i).or.AddSymm(i)) cycle
          indc=indc+1
          AddGen(indc)=i
          do j=1,NSymmS
            if(.not.BratSymmO(j)) cycle
            k=PerTab(i,j)
            AddSymm(k)=.true.
          enddo
        enddo
        NTwin=1
        if(NQMag(KPhase).gt.0) then
          MaxUsedKw(KPhase)=1
        else
          MaxUsedKw(KPhase)=0
        endif
        if(indc.gt.0) then
          ntrans=indc
          if(allocated(TransMat)) call DeallocateTrans
          call AllocateTrans
          do i=1,indc
            j=AddGen(i)
            call CopyMat(rm6o(1,j),TransMat(1,i,1),NDim(KPhase))
            call CopyVek( s6o(1,j),TransVec(1,i,1),NDim(KPhase))
            if(MagneticType(KPhase).gt.0) TransZM(i)=1.
            NTwin=NTwin+1
            call MatBlock3(rm6o(1,j),rtw(1,NTwin),NDim(KPhase))
            call MatInv(rtw(1,NTwin),rtwi(1,NTwin),det,3)
          enddo
        else
          ntrans=0
        endif
        NSymmN(KPhase)=NSymm(KPhase)
        call EM40SetTr(0)
        call EM40ExpandAll(ich)
        ntrans=0
        if(allocated(IAtActive))
     1    deallocate(IAtActive,LAtActive,NameAtActive)
        n=NAtInd+NAtMol
        allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
        NAtActive=0
        do i=1,NAtInd
          if(kswa(i).eq.KPhase) then
            NAtActive=NAtActive+1
            LAtActive(NAtActive)=.true.
            IAtActive(NAtActive)=i
          endif
        enddo
        call EM40MergeAtomsRun(.1,ich)
        if(.not.isPowder) then
          pom=1./float(NTwin)
          j=mxscu
3300      if(sc(j,1).le.0.) then
            j=j-1
            if(j.gt.1) go to 3300
          endif
          mxscutw=max(j+NTwin-1,6)
          mxscu=mxscutw-NTwin+1
          do i=1,NTwin
            sctw(i,KDatBlock)=pom
          enddo
        endif
      else if(NQMag(KPhase).gt.1) then
        NSymm (KPhase)=2
        NSymmN(KPhase)=2
        do i=1,3
          s6(1:NDim(KPhase),i,1,KPhase)=0.
          call UnitMat(rm6(1,i,1,KPhase),NDim(KPhase))
        enddo
        rm6(1:NDimQ(KPhase),2,1,KPhase)=-rm6(1:NDimQ(KPhase),2,1,KPhase)
      endif
      call FindSmbSg(Grupa(KPhase),ChangeOrderNo,1)
      call iom40Only(1,0,FileName(:idl)//'.m44')
      go to 9999
      entry SSG2QMag(FileName)
      if(NQMag(KPhase).le.0) then
        go to 9999
      else
        if(NDimI(KPhase).le.0) then
          if(VasekTest.ne.0)
     1      call FeWinMessage('Chce prepinat do QMag, ale je jiz '//
     2                        'prepnuto',' ')
          go to 9999
        endif
      endif
      idl=idel(FileName)
      if(ExistFile(FileName(:idl)//'.m44'))
     1   call iom40Only(0,0,FileName(:idl)//'.m44')
      NDim(KPhase)=3
      NDimI(KPhase)=NDim(KPhase)-3
      NDimQ(KPhase)=NDim(KPhase)**2
      MaxNDim=NDim(KPhase)
      MaxNDimI=MaxNDim-3
      call UnitMat(ZV (1,1,KPhase),NDim(KPhase))
      call UnitMat(ZVi(1,1,KPhase),NDim(KPhase))
      do i=1,NSymmS
        call CopyMat(rmo (1,i),rm6(1,i,1,KPhase),3)
        call CopyMat(rmo (1,i),rm (1,i,1,KPhase),3)
        call CopyVek(s6o (1,i),s6 (1,i,1,KPhase),3)
        call CodeSymm(rm6(1,i,1,KPhase),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        do j=1,NComp(KPhase)
          IswSymm(i,j,KPhase)=IswSymmO(i,j)
        enddo
      enddo
      NSymm(KPhase)=NSymmS
      NSymmN(KPhase)=NSymmS
      NTwin=1
      mxscu=6
      mxscutw=6
      Grupa(KPhase)=GrupaO
      NGrupa(KPhase)=NGrupaO
      deallocate(rm6o,rmo,s6o,AddSymm,AddGen,PerTab,BratSymmO,IswSymmO)
9999  write(format80(2:2),'(i1)') maxNDim
      format83 (2:2)=format80(2:2)
      format83p(2:2)=format80(2:2)
      format83e(2:2)=format80(2:2)
      format91 (2:2)=format80(2:2)
      format95 (5:5)=format80(2:2)
      PrfFormat(2:2)=format80(2:2)
      return
      end
