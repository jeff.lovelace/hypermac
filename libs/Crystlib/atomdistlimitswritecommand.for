      subroutine AtomDistLimitsWriteCommand(Command,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/AtomDist/ nRolMenuFirst,nEdwDMin,nEdwDMax,DMin,DMax,
     1                 AtNames(2)
      character*(*) Command
      character*2   AtNames
      integer RolMenuSelectedQuest
      save /AtomDist/
      ich=0
      Command='distlim'
      nRolMenu=nRolMenuFirst
      do i=1,2
        AtNames(i)=AtType(RolMenuSelectedQuest(nRolMenu),KPhase)
        call Uprat(AtNames(i))
        Command=Command(:idel(Command)+1)//AtNames(i)(:idel(AtNames(i)))
        nRolMenu=nRolMenu+1
      enddo
      call FeQuestRealFromEdw(nEdwDMin,DMin)
      write(Cislo,100) DMin
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
      call FeQuestRealFromEdw(nEdwDMax,DMax)
      write(Cislo,100) DMax
      call ZdrcniCisla(Cislo,1)
      Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
9999  return
100   format(f15.4)
      end
