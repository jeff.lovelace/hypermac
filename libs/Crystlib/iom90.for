      subroutine iom90(klic,FileName)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      dimension xp(1)
      character*12 :: Nazev,id(80) =
     1   (/'lambda      ','radtype     ','lpfactor    ','nalpha      ',
     2     'monangle    ','kalpha1     ','kalpha2     ','kalphar     ',
     3     'datcolltemp ','dattype     ','datblock    ','end         ',
     4     'nref        ','flimprint   ','flimcull    ','indslowest  ',
     5     'indfastest  ','dataave     ','addcentrsymm','sigimethod  ',
     6     'multave     ','obslim      ','scalelim    ','diffscales  ',
     7     'scnoscheme  ','norefitems  ','perfmono    ','toftth      ',
     8     'tofjason    ','tofdifc     ','tofdifa     ','tofzero     ',
     9     'tofct       ','tofat       ','tofzerot    ','tofce       ',
     a     'tofzeroe    ','tofwcross   ','toftcross   ','tofsig2     ',
     1     'tofsig1     ','tofsig0     ','tofgam2     ','tofgam1     ',
     2     'tofgam0     ','tofalfa0    ','tofalfa1    ','tofbeta0    ',
     3     'tofbeta1    ','tofalfa0t   ','tofalfa1t   ','tofbeta0t   ',
     4     'tofbeta1t   ','tofalfa0e   ','tofalfa1e   ','tofbeta0e   ',
     5     'tofbeta1e   ','edoffset    ','edslope     ','edtth       ',
     6     'pwdmethod   ','hklf5       ','useincspect ','eformat91   ',
     7     'alphagmono  ','betagmono   ','avesigwt    ','xddircos    ',
     8     'magfield    ','magpol      ','magpolplus  ','magpolminus ',
     9     'sctom90     ','unstab      ','#c75        ','#c76        ',
     a     '#c77        ','#c78        ','#c79        ','#c80        '/)
      character*(*) FileName
      character*256 Veta
      real LamAveP,LamA1P,LamA2P
      equivalence (IdNumbers( 1),IdLambda),
     2            (IdNumbers( 2),IdRadType),
     3            (IdNumbers( 3),IdLpFactor),
     4            (IdNumbers( 4),IdNAlpha),
     5            (IdNumbers( 5),IdMonAngle),
     6            (IdNumbers( 6),IdKalpha1),
     7            (IdNumbers( 7),IdKalpha2),
     8            (IdNumbers( 8),IdKalphar),
     9            (IdNumbers( 9),IdDatCollTemp),
     a            (IdNumbers(10),IdDataType),
     1            (IdNumbers(11),IdDatBlock),
     2            (IdNumbers(12),IdEnd),
     3            (IdNumbers(13),IdNRef90),
     4            (IdNumbers(14),IdFLimPrint),
     5            (IdNumbers(15),IdFLimCull),
     6            (IdNumbers(16),IdIndSlowest),
     7            (IdNumbers(17),IdIndFastest),
     8            (IdNumbers(18),IdDataAve),
     9            (IdNumbers(19),IdAddCentrSymm),
     a            (IdNumbers(20),IdSigIMethod),
     1            (IdNumbers(21),IdMultAve),
     2            (IdNumbers(22),IdEM9ObsLim),
     3            (IdNumbers(23),IdEM9ScaleLim),
     4            (IdNumbers(24),IdDiffScales),
     5            (IdNumbers(25),IdScNoScheme),
     6            (IdNumbers(26),IdNoRefItems),
     7            (IdNumbers(27),IdPerfMono),
     8            (IdNumbers(28),IdTOFTTh),
     9            (IdNumbers(29),IdTOFJason),
     a            (IdNumbers(30),IdTOFDIFC),
     1            (IdNumbers(31),IdTOFDIFA),
     2            (IdNumbers(32),IdTOFZero),
     3            (IdNumbers(33),IdTOFCt),
     4            (IdNumbers(34),IdTOFAt),
     5            (IdNumbers(35),IdTOFZeroT),
     6            (IdNumbers(36),IdTOFCe),
     7            (IdNumbers(37),IdTOFZeroe),
     8            (IdNumbers(38),IdTOFWCross),
     9            (IdNumbers(39),IdTOFTCross),
     a            (IdNumbers(40),IdTOFSig2),
     1            (IdNumbers(41),IdTOFSig1),
     2            (IdNumbers(42),IdTOFSig0),
     3            (IdNumbers(43),IdTOFGam2),
     4            (IdNumbers(44),IdTOFGam1),
     5            (IdNumbers(45),IdTOFGam0),
     6            (IdNumbers(46),IdTOFAlpha0),
     7            (IdNumbers(47),IdTOFAlpha1),
     8            (IdNumbers(48),IdTOFBeta0),
     9            (IdNumbers(49),IdTOFBeta1),
     a            (IdNumbers(50),IdTOFAlpha0t),
     1            (IdNumbers(51),IdTOFAlpha1t),
     2            (IdNumbers(52),IdTOFBeta0t),
     3            (IdNumbers(53),IdTOFBeta1t),
     4            (IdNumbers(54),IdTOFAlpha0e),
     5            (IdNumbers(55),IdTOFAlpha1e),
     6            (IdNumbers(56),IdTOFBeta0e),
     7            (IdNumbers(57),IdTOFBeta1e),
     8            (IdNumbers(58),IdEDOffset),
     9            (IdNumbers(59),IdEDSlope),
     a            (IdNumbers(60),IdEDTTh),
     1            (IdNumbers(61),IdPwdMethod),
     2            (IdNumbers(62),IdHKLF5),
     3            (IdNumbers(63),IdUseIncSpect),
     4            (IdNumbers(64),IdUseEFormat91),
     5            (IdNumbers(65),IdAlphaGMono),
     6            (IdNumbers(66),IdBetaGMono),
     7            (IdNumbers(67),IdAveSigWt),
     8            (IdNumbers(68),IdXDDirCos),
     9            (IdNumbers(69),IdMagField),
     a            (IdNumbers(70),IdMagPolFlag),
     1            (IdNumbers(71),IdMagPolPlus),
     2            (IdNumbers(72),IdMagPolMinus),
     3            (IdNumbers(73),IdScToM90),
     4            (IdNumbers(74),IdEM9Instab)
      equivalence (xp,Value)
      KDatBlockIn=KDatBlock
      ExistNeutronData=.false.
      ExistXRayData=.false.
      ExistElectronData=.false.
      ln=NextLogicNumber()
      if(Klic.eq.0) then
        ExistPowder=.false.
        ExistSingle=.false.
        UseEFormat91=.false.
        Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
        NDatBlock=0
        if(.not.ExistM90) go to 9999
        call OpenFile(ln,FileName,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        k=256
1000    if(k.eq.256) then
          read(ln,FormA,end=8000) Veta
          k=0
        endif
        call kus(Veta,k,Nazev)
        j=islovo(Nazev,id,74)
        if(j.eq.IdUseEFormat91) then
          call StToReal(Veta,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 1000
          IValue=nint(Value)
          UseEFormat91=IValue.gt.0
          if(UseEFormat91) then
            Format91='(3i4,2e15.6,3i4,8f8.4, e15.6,i15)'
          else
            Format91='(3i4,2f9.1 ,3i4,8f8.4, e15.6,i15)'
          endif
          go to 1000
        endif
        if(j.eq.IdDatBlock.or.j.eq.IdEnd) then
          if(NDatBlock.gt.0) then
            if(iabs(DataType(NDatBlock)).ne.2) then
              LamAve(NDatBlock)=LamAveP
              LamA1(NDatBlock)=LamA1P
              LamA2(NDatBlock)=LamA2P
            else if(DataType(NDatBlock).eq.2) then
              if(KLam(NDatBlock).eq.0) then
                LamAve(NDatBlock)=LamAveP
                LamA1(NDatBlock)=LamA1P
                LamA2(NDatBlock)=LamA2P
              else
                LamAve(NDatBlock)=LamAveD(KLam(NDatBlock))
                LamA1(NDatBlock)=LamA1D(KLam(NDatBlock))
                LamA2(NDatBlock)=LamA2D(KLam(NDatBlock))
              endif
              if(KRefLam(NDatBlock).eq.0) then
                if(KLam(NDatBlock).eq.0) then
                  LamAvePwd(NDatBlock)=LamAveP
                  LamPwd(1,NDatBlock)=LamA1P
                  LamPwd(2,NDatBlock)=LamA2P
                else
                  LamAvePwd(NDatBlock)=LamAveD(KLam(NDatBlock))
                  LamPwd(1,NDatBlock)=LamA1D(KLam(NDatBlock))
                  LamPwd(2,NDatBlock)=LamA2D(KLam(NDatBlock))
                endif
              endif
              LamRatPwd(NDatBlock)=LamRat(NDatBlock)
              KDatBlock=NDatBlock
              if(KUseLamFile(NDatBlock).eq.1)
     1          call PwdSetLamProfile(NDatBlock)
              LorentzToCrySize(NDatBlock)=
     1          10.*LamAvePwd(NDatBlock)/ToRad
              GaussToCrySize(NDatBlock)=
     1          sqrt8ln2*LorentzToCrySize(NDatBlock)
            endif
          endif
          if(j.eq.IdDatBlock) then
            NDatBlock=NDatBlock+1
            k=256
            call SetBasicM90(NDatBlock)
            LamAveP=-1.
            LamA1P=-1.
            LamA2P=-1.
            go to 1000
          else
            go to 8000
          endif
        else if(j.eq.IdMagField) then
          call StToReal(Veta,k,MagFieldDir(1,NRefBlock),3,.false.,ich)
          go to 1000
        endif
        if(j.eq.0.or.k.ge.256) go to 1000
        call StToReal(Veta,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 1000
        IValue=nint(Value)
        if(j.eq.IdLambda) then
          LamAveP=Value
          KLam(NDatBlock)=LocateInArray(LamAveP,LamAveD,7,.0001)
          LamA1P=Value
          LamA2P=Value
          NAlfa(NDatBlock)=1
          LamRat(NDatBlock)=0.
          LpFactor(NDatBlock)=1
          AngleMon(NDatBlock)=0.
          AlphaGMon(NDatBlock)=0.
          BetaGMon(NDatBlock)=0.
        else if(j.eq.IdRadType) then
          Radiation(NDatBlock)=IValue
          if(IValue.eq.NeutronRadiation) then
            ExistNeutronData=.true.
          else if(Ivalue.eq.XRayRadiation) then
            ExistXRayData=.true.
          else if(Ivalue.eq.ElectronRadiation) then
            ExistElectronData=.true.
          endif
        else if(j.eq.IdLpFactor) then
          LpFactor(NDatBlock)=IValue
        else if(j.eq.IdNAlpha) then
          NAlfa(NDatBlock)=IValue
        else if(j.eq.IdDataType) then
          DataType(NDatBlock)=IValue
          if(abs(IValue).eq.1) then
            ExistSingle=.true.
          else
            ExistPowder=.true.
          endif
        else if(j.eq.IdMonAngle) then
          AngleMon(NDatBlock)=Value
        else if(j.eq.IdAlphaGMono) then
          AlphaGMon(NDatBlock)=Value
        else if(j.eq.IdBetaGMono) then
          BetaGMon(NDatBlock)=Value
        else if(j.eq.IdKalpha1) then
          LamA1P=Value
        else if(j.eq.IdKalpha2) then
          LamA2P=Value
        else if(j.eq.IdKalphar) then
          LamRat(NDatBlock)=Value
        else if(j.eq.IdDatCollTemp) then
          DatCollTemp(NDatBlock)=Value
        else if(j.eq.IdNRef90) then
          NRef90(NDatBlock)=IValue
        else if(j.eq.IdIndSlowest) then
          IndSlowest(NDatBlock)=IValue
        else if(j.eq.IdIndFastest) then
          IndFastest(NDatBlock)=IValue
        else if(j.eq.IdFLimPrint) then
          FLimPrint(NDatBlock)=Value
        else if(j.eq.IdFLimCull) then
          FLimCull(NDatBlock)=Value
        else if(j.eq.IdDataAve) then
          DataAve(NDatBlock)=IValue
        else if(j.eq.IdAddCentrSymm) then
          AddCentrSymm(NDatBlock)=IValue
        else if(j.eq.IdSigIMethod) then
          SigIMethod(NDatBlock)=IValue
        else if(j.eq.IdMultAve) then
          MultAve(NDatBlock)=IValue
        else if(j.eq.IdEM9ObsLim) then
          EM9ObsLim(NDatBlock)=Value
        else if(j.eq.IdEM9ScaleLim) then
          EM9ScaleLim(NDatBlock)=Value
        else if(j.eq.IdDiffScales) then
          DiffScales(NDatBlock)=IValue
        else if(j.eq.IdNoRefItems) then
          NoRefItems(NDatBlock)=IValue
        else if(j.eq.IdPerfMono) then
          FractPerfMon(NDatBlock)=Value
        else if(j.eq.IdScNoScheme) then
          ScNoScheme(1,NDatBlock)=IValue
          i=1
2000      if(k.lt.256) then
            i=i+1
            call StToInt(Veta,k,ScNoScheme(i,NDatBlock),1,.false.,ich)
            if(ich.eq.0) go to 2000
          endif
        else if(j.eq.IdTOFTTh) then
          TOFTTh(NDatBlock)=Value
        else if(j.eq.IdTOFJason) then
          TOFJason(NDatBlock)=IValue
        else if(j.eq.IdTOFDIFC) then
          TOFDifc(NDatBlock)=Value
        else if(j.eq.IdTOFDIFA) then
          TOFDifa(NDatBlock)=Value
        else if(j.eq.IdTOFZERO) then
          TOFZero(NDatBlock)=Value
        else if(j.eq.IdTOFCt) then
          TOFCt(NDatBlock)=Value
        else if(j.eq.IdTOFAt) then
          TOFAt(NDatBlock)=Value
        else if(j.eq.IdTOFZeroT) then
          TOFZeroT(NDatBlock)=Value
        else if(j.eq.IdTOFCe) then
          TOFCe(NDatBlock)=Value
        else if(j.eq.IdTOFZeroE) then
          TOFZeroE(NDatBlock)=Value
        else if(j.eq.IdTOFWCross) then
          TOFWCross(NDatBlock)=Value
        else if(j.eq.IdTOFTCross) then
          TOFTCross(NDatBlock)=Value
        else if(j.eq.IdTOFSig2) then
          TOFSigma(1,NDatBlock)=Value
        else if(j.eq.IdTOFSig1) then
          TOFSigma(2,NDatBlock)=Value
        else if(j.eq.IdTOFSig0) then
          TOFSigma(3,NDatBlock)=Value
        else if(j.eq.IdTOFGam2) then
          TOFGamma(1,NDatBlock)=Value
        else if(j.eq.IdTOFGam1) then
          TOFGamma(2,NDatBlock)=Value
        else if(j.eq.IdTOFGam0) then
          TOFGamma(3,NDatBlock)=Value
        else if(j.eq.IdTOFAlpha0) then
          TOFAlfbe(1,NDatBlock)=Value
        else if(j.eq.IdTOFBeta0) then
          TOFAlfbe(2,NDatBlock)=Value
        else if(j.eq.IdTOFAlpha1) then
          TOFAlfbe(3,NDatBlock)=Value
        else if(j.eq.IdTOFBeta1) then
          TOFAlfbe(4,NDatBlock)=Value
        else if(j.eq.IdTOFAlpha0e) then
          TOFAlfbe(1,NDatBlock)=Value
        else if(j.eq.IdTOFBeta0e) then
          TOFAlfbe(2,NDatBlock)=Value
        else if(j.eq.IdTOFAlpha1e) then
          TOFAlfbe(3,NDatBlock)=Value
        else if(j.eq.IdTOFBeta1e) then
          TOFAlfbe(4,NDatBlock)=Value
        else if(j.eq.IdTOFAlpha0t) then
          TOFAlfbt(1,NDatBlock)=Value
        else if(j.eq.IdTOFBeta0t) then
          TOFAlfbt(2,NDatBlock)=Value
        else if(j.eq.IdTOFAlpha1t) then
          TOFAlfbt(3,NDatBlock)=Value
        else if(j.eq.IdTOFBeta1t) then
          TOFAlfbt(4,NDatBlock)=Value
        else if(j.eq.IdEDOffset) then
          EDOffset(NDatBlock)=Value
        else if(j.eq.IdEDSlope) then
          EDSlope(NDatBlock)=Value
        else if(j.eq.IdEDTTh) then
          EDTTh(NDatBlock)=Value
          EDEnergy2D(NDatBlock)=.5*TokEV/sin(EDTTh(NDatBlock)*.5*ToRad)
        else if(j.eq.IdPwdMethod) then
          PwdMethod(NDatBlock)=IValue
        else if(j.eq.IdHKLF5) then
          HKLF5(NDatBlock)=IValue
        else if(j.eq.IdUseIncSpect) then
          UseIncSpect(NDatBlock)=IValue
        else if(j.eq.IdAveSigWt) then
          AveSigWt(NDatBlock)=IValue
        else if(j.eq.IdXDDirCos) then
          XDDirCos(NDatBlock)=IValue
        else if(j.eq.IdMagPolFlag) then
          MagPolFlag(NDatBlock)=IValue
          if(IValue.ne.0) ExistMagPol=.true.
        else if(j.eq.IdMagPolPlus) then
          MagPolPlus(NDatBlock)=Value
        else if(j.eq.IdMagPolMinus) then
          MagPolMinus(NDatBlock)=Value
        else if(j.eq.IdScToM90) then
          EM9ScToM90(NDatBlock)=Value
        else if(j.eq.IdEM9Instab) then
          EM9Instab(NDatBlock)=Value
        endif
        go to 1000
      else if(Klic.eq.1) then
        call OpenFile(ln,FileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        do i=1,NDatBlock
          if(DataType(i).eq.1.and.ExistM95) then
            if(UseEFormat91) then
              Cislo='1'
            else
              Cislo='0'
            endif
            k=0
            call WriteLabeledRecord(ln,id(IdUseEFormat91),Cislo,k)
            exit
          endif
        enddo
        n=0
        do 6000i=1,NDatBlock
          if(NRef90(i).le.0) go to 6000
          k=0
          n=n+1
          Veta=DatBlockName(n)
          call WriteLabeledRecord(ln,id(IdDatBlock),Veta,k)
          write(Veta,FormI15) NRef90(i)
          if(DataType(i).eq.1.and.ExistM95) then
            write(Veta(idel(Veta)+2:),
     1            '(a12,f8.3,1x,a12,f15.5,1x,3(a12,i5,1x),a12,f8.3,1x,
     2              a12,i5)')
     2        Id(IdEM9ObsLim),EM9ObsLim(i),
     3        Id(IdScToM90),EM9ScToM90(i),
     4        Id(IdNoRefItems),NoRefItems(i),
     5        Id(IdDiffScales),DiffScales(i),
     6        Id(IdDataAve),DataAve(i),
     7        Id(IdEM9ScaleLim),EM9ScaleLim(i),
     8        Id(IdHKLF5),HKLF5(i)
            k=0
            call WriteLabeledRecord(ln,Id(IdNRef90),Veta,k)
            if(DiffScales(i).eq.3) then
              write(Veta,'(10i5)')(ScNoScheme(j,i),j=1,NoRefItems(i))
              k=0
              call WriteLabeledRecord(ln,Id(IdScNoScheme),Veta,k)
            endif
            if(DataAve(i).gt.0) then
              write(Veta,'(i2,1x,4(a12,i2,1x))') IndSlowest(i),
     1          Id(IdIndFastest),IndFastest(i),
     2          Id(IdAddCentrSymm),AddCentrSymm(i),
     3          Id(IdSigIMethod),SigIMethod(i),
     4          Id(IdMultAve),MultAve(i)
              k=0
              call WriteLabeledRecord(ln,Id(IdIndSlowest),Veta,k)
              write(Veta,'(f10.3,1x,a12,f10.3,1x,a12,i5)') FLimPrint(i),
     1          Id(IdFLimCull),FLimCull(i),
     2          Id(IdAveSigWt),AveSigWt(i)
              if(EM9Instab(i).gt.-900.)
     1          write(Veta(idel(Veta)+2:),'(a12,f10.6)')
     2            Id(IdEM9Instab),EM9Instab(i)
              k=0
              call WriteLabeledRecord(ln,Id(IdFLimPrint),Veta,k)
            endif
          else
            k=0
            call WriteLabeledRecord(ln,Id(IdNRef90),Veta,k)
          endif
          if(XDDirCos(i).gt.0) then
            write(Veta,110) XDDirCos(i)
            k=0
            call WriteLabeledRecord(ln,Id(IdXDDircos),Veta,k)
          endif
          Nazev=Id(IdRadType)
          write(Veta,110) Radiation(i),Id(IdDataType),DataType(i)
          if(DataType(i).eq.2.and.
     1       (Radiation(i).ne.NeutronRadiation.or.
     2        TOFDifc(i).le.0.).and.
     3       (Radiation(i).ne.XRayRadiation.or.
     4        EDTTh(i).le.0.)) then
             write(Veta(idel(Veta)+2:),'(a12,i3)')
     1         Id(IdPwdMethod),PwdMethod(i)
          endif
          if(DataType(i).eq.-2.and.Radiation(i).eq.NeutronRadiation)
     1       write(Veta(idel(Veta)+2:),'(a12,i3)')
     2         Id(IdUseIncSpect),UseIncSpect(i)
          k=0
          call WriteLabeledRecord(ln,Nazev,Veta,k)
          Nazev=' '
          if(LamAve(i).gt.0.) then
            write(Veta,'(f11.5,3(1x,a12,1x,i3))')
     1                   LamAve(i),Id(IdLpFactor),LpFactor(i)
            Nazev=id(IdLambda)
            if(Radiation(i).eq.XRayRadiation) then
              k=idel(Veta)+2
              if(LpFactor(i).ne.PolarizedGuinier)
     1          write(Veta(k:),'(a12,f10.4)') Id(IdMonAngle),AngleMon(i)
              k=idel(Veta)+2
              if(LpFactor(i).eq.PolarizedMonoPer.or.
     1           LpFactor(i).eq.PolarizedMonoPar) then
                write(Veta(k:),'(a12,f10.3)')
     1            Id(IdPerfMono),FractPerfMon(i)
              else if(LpFactor(i).eq.PolarizedGuinier) then
                k=idel(Veta)+2
                write(Veta(k:),'(3(a12,f10.3,1x))')
     1                        Id(IdAlphaGMono),AlphaGMon(i),
     2                        Id(IdBetaGMono),BetaGMon(i),
     3                        Id(IdPerfMono),FractPerfMon(i)
              endif
            endif
          else
            if(Radiation(i).eq.NeutronRadiation) then
              if(TOFDifc(i).gt.0.) then
                if(TOFJason(i).eq.0) then
                  write(Veta,111)
     1                      float(TOFJason(i)),
     2              id(IdTOFDIFC),TOFDifc(i),
     3              id(IdTOFDIFA),TOFDifa(i),
     4              id(IdTOFZero),TOFZero(i),
     5              id(IdTOFTTh) ,TOFTTh(i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFJason),Veta,k)
                else
                  write(Veta,111)
     1                       float(TOFJason(i)),
     2              id(IdTOFCT)   ,TOFCt(i),
     3              id(IdTOFAt)   ,TOFAt(i),
     4              id(IdTOFZerot),TOFZerot(i),
     5              id(IdTOFTTh)  ,TOFTTh(i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFJason),Veta,k)
                  write(Veta,111)
     1                              TOFCe(i),
     2              id(IdTOFZeroe) ,TOFZeroe(i),
     3              id(IdTOFWCross),TOFWCross(i),
     4              id(IdTOFTCross),TOFTCross(i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFCe),Veta,k)
                endif
              endif
              if(TOFSigma(1,i).gt.-3000.) then
                write(Veta,111) TOFSigma(1,i),
     2            id(IdTOFSig1),TOFSigma(2,i),
     3            id(IdTOFSig0),TOFSigma(3,i)
                k=0
                call WriteLabeledRecord(ln,id(IdTOFSig2),Veta,k)
              endif
              if(TOFGamma(1,i).gt.-3000.) then
                write(Veta,111) TOFGamma(1,i),
     2            id(IdTOFGam1),TOFGamma(2,i),
     3            id(IdTOFGam0),TOFGamma(3,i)
                k=0
                call WriteLabeledRecord(ln,id(IdTOFGam2),Veta,k)
              endif
              if(TOFAlfbe(1,i).gt.-3000.) then
                if(TOFJason(i).eq.0) then
                  write(Veta,111) TOFAlfbe(1,i),
     2              id(IdTOFBeta0),TOFAlfbe(2,i),
     3              id(IdTOFAlpha1),TOFAlfbe(3,i),
     4              id(IdTOFBeta1),TOFAlfbe(4,i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFAlpha0),Veta,k)
                else
                  write(Veta,111) TOFAlfbe(1,i),
     2              id(IdTOFBeta0e),TOFAlfbe(2,i),
     3              id(IdTOFAlpha1e),TOFAlfbe(3,i),
     4              id(IdTOFBeta1e),TOFAlfbe(4,i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFAlpha0e),Veta,k)
                  write(Veta,111) TOFAlfbt(1,i),
     2              id(IdTOFBeta0t),TOFAlfbt(2,i),
     3              id(IdTOFAlpha1t),TOFAlfbt(3,i),
     4              id(IdTOFBeta1t),TOFAlfbt(4,i)
                  k=0
                  call WriteLabeledRecord(ln,id(IdTOFAlpha0t),Veta,k)
                endif
              endif
            else if(Radiation(i).eq.XRayRadiation) then
              if(EDTTh(i).gt.0.) then
                write(Veta,111)
     1                          EDOffset(i),
     2            id(IdEDSlope),EDSlope(i),
     4            id(IdEDTTh  ),EDTTh(i)
                k=0
                call WriteLabeledRecord(ln,id(IdEDOffset),Veta,k)
              endif
            endif
          endif
          if(Nazev.ne.' ') then
            k=0
            call WriteLabeledRecord(ln,Nazev,Veta,k)
          endif
          if(Radiation(i).eq.NeutronRadiation) then
            ExistNeutronData=.true.
          else if(Radiation(i).eq.XRayRadiation) then
            ExistXRayData=.true.
          endif
          if(NAlfa(i).gt.1) then
            write(Veta,'(i1,3(1x,a12,1x,f10.6))') NAlfa(i),
     1       id(IdKAlpha1),LamA1(i),
     2       id(IdKAlpha2),LamA2(i),id(IdKAlphar),LamRat(i)
            k=0
            call WriteLabeledRecord(ln,Id(IdNAlpha),Veta,k)
          endif
4200      write(Veta,104) DatCollTemp(i)
          k=0
          call WriteLabeledRecord(ln,Id(IdDatCollTemp),Veta,k)
          if(Radiation(i).eq.NeutronRadiation.and.MagPolFlag(i).ne.0)
     1      then
            write(Veta,'(i5,1x,a12,3f10.6,2(1x,a12,f10.6))')
     1        MagPolFlag(i),
     2        id(IdMagField),MagFieldDir(1:3,i),
     3        id(IdMagPolPlus),MagPolPlus(i),
     4        id(IdMagPolMinus),MagPolMinus(i)
            call ZdrcniCisla(Veta,9)
            k=0
            call WriteLabeledRecord(ln,id(IdMagPolFlag),Veta,k)
          endif
6000    continue
        Veta=' '
        k=0
        call WriteLabeledRecord(ln,Id(IdEnd),Veta,k)
      endif
8000  if(Klic.eq.0) then
        do i=1,NDatBlock
          if(Radiation(i).eq.XRayRadiation) then
            if(DataType(i).gt.0) then
              if(KLam(i).eq.0) then
                klam(i)=LocateInArray(LamAve(i),LamAveD,7,.0001)
                if(LamAve(i).le..05) then
                  if(NDatBlock.gt.1) then
                    Write(Cislo,FormI15) i
                    call zhusti(Cislo)
                    Veta='It concerns DataBlok#'//Cislo(:idel(Cislo))//
     1                   '.'
                  else
                    Veta=' '
                  endif
                  write(Cislo,'(f10.6)') LamAve(i)
                  call FeChybne(-1.,-1.,'unrealistic wave length'//
     1               ':'//Cislo(:idel(Cislo))//' has been read in.',
     2               Veta,Warning)
                endif
              endif
            endif
          else
            LamA1(i)=LamAve(i)
            LamA2(i)=LamAve(i)
          endif
        enddo
        write(format91(2:2),'(i1)') maxNDim
      endif
9999  call CloseIfOpened(ln)
      KDatBlock=KDatBlockIn
      return
104   format(6f11.4)
110   format(i3,1x,a12,1x,i3)
111   format(f10.2,6(1x,a12,1x,f10.4))
      end
