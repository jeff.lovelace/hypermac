      subroutine CrlIoSigLogIoPlots
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xpp(3),xo(3)
      character*17 :: Labels(3) =
     1              (/'%Quit     ','%Print    ','%Save     '/)
      Tiskne=.false.
      ln=0
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(120.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,3
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.3) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ln=NextLogicNumber()
      call iom95(0,fln(:ifln)//'.m95')
      call OpenRefBlockM95(ln,KRefBlock,fln(:ifln)//'.m95')
      if(ErrFlag.ne.0) go to 8000
      HardCopy=0
      XMin=0.
      XMax=0.
      YMax=0.
2000  call DRGetReflectionFromM95(ln,iend,ich)
      if(iend.ne.0) go to 2100
      if(ri.lt..1) go to 2000
      xpp(1)=log(ri)
      xpp(2)=rs/sqrt(ri)
      YMax=max(YMax,xpp(2))
      XMax=max(XMax,xpp(1))
      XMin=min(XMin,xpp(1))
      go to 2000
2100  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        xomn=XMin
        xomx=XMax*1.1
        yomn=0.
        yomx=YMax*1.1
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'log(Iobs)')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,
     1                        'sig(Iobs)/sqrt(Iobs)')
        xpp(3)=0.
        call CloseIfOpened(ln)
        call OpenRefBlockM95(ln,KRefBlock,fln(:ifln)//'.m95')
2200    call DRGetReflectionFromM95(ln,iend,ich)
        if(iend.ne.0) go to 2300
        if(ri.lt..1) go to 2200
        xpp(1)=log(ri)
        xpp(2)=rs/sqrt(ri)
        call FeXf2X(xpp,xo)
        call FeCircleOpen(xo(1),xo(2),3.,White)
        go to 2200
2300    HardCopy=0
        call CloseIfOpened(ln)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  call FeQuestRemove(id)
      return
      end
