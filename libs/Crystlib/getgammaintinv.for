      subroutine GetGammaIntInv(RM6In,GammaOut)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension RM6In(*),GammaOut(*),GammaP(9)
      Klic=0
      go to 1000
      entry GetGammaInt(RM6In,GammaOut)
      Klic=1
1000  do j=4,NDim(KPhase)
        do i=4,NDim(KPhase)
          GammaP(i-3+(j-4)*NDimI(KPhase))=RM6In(i+(j-1)*NDim(KPhase))
        enddo
      enddo
      if(Klic.eq.0) then
        call Matinv(GammaP,GammaOut,pom,NDimI(KPhase))
      else
        call CopyVek(GammaP,GammaOut,NDimI(KPhase)**2)
      endif
      return
      end
