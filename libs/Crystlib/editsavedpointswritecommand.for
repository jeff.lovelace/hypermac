      subroutine EditSavedPointsWriteCommand(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EditSavedPointsC/ nEdwLabel,nEdwXYZ,LabelString,xs
      dimension xs(3)
      character*8   LabelString
      character*80  Veta
      character*(*) Command
      character*256 EdwStringQuest
      save /EditSavedPointsC/
      ich=0
      LabelString=EdwStringQuest(nEdwLabel)
      if(LabelString.eq.' ') go to 8000
      Veta=EdwStringQuest(nEdwXYZ)
      k=0
      call StToReal(Veta,k,xs,3,.false.,ich)
      if(ich.ne.0) go to 8100
      write(Command,'(a8,3f10.6)') LabelString,xs
      call ZdrcniCisla(Command,4)
      go to 9999
8000  Veta='the string is empty,try again.'
      go to 9000
8100  Veta='Inacceptable numerical string'
      ich=1
9000  call FeChybne(-1.,-1.,Veta,' ',SeriousError)
9999  return
      end
