      subroutine MergeFileToM50(MergeFile)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) MergeFile
      character*5555 t256
      t256=fln(:ifln)//'_'//MergeFile(:idel(MergeFile))//'.tmp'
      ln=NextLogicNumber()
      call OpenFile(ln,t256,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1000  read(ln,FormA,end=2000) t256
      if(t256.eq.' ') go to 1000
      t256='  '//t256(:idel(t256))
      call PisPo80(55,t256)
      go to 1000
2000  call CloseIfOpened(ln)
      call DeleteFile(fln(:ifln)//'_'//MergeFile(:idel(MergeFile))//
     1                '.tmp')
9999  return
      ErrFlag=0
      end
