      subroutine iom94(klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension PomMat(36)
      character*256 radka
      character*80 t80
      character*11 id(31)
      character*9 nazev
      integer Exponent10
      logical poprve,EqIgCase
      data (id(i),i=1,31)
     1       /'difcode','corrlp','corrabs','nref95','nref96','NDim',
     2        'unitsnumber','lambda','cell1','cell2','cell3',
     3        'qvec1','qvec2','qvec3','ormat','trmat','formula',
     4        'mi','atabs','face','pointgr','centro','symmetry',
     5        'pointmat','twin','radius','mmax','diffsat','latvec',
     6        'radtype','spgroup'/
      equivalence (IdNumbers(1), IdDifCode ),(IdNumbers(2), IdCorrLp  ),
     1            (IdNumbers(3), IdCorrAbs ),(IdNumbers(4), IdNref95  ),
     2            (IdNumbers(5), IdNref96  ),(IdNumbers(6), IdNDim    ),
     3            (IdNumbers(7), IdUnitsNum),(IdNumbers(8), IdLambda  ),
     4            (IdNumbers(9), IdCell1   ),(IdNumbers(10),IdCell2   ),
     5            (IdNumbers(11),IdCell3   ),
     6            (IdNumbers(12),IdQVec1   ),(IdNumbers(13),IdQVec2   ),
     7            (IdNumbers(14),IdQVec3   ),
     8            (IdNumbers(15),IdOrmat   ),(IdNumbers(16),IdTrMat   ),
     9            (IdNumbers(17),IdFormula ),(IdNumbers(18),IdMi      ),
     a            (IdNumbers(19),IdAtAbs   ),(IdNumbers(20),IdFace    ),
     1            (IdNumbers(21),IdPointGr ),(IdNumbers(22),IdCentro  ),
     2            (IdNumbers(23),IdSymmetry),(IdNumbers(24),IdPointMat),
     3            (IdNumbers(25),IdTwin    ),(IdNumbers(26),IdRadius  ),
     4            (IdNumbers(27),Idmmax    ),(IdNumbers(28),IdDiffSat ),
     5            (IdNumbers(29),Idlatvec  ),(IdNumbers(30),IdRadType ),
     6            (IdNumbers(31),Idspgroup )
      if(klic.eq.0) then
        t80='old'
      else
        t80='unknown'
      endif
      call OpenFile(94,fln(:ifln)//'.m94','formatted',t80)
      if(ErrFlag.ne.0) go to 9999
      if(Klic.eq.0) then
        CorrAbs(1)=0
        CorrLp(1)=0
        RadiusRefBlock(1)=0.
        NComp(KPhase)=1
        MMaxRefBlock(1)=4
        call SetRealArrayTo(DiffSatRefBlock(1,1),3,.001)
        call SetRealArrayTo(AtAbsCoeff(1,KPhase),20,0.)
        AtAbsCoeffOwn(KPhase)=.false.
        NAtAbs=0
        do 4000i=1,31
          rewind 94
          if(i.ge.IdQVec1.and.i.le.IdQVec3) then
            if(NDim(KPhase).le.3) go to 4000
            idim=0
          else if(i.eq.IdFace) then
            NFaces(KRefBlock)=0
          endif
          poprve=.true.
          k=len(Radka)
1000      if(k.ge.len(Radka)) then
1005        read(94,FormA,err=9000,end=9010) Radka
            if(Radka(1:1).eq.'*'.or.Radka(1:1).eq.'#'.or.
     1         idel(Radka).le.0) go to 1005
            call vykric(radka)
            if(EqIgCase(radka,'end')) go to 4000
            k=0
          endif
          call kus(radka,k,nazev)
          j=islovo(nazev,id,31)
          if(j.lt.0) then
            go to 1000
          else if(j.ne.i) then
            if(j.eq.IdPointGr) then
              call kus(radka,k,cislo)
            endif
            go to 1000
          else
            if(i.eq.IdDifCode) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) DifCode(1)
            else if(i.eq.IdCorrLp) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) CorrLp(1)
            else if(i.eq.IdCorrAbs) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) CorrAbs(1)
            else if(i.eq.IdMMax) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) MMaxRefBlock(1)
            else if(i.eq.IdCell1) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,CellRefBlock(1,0),6,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdCell2.or.i.eq.IdCell3) then
              k=256
              go to 4000
            else if(i.eq.IdQVec1) then
              idim=idim+1
              if(idim.gt.NDimI(KPhase)) go to 4000
              call StToReal(Radka,k,QuRefBlock(1,idim,0),3,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdQVec2.or.i.le.IdQVec3) then
              k=256
              go to 4000
            else if(i.eq.IdOrMat) then
              if(.not.poprve) go to 4000
              j=1
              do l=1,3
                read(94,FormA) Radka
                k=0
                call StToReal(Radka,k,PomMat(j),3,.false.,ich)
                if(ich.ne.0) go to 9000
                j=j+3
              enddo
              call TrMat(PomMat,ub,3,3)
            else if(i.eq.IdTrMat) then
              if(.not.poprve) go to 4000
              j=1
              do l=1,NDim(KPhase)
                read(94,FormA) Radka
                k=0
                call StToReal(Radka,k,PomMat(j),NDim(KPhase),.false.,
     1                        ich)
                if(ich.ne.0) go to 9000
                j=j+NDim(KPhase)
              enddo
              call TrMat(PomMat,TrMP,NDim(KPhase),NDim(KPhase))
              k=256
            else if(i.eq.IdDiffSat) then
              if(.not.poprve) go to 4000
              call StToReal(Radka,k,DiffSatRefBlock(1,KRefBlock),3,
     1                      .false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdAtAbs) then
2200          call kus(Radka,k,Cislo)
              call posun(cislo,1)
              NAtAbs=NAtAbs+1
              read(cislo,107,err=9000) AtAbsCoeff(NAtAbs,KPhase)
              if(AtAbsCoeff(NAtAbs,KPhase).gt..0001)
     1          AtAbsCoeffOwn(KPhase)=.true.
              if(k.lt.80) go to 2200
            else if(i.eq.Idmi) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,1)
              read(cislo,107,err=9000) ami
            else if(i.eq.IdFace) then
              NFaces(KRefBlock)=NFaces(KRefBlock)+1
              n=NFaces(KRefBlock)
              call StToReal(Radka,k,dface(1,n,KRefBlock),4,.false.,ich)
              if(ich.ne.0) go to 9000
            else if(i.eq.IdRadius) then
              if(.not.poprve) go to 4000
              call kus(radka,k,cislo)
              call posun(cislo,1)
              read(cislo,107,err=9000) RadiusRefBlock(1)
            else if(i.eq.IdPointMat) then
              read(94,FormA)(Radka,j=1,3)
            else if(i.eq.IdTwin) then
              call kus(radka,k,cislo)
              call posun(cislo,0)
              read(cislo,FormI15,err=9000) n
              do j=2,n
                read(94,FormA)(Radka,k=1,3)
              enddo
            endif
          endif
3900      poprve=.false.
          go to 1000
4000    continue
      else
        write(Radka,102) DifCode,Id(IdCorrLp),CorrLp,Id(IdCorrAbs),
     1                   CorrAbs
        k=0
        call WriteLabeledRecord(94,Id(IdDifCode),Radka,k)
        write(Radka,102) nref95(KRefBlock),Id(IdNRef96),0
        k=0
        call WriteLabeledRecord(94,Id(IdNRef95),Radka,k)
        write(Radka,102) NDim(KPhase),Id(IdUnitsNum),NUnits(KPhase)
        k=0
        call WriteLabeledRecord(94,Id(IdNDim),Radka,k)
        if(NDim(KPhase).gt.3) then
          write(Radka,110)(DiffSatRefBlock(j,i),j=1,3),Id(Idmmax),
     1                     MMaxRefBlock(i)
          k=0
          call WriteLabeledRecord(94,Id(IdDiffSat),Radka,k)
        endif
        write(Radka,100) Radiation(1)
        k=0
        call WriteLabeledRecord(94,Id(IdRadType),Radka,k)
        write(Radka,103) LamAve(1)
        k=0
        call WriteLabeledRecord(94,Id(IdLambda),Radka,k)
        klam(1)=LocateInArray(LamAve(1),LamAveD,7,.0001)
        Idp=IdCell1
        do i=1,3
          write(Radka,109)(CellRefBlock(j,i),j=1,6)
          k=0
          call WriteLabeledRecord(94,Id(Idp),Radka,k)
          Idp=Idp+1
        enddo
        Idp=IdQVec1
        do i=1,3
          do k=1,NDimI(KPhase)
            write(Radka,103)(QuRefBlock(j,k,i),j=1,3)
            l=0
            call WriteLabeledRecord(94,Id(Idp),Radka,l)
          enddo
          Idp=Idp+1
        enddo
        write(94,FormA) Id(IdOrmat)(:idel(Id(IdOrmat)))
        do i=1,3
          write(94,105)(ub(i,j,1),j=1,3)
        enddo
        write(94,FormA) Id(IdTrMat)(:idel(Id(IdTrMat)))
        do i=1,NDim(KPhase)
          write(94,106)(trmp(i+(j-1)*NDim(KPhase)),j=1,NDim(KPhase))
        enddo
        k=0
        call WriteLabeledRecord(94,Id(IdFormula),Formula(KPhase),k)
        pom=0.
        do i=1,NAtFormula(KPhase)
          call CrlReadAbsCoeff(0,AtType(i,KPhase),AtAbsCoeffTab(i),ich)
          if(ich.ne.0) go to 9900
          if(AtAbsCoeffTab(i).ne.0..and.AtAbsCoeff(i,KPhase).ne.0) then
            if(abs((AtAbsCoeff(i,KPhase)-AtAbsCoeffTab(i))/
     1              AtAbsCoeffTab(i)).gt..00001) go to 5430
          endif
        enddo
        go to 5460
5430    AtAbsCoeffOwn(KPhase)=.true.
        if(AtAbsCoeffOwn(KPhase)) then
          i=0
          Radka=Id(IdAtAbs)
          ir=idel(Radka)
5450      i=i+1
          if(i.le.NAtFormula(KPhase)) then
            j=5-Exponent10(AtAbsCoeff(i,KPhase))
            j=min(13,j)
            j=max(0,j)
            write(RealFormat(6:7),'(i2)') j
            write(Cislo,RealFormat) AtAbsCoeff(i,KPhase)
            call ZdrcniCisla(Cislo,1)
            ic=idel(Cislo)
            if(ir+ic+1.gt.80) then
              write(94,FormA) Radka(:ir)
              Radka=Id(IdAtAbs)
              ir=idel(Radka)
            endif
            Radka=Radka(:ir)//' '//Cislo(:ic)
            ir=ir+ic+1
            go to 5450
          endif
          write(94,FormA) Radka(:ir)
        endif
5460    if(RadiusRefBlock(1).gt.0.) then
          write(Radka,104) RadiusRefBlock(1)
          k=0
          call WriteLabeledRecord(94,Id(IdRadius),Radka,k)
        endif
        do i=1,NFaces(KRefBlock)
          write(Radka,104)(dface(j,i,1),j=1,4)
          k=0
          call WriteLabeledRecord(94,Id(IdFace),Radka,k)
        enddo
        if(Grupa(KPhase).ne.' ') then
          k=0
          call WriteLabeledRecord(94,Id(Idspgroup),Grupa(KPhase),k)
        endif
        write(94,'(''end'')')
      endif
      call matinv(ub,ubi,pom,3)
      call DRSetCell(0)
      go to 9999
9000  call FeReadError(94)
      go to 9900
9010  t80='Unexpected end of file'
      call FeChybne(-1.,-1.,'wrong data on M94 file.',t80,SeriousError)
9900  ErrFlag=1
9999  call CloseIfOpened(94)
      return
100   format(15i5)
102   format(i10,10(1x,a9,1x,i10))
103   format(8f10.6)
104   format(8f10.4)
105   format(6f12.6)
106   format(6f12.3)
107   format(f15.0)
109   format(3f10.4,3f10.3)
110   format(3f10.4,1x,a8,i10)
      end
