      subroutine EqStringToIntMatrix(String,Variables,n,Matrix,Vector,
     1                               ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) String,Variables
      character*80  Row(:)
      allocatable Row
      integer Matrix(n,n),Vector(n)
      allocate(Row(n))
      Row=' '
      ich=0
      if(index(String,',').gt.0) then
        k=1
        do i=1,n-1
          j=index(String(k:),',')+k-1
          Row(i)=String(k:j-1)
          k=j+1
        enddo
        Row(n)=String(k:)
      else
        if(idel(String).gt.n) go to 9000
        do i=1,n
          Row(i)=String(i:i)
        enddo
      endif
      Matrix=0
      Vector=0
      do i=1,n
        k=1
        Cislo=' '
        do m=1,idel(Row(i))
          j=LocateSubstring(Variables,Row(i)(m:m),.false.,.true.)
          if(j.gt.0) then
            if(Cislo.eq.' '.or.Cislo.eq.'+') then
              Cislo='1'
            else if(Cislo.eq.'-') then
              Cislo='-1'
            endif
            call Posun(Cislo,0)
            read(Cislo,FormI15,err=9000) l
            Matrix(j,i)=Matrix(j,i)+l
            Cislo=' '
          else if(Row(i)(m:m).eq.'*') then
            cycle
          else
            j=index(Cifry,Row(i)(m:m))
            if(j.eq.11) go to 9000
            Cislo=Cislo(:idel(Cislo))//Row(i)(m:m)
            if(m.eq.idel(Row(i))) then
              read(Cislo,FormI15,err=9000) l
              Vector(i)=l
            endif
          endif
        enddo
      enddo
      go to 9999
9000  ich=1
9999  deallocate(Row)
      return
      end
