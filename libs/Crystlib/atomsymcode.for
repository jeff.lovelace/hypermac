      subroutine AtomSymCode(AtomName,IAtom,ISym,Icentr,ShiftSym,ITrans,
     1           ich)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension  ShiftSym(*),ITrans(*)
      character*(*) AtomName
      character*80  SymSt
      ich=0
      n=idel(AtomName)
      l=index(AtomName,'#')
      if(l.le.0) l=n+1
      IAtom=ktat(atom,NAtAll,AtomName(:l-1))
      if(IAtom.le.0) then
        ich=1
        go to 9999
      endif
      isw=iswa(IAtom)
      ksw=kswa(IAtom)
      call SetRealArrayTo(ShiftSym,NDim(ksw),0.)
      call SetIntArrayTo(ITrans,3,0)
      if(l.eq.n+1) then
        ISym=1
        Icentr=1
        go to 9999
      endif
      SymSt=AtomName(l+1:n)
1010  i=index(SymSt,',')
      if(i.ne.0) then
        SymSt(i:i)=' '
        go to 1010
      endif
      n=n-l
      call mala(SymSt)
      is=index(SymSt,'s')
      if(is.gt.0) SymSt(is:is)=' '
      ic=index(SymSt,'c')
      if(ic.gt.0) SymSt(ic:ic)=' '
      it=index(SymSt,'t')
      if(it.gt.0) SymSt(it:it)=' '
      if(is.le.0) then
        ISym=1
      else
        j=is
        call kus(SymSt,j,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8000) ISym
        if(ISym.eq.0.or.iabs(ISym).gt.NSymm(ksw)) go to 8000
      endif
      call CopyVek(s6(1,iabs(ISym),isw,ksw),ShiftSym,NDim(ksw))
      if(ic.gt.0) then
        j=ic
        call kus(SymSt,j,cislo)
        call posun(cislo,0)
        read(cislo,FormI15,err=8000) icentr
        if(icentr.lt.1.or.icentr.gt.NLattVec(ksw)) go to 8000
        call AddVek(ShiftSym,vt6(1,icentr,isw,ksw),ShiftSym,
     1              NDim(ksw))
      else
        icentr=1
      endif
      if(it.gt.0) then
        j=it
        call StToInt(SymSt,j,ITrans,3,.false.,ich)
        if(ich.ne.0) go to 8000
        do i=1,3
          ShiftSym(i)=ShiftSym(i)+float(ITrans(i))
        enddo
      endif
      go to 9999
8000  ich=2
9999  return
      end
