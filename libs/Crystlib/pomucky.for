      subroutine Pomucky(Klic)
      use Basic_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      character*256 EdwStringQuest
      dimension xs(4),x4dif(3),tdif(3),GammaInt(1)
      il=2
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,140.,il,
     1  'Prevadi x40 pro zadany atom na t',0,LightGray,0,0)
      il=1
      Veta='%Atom'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=80.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwAtom=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      il=il+1
      dpom=40
      Veta='x40'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwX4=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,0.,.false.,.false.)
      Veta='t'
      tpom=75.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwT=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,0.,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw) then
        Veta=EdwStringQuest(nEdwAtom)
        call atsymi(Veta,ia,xs,x4dif,tdif,is,ich,*1500)
        call GetGammaInt(RM6(1,iabs(is),1,KPhase),GammaInt)
        if(CheckNumber.eq.nEdwX4) then
          call FeQuestRealFromEdw(nEdwX4,xpom)
          call qbyx(xs,tdif,1)
          xpom=GammaInt(1)*xpom-tdif(1)-x4dif(1)
          call FeQuestRealEdwOpen(nEdwT,xpom,.false.,.false.)
          go to 1500
        else

        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      return
      end
