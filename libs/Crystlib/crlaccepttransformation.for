      logical function CrlAcceptTransformation(Text,RMat,Shift,n,Smb,
     1                                         idf)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 Veta
      character*80 SmbPrime(6)
      character*(*) Text,Smb(*)
      real RMat(*),Shift(*)
      logical WizardModeIn
      WizardModeIn=WizardMode
      WizardMode=.false.
      do i=1,n
        SmbPrime(i)=Smb(i)(:idel(Smb(i)))//''''
      enddo
      id=NextQuestId()
      xqd=max(FeTxLength(Text)+10.,300.)
      il=6+n
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,-1)
      il=1
      tpom=5.
      call FeQuestLblMake(id,tpom,il,Text,'L','N')
      YShow=QuestYPosition(id,-55)+QuestYMin(id)
      XShow=QuestXMin(id)+65.
      call CrlMatEqShow(XShow,YShow,n,RMat,0,SmbPrime,Smb,3)
      il=il+5
      write(Veta,'(3f10.3)') Shift(1:n)
      call ZdrcniCisla(Veta,3)
      do i=1,idel(Veta)
        if(Veta(i:i).eq.' ') Veta(i:i)=','
      enddo
      Veta='Origin shift: ('//Veta(:idel(Veta))//')'
      call FeQuestLblMake(id,tpom+120.,il,Veta,'L','N')
      il=-10*il-7
      call FeQuestLinkaMake(id,il)
      il=il-7
      Veta='Do you want to apply this transformation?'
      tpom=xqd*.5
      call FeQuestLblMake(id,tpom,il,Veta,'C','N')
      il=il-10
      Veta='Yes'
      dpom=40.
      xpom=xqd*.5-dpom-10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtYes=ButtonLastMade
      Veta='No'
      xpom=xqd*.5+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtNo=ButtonLastMade
      CrlAcceptTransformation=idf.gt.0
      if(CrlAcceptTransformation) then
        nButtOld=nButtYes
      else
        nButtOld=nButtNo
      endif
1500  if(CrlAcceptTransformation) then
        nButtNew=nButtYes
      else
        nButtNew=nButtNo
      endif
      BlockedActiveObj=.false.
      call FeQuestActiveObjOff
      ActiveObj=10000*ActButton+nButtNew+ButtonFr-1
      call FeQuestActiveObjOn
      if(nButtNew.ne.nButtOld) then
        i=nButtNew
        call FeQuestMouseToButton(nButtNew)
        nButtOld=nButtNew
      endif
      call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1    (CheckNumber.eq.nButtYes.or.CheckNumber.eq.nButtNo)) then
        CrlAcceptTransformation=CheckNumber.eq.nButtYes
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call CrlMatEqHide
      call FeQuestRemove(id)
      WizardMode=WizardModeIn
      return
      end
