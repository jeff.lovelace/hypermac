      subroutine MakeKeyWordFile(KeyWord,Program,NActive,NPassive)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) KeyWord,Program
      character*5555 t256
      character*256 FileName
      character*80 t80
      logical   EqIgCase,TakeComments,JeToRefine,JeToDontUse
      jp=islovo(KeyWord,NactiKeywords,NactiKeys)
      ln=NextLogicNumber()
      FileName=fln(:ifln)//'_'//KeyWord(:idel(KeyWord))//'.tmp'
      call DeleteFile(FileName)
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      if(ErrFlag.ne.0) go to 9000
      call najdi(Program,j)
      if(j.ne.1) go to 9000
      nstar=0
      nline=0
      NPassive=0
      NActive=0
      TakeComments=.false.
      JeToRefine=EqIgCase(Program,'refine')
      if(JeToRefine) then
        JeToDontUse=EqIgCase(KeyWord,'dontuse')
      else
        JeToDontUse=.false.
      endif
1000  read(m50,FormA,end=9000) t80
1010  if(t80(1:1).eq.'*'.or.t80(1:1).eq.'#') then
        if(t80(2:2).eq.'#') then
          call mala(t80)
          if(index(t80,'begin').gt.0.and.
     1       index(t80,KeyWord(:idel(KeyWord))).gt.0) then
            TakeComments=.true.
            go to 1000
          endif
        endif
        if(TakeComments) then
          nstar=nstar+1
          nline=nline+1
          t80(1:1)='#'
          write(ln,FormA) t80(:idel(t80))
        endif
        go to 1000
      endif
      call DeleteFirstSpaces(t80)
      call vykric(t80)
      if(idel(t80).le.0) go to 1000
      k=0
      call kus(t80,k,Cislo)
      if(EqIgCase(Cislo,'end')) go to 9000
      j=0
      if(t80(1:1).eq.'!') j=1
      call kus(t80,j,Cislo)
      call Mala(Cislo)
      j=islovo(Cislo,NactiKeywords,NactiKeys)
      if(j.eq.jp.or.(JeToRefine.and.JeToDontUse.and.
     1               EqIgCase(Cislo,'useonly'))) then
        TakeComments=.true.
        t256=t80
        call DeleteFirstSpaces(t256)
1020    read(m50,FormA) t80
        call DeleteFirstSpaces(t80)
        if(t80.eq.' ') go to 1050
        k=idel(t256)
        j=1
        if(t80(j:j).eq.'&'.or.t256(k:k).eq.'+'.or.t256(k:k).eq.'-') then
          if(t256(k:k).eq.'+'.and.t80(j:j).eq.'+') then
            j=j+1
          else if(t256(k:k).eq.'-'.and.t80(j:j).eq.'+') then
            j=j+1
          else if(t256(k:k).eq.'+'.and.t80(j:j).eq.'-') then
            k=k-1
          else if(t256(k:k).eq.'-'.and.t80(j:j).eq.'-') then
            t80(j:j)='+'
            k=k-1
          else if(t80(j:j).eq.'&') then
            t80(j:j)=' '
            j=j+1
          endif
          t256=t256(:k)//t80(j:idel(t80))
          go to 1020
        endif
1050    write(ln,FormA) t256(:idel(t256))
        if(t256(1:1).eq.'!') then
          NPassive=NPassive+1
        else
          NActive=NActive+1
        endif
        nline=nline+1
        nstar=0
        go to 1010
      endif
      go to 1000
9000  call CloseIfOpened(m50)
      call CloseIfOpened(ln)
      if(nstar.gt.0)
     1  call DeleteLinesFromFile(FileName,nline-nstar+1,nline)
      return
      end
