      subroutine CrlDraw3dMap
      use Atoms_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*256 Veta,FileName,Command
      real TMat(36),TMatI(36),rmx(9),rmb(36),xp(9)
      logical WholeCell,ExistFile
      if(LocateSubstring(Call3dMaps,'mce.exe',.false.,.true.).gt.1)
     1  then
        IdCall3dMaps=IdCall3dMCE
      else if(LocateSubstring(Call3dMaps,'Vesta.exe',.false.,.true.)
     1       .gt.1) then
        IdCall3dMaps=IdCall3dVesta
      else if(LocateSubstring(Call3dMaps,'MoleCoolQt.exe',.false.,
     1                        .true.).gt.1) then
        IdCall3dMaps=IdCall3dMoleCoolQt
      else
        IdCall3dMaps=IdCall3dNone
      endif
      call NactiM81
      WholeCell=.true.
      do i=1,3
        pom=xmax(i)-xmin(i)+dx(i)
        if(abs(pom-1.).gt..001.and.WholeCell) then
          WholeCell=.false.
          exit
        endif
      enddo
      ln=NextLogicNumber()
      Veta=' '
      if(IdCall3dMaps.eq.IdCall3dMCE.or.
     1   IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
        FileName=fln(:ifln)//'_tmp.grd'
        if(IdCall3dMaps.eq.IdCall3dMCE.and.NAtAll.gt.0) then
          Veta=fln(:ifln)//'_tmp.cif'
          call CIFMakeBasicTemplate(Veta,0)
          call CIFUpdate(Veta,.true.,0)
        endif
      else
        FileName=fln(:ifln)//'_tmp.xplor'
      endif
      call OpenFile(ln,FileName,'formatted','unknown')
      if(IdCall3dMaps.eq.IdCall3dMCE.or.
     1   IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
        if(IdCall3dMaps.eq.IdCall3dMoleCoolQt) then
          i=0
        else
          i=1
        endif
        call ConMakeGrdFile(ln,nx(3),i)
      else
        do i=3,NDim(KPhase)
          if(nx(i).gt.1) then
            nx3=nx(i)
            exit
          endif
        enddo
        call ConMakeXPlorFile(ln,nx3,0)
      endif
      call CloseIfOpened(ln)
      if(IdCall3dMaps.eq.IdCall3dVESTA) then
        MakeCIFForGraphicViewer=.true.
        call SetRealArrayTo(TMat,NDimQ(KPhase),0.)
        k=0
        do i=1,NDim(KPhase)
          do j=1,NDim(KPhase)
            k=k+1
            if(j.eq.iorien(i)) TMat(k)=xmax(i)-xmin(i)+dx(i)
          enddo
        enddo
        call MatInv(TMat,TMatI,pom,NDim(KPhase))
        call MatBlock3(TMatI,rmx,NDim(KPhase))
        call SrotB(rmx,rmx,rmb)
        call UnitMat(TMat,NDim(KPhase))
        call MatInv(TMat,TMatI,pom,NDim(KPhase))
        open(m40,file=fln(:ifln)//'_tmp.m40')
        do i=1,5
          write(m40,FormA)
        enddo
        NaUse=0
        if(WholeCell) then
          if(iorien(1).le.3.and.iorien(2).le.3) then
            do i=1,NAtCalc
              if(kswa(i).eq.KPhase) NaUse=NaUse+1
            enddo
          endif
          do i=1,NAtCalc
            if(kswa(i).eq.KPhase) then
              call MultM(rmx,x(1,i),xp,3,3,1)
              itfp=min(itf(i),2)
              if(itf(i).gt.1) then
                call MultM(rmb,beta(1,i),xp(4),6,6,1)
                do j=1,6
                  beta(j,i)=beta(j,i)/urcp(j,iswa(i),KPhase)
                enddo
              else
                xp(4)=beta(1,i)/episq
                call SetRealArrayTo(xp(5),5,0.)
              endif
              write(m40,108) Atom(i),isf(i),itfp,ai(i),(xp(j),j=1,9)
            endif
          enddo
        else
          do i=1,3
            CellPar(i,1,KPhase)=CellParCon(i)*(xmax(i)-xmin(i)+dx(i))
          enddo
          do i=4,6
            CellPar(i,1,KPhase)=CellParCon(i)
          enddo
          call ConCellParNorm(CellPar(1,1,KPhase))
          Grupa(KPhase)='P1'
          NSymm(KPhase)=1
          NSymmN(KPhase)=1
          NLattVec(KPhase)=1
          NGrupa(KPhase)=1
          CrSystem(KPhase)=1
          Lattice(KPhase)='P'
          Veta=fln(:ifln)//'_dratoms_vesta.tmp'
          if(ExistFile(Veta)) then
            lna=NextLogicNumber()
            call OpenFile(lna,Veta,'formatted','unknown')
            if(ErrFlag.ne.0) go to 2860
2820        read(lna,'(2i3,6f10.5)',end=2860) ia,NumAt,xpp
            call MultM(rmx,xpp,xp,3,3,1)
            do i=1,3
              if(xp(i).lt.0.or.xp(i).gt.1.) go to 2820
            enddo
            NaUse=NaUse+1
            write(m40,108) Atom(ia),isf(ia),1,1.,(xp(i),i=1,3),.01
            go to 2820
2860        call CloseIfOpened(lna)
          endif
        endif
        rewind m40
        do i=1,5
          read(m40,FormA)
        enddo
        n=NAtCalc
        NAtCalc=NaUse
        Veta=fln(:ifln)//'_tmp.cif'
        call CIFMakeBasicTemplate(Veta,1)
        call CIFUpdate(Veta,.true.,1)
        close(m40,status='delete')
        NAtCalc=n
        MakeCIFForGraphicViewer=.false.
        Command=' '
        FileName=fln(:ifln)//'_tmp.vesta'
        call OpenFile(ln,FileName,'formatted','unknown')
        write(ln,FormA) '#VESTA_FORMAT_VERSION 2'
        write(ln,FormA)
        write(ln,FormA) 'IMPORT_STRUCTURE'
        write(ln,FormA) fln(:ifln)//'_tmp.cif'
        write(ln,FormA)
        write(ln,FormA) 'IMPORT_DENSITY'
        write(ln,FormA) '+1.00000 '//fln(:ifln)//'_tmp.xplor'
        write(ln,FormA)
        write(ln,FormA) 'TRANM'
        write(ln,'(3f10.6,9i4)')(0.,i=1,3),(nint(TMat(i)),i=1,9)
        write(ln,FormA)
        call CloseIfOpened(ln)
        Veta=' '
      endif
      Command=Call3dMaps(:idel(Call3dMaps))
      if(IdCall3dMaps.ne.IdCall3dMoleCoolQt) then
        Command=Call3dMaps(:idel(Call3dMaps))//' '//
     1       FileName(:idel(FileName))//' '//Veta(:idel(Veta))
      else
        call FeFillTextInfo('contour1.txt',0)
        i=index(TextInfo(1),'#$%')
        TextInfo(1)(i:)=FileName(:idel(FileName))//'"'
        if(.not.Obecny) then
          do i=4,6
            if(abs(CellPar(i,1,KPhase)-90.).gt..001) then
              NInfo=NInfo+1
              TextInfo(NInfo)='WARNING: The maps are related to '//
     1                              'the non-orthogonal system which '//
     2                              'not supported by the grd format.'
              NInfo=NInfo+1
              TextInfo(NInfo)='For this reason the maps in '//
     1                              'MoleCoolQt are deformated.'
              exit
            endif
          enddo
        endif
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
      endif
      call FeSystem(Command)
      call DeleteFile(FileName)
      if(IdCall3dMaps.eq.IdCall3dMCE.and.Veta.ne.' ')
     1  call DeleteFile(Veta)
      if(IdCall3dMaps.eq.IdCall3dVESTA) then
        call DeleteFile(fln(:ifln)//'_tmp.xplor')
        call DeleteFile(fln(:ifln)//'_tmp.cif')
        call DeleteFile(fln(:ifln)//'_tmp.xsf')
        call iom40(0,0,fln(:ifln)//'.m40')
        call iom50(0,0,fln(:ifln)//'.m50')
      endif
      call CloseIfOpened(81)
      return
108   format(a8,2i3,4x,4f9.6/6f9.6)
      end
