      subroutine PitForSimple(Klic,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension pa(1)
      character*80 Clen
      character*7 AtTypeP
      logical EqIgCase
      ich=0
      NAtFormula(KPhase)=0
      isfh(KPhase)=0
      k=0
      idl=idel(Formula(KPhase))
1100  call Kus(Formula(KPhase),k,Clen)
      idlcl=idel(Clen)
      kp=min(k,idl)-idlcl
      if=0
      ic=0
      do i=1,idlcl
        if(index(Cifry(:11),Clen(i:i)).gt.0) then
          if(ic.eq.0) ic=i
        else
          if(ic.gt.0) then
            Clen(i:)=' '
            k=kp+i-1
            go to 1300
          endif
          if(if.eq.0) if=i
        endif
      enddo
1300  if(if.ne.0) then
        NAtFormula(KPhase)=NAtFormula(KPhase)+1
        MaxNAtFormula=max(MaxNAtFormula,NAtFormula(KPhase))
        if(ic.gt.0) then
          AtTypeP=Clen(if:ic-1)
        else
          AtTypeP=Clen
        endif
        call UprAt(AtTypeP)
        do i=1,98
          if(EqIgCase(AtTypeP,atn(i))) go to 1360
        enddo
        if(EqIgCase(AtTypeP,'D')) then
          isfh(KPhase)=NAtFormula(KPhase)
          go to 1360
        endif
        go to 9200
1360    AtMultP=-1.
        if(EqIgCase(AtTypeP,'H').and.isfh(KPhase).eq.0)
     1    isfh(KPhase)=NAtFormula(KPhase)
        if(Klic.eq.0) then
          call ReallocFormF(NAtFormula(KPhase),NPhase,NDatBlock)
          AtMult(NAtFormula(KPhase),KPhase)=AtMultP
          AtType(NAtFormula(KPhase),KPhase)=AtTypeP
          AtTypeFull(NAtFormula(KPhase),KPhase)=AtTypeP
          AtTypeMag(NAtFormula(KPhase),KPhase)=' '
          AtTypeMagJ(NAtFormula(KPhase),KPhase)=' '
        endif
      endif
      if(ic.gt.0) then
        if(if.le.0.and.AtMultP.gt.0.) go to 9000
        kk=ic-1
        call StToReal(Clen,kk,pa,1,.false.,ich)
        if(ich.ne.0) go to 9100
        AtMultP=pa(1)
        if(Klic.eq.0) AtMult(NAtFormula(KPhase),KPhase)=AtMultP
      endif
      if(k.lt.len(Formula(KPhase))) go to 1100
      if(Klic.eq.0) then
        do i=1,NAtFormula(KPhase)
          if(AtMult(i,KPhase).lt.0.) AtMult(i,KPhase)=1.
        enddo
      endif
      go to 9999
9000  call FeChybne(-1.,-1.,'for atom "'//AtTypeP(:idel(AtTypeP))//
     1              '" in the formula "'//
     2              Formula(KPhase)(:idel(Formula(KPhase)))//'"',
     3              'there are two coefficients',SeriousError)
      go to 9900
9100  call FeChybne(-1.,-1.,'incorrect real number in chemical formula '
     1            //Formula(KPhase)(:idel(Formula(KPhase)))//'"',
     2              Clen(ic:),SeriousError)
      go to 9900
9200  call FeChybne(-1.,-1.,'atom type "'//
     1              AtTypeP(:idel(AtTypeP))//
     2              '" is not on the list.',' ',SeriousError)
9900  ich=1
      NAtFormula(KPhase)=0
9999  return
      end
