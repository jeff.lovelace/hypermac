      subroutine WriteKeys(Procedure)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      character*(*) Procedure
      character*256 CommandFile
      character*80  t80
      logical FeYesNo,FileDiff,EqIgCase,JeToContour
      save konec,JeToContour,CommandFile
      JeToContour=EqIgCase(Procedure,'Contour')
      call OpenFile(m50,fln(:ifln)//'.l51','formatted','old')
      if(ErrFlag.ne.0) go to 9000
      CommandFile='jcmd'
      call CreateTmpFile(CommandFile,i,0)
      call FeTmpFilesAdd(CommandFile)
      call OpenFile(55,CommandFile,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      konec=0
1000  read(m50,FormA,end=2000) t80
      write(55,FormA) t80(:idel(t80))
      k=0
      call kus(t80,k,Cislo)
      if(EqIgCase(Cislo,Procedure)) then
        if(NPhase.gt.1.and.k.lt.80) then
          call kus(t80,k,Cislo)
          if(.not.EqIgCase(Cislo,PhaseName(KPhase))) go to 1000
        else if(KPhase.gt.1.and..not.EqIgCase(Cislo,'refine')) then
          go to 1000
        endif
      else
        go to 1000
      endif
      backspace 55
1500  read(m50,FormA,end=2000) t80
      k=0
      call kus(t80,k,Cislo)
      if(.not.EqIgCase(Cislo,'end')) go to 1500
      go to 2100
2000  konec=1
2100  if(NPhase.le.1.or.EqIgCase(Procedure,'Refine')) then
        write(55,FormA) Procedure(:idel(Procedure))
      else
        write(55,FormA) Procedure(:idel(Procedure))//' '//
     1     PhaseName(KPhase)(:idel(PhaseName(KPhase)))
      endif
      t80=' '
      do 2200i=1,NactiInt+NactiReal
        id=max(idel(t80),1)
        if(i.le.NactiInt) then
          if(DefaultInt(i).eq.NacetlInt(i)) go to 2200
          write(Cislo,FormI15) NacetlInt(i)
          call Zhusti(Cislo)
        else
          if(DefaultReal(i).eq.NacetlReal(i)) go to 2200
          write(Cislo,'(f12.6)') NacetlReal(i)
          call ZdrcniCisla(Cislo,1)
        endif
        ik=idel(NactiKeywords(i))
        ic=idel(Cislo)
        if(id+ic+ik.lt.78.and.id.ne.0) then
          t80=t80(:id)//' '//NactiKeywords(i)(1:ik)//' '//Cislo(1:ic)
        else
          if(id.ne.0) write(55,FormA) t80(:id)
          t80='  '//NactiKeywords(i)(1:ik)//' '//Cislo(1:ic)
        endif
2200  continue
      if(idel(t80).gt.0) write(55,FormA) t80(:idel(t80))
      go to 9999
      entry NastavKeys(Procedure)
      rewind m50
      konec=0
2500  read(m50,FormA,end=9999) t80
      k=0
      call kus(t80,k,Cislo)
      if(EqIgCase(Cislo,Procedure)) then
        if(NPhase.gt.1.and.k.lt.80) then
          call kus(t80,k,Cislo)
          if(.not.EqIgCase(Cislo,PhaseName(KPhase))) go to 2500
        else if(KPhase.gt.1.and..not.EqIgCase(Cislo,'refine')) then
          go to 2500
        endif
      else
        go to 2500
      endif
      go to 9999
      entry DopisKeys(Klic)
      if(konec.eq.1) go to 3100
3000  read(m50,FormA,end=3100) t80
      write(55,FormA) t80(:idel(t80))
      go to 3000
3100  call CloseIfOpened( 55)
      call CloseIfOpened(m50)
      if(AlwaysStartProgram) then
        i=3
        go to 5000
      else if(NeverStartProgram) then
        i=1
        go to 5000
      endif
      if(FileDiff(CommandFile,fln(:ifln)//'.l51')) then
        if(JeToContour) then
          if(Obecny) then
            if(IPlane.gt.0) then
              if(klic.eq.1) then
                if(SaveAs.eq.0) then
                  call ConWriteKeysSaveAsWithRepeat(t80)
                  if(SaveAs.eq.1) go to 9999
                else if(SaveAs.eq.-1) then
                  i=1
                  go to 5000
                endif
                if(t80.eq.' ') then
                  i=2
                else
                  i=1
                endif
              else
                i=2
              endif
            else if(IPlane.eq.-2) then
              call ConWriteKeysSaveAs(3,t80)
              if(t80.eq.' ') then
                i=2
              else
                i=1
              endif
            endif
          else
            if(IPlane.eq.-2) then
              call ConWriteKeysSaveAs(3,t80)
            else
              call ConWriteKeysSaveAs(2,t80)
              if(t80.eq.' ') then
                i=2
              else
                i=1
              endif
            endif
          endif
        else
          if(Klic.eq.1) then
            i=1
          else
            call FeYesNoStart(-1.,-1.,'Do you want to save new '//
     1                        'commands?',3,i)
          endif
        endif
      else
        if(JeToContour) then
          i=2
        else
          if(Klic.eq.1) then
            i=2
          else
            if(FeYesNo(-1.,-1.,'Do you want to start the program?',1))
     1        then
              i=4
            else
              i=2
            endif
          endif
        endif
      endif
5000  if(i.eq.2.or.i.eq.4) then
        call DeleteFile(CommandFile)
      else
        call iom50(0,0,fln(:ifln)//'.m50')
        call MoveFile(CommandFile,fln(:ifln)//'.l51')
        call iom50(1,0,fln(:ifln)//'.m50')
        call iom50(0,0,fln(:ifln)//'.m50')
      endif
      call FeTmpFilesClear(CommandFile)
      StartProgram=i.gt.2
9000  call CloseIfOpened(m50)
9999  return
      end
