      subroutine CrlGetSmbQ4dFromSymm(QuIn,isw,StringQ,iQuIrr)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real qrp(3),QuIn(3),QuAll(3),QuIrr(3),QuRac(3),QIPom(3),xp(3)
      integer IIrac(3),IRac(3)
      character*(*) StringQ
      character*5   GrpQ(3)
      character*1 smbq(3)
      data smbq/'a','b','g'/
      if(NDimI(KPhase).ne.1) go to 9999
      IIrac=0
      IRac=0
      do i=1,3
        QIPom(i)=1./sqrt(float(i+4))
      enddo
      do i=1,NSymm(KPhase)
        call MultM(QIPom,rm(1,i,1,KPhase),QuAll,1,3,3)
        do j=1,3
          if(abs(QuAll(j)-rm6(16,i,1,KPhase)*QIPom(j)-
     1                    rm6( 4*j,i,1,KPhase)).gt..001.and.
     2       IIrac(j).eq.0) IIrac(j)=1
        enddo
      enddo
      qrp=0.
      do j=1,3
        if(IIrac(j).eq.0) then
          QuAll(j)=QIPom(j)
        else
          QuAll(j)=QuIn(j)
          qrp(j)=QuIn(j)
        endif
      enddo
      call GetQiQr(QuAll,xp,qrp,NDimI(KPhase),isw)
      go to 1000
      entry CrlGetSmbQ4dFromQ(QuIrr,QuRac,StringQ,iQuIrr)
      call AddVek(QuIrr,QuRac,QuAll,3)
      call CopyVek(QuRac,qrp,3)
1000  do i=1,3
        if(CrSystem(KPhase).ne.CrSystemTriclinic) then
          call ToFract(qrp(i),GrpQ(i),15)
          call zhusti(GrpQ(i))
        else
          GrpQ(i)=smbq(i)
        endif
      enddo
      if(mod(CrSystem(KPhase),10).eq.CrSystemMonoclinic) then
        if(abs(QuAll(Monoclinic(KPhase))-
     1         qrp(Monoclinic(KPhase))).gt..0001) then
          GrpQ(Monoclinic(KPhase))=smbq(Monoclinic(KPhase))
        else
          do i=1,3
            if(i.ne.Monoclinic(KPhase)) GrpQ(i)=smbq(i)
          enddo
        endif
      else if(CrSystem(KPhase).eq.CrSystemOrthorhombic) then
        do i=1,3
          if(abs(QuAll(i)-qrp(i)).gt..0001) then
            GrpQ(i)=smbq(i)
            iQuIrr=i
            go to 1200
          endif
        enddo
      else
        GrpQ(3)=smbq(3)
      endif
1200  StringQ='('
      do i=1,3
        StringQ=StringQ(:idel(StringQ))//GrpQ(i)
      enddo
      StringQ=StringQ(:idel(StringQ))//')'
9999  return
      end
