      subroutine iostfree(ln,a,ki,n,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension a(*),ki(*)
      character*80 Radka
      character*8  format
      integer Exponent10
      data format/'(f15.xx)'/
      i2=0
1000  i1=i2+1
      i2=i2+6
      if(i2.gt.n) i2=n
      nn=i2-i1+1
      if(nn.le.0) go to 9999
      if(klic.eq.0) then
        read(ln,FormA,err=9000,end=9100) Radka
        k=0
        call StToReal(Radka,k,a(i1),nn,.false.,ich)
        if(ich.ne.0) go to 9200
        if(k.lt.80) then
          k=k+1
1100      if(Radka(k:k).eq.' ') then
            k=k+1
            go to 1100
          endif
          read(Radka(k:),100,err=9000,end=9100)(ki(i),i=i1,i2)
        else
          call SetIntArrayTo(ki(i1),nn,0)
        endif
      else
        Radka=' '
        id=0
        do i=i1,i2
          if(abs(a(i)).gt.0.) then
            j=max(6-Exponent10(a(i)),0)
            if(a(i).gt.0.) j=j+1
            j=min(j,6)
          else
            j=6
          endif
          write(format(6:7),'(i2)') j
          write(Cislo,format) a(i)
          call zdrcniCisla(Cislo,1)
          if(id.eq.0) then
            Radka=Cislo
          else
            Radka(id+2:)=Cislo
          endif
          id=idel(Radka)
        enddo
        write(Radka(61:),100)(max(ki(i),0),i=i1,i2)
        write(ln,FormA) Radka(:idel(Radka))
      endif
      if(i2.lt.nn) go to 1000
      go to 9999
9000  ErrFlag=1
      go to 9999
9100  ErrFlag=2
      go to 9999
9200  ErrFlag=3
9999  return
100   format(12i1)
      end
