      function Koinc(x,xa,iod,ido,dmez,dist,iswi,iswa)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension x(*),xa(3,*),y1(3),y2(3),y3(3),y3r(3),iswa(*),smp(36),
     1          rmp(9),rm6p(36),s6p(6),vt6p(:,:)
      allocatable vt6p
      allocate(vt6p(NDim(KPhase),NLattVec(KPhase)))
      do j=iod,ido
        do jsym=1,NSymm(KPhase)
          iswj=ISwSymm(jsym,iswa(j),KPhase)
          if(iswj.ne.iswi) cycle
          if(iswj.eq.iswa(j)) then
            call CopyMat(rm (1,jsym,iswj,KPhase),rmp ,3)
            call CopyMat(rm6(1,jsym,iswj,KPhase),rm6p,NDim(KPhase))
            call CopyVek( s6(1,jsym,iswj,KPhase), s6p,NDim(KPhase))
            do l=1,NLattVec(KPhase)
              call CopyVek(vt6(1,l,iswj,KPhase),vt6p(1,l),NDim(KPhase))
            enddo
          else
            call multm(zv(1,iswj,KPhase),zvi(1,iswa(j),KPhase),smp,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(smp,rm6(1,jsym,iswa(j),KPhase),rm6p,
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call MatBlock3(rm6p,rmp,NDim(KPhase))
            call multm(smp,s6(1,jsym,iswa(j),KPhase),s6p,NDim(KPhase),
     1                 NDim(KPhase),1)
            do l=1,NLattVec(KPhase)
              call multm(smp,vt6(1,l,iswa(j),KPhase),vt6p(1,l),
     1                   NDim(KPhase),NDim(KPhase),1)
            enddo
          endif
          call multm(rmp,xa(1,j),y1,3,3,1)
          call AddVek(y1,s6p,y2,3)
          do ivt=1,NLattVec(KPhase)
            do m=1,3
              y3(m)=y2(m)+vt6p(m,ivt)
              y3(m)=x(m)-y3(m)
              y3(m)=y3(m)-anint(y3(m))
            enddo
            call multm(MetTens(1,iswi,KPhase),y3,y3r,3,3,1)
            dist=sqrt(scalmul(y3,y3r))
            if(dist.le.dmez) then
              koinc=j
              go to 9999
            endif
          enddo
        enddo
      enddo
      dist=-1.
      koinc=0
9999  if(allocated(vt6p)) deallocate(vt6p)
      return
      end
