      subroutine TransTLS
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80 t80
      character*8  form
      logical OrthoN
      dimension ttt(6),tlt(6),tst(9),TrMat(9),TrMat6(36),TrMat9(81),
     1          sttt(6),stlt(6),stst(9)
      form='(6f9.6)'
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ErrFlag.ne.0) go to 9999
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      j=3
      do i=4,6
        if(abs(CellPar(i,1,KPhase)-90.).gt..01) then
          j=6
          go to 1010
        endif
      enddo
1010  id=NextQuestId()
      xqd=200.
      call FeQuestCreate(id,-1.,-1.,xqd,j,'Transformation of TLS '//
     1                   'tensors',0,LightGray,0,0)
      call FeQuestLblMake(id,xqd*.5,j-2,'For L and S tensors use :' ,
     1                    'C','N')
      tpom=xqd*.5-30.
      xpom=tpom-CrwgXd*.5
      call FeQuestCrwMake(id,tpom,j-1,xpom,j,'%Radians','C',CrwgXd,
     1                    CrwgYd,0,1)
      call FeQuestCrwOpen(1,.true.)
      tpom=xqd*.5+30.
      xpom=tpom-CrwgXd*.5
      call FeQuestCrwMake(id,tpom,j-1,xpom,j,'%Degs','C',CrwgXd,CrwgYd,
     1                    0,1)
      call FeQuestCrwOpen(2,.false.)
      if(j.gt.3) then
        call FeQuestLblMake(id,xqd*.5,1,'Transform to :' ,'C','N')
        tpom=xqd*.5-50.
        xpom=tpom-CrwgXd*.5
        call FeQuestCrwMake(id,tpom,2,xpom,3,'%Orthonormal base','C',
     1                      CrwgXd,CrwgYd,0,2)
        call FeQuestCrwOpen(3,.true.)
        tpom=xqd*.5+50.
        xpom=tpom-CrwgXd*.5
        call FeQuestCrwMake(id,tpom,2,xpom,3,'%Normal base','C',CrwgXd,
     1                      CrwgYd,0,2)
        call FeQuestCrwOpen(4,.false.)
      endif
1020  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1020
      endif
      if(ich.eq.0) then
        if(j.gt.2.and.CrwLogic(CrwFr+3)) then
          OrthoN=.false.
        else
          OrthoN=.true.
        endif
        if(CrwLogic(CrwFr)) then
          radeg=1.
        else
          radeg=1./torad
        endif
        radegq=radeg**2
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call OpenFile(lst,fln(:ifln)//'.tls','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(OrthoN) then
        call CopyMat(TrToOrtho(1,1,KPhase),TrMat,3)
      else
        call UnitMat(TrMat,3)
        j=1
        do i=1,3
          TrMat(j)=1./rcp(i,1,KPhase)
          j=j+4
        enddo
      endif
      pom=1./(sqrt(2.)*pi)
      do i=1,9
        TrMat(i)=pom*TrMat(i)
      enddo
      call SrotB (TrMat,TrMat,TrMat6)
      call SrotSS(TrMat,TrMat,TrMat9)
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        write(t80,'(''Molecule : "'',9a1)')(Molname(i)(j:j),j=1,
     1                                    idel(Molname(i))),'"'
        k=idel(t80)
        do j=1,mam(i)
          ji=j+(i-1)*mxp
          if(KTLS(i).gt.0) then
            write(t80(k+1:),'('' position #'',i1)') j
            call multm(TrMat6,tt(1,ji),ttt,6,6,1)
            call multmq(TrMat6,stt(1,ji),sttt,6,6,1)
            TextInfo(1)='           Tensor T and its e.s.d.''s in A**2'
            TextInfo(2)='   T11      T22      T33      T12      '//
     1                  'T13      T23'
            write(TextInfo(3),form) ttt
            write(TextInfo(4),form) sttt
            call multm(TrMat6,tl(1,ji),tlt,6,6,1)
            call multmq(TrMat6,stl(1,ji),stlt,6,6,1)
            TextInfo(5)='___________________________________________'//
     1                  '____________'
            TextInfo(6)='          Tensor L and its e.s.d.''s in rad**2'
            if(radeg.gt.1.) TextInfo(6)(40:42)='deg'
            TextInfo(7)='   L11      L22      L33      L12      '//
     1                  'L13      L23'
            if(radeg.gt.1.) form(6:6)='5'
            write(TextInfo(8),form)(tlt(k)*radegq,k=1,6)
            write(TextInfo(9),form)(stlt(k)*radegq,k=1,6)
            if(radeg.gt.1.) form(6:6)='6'
            call multm(TrMat9,ts(1,ji),tst,9,9,1)
            call multmq(TrMat9,sts(1,ji),stst,9,9,1)
            TextInfo(10)=TextInfo(5)
            TextInfo(11)='          Tensor S and its e.s.d.''s in rad*A'
            if(radeg.gt.1.) TextInfo(11)(40:42)='deg'
            TextInfo(12)='   S11      S12      S13      S21      '//
     1                   'S22      S23'
            write(TextInfo(13),form)(tst(l)*radeg,l=1,6)
            write(TextInfo(14),form)(stst(l)*radeg,l=1,6)
            TextInfo(15)='   S31      S32      S33'
            write(TextInfo(16),form)(tst(l)*radeg,l=7,9)
            write(TextInfo(17),form)(stst(l)*radeg,l=7,9)
            Ninfo=17
            call FeInfoOut(-1.,-1.,t80,'L')
          endif
          t80='                '//t80
          write(lst,FormA) t80(:idel(t80))
          do m=1,17
            write(lst,FormA) TextInfo(m)(:idel(TextInfo(m)))
          enddo
          write(lst,FormA)
        enddo
      enddo
      call CloseIfOpened(lst)
      Ninfo=1
      TextInfo(1)='The results are saved in the file : '//
     1            fln(:ifln)//'.tls'
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
9999  return
      end
