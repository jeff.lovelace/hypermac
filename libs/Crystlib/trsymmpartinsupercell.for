      subroutine TrSymmPartInSuperCell(xp,rc,xs,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(*),xs(3),rc(9),fc(3),fc4(3)
      ich=0
      if(NDimI(KPhase).eq.1) then
        nc1=NCommQ(1,1,KPhase)
        nc2=NCommQ(2,1,KPhase)
        nc3=NCommQ(3,1,KPhase)
      else
        nc1=NCommen(1,1,KPhase)
        nc2=NCommen(2,1,KPhase)
        nc3=NCommen(3,1,KPhase)
      endif
      do ica=1,NCommAdd(1,1,KPhase)
        do ic3=0,nc3-1
          if(NDimI(KPhase).ne.1) fc(3)=ic3
          do ic2=0,nc2-1
            if(NDimI(KPhase).ne.1) fc(2)=ic2
            do 1200ic1=0,nc1-1
              if(NDimI(KPhase).eq.1) then
                pom=ic1
                do j=1,3
                  fc(j)=VCommQ(j,1,1,KPhase)*pom+
     1                  VCommAdd(j,1,1,KPhase)*float(ica-1)
                enddo
              else
                fc(1)=ic1
              endif
              call qbyx(fc,fc4,1)
              do i=1,NDimI(KPhase)
                pom=xp(i)+fc4(i)
                if(abs(pom-anint(pom)).gt..0001) go to 1200
              enddo
              call AddVek(fc,xs,fc,3)
              call MultM(rc,fc,xs,3,3,1)
              call od0do1(xs,xs,3)
              go to 9999
1200        continue
          enddo
        enddo
      enddo
      ich=1
9999  return
      end
