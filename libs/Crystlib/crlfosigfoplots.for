      subroutine CrlFoSigFoPlots
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xpp(3),xo(3)
      character*256 Veta
      character*60 format83a
      character*17 :: Labels(4) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Sho%w it in DPlot'/)
      Tiskne=.false.
      ln=0
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,3
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtDPlot=ButtonLastMade
        endif
        if(i.le.4) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.4) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.m83','formatted','old')
      if(ErrFlag.ne.0) go to 9999
      HardCopy=0
      FMax=0.
      YMax=0.
      read(ln,FormA) Veta
      i=LocateSubstring(Veta,'e',.false.,.true.)
      if(i.gt.NDim(KPhase)*4.and.i.lt.NDim(KPhase)*4+15) then
        format83a=format83e
      else
        format83a=format83
      endif
      rewind ln
2000  read(ln,format83a,end=2100)(j,i=1,maxNDim),Icalc,Iobs,sigI,Cislo,
     1                           itw,pom,Fobs,Fcalc,sigF
      sigF=sqrt(sigF**2+(.01*Fobs)**2)
c      YMax=max(YMax,sigI)
c      FMax=max(Iobs,FMax)
      YMax=max(YMax,sigF)
      FMax=max(Fobs,FMax)
      go to 2000
2100  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        rewind ln
        xomn=0.
        xomx=FMax*1.1
        yomn=0.
        yomx=YMax*1.1
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Fobs')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Fcalc')
        xpp(3)=0.
2200    read(ln,format83a,end=2300)(j,i=1,maxNDim),Icalc,Iobs,sigI,Cislo,
     1                             itw,pom,Fobs,Fcalc,sigF
        sigF=sqrt(sigF**2+(.01*Fobs)**2)
        xpp(1)=Fobs
        xpp(2)=sigF
        call FeXf2X(xpp,xo)
        call FeCircleOpen(xo(1),xo(2),3.,White)
        go to 2200
2300    xpp(1)=0.
c        xpp(2)=0.
c        call FeXf2X(xpp,xo)
c        xp(1)=xo(1)
c        yp(1)=xo(2)
c        xpp(1)=xomx
c        xpp(2)=yomx
c        call FeXf2X(xpp,xo)
c        xp(2)=xo(1)
c        yp(2)=xo(2)
c        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
9999  return
      end
