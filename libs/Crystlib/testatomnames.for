      subroutine TestAtomNames(AtomOld,AtomNew,isfold,NaUsed,iflag)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension natp(15),isfold(*)
      character*8 AtomNew(*),AtomOld(*)
      character*80 t80
      character*256 TmpName
      logical CrwLogicQuest
      k=0
      do i=1,NaUsed
        AtomNew(i)=AtomOld(i)
        if(idel(AtomOld(i)).gt.4) k=k+1
      enddo
      if(k.le.0) then
        iflag=0
        go to 9999
      endif
      xqd=300.
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,3,'Atom names are longer '//
     1                   'than 4 characters',0,LightGray,0,0)
      call FeQuestLblMake(id,xqd*.5,1,'How to correct them:',
     1                    'C','N')
      xpom=xqd*.5-100.
      do i=1,3
        if(i.eq.1) then
          t80='use %edit'
        else if(i.eq.2) then
          t80='%truncate'
        else
          t80='%rename'
        endif
        call FeQuestCrwMake(id,xpom,2,xpom-10.,3,t80,'C',CrwXd,CrwYd,0,
     1                      1)
        call FeQuestCrwOpen(i,i.eq.3)
        xpom=xpom+100.
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(CrwLogicQuest(1)) then
          ln=NextLogicNumber()
          TmpName=fln(:ifln)//'_names.tmp'
          open(ln,file=TmpName)
          write(ln,'(8(a8,1x))')(AtomNew(i),i=1,NaUsed)
          close(ln)
          call FeEdit(TmpName)
          open(ln,file=TmpName)
2000      read(ln,FormA,end=2200) t80
          k=0
          i=1
2100      call kus(t80,k,AtomNew(i))
          j=idel(AtomNew(i))
          if(j.gt.4) then
            call FeChybne(-1.,-1.,AtomNew(i)(:j)//'" has still '//
     1                    'more than 4 characters.',' ',Warning)
            close(ln,status='Delete')
            call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            go to 1500
          endif
          if(k.lt.80) then
            i=i+1
            go to 2100
          else
            go to 2000
          endif
2200      close(ln,status='Delete')
          iflag=1
        else if(CrwLogicQuest(2)) then
          iflag=2
        else
          iflag=3
          do i=1,NAtFormula(KPhase)
            natp(i)=0
          enddo
          do i=1,NaUsed
            j=isfold(i)
            natp(j)=natp(j)+1
            write(AtomNew(i),'(i3)') natp(j)
            AtomNew(i)=AtType(j,KPhase)//AtomNew(i)(1:3)
            call zhusti(AtomNew(i))
          enddo
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) iflag=-1
9999  return
      end
