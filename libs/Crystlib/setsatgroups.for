      subroutine SetSatGroups
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer ihp(6),ihpp(6)
      integer, allocatable :: UzJe(:,:,:)
      if(NDimI(KPhase).eq.1) then
        NSatGroups=19
        HSatGroups=0
        do i=1,19
          HSatGroups(1,i)=i
        enddo
      else
        HSatGroups=0
        NSatGroups=0
        if(NDimI(KPhase).eq.2) then
          icmx=8
          ipmx=0
        else
          icmx=16
          ipmx=4
        endif
        allocate(UzJe(0:4,-4:4,-ipmx:ipmx))
        UzJe=0
        UzJe(0,0,0)=1
        ihp=0
        ihpp=0
        do ic=1,icmx
          do im=4,0,-1
            if(im.ne.0) then
              inmn=-4
            else
              inmn=0
            endif
            do in=4,inmn,-1
              if(im.ne.0.and.in.ne.0.and.NDimI(KPhase).eq.3) then
                ipmn=-4
              else
                ipmn=0
              endif
              do ip=ipmx,ipmn,-1
                if(iabs(im)+iabs(in)+iabs(ip).ne.ic.or.
     1             UzJe(im,in,ip).eq.1) cycle
                NSatGroups=NSatGroups+1
                HSatGroups(1,NSatGroups)=im
                HSatGroups(2,NSatGroups)=in
                HSatGroups(3,NSatGroups)=ip
                if(NSatGroups.ge.19) go to 1300
                ihp(4)=im
                ihp(5)=in
                ihp(6)=ip
                do is=1,NSymm(KPhase)
                  call MultMIRI(ihp,Rm6(1,is,1,KPhase),ihpp,1,
     1                          NDim(KPhase),NDim(KPhase))
                  if(ihpp(4).lt.0.or.ihpp(4).gt.4) cycle
                  if(ihpp(5).lt.inmn.or.ihpp(5).gt.4) cycle
                  if(ihpp(6).lt.ipmn.or.ihpp(6).gt.4) cycle
                  if(ihpp(4).eq.0.and.ihpp(5).lt.0) cycle
                  if(ihpp(4).eq.0.and.ihpp(5).eq.0.and.
     1               ihpp(6).le.0) cycle
                  UzJe(ihpp(4),ihpp(5),ihpp(6))=1
                enddo
               enddo
            enddo
          enddo
        enddo
      endif
1300  if(allocated(UzJe)) deallocate(UzJe)
      return
      end
