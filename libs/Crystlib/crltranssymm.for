      subroutine CrlTransSymm(TrM6,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension TrM6(*),TrM6I(36),TrM3I(9),rmp(36),xp(6)
      logical EqRV
      ich=0
      call MatInv(TrM6,TrM6I,VolRatio,NDim(KPhase))
      call MatBlock3(TrM6I,TrM3I,NDim(KPhase))
      VolRatio=abs(VolRatio)
      do j=1,NSymm(KPhase)
        call multm(TrM6I,rm6(1,j,1,KPhase),rmp,NDim(KPhase),
     1             NDim(KPhase),NDim(KPhase))
        call multm(rmp,TrM6,rm6(1,j,1,KPhase),NDim(KPhase),
     1             NDim(KPhase),NDim(KPhase))
        call multm(TrM6I,s6(1,j,1,KPhase),rmp,NDim(KPhase),
     1             NDim(KPhase),1)
        call od0do1(rmp,s6(1,j,1,KPhase),NDim(KPhase))
        call MatBlock3(rm6(1,j,1,KPhase),rm(1,j,1,KPhase),
     1                 NDim(KPhase))
      enddo
      nvt=0
      do 1100j=1,NLattVec(KPhase)
        call multm(TrM6I,vt6(1,j,1,KPhase),xp,NDim(KPhase),
     1             NDim(KPhase),1)
        call od0do1(xp,xp,NDim(KPhase))
        do k=1,nvt
          if(eqrv(xp,vt6(1,k,1,KPhase),NDim(KPhase),.001))
     1       go to 1100
        enddo
        nvt=nvt+1
        call CopyVek(xp,vt6(1,nvt,1,KPhase),NDim(KPhase))
1100  continue
      if(VolRatio.gt.1.5) then
        ik=nint(VolRatio)-1
        do i3=-ik,ik
          xp(3)=i3
          do i2=-ik,ik
            xp(2)=i2
            do 3160i1=-ik,ik
              xp(1)=i1
              call multm(TrM3I,xp,rmp,3,3,1)
              call od0do1(rmp,rmp,3)
              do j=1,nvt
                if(eqrv(rmp,vt6(1,j,1,KPhase),3,.001)) go to 3160
              enddo
              nvt=nvt+1
              call ReallocSymm(NDim(KPhase),NSymm(KPhase),nvt,
     1                         NComp(KPhase),NPhase,0)
              NLattVec(KPhase)=nvt
              call CopyVek(rmp,vt6(1,nvt,1,KPhase),3)
              if(NDimI(KPhase).gt.0)
     1          call SetRealArrayTo(vt6(4,nvt,1,KPhase),NDimI(KPhase),
     2                              0.)
3160        continue
          enddo
        enddo
      endif
      call ReallocSymm(NDim(KPhase),NSymm(KPhase),1000,NComp(KPhase),
     1                 NPhase,0)
      call EM50CompleteCentr(0,vt6,ubound(vt6,1),nvt,1000,ich)
      NLattVec(KPhase)=nvt
      return
      end
