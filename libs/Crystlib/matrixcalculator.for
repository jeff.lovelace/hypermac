      subroutine MatrixCalculator(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension RMatP(36),RMatI(36),RMatIn(*),RMatMenu(:,:),
     1          QMat(36)
      character*1000 Veta
      character*256  LastStructure,EdwStringQuest
      character*80 ZalohaM50,ZalohaM95,t80
      character*40 Menu(:)
      integer CalcMode,RolMenuSelectedQuest,TypeMatMenu(:),
     1        CalcPlus,CalcMinus,CalcDivide,CalcSave,CalcRestore,CalcCR,
     2        CalcCM
      logical EqRV0,VolatHide,EqIgCase,StructureExists,SaveMode,
     1        WizardModeIn
      allocatable Menu,RMatMenu,TypeMatMenu
      Klic=0
      NRowCalc(0)=3
      NColCalc(0)=3
      call UnitMat(RMatCalc(1,0),3)
      go to 1100
      entry MatrixCalculatorInOut(RMatIn,NRowIn,NColIn,ich)
      Klic=1
      call CopyVek(RMatIn,RMatCalc(1,0),NRowIn*NColIn)
      NRowCalc(0)=NRowIn
      NColCalc(0)=NColIn
1100  NRowQ=0
      NColQ=0
      CalcMode=0
      WizardModeIn=WizardMode
      WizardMode=.false.
      if(StructureExists(Fln)) then
!        call iom50(0,0,fln(:ifln)//'.m50')
        KRefBlockIn=KRefBlock
        KDatBlockIn=KDatBlock
        NRefBlockIn=NRefBlock
        NDatBlockIn=NDatBlock
        KPhaseIn=KPhase
        ZalohaM50='jm50'
        call CreateTmpFile(ZalohaM50,i,0)
        call FeTmpFilesAdd(ZalohaM50)
        call iom50(1,0,ZalohaM50)
        ZalohaM95='jm95'
        call CreateTmpFile(ZalohaM95,i,0)
        call FeTmpFilesAdd(ZalohaM95)
        call iom95(1,ZalohaM95)
      endif
      id=NextQuestId()
      xqd=620.
      if(Klic.eq.0) then
        il=10
      else
        il=8
      endif
      Veta='Matrix calculator'
      if(Klic.eq.0) then
        i=-1
      else
        i=0
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,i,0)
      if(Klic.eq.0) then
        il=1
        xpom=xqd*.5-80.
        Veta='# of rows'
        tpom=xpom-FeTxLength(Veta)-5.
        dpom=45.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        call FeQuestIntEdwOpen(EdwLastMade,NRowCalc(0),.false.)
        call FeQuestEudOpen(EdwLastMade,1,6,1,0.,0.,0.)
        nEdwRow=EdwLastMade
        tpom=xqd*.5+30.
        Veta='# of columns'
        xpom=tpom+FeTxLength(Veta)+5.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        if(Klic.eq.0) then
          call FeQuestIntEdwOpen(EdwLastMade,NColCalc(0),.false.)
          call FeQuestEudOpen(EdwLastMade,1,6,1,0.,0.,0.)
        endif
        nEdwCol=EdwLastMade
        il=il+1
      else
        il=0
        nEdwRow=0
        nEdwCol=0
      endif
      dpom=70.
      do i=1,6
        il=il+1
        xpom=5.
        tpom=5.
        do j=1,6
          call FeQuestEdwMake(id,tpom,il,xpom,il,' ','L',dpom,EdwYd,0)
          xpom=xpom+80.
          tpom=tpom+80.
          if(i.eq.1.and.j.eq.1) nEdwMat=EdwLastMade
        enddo
      enddo
      xpom=xpom+dpom-70.
      il=il-6
      Veta='STO'
      dpom=25.
      do i=1,10
        il=il+1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          Veta='CR'
          CalcSave=i
          nButtSave=ButtonLastMade
        else if(i.eq.2) then
          Veta='+'
          CalcCR=i
          nButtCR=ButtonLastMade
        else if(i.eq.3) then
          Veta='X'
          CalcPlus=i
          nButtPlus=ButtonLastMade
        else if(i.eq.4) then
          Veta='1/x'
          CalcMultiply=i
          nButtMultiply=ButtonLastMade
        else if(i.eq.5) then
          il=il-5
          xpom=xpom+dpom+10.
          Veta='RCL'
          nButtInvert=ButtonLastMade
        else if(i.eq.6) then
          Veta='CM'
          CalcRestore=i
          nButtRestore=ButtonLastMade
        else if(i.eq.7) then
          Veta='-'
          CalcCM=i
          nButtCM=ButtonLastMade
        else if(i.eq.8) then
          Veta='/'
          CalcMinus=i
          nButtMinus=ButtonLastMade
        else if(i.eq.9) then
          Veta='Tr'
          CalcDivide=i
          nButtDivide=ButtonLastMade
        else if(i.eq.10) then
          nButtTranspose=ButtonLastMade
        endif
      enddo
      il=il+1
      xpom=xpom-dpom-10.
      dpom=2.*dpom+10.
      Veta='='
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtCalc=ButtonLastMade
      il=il-6
      xpom=xpom+dpom+15.
      dpom=20.
      do i=1,6
        il=il+1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,
     1                         char(ichar('A')+i-1))
        if(i.eq.1) then
          Veta='%Restore from'
          NButtRegFirst=ButtonLastMade
        else if(i.eq.6) then
          nButtRegLast=ButtonLastMade
        endif
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      dpom=110.
      Posun=20.
      n=4
      xpom=xqd*.5-float(n)*.5*dpom-float(n-1)*.5*Posun
      Veta='Set %unit matrix'
      do i=1,4
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          nButtUnit=ButtonLastMade
          Veta='Set %zero matrix'
        else if(i.eq.2) then
          nButtZero=ButtonLastMade
          Veta='Read from %clipboard'
        else if(i.eq.3) then
          Veta='Import from %structure'
          nButtReadClipboard=ButtonLastMade
        else
          nButtImportFromStructure=ButtonLastMade
        endif
        xpom=xpom+dpom+Posun
      enddo
      SaveMode=.false.
      m=0
1200  do i=1,6
        nEdw=nEdwMat+(i-1)*6
        do j=1,6
          if(i.le.NRowCalc(0).and.j.le.NColCalc(0)) then
            k=i+(j-1)*NRowCalc(0)
            call FeQuestRealEdwOpen(nEdw,RMatCalc(k,0),.false.,.true.)
          else
            call FeQuestEdwClose(nEdw)
          endif
          nEdw=nEdw+1
        enddo
      enddo
      if(Klic.eq.0) then
        call FeQuestIntEdwOpen(nEdwRow,NRowCalc(0),.false.)
        call FeQuestIntEdwOpen(nEdwCol,NColCalc(0),.false.)
      endif
1300  nButt=nButtRegFirst
      do i=1,6
        if(.not.EqRV0(RMatCalc(1,i),36,.0001).or.SaveMode) then
          call FeQuestButtonOff(nButt)
        else
          call FeQuestButtonDisable(nButt)
        endif
        nButt=nButt+1
      enddo
1400  nButt=nButtCR
      do i=2,11
        if(SaveMode.or.(nButt.eq.nButtCR.and.NRowQ.le.0).or.
     1     (nButt.eq.nButtCalc.and.NRowQ.le.0)) then
          call FeQuestButtonDisable(nButt)
        else
          call FeQuestButtonOff(nButt)
        endif
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      k=0
      do i=1,NColCalc(0)
        nEdw=nEdwMat+i-1
        do j=1,NRowCalc(0)
          k=k+1
          call FeQuestRealFromEdw(nEdw,RMatCalc(k,0))
          nEdw=nEdw+6
        enddo
      enddo
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwRow) then
        call FeQuestIntFromEdw(nEdwRow,NRowNew)
        call SetRealArrayTo(RMatP,NRowNew*NColCalc(0),0.)
        k=0
        do i=1,NColCalc(0)
          do j=1,NRowCalc(0)
            k=k+1
            l=j+(i-1)*NRowNew
            RMatP(l)=RMatCalc(k,0)
          enddo
        enddo
        call CopyVek(RMatP,RMatCalc(1,0),NRowNew*NColCalc(0))
        NRowCalc(0)=NRowNew
        go to 1200
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwCol)
     1  then
        call FeQuestIntFromEdw(nEdwCol,NColNew)
        if(NColNew.gt.NColCalc(0))
     1    call SetRealArrayTo(RMatCalc(NColCalc(0)*NRowCalc(0)+1,0),
     1                        NRowCalc(0)*(NColNew-NColCalc(0)),0.)
        NColCalc(0)=NColNew
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSave)
     1  then
        SaveMode=.true.
        go to 1300
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRestore)
     1  then
        SaveMode=.false.
        go to 1300
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPlus.or.
     2         CheckNumber.eq.nButtMinus.or.
     3         CheckNumber.eq.nButtMultiply.or.
     4         CheckNumber.eq.nButtDivide.or.
     5         CheckNumber.eq.nButtCalc)) then
        IErr=0
        if(CalcMode.eq.0) then
          call CopyVek(RMatCalc(1,0),QMat,NRowCalc(0)*NColCalc(0))
          NRowQ=NRowCalc(0)
          NColQ=NColCalc(0)
        else if(CalcMode.eq.CalcPlus) then
          if(NRowQ.ne.NRowCalc(0).or.NColQ.ne.NColCalc(0)) then
            IErr=1
            go to 1600
          endif
          call AddVek(QMat,RMatCalc(1,0),QMat,NRowCalc(0)*NColCalc(0))
        else if(CalcMode.eq.CalcMinus) then
          if(NRowQ.ne.NRowCalc(0).or.NColQ.ne.NColCalc(0)) then
            IErr=1
            go to 1600
          endif
          call RealVectorToOpposite(RMatCalc(1,0),RMatP,
     1                              NRowCalc(0)*NColCalc(0))
          call AddVek(QMat,RMatP,QMat,NRowCalc(0)*NColCalc(0))
        else if(CalcMode.eq.CalcMultiply) then
          if(NColQ.ne.NRowCalc(0)) then
            IErr=1
            go to 1600
          endif
          call MultM(QMat,RMatCalc(1,0),RMatP,NRowQ,NColQ,NColCalc(0))
          NColQ=NColCalc(0)
          call CopyVek(RMatP,QMat,NRowQ*NColQ)
        else if(CalcMode.eq.CalcDivide) then
          if(NColQ.ne.NRowCalc(0)) then
            IErr=1
            go to 1600
          else if(NRowCalc(0).ne.NColCalc(0)) then
            IErr=2
            go to 1600
          endif
          call MatInv(RMatCalc(1,0),RMatI,Det,NColCalc(0))
          if(abs(Det).lt..0001) then
            IErr=3
            go to 1600
          endif
          call MultM(QMat,RMatI,RMatP,NRowQ,NColQ,NColCalc(0))
          NColQ=NColCalc(0)
          call CopyVek(RMatP,QMat,NRowQ*NColQ)
        endif
        if(CheckNumber.eq.nButtPlus) then
          CalcMode=CalcPlus
        else if(CheckNumber.eq.nButtMinus) then
          CalcMode=CalcMinus
        else if(CheckNumber.eq.nButtMultiply) then
          CalcMode=CalcMultiply
        else if(CheckNumber.eq.nButtDivide) then
          CalcMode=CalcDivide
        else if(CheckNumber.eq.nButtCalc) then
          call CopyVek(QMat,RMatCalc(1,0),NRowQ*NColQ)
          NRowCalc(0)=NRowQ
          NColCalc(0)=NColQ
          CalcMode=0
          call SetRealArrayTo(QMat,NRowQ*NColQ,0.)
          NRowQ=0
          NColQ=0
          go to 1200
        endif
        go to 1400
1600    if(IErr.ne.0) then
          write(Veta ,100) NRowCalc(0),NColCalc(0)
          write(Cislo,100) NRowQ,NColQ
          t80=' '
          if(IErr.eq.1) then
            Veta='the operation cannot be performed as the dimensions'//
     1           ' of the actual matrix '//Veta(:idel(Veta))
            t80='doesn''t correspond to that of the cummulated one '//
     2          Cislo(:idel(Cislo))//'.'
          else if(IErr.eq.2) then
            Veta='the operation cannot be performed as the actual '//
     1           'matrix having dimensions '//Veta(:idel(Veta))//
     2           ' isn''t a square matrix.'
          else if(IErr.eq.3) then
            Veta='the operation cannot be performed as the actual '//
     1           'matrix is singular.'
          endif
          call FeChybne(-1.,-1.,Veta,t80,SeriousError)
        endif
        go to 1500
      else if(CheckNumber.ge.nButtRegFirst.and.
     1        CheckNumber.le.nButtRegLast) then
        m=CheckNumber-nButtRegFirst+1
        if(SaveMode) then
          call SetRealArrayTo(RMatCalc(1,m),36,0.)
          NRowCalc(m)=NRowCalc(0)
          NColCalc(m)=NColCalc(0)
          call CopyVek(RMatCalc(1,0),RMatCalc(1,m),
     1                 NRowCalc(0)*NColCalc(0))
          SaveMode=.false.
          go to 1300
        else
          if(nEdwRow.gt.0) then
            call SetRealArrayTo(RMatCalc(1,0),36,0.)
            NRowCalc(0)=NRowCalc(m)
            NColCalc(0)=NColCalc(m)
            call CopyVek(RMatCalc(1,m),RMatCalc(1,0),
     1                   NRowCalc(0)*NColCalc(0))
            call FeQuestIntEdwOpen(nEdwRow,NRowCalc(0),.false.)
            call FeQuestIntEdwOpen(nEdwCol,NColCalc(0),.false.)
            go to 1200
          else
            call MatrixCalculatorUpdateMatrix(RMatCalc(1,0),
     1        NRowCalc(0),NColCalc(0),RMatCalc(1,m),NRowCalc(m),
     2        NColCalc(m),ich)
            if(ich.eq.0) then
              go to 1200
            else
              go to 1500
            endif
          endif
        endif
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtUnit)
     1  then
        call UnitMat(RMatCalc(1,0),NRowCalc(0))
        if(Klic.eq.0) then
          NColCalc(0)=NRowCalc(0)
          call FeQuestIntEdwOpen(nEdwCol,NColCalc(0),.false.)
        endif
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtZero)
     1  then
        call SetRealArrayTo(RMatCalc(1,0),NRowCalc(0)*NColCalc(0),0.)
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCR)
     1  then
        call SetRealArrayTo(QMat,NRowQ*NColQ,0.)
        NRowQ=0
        NColQ=0
        CalcMode=0
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCM)
     1  then
        do i=1,6
          NRowCalc(i)=0
          NColCalc(i)=0
          call SetRealArrayTo(RMatCalc(1,i),36,0.)
        enddo
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInvert)
     1  then
        if(NRowCalc(0).ne.NColCalc(0)) go to 1710
        call MatInv(RMatCalc(1,0),RMatI,Det,NRowCalc(0))
        pom=0.
        do i=1,NRowCalc(0)**2
          pom=max(pom,abs(RMatCalc(i,0)))
        enddo
        if(abs(Det).lt..0001*pom) go to 1720
        call CopyMat(RMatI,RMatCalc(1,0),NRowCalc(0))
        go to 1200
1710    call FeChybne(-1.,-1.,'the actual matrix isn''t square matrix.',
     1                ' ',SeriuosError)
        go to 1500
1720    call FeChybne(-1.,-1.,'the actual matrix is singular.',' ',
     1                SeriuosError)
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtTranspose) then
        call TrMat(RMatCalc(1,0),RMatP,NRowCalc(0),NColCalc(0))
        call CopyVek(RMatP,RMatCalc(1,0),NRowCalc(0)*NColCalc(0))
        i=NRowCalc(0)
        NRowCalc(0)=NColCalc(0)
        NColCalc(0)=i
        if(nEdwRow.gt.0) then
          call FeQuestIntEdwOpen(nEdwRow,NRowCalc(0),.false.)
          call FeQuestIntEdwOpen(nEdwCol,NColCalc(0),.false.)
        endif
        go to 1200
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtReadClipboard) then
        call FeGetClipboardLongText(Veta,n)
        n=0
        k=0
1810    call Kus(Veta,k,Cislo)
        n=n+1
        read(Cislo,'(f15.0)',err=1850) RMatP(n)
        if(k.lt.len(Veta)) go to 1810
1850    call TrMat(RMatP,RMatCalc(1,0),NRowCalc(0),NColCalc(0))
        go to 1200
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtImportFromStructure) then
        idp=NextQuestId()
        xqdp=500.
        ild=9
        Veta='Import matrix from structure'
        call FeQuestCreate(idp,-1.,-1.,xqdp,ild,Veta,0,LightGray,0,0)
        il=1
        Veta='%Structure:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpomb=100.
        dpom=xqdp-xpom-dpomb-15.
        call FeQuestEdwMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwStructure=EdwLastMade
        call FeQuestStringEdwOpen(EdwLastMade,fln)
        Veta='%Browse'
        xpom=xpom+dpom+10.
        call FeQuestButtonMake(idp,xpom,il,dpomb,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nButtBrowse=ButtonLastMade
        il=il+1
        Veta='Select %matrix to be imported:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        dpom=200.
        call FeQuestRolMenuMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          1)
        nRolMenuMatrix=RolMenuLastMade
        Veta='No matrix present in the structure'
        call FeQuestLblMake(idp,tpom,il,Veta,'L','N')
        nLblMatrix=LblLastMade
        call FeQuestLblOff(LblLastMade)
        VolatHide=.false.
        YShow=QuestYPosition(idp,10)+QuestYMin(idp)
        LastStructure=Fln
2300    NMat=0
        NMat=NMat+NSymm(1)-1
        NMat=NMat+NTwin-1
        NMat=NMat+NComp(1)-1
        do i=1,NRefBlock
          if(UseTrRefBlock(i)) NMat=NMat+1
          if(ub(1,1,i).gt.-300.) NMat=NMat+1
        enddo
        NSel=1
        if(NMat.gt.0) then
          call FeQuestLblOff(nLblMatrix)
          if(allocated(Menu)) deallocate(Menu,RMatMenu,TypeMatMenu)
          allocate(Menu(NMat),RMatMenu(36,NMat),TypeMatMenu(NMat))
          n=0
          do i=2,NSymm(1)
            n=n+1
            write(Cislo,101) i
            call Zhusti(Cislo)
            Menu(n)='Symmetry matrix '//Cislo(:idel(Cislo))
            call CopyMat(rm6(1,i,1,KPhase),RMatMenu(1,n),NDim(KPhase))
            TypeMatMenu(n)=NDim(KPhase)*10
          enddo
          do i=2,NTwin
            n=n+1
            write(Cislo,101) i
            call Zhusti(Cislo)
            Menu(n)='Twinning matrix '//Cislo(:idel(Cislo))
            call TrMat(rtw(1,i),RMatMenu(1,n),3,3)
            TypeMatMenu(n)=3*10
          enddo
          do i=2,NComp(1)
            n=n+1
            write(Cislo,101) i
            call Zhusti(Cislo)
            Menu(n)='Composite matrix '//Cislo(:idel(Cislo))
            call TrMat(ZV(1,i,KPhase),RMatMenu(1,n),NDim(KPhase),
     1                 NDim(KPhase))
            TypeMatMenu(n)=NDim(KPhase)*10
          enddo
          do i=1,NRefBlock
            if(UseTrRefBlock(i)) then
              n=n+1
              write(Cislo,101) i
              call Zhusti(Cislo)
              Menu(n)='Import transformation matrix '//
     1                Cislo(:idel(Cislo))
              call TrMat(TrRefBlock(1,i),RMatMenu(1,n),NDim(KPhase),
     1                   NDim(KPhase))
              TypeMatMenu(n)=NDim(KPhase)*10
            endif
          enddo
          do i=1,NRefBlock
            if(ub(1,1,i).gt.-300.) then
              n=n+1
              write(Cislo,101) i
              call Zhusti(Cislo)
              Menu(n)='Orientation matrix '//Cislo(:idel(Cislo))
              call CopyMat(ub(1,1,i),RMatMenu(1,n),3)
              TypeMatMenu(n)=3*10+6
            endif
          enddo
          call FeQuestRolMenuOpen(nRolMenuMatrix,Menu,NMat,1)
          if(VolatHide) call CrlMatHide
          call CrlMatShow(-1.,YShow,6,mod(TypeMatMenu(NSel),10),
     1                    RMatMenu(1,NSel),TypeMatMenu(NSel)/10)
          VolatHide=.true.
        else
          call FeQuestRolMenuClose(nRolMenuMatrix)
          call FeQuestLblOn(nLblMatrix)
        endif
2500    call FeQuestEvent(idp,ichp)
        if((CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwStructure).or.
     1     (CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse))
     2    then
          Veta=EdwStringQuest(nEdwStructure)
          if(CheckType.eq.EventButton) then
            if(VolatHide) then
              call CrlMatHide
              VolatHide=.false.
            endif
            call FeFileManager('Define name the structure',Veta,' ',1,
     1                         .true.,ichb)
            if(ichb.ne.0) go to 2500
            call FeQuestButtonOff(nButtBrowse)
          endif
          if(.not.EqIgCase(Veta,LastStructure)) then
            if(StructureExists(Veta)) then
              call iom50(0,0,Veta(:idel(Veta))//'.m50')
              call iom95(0,Veta(:idel(Veta))//'.m95')
              call FeQuestStringEdwOpen(nEdwStructure,Veta)
              LastStructure=Veta
              go to 2300
            endif
          endif
          go to 2500
        else if(CheckType.eq.EventRolMenu.and.
     1          CheckNumber.eq.nRolMenuMatrix) then
          NSel=RolMenuSelectedQuest(nRolMenuMatrix)
          if(VolatHide) call CrlMatHide
          call CrlMatShow(-1.,YShow,6,mod(TypeMatMenu(NSel),10),
     1                    RMatMenu(1,NSel),TypeMatMenu(NSel)/10)
          VolatHide=.true.
          EventType=0
          go to 2500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 2500
        endif
        if(ichp.eq.0) then
          if(Klic.eq.0) then
            call SetRealArrayTo(RMatCalc(1,0),36,0.)
            NRowCalc(0)=TypeMatMenu(NSel)/10
            NColCalc(0)=TypeMatMenu(NSel)/10
            call CopyVek(RMatMenu(1,NSel),RMatCalc(1,0),
     1                   NRowCalc(0)*NColCalc(0))
          else
            n=TypeMatMenu(NSel)/10
            call MatrixCalculatorUpdateMatrix(RMatCalc(1,0),NRowCalc(0),
     1        NColCalc(0),RMatMenu(1,NSel),n,n,ichp)
          endif
        endif
        if(allocated(Menu)) then
          if(VolatHide) call CrlMatHide
          deallocate(Menu,RMatMenu,TypeMatMenu)
        endif
        call FeQuestRemove(idp)
        if(ichp.eq.0) then
          if(Klic.eq.0) then
            call FeQuestIntEdwOpen(nEdwRow,NRowCalc(0),.false.)
            call FeQuestIntEdwOpen(nEdwCol,NColCalc(0),.false.)
          endif
          go to 1200
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0.and.Klic.eq.1) then
        do i=1,NRowIn
          if(i.gt.NRowCalc(0)) cycle
          do j=1,NColIn
            if(j.gt.NColCalc(0)) cycle
            RMatIn(i+(j-1)*NRowIn)=RMatCalc(i+(j-1)*NRowCalc(0),0)
          enddo
        enddo
      endif
      call FeQuestRemove(id)
      if(StructureExists(Fln)) then
        call iom50(0,0,ZalohaM50)
        call DeleteFile(ZalohaM50)
        call FeTmpFilesClear(ZalohaM50)
        call iom95(0,ZalohaM95)
        call DeleteFile(ZalohaM95)
        call FeTmpFilesClear(ZalohaM95)
        KRefBlock=KRefBlockIn
        KDatBlock=KDatBlockIn
        NRefBlock=NRefBlockIn
        NDatBlock=NDatBlockIn
        KPhase=KPhaseIn
      endif
      WizardMode=WizardModeIn
      return
100   format('[',i1,',',i1,']')
101   format('#',i5)
      end
