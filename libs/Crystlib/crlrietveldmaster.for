      subroutine CrlRietveldMaster(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      include 'main.cmn'
      dimension RFac(5),ila(2)
      character*256 Veta,FileName,t256
      logical FeYesNo,ExistFile,WizardModeOld,StructureExists,RefineEnd,
     1        AlreadyAllocated
      Klic=1
      call CopyFile(fln(:ifln)//'.m41',PreviousM41)
      go to 1100
      entry CrlLeBailMaster(ich)
      call CopyFile(fln(:ifln)//'.m41',PreviousM41)
      Klic=2
      go to 1100
      entry CrlSingleCrystalMaster(ich)
      Klic=3
1100  FileName=' '
      call CopyFile(fln(:ifln)//'.m40',PreviousM40)
      call CopyFile(fln(:ifln)//'.m50',PreviousM50)
      WizardModeOld=WizardMode
      AlreadyAllocated=allocated(CifKey)
      if(.not.AlreadyAllocated)
     1  allocate(CifKey(400,40),CifKeyFlag(400,40))
      call NactiCifKeys(CifKey,CifKeyFlag,0)
      call DeleteFile(Fln(:ifln)//'.dis')
      call DeleteFile(Fln(:ifln)//'.ref')
      if(Klic.ne.3) then
        if(NDimI(KPhase).gt.0) then
          if(Klic.eq.1) then
            SatFrMod(KPhase,KDatBlock)=.true.
            call SetIntArrayTo(kiPwd,MxParPwd,0)
          else if(Klic.eq.2) then
            SatFrMod(KPhase,KDatBlock)=.false.
            MMaxPwd(1,KPhase,KDatBlock)=1
          endif
        endif
        call iom41(1,0,fln(:ifln)//'.m41')
        call CopyFile(fln(:ifln)//'.m41',PreviousM41)
        call RefOpenCommands
        if(Klic.eq.1) then
          NacetlInt(nCmdDoLeBail)=0
        else if(Klic.eq.2) then
          NacetlInt(nCmdDoLeBail)=1
        endif
        call RefRewriteCommands(1)
        call CopyFile(fln(:ifln)//'.m50',PreviousM50)
      endif
      ich=0
      id=NextQuestId()
      if(WizardMode) then
        xqd=WizardLength
      else
        xqd=570.
      endif
      nButtScales=0
      nButtExtinction=0
      nButtDist=0
      nButtPlot=0
      nButtProfile=0
      if(Klic.eq.1) then
        Veta='Refinement of the nuclear structure by the Rietveld '//
     1       'method'
        ild=8
        nButt=5
      else if(Klic.eq.2) then
        Veta='Refinement of the powder profile by the le Bail algorithm'
        ild=7
        nButt=5
      else if(Klic.eq.3) then
        Veta='Refinement of the nuclear structure'
        ild=8
        nButt=7
      endif
      if(WizardMode) then
        call FeQuestTitleMake(id,Veta)
      else
        call FeQuestCreate(id,-1.,-1.,xqd,ild,Veta,0,LightGray,-1,-1)
      endif
      il=1
      tpom=5.
      call SetRealArrayTo(RFac,5,99.99)
      if(Klic.eq.3) then
        write(Veta,103)
        nRFac=5
      else
        write(Veta,101)
        nRFac=3
      endif
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblInfo=LblLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      xpom=30.
      dpom=150.
      if(Klic.eq.1.or.Klic.eq.3) then
        Veta='Edit %atom parameters'
      else if(Klic.eq.2) then
        Veta='Edit pro%file parameters'
      endif
      call SetIntArrayTo(ila,2,0)
      nButtEditM50=0
      do i=1,nButt
        il=il+1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          nButtEdit=ButtonLastMade
          Veta='Edit refinement %commands'
        else if(i.eq.2) then
          nButtRefineCommands=ButtonLastMade
          if(Klic.eq.3) then
            Veta='Edit %scale factors'
          else
            il=2
            xpom=xpom+dpom+30.
            Veta='Run %Refine'
          endif
        else if(i.eq.3) then
          if(Klic.eq.3) then
            nButtScales=ButtonLastMade
            Veta='Edit e%xtintion parameters'
          else
            ila(1)=il
            nButtRefine=ButtonLastMade
            Veta='Show %powder profile'
          endif
        else if(i.eq.4) then
          if(Klic.eq.3) then
            nButtExtinction=ButtonLastMade
            il=2
            xpom=xpom+dpom+30.
            Veta='Run %Refine'
          else
            nButtProfile=ButtonLastMade
            if(Klic.eq.2) then
              il=il-1
              xpom=xpom+dpom+30.
              Veta='Run EditM%50'
            else
              Veta='Run %Dist'
            endif
          endif
        else if(i.eq.5) then
          if(Klic.eq.3) then
            ila(1)=il
            nButtRefine=ButtonLastMade
            Veta='%Plot structure'
          else if(Klic.eq.2) then
            nButtEditM50=ButtonLastMade
          else if(Klic.eq.1) then
            ila(2)=il
            nButtDist=ButtonLastMade
          endif
        else if(i.eq.6) then
          if(Klic.eq.3) then
            nButtPlot=ButtonLastMade
            if(CallGraphic.eq.' ')
     1        call FeQuestButtonDisable(ButtonLastMade)
            Veta='Run %Dist'
          endif
        else if(i.eq.7) then
          ila(2)=il
          nButtDist=ButtonLastMade
          il=il+1
        endif
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Sav%e as'
      xpom=30.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          nButtSaveAs=ButtonLastMade
          Veta='Reco%ver parameters'
        else if(i.eq.2) then
          nButtRecover=ButtonLastMade
          Veta='Copy %in'
        else if(i.eq.3) then
          nButtCopyIn=ButtonLastMade
        endif
        xpom=xpom+dpom+30.
      enddo
      xpom=xpom-dpom-30.
      tpom=xpom-25.
      Veta='S%how listing'
      do i=1,2
        if(ila(i).gt.0) then
          call FeQuestButtonMake(id,xpom,ila(i),dpom,ButYd,Veta)
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          call FeQuestLblMake(id,tpom,ila(i),'<=','L','N')
          n=ButtonLastMade
        else
          n=0
        endif
        if(i.eq.1) then
          nButtListRefine=n
        else
          nButtListDist=n
        endif
        Veta='Sho%w listing'
      enddo
      il=ild
      if(WizardMode) then
        if(Klic.eq.2.or.Klic.eq.3)
     1    call FeQuestButtonDisable(ButtonEsc-ButtonFr+1)
        nButtFinish=0
      else
        Veta='Fi%nish'
        dpom=FeTxLengthUnder(Veta)+40.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,-10*il-1,dpom,ButYd,Veta)
        nButtFinish=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      endif
1300  Veta=fln(:ifln)//'.m70'
      WizardMode=WizardModeOld
      if(ExistFile(Veta)) then
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 1500
1320    read(ln,FormA,end=1340) Veta
        m=12
        n=15
        do i=1,nRFac
          j=LocateSubstring(Veta,CIFKey(m,n)(:idel(CIFKey(m,n))),
     1                      .false.,.true.)
          if(j.gt.0) then
            k=j+idel(CIFKey(m,n))
            call StToReal(Veta,k,RFac(i),1,.false.,ichp)
            if(ichp.ne.0) RFac(i)=.9999
            if(i.ne.1) RFac(i)=RFac(i)*100.
            go to 1320
          endif
          if(i.eq.1) then
            if(Klic.eq.3) then
              m=23
            else
              m=142
              n=20
            endif
          else if(i.eq.2) then
            if(Klic.eq.3) then
              m=38
            else
              m=143
            endif
          else if(i.eq.3) then
            m=22
          else if(i.eq.4) then
            m=40
          endif
        enddo
        go to 1320
1340    call CloseIfOpened(ln)
      endif
      if(Klic.eq.3) then
        if(RFac(1).gt.90.) then
          write(Veta,103)
        else
          write(Veta,102)(RFac(i),i=1,nRFac)
        endif
      else
        if(RFac(1).gt.90.) then
          write(Veta,101)
        else
          write(Veta,100)(RFac(i),i=1,nRFac)
        endif
      endif
      call FeQuestLblChange(nLblInfo,Veta)
      if(nButtListRefine.gt.0) then
        if(ExistFile(Fln(:ifln)//'.ref')) then
          call FeQuestButtonOff(nButtListRefine)
        else
          call FeQuestButtonDisable(nButtListRefine)
        endif
      endif
      if(nButtListDist.gt.0) then
        if(ExistFile(Fln(:ifln)//'.dis')) then
          call FeQuestButtonOff(nButtListDist)
        else
          call FeQuestButtonDisable(nButtListDist)
        endif
      endif
      if(ExistFile(Fln(:iFln)//'.s40').or.
     1   (isPowder.and.ExistFile(Fln(:iFln)//'.s41'))) then
        call FeQuestButtonOff(nButtRecover)
      else
        call FeQuestButtonDisable(nButtRecover)
      endif
1500  WizardMode=WizardModeOld
      call FeQuestEvent(id,ich)
      WizardMode=.false.
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtFinish) then
        if(.not.FeYesNo(-1.,200.,'Do you really want to close '//
     1                  'the refinement tool?',0)) go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEdit)
     1  then
        if(Klic.eq.1.or.Klic.eq.3) then
          call EM40EditParameters(7,2)
        else if(Klic.eq.2) then
          call EM40EditParameters(IdParamPowder,0)
        endif
        go to 2000
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtRefineCommands) then
        k=2
        NeverStartProgram=.true.
        call SetCommands(k)
        NeverStartProgram=.false.
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtScales)
     1  then
        call EM40EditParameters(2,0)
        go to 2000
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtExtinction) then
        call EM40EditParameters(4,0)
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRefine)
     1  then
        call NewPg(1)
        call RewriteTitle('Refine')
        call Refine(0,RefineEnd)
        if(ErrFlag.eq.0) then
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
          call CopyFile(fln(:ifln)//'.m50',PreviousM50)
          call CopyFile(fln(:ifln)//'.m40',PreviousM40)
          if(ExistPowder) call CopyFile(fln(:ifln)//'.m41',PreviousM41)
          call UpdateSummary
        else
          ErrFlag=0
        endif
        go to 1300
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtProfile)
     1  then
        call PwdPrf(0)
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPlot)
     1  then
        call CrlPlotStructure
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditM50)
     1  then
        call EditM50(ichp)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDist)
     1  then
        call NewPg(1)
        call Dist
        go to 1300
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtListRefine.or.
     2         CheckNumber.eq.nButtListDist)) then
        if(CheckNumber.eq.nButtListRefine) then
          Veta=Fln(:iFln)//'.ref'
        else
          Veta=Fln(:iFln)//'.dis'
        endif
        call FeListView(Veta,0)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRecover)
     1  then
        call CrlRecoverParameterFiles
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSaveAs)
     1  then
        Veta='Select the name where to save temporary results'
        if(FileName.eq.' ') FileName=fln(:ifln)//'_tmp'
        call FeFileManager(Veta,FileName,' ',1,.false.,ichp)
        idl=idel(FileName)
        if(ichp.eq.0.and.idl.gt.0) then
          do i=1,6
            if(i.eq.1) then
              if(Klic.eq.2) cycle
              Cislo='.m40'
            else if(i.eq.2) then
              Cislo='.m50'
            else if(i.eq.3) then
              if(.not.IsPowder) cycle
              Cislo='.m41'
            else if(i.eq.4) then
              Cislo='.dis'
            else if(i.eq.5) then
              Cislo='.ref'
            else
              Cislo='.m70'
            endif
            Veta=FileName(:idl)//Cislo
            t256=fln(:ifln)//Cislo
            if(ExistFile(t256)) then
              call CopyFile(t256,Veta)
            endif
          enddo
        endif
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCopyIn)
     1  then
        Veta='Select the name from where to read '
        call FeFileManager(Veta,FileName,' ',1,.false.,ichp)
        idl=idel(FileName)
        if(ichp.eq.0.and.idl.gt.0) then
          if(StructureExists(FileName)) then
            do i=1,6
              if(i.eq.1) then
                if(Klic.eq.2) cycle
                Cislo='.m40'
              else if(i.eq.2) then
                Cislo='.m50'
              else if(i.eq.3) then
                if(.not.IsPowder) cycle
                Cislo='.m41'
              else if(i.eq.4) then
                Cislo='.dis'
              else if(i.eq.5) then
                Cislo='.ref'
              else
                Cislo='.m70'
              endif
              Veta=FileName(:idl)//Cislo
              t256=fln(:ifln)//Cislo
              if(ExistFile(Veta)) then
                call CopyFile(Veta,t256)
              endif
            enddo
            go to 2000
          else
            Veta='the structure "'//FileName(:idl)//'" does not exist.'
            WaitTime=0
            call FeChybne(-1.,-1.,Veta,' ',Warning)
          endif
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 5000
2000  call iom50(0,0,fln(:ifln)//'.m50')
      call CopyFile(fln(:ifln)//'.m50',PreviousM50)
      call iom40(0,0,fln(:ifln)//'.m40')
      call CopyFile(fln(:ifln)//'.m40',PreviousM40)
      if(isPowder)
     1  call CopyFile(fln(:ifln)//'.m41',PreviousM41)
      go to 1300
5000  WizardMode=WizardModeOld
      if(.not.WizardMode) call FeQuestRemove(id)
      if(.not.AlreadyAllocated.and.allocated(CifKey))
     1  deallocate(CifKey,CifKeyFlag)
      return
100   format('GOF=',f6.2,20x,'Rp=',f6.2,20x,'Rwp=',f6.2)
101   format('GOF=   ---',20x,'Rp=   ---',20x,'Rwp=   ---')
102   format('GOF=',f6.2,15x,'R(obs)=',f6.2,15x,'Rw(obs)=',f6.2,15x,
     1       'R(all)=',f6.2,15x,'Rw(all)=',f6.2)
103   format('GOF=   ---',15x,'R(obs)=   ---',15x,'Rw(obs)=   ---',15x,
     1       'R(all)=   ---',15x,'Rw(all)=   ---')
104   format(10f6.2)
      end
