      subroutine SelMolecules(Text,Molecule,Brat,n,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Text,Molecule(n)
      character*256 EdwStringQuest
      character*80 t80
      character*8  At
      logical Brat(n),EqWild,lpom,SelOne,WizardModeIn
      integer SbwLnQuest,RolMenuSelectedQuest,SbwItemSelQuest,
     1        SbwItemPointerQuest
      external SelAtomsCheck,FeVoid
      SelOne=.false.
      go to 1000
      entry SelOneMolecule(Text,Molecule,ia,n,ich)
      SelOne=.true.
1000  if(n.le.0) go to 9999
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_listm.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      dl=0.
      idl=0
      do i=1,n
        write(ln,FormA) Molecule(i)(:idel(Molecule(i)))
        dl=max(dl,FeTxLength(Molecule(i))+10.)
        idl=max(idl,idel(Molecule(i)))
      enddo
      call CloseIfOpened(ln)
      xqd=400.
      if(n.gt.50) then
        nrow=11
      else if(n.gt.20) then
        nrow=10
      else if(n.gt.10) then
        nrow=5
      else
        nrow=5
      endif
      if(idl.le.8) then
        ncol=7
      else
        ncol=max(xqd/dl,1.)
      endif
      if(SelOne) then
        il=nrow+1
      else
        il=nrow+2
      endif
      WizardModeIn=WizardMode
      WizardMode=.false.
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,Text,0,LightGray,0,0)
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=nrow
      ild=nrow
      call FeQuestSbwMake(id,xpom,il,dpom,ild,ncol,CutTextFromRight,
     1                    SbwHorizontal)
      nSbw=SbwLastMade
      if(SelOne) then
        nButtAll=0
        nButtRefresh=0
      else
        il=il+2
        dpom=80.
        xpom=xqd*.5-dpom-10.
        t80='Select %all'
        call FeQuestButtonMake(id,xpom,-10*il+3,dpom,ButYd,t80)
        nButtAll=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xqd*.5+10.
        t80='%Refresh'
        call FeQuestButtonMake(id,xpom,-10*il+3,dpom,ButYd,t80)
        nButtRefresh=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      endif
1200  if(SelOne) then
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_listm.tmp')
      else
        do i=1,n
          if(Brat(i)) then
            j=1
          else
            j=0
          endif
          call FeQuestSetSbwItemSel(i,nSbw,j)
        enddo
        call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_listm.tmp')
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        lpom=CheckNumber.eq.nButtAll
        call SetLogicalArrayTo(Brat,n,lpom)
        go to 1600
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 2000
1600  call CloseIfOpened(SbwLnQuest(nSbw))
      go to 1200
2000  if(SelOne) then
        ia=SbwItemPointerQuest(nSbw)
      else
        do i=1,n
          Brat(i)=SbwItemSelQuest(i,nSbw).eq.1
        enddo
      endif
      call FeQuestRemove(id)
9999  call DeleteFile(fln(:ifln)//'_listm.tmp')
      WizardMode=WizardModeIn
      return
      end
