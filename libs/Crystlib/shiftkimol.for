      subroutine ShiftKiMol(imp,KTLSn,kmodsn,kmodxn,kmodbn,Keep)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer CumOld(8),CumNew(8)
      logical Keep
      dimension kip(:)
      allocatable kip
      equivalence (idp,CumNew(8))
      im=(imp-1)/mxp+1
      if(DelkaKiMolekuly(imp).gt.0) then
        if(KTLS(im).le.0) then
          CumOld(1)=7
        else
          CumOld(1)=28
        endif
        if(KModM(1,imp).gt.0) then
          CumOld(2)=CumOld(1)+1+2*KModM(1,imp)
        else
          CumOld(2)=CumOld(1)
        endif
        CumOld(3)=CumOld(2)+6*KModM(2,imp)
        CumOld(4)=CumOld(3)+6*KModM(2,imp)
        CumOld(5)=CumOld(4)+12*KModM(3,imp)
        CumOld(6)=CumOld(5)+12*KModM(3,imp)
        CumOld(7)=CumOld(6)+18*KModM(3,imp)
        if(CumOld(7).gt.CumOld(1)) then
          CumOld(8)=CumOld(7)+1
        else
          CumOld(8)=CumOld(7)
        endif
      else
        do i=1,8
          CumOld(i)=0
        enddo
      endif
      if(KTLSn.le.0) then
        CumNew(1)=7
      else
        CumNew(1)=28
      endif
      if(kmodsn.gt.0) then
        CumNew(2)=CumNew(1)+1+2*kmodsn
      else
        CumNew(2)=CumNew(1)
      endif
      CumNew(3)=CumNew(2)+6*kmodxn
      CumNew(4)=CumNew(3)+6*kmodxn
      CumNew(5)=CumNew(4)+12*kmodbn
      CumNew(6)=CumNew(5)+12*kmodbn
      CumNew(7)=CumNew(6)+18*kmodbn
      if(CumNew(7).gt.CumNew(1)) then
        CumNew(8)=CumNew(7)+1
      else
        CumNew(8)=CumNew(7)
      endif
      idd=idp-DelkaKiMolekuly(imp)
      if(.not.Keep) then
        allocate(kip(DelkaKiMolekuly(imp)))
        do i=1,DelkaKiMolekuly(imp)
          kip(i)=KiMol(i,imp)
        enddo
      endif
      if(idd.ne.0) then
        do i=1,NMolec
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            if(ji.gt.imp) PrvniKiMolekuly(ji)=PrvniKiMolekuly(ji)+idd
          enddo
        enddo
        PosledniKiMol=PosledniKiMol+idd
        PrvniKiAtXYZMode=PrvniKiAtXYZMode+idd
        PosledniKiAtXYZMode=PosledniKiAtXYZMode+idd
        PrvniKiAtMagMode=PrvniKiAtMagMode+idd
        PosledniKiAtMagMode=PosledniKiAtMagMode+idd
      endif
      if(.not.Keep) then
        do i=1,8
          if(CumOld(i).ne.CumNew(i)) go to 3150
        enddo
        go to 9999
3150    KiMol(1:mxdm,imp)=0
        do i=1,8
          if(i.eq.1) then
            jp=1
            k=1
            jd=min(CumOld(i),CumNew(i))
          else
            jp=CumNew(i-1)+1
            k=CumOld(i-1)+1
            jd=min(CumOld(i)-CumOld(i-1),CumNew(i)-CumNew(i-1))
          endif
          if(jd.gt.0) call CopyVekI(kip(k),KiMol(jp,imp),jd)
        enddo
      endif
      DelkaKiMolekuly(imp)=idp
9999  if(allocated(kip)) deallocate(kip)
      return
      end
