      subroutine SelNeighborAtoms(Text,DMax,AtCentr,AtSel,NMax,N,isw,
     1                            ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Text,AtCentr,AtSel(*)
      character*80 Veta
      integer SbwLnQuest,SbwItemSelQuest,SbwItemPointerQuest,UseTabsIn
      call DistFromAtom(AtCentr,DMax,isw)
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_selneigh.tmp','formatted',
     1              'unknown')
      xpom=FeTxLength('XXXXXXXX |')
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      xpom=xpom+FeTxLength('   XX.')
      call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
      xpom=xpom+FeTxLength('XXX   |')
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      do i=1,NDist
        k=ipord(i)
        write(Cislo,'(f6.3)') ddist(k)
        Veta=adist(k)(:idel(adist(k)))//Tabulator//'|'//
     1       Tabulator//Cislo(:idel(Cislo))//Tabulator//'|'//
     2       Tabulator//SymCodeDist(k)(:idel(SymCodeDist(k)))
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,7,Text,0,LightGray,0,0)
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=6
      ild=5
      call FeQuestSbwMake(id,xpom,il,dpom,ild,1,CutTextFromRight,
     1                    SbwVertical)
      il=1
      tpom=12.
      call FeQuestLblMake(id,tpom,il,'Atom','L','N')
      tpom=80.
      call FeQuestLblMake(id,tpom,il,'Distance','L','N')
      tpom=140.
      call FeQuestLblMake(id,tpom,il,'Symmetry','L','N')
      nSbw=SbwLastMade
      dpom=60.
      xpom=xqd*.5-dpom-10.
      il=7
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Refresh')
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd*.5+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Select all')
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      NButtAll=ButtonLastMade
      isel=0
2000  do i=1,NDist
        call FeQuestSetSbwItemSel(i,nSbw,isel)
      enddo
      call FeQuestSbwSelectOpen(nSbw,fln(:ifln)//'_selneigh.tmp')
3500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtRefresh) then
          isel=0
        else
          isel=1
        endif
        call CloseIfOpened(SbwLnQuest(nSbw))
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 3500
      endif
      if(ich.eq.0) then
        n=0
4000    do i=1,NDist
          if(SbwItemSelQuest(i,nSbw).eq.1.and.n.lt.Nmax) then
            k=ipord(i)
            n=n+1
            AtSel(n)=adist(k)
            j=idel(SymCodeJanaDist(k))
            if(j.gt.0) AtSel(n)=AtSel(n)(:idel(AtSel(n)))//'#'//
     1                          SymCodeJanaDist(k)(:j)
          endif
        enddo
        if(n.le.0.and.NDist.gt.0) then
          i=SbwItemPointerQuest(nSbw)
          call FeQuestSetSbwItemSel(i,nSbw,1)
          go to 4000
        endif
      else
        n=0
      endif
      call FeQuestRemove(id)
      call DeleteFile(fln(:ifln)//'_selneigh.tmp')
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
      end
