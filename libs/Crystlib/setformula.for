      subroutine SetFormula(FormulaP)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) FormulaP
      do 1900i=1,NAtFormula(KPhase)
        call UprAt(AtType(i,KPhase))
        if(i.eq.1) then
          FormulaP=AtType(i,KPhase)
        else
          FormulaP=FormulaP(:idel(FormulaP)+1)//AtType(i,KPhase)
        endif
        if(abs(AtMult(i,KPhase)-1.).lt..0001) go to 1900
        write(cislo,'(f15.3)') AtMult(i,KPhase)
        call ZdrcniCisla(Cislo,1)
        FormulaP=FormulaP(:idel(FormulaP))//Cislo(:idel(Cislo))
1900  continue
      return
      end
