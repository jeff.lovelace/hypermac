      function BinomCoef(n,m)
      include 'fepc.cmn'
      BinomCoef=factorial(n)/(factorial(m)*factorial(n-m))
      return
      end
      real function KroneckerDelta(i,j)
      include 'fepc.cmn'
      if(i.eq.j) then
        KroneckerDelta=1.
      else
        KroneckerDelta=0.
      endif
      return
      end
      subroutine SlaterDensityDer(r,zeta,n,DSto)
      dimension DSto(0:2)
      E=ExpJana(-zeta*r,ich)
      if(n.ne.0) then
        rn=r**n
      else
        rn=1.
      endif
      DSto0=rn*E
      DSto(0)=DSto0
      if(r.gt.0.) then
        fn=float(n)
        rr=1./r
        fnrr=fn*rr
        V=fnrr-zeta
        DSto1=V*DSto0
        DSto(1)=DSto1
        DSto(2)=-fnrr*rr*DSto0+V*DSto1
      else
        if(n.eq.1) then
          DSto(1)=1.
        else if(n.eq.0) then
          DSto(1)=-zeta
        else
          DSto(1)=0.
        endif
        if(n.eq.2) then
          DSto(2)=2.
        else if(n.eq.1) then
          DSto(2)=-2.*zeta
        else if(n.eq.0) then
          DSto(2)=zeta**2
        else
          DSto(2)=0.
        endif
      endif
      return
      end
      function SlaterDensity(r,zeta,n)
      if(n.ne.0) then
        rn=r**n
      else
        rn=1.
      endif
      SlaterDensity=rn*ExpJana(-zeta*r,ich)
      return
      end
      subroutine SlaterNorm(nx,z,xn)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension nx(8),z(8),xn(8)
      do l=1,8
        n=nx(l)+2
        zl=z(l)/BohrRad
        rn=zl
        do k=1,n
          rn=rn*zl/float(k)
        enddo
        xn(l)=rn
        z(l)=zl
      enddo
      return
      end
      subroutine RadialDer(x,y,z,r,DSto,Type,STOArr)
      dimension STOArr(*),DSto(0:2)
      integer Type
      STOArr(1)=DSto(0)
      if(Type.gt.0) then
        if(r.gt..001) then
          rr=1./r
          V1=rr*DSto(1)
          STOArr(2)=x*V1
          STOArr(3)=y*V1
          STOArr(4)=z*V1
          if(Type.gt.1) then
            V2=(-V1+DSto(2))*rr**2
            STOArr(5)=V1+x**2*V2
            STOArr(6)=V1+y**2*V2
            STOArr(7)=V1+z**2*V2
            STOArr( 8)=x*y*V2
            STOArr( 9)=x*z*V2
            STOArr(10)=y*z*V2
          endif
        else
          call SetRealArrayTo(STOArr(2),3,DSto(1))
          call SetRealArrayTo(STOArr(5),6,DSto(2))
        endif
      endif
      return
      end
      function Finter(x,fx,dx,n)
      dimension fx(n)
      pt=x/dx+1.
      ipt=ifix(pt)-1
      Finter=0.
      do k=1,4
        pom=1.
        do l=1,4
          if(k.ne.l) pom=pom*(pt-float(ipt+l))/float(k-l)
        enddo
        l=min(k+ipt,n)
        l=max(l,1)
        Finter=Finter+pom*fx(l)
      enddo
      return
      end
      function Finter4(f,x,xx)
      dimension x(4),f(4)
      Finter4=0.
      do k=1,4
        pom=1.
        do l=1,4
          if(k.ne.l.and.x(k).ne.x(l)) pom=pom*(xx-x(l))/(x(k)-x(l))
        enddo
        Finter4=Finter4+pom*f(k)
      enddo
      return
      end
      function SlaterFB(nl,l,sinthl,z,DerZ,DerS,ich)
      include 'fepc.cmn'
      dimension Sla(210),DerZA(190),DerSA(190)
      data sold,zold,nold/2*-9999.,-9999/
      save Sla,DerZA,DerSA
      SlaterFB=0.
      DerZ=0.
      ich=0
      n=nl+2
      s=pi4*sinthl
      if(sold.ne.s.or.z.ne.zold.or.n.gt.nold) then
        call Slater(n,s,z,sla,derza,dersa,ich)
        if(ich.ne.0) go to 9999
      endif
      if(l.lt.0) go to 9000
      i=(n*(n-1))/2+l+1
      SlaterFB=sla(i)
      DerZ=derza(i)
      DerS=dersa(i)
9000  nold=max(n,nold)
      zold=z
      sold=s
9999  return
      end
      subroutine Slater(nmax,s,z,sla,derz,ders,ich)
      include 'fepc.cmn'
      dimension sla(*),derz(*),ders(*)
      ich=0
      if(nmax.lt.1.or.nmax.gt.20) then
        ich=1
        go to 9999
      endif
      nmxp1=nmax+1
      zq=z**2
      dinv=1./(s**2+zq)
      pom1=2.*s*z*dinv
      dinv=zq*dinv
      pom2=2.*dinv
      pom3=pi4*dinv
      sla(1)=pom3
      nn=1
      do 1500n=2,nmxp1
        nn=nn+n
        fnm1 =float(n-1)
        rnm1=1./fnm1
        rn=1./float(n)
        pom=fnm1*pom3*rn
        pom3=pom1*pom
        sla(nn)=pom3
        pom4=pom2*pom
        nnp=nn-1
        sla(nnp)=pom4
        if(n.lt.3) go to 1500
        n1=nn-n
        n2=n1-n+2
        n3=2*n-3
        n4=0
        do l=n-3,0,-1
          nnp=nnp-1
          n1=n1-1
          n2=n2-1
          n3=n3-1
          n4=n4+1
          sla(nnp)=dinv*rn*
     1             (2.*fnm1*sla(n1)-float(n3*n4)*sla(n2)*rnm1)
        enddo
1500  continue
      nn=0
      do n=1,nmax
        pom2=float(n+1)/z
        pom3=pom2*pi4
        nnz=nn+n
        do 2000l=0,n-1
          nn=nn+1
          nnz=nnz+1
          pom=sla(nn)
          if(pom.ne.0.) then
            pom1=1./pom
          else
            ders(nn)=0.
            derz(nn)=0.
            go to 2000
          endif
          pom=-float(l+1)*sla(nnz+1)
          if(n.ne.0) pom=pom+float(l)*sla(nnz-1)
          ders(nn)=pom3*pom1*pom/float(2*l+1)
          derz(nn)=pom2*(1.-sla(nnz)*pom1)
2000    continue
      enddo
9999  return
      end
      subroutine SlaterOld(nmax,s,z,sla,derz,ders,ich)
      include 'fepc.cmn'
      dimension sla(*),derz(*),ders(*)
      real ninv
      ich=0
      if(nmax.lt.1.or.nmax.gt.20) then
        ich=1
        go to 9999
      endif
      nmxp1=nmax+1
      pom=z**2
      dinv=1./(s**2+pom)
      pom1=2.*s*dinv
      pom2=2.*z*dinv
      pom3=pi4*dinv*pom
      sinv=z/s
      sla(1)=pom3
      nn=3
      do n=2,nmxp1
        pom=z*float(n-1)*pom3/float(n)
        pom3=pom1*pom
        sla(nn)=pom3
        pom4=pom2*pom
        nnp=nn
        nn=nn-1
        sla(nn)=pom4
        if(n.lt.3) go to 1400
        ninv=1./float(n)
        do l=n-3,0,-1
          nn=nn-1
          nnz=nnz-1
          pom4=sinv*(pom4-float(n-l-2)*sla(nnz)*ninv)
          sla(nn)=pom4
        enddo
1400    nnz=nnp+1
        nn=nnz+n
      enddo
      nn=0
      do n=1,nmax
        pom2=float(n+1)/z
        pom3=pom2*pi4
        nnz=nn+n
        do l=0,n-1
          nn=nn+1
          nnz=nnz+1
          pom1=1./sla(nn)
          pom=-float(l+1)*sla(nnz+1)
          if(n.ne.0) pom=pom+float(l)*sla(nnz-1)
          ders(nn)=pom3*pom1*pom/float(2*l+1)
          derz(nn)=pom2*(1.-sla(nnz)*pom1)
        enddo
      enddo
9999  return
      end
      subroutine SpherHarm(h,f,n,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension h(3),f((n+1)*(n+1)),xyz(3)
      equivalence (xyz(1),x),(xyz(2),y),(xyz(3),z)
      r=sqrt(h(1)**2+h(2)**2+h(3)**2)
      if(r.lt..001) then
        call SetRealArrayTo(f,(n+1)*(n+1),0.)
        go to 9999
      endif
      if(n.ge.0) f(1)=1.
      if(n.ge.1) then
        call CopyVek(h,xyz,3)
        call VecOrtNorm(xyz,3)
        f(2)=z
        f(3)=x
        f(4)=y
      endif
      if(n.ge.2) then
        x2=x**2
        y2=y**2
        z2=z**2
        xy=x*y
        xz=x*z
        yz=y*z
        f(5)=3.*z2-1.
        f(6)=xz
        f(7)=yz
        f(8)=(x2-y2)*.5
        f(9)=xy
      endif
      if(n.ge.3) then
        x3=x2*x
        y3=y2*y
        z3=z2*z
        f(10)=5.*z3-3.*z
        pom=5.*z2-1.
        f(11)=pom*x
        f(12)=pom*y
        f(13)=2.*z*f(8)
        f(14)=2.*z*f(9)
        f(15)= x3-3.*xy*y
        f(16)=-y3+3.*xy*x
      endif
      if(n.ge.4) then
        x4=x3*x
        y4=y3*y
        z4=z3*z
        f(17)=35.*z4-30.*z2+3.
        pom=7.*z3-3.*z
        f(18)=pom*x
        f(19)=pom*y
        pom=14.*z2-2.
        f(20)=pom*f(8)
        f(21)=pom*f(9)
        f(22)=z*f(15)
        f(23)=z*f(16)
        f(24)=x4-6.*x2*y2+y4
        f(25)=4.*(x3*y-x*y3)
      endif
      if(n.ge.5) then
        x5=x4*x
        y5=y4*y
        z5=z4*z
        f(26)=63.*z5-70.*z3+15.*z
        pom=(21.*z4-14.*z2+1.)
        f(27)=pom*x
        f(28)=pom*y
        pom=6.*z3-2.*z
        f(29)=pom*f(8)
        f(30)=pom*f(9)
        pom=(9.*z2-1)
        f(31)=pom*f(15)
        f(32)=pom*f(16)
        f(33)=z*f(24)
        f(34)=z*f(25)
        f(35)=x5-10.*x3*y2+5.*x*y4
        f(36)=5.*x4*y-10.*x2*y3+y5
      endif
      if(n.ge.6) then
        x6=x5*x
        y6=y5*y
        z6=z5*z
        f(37)=231.*z6-315.*z4+105.*z2-5
        pom=33.*z5-30.*z3+5.*z
        f(38)=pom*x
        f(39)=pom*y
        pom=66.*z4-36.*z2+2.
        f(40)=pom*f(8)
        f(41)=pom*f(9)
        pom=11.*z3-3.*z
        f(42)=pom*f(15)
        f(43)=pom*f(16)
        pom=11.*z2-1.
        f(44)=pom*f(24)
        f(45)=pom*f(25)
        f(46)=z*f(35)
        f(47)=z*f(36)
        f(48)=x6-15.*x4*y2+15.*x2*y4-y6
        f(49)=6.*x5*y-20.*x3*y3+6.*x*y5
      endif
      if(n.ge.7) then
        x7=x6*x
        y7=y6*y
        z7=z6*z
        f(50)=429.*z7-693.*z5+315.*z3-35.*z
        pom=429.*z6-495.*z4+135.*z2-5.
        f(51)=pom*x
        f(52)=pom*y
        pom=286.*z5-220.*z3+30.*z
        f(53)=pom*f(8)
        f(54)=pom*f(9)
        pom=143.*z4-66.*z2+3.
        f(55)=pom*f(15)
        f(56)=pom*f(16)
        pom=13.*z3-3.*z
        f(57)=pom*f(24)
        f(58)=pom*f(25)
        pom=13.*z2-1.
        f(59)=pom*f(35)
        f(60)=pom*f(36)
        f(61)=z*f(48)
        f(62)=z*f(49)
        f(63)=x7-21.*x5*y2+35.*x3*y4-7.*x*y6
        f(64)=7.*x6*y-35.*x4*y3+21.*x2*y5-y7
      endif
      if(n.ge.8) then
        x8=x7*x
        y8=y7*y
        z8=z7*z
        f(65)=6435.*z8-12012.*z6+6930.*z4-1260.*z2+35.
        pom=715.*z7-1001.*z5+385.*z3-35.*z
        f(66)=pom*x
        f(67)=pom*y
        pom=286.*z6-286.*z4+66.*z2-2.
        f(68)=pom*f(8)
        f(69)=pom*f(9)
        pom=39.*z5-26.*z3+3.*z
        f(70)=pom*f(15)
        f(71)=pom*f(16)
        pom=65.*z4-26.*z2+1.
        f(72)=pom*f(24)
        f(73)=pom*f(25)
        pom=5.*z3-z
        f(74)=pom*f(35)
        f(75)=pom*f(36)
        pom=15.*z2-1.
        f(76)=pom*f(48)
        f(77)=pom*f(49)
        pom=z
        f(78)=pom*f(63)
        f(79)=pom*f(64)
        f(80)=x8-28.*x6*y2+70.*x4*y4-28.*x2*y6+y8
        f(81)=8.*x7*y-56.*x5*y3+56.*x3*y5-8.*x*y7
      endif
      if(Klic.eq.1) then
        do i=1,(n+1)**2
          f(i)=f(i)*Clm(i)
        enddo
      else if(Klic.eq.2) then
        do i=1,(n+1)**2
          f(i)=f(i)*Mlm(i)
        enddo
      else if(Klic.eq.3) then
        do i=1,(n+1)**2
          f(i)=f(i)*Llm(i)
        enddo
      endif
9999  return
      end
      subroutine SpherHarmDer1(h,f,n,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension h(3),f(3,(n+1)*(n+1)),xyz(3)
      equivalence (xyz(1),x),(xyz(2),y),(xyz(3),z)
      r=sqrt(h(1)**2+h(2)**2+h(3)**2)
      if(r.lt..001) then
        call SetRealArrayTo(f,3*(n+1)*(n+1),0.)
        go to 9999
      endif
      if(n.ge.0) call SetRealArrayTo(f(1,1),3,0.)
      if(n.ge.1) then
        call CopyVek(h,xyz,3)
        r=sqrt(x**2+y**2+z**2)
        pom=1./r**3
        f(1,2)=        -x*z*pom
        f(2,2)=        -y*z*pom
        f(3,2)= (x**2+y**2)*pom
        f(1,3)= (y**2+z**2)*pom
        f(2,3)=        -x*y*pom
        f(3,3)=        -x*z*pom
        f(1,4)=        -x*y*pom
        f(2,4)= (x**2+z**2)*pom
        f(3,4)=        -y*z*pom
      endif
      if(n.ge.2) then
        pom=1./r**4
        f(1,5)= 2.*x*(  -r**2+x**2+y**2-2.*z**2)*pom
        f(2,5)= 2.*y*(  -r**2+x**2+y**2-2.*z**2)*pom
        f(3,5)= 2.*z*(2.*r**2+x**2+y**2-2.*z**2)*pom
        f(1,6)= z*(1./r**2-2.*x**2*pom)
        f(2,6)=          -2.*x*y*z*pom
        f(3,6)= x*(1./r**2-2.*z**2*pom)
        f(1,7)=          -2.*x*y*z*pom
        f(2,7)= z*(1./r**2-2.*y**2*pom)
        f(3,7)= y*(1./r**2-2.*z**2*pom)
        f(1,8)= ( x/r**2-x*(x**2-y**2)*pom)
        f(2,8)= (-y/r**2-y*(x**2-y**2)*pom)
        f(3,8)=         -z*(x**2-y**2)*pom
        f(1,9)= y*(1./r**2-2.*x**2*pom)
        f(2,9)= x*(1./r**2-2.*y**2*pom)
        f(3,9)=          -2.*x*y*z*pom
      endif
      if(n.ge.3) then
        pom=1./r**5
        f(1,10)= 3.*x*z*(r**2-5.*z**2)*pom
        f(2,10)= 3.*y*z*(r**2-5.*z**2)*pom
        f(3,10)=-3.*(r-z)*(r+z)*(r**2-5.*z**2)*pom
        f(1,11)= (-3*r**2*x**2+3.*x**4-r**2*y**2+3*x**2*y**2
     1          +4.*r**2*z**2-12.*x**2*z**2)*pom
        f(2,11)= x*y*(-2.*r**2+3.*x**2+3.*y**2-12.*z**2)*pom
        f(3,11)= x*z*( 8.*r**2+3.*x**2+3.*y**2-12.*z**2)*pom
        f(1,12)= x*y*(-2.*r**2+3.*x**2+3.*y**2-12.*z**2)*pom
        f(2,12)= (-(r**2*x**2)-3.*r**2.*y**2+3*x**2*y**2+
     1           3.*y**4+4*r**2*z**2-12.*y**2*z**2)*pom
        f(3,12)= y*z*( 8.*r**2+3.*x**2+3.*y**2-12*z**2)*pom
        f(1,13)= x*z*( 2.*r**2-3.*x**2+3.*y**2)*pom
        f(2,13)= y*z*(-2.*r**2-3.*x**2+3.*y**2)*pom
        f(3,13)= (r**2-3.*z**2)*(x-y)*(x+y)*pom
        f(1,14)= 2.*y*z*(r**2-3.*x**2)*pom
        f(2,14)= 2.*x*z*(r**2-3.*y**2)*pom
        f(3,14)= 2.*x*y*(r**2-3.*z**2)*pom
        f(1,15)=-3.*(-r**2*x**2+x**4+r**2*y**2-3*x**2*y**2)*pom
        f(2,15)=-3.*x*y*(2*r**2+x**2-3*y**2)*pom
        f(3,15)=-3.*x*z*(x**2-3*y**2)*pom
        f(1,16)=-3.*x*y*(-2.*r**2+3.*x**2-y**2)*pom
        f(2,16)= 3.*(r**2*x**2-r**2*y**2-3*x**2*y**2+y**4)*pom
        f(3,16)=-3.*y*z*(3.*x**2-y**2)*pom
      endif
      if(n.ge.4) then
        pom=1./r**6
        f(1,17)= x*z**2*(60.-140.*z**2/r**2)/r**4
        f(2,17)= y*z**2*(60.-140.*z**2/r**2)/r**4
        f(3,17)= -60.*z/r**2+200.*z**3/r**4-140.*z**5*pom
        f(1,18)=z*(-3+6*x**2/r**2+7*z**2/r**2-28.*x**2*z**2/r**4)/r**2
        f(2,18)=-2.*x*y*z*(-3*r**2 + 14.*z**2)*pom
        f(3,18)=x*(-3.+27.*z**2/r**2-28.*z**4/r**4)/r**2
        f(1,19)=-2.*x*y*z*(-3.*r**2+14.*z**2)*pom
        f(2,19)=z*(-3.+6.*y**2/r**2+7.*z**2/r**2
     1             -28.*y**2*z**2/r**4)/r**2
        f(3,19)=y*(-3.+27.*z**2/r**2-28.*z**4/r**4)/r**2
        f(1,20)=4.*x*(-r**2*x**2+x**4-y**4+3.*r**2*z**2
     1                -6.*x**2*z**2+6.*y**2*z**2)*pom
        f(2,20)=-4.*y*(-x**4-r**2*y**2+y**4+3*r**2*z**2+6.*x**2*z**2
     1                 -6.*y**2*z**2)*pom
        f(3,20)=4.*z*(x-y)*(x+y)*(3.*r**2+x**2+y**2-6.*z**2)*pom
        f(1,21)=y*(-6.*x**2+8.*x**4/r**2-2.*y**2+8.*x**2*y**2/r**2
     1             +12.*z**2-48.*x**2*z**2/r**2)/r**4
        f(2,21)=x*(-2.*x**2-6.*y**2+8.*x**2*y**2/r**2+8.*y**4/r**2
     1             +12.*z**2-48.*y**2*z**2/r**2)/r**4
        f(3,21)= 8.*x*y*z*(3.*r**2+x**2+y**2-6.*z**2)*pom
        f(1,22)=z*(3.*x**2-3.*y**2)/r**4-4.*x*z*(x**3-3.*x*y**2)*pom
        f(2,22)=-2.*x*y*z*(3.*r**2+2.*x**2-6.*y**2)*pom
        f(3,22)=x*(r**2-4.*z**2)*(x**2-3.*y**2)*pom
        f(1,23)= -2.*x*y*z*(-3.*r**2+6.*x**2-2.*y**2)*pom
        f(2,23)=z*(3.*r**2*x**2-3.*r**2*y**2-12.*x**2*y**2+4.*y**4)*pom
        f(3,23)= -y*((r**2-4.*z**2)*(-3.*x**2+y**2)*pom)
        f(1,24)=-4.*x*(-(r**2*x**2)+x**4+3.*r**2*y**2-6.*x**2*y**2
     1                +y**4)*pom
        f(2,24)=-4.*y*(3.*r**2*x**2+x**4-r**2*y**2
     1                 -6.*x**2*y**2+y**4)*pom
        f(3,24)=-4.*z*(x**4-6.*x**2*y**2+y**4)*pom
        f(1,25)=-4.*y*(-3.*r**2*x**2+4.*x**4+r**2*y**2-4.*x**2*y**2)*pom
        f(2,25)= 4.*x*(r**2*x**2-3.*r**2*y**2-4.*x**2*y**2+4.*y**4)*pom
        f(3,25)=-16.*x*y*z*(x-y)*(x+y)*pom
      endif
      if(n.ge.5) then

      endif
      if(n.ge.6) then

      endif
      if(n.ge.7) then

      endif
      if(Klic.eq.1) then
        do i=1,(n+1)**2
          do j=1,3
            f(j,i)=f(j,i)*Clm(i)
          enddo
        enddo
      else if(Klic.eq.2) then
        do i=1,(n+1)**2
          do j=1,3
            f(j,i)=f(j,i)*Mlm(i)
          enddo
        enddo
      else if(Klic.eq.3) then
        do i=1,(n+1)**2
          do j=1,3
            f(j,i)=f(j,i)*Llm(i)
          enddo
        enddo
      endif
9999  return
      end
      subroutine SpherHarmDer2(h,f,n,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension h(3),f(6,(n+1)*(n+1)),xyz(3)
      equivalence (xyz(1),x),(xyz(2),y),(xyz(3),z)
      r=sqrt(h(1)**2+h(2)**2+h(3)**2)
      if(r.lt..001) then
        call SetRealArrayTo(f,6*(n+1)*(n+1),0.)
        go to 9999
      endif
      if(n.ge.0) call SetRealArrayTo(f(1,1),6,0.)
      if(n.ge.1) then
        call CopyVek(h,xyz,3)
        r=sqrt(x**2+y**2+z**2)
        pom=1./r**5
        f(1,2)=( 2.*x**2*z-y**2*z-z**3)*pom
        f(2,2)=(-x**2*z+2.*y**2*z-z**3)*pom
        f(3,2)=(  -3.*x**2*z-3.*y**2*z)*pom
        f(4,2)=                3.*x*y*z*pom
        f(5,2)=       -x*(r**2-3.*z**2)*pom
        f(6,2)=       -y*(r**2-3.*z**2)*pom
        f(1,3)=(   -3.*x*y**2-3*x*z**2)*pom
        f(2,3)=(-x**3+2.*x*y**2-x*z**2)*pom
        f(3,3)=(-x**3-x*y**2+2.*x*z**2)*pom
        f(4,3)=       -y*(r**2-3.*x**2)*pom
        f(5,3)=       -z*(r**2-3.*x**2)*pom
        f(6,3)=                3.*x*y*z*pom
        f(1,4)=( 2.*x**2*y-y**3-y*z**2)*pom
        f(2,4)=(  -3.*x**2*y-3.*y*z**2)*pom
        f(3,4)=(-x**2*y-y**3+2.*y*z**2)*pom
        f(4,4)=       -x*(r**2-3.*y**2)*pom
        f(5,4)=                3.*x*y*z*pom
        f(6,4)=       -z*(r**2-3.*y**2)*pom
      endif
      if(n.ge.2) then
        pom=1./r**6
        f(1,5)=(-2./r**2+8.*x**2/r**4+
     1         (-2./r**4+8.*x**2/r**6)*(-x**2-y**2+2.*z**2))
        f(2,5)=(-2./r**2+8.*y**2/r**4+
     1         (-2./r**4+8.*y**2/r**6)*(-x**2-y**2+2.*z**2))
        f(3,5)=(4./r**2-16.*z**2/r**4+
     1         (-2./r**4+8.*z**2/r**6)*(-x**2-y**2+2.*z**2))
        f(4,5)= 24.*x*y*z**2*pom
        f(5,5)=-12.*x*z/r**4+24.*x*z**3*pom
        f(6,5)=-12.*y*z/r**4+24.*y*z**3*pom
        f(1,6)=x*z*(-6./r**4+8.*x**2*pom)
        f(2,6)=x*z*(-2./r**4+8.*y**2*pom)
        f(3,6)=x*z*(-6./r**4+8.*z**2*pom)
        f(4,6)=(y*z*(-2.+(8.*x**2)/r**2))/r**4
        f(5,6)=(r**4-2.*r**2*x**2-2.*r**2*z**2+8.*x**2*z**2)*pom
        f(6,6)=(x*y*(-2.+(8.*z**2)/r**2))/r**4
        f(1,7)=y*z*(-2./r**4+8.*x**2*pom)
        f(2,7)=y*z*(-6./r**4+8.*y**2*pom)
        f(3,7)=y*z*(-6./r**4+8.*z**2*pom)
        f(4,7)=x*z*(-2.+(8.*y**2)/r**2)/r**4
        f(5,7)=x*y*(-2.+(8.*z**2)/r**2)/r**4
        f(6,7)=(x**4-y**4+6.*y**2*z**2-z**4)*pom
        f(1,8)=1./r**2-4.*x**2/r**4+(-1./r**4+4.*x**2*pom)*(x**2-y**2)
        f(2,8)=-1./r**2+4.*y**2/r**4+(-1./r**4+4.*y**2*pom)*(x**2-y**2)
        f(3,8)=-(r**2-4.*z**2)*(x**2-y**2)*pom
        f(4,8)=4.*x*y*(x**2-y**2)*pom
        f(5,8)=2.*x*z*(-r**2+2.*x**2-2.*y**2)*pom
        f(6,8)=2.*y*z*( r**2+2.*x**2-2.*y**2)*pom
        f(1,9)=x*y*(-6./r**4+8.*x**2*pom)
        f(2,9)=x*y*(-6./r**4+8.*y**2*pom)
        f(3,9)=x*y*(-2./r**4+8.*z**2*pom)
        f(4,9)=(r**4-2.*r**2*x**2-2.*r**2*y**2+8.*x**2*y**2)*pom
        f(5,9)=y*(-2.*z/r**4+8.*x**2*z*pom)
        f(6,9)=x*(-2.*z/r**4+8.*y**2*z*pom)
      endif
      if(n.ge.3) then
        pom=1./r**7
        f(1,10)=3.*z*(r**4-3.*r**2*x**2-5.*r**2*z**2+25.*x**2*z**2)*pom
        f(2,10)=3.*z*(r**4-3.*r**2*y**2-5.*r**2*z**2+25.*y**2*z**2)*pom
        f(3,10)=(39.*z*r**4-114.*z**3*r**2+75.*z**5)*pom
        f(4,10)=(-3.*x*y*z*(-2.*r**2+5.*x**2+5.*y**2-20.*z**2))*pom
        f(5,10)=(3.*x*(r**2*x**2+r**2*y**2-12.*r**2*z**2-5.*x**2*z**2
     1           -5.*y**2*z**2+20.*z**4))*pom
        f(6,10)=(3.*y*(r**2*x**2+r**2*y**2-12.*r**2*z**2-5.*x**2*z**2
     1          -5.*y**2*z**2+20.*z**4))*pom
        f(1,11)= x*(-6.*r**4+21.*r**2*x**2-15.*x**4+9.*r**2*y**2
     1          -15.*x**2*y**2-36.*r**2*z**2+60.*x**2*z**2)*pom
        f(2,11)= x*(-2.*r**4+3.*r**2*x**2+15.*r**2*y**2-15.*x**2*y**2
     1              -15.*y**4-12.*r**2*z**2+60.*y**2*z**2)*pom
        f(3,11)= x*(8.*r**4+3.*r**2*x**2+3.*r**2*y**2-60.*r**2*z**2
     1              -15.*x**2*z**2-15.*y**2*z**2+60.*z**4)*pom
        f(4,11)= y*(r**4-3.*r**2*x**2-15.*r**2*z**2+75.*x**2*z**2)*pom
        f(5,11)= z*(11.-33.*x**2/r**2-15.*z**2/r**2+
     1              75.*x**2*z**2/r**4)/r**3
        f(6,11)= -x*y*z*(28.*r**2+5.*x**2+5.*y**2-70.*z**2)*pom
        f(1,12)= y*(-2.*r**4+15.*r**2*x**2+3.*r**2*y**2-15.*x**4
     1              -15.*x**2*y**2-12.*r**2*z**2+60.*x**2*z**2)*pom
        f(2,12)= y*(-6.*r**4+9.*r**2*x**2+21.*r**2*y**2-15.*x**2*y**2
     1              -15.*y**4-36.*r**2*z**2+60.*y**2*z**2)*pom
        f(3,12)= y*(8.*r**4+3.*r**2*x**2+3.*r**2*y**2-60.*r**2*z**2
     1              -15.*x**2*z**2-15.*y**2*z**2+60.*z**4)*pom
        f(4,12)= 2.*x*y**2/r**5+(r**2-15.*z**2)*(x/r**5-5.*x*y**2*pom)
        f(5,12)= -28.*x*y*z/r**5-(5.*x*y*z*(r**2-15.*z**2))*pom
        f(6,12)= z*(11.-33.*y**2/r**2-15.*z**2/r**2+
     1              75.*y**2*z**2/r**4)/r**3
        f(1,13)= z*( 2.*r**4+3.*r**2*y**2-15.*r**2*x**2-15.*x**2*y**2
     1              +15.*x**4)*pom
        f(2,13)= z*(-2.*r**4-3.*r**2*x**2+15.*r**2*y**2+15.*x**2*y**2
     1              -15.*y**4)*pom
        f(3,13)= 3.*z*(-3.*r**2+5.*z**2)*(x-y)*(x+y)*pom
        f(4,13)= 15.*x*y*z*(x-y)*(x+y)*pom
        f(5,13)= x*(2.*r**4-3.*r**2*x**2+3.*r**2*y**2-6.*r**2*z**2
     1              +15.*x**2*z**2-15.*y**2*z**2)*pom
        f(6,13)= y*(-2.*r**4-3.*r**2*x**2+3.*r**2*y**2+6.*r**2*z**2
     1              +15.*x**2*z**2-15.*y**2*z**2)*pom
        f(1,14)= x*y*z*(-18.*r**2+30.*x**2)*pom
        f(2,14)= x*y*z*(-18.*r**2+30.*y**2)*pom
        f(3,14)= x*y*z*(-18.*r**2+30.*z**2)*pom
        f(4,14)= 2.*z*(r**4-3.*r**2*x**2-3.*r**2*y**2+15.*x**2*y**2)*pom
        f(5,14)= 2.*y*(r**4-3.*r**2*x**2-3.*r**2*z**2+15.*x**2*z**2)*pom
        f(6,14)= 2.*x*(r**4-3.*r**2*y**2-3.*r**2*z**2+15.*y**2*z**2)*pom
        f(1,15)= x*(6.*r**4-21.*r**2*x**2+15.*x**4+27.*r**2*y**2
     1              -45.*x**2*y**2)*pom
        f(2,15)=-3.*x*(2.*r**4+r**2*x**2-15.*r**2*y**2-5.*x**2*y**2
     1                 +15.*y**4)*pom
        f(3,15)= -3.*x*(r**2-5.*z**2)*(x**2-3.*y**2)*pom
        f(4,15)= y*(-6.*r**4+9.*r**2*x**2+15.*x**4+9.*r**2*y**2
     1              -45.*x**2*y**2)*pom
        f(5,15)= z*(-9.*x**2+15.*x**4/r**2+9.*y**2-
     1                                       (45.*x**2*y**2)/r**2)/r**5
        f(6,15)= 3.*x*y*z*(6.*r**2+5.*x**2-15.*y**2)*pom
        f(1,16)= 3.*y*(2.*r**4-15.*r**2*x**2+15.*x**4+
     1                                      r**2*y**2-5.*x**2*y**2)*pom
        f(2,16)= y*(-6.*r**4-27.*r**2*x**2+21.*r**2*y**2+45.*x**2*y**2
     1              -15.*y**4)*pom
        f(3,16)= 3.*y*(r**2-5.*z**2)*(y**2-3.*x**2)*pom
        f(4,16)= x*(6.*r**4-9.*r**2*x**2-9.*r**2*y**2+
     1                                      45.*x**2*y**2-15.*y**4)*pom
        f(5,16)= 3.*x*y*z*(-6.*r**2+15.*x**2-5.*y**2)*pom
        f(6,16)= z*(-9.*x**2+9.*y**2+45.*x**2*y**2/r**2
     1              -15.*y**4/r**2)/r**5
      endif
      if(n.ge.4) then
        pom=1./r**8
        f(1,17)=z**2*(60.-240.*x**2/r**2-140.*z**2/r**2
     1              +840.*x**2*z**2/r**4)/r**4
        f(2,17)=z**2*(60.-240.*y**2/r**2-140.*z**2/r**2
     1              +840.*y**2*z**2/r**4)/r**4
        f(3,17)=-60.*(r-z)*(r+z)*(r**4-11.*r**2*z**2+14.*z**4)*pom
        f(4,17)=x*y*z**2*(-240.+(840.*z**2)/r**2)/r**6
        f(5,17)=(40.*x*z*(3.*r**4-20.*r**2*z**2+21.*z**4))*pom
        f(6,17)=(40.*y*z*(3.*r**4-20.*r**2*z**2+21.*z**4))*pom
        f(1,18)=6.*x*z*(3.*r**4-4.*r**2*x**2-14.*r**2*z**2
     1                  +28.*x**2*z**2)*pom
        f(2,18)=x*z*(6.-24.*y**2/r**2-28.*z**2/r**2
     1               +168.*y**2*z**2/r**4)/r**4
        f(3,18)=4.*x*z*(15.*r**4-55.*r**2*z**2+42.*z**4)*pom
        f(4,18)=y*z*(6.-24.*x**2/r**2-28.*z**2/r**2+
     1             168.*x**2*z**2/r**4)/r**4
        f(5,18)=(-3.+6.*x**2/r**2+27.*z**2/r**2-108.*x**2*z**2/r**4
     1         -28.*z**4/r**4+168.*x**2*z**4/r**6)/r**2
        f(6,18)=6.*x*y*(r**4-18.*r**2*z**2+28.*z**4)*pom
        f(1,19)=y*z*(6.-24.*x**2/r**2-28.*z**2/r**2
     1               +168.*x**2*z**2/r**4)/r**4
        f(2,19)=6.*y*z*(3.*r**4-4.*r**2*y**2-14.*r**2*z**2
     1                  +28.*y**2*z**2)*pom
        f(3,19)=4.*y*z*(15.*r**4-55.*r**2*z**2+42.*z**4)*pom
        f(4,19)=x*z*(6.-24.*y**2/r**2-28.*z**2/r**2
     1               +168.*y**2*z**2/r**4)/r**4
        f(5,19)=6*x*y*(r**4-18.*r**2*z**2+28.*z**4)*pom
        f(6,19)=(-3.+6.*y**2/r**2+27.*z**2/r**2-
     1            108.*y**2*z**2/r**4-28.*z**4/r**4
     2           +168.*y**2*z**4/r**6)/r**2
        f(1,20)=-4*(3.*r**4*x**2-9.*r**2*x**4+6.*x**6+r**2*y**4
     1              -6.*x**2*y**4-3.*r**4*z**2+30.*r**2*x**2*z**2
     2              -36.*x**4*z**2-6.*r**2*y**2*z**2
     3              +36.*x**2*y**2*z**2)*pom
        f(2,20)=4*(r**2*x**4+3.*r**4*y**2-6.*x**4*y**2-9.*r**2*y**4
     1             +6.*y**6-3.*r**4*z**2-6.*r**2*x**2*z**2
     2             +30.*r**2*y**2*z**2+36.*x**2*y**2*z**2
     3             -36.*y**4*z**2)*pom
        f(3,20)=4.*(x-y)*(x+y)*(3.*r**4+r**2*x**2+r**2*y**2
     1                          -30.*r**2*z**2-6.*x**2*z**2
     2                          -6.*y**2*z**2+36.*z**4)*pom
        f(4,20)=-8.*x*y*(x-y)*(x+y)*(-2.*r**2+3*x**2+3.*y**2
     1                               -18.*z**2)*pom
        f(5,20)=x*z*( 24.*r**4-32.*r**2*x**2-24.*x**4+48.*r**2*y**2
     1               +24.*y**4-48.*r**2*z**2+144.*x**2*z**2
     2               -144.*y**2*z**2)*pom
        f(6,20)=y*z*(-24.*r**4-48.*r**2*x**2-24.*x**4+32.*r**2*y**2
     1               +24.*y**4+48.*r**2*z**2+144.*x**2*z**2
     2               -144.*y**2*z**2)*pom
        f(1,21)=x*y*(-12.*r**4+56.*r**2*x**2-48.*x**4+24.*r**2*y**2
     1               -48.*x**2*y**2-144.*r**2*z**2+288.*x**2*z**2)*pom
        f(2,21)=x*y*(-12.*r**4+24.*r**2*x**2+56.*r**2*y**2-48.*x**2*y**2
     1               -48.*y**4-144.*r**2*z**2+288.*y**2*z**2)*pom
        f(3,21)=8.*x*y*(3.*r**4+r**2*x**2+r**2*y**2-30.*r**2*z**2
     1                 -6.*x**2*z**2-6.*y**2*z**2+36.*z**4)*pom
        f(4,21)= (-2.*(r**6-2.*r**4*x**2-2.*r**4*y**2+
     1                 8.*r**2*x**2*y**2-7.*r**4*z**2
     2                 +28.*r**2*x**2*z**2+28.*r**2*y**2*z**2
     3                 -168.*x**2*y**2*z**2))*pom
        f(5,21)=y*z*(32.-128.*x**2/r**2-56.*z**2/r**2
     1               +336.*x**2*z**2/r**4)/r**4
        f(6,21)=x*z*(32.-128.*y**2/r**2-56.*z**2/r**2
     1               +336.*y**2*z**2/r**4)/r**4
        f(1,22)=x*z*( 6.*r**4-28.*r**2*x**2+24.*x**4+36.*r**2*y**2
     1               -72.*x**2*y**2)*pom
        f(2,22)=x*z*(-6.*r**4-4.*r**2*x**2+60.*r**2*y**2+24.*x**2*y**2
     1               -72.*y**4)*pom
        f(3,22)=12.*x*z*(-r**2+2.*z**2)*(x**2-3.*y**2)*pom
        f(4,22)=-6.*y*z*(r**4-2.*r**2*x**2-4.*x**4-2.*r**2*y**2
     1                    +12.*x**2*y**2)*pom
        f(5,22)=((3.*x**2-3.*y**2)/r**4-4.*z**2*(3.*x**2-3.*y**2)/r**6
     1           -4.*x*(x**3-3.*x*y**2)/r**6
     2           +24.*x*z**2*(x**3-3.*x*y**2)*pom)
        f(6,22)=x*y*(-6.*r**4-4.*r**2*x**2+12.*r**2*y**2+24.*r**2*z**2
     1               +24.*x**2*z**2-72.*y**2*z**2)*pom
        f(1,23)=y*z*(6.*r**4-60.*r**2*x**2+72.*x**4+4.*r**2*y**2
     1               -24.*x**2*y**2)*pom
        f(2,23)=y*z*(-6.*r**4-36.*r**2*x**2+28.*r**2*y**2+72.*x**2*y**2
     1               -24.*y**4)*pom
        f(3,23)=-12.*y*z*(-r**2+2.*z**2)*(-3.*x**2+y**2)*pom
        f(4,23)= 6.*x*z*(r**4-2.*r**2*x**2-2*r**2*y**2+12.*x**2*y**2
     1                   -4.*y**4)*pom
        f(5,23)=x*y*(6.*r**4-12.*r**2*x**2+4.*r**2*y**2-24.*r**2*z**2
     1               +72.*x**2*z**2-24.*y**2*z**2)*pom
        f(6,23)=(3.*x**2-3.*y**2-12.*x**2*y**2/r**2+4.*y**4/r**2
     1           -12.*x**2*z**2/r**2+12.*y**2*z**2/r**2
     1           +72.*x**2*y**2*z**2/r**4-24.*y**4*z**2/r**4)/r**4
        f(1,24)=(12.*r**4*x**2-36.*r**2*x**4+24.*x**6-12.*r**4*y**2
     1           +120.*r**2*x**2*y**2-144.*x**4*y**2-4.*r**2*y**4
     2           +24.*x**2*y**4)*pom
        f(2,24)=(-12.*r**4*x**2-4.*r**2*x**4+12.*r**4*y**2
     1           +120.*r**2*x**2*y**2+24.*x**4*y**2-36.*r**2*y**4
     2           -144.*x**2*y**4+24.*y**6)*pom
        f(3,24)=-4.*(r**2-6.*z**2)*(x**4-6.*x**2*y**2+y**4)*pom
        f(4,24)=8.*x*y*(-3.*r**4+4.*r**2*x**2+3.*x**4
     1                  +4.*r**2*y**2-18.*x**2*y**2+3*y**4)*pom
        f(5,24)=x*z*(-16.*r**2*x**2+24.*x**4+48.*r**2*y**2
     1               -144.*x**2*y**2+24.*y**4)*pom
        f(6,24)=y*z*(48.*r**2*x**2+24.*x**4-16.*r**2*y**2
     1               -144.*x**2*y**2+24.*y**4)*pom
        f(1,25)=8.*x*y*(3.*r**4-14.*r**2*x**2+12.*x**4+6.*r**2*y**2
     1                  -12.*x**2*y**2)*pom
        f(2,25)=-8.*x*y*(3.*r**4-14.*r**2*y**2+12.*y**4+6.*r**2*x**2
     1                   -12.*x**2*y**2)*pom
        f(3,25)=-16.*x*y*(r**2-6.*z**2)*(x-y)*(x+y)*pom
        f(4,25)= 4.*(x-y)*(x+y)*(3.*r**4-4.*r**2*x**2-4.*r**2*y**2
     1          +24.*x**2*y**2)*pom
        f(5,25)= 16.*y*z*(-3.*r**2*x**2+6.*x**4+r**2*y**2
     1                    -6.*x**2*y**2)*pom
        f(6,25)=-16.*x*z*(r**2*x**2-3.*r**2*y**2-6.*x**2*y**2
     1                    +6.*y**4)*pom
      endif
      if(n.ge.5) then

      endif
      if(n.ge.6) then

      endif
      if(n.ge.7) then

      endif
      if(Klic.eq.1) then
        do i=1,(n+1)**2
          do j=1,6
            f(j,i)=f(j,i)*Clm(i)
          enddo
        enddo
      else if(Klic.eq.2) then
        do i=1,(n+1)**2
          do j=1,6
            f(j,i)=f(j,i)*Mlm(i)
          enddo
        enddo
      else if(Klic.eq.3) then
        do i=1,(n+1)**2
          do j=1,6
            f(j,i)=f(j,i)*Llm(i)
          enddo
        enddo
      endif
9999  return
      end
      subroutine SpherHarmN(h,f,fd1,fd2,lmax,Type,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer Type
      real h(3),f(*),fd1(3,*),fd2(6,*)
      dimension fm(-7:7),fmd1(2,-7:7),fmd2(3,-7:7)
      XX=h(1)
      YY=h(2)
      ZZ=h(3)
      r=XX**2+YY**2+ZZ**2
      lmax1q=(lmax+1)**2
      if(r.lt..00001) then
        call SetRealArrayTo(f,lmax1q,0.)
        if(Type.gt.0) then
          call SetRealArrayTo(fd1,3*lmax1q,0.)
          if(Type.gt.0) then
             call SetRealArrayTo(fd2,6*lmax1q,0.)
          endif
        endif
        go to 9999
      endif
      r=1./r
      pom=sqrt(r)
      x=XX*pom
      y=YY*pom
      z=ZZ*pom
      if(Type.gt.0) then
        pom3=pom*r
        XXYYQ=XX**2+YY**2
        XXZZQ=XX**2+ZZ**2
        YYZZQ=YY**2+ZZ**2
        XXpom3=XX*pom3
        YYpom3=YY*pom3
        ZZpom3=ZZ*pom3
        DxDh=YYZZQ*pom3
        DxDk=-XX*YYpom3
        DxDl=-XX*ZZpom3
        DyDh=DxDk
        DyDk=XXZZQ*pom3
        DyDl=-YY*ZZpom3
        DzDh=DxDl
        DzDk=DyDl
        DzDl=XXYYQ*pom3
        if(Type.gt.1) then
          pom5=pom3*r
          XXpom5=XX*pom5
          YYpom5=YY*pom5
          ZZpom5=ZZ*pom5
          D2xDh2=-3.*YYZZQ*XXpom5
          D2xDk2=-XXpom3*(1.-3.*YY**2*r)
          D2xDl2=-XXpom3*(1.-3.*ZZ**2*R)
          D2xDhDk=YYpom3*(2.-3.*YYZZQ*r)
          D2xDhDl=ZZpom3*(2.-3.*YYZZQ*r)
          D2xDkDl=3.*XX*YY*ZZpom5
          D2yDh2=-YYpom3*(1.-3.*XX**2*r)
          D2yDk2=-3.*XXZZQ*YYpom5
          D2yDl2=-YYpom3*(1.-3.*ZZ**2*r)
          D2yDhDk=D2xDk2
          D2yDhDl=D2xDkDl
          D2yDkDl=ZZpom3*(2.-3.*XXZZQ*r)
          D2zDh2=-ZZpom3*(1.-3.*XX**2*r)
          D2zDk2=-ZZpom3*(1.-3.*YY**2*r)
          D2zDl2=-3.*XXYYQ*ZZpom5
          D2zDhDk=D2xDkDl
          D2zDhDl=D2xDl2
          D2zDkDl=D2yDl2
        endif
      endif
      Pmm=1.
      f(1)=Pmm
      Fmm=0.
      fd1(3,1)=0.
      Smm=0.
      fd2(3,1)=0.
      pom=max((1.-z)*(1.+z),.000001)
      arg=sqrt(pom)
      sarg1=-1./pom
      farg=z*sarg1
      sarg2=farg**2
      mm=0
      n=-1
      do m=0,lmax
        n=n+2
        mm=mm+n
        if(m.eq.1) mm=mm-1
        if(m.eq.0) then
          Pmmp=1.
          if(Type.gt.0) then
            Fmmp=0.
            if(Type.gt.1) Smmp=0.
          endif
          fact1=1.
        else
          Pmmp=-Pmmp*fact1*arg
          Pmm=Pmmp
          f(mm)=Pmm
          if(Type.gt.0) then
            Fmmp=Pmmp*float(m)*farg
            Fmm=Fmmp
            fd1(3,mm)=Fmmp
            if(Type.gt.1) then
              Smmp=Pmmp*float(m)*(sarg1+float(m-2)*sarg2)
              Smm=Smmp
              fd2(3,mm)=Smmp
            endif
          endif
          fact1=fact1+2.
        endif
        np=n
        mp=mm
        if(m.le.lmax-1) then
          mp=mp+np
          pom=z*fact1
          Pmm1=pom*Pmm
          f(mp)=Pmm1
          if(Type.gt.0) then
            Fmm1=pom*Fmm+fact1*Pmm
            fd1(3,mp)=Fmm1
            if(Type.gt.1) then
              Smm1=pom*Smm+2.*fact1*Fmm
              fd2(3,mp)=Smm1
            endif
          endif
          np=np+2
        endif
        fact2=fact1+2.
        fact3=fact1
        fact4=2.
        do m2=m+2,lmax
          mp=mp+np
          rfact4=1./fact4
          pom=z*fact2
          Pmm2=(pom*Pmm1-fact3*Pmm)*rfact4
          f(mp)=Pmm2
          if(Type.gt.0) then
            Fmm2=(pom*Fmm1+fact2*Pmm1-fact3*Fmm)*rfact4
            fd1(3,mp)=Fmm2
            if(Type.gt.1) then
              Smm2=(pom*Smm1+2.*fact2*Fmm1-fact3*Smm)*rfact4
              fd2(3,mp)=Smm2
            endif
          endif
          Pmm=Pmm1
          Pmm1=Pmm2
          if(Type.gt.0) then
            Fmm=Fmm1
            Fmm1=Fmm2
            if(Type.gt.1) then
              Smm=Smm1
              Smm1=Smm2
            endif
          endif
          fact2=fact2+2.
          fact3=fact3+1.
          fact4=fact4+1.
          np=np+2
        enddo
      enddo
      fm(0)=1.
      fmd1(1,0)=0.
      fmd1(2,0)=0.
      fmd2(1,0)=0.
      fmd2(2,0)=0.
      fmd2(3,0)=0.
      r=x**2+y**2
      if(r.lt..00001) then
        do l=-lmax,lmax
          fm(l)=0.
          fmd1(1,l)=0.
          fmd1(2,l)=0.
          fmd2(1,l)=0.
          fmd2(2,l)=0.
          fmd2(3,l)=0.
        enddo
        fm(0)=1.
        go to 3500
      endif
      r=1./r
      pom=sqrt(r)
      pom3=pom*r
      sfi=y*pom
      cfi=x*pom
      if(Type.gt.0) then
        DsfiDx=-x*y*pom3
        DsfiDy= x**2*pom3
        DcfiDx= y**2*pom3
        DcfiDy=DsfiDx
        if(Type.gt.1) then
          pom5=pom3*r
          D2sfiDx2=-y*pom3*(1.-3.*x**2*r)
          D2sfiDy2=-3.*x**2*y*pom5
          D2sfiDxDy=-x*pom3*(1.-3.*y**2*r)
          D2cfiDx2=-3.*x*y**2*pom5
          D2cfiDy2=D2sfiDxDy
          D2cfiDxDy=D2sfiDx2
        endif
      endif
      cfin1=1.
      sfin1=0.
      Dcfin1Dcfi=0.
      Dcfin1Dsfi=0.
      Dsfin1Dcfi=0.
      Dsfin1Dsfi=0.
      D2cfin1Dcfi2=0.
      D2cfin1Dsfi2=0.
      D2cfin1DsfiDcfi=0.
      D2sfin1Dcfi2=0.
      D2sfin1Dsfi2=0.
      D2sfin1DsfiDcfi=0.
      do i=1,lmax
        cfin=cfin1*cfi-sfin1*sfi
        sfin=sfin1*cfi+cfin1*sfi
        fm( i)= cfin
        fm(-i)= sfin
        if(Type.gt.0) then
          DcfinDcfi=Dcfin1Dcfi*cfi-Dsfin1Dcfi*sfi+cfin1
          DcfinDsfi=Dcfin1Dsfi*cfi-Dsfin1Dsfi*sfi-sfin1
          DsfinDcfi=Dsfin1Dcfi*cfi+Dcfin1Dcfi*sfi+sfin1
          DsfinDsfi=Dsfin1Dsfi*cfi+Dcfin1Dsfi*sfi+cfin1
          fmd1(1, i)= DcfinDcfi*DcfiDx+DcfinDsfi*DsfiDx
          fmd1(1,-i)= DsfinDcfi*DcfiDx+DsfinDsfi*DsfiDx
          fmd1(2, i)= DcfinDcfi*DcfiDy+DcfinDsfi*DsfiDy
          fmd1(2,-i)= DsfinDcfi*DcfiDy+DsfinDsfi*DsfiDy
          if(Type.gt.1) then
            D2cfinDcfi2=D2cfin1Dcfi2*cfi+2.*Dcfin1Dcfi-D2sfin1Dcfi2*sfi
            D2cfinDsfi2=D2cfin1Dsfi2*cfi-2.*Dsfin1Dsfi-D2sfin1Dsfi2*sfi
            D2cfinDsfiDcfi=D2cfin1DsfiDcfi*cfi-D2sfin1DsfiDcfi*sfi
     1                    -Dsfin1Dcfi+Dcfin1Dsfi
            D2sfinDcfi2=D2sfin1Dcfi2*cfi+2.*Dsfin1Dcfi+D2cfin1Dcfi2*sfi
            D2sfinDsfi2=D2sfin1Dsfi2*cfi+2.*Dcfin1Dsfi+D2cfin1Dsfi2*sfi
            D2sfinDsfiDcfi=D2sfin1DsfiDcfi*cfi+D2cfin1DsfiDcfi*sfi
     1                    +Dcfin1Dcfi+Dsfin1Dsfi
            fmd2(1, i)= DcfinDcfi*D2cfiDx2+DcfinDsfi*D2sfiDx2
     1                 +D2cfinDcfi2*DcfiDx**2+D2cfinDsfi2*DsfiDx**2
     2                 +2.*D2cfinDsfiDcfi*DcfiDx*DsfiDx
            fmd2(1,-i)= DsfinDcfi*D2cfiDx2+DsfinDsfi*D2sfiDx2
     1                 +D2sfinDcfi2*DcfiDx**2+D2sfinDsfi2*DsfiDx**2
     2                 +2.*D2sfinDsfiDcfi*DcfiDx*DsfiDx
            fmd2(2, i)= DcfinDcfi*D2cfiDy2+DcfinDsfi*D2sfiDy2
     1                 +D2cfinDcfi2*DcfiDy**2+D2cfinDsfi2*DsfiDy**2
     2                 +2.*D2cfinDsfiDcfi*DcfiDy*DsfiDy
            fmd2(2,-i)= DsfinDcfi*D2cfiDy2+DsfinDsfi*D2sfiDy2
     1                 +D2sfinDcfi2*DcfiDy**2+D2sfinDsfi2*DsfiDy**2
     2                 +2.*D2sfinDsfiDcfi*DcfiDy*DsfiDy
            fmd2(3, i)= DcfinDcfi*D2cfiDxDy+DcfinDsfi*D2sfiDxDy
     1                 +D2cfinDcfi2*DcfiDx*DcfiDy
     2                 +D2cfinDsfi2*DsfiDx*DsfiDy
     3                 +D2cfinDsfiDcfi*(DcfiDx*DsfiDy+DcfiDy*DsfiDx)
            fmd2(3,-i)= DsfinDcfi*D2cfiDxDy+DsfinDsfi*D2sfiDxDy
     1                 +D2sfinDcfi2*DcfiDx*DcfiDy
     2                 +D2sfinDsfi2*DsfiDx*DsfiDy
     3                 +D2sfinDsfiDcfi*(DcfiDx*DsfiDy+DcfiDy*DsfiDx)
          endif
        endif
        if(mod(i,2).eq.1) then
          fm( i)=-fm( i)
          fm(-i)=-fm(-i)
          if(Type.gt.0) then
            fmd1(1, i)=-fmd1(1, i)
            fmd1(1,-i)=-fmd1(1,-i)
            fmd1(2, i)=-fmd1(2, i)
            fmd1(2,-i)=-fmd1(2,-i)
            if(Type.gt.1) then
              fmd2(1, i)=-fmd2(1, i)
              fmd2(1,-i)=-fmd2(1,-i)
              fmd2(2, i)=-fmd2(2, i)
              fmd2(2,-i)=-fmd2(2,-i)
              fmd2(3, i)=-fmd2(3, i)
              fmd2(3,-i)=-fmd2(3,-i)
            endif
          endif
        endif
        sfin1=sfin
        cfin1=cfin
        if(Type.gt.0) then
          Dsfin1Dcfi=DsfinDcfi
          Dsfin1Dsfi=DsfinDsfi
          Dcfin1Dcfi=DcfinDcfi
          Dcfin1Dsfi=DcfinDsfi
          if(Type.gt.1) then
            D2sfin1Dcfi2=D2sfinDcfi2
            D2sfin1Dsfi2=D2sfinDsfi2
            D2sfin1DsfiDcfi=D2sfinDsfiDcfi
            D2cfin1Dcfi2=D2cfinDcfi2
            D2cfin1Dsfi2=D2cfinDsfi2
            D2cfin1DsfiDcfi=D2cfinDsfiDcfi
          endif
        endif
      enddo
3500  k=0
      do l=0,lmax
        do n=1,2*l+1
          k=k+1
          if(n.eq.1.or.mod(n,2).eq.0) then
            fk=f(k)
            dfk=fd1(3,k)
            d2fk=fd2(3,k)
            m=n/2
          else
            m=-m
          endif
          fmm=fm(m)
          dfmdx=fmd1(1,m)
          dfmdy=fmd1(2,m)
          d2fmdx2=fmd2(1,m)
          d2fmdy2=fmd2(2,m)
          d2fmdxdy=fmd2(3,m)
          f(k)=fk*fmm
          DfmmDh=dfmdx*DxDh+dfmdy*DyDh
          DfkDh=dfk*DzDh
          DfmmDk=dfmdx*DxDk+dfmdy*DyDk
          DfkDk=dfk*DzDk
          DfmmDl=dfmdx*DxDl+dfmdy*DyDl
          DfkDl=dfk*DzDl
          if(Type.gt.0) then
            fd1(1,k)=fk*DfmmDh+fmm*DfkDh
            fd1(2,k)=fk*DfmmDk+fmm*DfkDk
            fd1(3,k)=fk*DfmmDl+fmm*DfkDl
            if(Type.gt.1) then
              fd2(1,k)=fk*(dfmdx*D2xDh2+dfmdy*D2yDh2+
     1                     d2fmdx2*DxDh**2+d2fmdy2*DyDh**2+
     2                     2.*d2fmdxdy*DxDh*DyDh)+
     3                 fmm*(dfk*D2zDh2+d2fk*DzDh**2)+2.*DfmmDh*DfkDh
              fd2(2,k)=fk*(dfmdx*D2xDk2+dfmdy*D2yDk2+
     1                     d2fmdx2*DxDk**2+d2fmdy2*DyDk**2+
     2                     2.*d2fmdxdy*DxDk*DyDk)+
     3                 fmm*(dfk*D2zDk2+d2fk*DzDk**2)+2.*DfmmDk*DfkDk
              fd2(3,k)=fk*(dfmdx*D2xDl2+dfmdy*D2yDl2+
     1                     d2fmdx2*DxDl**2+d2fmdy2*DyDl**2+
     2                     2.*d2fmdxdy*DxDl*DyDl)+
     3                 fmm*(dfk*D2zDl2+d2fk*DzDl**2)+2.*DfmmDl*DfkDl
              fd2(4,k)=fk*(dfmdx*D2xDhDk+dfmdy*D2yDhDk+
     1                     d2fmdx2*DxDh*DxDk+d2fmdy2*DyDh*DyDk+
     2                     d2fmdxdy*(DxDh*DyDk+DxDk*DyDh))+
     3                 fmm*(dfk*D2zDhDk+d2fk*DzDh*DzDk)+
     4                 DfmmDh*DfkDk+DfmmDk*DfkDh
              fd2(5,k)=fk*(dfmdx*D2xDhDl+dfmdy*D2yDhDl+
     1                     d2fmdx2*DxDh*DxDl+d2fmdy2*DyDh*DyDl+
     2                     d2fmdxdy*(DxDh*DyDl+DxDl*DyDh))+
     3                 fmm*(dfk*D2zDhDl+d2fk*DzDh*DzDl)+
     4                 DfmmDh*DfkDl+DfmmDl*DfkDh
              fd2(6,k)=fk*(dfmdx*D2xDkDl+dfmdy*D2yDkDl+
     1                     d2fmdx2*DxDk*DxDl+d2fmdy2*DyDk*DyDl+
     2                     d2fmdxdy*(DxDk*DyDl+DxDl*DyDk))+
     3                 fmm*(dfk*D2zDkDl+d2fk*DzDk*DzDl)+
     4                 DfmmDk*DfkDl+DfmmDl*DfkDk
            endif
          endif
        enddo
      enddo
6000  if(Klic.eq.2) then
        do i=1,lmax1q
          pom=MlmC(i)
          f(i)=f(i)*pom
          if(Type.gt.0) then
            do j=1,3
              fd1(j,i)=fd1(j,i)*pom
            enddo
            if(Type.gt.1) then
              do j=1,6
                fd2(j,i)=fd2(j,i)*pom
              enddo
            endif
          endif
        enddo
      else if(Klic.eq.3) then
        do i=1,lmax1q
          pom=LlmC(i)
          f(i)=f(i)*pom
          if(Type.gt.0) then
            do j=1,3
              fd1(j,i)=fd1(j,i)*pom
            enddo
            if(Type.gt.1) then
              do j=1,6
                fd2(j,i)=fd2(j,i)*pom
              enddo
            endif
          endif
        enddo
      endif
9999  return
      end
      function SlaterFBOld(nl,l,sinthl,z,DerZ,DerS,ich)
      include 'fepc.cmn'
c      include 'basic.cmn'
      dimension fact(0:8)
      data fact/1.,1.,2.,6.,24.,120.,720.,5040.,40320./
      SlaterFBOld=0.
      DerZ=0.
      ich=0
      n=nl+2
      s=pi4*sinthl
      den=s**2+z**2
      if(den.le.0.) go to 9999
      fn=z**(n+1)/(fact(n)*den**n)
      if(z.ne.0.) then
        fnz=float(n+1)*fn/z
      else
        if(n.gt.1) then
          fnz=0.
        else
          fnz=1./den
        endif
      endif
      fnz=fnz-2.*z*float(n)*fn/den
      fns=-2.*s*float(n)*fn/den
      s2=s**2
      z2=z**2
      sz=s*z
      if(n.eq.1) then
        fnn=1.
        SlaterFBOld=1.
        DerZ=0.
        DerS=0.
      else if(n.eq.2) then
        fnn=2.
        if(l.eq.0) then
          SlaterFBOld=z
          DerZ=1.
          DerS=0.
        else
          SlaterFBOld=s
          DerZ=0.
          DerS=1.
        endif
      else if(n.eq.3) then
        if(l.eq.0) then
          fnn=2.
          SlaterFBOld=3.*z2-s2
          DerZ=6.*z
          DerS=-2.*s
        else if(l.eq.1) then
          fnn=8.
          SlaterFBOld=sz
          DerZ=s
          DerS=z
        else
          fnn=8.
          SlaterFBOld=s2
          DerZ=0.
          DerS=2.*s
        endif
      else if(n.eq.4) then
        if(l.eq.0) then
          fnn=24.
          SlaterFBOld=z*z2-z*s2
          DerZ=3.*z2-s2
          DerS=-2.*sz
        else if(l.eq.1) then
          fnn=8.
          SlaterFBOld=5.*s*z2-s*s2
          DerZ=10.*sz
          DerS=5.*z2-3.*s2
        else if(l.eq.2) then
          fnn=48.
          SlaterFBOld=s2*z
          DerZ=s2
          DerS=2.*sz
        else
          fnn=48.
          SlaterFBOld=s*s2
          DerZ=0.
          DerS=3.*s2
        endif
      else if(n.eq.5) then
        if(l.eq.0) then
          fnn=24.
          SlaterFBOld=5.*z2*z2-10.*s2*z2+s2*s2
          DerZ=20.*(z*z2-z*s2)
          DerS=-20.*s*z2+4.*s*s2
        else if(l.eq.1) then
          fnn=48.
          pom1=5.*z2-3.*s2
          SlaterFBOld=sz*pom1
          DerZ=s*(pom1+10.*z2)
          DerS=z*(pom1-6.*s2)
        else if(l.eq.2) then
          fnn=48.
          pom1=7.*z2-s2
          SlaterFBOld=s2*pom1
          DerZ=14.*s2*z
          DerS=(s+s)*(pom1-s2)
        else if(l.eq.3) then
          fnn=384.
          SlaterFBOld=s2*sz
          DerZ=s2*s
          DerS=3.*s2*z
        else
          fnn=384.
          SlaterFBOld=s2*s2
          DerZ=0.
          DerS=4.*s*s2
        endif
      else if(n.eq.6) then
        if(l.eq.0) then
          fnn=240.
          pom1=3.*z2**2-10.*s2*z2+3.*s2**2
          SlaterFBOld=z*pom1
          DerZ=pom1+z2*(12.*z2-20.*s2)
          DerS=sz*(-20.*z2+12.*s2)
        else if(l.eq.1) then
          fnn=48.
          pom1=35.*z2**2-42.*s2*z2+3.*s2**2
          SlaterFBOld=s*pom1
          DerZ=sz*(140.*z2-84.*s2)
          DerS=pom1+s2*(-84.*z2+12.*s2)
        else if(l.eq.2) then
          fnn=384.
          pom1=s2*z
          pom2=7.*z2-3.*s2
          SlaterFBOld=pom1*pom2
          DerZ=s2*pom2+14.*z*pom1
          DerS=(sz+sz)*pom2-pom1*6.*s
        else if(l.eq.3) then
          fnn=384.
          pom1=s*s2
          pom2=9.*z2-s2
          SlaterFBOld=pom1*pom2
          DerZ=pom1*18.*z
          DerS=3.*s2*pom2-(s+s)*pom1
        else if(l.eq.4) then
          fnn=3840.
          DerZ=s2**2
          SlaterFBOld=z*DerZ
          DerS=4.*s2*sz
        else
          fnn=3840.
          SlaterFBOld=s*s2**2
          DerZ=0.
          DerS=5.*s2**2
        endif
      else if(n.eq.7) then
        if(l.eq.0) then
          fnn=720.
          pom1=z2**2
          pom2=7.*z2-35.*s2
          pom3=s2**2
          pom4=21.*z2-s2
          SlaterFBOld=pom1*pom2+pom3*pom4
          DerZ=z*(4.*z2*pom2+14.*pom1+42.*pom3)
          DerS=s*(-70.*pom1+4.*s2*pom4-pom3-pom3)
        else if(l.eq.1) then
          fnn=1920.
          pom1=7.*z2**2-14.*s2*z2+3.*s2**2
          SlaterFBOld=sz*pom1
          DerZ=s*pom1+sz*28.*z*(z2-s2)
          DerS=z*pom1+sz*s*(-28.*z2+12.*s2)
        else if(l.eq.2) then
          fnn=1152.
          pom1=21.*z2**2-18.*s2*z2+s2**2
          SlaterFBOld=s2*pom1
          DerZ=s2*z*(84.*z2-36.*s2)
          DerS=(s+s)*pom1+s*s2*(-36.*z2+4.*s2)
        else if(l.eq.3) then
          fnn=11520.
          pom1=s2*sz
          pom2=3.*z2-s2
          SlaterFBOld=pom1*pom2
          DerZ=s*s2*pom2+6.*z*pom1
          DerS=3.*s2*z*pom2-pom1*(s+s)
        else if(l.eq.4) then
          fnn=3840.
          pom1=s2**2
          pom2=11.*z2-s2
          SlaterFBOld=pom1*pom2
          DerZ=pom1*22*z
          DerS=s*(4.*s2*pom2-pom1-pom1)
        else if(l.eq.5) then
          fnn=46080.
          DerZ=s*s2**2
          SlaterFBOld=z*DerZ
          DerS=5.*SlaterFBOld/s
        else
          fnn=46080.
          SlaterFBOld=s2**3
          DerZ=0.
          DerS=6.*SlaterFBOld/s
        endif
      else if(n.eq.8) then
        if(l.eq.0) then
          fnn=40320.
          pom1=z2**2
          pom2=z2-7.*s2
          pom3=s2**2
          pom4=7.*z2-s2
          SlaterFBOld=z*(pom1*pom2+pom3*pom4)
          DerZ=SlaterFBOld/z+z2*(4.*z2*pom2+pom1+pom1+14.*pom3)
          DerS=sz*(-14.*pom1+4.*s2*pom4-pom3-pom3)
        else if(l.eq.1) then
          fnn=5760.
          pom1=z2**2
          pom2=21.*z2-63.*s2
          pom3=s2**2
          pom4=27.*z2-s2
          SlaterFBOld=s*(pom1*pom2+pom3*pom4)
          DerZ=sz*(4.*z2*pom2+42.*pom1+54.*pom3)
          DerS=SlaterFBOld/s+s2*(-126.*pom1+4.*s2*pom4-pom3-pom3)
        else if(l.eq.2) then
          fnn=11520.
          pom1=s2*z
          pom2=21.*z2**2-30.*s2*z2+5.*s2**2
          SlaterFBOld=pom1*pom2
          DerZ=s2*pom2+pom1*z*(84.*z2-60.*s2)
          DerS=(sz+sz)*pom2+pom1*s*(-60.*z2+20.*s2)
        else if(l.eq.3) then
          fnn=11520.
          pom1=s*s2
          pom2=33.*z2**2-22.*s2*z2+s2**2
          SlaterFBOld=pom1*pom2
          DerZ=pom1*z*(132.*z2-44.*s2)
          DerS=3.*s2*pom2+pom1*s*(-44.*z2+4.*s2)
        else if(l.eq.4) then
          fnn=46080.
          pom1=s2**2*z
          pom2=11.*z2-3.*s2
          SlaterFBOld=pom1*pom2
          DerZ=SlaterFBOld/z+pom1*22.*z
          DerS=4.*SlaterFBOld/s-pom1*6.*s
        else if(l.eq.5) then
          fnn=46080.
          pom1=s*s2**2
          pom2=13.*z2-s2
          SlaterFBOld=pom1*pom2
          DerZ=pom1*26.*z
          DerS=5.*SlaterFBOld/s-pom1*(s+s)
        else if(l.eq.6) then
          fnn=645120.
          DerZ=s2**3
          SlaterFBOld=DerZ*z
          DerS=6.*SlaterFBOld/s
        else
          fnn=645120.
          pom1=s2**3
          SlaterFBOld=s*pom1
          DerZ=0.
          DerS=7.*pom1
        endif
      else
        ich=1
        go to 9999
      endif
      DerZ=pi4*(SlaterFBOld*fnz+DerZ*fn)*fnn
      DerS=pi4**2*(SlaterFBOld*fns+DerS*fn)*fnn
      SlaterFBOld=pi4*SlaterFBOld*fn*fnn
      if(SlaterFBOld.ne.0.) then
        DerZ=DerZ/SlaterFBOld
        DerS=DerS/SlaterFBOld
      else
        DerZ=0.
        DerS=0.
      endif
9999  return
      end
      subroutine gauleg(x1,x2,x,w,n)
      dimension x(n),w(n)
      double precision eps
      parameter(eps=3.d-14)
      double precision p1,p2,p3,xl,xm,z,z1,dj
      xl=0.5d0*(x2-x1)
      xm=0.5d0*(x2+x1)
      m=(n+1)/2
      do i=1,m
        z=dcos(3.1415926536d0*(float(i)-.25d0)/(float(n)+.5d0))
1000    p1=1.d0
        p2=0.d0
        do j=1,n
          dj=float(j)
          p3=p2
          p2=p1
          p1=((2.d0*dj-1.d0)*z*p2-(dj-1.d0)*p3)/dj
        enddo
        pp=float(n)*(z*p1-p2)/(z**2-1.d0)
        z1=z
        z=z1-p1/pp
        if(dabs(z-z1).gt.eps) go to 1000
        x(i)=xm-xl*z
        x(n-i+1)=xm+xl*z
        w(i)=2.*xl/((1.-z**2)*pp**2)
        w(n-i+1)=w(i)
      enddo
      return
      end
      function ExpJana(x,ich)
      if(abs(x).lt.88.) then
        ExpJana=exp(x)
        ich=0
      else if(x.gt.0.) then
        ExpJana=3.40e+38
        ich=1
      else
        ExpJana=1.18e-38
        ich=2
      endif
      return
      end
      function ExpJanaNew(x,ich)
      real*16 dx,fdx
      dx=x
      ich=0.
c      if(dx.gt.7000.) then
c        ExpJanaNew=1.d+60
c        ich=1
c        go to 9999
c      else if(dx.lt.-7000.) then
c        ExpJana=0.
c        ich=2
c        go to 9999
c      endif
      fdx=qexp(dx)
      if(fdx.gt.1.d+60) then
        fdx=1.d+60
        ich=1
      else if(fdx.lt.1.d-60) then
        fdx=0.
        ich=2
      endif
      ExpJanaNew=fdx
9999  return
      end
      function ExpJanaOld(x,ich)
      double precision dx,fdx
      dx=x
      ich=0.
      if(dx.gt.700.) then
        ExpJanaOld=1.e+30
        ich=1
        go to 9999
      else if(dx.lt.-700.) then
        ExpJanaOld=0.
        ich=2
        go to 9999
      endif
      fdx=dexp(dx)
      if(fdx.gt.1.e+30) then
        fdx=1.e+30
        ich=1
      else if(fdx.lt.1.e-30) then
        fdx=0.
        ich=2
      endif
      ExpJanaOld=fdx
9999  return
      end
      integer function Exponent10(x)
      pom=abs(x)
      Exponent10=0
      if(pom.le.0.) go to 9999
1000  if(pom.ge.10.) then
        pom=pom*.1
        Exponent10=Exponent10+1
        go to 1000
      endif
2000  if(pom.lt.1.) then
        pom=pom*10.
        Exponent10=Exponent10-1
        go to 2000
      endif
9999  return
      end
      subroutine triangl(a,b,n,m,nx)
      dimension a(n,m),b(n)
      jp=1
      do i=1,m
        do j=jp,n
          if(abs(a(j,i)).gt.0.0001) go to 1100
          a(j,i)=0.
        enddo
        cycle
1100    if(j.ne.jp) then
          do k=i,m
            p=a(j,k)
            a(j,k)=a(jp,k)
            a(jp,k)=p
          enddo
          p=b(j)
          b(j)=b(jp)
          b(jp)=p
        endif
        p=1./a(jp,i)
        if(p.ne.1.) then
          a(jp,i)=1.
          do k=i+1,m
            a(jp,k)=p*a(jp,k)
          enddo
          b(jp)=p*b(jp)
        endif
        do l=jp+1,n
          p=a(l,i)
          a(l,i)=0.
          if(p.ne.0.) then
            do k=i+1,m
              a(l,k)=a(l,k)-a(jp,k)*p
            enddo
            b(l)=b(l)-b(jp)*p
          endif
        enddo
        jp=jp+1
        if(jp.gt.m) go to 2500
      enddo
2500  do i=n,2,-1
        do jp=1,m
          if(abs(a(i,jp)).gt.0.0001) go to 3100
          a(i,jp)=0.
        enddo
        cycle
3100    do l=i-1,1,-1
          p=a(l,jp)
          a(l,jp)=0.
          if(p.ne.0.) then
            do k=jp+1,m
              a(l,k)=a(l,k)-a(i,k)*p
            enddo
            b(l)=b(l)-b(i)*p
          endif
        enddo
      enddo
      nx=0
      do i=1,n
        do j=1,m
          if(abs(a(i,j)).gt..001) then
            nx=nx+1
            exit
          endif
        enddo
      enddo
9999  return
      end
      subroutine TrianglC(a,b,n,m,nx)
      complex a(n,m),b(n),p
      jp=1
      do 2000i=1,m
        do j=jp,n
          if(abs(a(j,i)).gt..001) go to 1100
          a(j,i)=(0.,0.)
        enddo
        go to 2000
1100    if(j.ne.jp) then
          do k=i,m
            p=a(j,k)
            a(j,k)=a(jp,k)
            a(jp,k)=p
          enddo
          p=b(j)
          b(j)=b(jp)
          b(jp)=p
        endif
        p=1./a(jp,i)
        if(p.ne.1.) then
          a(jp,i)=1.
          do k=i+1,m
            a(jp,k)=p*a(jp,k)
          enddo
          b(jp)=p*b(jp)
        endif
        do l=jp+1,n
          p=a(l,i)
          a(l,i)=(0.,0.)
          if(p.ne.0.) then
            do k=i+1,m
              a(l,k)=a(l,k)-a(jp,k)*p
            enddo
            b(l)=b(l)-b(jp)*p
          endif
        enddo
        jp=jp+1
        if(jp.gt.m) go to 2500
2000  continue
2500  do i=n,2,-1
        do jp=1,m
          if(abs(a(i,jp)).gt..001) go to 3100
          a(i,jp)=(0.,0.)
        enddo
        cycle
3100    do l=i-1,1,-1
          p=a(l,jp)
          a(l,jp)=(0.,0.)
          if(p.ne.(0.,0.)) then
            do k=jp+1,m
              a(l,k)=a(l,k)-a(i,k)*p
            enddo
            b(l)=b(l)-b(i)*p
          endif
        enddo
      enddo
      nx=0
      do i=1,n
        do j=1,m
          if(abs(a(i,j)).gt..001) then
            nx=nx+1
            exit
          endif
        enddo
      enddo
9999  return
      end
      subroutine Gausz(a,m,n,r,q)
      integer r,s,q(*),z(:)
      double precision eps,g,h,b(:)
      complex a(m,n),hc
      allocatable z,b
      allocate(z(m),b(m))
      eps=0.1e-5
      do i=1,m
        g=0.
        do j=1,n
          h=max(g,dble(cabs(a(i,j))))
        enddo
        if(g.ne.0) then
          b(i)=1.0/g
        else
          b(i)=1.
        endif
      enddo
      r=0
      do s=1,n
        h=0.
        do ii=r+1,m
          g=abs(a(ii,s))*b(ii)
          if(g.gt.h) then
            h=g
            k=ii
          endif
        enddo
        if(h.lt.eps) cycle
        r=r+1
        q(r)=s
        if(k.ne.r) then
          h=b(r)
          b(r)=b(k)
          b(k)=h
          do j=s,n
            hc=a(r,j)
            a(r,j)=a(k,j)
            a(k,j)=hc
          enddo
        endif
        hc=(1.0,0.0)/a(r,s)
        do j=s,n
          a(r,j)=hc*a(r,j)
        enddo
        do i=r+1,m
          do j=n,s,-1
            a(i,j)=a(i,j)-a(i,s)*a(r,j)
            p1=real(a(i,j))
            p2=aimag(a(i,j))
            if(abs(p1).lt.1.e-5) p1=0.
            if(abs(p2).lt.1.e-5) p2=0.
            a(i,j)=cmplx(p1,p2)
          enddo
        enddo
      enddo
      call SetIntArrayTo(z,n,0)
      do i=1,r
        j=q(i)
        z(j)=1
      enddo
      do i=r,2,-1
        s=q(i)
        do l=s+1,n
          if(z(l).ne.0) cycle
          do k=i-1,1,-1
            a(k,l)=a(k,l)-a(k,s)*a(i,l)
            p1=real(a(k,l))
            p2=aimag(a(k,l))
            if(abs(p1).lt.1.e-5) p1=0.0
            if(abs(p2).lt.1.e-5) p2=0.0
            a(k,l)=cmplx(p1,p2)
          enddo
        enddo
        do k=i-1,1,-1
          a(k,s)=(0.0,0.0)
        enddo
      enddo
      deallocate(z,b)
      return
      end
      subroutine GauszGetOut(a,m,n,r,q,dim)
      integer dim,p(:),q(n),r
      complex a(m,n),b(:,:)
      real lambda
      allocatable b,p
      allocate(b(n,n),p(n))
      call SetComplexArrayTo(b,n*n,(0.,0.))
      call SetIntArrayTo(q(r+1),n-r,0)
      k=1
      l=0
      do j=1,n
        if(q(k).eq.j) then
          k=k+1
        else
          l=l+1
          p(l)=j
        endif
      enddo
      do j=1,n-r
        j2=p(j)
        b(j2,j)=(-1.0,0.0)
        do i=1,r
          i2=q(i)
          b(i2,j)=a(i,j2)
        enddo
      enddo
      lambda = 0.
      do i=1,dim
        lambda=lambda+abs(b(i,1))**2
      enddo
      lambda=sqrt(lambda)
      do i=1,dim
        do j=1,dim
          a(i,j)=b((i-1)*dim+j,1)/lambda
        enddo
      enddo
      deallocate(b,p)
      return
      end
      subroutine ScalDivMatC(AMat,BMat,n,m,ScalDiv,ich)
      complex AMat(n,m),ScalDiv
      dimension BMat(n,m)
      logical EqCV0,EqRV0,UzHoMame
      ich=0
      ScalDiv=0.
      if(EqCV0(AMat,n*m,.001)) then
        go to 9999
      else if(EqRV0(BMat,n*m,.001)) then
        go to 9000
      else
        UzHoMame=.false.
        do i=1,m
          do j=1,n
            if(abs(AMat(i,j)).lt..001) then
              if(abs(BMat(i,j)).lt..001) then
                cycle
              else
                go to 9000
              endif
            else
              if(abs(BMat(i,j)).lt..001) then
                go to 9000
              else
                if(UzHoMame) then
                  if(abs(AMat(i,j)-ScalDiv*BMat(i,j)).gt..001)
     1              go to 9000
                else
                  ScalDiv=AMat(i,j)/cmplx(BMat(i,j))
                  UzHoMame=.true.
                endif
              endif
            endif
          enddo
        enddo
      endif
      go to 9999
9000  ich=1
9999  return
      end
      subroutine matinv(a,b,det,n)
      dimension a(n,n),b(n,n),ipiv(:),indxr(:),indxc(:)
      allocatable ipiv,indxr,indxc
      allocate(ipiv(n),indxr(n),indxc(n))
      det=1.
      do i=1,n
        do j=1,n
          b(i,j)=a(i,j)
        enddo
      enddo
      do j=1,n
        ipiv(j)=0
      enddo
      do i=1,n
        big=0.
        do j=1,n
          if(ipiv(j).ne.1) then
            do k=1,n
              if(ipiv(k).eq.0) then
                if(abs(B(j,k)).ge.big) then
                  big=abs(b(J,K))
                  irow=j
                  icol=k
                endif
              else if(ipiv(k).gt.1) then
                det=0.
                go to 9999
              endif
            enddo
          endif
        enddo
        ipiv(icol)=ipiv(icol)+1
        if(irow.ne.icol) then
          do l=1,n
            dum=b(irow,l)
            b(irow,l)=b(icol,l)
            b(icol,l)=dum
          enddo
          det=-det
        endif
        indxr(i)=irow
        indxc(i)=icol
        if(b(icol,icol).eq.0.) then
          det=0.
          go to 9999
        endif
        det=det*b(icol,icol)
        pivinv=1./b(icol,icol)
        b(icol,icol)=1.
        do l=1,n
          b(icol,l)=b(icol,l)*pivinv
        enddo
        do ll=1,n
          if(ll.ne.icol) then
            dum=b(ll,icol)
            b(ll,icol)=0.
            do l=1,n
              b(ll,l)=b(ll,l)-b(icol,l)*dum
            enddo
          endif
        enddo
      enddo
      do l=n,1,-1
        if(indxr(l).ne.indxc(l)) then
          do k=1,n
            dum=b(k,indxr(l))
            b(k,indxr(l))=b(k,indxc(l))
            b(k,indxc(l))=dum
          enddo
        endif
      enddo
9999  deallocate(ipiv,indxr,indxc)
      return
      end
      subroutine ql(a,z,d,n)
      dimension e(20),d(n),a(*),z(*)
      data tol,eps/.0000001,.0000001/
      imd=1-2*mod(n,2)
      nb=n*(n-1)+1
      k=0
      do i=1,n
        do j=1,n
          k=k+1
          if(i.eq.j) then
            z(k)=1.
          else
            z(k)=0.
          endif
        enddo
      enddo
      iz=n*(n+1)/2
      l=n
      do ip=3,n
        i=l
        l=l-1
        h=0.
        iz=iz-i
        do k=1,l
          ind=iz+k
          f=a(ind)
          d(k)=f
          h=h+f**2
        enddo
        if(h.le.tol) then
          e(i)=0.
          h=0.
          go to 890
        endif
        g=-sqrt(h)
        if(f.lt.0.) g=-g
        e(i)=g
        h=h-f*g
        rh=1./h
        dl=f-g
        d(l)=dl
        ind=iz+l
        a(ind)=dl
        f=0.
        ind=1
        do j=1,l
          g=0.
          jk=ind
          do k=1,l
            g=g+a(jk)*d(k)
            id=1
            if(j.le.k) id=k
            jk=jk+id
          enddo
          g=g*rh
          e(j)=g
          f=f+g*d(j)
          ind=ind+j
        enddo
        hh=f*.5*rh
        jk=0
        do j=1,l
          f=d(j)
          g=e(j)-hh*f
          e(j)=g
          do k=1,j
            jk=jk+1
            a(jk)=a(jk)-f*e(k)-g*d(k)
          enddo
        enddo
890     jk=iz+i
        d(i)=a(jk)
        a(jk)=h
      enddo
      d(1)=a(1)
      e(2)=a(2)
      d(2)=a(3)
      b=0.
      f=0.
      do l=1,n
        ln=l*n-n
        if(l.eq.n) go to 1800
        l1=l+1
        h=eps*(abs(d(l))+abs(e(l1)))
        if(b.lt.h) b=h
        do m=l1,n
          if(abs(e(m)).le.b) go to 1200
        enddo
        m=n+1
1200    m=m-1
        if(m.eq.l) go to 1800
        ml=m-l
        mn=m*n
1250    g=d(l)
        el=e(l1)
        dl=(d(l1)-g)/(2.*el)
        h=abs(dl)+sqrt(dl**2+1.)
        if(dl.lt.0.) h=-h
        dl=el/h
        d(l)=dl
        h=g-dl
        do i=l1,n
          d(i)=d(i)-h
        enddo
        f=f+h
        dl=d(m)
        c=1.
        s=0.
        ind=mn
        do jk=1,ml
          ind=ind-n
          i=m-jk
          i1=i+1
          el=e(i1)
          g=c*el
          h=c*dl
          if(abs(dl).ge.abs(el)) then
            c=el/dl
            r=sqrt(c**2+1.)
            e(i+2)=s*dl*r
            s=c/r
            c=1./r
          else
            c=dl/el
            r=sqrt(c**2+1.)
            e(i+2)=s*el*r
            s=1./r
            c=c*s
          endif
          dl=c*d(i)-s*g
          d(i1 )=h+s*(c*g+s*d(i))
          do k=1,n
            iz=k+ind
            izn=iz-n
            h=z(izn)
            g=z(iz)
            z(izn)=c*h-s*g
            z(iz) =s*h+c*g
          enddo
        enddo
        el=s*dl
        e(l1)=el
        d(l)=c*dl
        if(abs(el).gt.b) go to 1250
1800    dl=d(l)+f
        jk=l-1
        iz=ln
        do k=1,jk
          i=l-k
          iz=iz-n
          if(dl.ge.d(i)) go to 1860
          imd=-imd
          do j=1,n
            ind=iz+j
            inz=ind+n
            p=z(ind)
            z(ind)=z(inz)
            z(inz)=p
          enddo
          d(i+1)=d(i)
        enddo
        i=0
1860    d(i+1)=dl
      enddo
      if(imd.lt.0) then
        do i=1,n
          z(i)=-z(i)
        enddo
      endif
      iz=1
      do i=3,n
        l=i-1
        iz=iz+l
        ind=iz+i
        h=a(ind)
        if(h.ne.0.) then
          do j=1,nb,n
            jk=j-1
            s=0.
            do k=1,l
              m=iz+k
              ind=jk+k
              s=s+a(m)*z(ind)
            enddo
            s=s/h
            do k=1,l
              ind=jk+k
              m=iz+k
              z(ind)=z(ind)-s*a(m)
            enddo
          enddo
        endif
      enddo
      return
      end
      subroutine qln(a,rot,diag,n)
      dimension a(n,n),rot(n,n),diag(n)
      dimension e(:)
      allocatable e
      allocate(e(n))
      call CopyMat(a,rot,n)
      call tred2(rot,n,n,diag,e)
      call tqli(diag,e,n,n,rot,ich)
      if(ich.ne.0) then
        do i=1,n
          diag(i)=a(i,i)
        enddo
        call UnitMat(rot,n)
      endif
      deallocate(e)
      end
      SUBROUTINE tqli(d,e,n,np,z,ich)
      INTEGER n,np
      REAL d(np),e(np),z(np,np)
      INTEGER i,iter,k,l,m
      REAL b,c,dd,f,g,p,r,s,pythag
      ich=0
      do i=2,n
        e(i-1)=e(i)
      enddo
      e(n)=0.
      do 15 l=1,n
        iter=0
1       do m=l,n-1
          dd=abs(d(m))+abs(d(m+1))
          if (abs(e(m))+dd.eq.dd) go to 2
        enddo
        m=n
2       if(m.ne.l)then
          if(iter.ge.30) then
            ich=1
          endif
          iter=iter+1
          g=(d(l+1)-d(l))/(2.*e(l))
          r=pythag(g,1.)
          g=d(m)-d(l)+e(l)/(g+sign(r,g))
          s=1.
          c=1.
          p=0.
          do 14 i=m-1,l,-1
            f=s*e(i)
            b=c*e(i)
            r=pythag(f,g)
            e(i+1)=r
            if(r.eq.0.)then
              d(i+1)=d(i+1)-p
              e(m)=0.
              goto 1
            endif
            s=f/r
            c=g/r
            g=d(i+1)-p
            r=(d(i)-g)*s+2.*c*b
            p=s*r
            d(i+1)=g+p
            g=c*r-b
C     Omit lines from here ...
            do 13 k=1,n
              f=z(k,i+1)
              z(k,i+1)=s*z(k,i)+c*f
              z(k,i)=c*z(k,i)-s*f
13          continue
C     ... to here when finding only eigenvalues.
14        continue
          d(l)=d(l)-p
          e(l)=g
          e(m)=0.
          goto 1
        endif
15    continue
      return
      END
      FUNCTION pythag(a,b)
      REAL a,b,pythag
      REAL absa,absb
      absa=abs(a)
      absb=abs(b)
      if(absa.gt.absb)then
        pythag=absa*sqrt(1.+(absb/absa)**2)
      else
        if(absb.eq.0.)then
          pythag=0.
        else
          pythag=absb*sqrt(1.+(absa/absb)**2)
        endif
      endif
      return
      END
      SUBROUTINE tred2(a,n,np,d,e)
      INTEGER n,np
      REAL a(np,np),d(np),e(np)
      INTEGER i,j,k,l
      REAL f,g,h,hh,scale
      do 18 i=n,2,-1
        l=i-1
        h=0.
        scale=0.
        if(l.gt.1)then
          do 11 k=1,l
            scale=scale+abs(a(i,k))
11        continue
          if(scale.eq.0.)then
            e(i)=a(i,l)
          else
            do 12 k=1,l
              a(i,k)=a(i,k)/scale
              h=h+a(i,k)**2
12          continue
            f=a(i,l)
            g=-sign(sqrt(h),f)
            e(i)=scale*g
            h=h-f*g
            a(i,l)=f-g
            f=0.
            do 15 j=1,l
C     Omit following line if finding only eigenvalues
              a(j,i)=a(i,j)/h
              g=0.
              do 13 k=1,j
                g=g+a(j,k)*a(i,k)
13            continue
              do 14 k=j+1,l
                g=g+a(k,j)*a(i,k)
14            continue
              e(j)=g/h
              f=f+e(j)*a(i,j)
15          continue
            hh=f/(h+h)
            do 17 j=1,l
              f=a(i,j)
              g=e(j)-hh*f
              e(j)=g
              do 16 k=1,j
                a(j,k)=a(j,k)-f*e(k)-g*a(i,k)
16            continue
17          continue
          endif
        else
          e(i)=a(i,l)
        endif
        d(i)=h
18    continue
C     Omit following line if finding only eigenvalues.
      d(1)=0.
      e(1)=0.
      do 24 i=1,n
C     Delete lines from here ...
        l=i-1
        if(d(i).ne.0.)then
          do 22 j=1,l
            g=0.
            do 19 k=1,l
              g=g+a(i,k)*a(k,j)
19          continue
            do 21 k=1,l
              a(k,j)=a(k,j)-g*a(k,i)
21          continue
22        continue
        endif
C     ... to here when finding only eigenvalues.
        d(i)=a(i,i)
C     Also delete lines from here ...
        a(i,i)=1.
        do 23 j=1,l
          a(i,j)=0.
          a(j,i)=0.
23      continue
C     ... to here when finding only eigenvalues.
24    continue
      return
      END
      subroutine JacobiComplexOld(AIn,n,D,V,NRot)
      complex AIn(n,n),D(n),V(n,n)
      integer p,q
      complex ap,bp,cp,LK,BetaK,app,aqq,apq,aqp,apj,aip,aqj,aiq,
     1        A(:,:),U(:,:),VP(:,:)
      allocatable A,U,VP
      allocate(A(n,n),U(n,n),VP(n,n))
      do p=1,n
        do q=1,n
          V(p,q)=0.
          A(p,q)=AIn(p,q)
        enddo
        V(p,p)=1.
      enddo
      NRot=0
      do m=1,50
        sm=(0.,0.)
        do p=1,n-1
          do q=p+1,n
            sm=sm+abs(A(p,q))
          enddo
        enddo
        if(abs(sm).le..00001) go to 9999
        if(m.lt.4) then
          tresh=.2*sm/n**2
        else
          tresh=.00001
        endif
        do p=1,n-1
          do q=p+1,n
            if(abs(A(p,q)).gt.tresh) then
              ap=A(q,p)
              bp=A(p,p)-A(q,q)
              cp=-A(p,q)
              BetaK=(-bp+sqrt(bp**2-4.*ap*cp))/(2.*ap)
              ThetaK=atan(abs(BetaK))
              AlphaK=atan2(aimag(BetaK),real(BetaK))
              CosThetaK=cos(ThetaK)
              SinThetaK=sin(ThetaK)
              CosTheta2K=CosThetaK**2
              LK=(A(q,q)-A(p,p))*cmplx(SinThetaK**2,0.)+
     1           (A(p,q)*exp(-cmplx(0.,AlphaK))+
     2            A(q,p)*exp( cmplx(0.,AlphaK)))*
     3            cmplx(SinThetaK*CosThetaK,0.)
              app=A(p,p)
              aqq=A(p,p)
              apq=A(p,q)
              aqp=A(q,p)
              do i=1,n
                aip=A(i,p)
                aiq=A(i,q)
                do j=1,n
                  apj=A(p,j)
                  aqj=A(q,j)
                  if(i.eq.p.and.j.eq.p) then
                    A(p,p)=app+LK
                  else if(i.eq.q.and.j.eq.q) then
                    A(q,q)=aqq-LK
                  else if(i.eq.p.and.j.eq.q) then
                    A(p,q)=(apq+(aqq-app)*BetaK
     1                         -aqp*BetaK**2)*CosTheta2K
                  else if(i.eq.q.and.j.eq.p) then
                    A(q,p)=(aqp+(aqq-app)*conjg(BetaK)
     1                         -apq*conjg(BetaK)**2)*CosTheta2K
                  else if(i.eq.p) then
                    A(p,j)=apj*CosThetaK+
     1                     aqj*SinThetaK*exp(cmplx(0., AlphaK))
                  else if(j.eq.p) then
                    A(i,p)=aip*CosThetaK+
     1                     aiq*SinThetaK*exp(cmplx(0.,-AlphaK))
                  else if(i.eq.q) then
                    A(q,j)=aqj*CosThetaK-
     1                     apj*SinThetaK*exp(cmplx(0.,-AlphaK))
                  else if(j.eq.q) then
                    A(i,q)=aiq*CosThetaK-
     1                     aip*SinThetaK*exp(cmplx(0., AlphaK))
                  endif
                enddo
              enddo
              do i=1,n
                do j=1,n
                  if((i.eq.p.and.j.eq.p).or.(i.eq.q.and.j.eq.q))
     1              then
                    U(i,j)=CosThetaK
                  else if(i.eq.p.and.j.eq.q) then
                    U(i,j)=-SinThetaK*exp(cmplx(0., AlphaK))
                  else if(i.eq.q.and.j.eq.p) then
                    U(i,j)= SinThetaK*exp(cmplx(0.,-AlphaK))
                  else if(i.eq.j) then
                    U(i,j)=1.
                  else
                    U(i,j)=0.
                  endif
                enddo
              enddo
              call CopyVekC(V,VP,n**2)
              call MultMC(VP,U,V,n,n,n)
              NRot=NRot+1
            endif
          enddo
        enddo
      enddo
9999  do p=1,n
        D(p)=A(p,p)
      enddo
      deallocate(A,U,VP)
      return
      end
      subroutine JacobiComplex(AIn,n,D,V,NRot)
      include 'fepc.cmn'
      complex AIn(n,n),D(n),V(n,n),B(:,:)
      integer p,q
      complex ap,bp,cp,LK,BetaK,app,aqq,apq,aqp,aip,aiq,apj,aqj,
     1        A(:,:),Vpom(:,:),VpomT(:,:),Upp,Uqq,Upq,Uqp,Vip,Viq,
     2        Apom(:,:)
      allocatable A,Vpom,VpomT,Apom,B
      allocate(A(n,n),B(n,n),Apom(n,n),Vpom(n,n),VPomT(n,n))
      pom=0.
      do p=1,n
        do q=1,n
          V(p,q)=0.
          A(p,q)=AIn(p,q)
          if(p.eq.q) pom=pom+abs(A(p,q))
        enddo
        V(p,p)=1.
      enddo
c
c   Pokus o jakesi pootoceni v pripade, ze na diagonle jsou same nuly
c
c      if(pom.le.0.) then
c        if(n.eq.3) then
c          Vpom (1,1)= 1./sqrt(3.)
c          VpomT(1,1)= 1./sqrt(3.)
c          Vpom (2,1)= 1./sqrt(3.)
c          VpomT(1,2)= 1./sqrt(3.)
c          Vpom (3,1)= 1./sqrt(3.)
c          VpomT(1,3)= 1./sqrt(3.)
c          Vpom (1,2)= 1./sqrt(2.)
c          VpomT(2,1)= 1./sqrt(2.)
c          Vpom (2,2)=-1./sqrt(2.)
c          VpomT(2,2)=-1./sqrt(2.)
c          Vpom (3,2)= 0.
c          VpomT(2,3)= 0.
c          Vpom (1,3)= 1./sqrt(6.)
c          VpomT(3,1)= 1./sqrt(6.)
c          Vpom (2,3)= 1./sqrt(6.)
c          VpomT(3,2)= 1./sqrt(6.)
c          Vpom (3,3)=-2./sqrt(6.)
c          VpomT(3,3)=-2./sqrt(6.)
c          call MultMC(Vpom,A,Apom,3,3,3)
c          call MultMC(Apom,VPomT,A,3,3,3)
c          write(6,'('' Kontrola matic'')')
c          write(6,'(6f10.3)') A
c          write(6,'(1x,60(''-''))')
c          V=VpomT
c        endif
c      endif
      NRot=0
      do m=1,200
        sm=(0.,0.)
        do p=1,n
          do q=1,n
            if(p.ne.q) sm=sm+abs(A(p,q))
          enddo
        enddo
        if(abs(sm).le..000001) go to 9000
        if(m.lt.4) then
          tresh=.2*sm/n**2
        else
          tresh=.0000001
        endif
        do p=1,n-1
          do q=p+1,n
            if(abs(A(p,q)).le.tresh) cycle
            ap=A(q,p)
            bp=A(p,p)-A(q,q)
            cp=-A(p,q)
            if(abs(ap).gt.0.) then
              BetaK=(-bp+sqrt(bp**2-4.*ap*cp))/(2.*ap)
              go to 1100
            else if(abs(bp).gt.0.) then
              BetaK=-cp/bp
              go to 1100
            else
              ap=A(p,q)
              bp=A(p,p)-A(q,q)
              cp=-A(q,p)
              BetaK=A(p,q)
            endif
1100        if(abs(BetaK).lt.tresh) cycle
            AlphaK=atan2(aimag(BetaK),real(BetaK))
            ThetaK=atan(abs(BetaK))
1200        app=A(p,p)
            aqq=A(q,q)
            apq=A(p,q)
            aqp=A(q,p)
            CosThetaK=cos(ThetaK)
            SinThetaK=sin(ThetaK)
            CosTheta2K=CosThetaK**2
            LK=(aqq-app)*cmplx(SinThetaK**2,0.)+
     1         (apq*cexp(-cmplx(0.,AlphaK))+
     2          aqp*cexp( cmplx(0.,AlphaK)))*
     3          cmplx(SinThetaK*CosThetaK,0.)
            A(p,p)=app+LK
            A(q,q)=aqq-LK
            A(p,q)=(apq+(aqq-app)*BetaK
     1             -aqp*BetaK**2)*CosTheta2K
            A(q,p)=(aqp+(aqq-app)*conjg(BetaK)
     1             -apq*conjg(BetaK)**2)*CosTheta2K
            do i=1,n
              if(i.eq.p.or.i.eq.q) cycle
              aip=A(i,p)
              aiq=A(i,q)
              A(i,p)=aip*CosThetaK+
     1               aiq*SinThetaK*cexp(cmplx(0.,-AlphaK))
              A(i,q)=aiq*CosThetaK-
     1               aip*SinThetaK*cexp(cmplx(0., AlphaK))
            enddo
            do j=1,n
              if(j.eq.p.or.j.eq.q) cycle
              apj=A(p,j)
              aqj=A(q,j)
              A(p,j)=apj*CosThetaK+
     1               aqj*SinThetaK*cexp(cmplx(0., AlphaK))
              A(q,j)=aqj*CosThetaK-
     1               apj*SinThetaK*cexp(cmplx(0.,-AlphaK))
            enddo
            Upp=CosThetaK
            Uqq=CosThetaK
            Upq=-SinThetaK*cexp(cmplx(0., AlphaK))
            Uqp= SinThetaK*cexp(cmplx(0.,-AlphaK))
            do i=1,n
              Vip=V(i,p)
              Viq=V(i,q)
              V(i,p)=Vip*Upp+Viq*Uqp
              V(i,q)=Vip*Upq+Viq*Uqq
            enddo
            NRot=NRot+1
          enddo
        enddo
      enddo
9000  do p=1,n
        D(p)=A(p,p)
      enddo
      go to 9999
c
c   Kontrola diagonalizace
c
c      Vpom=V
c      call MatTransConjg(Vpom,VpomT,n)
c      call MultMC(VpomT,AIn,Apom,n,n,n)
c      call MultMC(Apom,Vpom,B,n,n,n)
c      do p=1,n
c        do q=1,n
c          if(abs(B(p,q)-A(p,q)).gt..1) go to 9100
c        enddo
c      enddo
c      go to 9999
c9100  write(6,'('' Kontrola'')')
c      do p=1,n
c        write(6,'(20f10.5)')(A(p,q),q=1,n)
c      enddo
c      write(6,'(1x,60(''-''))')
c      do p=1,n
c        write(6,'(20f10.5)')(B(p,q),q=1,n)
c      enddo
c      write(6,'(1x,60(''=''))')
9999  deallocate(A,B,Apom,Vpom,VpomT)
      return
      end
      subroutine JacobiComplexZkouska(AIn,n,D,V,NRot)
      include 'fepc.cmn'
c      include 'basic.cmn'
      complex AIn(n,n),D(n),V(n,n)
      integer p,q
      complex ap,bp,cp,LK,BetaK,app,aqq,apq,aqp,aip,aiq,apj,aqj,
     1        A(:,:),Upp,Uqq,Upq,Uqp,Vip,Viq,
     2        UPom(:,:),UPomC(:,:),ANew(:,:),APom(:,:)
      allocatable A,UPom,UPomC,ANew,APom
      allocate(A(n,n),UPom(n,n),UPomC(n,n),ANew(n,n),APom(n,n))
      do p=1,n
        do q=1,n
          V(p,q)=0.
          A(p,q)=AIn(p,q)
        enddo
        V(p,p)=1.
      enddo
      NRot=0
      do m=1,50
        sm=(0.,0.)
        do p=1,n-1
          do q=p+1,n
            sm=sm+abs(A(p,q))
          enddo
        enddo
        if(abs(sm).le..000001) go to 9999
        if(m.lt.4) then
          tresh=.2*sm/n**2
        else
          tresh=.0000001
        endif
        do p=1,n-1
          do q=p+1,n
            if(abs(A(p,q)).le.tresh) cycle
            ap=A(q,p)
            bp=A(p,p)-A(q,q)
            cp=-A(p,q)
            BetaK=(-bp+sqrt(bp**2-4.*ap*cp))/(2.*ap)
            if(abs(BetaK).lt.tresh) cycle

            AlphaK=atan2(aimag(BetaK),real(BetaK))
            ThetaK=atan(abs(BetaK))
            call UnitMatC(UPom,n)
c            write(6,'('' To ma byt jednotkova matice'')')
c            write(6,'(8f10.6)')((UPom(ii,jj),jj=1,n),ii=1,n)
c            write(6,'(2i5,2f10.6)') p,q,ThetaK,AlphaK
            UPom(p,p)=cmplx(cos(ThetaK))
            UPom(q,q)=cmplx(cos(ThetaK))
            UPom(p,q)= cexp(cmplx(0., AlphaK))*cmplx(sin(ThetaK),0.)
            UPom(q,p)=-cexp(cmplx(0.,-AlphaK))*cmplx(sin(ThetaK),0.)
            call MatTransConjg(UPom,UPomC,n)
c            write(6,'('' To ma byt konjugovana matice'')')
cc            write(6,'(8f10.6)')((UPomC(ii,jj),jj=1,n),ii=1,n)
            call MultmC(UPom,A,ANew,n,n,n)
            call MultmC(ANew,UPomC,Apom,n,n,n)
c            write(6,'('' Vysledek1:'')')
c            write(6,'(8f10.6)')((APom(ii,jj),jj=1,n),ii=1,n)
c
c            write(6,'('' To ma byt transformacni matice'')')
c            write(6,'(8f10.6)')((UPom(ii,jj),jj=1,n),ii=1,n)


            app=A(p,p)
            aqq=A(q,q)
            apq=A(p,q)
            aqp=A(q,p)
c            ThetaK=atan(abs(BetaK))
c            BetaK=exp(cmplx(0.,AlphaK))*tan(ThetaK)
c                  write(6,'(2i5,2f10.6)') p,q,ThetaK,AlphaK
            CosThetaK=cos(ThetaK)
            SinThetaK=sin(ThetaK)
            CosTheta2K=CosThetaK**2
            LK=(aqq-app)*cmplx(SinThetaK**2,0.)+
     1         (apq*cexp(-cmplx(0.,AlphaK))+
     2          aqp*cexp( cmplx(0.,AlphaK)))*
     3          cmplx(SinThetaK*CosThetaK,0.)
            A(p,p)=app+LK
            A(q,q)=aqq-LK
            A(p,q)=(apq+(aqq-app)*BetaK
     1             -aqp*BetaK**2)*CosTheta2K
            A(q,p)=(aqp+(aqq-app)*conjg(BetaK)
     1             -apq*conjg(BetaK)**2)*CosTheta2K
            if(abs(A(p,p)-APom(p,p)).gt..0001) then
              write(6,'('' Rozdil pp:'',4i5,4f10.6)')
     1          p,q,p,p,A(p,p),APom(p,p)
              pause
            endif
            if(abs(A(q,q)-APom(q,q)).gt..0001) then
              write(6,'('' Rozdil qq:'',4i5,4f10.6)')
     1          p,q,q,q,A(q,q),APom(q,q)
              pause
            endif
            if(abs(A(p,q)-APom(p,q)).gt..0001) then
              write(6,'('' Rozdil pq:'',4i5,4f10.6)')
     1          p,q,p,q,A(p,q),APom(p,q)
              pause
            endif
            if(abs(A(q,p)-APom(q,p)).gt..0001) then
              write(6,'('' Rozdil qp:'',4i5,4f10.6)')
     1          p,q,q,p,A(q,p),APom(q,p)
              pause
            endif

c            write(6,'(2f10.6)') A(p,q)
c            write(6,'(2f10.6)') A(q,p)
            do i=1,n
              if(i.eq.p.or.i.eq.q) cycle
              aip=A(i,p)
              aiq=A(i,q)
              A(i,p)=aip*CosThetaK+
     1               aiq*SinThetaK*cexp(cmplx(0.,-AlphaK))
              if(abs(A(i,p)-APom(i,p)).gt..0001) then
                write(6,'('' Rozdil ip:'',4i5,4f10.6)')
     1            p,q,i,p,A(i,p),APom(i,p)
                pause
              endif
              A(i,q)=aiq*CosThetaK-
     1               aip*SinThetaK*cexp(cmplx(0., AlphaK))
              if(abs(A(i,q)-APom(i,q)).gt..0001) then
                write(6,'('' Rozdil iq:'',4i5,4f10.6)')
     1            p,q,i,q,A(i,q),APom(i,q)
                pause
              endif


            enddo
            do j=1,n
              if(j.eq.p.or.j.eq.q) cycle
              apj=A(p,j)
              aqj=A(q,j)
              A(p,j)=apj*CosThetaK+
     1               aqj*SinThetaK*cexp(cmplx(0., AlphaK))
              if(abs(A(p,j)-APom(p,j)).gt..0001) then
                write(6,'('' Rozdil pj:'',4i5,8f10.6)')
     1            p,q,p,j,apj,aqj,A(p,j),APom(p,j)
                pause
              endif
              A(q,j)=aqj*CosThetaK-
     1               apj*SinThetaK*cexp(cmplx(0.,-AlphaK))
              if(abs(A(q,j)-APom(q,j)).gt..0001) then
                write(6,'('' Rozdil qj:'',4i5,8f10.6)')
     1            p,q,q,j,apj,aqj,A(q,j),APom(q,j)
                pause
              endif
            enddo
c            write(6,'('' Vysledek2:'')')
c            write(6,'(8f10.6)')((A(ii,jj),jj=1,n),ii=1,n)
c            pause
            Upp=CosThetaK
            Uqq=CosThetaK
            Upq=-SinThetaK*cexp(cmplx(0., AlphaK))
            Uqp= SinThetaK*cexp(cmplx(0.,-AlphaK))
            do i=1,n
              Vip=V(i,p)
              Viq=V(i,q)
              V(i,p)=Vip*Upp+Viq*Uqp
              V(i,q)=Vip*Upq+Viq*Uqq
            enddo


            NRot=NRot+1
          enddo
        enddo
      enddo
9999  do p=1,n
        D(p)=A(p,p)
      enddo
      deallocate(A,UPom,UPomC,ANew,APom)
      return
      end
      function PolByHorn(x,a,n)
      dimension a(n)
      PolByHorn=a(1)
      do i=2,n
        PolByHorn=PolByHorn*x+a(i)
      enddo
      end
      function bessj0(x)
      double precision y,p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6
     1      ,s1,s2,s3,s4,s5,s6
      data p1,p2,p3,p4,p5/1.d0,-.1098628627d-2,.2734510407d-4,
     1     -.2073370639d-5,.2093887211d-6/,
     2     q1,q2,q3,q4,q5/-.1562499995d-1,.1430488765d-3,
     3     -.6911147651d-5,.7621095161d-6,-.934945152d-7/
      data r1,r2,r3,r4,r5,r6/57568490574.d0,-13362590354.d0,
     1     651619640.7d0,-11214424.18d0,77392.33017d0,-184.9052456d0/,
     2     s1,s2,s3,s4,s5,s6/57568490411.d0,1029532985.d0,
     3     9494680.718d0,59272.64853d0,267.8532712d0,1.d0/
      if(abs(x).lt.8.) then
          y=x**2
          bessj0=(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))
     1           /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
      else
          ax=abs(x)
          z=8./ax
          y=z**2
          xx=ax-.785398164
          bessj0=sqrt(.636619772/ax)*(cos(xx)*(p1+y*(p2+y*(p3+y*(p4
     1           +y*p5))))-z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))
      endif
      return
      end
      function bessj1(x)
      double precision y,p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6
     1      ,s1,s2,s3,s4,s5,s6
      data r1,r2,r3,r4,r5,r6/72362614232.d0,-7895059235.d0,
     1     242396853.1d0,-2972611.439d0,15704.48260d0,
     2     -30.16036606d0/,
     3     s1,s2,s3,s4,s5,s6/144725228442.d0,2300535178.d0,
     4     18583304.74d0,99447.43394d0,376.9991397d0,1.d0/
      data p1,p2,p3,p4,p5/1.d0,.183105d-2,-.3516396496d-4,
     1     .2457520174d-5,-.240337019d-6/,
     2     q1,q2,q3,q4,q5/.04687499995d0,-.2002690873d-3,
     4     .8449199096d-5,-.88228987d-6,.105787412d-6/
      if (abs(x).lt.8.)then
           y=x**2
           bessj1=x*(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))
     1            /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
      else
          ax=abs(x)
          z=8./ax
          y=z**2
          xx=ax-2.356194491
          bessj1=sqrt(.636619772/ax)*(cos(xx)*(p1+y*(p2+y*(p3+y*(p4
     1           +y*p5))))-z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))
     2           *sign(1.,x)
      endif
      return
      end
      subroutine bessj(x,n,besp,besp1)
      parameter (iacc=40,bigno=1.e10,bigni=1.e-10)
      double precision bj,bjm,bjp
      dimension besp(*)
      if(n.lt.0) go to 9999
      if(x.eq.0.) then
        besp(1)=1.
        do i=2,n+1
          besp(i)=0.
        enddo
        besp1=0.
      else
        bjm=bessj0(x)
        bj =bessj1(x)
        besp(1)=bjm
        if(n.gt.0) then
          besp(2)=bj
        else
          besp1=bj
          go to 9999
        endif
        ax=abs(x)
        tox=2./ax
        if(x.lt.0.) bj=-bj
        mez=min(ifix(ax),n+1)
        if(mez.lt.2) go to 2000
        do j=1,mez-1
          bjp=j*tox*bj-bjm
          bjm=bj
          bj=bjp
          if(j.lt.n) then
            besp(j+2)=bj
          else
            besp1=bj
          endif
        enddo
        if(mez.ge.n+1) go to 3000
2000     m=2*((n+1+int(sqrt(float(iacc*(n+1)))))/2)
        mez=max(mez,1)
        do i=mez+2,n+1
          besp(i)=0.
        enddo
        besp1=0.
        jsum=0
        sum=0.
        bjp=0.
        bj=1.
        do j=m,1,-1
          bjm=j*tox*bj-bjp
          bjp=bj
          bj=bjm
          if(abs(bj).gt.bigno) then
            bj=bj*bigni
            bjp=bjp*bigni
            do i=mez+2,n+1
              besp(i)=besp(i)*bigni
            enddo
            besp1=besp1*bigni
            sum=sum*bigni
          endif
          if(jsum.ne.0) sum=sum+bj
          jsum=1-jsum
          if(j.le.n.and.j.ge.mez+1) then
            besp(j+1)=bjp
          else if(j.eq.n+1) then
            besp1=bjp
          endif
        enddo
        sum=2.*sum-bj
        sum=1./sum
        do i=mez+2,n+1
          besp(i)=besp(i)*sum
        enddo
        besp1=besp1*sum
3000    if(x.lt.0.) then
          do i=3,n,2
            besp(i+1)=-besp(i+1)
          enddo
          if(mod(n+1,2).eq.1) besp1=-besp1
        endif
      endif
9999  return
      end
      function bessi0(x)
      double precision y,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,
     1                 q9
      data p1,p2,p3,p4,p5,p6,p7/1.d0,3.5156229d0,3.0899424d0,
     1     1.2067492d0,.2659732d0,0.360768d-1,0.45813d-2/
      data q1,q2,q3,q4,q5,q6,q7,q8,q9/0.39894228d0,0.1328592d-1,
     1     0.225319d-2,-0.157565d-2,0.916281d-2,-0.2057706d-1,
     2     0.2635537d-1,-0.1647633d-1,0.392377d-2/
      if(abs(x).lt.3.75) then
         y=(x/3.75)**2
         bessi0=p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7)))))
      else
          ax=abs(x)
          y=3.75/ax
          bessi0=(exp(ax)/sqrt(ax))*(q1+y*(q2+y*(q3+y*(q4
     1           +y*(q5+y*(q6+y*(q7+y*(q8+y*q9))))))))
      endif
      return
      end
      function bessi1(x)
      double precision y,p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,
     1                 q9
      data p1,p2,p3,p4,p5,p6,p7/0.5d0,0.87890594d0,0.51498869d0,
     1     0.15084934d0,0.2658733d-1,0.301532d-2,0.32411d-3/
      data q1,q2,q3,q4,q5,q6,q7,q8,q9/0.39894228d0,-0.3988024d-1,
     1     -0.362018d-2,0.163801d-2,-0.1031555d-1,0.2282967d-1,
     2     -0.2895312d-1,0.1787654d-1,-0.420059d-2/
      if(abs(x).lt.3.75) then
         y=(x/3.75)**2
         bessi1=x*(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7))))))
      else
          ax=abs(x)
          y=3.75/ax
          bessi1=(exp(ax)/sqrt(ax))*(q1+y*(q2+y*(q3+y*(q4
     1           +y*(q5+y*(q6+y*(q7+y*(q8+y*q9))))))))
          if (x.lt.0.)bessi1=-bessi1
      endif
      return
      end
      subroutine bessi(x,n,besp,besp1)
      parameter (iacc=40,bigno=1.e10,bigni=1.e-10)
      dimension besp(*)
      double precision bi,bim,bip
      if(n.lt.0) go to 9999
      if(abs(x).lt.bigni) then
        besp(1)=1.
        do i=2,n+1
          besp(i)=0.
        enddo
        besp1=0.
      else
        besp(1)=bessi0(x)
        bi=bessi1(x)
        if(n.ge.1) then
          besp(2)=bi
        else
          besp1=bi
          go to 9999
        endif
        tox=2./abs(x)
        m=2*(n+1+int(sqrt(float(iacc*(n+1)))))
        do i=3,n+1
          besp(i)=0.
        enddo
        besp1=0.
        bip=0.
        bi=1.
        do j=m,1,-1
          bim=bip+j*tox*bi
          bip=bi
          bi=bim
          if(abs(bi).gt.bigno) then
            bi=bi*bigni
            bip=bip*bigni
            do i=3,n+1
              besp(i)=besp(i)*bigni
            enddo
            besp1=besp1*bigni
          endif
          if(j.le.n.and.j.ge.2) then
            besp(j+1)=bip
          else if(j.eq.n+1) then
            besp1=bip
          endif
        enddo
        bi=besp(1)/bi
        do i=3,n+1
          besp(i)=besp(i)*bi
        enddo
        besp1=besp1*bi
3000    if(x.lt.0.) then
          do i=3,n,2
            besp(i+1)=-besp(i+1)
          enddo
          if(mod(n+1,2).eq.1) besp1=-besp1
        endif
      endif
9999  return
      end
      function erfcn(x)
      if(x.lt.0.)then
        erfcn=1.+gammp(.5,x**2)
      else
        erfcn=gammq(.5,x**2)
      endif
      return
      end
      function gammp(a,x)
      if(x.lt.0..or.a.le.0.) pause 'bad arguments in gammp'
      if(x.lt.a+1.)then
        call gser(gamser,a,x,gln)
        gammp=gamser
      else
        call gcf(gammcf,a,x,gln)
        gammp=1.-gammcf
      endif
      return
      end
      function gammq(a,x)
      if(x.lt.0..or.a.le.0.)pause 'bad arguments in gammq'
      if(x.lt.a+1.)then
        call gser(gamser,a,x,gln)
        gammq=1.-gamser
      else
        call gcf(gammcf,a,x,gln)
        gammq=gammcf
      endif
      return
      end
      subroutine gser(gamser,a,x,gln)
      parameter (itmax=100,eps=3.e-7)
      gln=gammln(a)
      if(x.le.0.) then
        if(x.lt.0.) pause 'x < 0 in gser'
        gamser=0.
        return
      endif
      ap=a
      sum=1./a
      del=sum
      do n=1,itmax
        ap=ap+1.
        del=del*x/ap
        sum=sum+del
        if(abs(del).lt.abs(sum)*eps) go to 1
      enddo
      pause 'a too large, itmax too small in gser'
1     gamser=sum*exp(-x+a*log(x)-gln)
      return
      end
      subroutine gcf(gammcf,a,x,gln)
      parameter (itmax=100,eps=3.e-7,fpmin=1.e-30)
      gln=gammln(a)
      b=x+1.-a
      c=1./fpmin
      d=1./b
      h=d
      do  i=1,itmax
        an=-i*(i-a)
        b=b+2.
        d=an*d+b
        if(abs(d).lt.fpmin) d=fpmin
        c=b+an/c
        if(abs(c).lt.fpmin) c=fpmin
        d=1./d
        del=d*c
        h=h*del
        if(abs(del-1.).lt.eps) go to 1
      enddo
      pause 'a too large, itmax too small in gcf'
1     gammcf=exp(-x+a*log(x)-gln)*h
      return
      end
      function gammln(xx)
      real gammln,xx
      integer j
      double precision ser,stp,tmp,x,y,cof(6)
      save cof,stp
      data cof,stp/76.18009172947146d0,-86.50532032941677d0,
     1 24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2,
     2 -.5395239384953d-5,2.5066282746310005d0/
      x=xx
      y=x
      tmp=x+5.5d0
      tmp=(x+0.5d0)*log(tmp)-tmp
      ser=1.000000000190015d0
      do j=1,6
        y=y+1.d0
        ser=ser+cof(j)/y
      enddo
      gammln=tmp+log(stp*ser/x)
      return
      end
      subroutine E1Z(Z,CE1)
      implicit none
      integer k
      complex*16 Z,CE1,CR,CT,CT0
      double precision Pi,El,A0,x,fk
      data Pi,El/3.141592653589793D0,0.5772156649015328D0/
      x=real(Z)
      A0=cdabs(Z)
      if(A0.EQ.0.0D0) then
        CE1=(1.d+300,0.)
      else if(A0.le.10..or.(x.lt.0..and.A0.lt.20.)) then
        CE1=(1.0,0.0)
        CR =(1.0,0.0)
        do k=1,150
           fk=dble(float(k))
           CR=-CR*fk*Z/(fk+1.0D0)**2
           CE1=CE1+CR
           if(cdabs(CR).le.cdabs(CE1)*1.0D-15) go to 15
        enddo
15      CE1=-EL-cdlog(Z)+Z*CE1
      else
        CT0=(0.,0.)
        do k=120,1,-1
          fk=dble(float(k))
          CT0=fk/(1.0D0+fk/(Z+CT0))
        enddo
        CT=1.0D0/(Z+CT0)
        CE1=CDEXP(-Z)*CT
        if(x.le.0..and.dimag(Z).EQ.0.) CE1=CE1-pi*(0.,1.)
      endif
      return
      end
      function ConvTOFWithLorentz(x,al,be,ga,dx,dal,dbe,dga)
      include 'fepc.cmn'
      complex*16 aa,bb,aap,bbp,e1a,e1b,poma,pomb,expaa,expbb
      fn=-al*be/(pi*(al+be))
      aap= x*(1.,0.)+ga*.5*(0.,1.)
      bbp=-x*(1.,0.)+ga*.5*(0.,1.)
      aa=al*aap
      bb=be*bbp
      if(abs(aa).gt.700..or.abs(bb).gt.700.) then
        ConvTOFWithLorentz=0.
        dx=0.
        dga=0.
        dal=0.
        dbe=0.
        go to 9999
      endif
      call e1z(aa,e1a)
      call e1z(bb,e1b)
      if(al*x.le.0.) then
        expaa=exp(aa)
      else
        expaa=1./exp(-aa)
      endif
      if(be*x.ge.0.) then
        expbb=exp(bb)
      else
        expbb=1./exp(-bb)
      endif
      ConvTOFWithLorentz=fn*aimag(expaa*e1a+expbb*e1b)
      poma=expaa*e1a-1./aa
      pomb=expbb*e1b-1./bb
      dx=fn*(al*aimag(poma)-be*aimag(pomb))
      dga=fn*.5*(al*aimag(poma*(0.,1.))+be*aimag(pomb*(0.,1.)))
      dal=fn*aimag(poma*aap)+ConvTOFWithLorentz*(1./al-1./(al+be))
      dbe=fn*aimag(pomb*bbp)+ConvTOFWithLorentz*(1./be-1./(al+be))
9999  return
      end
      function ConvTOFWithGauss(x,al,be,fwhm,dx,dal,dbe,dsig)
      include 'fepc.cmn'
      data TwoOverSqrtPi/1.128379/
      sig=fwhm/2.35482
      GaussNorm=1.
      fn=al*be/(2.*(al+be))
      SigOverSqrt2=rsqrt2/sig
      GaussNorm=1./(sqrt(2.*pi)*sig)
      poma=al*sig**2+x
      u=al*.5*(poma+x)
      ConvTOFWithGauss=0.
      if(u.lt.30.) then
        expu=ExpJana(u,ich)
        y=poma*SigOverSqrt2
        erfcny=erfc(y)
        expy=ExpJana(-y**2,ich)
        ConvTOFWithGauss=fn*expu*erfcny
        dx=fn*expu*(erfcny*al-expy*TwoOverSqrtPi*SigOverSqrt2)
        dal=fn*expu*(erfcny*(al*sig**2+x)-
     1                TwoOverSqrtPi*expy*sig*rsqrt2)
     2      +ConvTOFWithGauss*(1./al-1./(al+be))
        dala=ConvTOFWithGauss*(1./al-1./(al+be))
        dbe=ConvTOFWithGauss*(1./be-1./(al+be))
        pomp=x*SigOverSqrt2/sig
        dsig=fn*expu*(erfcny*al**2*sig-
     1                TwoOverSqrtPi*expy*(al*rsqrt2-pomp))
      else
        ConvTOFWithGauss=.5*GaussNorm*ExpJana(-x**2/(2.*sig**2),ich)
        dx=-ConvTOFWithGauss*x/sig**2
        dal=0.
        dbe=0.
        dsig=ConvTOFWithGauss*x**2/sig**3
      endif
      pomb=be*sig**2-x
      v=be*.5*(pomb-x)
      if(v.lt.30.) then
        expv=ExpJana(v,ich)
        z=pomb*SigOverSqrt2
        erfcnz=erfc(z)
        expz=ExpJana(-z**2,ich)
        ConvTOFWithGaussP=fn*expv*erfcnz
        ConvTOFWithGauss=ConvTOFWithGauss+ConvTOFWithGaussP
        dx=dx-fn*expv*(erfcnz*be-expz*TwoOverSqrtPi*SigOverSqrt2)
        dal=dal+ConvTOFWithGaussP*(1./al-1./(al+be))
        dbe=dbe+fn*expv*(erfcnz*(be*sig**2-x)-
     1              TwoOverSqrtPi*expz*sig*rsqrt2)
     2         +ConvTOFWithGaussP*(1./be-1./(al+be))
        pomp=x*SigOverSqrt2/sig
        dsig=dsig+
     1       fn*expv*(erfcnz*be**2*sig-
     3                 TwoOverSqrtPi*expz*(be*rsqrt2+pomp))
      else
        pom=.5*GaussNorm*ExpJana(-x**2/(2.*sig**2),ich)
        ConvTOFWithGauss=ConvTOFWithGauss+pom
        dx=dx-pom*x/sig**2
        dsig=dsig+ConvTOFWithGauss*x**2/sig**3
      endif
      dsig=dsig/2.35482
      return
      end
      subroutine PVoigt(dt,func,dfdtp,dfds,dfdg)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      data amlp,bmlp/.819449,1.656854/
      dtq=dt**2
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
        bunbo=(.5*fwhm)**2+dtq
        tl=fwhm/(pi2*bunbo)
        if(CalcDer) then
          dtldt=-2.*etaPwd*dt*tl/bunbo
          tv=etaPwd*(tl/fwhm-pi*tl**2)
        endif
      else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
        aml=amlp/fwhm
        bml=bmlp/fwhm**2
        den=1.+bml*dtq
        tl=aml/den**2
        if(CalcDer) then
          dtldt=-4.*aml*bml*dt/den**3
          tv=4.*tl/den*bml/fwhm*dtq-tl/fwhm
        endif
      else
        tl=0.
        if(CalcDer) then
          dtldt=0.
          tv=0.
        endif
      endif
      if((KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt).and.sigp.ne.0.)
     2  then
        ex=-.5*dtq/sigp
        tg=.9394373*ExpJana(ex,ich)/fwhm
      else
        ex=0.
        tg=0.
      endif
      func=etaPwd*tl+(1.-etaPwd)*tg
      if(CalcDer) then
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          ts=-2.*(1.-etaPwd)*tg*(ex+.5)/fwhm
          if(sigp.ne.0.) then
            pomg=-(1.-etaPwd)*tg*dt/sigp
          else
            pomg=0.
          endif
        else
          ts=0.
          pomg=0.
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          poml=dtldt
        else
          poml=0.
        endif
        dfdtp=poml+pomg
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          dfds=dedffg*(tl-tg)+(tv+ts)*dfwdg
          if(sqsg.ne.0.) then
            dfds=1.17741*dfds/sqsg
          else
            dfds=0.
          endif
        else
          dfds=0.
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          dfdg=dedffl*(tl-tg)+(tv+ts)*dfwdl
        else
          dfdg=0.
        endif
      endif
      return
      end
      subroutine PVoigtTOF(dt,R,dRdAl,dRdBe,dRdT,dRdS,dRdG)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      etaPwdComp=(1.-etaPwd)
      dtq=dt**2
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
        RLorentz=ConvTOFWithLorentz(dt,Alpha12,Beta12,fwhm,dRLdT,dRLdAl,
     1                              dRLdBe,dRLdG)
        if(CalcDer) then
          dRLdG=etaPwd*dRLdG
          dRLdT=etaPwd*dRLdT
          dRLdAl=etaPwd*dRLdAl
          dRLdBe=etaPwd*dRLdBe
        endif
      else
        RLorentz=0.
        if(CalcDer) then
          dRLdT=0.
          dRLdG=0.
          dRLdAl=0.
          dRLdBe=0.
          dRLdG=0.
        endif
      endif
      if((KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1   KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt).and.sigp.ne.0.)
     2  then
        RGauss=ConvTOFWithGauss(dt,Alpha12,Beta12,fwhm,dRGdT,dRGdAl,
     1                          dRGdBe,dRGdS)
        if(CalcDer) then
          dRGdS=etaPwdComp*dRGdS
          dRGdT=etaPwdComp*dRGdT
          dRGdAl=etaPwdComp*dRGdAl
          dRGdBe=etaPwdComp*dRGdBe
        endif
      else
        RGauss=0.
        if(CalcDer) then
          dRGdT=0.
          dRGdS=0.
          dRGdAl=0.
          dRGdBe=0.
          dRGdS=0.
        endif
      endif
      R=etaPwd*RLorentz+etaPwdComp*RGauss
      if(CalcDer) then
        dRdT=dRLdT+dRGdT
        dRdAl=dRLdAl+dRGdAl
        dRdBe=dRLdBe+dRGdBe
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          dRdS=dedffg*(RLorentz-RGauss)+(dRLdG+dRGdS)*dfwdg
          if(sqsg.ne.0.) then
            dRdS=1.17741*dRdS/sqsg
          else
            dRdS=0.
          endif
        else
          dRdS=0.
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt) then
          dRdG=dedffl*(RLorentz-RGauss)+(dRLdG+dRGdS)*dfwdl
        else
          dRdG=0.
        endif
      endif
      return
      end
      function Chebev(a,da,x,n)
      dimension a(n),da(n)
      if(n.le.0) then
        Chebev=0.
        go to 9999
      endif
      d=0.
      dd=0.
      x2=2.*x
      do i=n,2,-1
        sv=d
        d=x2*d-dd+a(i)
        dd=sv
      enddo
      chebev=x*d-dd+a(1)
      do i=n,2,-1
        d=1.
        dd=0.
        do j=i-1,2,-1
          sv=d
          d=x2*d-dd
          dd=sv
        enddo
        da(i)=x*d-dd
      enddo
      da(1)=1.
9999  return
      end
      subroutine GetFPol(x,FPol,NPol,Type)
      integer Type
      real x,FPol(NPol),XMat(:,:)
      integer :: NAlloc = 0
      allocatable XMat
      save XMat,NAlloc
      if(Type.eq.2) then
        call FLeg(x,FPol,NPol)
      else if(Type.eq.3) then
        FPol(1)=1.
        if(NPol-1.gt.NAlloc) then
          if(allocated(XMat)) deallocate(XMat)
          allocate(XMat(NPol-1,NPol-1))
          NAlloc=NPol-1
          call MakeXHarmOrtho(XMat,NPol-1)
        endif
        do i=2,NPol
          FPol(i)=XHarmOrtho(x,i-1,XMat,NAlloc)
        enddo
      endif
      return
      end
      subroutine FLeg(x,pl,nl)
      integer nl
      real x,pl(nl)
      integer j
      real d,f1,f2,twox
      pl(1)=1.
      pl(2)=x
      if(nl.gt.2) then
        twox=2.*x
        f2=x
        d=1.
        do j=3,nl
          f1=d
          f2=f2+twox
          d=d+1.
          pl(j)=(f2*pl(j-1)-f1*pl(j-2))/d
        enddo
      endif
      return
      end
      function FLegen(a,da,x,n)
      include 'fepc.cmn'
      real a(*),da(*)
      if(n.le.0) then
        FLegen=0.
        go to 9999
      endif
      da(1)=1.
      da(2)=x
      FLegen=a(1)+a(2)*x
      do i=3,n
        da(i)=(2.-1./float(i-1))*(da(i-1)*x-da(i-2))+da(i-2)
        FLegen=FLegen+a(i)*da(i)
      enddo
9999  return
      end
      function XHarmOrtho(x,n,XMat,nd)
      include 'fepc.cmn'
c      include 'basic.cmn'
      dimension XMat(nd,nd)
      XHarmOrtho=0.
      do i=1,n
        if(abs(XMat(n,i)).lt..000001) cycle
        if(i.eq.1) then
          XHarmOrtho=XHarmOrtho+XMat(n,i)*x
        else
          j=i/2
          if(mod(i,2).eq.1) then
            XHarmOrtho=XHarmOrtho+XMat(n,i)*sin(pi*float(j)*x)
          else
            XHarmOrtho=XHarmOrtho+XMat(n,i)*cos(pi*float(j)*x)
          endif
        endif
      enddo
      return
      end
      subroutine MakeXHarmOrtho(XMat,nd)
      dimension XMat(nd,nd),GMat(:,:)
      allocatable GMat
      allocate(GMat(nd,nd))
      do i=1,nd
        do j=1,i
          GMat(i,j)=ScProdXHarm(i,j)
          if(i.ne.j) GMat(j,i)=GMat(i,j)
        enddo
      enddo
      do n=1,nd
        do i=1,nd
          XMat(n,i)=0.
        enddo
        do j=1,n-1
          aj=0.
          do k=1,j
            aj=aj-XMat(j,k)*GMat(k,n)
          enddo
          do k=1,j
            XMat(n,k)=XMat(n,k)+aj*XMat(j,k)
          enddo
        enddo
        XMat(n,n)=1.
        pom=0.
        do i=1,n
          do j=1,n
            pom=pom+XMat(n,i)*XMat(n,j)*GMat(i,j)
          enddo
        enddo
        if(pom.gt.0.) then
          pom=sqrt(1./pom)
        else
          pom=0.
        endif
        do i=1,n
          if(i.ne.n.or.pom.ne.0.) then
            XMat(n,i)=XMat(n,i)*pom
          else
            XMat(n,i)=1.
          endif
        enddo
      enddo
      deallocate(GMat)
      return
      end
      function ScProdXHarm(n,m)
      include 'fepc.cmn'
c      include 'basic.cmn'
      if(n.eq.1.or.m.eq.1) then
        if(n.eq.m) then
          ScProdXHarm=2./3.
        else
          if(n.eq.1) then
            mp=m
            np=m/2
          else
            mp=n
            np=n/2
          endif
          if(mod(mp,2).eq.1) then
            ScProdXHarm=2./(float(np)*pi)
            if(mod(np,2).eq.0) ScProdXHarm=-ScProdXHarm
          else
            ScProdXHarm=0.
          endif
        endif
      else
        if(n.eq.m) then
          ScProdXHarm=1.
        else
          ScProdXHarm=0.
        endif
      endif
      return
      end
      subroutine TestTOF
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 SvFile
      dimension XTOF(-500:500)
      real ITOF(-500:500,10)
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call UnitMat(F2O,3)
      xomn= 9999.
      xomx=-9999.
      yomn= 0.
      yomx=-100.
      be=1.
      al=5.
      sig=.25
      sum1=0.
      sum2=0.
      etaPwd=0.
      sigp=sig**2
      fwhm=sig*sqrt(5.545177)
      do i=-500,500
        XTOF(i)=float(i)*.002
        ITOF(i,1)=ConvTOFWithGauss(XTOF(i),al,be,fwhm,dx,dal,dbe,dsig)

c        ITOF(i,2)=dx
c        ITOF(i,3)=(ConvTOFWithGauss(XTOF(i)+.01,al,be,fwhm,dap,dbp,dxp,
c     1                              dgp)-ITOF(i,1))*100.

c        ITOF(i,2)=dal
c        ITOF(i,3)=(ConvTOFWithGauss(XTOF(i),al+.1,be,fwhm,dap,dbp,dxp,
c     1                              dgp)-ITOF(i,1))*10.

c        ITOF(i,2)=dbe
c        ITOF(i,3)=(ConvTOFWithGauss(XTOF(i),al,be+.01,fwhm,dap,dbp,dxp,
c     1                              dgp)-ITOF(i,1))*100.

        ITOF(i,2)=dsig
        ITOF(i,3)=(ConvTOFWithGauss(XTOF(i),al,be,fwhm+.01,dap,dbp,dxp,
     1                              dgp)-ITOF(i,1))*100.


c        ITOF(i,4)=ConvTOFWithGauss(XTOF(i),al,be,sig,dx,dal,dbe,dsig)
c        ITOF(i,2)=ConvTOFWithGauss(XTOF(i),al,be,sig,dx,dal,dbe,dsig)
c        ITOF(i,2)=(ConvTOFWithGauss(XTOF(i),al,be,sig+.01,dap,dbp,dxp,
c     1                              dgp)-ITOF(i,1))*100.
c        ITOF(i,1)=dsig
        xomn=min(xomn,XTOF(i))
        xomx=max(xomx,XTOF(i))
c        yomn=min(yomn,ITOF(i,1),ITOF(i,2))
c        yomx=max(yomx,ITOF(i,1),ITOF(i,2))
        yomn=min(yomn,ITOF(i,1),ITOF(i,2),ITOF(i,3))
        yomx=max(yomx,ITOF(i,1),ITOF(i,2),ITOF(i,3))
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'TOF')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      do i=1,3
        if(i.eq.1) then
          j=Red
        else if(i.eq.2) then
          j=Green
        else if(i.eq.3) then
          j=Magenta
        else if(i.eq.4) then
          j=Khaki
        else if(i.eq.5) then
          j=Blue
        endif
        call FeXYPlot(XTOF(-500),ITOF(-500,i),1001,NormalLine,
     1                  NormalPlotMode,j)
      enddo
      call FeReleaseOutput
      call FeDeferOutput
6000  call FeEvent(0)
      if(EventType.ne.EventKey.or.EventNumber.ne.JeEscape) go to 6000
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
      subroutine HeapI(n,ra,sgn)
      integer ra(n),rra,sgn
      if(n.le.1) go to 9999
      l=n/2+1
      ir=n
10    continue
      if(l.gt.1) then
        l=l-1
        rra=ra(l)
      else
        rra=ra(ir)
        ra(ir)=ra(1)
        ir=ir-1
        if(ir.eq.1) then
          ra(1)=rra
          go to 9999
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if((sgn.lt.0.and.ra(j).gt.ra(j+1)).or.
     1       (sgn.gt.0.and.ra(j).lt.ra(j+1))) j=j+1
        endif
        if((sgn.lt.0.and.rra.gt.ra(j)).or.
     1     (sgn.gt.0.and.rra.lt.ra(j))) then
          ra(i)=ra(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      ra(i)=rra
      go to 10
9999  return
      end
      subroutine HeapII(n,ra,rb,sgn)
      integer ra(n),rb(n),rra,rrb,sgn
      if(n.le.1) go to 9999
      l=n/2+1
      ir=n
10    continue
      if(l.gt.1) then
        l=l-1
        rra=ra(l)
        rrb=rb(l)
      else
        rra=ra(ir)
        rrb=rb(ir)
        ra(ir)=ra(1)
        rb(ir)=rb(1)
        ir=ir-1
        if(ir.eq.1) then
          ra(1)=rra
          rb(1)=rrb
          go to 9999
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if((sgn.lt.0.and.ra(j).gt.ra(j+1)).or.
     1       (sgn.gt.0.and.ra(j).lt.ra(j+1))) j=j+1
        endif
        if((sgn.lt.0.and.rra.gt.ra(j)).or.
     1     (sgn.gt.0.and.rra.lt.ra(j))) then
          ra(i)=ra(j)
          rb(i)=rb(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      ra(i)=rra
      rb(i)=rrb
      go to 10
9999  return
      end
      subroutine HeapR(n,ra,sgn)
      dimension ra(n)
      integer sgn
      if(n.le.1) go to 9999
      l=n/2+1
      ir=n
10    continue
      if(l.gt.1) then
        l=l-1
        rra=ra(l)
      else
        rra=ra(ir)
        ra(ir)=ra(1)
        ir=ir-1
        if(ir.eq.1) then
          ra(1)=rra
          go to 9999
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if((sgn.lt.0.and.ra(j).gt.ra(j+1)).or.
     1       (sgn.gt.0.and.ra(j).lt.ra(j+1))) j=j+1
        endif
        if((sgn.lt.0.and.rra.gt.ra(j)).or.
     1     (sgn.gt.0.and.rra.lt.ra(j))) then
          ra(i)=ra(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      ra(i)=rra
      go to 10
9999  return
      end
      subroutine HeapRII(n,ra,rb,rc,sgn)
      dimension ra(n)
      integer rb(n),rc(n),sgn
      if(n.le.1) go to 9999
      l=n/2+1
      ir=n
10    continue
      if(l.gt.1) then
        l=l-1
        rra=ra(l)
        irb=rb(l)
        irc=rc(l)
      else
        rra=ra(ir)
        ra(ir)=ra(1)
        irb=rb(ir)
        rb(ir)=rb(1)
        irc=rc(ir)
        rc(ir)=rc(1)
        ir=ir-1
        if(ir.eq.1) then
          ra(1)=rra
          rb(1)=irb
          rc(1)=irc
          go to 9999
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if((sgn.lt.0.and.ra(j).gt.ra(j+1)).or.
     1       (sgn.gt.0.and.ra(j).lt.ra(j+1))) j=j+1
        endif
        if((sgn.lt.0.and.rra.gt.ra(j)).or.
     1     (sgn.gt.0.and.rra.lt.ra(j))) then
          ra(i)=ra(j)
          rb(i)=rb(j)
          rc(i)=rc(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      ra(i)=rra
      rb(i)=irb
      rc(i)=irc
      go to 10
9999  return
      end
      subroutine indexx(n,arrin,indx)
      dimension arrin(n),indx(n)
      integer arrin,q
      if(n.eq.0) then
        return
      else if(n.eq.1) then
        indx(1)=1
        return
      endif
      do j=1,n
        indx(j)=j
      enddo
      l=n/2+1
      ir=n
10    if(l.gt.1) then
        l=l-1
        indxt=indx(l)
        q=arrin(indxt)
      else
        indxt=indx(ir)
        q=arrin(indxt)
        indx(ir)=indx(1)
        ir=ir-1
        if(ir.eq.1) then
          indx(1)=indxt
          return
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if(arrin(indx(j)).lt.arrin(indx(j+1))) j=j+1
        endif
        if(q.lt.arrin(indx(j))) then
          indx(i)=indx(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      indx(i)=indxt
      go to 10
      end
      subroutine rindexx(n,arrin,indx)
      dimension arrin(n),indx(n)
      real arrin,q
      if(n.eq.0) then
        return
      else if(n.eq.1) then
        indx(1)=1
        return
      endif
      do j=1,n
        indx(j)=j
      enddo
      l=n/2+1
      ir=n
10    if(l.gt.1) then
        l=l-1
        indxt=indx(l)
        q=arrin(indxt)
      else
        indxt=indx(ir)
        q=arrin(indxt)
        indx(ir)=indx(1)
        ir=ir-1
        if(ir.eq.1) then
          indx(1)=indxt
          return
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if(arrin(indx(j)).lt.arrin(indx(j+1))) j=j+1
        endif
        if(q.lt.arrin(indx(j))) then
          indx(i)=indx(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      indx(i)=indxt
      go to 10
      end
      subroutine SortText(n,arrin,indx)
      dimension indx(n)
      character*(*) arrin(n)
      character*256 q
      if(n.eq.0) then
        return
      else if(n.eq.1) then
        indx(1)=1
        return
      endif
      do j=1,n
        indx(j)=j
      enddo
      l=n/2+1
      ir=n
10    continue
      if(l.gt.1) then
        l=l-1
        indxt=indx(l)
        q=arrin(indxt)
      else
        indxt=indx(ir)
        q=arrin(indxt)
        indx(ir)=indx(1)
        ir=ir-1
        if(ir.eq.1) then
          indx(1)=indxt
          return
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if(arrin(indx(j)).lt.arrin(indx(j+1))) j=j+1
        endif
        if(q.lt.arrin(indx(j))) then
          indx(i)=indx(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      indx(i)=indxt
      go to 10
      end
      subroutine SortTextIgnoreCase(n,arrin,indx)
      dimension indx(n)
      character*(*) arrin(n)
      character*256 arrinp(:)
      character*256 q
      allocatable arrinp
      if(n.eq.0) then
        go to 9999
      else if(n.eq.1) then
        indx(1)=1
        go to 9999
      endif
      allocate(arrinp(n))
      do j=1,n
        indx(j)=j
        arrinp(j)=arrin(j)
        call mala(arrinp(j))
      enddo
      l=n/2+1
      ir=n
10    continue
      if(l.gt.1) then
        l=l-1
        indxt=indx(l)
        q=arrinp(indxt)
      else
        indxt=indx(ir)
        q=arrinp(indxt)
        indx(ir)=indx(1)
        ir=ir-1
        if(ir.eq.1) then
          indx(1)=indxt
          go to 9000
        endif
      endif
      i=l
      j=l+l
20    if(j.le.ir) then
        if(j.lt.ir) then
          if(arrinp(indx(j)).lt.arrinp(indx(j+1))) j=j+1
        endif
        if(q.lt.arrinp(indx(j))) then
          indx(i)=indx(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        go to 20
      endif
      indx(i)=indxt
      go to 10
9000  deallocate(arrinp)
9999  return
      end
      subroutine SmiHandleSing(a,n,NSingArr,NSing)
      dimension a(*),h(:),NSingArr(*)
      allocatable h
      data diff/.000001/
      call SetIntArrayTo(NSingArr,n,0)
      if(n.eq.0) then
        go to 9999
      else if(n.eq.1) then
        if(a(1).ne.0.) then
          a(1)=1./a(1)
        else
          NSing=1
          NSingArr(1)=1
        endif
        go to 9999
      endif
      NSing=0
      allocate(h(n))
      do k=n,1,-1
        p=a(1)
        if(p.lt.diff) then
          NSing=NSing+1
          NSingArr(NSing)=n+1-k
          a(1)=1.
          ij=1
          do i=1,n-1
            ij=ij+i
            a(ij)=0.
          enddo
          p=1.
        endif
        p=1./p
        ii=1
        do i=2,n
          m=ii
          ii=ii+i
          q=a(m+1)
          hi=q*p
          if(i.le.k) hi=-hi
          h(i)=hi
          m2=m+2
          do ij=m2,ii
            a(ij-i)=a(ij)+q*h(ij-m)
          enddo
        enddo
        m=m-1
        a(ii)=p
        do i=2,n
          a(m+i)=h(i)
        enddo
      enddo
      deallocate(h)
9999  return
      end
      subroutine smi(a,n,ising)
      dimension a(*),h(:)
      allocatable h
      data diff/.000001/
      allocate(h(n))
      ising=0
      if(n.eq.0) then
        go to 9999
      else if(n.eq.1) then
        if(a(1).ne.0.) then
          a(1)=1./a(1)
        else
          ising=1
        endif
        go to 9999
      endif
      do k=n,1,-1
        p=a(1)
        if(p.lt.diff) then
          ising=n+1-k
          exit
        endif
        p=1./p
        ii=1
        do i=2,n
          m=ii
          ii=ii+i
          q=a(m+1)
          hi=q*p
          if(i.le.k) hi=-hi
          h(i)=hi
          m2=m+2
          do ij=m2,ii
            a(ij-i)=a(ij)+q*h(ij-m)
          enddo
        enddo
        m=m-1
        a(ii)=p
        do i=2,n
          a(m+i)=h(i)
        enddo
      enddo
9999  deallocate(h)
      return
      end
      subroutine smiold(a,h,n,ising)
      dimension a(*),h(*)
      ising=0
      ij=0
      do 500i=1,n
        ij=ij+i
        p=a(ij)
        if(p.gt.0.) go to 500
        ising=i
        go to 9999
500   continue
      if(n.le.1) then
        a(1)=1./a(1)
        go to 9999
      endif
      do k=n,1,-1
        p=a(1)
        if(p.le.0.) then
          ising=n+1-k
          go to 9999
        endif
        p=1./p
        ii=1
        do i=2,n
          m=ii
          ii=ii+i
          q=a(m+1)
          hi=q*p
          if(i.le.k) hi=-hi
          h(i)=hi
          m2=m+2
          do ij=m2,ii
            iji=ij-i
            ijm=ij-m
            a(iji)=a(ij)+q*h(ijm)
          enddo
        enddo
        m=m-1
        a(ii)=p
        do i=2,n
          ijm=m+i
          a(ijm)=h(i)
        enddo
      enddo
9999  return
      end
      subroutine nasob(a,p,x,n)
      dimension a(*),p(n),x(n)
      ijp=0
      do i=1,n
        xi=0.0
        ij=ijp
        id=1
        do j=1,n
          ij=ij+id
          xi=xi+a(ij)*p(j)
          if(j.ge.i) id=j
        enddo
        x(i)=xi
        ijp=ijp+i
      enddo
      return
      end
      subroutine vecnor(u,ug)
      dimension u(3),ug(3)
      pom=scalmul(u,ug)
      if(pom.gt.0.) then
        pom=1./sqrt(pom)
        do i=1,3
          ug(i)=ug(i)*pom
          u(i)=u(i)*pom
        enddo
      endif
      return
      end
      subroutine VecMul(a,b,c)
      dimension a(3),b(3),c(3)
      c(1)=a(2)*b(3)-a(3)*b(2)
      c(2)=a(3)*b(1)-a(1)*b(3)
      c(3)=a(1)*b(2)-a(2)*b(1)
      return
      end
      function scalmul(u,vg)
      dimension u(*),vg(*)
      scalmul=0.
      do i=1,3
        scalmul=scalmul+u(i)*vg(i)
      enddo
      return
      end
      function VecOrtLen(v,n)
      dimension v(n)
      VecOrtLen=0.
      do i=1,n
        VecOrtLen=VecOrtLen+v(i)**2
      enddo
      VecOrtLen=sqrt(VecOrtLen)
      return
      end
      function VecOrtScal(u,v,n)
      dimension u(n),v(n)
      VecOrtScal=0.
      do i=1,n
        VecOrtScal=VecOrtScal+u(i)*v(i)
      enddo
      return
      end
      subroutine VecOrtNorm(v,n)
      dimension v(n)
      pom=VecOrtLen(v,n)
      if(pom.le.0.) go to 9999
      pom=1./pom
      do i=1,n
        v(i)=v(i)*pom
      enddo
9999  return
      end
      subroutine cultm(a,b,c,n1,n2,n3)
      dimension a(*),b(*),c(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=c(ik)
          do j=1,n2
            p=p+a(ij)*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=p
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine cultmq(a,b,c,n1,n2,n3)
      dimension a(*),b(*),c(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=c(ik)**2
          do j=1,n2
            p=p+(a(ij)*b(jk))**2
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=sqrt(p)
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine multm(a,b,c,n1,n2,n3)
      dimension a(*),b(*),c(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=0.
          do j=1,n2
            p=p+a(ij)*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=p
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine multmc(a,b,c,n1,n2,n3)
      complex a(*),b(*),c(*),p
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=(0.,0.)
          do j=1,n2
            p=p+a(ij)*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=p
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine multmq(a,b,c,n1,n2,n3)
      dimension a(*),b(*),c(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=0.
          do j=1,n2
            p=p+(a(ij)*b(jk))**2
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=sqrt(p)
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine multmi(a,b,c,n1,n2,n3)
      integer a(*),b(*),c(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          ip=0
          do j=1,n2
            ip=ip+a(ij)*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=ip
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine MultMIRI(a,b,c,n1,n2,n3)
      dimension b(*)
      integer a(*),c(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=0.
          do j=1,n2
            p=p+float(a(ij))*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=nint(p)
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine MultMIRR(a,b,c,n1,n2,n3)
      dimension b(*),c(*)
      integer a(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=0.
          do j=1,n2
            p=p+float(a(ij))*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=p
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine MultMRRI(a,b,c,n1,n2,n3,ich)
      dimension a(*),b(*)
      integer c(*)
      ich=0
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=0.
          do j=1,n2
            p=p+a(ij)*b(jk)
            ij=ij+n1
            jk=jk+1
          enddo
          ip=nint(p)
          if(abs(p-float(ip)).gt..001) then
            ich=1
            go to 9999
          else
            c(ik)=ip
          endif
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
9999  return
      end
      subroutine MultMRIR(a,b,c,n1,n2,n3)
      dimension a(*),c(*)
      integer b(*)
      do i=1,n1
        jkp=1
        ik=i
        do k=1,n3
          ij=i
          jk=jkp
          p=0.
          do j=1,n2
            p=p+a(ij)*float(b(jk))
            ij=ij+n1
            jk=jk+1
          enddo
          c(ik)=p
          ik=ik+n1
          jkp=jkp+n2
        enddo
      enddo
      return
      end
      subroutine MatVek(m,v,p,n)
      dimension m(n,n),v(n),p(n)
      do i=1,n
        pi=0.
        do j=1,n
          pi=pi+float(m(i,j))*v(j)
        enddo
        p(i)=pi
      enddo
      return
      end
      subroutine TrMat(a,b,n,m)
      dimension a(n,m),b(m,n)
      do j=1,m
        do i=1,n
          b(j,i)=a(i,j)
        enddo
      enddo
      return
      end
      subroutine MatTransConjg(A,B,n)
      complex A(n,n),B(n,n)
      do i=1,n
        do j=1,n
          B(j,i)=conjg(A(i,j))
        enddo
      enddo
      return
      end
      subroutine MatConjg(A,B,n)
      complex A(n,n),B(n,n)
      do i=1,n
        do j=1,n
          B(i,j)=conjg(A(i,j))
        enddo
      enddo
      return
      end
      logical function eqiv(a,b,n)
      integer a(n),b(n)
      eqiv=.false.
      do i=1,n
        if(a(i).ne.b(i)) go to 2000
      enddo
      eqiv=.true.
2000  return
      end
      logical function eqivm(a,b,n)
      integer a(n),b(n)
      eqivm=.false.
      do i=1,n
        if(a(i).ne.-b(i)) go to 2000
      enddo
      eqivm=.true.
2000  return
      end
      logical function eqiv0(a,n)
      integer a(n)
      eqiv0=.false.
      do i=1,n
        if(a(i).ne.0) go to 2000
      enddo
      eqiv0=.true.
2000  return
      end
      logical function eqrv0(a,n,dif)
      dimension a(n)
      eqrv0=.false.
      do i=1,n
        if(abs(a(i)).gt.dif) go to 2000
      enddo
      eqrv0=.true.
2000  return
      end
      logical function eqrv(a,b,n,dif)
      dimension a(n),b(n)
      eqrv=.false.
      do i=1,n
        if(abs(a(i)-b(i)).ge.dif) go to 9999
      enddo
      eqrv=.true.
9999  return
      end
      logical function eqrvm(a,b,n,dif)
      dimension a(n),b(n)
      eqrvm=.false.
      do i=1,n
        if(abs(a(i)+b(i)).ge.dif) go to 9999
      enddo
      eqrvm=.true.
9999  return
      end
      logical function eqrvlt0(a,n)
      dimension a(n)
      n0=0
      do i=1,n
        if(a(i).gt.0.) then
          eqrvlt0=.false.
          go to 9999
        else if(a(i).eq.0.) then
          n0=n0+1
        endif
      enddo
      eqrvlt0=n0.lt.n
9999  return
      end
      logical function eqcv(a,b,n,dif)
      complex a(n),b(n)
      eqcv=.false.
      do i=1,n
        if(abs(a(i)-b(i)).ge.dif) go to 9999
      enddo
      eqcv=.true.
9999  return
      end
      logical function eqcv0(a,n,dif)
      complex a(n)
      eqcv0=.false.
      do i=1,n
        if(abs(a(i)).gt.dif) go to 2000
      enddo
      eqcv0=.true.
2000  return
      end
      logical function MatRealEqUnitMat(RM,n,Toll)
      dimension RM(n,n)
      MatRealEqUnitMat=.true.
      do i=1,n
        do j=1,n
          if(i.eq.j) then
            p=1.
          else
            p=0.
          endif
          if(abs(RM(i,j)-p).gt.Toll) then
            MatRealEqUnitMat=.false.
            go to 9999
          endif
        enddo
      enddo
9999  return
      end
      logical function MatRealEqMinusUnitMat(RM,n,Toll)
      dimension RM(n,n)
      MatRealEqMinusUnitMat=.true.
      do i=1,n
        do j=1,n
          if(i.eq.j) then
            p=-1.
          else
            p=0.
          endif
          if(abs(RM(i,j)-p).gt.Toll) then
            MatRealEqMinusUnitMat=.false.
            go to 9999
          endif
        enddo
      enddo
9999  return
      end
      subroutine RealMatrixToOpposite(rmi,rmo,n)
      dimension rmi(*),rmo(*)
      do i=1,n**2
        rmo(i)=-rmi(i)
      enddo
      return
      end
      subroutine IntMatrixToOpposite(rmi,rmo,n)
      integer rmi(*),rmo(*)
      do i=1,n**2
        rmo(i)=-rmi(i)
      enddo
      return
      end
      subroutine RealVectorToOpposite(vi,vo,n)
      dimension vi(*),vo(*)
      do i=1,n
        vo(i)=-vi(i)
      enddo
      return
      end
      subroutine IntVectorToOpposite(vi,vo,n)
      integer vi(*),vo(*)
      do i=1,n
        vo(i)=-vi(i)
      enddo
      return
      end
      subroutine UnitMat(a,n)
      dimension a(n,n)
      do i=1,n
        do j=1,n
          if(i.eq.j) then
            a(i,j)=1.
          else
            a(i,j)=0.
          endif
        enddo
      enddo
      return
      end
      subroutine UnitMatC(a,n)
      complex a(n,n)
      do i=1,n
        do j=1,n
          if(i.eq.j) then
            a(i,j)=(1.,0.)
          else
            a(i,j)=(0.,0.)
          endif
        enddo
      enddo
      return
      end
      subroutine UnitMatI(a,n)
      integer a(n,n)
      do i=1,n
        do j=1,n
          if(i.eq.j) then
            a(i,j)=1
          else
            a(i,j)=0
          endif
        enddo
      enddo
      return
      end
      subroutine SetPermutMat(RMat,n,iord,ich)
      dimension Rmat(n,n)
      call SetRealArrayTo(RMat,n**2,0.)
      ich=0
      k=iord
      m=10**(n-1)
      do i=1,n
        j=k/m
        if(j.lt.1.or.j.gt.3) go to 9000
        RMat(i,j)=1.
        k=k-j*m
        m=m/10
      enddo
      do i=1,n
        sum=0.
        do j=1,n
          sum=sum+RMat(j,i)
          if(sum.gt.1.1) go to 9000
        enddo
        if(sum.lt..1) go to 9000
      enddo
      go to 9999
9000  ich=1
9999  return
      end
      subroutine SetRealArrayTo(Array,n,Value)
      dimension Array(*)
      do i=1,n
        Array(i)=Value
      enddo
      return
      end
      subroutine SetComplexArrayTo(Array,n,Value)
      complex Array(*),Value
      do i=1,n
        Array(i)=Value
      enddo
      return
      end
      subroutine SetStringArrayTo(Array,n,Value)
      character*(*) Array(*),Value
      do i=1,n
        Array(i)=Value
      enddo
      return
      end
      subroutine SetLogicalArrayTo(Array,n,Value)
      logical Array(*),Value
      do i=1,n
        Array(i)=Value
      enddo
      return
      end
      subroutine SetDoubleRealArrayTo(Array,n,Value)
      double precision Array(*)
      do i=1,n
        Array(i)=Value
      enddo
      return
      end
      subroutine SetIntArrayTo(Array,n,Value)
      integer Array(*),Value
      do i=1,n
        Array(i)=Value
      enddo
      return
      end
      subroutine CopyMat(a,b,n)
      dimension a(n,n),b(n,n)
      do i=1,n
        do j=1,n
          b(i,j)=a(i,j)
        enddo
      enddo
      return
      end
      subroutine CopyMatC(a,b,n)
      complex a(n,n),b(n,n)
      do i=1,n
        do j=1,n
          b(i,j)=a(i,j)
        enddo
      enddo
      return
      end
      subroutine CopyMatI(a,b,n)
      integer a(n,n),b(n,n)
      do i=1,n
        do j=1,n
          b(i,j)=a(i,j)
        enddo
      enddo
      return
      end
      subroutine CopyVek(a,b,n)
      dimension a(n),b(n)
      do i=1,n
        b(i)=a(i)
      enddo
      return
      end
      subroutine AddVek(x,y,z,n)
      dimension x(n),y(n),z(n)
      do i=1,n
        z(i)=x(i)+y(i)
      enddo
      return
      end
      subroutine AddVekI(x,y,z,n)
      integer x(n),y(n),z(n)
      do i=1,n
        z(i)=x(i)+y(i)
      enddo
      return
      end
      subroutine CopyVekC(a,b,n)
      complex a(n),b(n)
      do i=1,n
        b(i)=a(i)
      enddo
      return
      end
      subroutine CopyStringArray(StringIn,StringOut,n)
      character*(*) StringIn(*),StringOut(*)
      do i=1,n
        StringOut(i)=StringIn(i)
      enddo
      return
      end
      subroutine AddVekQ(x,y,z,n)
      dimension x(n),y(n),z(n)
      do i=1,n
        z(i)=sqrt(x(i)**2+y(i)**2)
      enddo
      return
      end
      subroutine CopyVekI(a,b,n)
      integer a(n),b(n)
      do i=1,n
        b(i)=a(i)
      enddo
      return
      end
      subroutine MatBlock3(rm,r3,n)
      dimension rm(*),r3(3,3)
      do j=1,3
        do i=1,3
          r3(i,j)=rm(i+(j-1)*n)
        enddo
      enddo
      return
      end
      subroutine MatFromBlock3(r3,rm,n)
      include 'fepc.cmn'
      dimension rm(*),r3(3,3)
      call UnitMat(rm,n)
      do j=1,3
        do i=1,3
          rm(i+(j-1)*n)=r3(i,j)
        enddo
      enddo
      return
      end
      subroutine SetGenRot(fi,u,isgn,r)
      dimension r(3,3),u(3)
      sgn=isgn
      cs=sgn*cos(fi)
      sn=sgn*sin(fi)
      call UnitMat(r,3)
      do i=1,3
        r(i,i)=r(i,i)*cs
      enddo
      do i=1,3
        do j=1,3
          if(i.ne.j) then
            k=6-i-j
            pom=sn*u(k)
            if(mod(2*i+j,3).eq.1) pom=-pom
            r(i,j)=r(i,j)+pom
          endif
        enddo
      enddo
      cs=sgn*(1.-sgn*cs)
      do i=1,3
        do j=1,3
          r(i,j)=r(i,j)+u(i)*u(j)*cs
        enddo
      enddo
      return
      end
      integer function ColorOrder(ColorString)
      include 'fepc.cmn'
      character*(*) ColorString
      logical   EqIgCase
      ColorOrder=0
      do i=1,pocetbarev
        if(EqIgCase(ColorString,ColorNames(i))) then
          ColorOrder=i
          exit
        endif
      enddo
      return
      end
      function LocateInArray(x,ax,n,dx)
      dimension ax(n)
      do i=1,n
        if(abs(x-ax(i)).le.dx) go to 2000
      enddo
      i=0
2000  LocateInArray=i
      return
      end
      function LocateInIntArray(x,ax,n)
      integer ax(n),x
      do i=1,n
        if(x.eq.ax(i)) go to 2000
      enddo
      i=0
2000  LocateInIntArray=i
      return
      end
      function LocateInStringArray(StringArr,n,String,IgnoreCase)
      character*(*) StringArr(n),String
      logical IgnoreCase,EqIgCase
      do i=1,n
        if(StringArr(i).eq.String.or.
     1     (EqIgCase(StringArr(i),String).and.IgnoreCase)) go to 2000
      enddo
      i=0
2000  LocateInStringArray=i
      return
      end
      logical function EqMod1(x,y,dif)
      pom=abs(x-anint(x)-y+anint(y))
      EqMod1=pom.lt.dif.or.pom.gt.1.-dif
      return
      end
      subroutine minmax(xx,yy,xm,xv,ym,yv)
      xm=min(xx,xm)
      xv=max(xx,xv)
      ym=min(yy,ym)
      yv=max(yy,yv)
      return
      end
      function IndPack(ih,ihd,ihm,n)
      dimension ih(n),ihd(n),ihm(n)
      IndPack=ih(1)+ihm(1)
      do i=2,n
        IndPack=IndPack*ihd(i)+ih(i)+ihm(i)
      enddo
      return
      end
      subroutine IndUnpack(m,ih,ihd,ihm,n)
      dimension ih(n),ihd(n),ihm(n)
      j=m
      do i=n,2,-1
        k=mod(j,ihd(i))
        ih(i)=k-ihm(i)
        j=j/ihd(i)
      enddo
      ih(1)=j-ihm(1)
      return
      end
      integer function RecPack(nn,nx,n)
      dimension nn(n),nx(n)
      RecPack=0
      do i=n,1,-1
        RecPack=RecPack*nx(i)+nn(i)-1
      enddo
      RecPack=RecPack+1
      return
      end
      subroutine RecUnpack(m,nn,nx,n)
      dimension nn(n),nx(n)
      j=m-1
      do i=1,n
        nn(i)=mod(j,nx(i))+1
        j=j/nx(i)
      enddo
      return
      end
      subroutine ctimat(m,n)
      dimension m(n,n),mp(9),mpp(9)
      character*2 nty
      character*1 cislo
      do i=1,n
        write(cislo,'(i1)') i
        call ctiint('Type '//cislo//nty(i)//' row of the matrix',
     1               j,j,mp,mpp,n,0)
        do j=1,n
          m(i,j)=mp(j)
        enddo
      enddo
      return
      end
      subroutine ctimatr(r,n)
      dimension r(n,n),p(9),pp(9)
      character*80 t80
      character*2 nty
      character*1 cislo
      data ich/0/
      do i=1,n
        write(cislo,'(i1)') i
        call ctireal('Type '//cislo//nty(i)//' row of the matrix',
     1               pom,pom,p,pp,n,0,0,t80,ich)
        do j=1,n
          r(i,j)=p(j)
        enddo
      enddo
      return
      end
      subroutine kus(radka,k,slovo)
      character*(*) radka,slovo
      mxs=len(slovo)
      mxr=len(radka)
      slovo=' '
      l=0
      if(k.ge.mxr) go to 9999
      k=k+1
1000  if(radka(k:k).eq.' '.or.ichar(radka(k:k)).eq.9) then
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 1000
      endif
2000  if(radka(k:k).ne.' '.and.ichar(radka(k:k)).ne.9) then
        if(l.lt.mxs) then
          l=l+1
          slovo(l:l)=radka(k:k)
        endif
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 2000
      endif
3000  if(radka(k:k).eq.' '.or.ichar(radka(k:k)).eq.9) then
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 3000
      endif
      k=k-1
9999  return
      end
      subroutine kusap(radka,k,slovo)
      character*(*) radka,slovo
      character*1 ZnakKonce
      mxs=len(slovo)
      mxr=len(radka)
      slovo=' '
      l=0
      if(k.ge.mxr) go to 9999
      k=k+1
1000  if(radka(k:k).eq.' ') then
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 1000
      endif
      if(radka(k:k).eq.'''') then
        ZnakKonce=''''
        k=k+1
      else if(radka(k:k).eq.'"') then
        ZnakKonce='"'
        k=k+1
      else
        ZnakKonce=' '
      endif
2000  if(radka(k:k).ne.ZnakKonce) then
        if(l.lt.mxs) then
          l=l+1
          slovo(l:l)=radka(k:k)
        endif
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 2000
      endif
      if(ZnakKonce.ne.' ') k=k+1
3000  if(radka(k:k).eq.' ') then
        if(k.ge.mxr) go to 9999
        k=k+1
        go to 3000
      endif
      k=k-1
9999  return
      end
      function idel(l)
      character*(*) l
      idel=len(l)
1000  if(idel.ne.0) then
        if(l(idel:idel).eq.' ') then
          idel=idel-1
          go to 1000
        endif
      endif
      return
      end
      subroutine RozdelMezerou(Veta,Znak)
      character*(*) Veta
      character*512 String
      character*1  Znak
      String=' '
      is=0
      do i=1,idel(Veta)
        if(i.gt.1) then
          if(Veta(i:i).eq.Znak.and.Veta(i-1:i-1).ne.' ') then
            is=is+1
            String(is:is)=' '
          endif
        endif
        is=is+1
        String(is:is)=Veta(i:i)
      enddo
      Veta=String
      return
      end
      subroutine GetNextReal(radka,k,array,n,istat)
C istat=0 ... OK; istat=n ... malo slov; -n ... read error
C n je cislo slova, u nehoz nastala chyba
      dimension array(*),irray(*)
      character*10 fmt
      character*80 slovo
      character*(*) radka
      ifl=1
      go to 400
      entry GetNextInteger(radka,k,irray,n,istat)
      fmt='(i80)'
      ifl=0
400   call zhusti(fmt)
      id=idel(radka)
      do i=1,n
        if(k.ge.id) then
          istat=i
          go to 9999
        endif
        call kus(radka,k,slovo)
        call posun(slovo,ifl)
        if(ifl.eq.1) then
          ipom=idel(slovo)-index(slovo,'.')
          if(ipom.lt.10) then
            write(fmt,'(''(f80.'',i1,'')'')') ipom
          else
            write(fmt,'(''(f80.'',i2,'')'')') ipom
          endif
          read(slovo,fmt,err=9100) array(i)
        else
          read(slovo,fmt,err=9100) irray(i)
        endif
      enddo
      istat=0
      go to 9999
9100  istat=-i
9999  return
      end
      subroutine zhusti(s)
      character*(*) s
      idl=idel(s)
      k=0
      do i=1,idl
        if(s(i:i).eq.' ') cycle
        k=k+1
        if(k.ne.i) then
          s(k:k)=s(i:i)
          s(i:i)=' '
        endif
      enddo
      return
      end
      subroutine DelChar(Veta,Znak)
      character*(*) Veta
      character*1   Znak
      character*80 t80
      t80=Veta
      i=index(t80,Znak)
      if(i.eq.0) then
        return
      else
        if(i.gt.1) then
          if(i.lt.idel(t80)) then
            Veta=t80(1:i-1)//t80(i+1:)
          else
            Veta=t80(1:i-1)
          endif
        else
          Veta=t80(2:)
        endif
      endif
      return
      end
      subroutine DeleteFirstSpaces(Veta)
      character*(*) Veta
      if(Veta.ne.' ') then
        i=1
1000    if(Veta(i:i).eq.' ') then
          i=i+1
          go to 1000
        endif
        Veta=Veta(i:)
      endif
      return
      end
      subroutine FeDelTwoSpaces(s)
      character*(*) s
      k=0
      id=idel(s)
      do 1000i=1,id
        if(i.eq.id) go to 500
        if(s(i:i+1).eq.'  ') go to 1000
500     k=k+1
        if(k.ne.i) then
          s(k:k)=s(i:i)
          s(i:i)=' '
        endif
1000  continue
      return
      end
      subroutine ChangeCommasForPoints(Radka)
      character*(*) Radka
      do i=1,idel(Radka)
        if(Radka(i:i).eq.',') Radka(i:i)='.'
      enddo
      return
      end
      subroutine posun(cislo,tecka)
      character*(*) cislo
      integer tecka
      l=idel(cislo)
      m=len(cislo)
      if(tecka.eq.1.and.index(cislo,'.').le.0.and.l.lt.m) then
        l=l+1
        cislo(l:l)='.'
      endif
      if(l.lt.m) then
        do i=l,1,-1
          cislo(m-l+i:m-l+i)=cislo(i:i)
          cislo(i:i)=' '
        enddo
      endif
      return
      end
      subroutine Mala(Veta)
      character*(*) Veta
      character*80 Pismena(2)
      data Pismena
     1     /'abcdefghijklmnopqrstuvwxyz',
     2      'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
      IndIn=2
      IndOut=1
      go to 1000
      entry Velka(Veta)
      IndIn=1
      IndOut=2
1000  idl=44
      do i=1,idel(Veta)
        j=index(Pismena(IndIn)(:idl),Veta(i:i))
        if(j.gt.0) Veta(i:i)=Pismena(IndOut)(j:j)
      enddo
      return
      end
      subroutine ChangeCase(String)
      character*(*) String
      character*1   Letter
      do i=1,idel(String)
        Letter=String(i:i)
        call Mala(Letter)
        if(Letter.eq.String(i:i)) then
          call Velka(String(i:i))
        else
          String(i:i)=Letter
        endif
      enddo
      return
      end
      logical function EqIgProcenta(Veta1,Veta2)
      character*(*) Veta1,Veta2
      character*256 Veta1P,Veta2P
      logical EqIgCase
      Veta1P=Veta1
      k=0
      call DeleteString(Veta1P,'%',k)
      Veta2P=Veta2
      k=0
      call DeleteString(Veta2P,'%',k)
      EqIgProcenta=EqIgCase(Veta1P,Veta2P)
      return
      end
      logical function EqIgCase(St1,St2)
      character*(*) St1,St2
      character*1   CharMal
      id1=idel(St1)
      id2=idel(St2)
      EqIgCase=.false.
      if(id1.ne.id2) go to 9999
      do i=1,id1
        if(CharMal(St1(i:i)).ne.CharMal(St2(i:i))) go to 9999
      enddo
      EqIgCase=.true.
9999  return
      end
      function CharMal(St)
      character*1 charmal,St
      CharMal=St
      call mala(CharMal)
      return
      end
      subroutine uprap(format)
      character*(*) format
      character*80  t80
      j=1
      t80=' '
      do i=1,idel(format)
        if(format(i:i).eq.'''') then
          t80(j:j+1)=''''''
          j=j+2
        else
          t80(j:j)=format(i:i)
          j=j+1
        endif
        if(j.eq.80) go to 2000
      enddo
2000  format=t80(1:j)
      return
      end
      subroutine SkrtniPrvniMezery(St)
      character*(*) St
      if(St.eq.' ') go to 9999
1000  if(St(1:1).eq.' ') then
        St=St(2:)
        go to 1000
      endif
9999  return
      end
      subroutine DeleteString(String,ToDelete,Position)
      character*(*) String,ToDelete
      integer Position
      id=idel(ToDelete)
      if(id.le.0) then
        Position=0
      else
        Position=index(String,ToDelete)
        if(Position.gt.0) then
          if(Position.eq.1) then
            String=String(Position+id:)
          else if(Position+id-1.ge.idel(String)) then
            String=String(:Position-1)
          else
            String=String(:Position-1)//String(Position+id:)
          endif
        endif
      endif
      return
      end
      subroutine ReplaceString(String,ToFind,ToPut,Position)
      character*(*) String,ToFind,ToPut
      integer Position
      id1=idel(ToFind)
      id2=idel(ToPut)
      if(id1.le.0.or.id2.le.0) then
        Position=0
      else
        Position=index(String,ToFind)
        if(Position.gt.0) then
          if(Position.eq.1) then
            String=ToPut(:id2)//String(Position+id1:)
          else if(Position+id1-1.ge.idel(String)) then
            String=String(:Position-1)//ToPut(:id2)
          else
            String=String(:Position-1)//ToPut(:id2)//
     1             String(Position+id1:)
          endif
        endif
      endif
      return
      end
      integer function LocateSubstring(String,SubString,CaseSensitive,
     1                                 FromLeft)
      character*(*) String,SubString
      logical CaseSensitive,FromLeft,EqIgCase
      idl =idel(String)
      idls=len(SubString)
      LocateSubstring=0
      if(idls.gt.idl) go to 9999
      if(FromLeft) then
        ip=1
        ik=idl-idls+1
        is=1
      else
        ip=idl-idls+1
        ik=1
        is=-1
      endif
      do i=ip,ik,is
        if(CaseSensitive) then
          if(String(i:i+idls-1).eq.SubString) go to 2000
        else
          if(EqIgCase(String(i:i+idls-1),SubString)) go to 2000
        endif
      enddo
      i=0
2000  LocateSubstring=i
9999  return
      end
      integer function FirstNonSpaceChar(Veta)
      character*(*) Veta
      FirstNonSpaceChar=0
      do i=1,idel(Veta)
        if(Veta(i:i).ne.' ') then
          FirstNonSpaceChar=i
          exit
        endif
      enddo
      return
      end
      subroutine UprAt(at)
      character*(*) at
      call mala(at)
      call velka(at(1:1))
      return
      end
      subroutine Skrtni09(String)
      character*(*) String
      do i=1,idel(String)
        if(ichar(String(i:i)).eq.9) String(i:i)=' '
      enddo
      return
      end
      integer function BinStToInt(String,kp,n)
      character*(*) String
      kk=kp+min(n,4)-1
      BinStToInt=0
      do i=kk,kp,-1
        BinStToInt=BinStToInt*256+ichar(String(i:i))
      enddo
      kp=kk+1
      return
      end
      subroutine IntToBinSt(Int,n,String,kp)
      character*(*) String
      kk=kp+min(n,4)-1
      IntPom=Int
      do i=kp,kk
        j=mod(IntPom,256)
        String(i:i)=char(j)
        IntPom=IntPom/256
      enddo
      kp=kk+1
      return
      end
      subroutine Extrapol(t,dx,a,b,c,d)
      parameter (nn=10)
      dimension xp(3),dx(*),t(0:2,0:2,0:2),b(3),c(6),d(10),
     1          dr(20,0:2,0:2,0:2),px(20),rx(210),sol(20)
      character*80 t80
      logical :: First = .true.
      save dr
      if(First) then
        do i1=0,2
          xp(1)=i1-1
          do i2=0,2
            xp(2)=i2-1
            do i3=0,2
              xp(3)=i3-1
              n=1
              dr(1,i1,i2,i3)=1.
              do j1=1,3
                n=n+1
                dr(n,i1,i2,i3)=xp(j1)
              enddo
              do j1=1,3
                do j2=j1,3
                  n=n+1
                  dr(n,i1,i2,i3)=xp(j1)*xp(j2)
                enddo
              enddo
            enddo
          enddo
        enddo
        First=.false.
      endif
      call SetRealArrayTo(px,nn,0.)
      call SetRealArrayTo(rx,(nn*(nn+1))/2,0.)
      do i1=0,2
        xp(1)=i1-1
        do i2=0,2
          xp(2)=i2-1
          do i3=0,2
            xp(3)=i3-1
            tp=t(i1,i2,i3)
            n=0
            do i=1,nn
              px(i)=px(i)+dr(i,i1,i2,i3)*tp
              do j=1,i
                n=n+1
                rx(n)=rx(n)+dr(i,i1,i2,i3)*dr(j,i1,i2,i3)
              enddo
            enddo
          enddo
        enddo
      enddo



      n=0
      do i=1,nn
        n=n+i
        sol(i)=1./sqrt(rx(n))
      enddo
      call znorm(rx,sol,nn)
      call smi(rx,nn,ising)
      call znorm(rx,sol,nn)
      if(ising.eq.0) then
        call nasob(rx,px,sol,nn)
        do i1=0,2
          xp(1)=i1-1
          do i2=0,2
            xp(2)=i2-1
            do i3=0,2
              xp(3)=i3-1
              n=1
            px(1)=1.
              do j1=1,3
                n=n+1
                px(n)=xp(j1)
              enddo
              do j1=1,3
                do j2=j1,3
                  n=n+1
                  px(n)=xp(j1)*xp(j2)
                enddo
              enddo
              pom=VecOrtScal(sol,px,nn)
            enddo
          enddo
        enddo
      else
        write(t80,'(i5)') ising
        call FeWinMessage('Je to singular',t80)
      endif
      return
      end
      integer function FeRGBCompress(IRed,IGreen,IBlue)
      include 'fepc.cmn'
      FeRGBCompress=IRed+256*IGreen+65536*IBlue
      return
      end
      subroutine FeRGBUncompress(Colour,IRed,IGreen,IBlue)
      include 'fepc.cmn'
      integer Colour
      i=Colour
      IBlue=i/65536
      i=i-IBlue*65536
      IGreen=i/256
      IRed=i-IGreen*256
      return
      end
      function fract(veta,ich)
      character*(*) veta
      character*15 pomc
      character*5 format
      ich=0
      pomc=veta
      idl=idel(pomc)
      i=index(pomc,'/')
      if(i.le.0) then
        call posun(pomc,1)
        read(pomc,'(f15.0)',err=9000) fract
      else
        format='(i  )'
        if(i.eq.1.or.i.eq.idel(pomc)) then
          fract=0.
          go to 9999
        endif
        write(format(3:4),100) i-1
        read(pomc(1:i-1),format,err=9000) i1
        write(format(3:4),100) idl-i
        read(pomc(i+1:idl),format,err=9000) i2
        fract=float(i1)/float(i2)
      endif
      go to 9999
9000  ich=1
9999  return
100   format(i2)
      end
      subroutine ToFract(x,st,n)
      character*(*) st
      character*10  t10
      do k=1,n
        fk=float(k)
        p=x*fk
        l=nint(p)
        if(abs(p-float(l)).lt..001) go to 1100
      enddo
      go to 9000
1100  if(l.gt.99) go to 9000
      write(t10,'(i3,''/'',i2)') l,k
      if(k.eq.1) t10(4:)='  '
      call zhusti(t10)
      st=t10
      go to 9999
9000  st='?/?'
9999  return
      end
      function GetFract(x,Diff,n)
      GetFract=x
      do k=1,n
        fk=float(k)
        p=x*fk
        l=nint(p)
        if(abs(p-float(l)).lt.Diff) then
          GetFract=float(l)/fk
          exit
        endif
      enddo
      return
      end
      integer function GetDenom(x,n)
      do k=1,n
        GetDenom=float(k)
        p=x*GetDenom
        l=nint(p)
        if(abs(p-float(l)).lt..001) go to 9999
      enddo
      GetDenom=0
9999  return
      end
      function csprod(n,m,x1,x2,natp)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension fc(3),xs(6)
      if(KCommen(KPhase).ge.0.or.natp.le.0) then
        if(n.le.m) then
          nl=n
          ml=m
        else
          nl=m
          ml=n
        endif
        if(nl.gt.1) then
          i=isign(iabs(nl)/2,nl)
          a=pi2*float(i)
        endif
        if(ml.gt.1) then
          i=isign(iabs(ml)/2,nl)
          b=pi2*float(i)
        endif
        i=isign(iabs(nl)/2,nl)
        knl=mod(nl,2)
        kml=mod(ml,2)
        if(nl.eq.1) then
          if(ml.eq.1) then
            csprod=x2-x1
          else if(kml.eq.0) then
            csprod=-1./b*(cosnas(b*x2)-cosnas(b*x1))
          else
            csprod=1./b*(sinnas(b*x2)-sinnas(b*x1))
          endif
        else if(knl.eq.0.and.kml.eq.0) then
          if(a.eq.b) then
            csprod=.5*(x2-x1)-.25/a*(sinnas(2.*a*x2)-sinnas(2.*a*x1))
          else if(a.eq.-b) then
            csprod=.5*(x1-x2)+.25/a*(sinnas(2.*a*x2)-sinnas(2.*a*x1))
          else
            amb=a-b
            apb=a+b
            csprod=.5/amb*(sinnas(amb*x2)-sinnas(amb*x1))-
     1             .5/apb*(sinnas(apb*x2)-sinnas(apb*x1))
          endif
        else if(knl.eq.1.and.kml.eq.1) then
          if(abs(a).eq.abs(b)) then
            csprod=.5*(x2-x1)+.25/a*(sinnas(2.*a*x2)-sinnas(2.*a*x1))
          else
            amb=a-b
            apb=a+b
            csprod=.5/amb*(sinnas(amb*x2)-sinnas(amb*x1))+
     1             .5/apb*(sinnas(apb*x2)-sinnas(apb*x1))
          endif
        else
          if(knl.eq.1) then
            pom=a
            a=b
            b=pom
          endif
          if(abs(a).eq.abs(b)) then
            csprod=.5/a*(sinnas(a*x2)**2-sinnas(a*x1)**2)
          else
            amb=a-b
            apb=a+b
            csprod=-.5/amb*(cosnas(amb*x2)-cosnas(amb*x1))-
     1              .5/apb*(cosnas(apb*x2)-cosnas(apb*x1))
          endif
        endif
      else
        isw=iswa(natp)
        if(n.gt.1) a=pi2*float(n/2)
        if(m.gt.1) b=pi2*float(m/2)
        kn=mod(n,2)
        km=mod(m,2)
        suma=0.
        do ic3=0,NCommQ(3,isw,KPhase)-1
          fc(3)=ic3
          do ic2=0,NCommQ(2,isw,KPhase)-1
            fc(2)=ic2
            do 4100ic1=0,NCommQ(1,isw,KPhase)-1
              fc(1)=ic1
              pom=ic1
              do j=1,3
                fc(j)=VCommQ(j,1,isw,KPhase)*pom
              enddo
              do j=1,3
                xs(j)=x(j,natp)+fc(j)
              enddo
              call qbyx(xs,xs(4),isw)
              xs(4)=trez(1,isw,KPhase)+xs(4)
              argl=xs(4)
2010          if(argl.le.x2) go to 2020
              argl=argl-1.
              go to 2010
2020          if(argl.ge.x1) go to 2030
              argl=argl+1.
              go to 2020
2030          if(argl.gt.x2) go to 4100
              if(n.ne.1) then
                if(kn.eq.1) then
                  pom=cosnas(a*argl)
                else
                  pom=sinnas(a*argl)
                endif
              else
                pom=1.
              endif
              if(m.ne.1) then
                if(km.eq.1) then
                  pom=pom*cosnas(b*argl)
                else
                  pom=pom*sinnas(b*argl)
                endif
              endif
              suma=suma+pom
4100        continue
          enddo
        enddo
        csprod=suma
      endif
      return
      end
      function cosnas(x)
      cosnas=dcos(dble(od0doPi2(x)))
      return
      end
      function sinnas(x)
      sinnas=dsin(dble(od0doPi2(x)))
      return
      end
      function od0doPi2(x)
      include 'fepc.cmn'
      od0doPi2=x-ifix(x/pi2)*pi2
      return
      end
      real function erfi(x)
      real :: pi = 3.141593
      double precision :: a = .140012, TwoDivPia = 4.546894,
     1                    pom,poml,dx
      a=0.147
      TwoDivPia=2./(pi*a)
      if(x.ge.1.) then
        erfi= 1.
      else if(x.le.-1.) then
        erfi=-1.
      else
        dx=x
        poml=dlog(1.-dx**2)
        pom=TwoDivPia+.5*poml
        erfi=dsqrt(dsqrt(pom**2-poml/a)-pom)
        if(x.lt.0.) erfi=-erfi
      endif
      return
      end
      logical function FileDiff(File1,File2)
      include 'fepc.cmn'
      character*(*) File1,File2
      character*256 Veta1,Veta2
      FileDiff=.true.
      ln1=0
      ln2=0
      ln1=NextLogicNumber()
      call OpenFile(ln1,File1,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      ln2=NextLogicNumber()
      call OpenFile(ln2,File2,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
1000  read(ln1,FormA,end=2000) Veta1
      read(ln2,FormA,end=3000) Veta2
      if(Veta1.eq.Veta2) go to 1000
      go to 9999
2000  ln=ln2
      go to 4000
3000  ln=ln1
      go to 4010
4000  read(ln,FormA,end=9000) Veta1
4010  if(Veta1.ne.' ') go to 9999
      go to 4000
9000  FileDiff=.false.
9999  call CloseIfOpened(ln1)
      call CloseIfOpened(ln2)
      ErrFlag=0
      return
      end
      subroutine MoveFile(File1,File2)
      include 'fepc.cmn'
      character*(*) file1,file2
      call CopyFile(file1,file2)
      call DeleteFile(file1)
      return
      end
      subroutine CopyFile(File1,File2)
      include 'fepc.cmn'
      character*256 t256
      character*(*) File1,File2
      t256='"'//File1(:idel(File1))//'" "'//File2(:idel(File2))//
     1            '"'
      i=idel(t256)
      if(OpSystem.le.0) then
        call FeCopyFile(File1,File2)
      else if(OpSystem.eq.1) then
        t256=ObrLom//'cp '//t256(:i)
        call FeSystem(t256)
      else if(OpSystem.eq.2) then
        t256='copy '//t256(:i)
        call FeSystem(t256)
      else
        ln1=NextLogicNumber()
        ln2=0
        call OpenFile(ln1,File1,'formatted','old')
        if(ErrFlag.ne.0) go to 2000
        ln2=NextLogicNumber()
        call OpenFile(ln1,File1,'formatted','unknown')
        if(ErrFlag.ne.0) go to 2000
1000    read(ln1,FormA128,end=2000) t256
        write(ln2,FormA1)(t256(i:i),i=1,idel(t256))
        go to 1000
2000    call CloseIfOpened(ln1)
        call CloseIfOpened(ln2)
      endif
      return
      end
      subroutine multm5(x,a,b,c,d,e,izn)
      dimension x(9),a(9),b(9),c(9),d(9),e(9),p(9)
      call multm(a,b,p,3,3,3)
      call multm(p,c,x,3,3,3)
      call multm(x,d,p,3,3,3)
      call multm(p,e,x,3,3,3)
      if(izn.lt.0) then
        do i=1,9
          x(i)=-x(i)
        enddo
      endif
      return
      end
      logical function EqWild(Veta,Hledany,CaseSensitive)
      character*(*) Veta,Hledany
      logical Hvezda,CaseSensitive
      idlh=idel(Hledany)
      idlv=idel(Veta)
      EqWild=.true.
      ik=0
      jk=0
1000  Hvezda=.false.
      ip=ik+1
      jp=jk+1
1100  if(Hledany(ip:ip).eq.'*') then
        Hvezda=.true.
      else if(Hledany(ip:ip).eq.'?') then
        if(jp.lt.idlv) then
          jp=jp+1
        else if(jp.eq.idlv.and.ip.eq.idlh) then
          go to 9000
        else
          go to 8000
        endif
      else
        go to 1200
      endif
      if(ip.ge.idlh) then
        if(Hvezda) then
          go to 9000
        else
          go to 8000
        endif
      else
        ip=ip+1
        go to 1100
      endif
1200  ik=ip
1500  if(Hledany(ik:ik).ne.'*'.and.Hledany(ik:ik).ne.'?') then
        if(ik.lt.idlh) then
          ik=ik+1
          go to 1500
        else
          go to 2000
        endif
      endif
      ik=ik-1
2000  idlp=ik-ip
      if(Hvezda) then
        idlpp=idlp
        do i=ik+1,idlh
          if(Hledany(i:i).ne.'*') then
            idlpp=idlpp+1
          else
            go to 2200
          endif
        enddo
        jp=idlv-idlpp
      endif
2200  if(jp.le.0) go to 8000
      i=LocateSubstring(Veta(jp:),Hledany(ip:ik),CaseSensitive,.true.)-1
      if(i.lt.0.or.(.not.Hvezda.and.i.gt.0)) go to 8000
      jk=jp+idlp+i
      if(ik.lt.idlh) then
        go to 1000
      else
        if(jk.lt.idlv) then
          go to 8000
        else
          go to 9000
        endif
      endif
8000  EqWild=.false.
9000  return
      end
      subroutine vrchol(xnula,xkrok,yjed,ydva,ytri,xpik,ypik)
      a=.5*(ytri-yjed)
      b=2.*ydva-ytri-yjed
      if(b.eq.0.) then
        b=1.
        a=0.
      endif
      b=a/b
      xpik=xnula+xkrok*b
      ypik=ydva+.5*a*b
      return
      end
      subroutine indtr(ih,trm,ihh,n)
      dimension ih(n),ihh(n),trm(n,n)
      ihh=0
      do i=1,n
        pom=0.
        do j=1,n
          pom=pom+float(ih(j))*trm(j,i)
        enddo
        ihh(i)=nint(pom)
        if(abs(pom-float(ihh(i))).gt..01) then
          ihh(1)=999
          go to 9999
        endif
      enddo
9999  return
      end
      subroutine WriteLabeledRecord(ln,Label,Record,k)
      include 'fepc.cmn'
      character*(*) Label,Record
      character*256 t256
      character*80  Item
      character*1 Ch
      t256=Label
1000  if(k.lt.len(Record)) then
        call kus(Record,k,Item)
        do i=1,idel(Item)
          Ch=Item(i:i)
          if(Ch.eq.'.') cycle
          if(index(Cifry,Ch).le.0) go to 2100
        enddo
        if(index(Item,'.').gt.0) call RealToFreeForm(Item)
2100    t256=t256(:idel(t256)+1)//Item(:idel(Item))
        go to 1000
      endif
      write(ln,FormA1)(t256(i:i),i=1,idel(t256))
      return
      end
      subroutine ctitext(text,stout,stin)
      include 'fepc.cmn'
      character*(*) text,stout,stin
      character*120 format
      character*80 t80
      format=' : '
      i=idel(text)
      j=idel(stin)
      if(j.gt.0) format=' ['//stin(1:j)// ']'//format(1:3)
      if(i.gt.0) format=text(1:i)//format(1:idel(format)+1)
      if(i.le.0) format='?'//format(1:idel(format)+1)
      write(out,'('' '//format(1:idel(format)+1)//''',$)')
      read(dta,FormA) t80
      if(idel(t80).le.0) then
        stout=stin
      else
        stout=t80
      endif
      return
      end
      subroutine Chybne(text1,text2,tisk,konec)
      include 'fepc.cmn'
      character*128 ven
      character*(*) text1,text2
      integer tisk
      if(konec.eq.1) then
        ven='Fatal -'
      else
        ven='Error -'
      endif
      ven=ven(1:idel(ven))//' '//text1(1:idel(text1))
      n=idel(ven)
      if(tisk.ne.-1.or.konec.eq.1) write(out,FormA1)(ven(i:i),
     1                                            i=1,min(79,n))
      if(tisk.eq.1) then
        call newln(1)
        write(lst,FormA1)(ven(i:i),i=1,n)
      endif
      n=idel(text2)
      if(n.ne.0) then
        if(konec.eq.1) then
          ven='        '//text2(1:idel(text2))
        else
          ven='          '//text2(1:idel(text2))
        endif
        n=idel(ven)
        if(tisk.ne.-1.or.konec.eq.1) write(out,FormA1)(ven(i:i),
     1                                              i=1,min(79,n))
        if(tisk.eq.1) then
          call newln(1)
          write(lst,FormA1)(ven(i:i),i=1,n)
        endif
      endif
      if(konec.eq.1) stop
9999  return
      end
      subroutine ctireal(text,x,dx,a,da,n,iw,id,veta,klic)
      include 'fepc.cmn'
      dimension a(n),da(n)
      character*(*) text
      character*120 format
      character*80 veta
      character*6  fwd
      character*2  nty
      if(n.le.0) return
      np=1
      if(iw.gt.0) then
        if(id.gt.0) then
          write(fwd,'(''f'',i2,''.'',i2)') iw,id
        else
          write(fwd,'(''i'',i2)') iw
        endif
      endif
      format=text(1:idel(text))
      call uprap(format)
      ifk=idel(format)
1000  if(iw.gt.0) then
        format=format(1:ifk)//' ['','
        do i=np,n-1
          format=format(1:idel(format))//fwd//','','','
        enddo
        format=format(1:idel(format))//fwd//',''] : '
        if(id.gt.0) then
          if(n.eq.1) then
            write(out,'('' '//format(1:idel(format)+1)//''',$)') dx
          else
            write(out,'('' '//format(1:idel(format)+1)//''',$)')
     1            (da(i),i=np,n)
          endif
        else
          if(n.eq.1) then
            write(out,'('' '//format(1:idel(format)+1)//''',$)')
     1            nint(dx)
          else
            write(out,'('' '//format(1:idel(format)+1)//''',$)')
     1           (nint(da(i)),i=np,n)
          endif
        endif
      else
        format=format(1:ifk)//' : '
        write(out,'('' '//format(1:idel(format)+1)//''',$)')
      endif
      read(dta,FormA) veta
      idelv=idel(veta)
      if(idelv.le.0.and.iw.ne.0) then
        if(n.eq.1) then
          x=dx
        else
          do i=np,n
            a(i)=da(i)
          enddo
        endif
      else
        if(idelv.eq.0) then
          write(out,'('' The string is empty, try again'')')
          go to 1000
        endif
        k=0
        if(n.eq.1) then
          call kus(veta,k,cislo)
          x=fract(cislo,ich)
          if(ich.eq.0) return
          if(klic.eq.1) then
            klic=-1
            return
          endif
          write(out,102)
          go to 1000
        else
          do i=np,n
            call kus(veta,k,cislo)
            a(i)=fract(cislo,ich)
            if(ich.ne.0) go to 1250
            if(k.eq.80) go to 1260
          enddo
          return
1250      if(klic.eq.1) then
            klic=-1
            return
          endif
          np=i
          write(out,'('' Error - non-numerical character in the '',i1,
     1                a2,''item of this line'')') np,nty(np)
          go to 1000
1260      np=i+1
          if(np.gt.n) return
          write(out,'('' Response not complete continue with the '',i1,
     1                a2,'' item of this line'')') np,nty(np)
          go to 1000
        endif
      endif
102   format(' String contains non-numerical character, try again')
      end
      subroutine ctiint(text,x,dx,a,da,n,iw)
      include 'fepc.cmn'
      character*(*) text
      character*120 format
      character*80 t80
      character*3  fwd
      character*2  nty
      integer x,dx,a(n),da(n)
      if(n.le.0) return
      np=1
      if(iw.gt.0) write(fwd,'(''i'',i2)') iw
      format=text(1:idel(text))
      call uprap(format)
      ifk=idel(format)
1000  if(iw.gt.0) then
        format=format(1:ifk)//' ['','
        do i=np,n-1
          format=format(1:idel(format))//fwd//','','','
        enddo
        format=format(1:idel(format))//fwd//',''] : '
        if(n.eq.1) then
          write(out,'('' '//format(1:idel(format)+1)//''',$)') dx
        else
          write(out,'('' '//format(1:idel(format)+1)//''',$)')
     1          (da(i),i=np,n)
        endif
      else
        format=format(1:ifk)//' : '
        write(out,'('' '//format(1:idel(format)+1)//''',$)')
      endif
      read(dta,FormA) t80
      idelt80=idel(t80)
      if(idelt80.le.0.and.iw.ne.0) then
        if(n.eq.1) then
          x=dx
        else
          do i=np,n
            a(i)=da(i)
          enddo
        endif
      else
        if(idelt80.eq.0) then
          if(n.eq.1) then
            x=999
          else
            do i=np,n
              a(i)=999
            enddo
          endif
          return
        endif
        k=0
        if(n.eq.1) then
          call kus(t80,k,cislo)
          call posun(cislo,0)
          read(cislo,FormI15,err=1150) x
          return
1150      write(out,102)
          go to 1000
        else
          do i=np,n
            call kus(t80,k,cislo)
            call posun(cislo,0)
            read(cislo,FormI15,err=1250) a(i)
            if(k.eq.80) go to 1260
          enddo
          return
1250      np=i
          write(out,'('' Error : non-numerical character in the '',i1,
     1                a2,''item of this line'')') np,nty(np)
          go to 1000
1260      np=i+1
          if(np.gt.n) return
          write(out,'('' Response not complete continue with the '',i1,
     1                a2,'' item of this line'')') np,nty(np)
          go to 1000
        endif
      endif
102   format(' String contains non-numerical character, try again')
      end
      logical function yesno(text,idflt)
      include 'fepc.cmn'
      character*(*) text
      character*120 format
      character*80 t80
      logical EqIgCase
      if(idflt.eq.1) then
        format=text(1:idel(text))//'? ([y]/n) : '
      else
        format=text(1:idel(text))//'? (y/[n]) : '
      endif
      call uprap(format)
1000  write(out,'('' '//format(1:idel(format)+1)//''',$)')
      read(dta,FormA) t80
      if(idel(t80).eq.0) then
        i=idflt
      else
        call zhusti(t80)
        if(EqIgCase(t80(1:1),'y')) then
          i=1
        else if(EqIgCase(t80(1:1),'n')) then
          i=0
        else
          write(out,'('' Your answer is not correct, try again'')')
          go to 1000
        endif
      endif
      yesno=i.eq.1
      return
      end
      subroutine StToReal(Veta,k,X,n,Message,ErrFlg)
      include 'fepc.cmn'
      dimension X(n)
      character*(*) Veta
      character*80  Polozka
      character*7 FormR
      character*5 FormI
      character*1 t1,tm1
      logical   Message
      integer ErrFlg
      data FormR,FormI/'(f15.0)','(i10)'/
      kin=k
      ErrFlg=0
      call SetRealArrayTo(X,n,0.)
      id=idel(Veta)
      if(id.le.0) go to 9900
      nn=0
      i=k
1000  idl=0
      Polozka=' '
      t1=' '
1100  if(idl.eq.0) then
        tm1=' '
      else
        tm1=t1
      endif
      i=i+1
      if(i.gt.id) go to 2000
      t1=Veta(i:i)
      call Mala(t1)
      if(ichar(t1).eq.9) t1=' '
      if(t1.eq.' ') then
        if(idl.eq.0) then
          go to 1100
        else
          go to 2000
        endif
      else if(index(Cifry(12:),t1).gt.0.and.idl.gt.0) then
        if(tm1.eq.'e'.or.tm1.eq.'d') then
          go to 1500
        else
          i=i-1
          go to 2000
        endif
      endif
1500  idl=idl+1
      Polozka(idl:idl)=t1
      go to 1100
2000  if(idl.le.0) go to 5000
      nn=nn+1
      j=index(Polozka,'/')
      if(j.le.0) then
        if(index(Polozka,'e').ne.0) then
          FormR(2:2)='e'
        else if(index(Polozka,'d').ne.0) then
          FormR(2:2)='d'
        else
          FormR(2:2)='f'
          if(index(Polozka,'.').le.0) then
            idl=idl+1
            Polozka(idl:idl)='.'
          endif
        endif
        write(FormR(3:4),100) idl
        read(Polozka,FormR,err=9000) X(nn)
      else
        if(j.eq.1.or.j.eq.idl) then
          X(nn)=0.
          go to 3000
        endif
        write(FormI(3:4),100) j-1
        read(Polozka(1:j-1),FormI,err=9000) i1
        write(FormI(3:4),100) idl-j
        read(Polozka(j+1:idl),FormI,err=9000) i2
        X(nn)=float(i1)/float(i2)
      endif
3000  if(nn.lt.n) go to 1000
5000  k=i
      if(nn.lt.n) go to 9100
      go to 9900
9000  errflg=1
      k=kin
      if(Message) then
        call FeChybne(-1.,-1.,'illegal real number - try again',Polozka,
     1                SeriousError)
      endif
      go to 9900
9100  errflg=2+nn
      if(Message) then
        call FeChybne(-1.,-1.,'the string doesn''t contains all '//
     1                'requested real numbers',Veta(1:idel(Veta)),
     2                SeriousError)
      endif
9900  k=min(len(Veta),k)
      if(k.ge.len(Veta)) go to 9999
9910  if(Veta(k+1:k+1).eq.' ') then
        k=k+1
        if(k.lt.len(Veta)) go to 9910
      endif
9999  return
100   format(i2)
      end
      subroutine GetRealLengthPoint(Radka,k,i,j)
      include 'fepc.cmn'
c      include 'basic.cmn'
      character*(*) Radka
      call Kus(Radka,k,Cislo)
      id=idel(Cislo)
      j=index(Cislo,'.')
      if(j.gt.0) then
        i=j
        j=id-j
      else
        i=id
        j=0
      endif
      return
      end
      subroutine StToInt(Veta,k,X,n,Message,ErrFlg)
      include 'fepc.cmn'
      integer X(n)
      character*(*) Veta
      character*80 Polozka
      character*5 FormI
      character*1 t1,tm1
      logical Message
      integer ErrFlg
      data FormI/'(i10)'/
      kin=k
      ErrFlg=0
      call SetIntArrayTo(X,n,0)
      id=idel(Veta)
      if(id.le.0) go to 9900
      nn=0
      i=k
1000  idl=0
      Polozka=' '
      t1=' '
1100  if(idl.eq.0) then
        tm1=' '
      else
        tm1=t1
      endif
      i=i+1
      if(i.gt.id) go to 2000
      t1=Veta(i:i)
      if(ichar(t1).eq.9) t1=' '
      if(t1.eq.' ') then
        if(idl.eq.0) then
          go to 1100
        else
          go to 2000
        endif
      else if(index(Cifry(12:),t1).gt.0.and.idl.gt.0) then
        i=i-1
        go to 2000
      endif
1500  idl=idl+1
      Polozka(idl:idl)=t1
      go to 1100
2000  if(idl.le.0) go to 5000
      nn=nn+1
      write(FormI(3:4),100) idl
      read(Polozka,FormI,err=9000) X(nn)
3000  if(nn.lt.n) go to 1000
5000  k=i
      if(nn.lt.n) go to 9100
      go to 9900
9000  errflg=1
      if(Message) then
        call zhusti(Cislo)
        call FeChybne(-1.,-1.,'illegal integer number - try again',
     1                Cislo,SeriousError)
      endif
      go to 9900
9100  errflg=2+nn
      k=kin
      if(Message) then
        call FeChybne(-1.,-1.,'the string doesn''t contains all '//
     1                'requested integers',Veta(1:idel(Veta)),
     2                SeriousError)
      endif
9900  k=min(len(Veta),k)
      if(k.ge.len(Veta)) go to 9999
9910  if(Veta(k+1:k+1).eq.' ') then
        k=k+1
        if(k.lt.len(Veta)) go to 9910
      endif
9999  return
100   format(i2)
      end
      character*2 function nty(i)
      include 'fepc.cmn'
      character*2 th(4)
      data th/'st','nd','rd','th'/
      if(i.lt.10.or.i.gt.19) then
        j=min(mod(i-1,10)+1,4)
      else
        j=4
      endif
      nty=th(j)
      return
      end
      function islovo(nazev,id,n)
      character*(*) id(n),nazev
      logical EqIgCase
      islovo=0
      do i=1,n
        if(EqIgCase(id(i),nazev)) then
          islovo=i
          go to 9999
        endif
      enddo
9999  return
      end
      subroutine Real2String(X,Veta,n)
      character*(*) Veta
      character*15 format
      integer Exponent10
      i=max(n-Exponent10(X),0)
      write(Format,'(''(f20.'',i4,'')'')') i
      write(Veta,Format) x
      return
      end
      subroutine RealToFreeForm(cislo)
      character*(*) cislo
      idl=idel(cislo)
1000  if(cislo(idl:idl).eq.'0') then
        cislo(idl:idl)=' '
        idl=idl-1
        if(idl.gt.0) go to 1000
      endif
      if(cislo(idl:idl).eq.'.') cislo(idl:idl)=' '
      if(cislo.eq.' ') cislo='0'
      return
      end
      subroutine vykric(veta)
      character*(*) veta
      n=index(veta(2:),'!')+1
      if(n.gt.1) Veta(n:)=' '
      return
      end
      subroutine ZdrcniCisla(radka,kolik)
      character*(*) radka
      character*256 t256,s256
      if(idel(radka).eq.0) return
      k=0
      do i=1,kolik
        call kus(radka,k,s256)
        if(index(s256,'.').gt.0) call RealToFreeForm(s256)
        idl=idel(s256)
        if(i.eq.1) then
          t256=s256(1:idl)
        else
          t256=t256(1:idel(t256))//' '//s256(1:idl)
        endif
      enddo
      radka=t256
      return
      end
      subroutine RomanNumber(n,String)
      character*(*) String
      if(n.gt.20) then
        String='???'
        go to 9999
      endif
      j=mod(n-1,10)+1
      if(j.eq.1) then
        String='i'
      else if(j.eq.2) then
        String='ii'
      else if(j.eq.3) then
        String='iii'
      else if(j.eq.4) then
        String='iv'
      else if(j.eq.5) then
        String='v'
      else if(j.eq.6) then
        String='vi'
      else if(j.eq.7) then
        String='vii'
      else if(j.eq.8) then
        String='viii'
      else if(j.eq.9) then
        String='ix'
      else if(j.eq.10) then
        String='x'
      endif
      if(n.gt.10) String='x'//String(:idel(String))
9999  return
      end
      logical function ExistFile(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      integer FeFileSize
      logical Exist
      ExistFile=.false.
      if(FileName.eq.' ') go to 9999
      if(FileName(1:1).eq.'.'.and.FileName(2:2).ne.'/'.and.
     1   FileName(2:2).ne.'.') go to 9999
      inquire(file=FileName,exist=Exist,err=9000)
      ExistFile=Exist
      if(ExistFile) then
        ExistFile=.not.FeFileSize(FileName).eq.0
        if(.not.ExistFile) call DeleteFile(FileName)
      endif
      go to 9999
9000  ExistFile=.false.
      ErrFlag=1
9999  return
      end
      logical function ExistAtLeastEmptyFile(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      integer FeFileSize
      logical Exist
      ExistAtLeastEmptyFile=.false.
      if(FileName.eq.' ') go to 9999
      if(FileName(1:1).eq.'.'.and.FileName(2:2).ne.'/'.and.
     1   FileName(2:2).ne.'.') go to 9999
      inquire(file=FileName,exist=Exist,err=9000)
      ExistAtLeastEmptyFile=Exist
      if(ExistAtLeastEmptyFile) then
        ExistAtLeastEmptyFile=.not.FeFileSize(FileName).eq.0
      endif
      go to 9999
9000  ExistAtLeastEmptyFile=.false.
      ErrFlag=1
9999  return
      end
      subroutine FileChanges
      include 'fepc.cmn'
      character*(*) FileName
      character*(*) Lines(*)
      character*256 t256
      entry AppendFile(FileName,Lines,n)
      ln1=NextLogicNumber()
      call OpenFile(ln1,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      nold=0
1000  read(ln1,FormA,end=1100) t256
      if(t256.eq.' ') go to 1100
      nold=nold+1
      go to 1000
1100  call CloseIfOpened(ln1)
      call OpenFile(ln1,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      do i=1,nold
        read(ln1,FormA) t256
      enddo
      do i=1,n
        write(ln1,FormA1)(Lines(i)(j:j),j=1,idel(Lines(i)))
      enddo
      call CloseIfOpened(ln1)
      go to 9999
      entry DeleteLinesFromFile(FileName,n,m)
      id=1
      go to 1500
      entry RewriteLinesOnFile(FileName,n,m,Lines,nl)
      id=0
1500  ln1=NextLogicNumber()
      call OpenFile(ln1,FileName,'formatted','old')
      if(ErrFlag.ne.0) go to 9000
      ln2=NextLogicNumber()
      call OpenFile(ln2,'WorkFile.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9000
      i=0
2000  read(ln1,FormA,end=2100) t256
      i=i+1
      if(i.lt.n.or.i.gt.m) then
        write(ln2,FormA1)(t256(j:j),j=1,idel(t256))
      else if(id.eq.0.and.i.eq.n) then
        do k=1,nl
          write(ln2,FormA1)(lines(k)(j:j),j=1,idel(Lines(k)))
        enddo
      endif
      go to 2000
2100  call CloseIfOpened(ln1)
      call CloseIfOpened(ln2)
      call MoveFile('WorkFile.tmp',FileName)
      go to 9999
9000  call CloseIfOpened(ln1)
9999  return
      end
      subroutine OpenFile(ln,FileName,form,status)
      include 'fepc.cmn'
      character*(*) FileName,form,status
      logical ExistFile,ef,opened
      character*80 t80
      character*40 t40
      character*10 formatted
      ErrFlag=0
      inquire(file=FileName,opened=opened,err=8000)
      if(opened) then
        t80='attempt to open already opened file.'
        goto 9100
      else
        ef=ExistFile(FileName)
      endif
      if(ef) then
        if(OpSystem.le.0) then
          inquire(File=FileName,formatted=formatted,err=8000)
          call mala(formatted)
        else
          formatted='unknown'
        endif
      else
        formatted='unknown'
      endif
      if(status.eq.'new') then
        if(ef) then
          t80='cannot be opened as a new file, it already exists.'
          go to 9000
        endif
      else if(status.eq.'old') then
        if(.not.ef) then
          t80='cannot be opened as an old file, it doesn''t exist.'
          go to 9000
        endif
      else if(status.eq.'unknown') then
        go to 1000
      else
        t80='the incorrect open parameter "status" : '//
     1      status(:idel(status))//'.'
        go to 9000
      endif
1000  if(form.eq.'formatted') then
        if(ef.and.formatted.eq.'no') then
          t80='attempt to open unformatted file as formatted.'
          go to 9000
        endif
      else if(form.eq.'unformatted') then
        if(ef.and.formatted.eq.'yes') then
          t80='attempt to open formatted file as unformatted.'
          go to 9000
        endif
      else
        t80='the incorrect open parameter "form" : '//
     1      form(:idel(form))//','
        go to 9000
      endif
      open(ln,file=FileName,status=status,form=form,err=8000)
      go to 9999
8000  t80='couldn''t be opened.'
9000  ErrFlag=1
9100  id=idel(FileName)
      call FeCutName(FileName,t40,40,CutTextFromLeft)
      if(ErrFlag.eq.0) then
        i=Warning
      else
        i=SeriousError
      endif
      call FeChybne(-1.,-1.,'the file "'//t40(:idel(t40))//'"',
     1              t80,i)
9999  return
      end
      logical function FileAlreadyOpened(FileName)
      include 'fepc.cmn'
      character*(*) FileName
      inquire(file=FileName,opened=FileAlreadyOpened)
      return
      end
      subroutine OpenForAppend(ln,FileName)
      include 'fepc.cmn'
      character*(*) FileName
      call OpenFile(ln,FileName,'formatted','old')
      if(ErrFlag.ne.0) go to 9999
      n=0
1000  read(ln,FormA,end=2000)
      n=n+1
      go to 1000
2000  rewind ln
      do i=1,n
        read(ln,FormA)
      enddo
9999  return
      end
      subroutine CheckFileIfNotReadOnly(FileName,Klic)
      include 'fepc.cmn'
      character*(*) FileName
      logical FeReadOnly,FeYesNoHeader
      ErrFlag=0
      if(FeReadOnly(FileName(:idel(FileName)))) then
        if(Klic.eq.1) then
          NInfo=4
          TextInfo(1)='The file "'//FileName(:idel(FileName))//
     1                '" has attribute READONLY.'
          TextInfo(2)='In case that you will ask to continue with '//
     1                'the structure'
          TextInfo(3)='the program will reset this attribute. '//
     1                'Otherwise program will'
          TextInfo(4)='continue without specified structure.'
          if(FeYesNoHeader(-1.,-1.,'Do you want to continue with the '//
     1                     'structure?',1)) then
           go to 1000
          else
           ErrFlag=1
          endif
        else
          go to 1000
        endif
      endif
      go to 9999
1000  call FeResetReadOnly(FileName(:idel(FileName)))
9999  return
      end
      subroutine CheckEOLOnFile(FileName,Klic)
      include 'fepc.cmn'
      character*(*) FileName
      character*80  Message
      character*256 t256
      character*1   ch1,String(256)
      integer FileType,CR,FeOpenBinaryFile,FeReadBinaryFile,
     1        FeCloseBinaryFile
      logical FeYesNoHeader,EOL
      equivalence (t256,String)
      data CR,LF/13,10/
      ln=FeOpenBinaryFile(FileName(:idel(FileName)))
      lno=0
      if(ln.le.0) then
        call FeCutName(FileName,Message,70-26,CutTextFromLeft)
        Message='cannot open file "'//
     1              Message(:idel(Message))//'"'
        call FeChybne(-1.,-1.,Message,' ',SeriousError)
        ErrFlag=1
        go to 9000
      endif
      n=FeReadBinaryFile(ln,String,256)
      do i=1,255
        ich1=ichar(t256(i:i))
        if(ich1.eq.CR) then
          ich2=ichar(t256(i+1:i+1))
          if(ich2.eq.LF) then
            FileType=-1
          else if(ich2.eq.CR) then
            ich3=ichar(t256(i+2:i+2))
            if(ich3.eq.LF) then
              FileType=-2
            else
              FileType=2
            endif
          else
            FileType=2
          endif
          go to 1200
        else if(ich1.eq.LF) then
          FileType=1
          go to 1200
        endif
      enddo
      go to 9000
1200  if(OpSystem.eq.FileType) go to 9000
      if(Klic.ge.1) then
        NInfo=3
        if(FileType.eq.-1) then
          Cislo='Windows CR/LF'
        else if(FileType.eq.1) then
          Cislo='UNIX LF'
        else if(FileType.eq.2) then
          Cislo='MAC CR'
        endif
        t256='Do you want to continue?'
        if(Klic.eq.1) then
          TextInfo(1)='End of line characters of one or more basic '//
     1                'JANA files are not'
          TextInfo(2)='compatible with currently used system. Jana '//
     1                'must convert them.'
          t256=t256(:idel(t256)-1)//' with this structure?'
        else
          call FeCutName(FileName,TextInfo(1),len(TextInfo(1))-35,
     1                   CutTextFromLeft)
          TextInfo(1)='End of line characters of "'//
     1                TextInfo(1)(:idel(TextInfo(1)))//'" is not'
          TextInfo(2)='compatible with currently used system. Jana '//
     1                'must convert it.'
          t256='Do you want to continue?'
        endif
        TextInfo(3)='Jana converts between Windows, MacIntosh and Unix.'
        if(FeYesNoHeader(-1.,-1.,t256,1)) then
          go to 2000
        else
          ErrFlag=1
        endif
      else
        go to 2000
      endif
      go to 9000
2000  i=FeCloseBinaryFile(ln)
      call FeFileBufferOpen(FileName,OpSystem)
      lno=NextLogicNumber()
      open(lno,file='WorkFile.tmp')
      n=0
2100  id=0
      t256=' '
2200  call FeFileBufferGetOneCharacter(ch1,ich)
      if(ich.ne.0) go to 5000
      if(FileType.eq.1) then
        EOL=ichar(ch1).eq.LF
      else
        EOL=ichar(ch1).eq.CR
        if(EOL.and.FileType.le.0) then
          call FeFileBufferGetOneCharacter(ch1,ich)
          if(ich.ne.0) go to 5000
          if(FileType.eq.-2) then
            call FeFileBufferGetOneCharacter(ch1,ich)
            if(ich.ne.0) go to 5000
          endif
        endif
      endif
      if(EOL) then
        n=n+1
        if(n.eq.1000) then
          Message='     0 records of "'//FileName(:idel(FileName))//
     1            '" already converted'
          write(Message(1:6),100) n
          call FeTxOut(-1.,-1.,Message)
        else if(n.gt.5000.and.mod(n,5000).eq.0) then
          write(Message(1:6),100) n
          call FeTxOutCont(Message)
        endif
        if(id.gt.0) then
          write(lno,FormA1)(t256(i:i),i=1,id)
        else
          write(lno,FormA1)
        endif
        go to 2100
      else
        id=min(id+1,256)
        t256(id:id)=ch1
        go to 2200
      endif
5000  if(n.ge.1000) then
        write(Message(1:6),100) n
        call FeTxOutCont(Message)
        call FeTxOutEnd
        call FeReleaseOutput
      endif
      i=FeCloseBinaryFile(ln)
      call CloseIfOpened(lno)
      call MoveFile('WorkFile.tmp',FileName)
      go to 9999
9000  i=FeCloseBinaryFile(ln)
      call CloseIfOpened(lno)
      call DeleteFile('WorkFile.tmp')
9999  return
100   format(i6)
      end
      subroutine FeFileBufferOpen(FileName,OpSystem)
      integer OpSystem
      character*(*) FileName
      character*1 ch,String(16384)
      character*16384 Buffer
      integer FeOpenBinaryFile,FeReadBinaryFile,FeCloseBinaryFile,
     1        BuffLen
      equivalence (Buffer,String)
      save n,nmax,ln,BuffLen,Buffer
      ln=FeOpenBinaryFile(FileName(:idel(FileName)))
      nmax=len(Buffer)
      BuffLen=nmax
      n=nmax
      go to 9999
      entry FeFileBufferGetOneCharacter(ch,ich)
      ich=0
      if(n.ge.nmax) then
        if(nmax.ge.Bufflen) then
          nmax=FeReadBinaryFile(ln,String,BuffLen)
          n=0
        else
          i=FeCloseBinaryFile(ln)
          ich=1
          go to 9999
        endif
      endif
      n=n+1
      ch=Buffer(n:n)
      go to 9999
      entry FeFileBufferClose
      i=FeCloseBinaryFile(ln)
9999  return
      end
      subroutine OpenIfExist(Ln,File,Formatted,Message)
      include 'fepc.cmn'
      character*80 ch80
      character*(*) File,Formatted,Message
      logical   ExistFile
      ErrFlag=0
      if(ExistFile(File)) then
        open(Ln,file=File,form=Formatted)
      else
        if(Message.ne.' ') then
          ch80='the file : '//File(:idel(File))//' doesn''t exist'
          call FeChybne(-1.,-1.,ch80(:idel(ch80)),Message,SeriousError)
        endif
        ErrFlag=1
      endif
      return
      end
      subroutine OpenIfNotExist(Ln,File,Formatted)
      include 'fepc.cmn'
      character*80 ch80
      character*(*) File,Formatted
      logical   ExistFile,FeYesNoLong
      if(.not.ExistFile(File)) then
        open(Ln,file=File,form=Formatted)
      else
        open(Ln,file=File,form=Formatted)
        ch80=Formatted
        call mala(ch80)
        if(ch80.eq.'formatted') then
          read(Ln,'(a)',end=500) ch80(1:1)
        else
          read(Ln,end=500) ch80(1:1)
        endif
        rewind Ln
        TextInfo(1)='The file '//File(1:idel(File))//' already exists.'
        TextInfo(2)='Do you want to overwrite it?'
        NInfo=2
        if(.not.FeYesNoLong(-1.,-1.,'Warning',0)) then
          call CloseIfOpened(ln)
          ErrFlag=1
        endif
      endif
 500  return
      end
      subroutine CloseIfOpened(n)
      logical Opened
      if(n.gt.0) then
        inquire(n,opened=opened)
        if(opened) close(n)
      endif
      return
      end
      subroutine CloseAllFiles
      logical Opened
      do i=40,99
        inquire(i,opened=opened)
        if(opened) close(i)
      enddo
      return
      end
      subroutine CloseListing
      include 'fepc.cmn'
      logical Opened
      if(LstOpened) then
        inquire(lst,opened=opened)
        if(Opened) then
          do i=1,mxline-line
            write(lst,FormA1)
          enddo
          close(lst)
        endif
        LstOpened=.false.
      endif
      return
      end
      subroutine TrPopulCoef(Pin,Rot,lmax,Pout)
      include 'fepc.cmn'
      include 'basic.cmn'
      real KroneckerDelta
      dimension Pin(*),Pout(*),rot(3,3),R(225)
      call matinv(Rot,R,det,3)
      det=sign(1.,det)
      r33=rot(3,3)*det
      if(abs(r33).le..99999) then
        beta=acos(r33)
        alfa=atan2(rot(3,2),rot(3,1))
        gama=atan2(rot(2,3),-rot(1,3))
      else
        alfa=atan2(rot(1,2),rot(1,1))
        gama=0.
        if(r33.gt.0.) then
          beta=0.
        else
          beta=pi
        endif
      endif
      if(det.lt.0.) then
        alfa=alfa-pi
        gama=gama-pi
      endif
      chbeta=cos(.5*beta)
      shbeta=sin(.5*beta)
      ip=1
      Pout(1)=Pin(1)
      do l=1,lmax
        mq=0
        n=2*l+1
        k=0
        call SetRealArrayTo(R,225,0.)
        do i=1,n
          m=iabs(mq)
          fm=float(m)
          f1=Llm(i+ip)/Clm(i+ip)
          if(mod(l,2).eq.1.and.det.lt.0.) f1=-f1
          j=1
          do mp=0,l
            fmp=float(mp)
            f2=Clm(j+ip)/Llm(j+ip)
            cp=0.
            kp=max(0,m-mp)
            A=(-1)**kp
            pom=chbeta**(2*l-2*(kp-1)+m-mp)*shbeta**(2*(kp-1)+mp-m)
            pomd=(shbeta/chbeta)**2
            do kk=kp,min(l-mp,l+m)
              pom=pom*pomd
              cp=cp+A*BinomCoef(l+m,kk)*BinomCoef(l-m,l-mp-kk)*pom
              A=-A
            enddo
            cm=0.
            kp=max(0,m+mp)
            A=(-1)**(mp+kp)
            pom=chbeta**(2*l-2*(kp-1)+m+mp)*shbeta**(2*(kp-1)-mp-m)
            do kk=kp,min(l+mp,l+m)
              pom=pom*pomd
              cm=cm+A*BinomCoef(l+m,kk)*BinomCoef(l-m,l+mp-kk)*pom
              A=-A
            enddo
            A=factorial(l-mp)/factorial(l-m)*f1*f2*
     1                        0.5*(2.-KroneckerDelta(0,mp))
            angp=fm*gama+fmp*alfa
            angm=fm*gama-fmp*alfa
            csp=cos(angp)*cp
            csm=cos(angm)*cm
            snp=sin(angp)*cp
            snm=sin(angm)*cm
            if(mq.ge.0) then
              k=k+1
              R(k)=A*(csp+csm)
              if(mp.ne.0) then
                k=k+1
                R(k)=A*(snp-snm)
              endif
            else
              k=k+1
              R(k)=-A*(snp+snm)
              if(mp.ne.0) then
                k=k+1
                R(k)=A*(csp-csm)
              endif
            endif
            j=j+2
          enddo
          mq=-mq
          if(mod(i,2).eq.1) mq=mq+1
        enddo
        call multm(R,Pin(ip+1),Pout(ip+1),n,n,1)
        ip=ip+n
      enddo
      return
      end
      subroutine SetPopulTrans(Rot,lmax,R)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rot(3,3),roti(3,3),R(*)
      real KroneckerDelta
      call matinv(rot,roti,det,3)
      det=sign(1.,det)
      do i=1,3
        do j=1,3
          roti(i,j)=det*rot(i,j)
        enddo
      enddo
      if(abs(roti(3,3)).le..99999) then
        beta=acos(roti(3,3))
        alfa=atan2(roti(3,2), roti(3,1))
        gama=atan2(roti(2,3),-roti(1,3))
      else
        gama=0.
        if(roti(3,3).gt.0.) then
          beta=0.
          alfa=atan2(roti(1,2),roti(1,1))
        else
          beta=pi
          alfa=atan2(-roti(1,2),-roti(1,1))
        endif
      endif
      chbeta=cos(.5*beta)
      shbeta=sin(.5*beta)
      if(abs(chbeta).lt..0001) chbeta=0.
      if(abs(shbeta).lt..0001) shbeta=0.
      if(chbeta.gt. .9999) chbeta= 1.
      if(shbeta.gt. .9999) shbeta= 1.
      if(chbeta.lt.-.9999) chbeta=-1.
      if(shbeta.lt.-.9999) shbeta=-1.
      ip=1
      k=1
      do l=1,lmax
        mz=0
        nl=2*l+1
        do i=1,nl
          m=iabs(mz)
          znm=isign(1,mz)
          f1=Llm(i+ip)/Mlm(i+ip)
          if(mod(l,2).eq.1.and.det.lt.0.) f1=-f1
          mpz=0
          do j=1,nl
            mp=iabs(mpz)
            znn=isign(1,mpz)
            f2=1.
            f2=Mlm(j+ip)/Llm(j+ip)
            A=f1*f2*float((-1)**(m+mp))/
     1        sqrt((1.+KroneckerDelta(0,m))*(1.+KroneckerDelta(0,mp)))
            argp=m*gama+mp*alfa
            argn=m*gama-mp*alfa
            k=k+1
            if(mpz*mz.gt.0.or.(mz*mpz.eq.0.and.mz+mpz.ge.0)) then
              R(k)=A*(DTrCoef(l, mp,m,shbeta,chbeta)*cos(argp)+
     1                DTrCoef(l,-mp,m,shbeta,chbeta)*cos(argn)*znn*
     2                                                         (-1)**mp)
            else
              R(k)=A*(DTrCoef(l, mp,m,shbeta,chbeta)*sin(argp)*znm-
     1                DTrCoef(l,-mp,m,shbeta,chbeta)*sin(argn)*(-1)**mp)
            endif
            mpz=-mpz
            if(mod(j,2).eq.1) mpz=mpz+1
          enddo
          mz=-mz
          if(mod(i,2).eq.1) mz=mz+1
        enddo
        ip=ip+nl
      enddo
      return
      end
      function DTrCoef(l,mp,m,shbeta,chbeta)
      include 'fepc.cmn'
      DTrCoef=0.
      kp=max(0,m-mp)
      kk=min(l+m,l-mp)
      kkk=kp+mp-m
      A=(-1)**kkk*sqrt(factorial(l+mp)*factorial(l-mp)/
     1                (factorial(l+m)*factorial(l-m)))
      do k=kp,kk
        DTrCoef=DTrCoef+A*chbeta**(2*l+m-mp-2*k)*shbeta**(2*k+mp-m)*
     1                  Binomcoef(l+m,k)*Binomcoef(l-m,l-mp-k)
        A=-A
      enddo
      return
      end
      subroutine SetPopulTransOld(Rot,lmax,R)
      include 'fepc.cmn'
      include 'basic.cmn'
      real KroneckerDelta
      dimension rot(3,3),roti(3,3),R(*)
      call matinv(rot,roti,det,3)
      det=sign(1.,det)
      do i=1,3
        do j=1,3
          roti(i,j)=det*rot(i,j)
        enddo
      enddo
      if(abs(roti(3,3)).le..99999) then
        beta=acos(roti(3,3))
        alfa=atan2(roti(3,2), roti(3,1))
        gama=atan2(roti(2,3),-roti(1,3))
      else
        alfa=atan2(roti(1,2),roti(1,1))
        gama=0.
        if(roti(3,3).gt.0.) then
          beta=0.
        else
          beta=pi
        endif
      endif
      chbeta=cos(.5*beta)
      shbeta=sin(.5*beta)
      ip=1
      k=1
      do l=1,lmax
        mq=0
        n=2*l+1
        do i=1,n
          m=iabs(mq)
          fm=float(m)
          f1=Llm(i+ip)/Clm(i+ip)
          if(mod(l,2).eq.1.and.det.lt.0.) f1=-f1
          j=1
          do mp=0,l
            fmp=float(mp)
            f2=Clm(j+ip)/Llm(j+ip)
            cp=0.
            kp=max(0,m-mp)
            kk=min(l-mp,l+m)
            if(shbeta.ne.0.) then
              if(chbeta.ne.0.) then
                pom=chbeta**(2*l-2*kp+m-mp)*shbeta**(2*kp+mp-m)
                pomd=(shbeta/chbeta)**2
              else
                if(2*l-2*kk+m-mp.eq.0) then
                  pom=shbeta**(2*kk+mp-m)
                else
                  pom=0.
                endif
                kp=kk
                pomd=0.
              endif
            else
              if(2*kp+mp-m.eq.0) then
                pom=chbeta**(2*l-2*kp+m-mp)
              else
                pom=0.
              endif
              kk=kp
              pomd=0.
            endif
            A=(-1)**kp
            do kk=kp,min(l-mp,l+m)
              cp=cp+A*BinomCoef(l+m,kk)*BinomCoef(l-m,l-mp-kk)*pom
              A=-A
              pom=pom*pomd
            enddo
            cm=0.
            kp=max(0,m+mp)
            A=(-1)**(mp+kp)
            if(shbeta.ne.0.) then
              pom=chbeta**(2*l-2*kp+m+mp)*shbeta**(2*kp-mp-m)
            else
              if(2*kp-mp-m.eq.0) then
                pom=chbeta**(2*l-2*kp+m+mp)
              else
                pom=0.
              endif
              pomd=0.
            endif
            do kk=kp,min(l+mp,l+m)
              cm=cm+A*BinomCoef(l+m,kk)*BinomCoef(l-m,l+mp-kk)*pom
              A=-A
              pom=pom*pomd
            enddo
            A=factorial(l-mp)/factorial(l-m)*f1*f2*
     1                        0.5*(2.-KroneckerDelta(0,mp))
            angp=fm*gama+fmp*alfa
            angm=fm*gama-fmp*alfa
            csp=cos(angp)*cp
            csm=cos(angm)*cm
            snp=sin(angp)*cp
            snm=sin(angm)*cm
            if(mq.ge.0) then
              k=k+1
              R(k)=A*(csp+csm)
              if(mp.ne.0) then
                k=k+1
                R(k)=A*(snp-snm)
              endif
            else
              k=k+1
              R(k)=-A*(snp+snm)
              if(mp.ne.0) then
                k=k+1
                R(k)=A*(csp-csm)
              endif
            endif
            j=j+2
          enddo
          mq=-mq
          if(mod(i,2).eq.1) mq=mq+1
        enddo
        ip=ip+n
      enddo
      return
      end
      subroutine FractVecToNomDenForm(x,y,n,ich)
      dimension x(n)
      ich=0
      y=1.
      do i=1,n
        yp=1.
1000    pom=x(i)*yp*y
        if(abs(anint(pom)-pom).gt..0001) then
          yp=yp+1.
          if(yp*y.lt.24.) then
            go to 1000
          else
            ich=1
            y=1.
            go to 9999
          endif
        endif
        y=y*yp
      enddo
      do i=1,n
        x(i)=x(i)*y
      enddo
9999  return
      end
      SUBROUTINE four1(data,nn,isign)
      include 'fepc.cmn'
      INTEGER isign,nn
      REAL data(2*nn)
      INTEGER i,istep,j,m,mmax,n
      REAL tempi,tempr
      DOUBLE PRECISION theta,wi,wpi,wpr,wr,wtemp
      n=2*nn
      j=1
      do  i=1,n,2
        if(j.gt.i)then
          tempr=data(j)
          tempi=data(j+1)
          data(j)=data(i)
          data(j+1)=data(i+1)
          data(i)=tempr
          data(i+1)=tempi
        endif
        m=nn
1       if ((m.ge.2).and.(j.gt.m)) then
          j=j-m
          m=m/2
        goto 1
        endif
        j=j+m
      enddo
      mmax=2
2     if (n.gt.mmax) then
        istep=2*mmax
        theta=6.28318530717959d0/(isign*mmax)
        wpr=-2.d0*sin(0.5d0*theta)**2
        wpi=sin(theta)
        wr=1.d0
        wi=0.d0
        do  m=1,mmax,2
          do  i=m,n,istep
            j=i+mmax
            tempr=sngl(wr)*data(j)-sngl(wi)*data(j+1)
            tempi=sngl(wr)*data(j+1)+sngl(wi)*data(j)
            data(j)=data(i)-tempr
            data(j+1)=data(i+1)-tempi
            data(i)=data(i)+tempr
            data(i+1)=data(i+1)+tempi
          enddo
          wtemp=wr
          wr=wr*wpr-wi*wpi+wr
          wi=wi*wpr+wtemp*wpi+wi
        enddo
        mmax=istep
      goto 2
      endif
      return
      END
      SUBROUTINE fourn(data,nn,ndim,isign)
      include 'fepc.cmn'
      INTEGER isign,ndim,nn(ndim)
      REAL data(*)
      INTEGER i1,i2,i2rev,i3,i3rev,ibit,idim,ifp1,ifp2,ip1,ip2,ip3,k1,
     *k2,n,nprev,nrem,ntot
      REAL tempi,tempr
      DOUBLE PRECISION theta,wi,wpi,wpr,wr,wtemp
      ntot=1
      do  idim=1,ndim
        ntot=ntot*nn(idim)
      enddo
      nprev=1
      do  idim=1,ndim
        n=nn(idim)
        nrem=ntot/(n*nprev)
        ip1=2*nprev
        ip2=ip1*n
        ip3=ip2*nrem
        i2rev=1
        do  i2=1,ip2,ip1
          if(i2.lt.i2rev)then
            do  i1=i2,i2+ip1-2,2
              do  i3=i1,ip3,ip2
                i3rev=i2rev+i3-i2
                tempr=data(i3)
                tempi=data(i3+1)
                data(i3)=data(i3rev)
                data(i3+1)=data(i3rev+1)
                data(i3rev)=tempr
                data(i3rev+1)=tempi
              enddo
            enddo
          endif
          ibit=ip2/2
1         if ((ibit.ge.ip1).and.(i2rev.gt.ibit)) then
            i2rev=i2rev-ibit
            ibit=ibit/2
          goto 1
          endif
          i2rev=i2rev+ibit
        enddo
        ifp1=ip1
2       if(ifp1.lt.ip2)then
          ifp2=2*ifp1
          theta=isign*6.28318530717959d0/(ifp2/ip1)
          wpr=-2.d0*sin(0.5d0*theta)**2
          wpi=sin(theta)
          wr=1.d0
          wi=0.d0
          do  i3=1,ifp1,ip1
            do  i1=i3,i3+ip1-2,2
              do  i2=i1,ip3,ifp2
                k1=i2
                k2=k1+ifp1
                tempr=sngl(wr)*data(k2)-sngl(wi)*data(k2+1)
                tempi=sngl(wr)*data(k2+1)+sngl(wi)*data(k2)
                data(k2)=data(k1)-tempr
                data(k2+1)=data(k1+1)-tempi
                data(k1)=data(k1)+tempr
                data(k1+1)=data(k1+1)+tempi
              enddo
            enddo
            wtemp=wr
            wr=wr*wpr-wi*wpi+wr
            wi=wi*wpr+wtemp*wpi+wi
          enddo
          ifp1=ifp2
        goto 2
        endif
        nprev=n*nprev
      enddo
      return
      END
      subroutine GenAnharmCoeff
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ia(:),im(:),iap(:,:),iar(:,:),iapp(:,:),iapr(:,:),
     1          idlcn(:,:),ipocn(:,:),invn(:),ioffmn(:),ioffvn(:)
      integer Order(:,:),Used(:),Exponent10,FeChDir
      character*80  Veta,Frmt
      character*256 CurrentDirO
      allocatable ia,im,iap,iar,iapp,iapr,Order,Used,idlcn,ipocn,invn,
     1            ioffmn,ioffvn
      allocate(ia(6),im(6),iap(6,729),iar(6,729),iapp(729,4),
     1         iapr(729,4),Order(729,4),Used(729),idlcn(28,4),
     2         ipocn(28,4),invn(4),ioffmn(4),ioffvn(4))
      CurrentDirO=CurrentDir
      i=FeChdir('C:\Jana2006\testy')
      call SetIntArrayTo(im,6,3)
      ioffvn(1)=0
      do n=3,6
        n3=3**n
        invn(n-2)=n3
        if(n.lt.6) ioffvn(n-1)=ioffvn(n-2)+n3
        do 1200m=1,n3
          call RecUnpack(m,ia,im,n)
          do i=1,n
            iap(n-i+1,m)=ia(i)
          enddo
          k=0
          do i=1,3
            do j=1,n
              if(iap(j,m).eq.i) then
                k=k+1
                iar(k,m)=i
                if(k.eq.n) go to 1200
              endif
            enddo
          enddo
1150      pause
1200    continue
        do i=1,n3
          ip =0
          ipr=0
          do j=1,n
            ip =ip *4+iap(j,i)
            ipr=ipr*4+iar(j,i)
          enddo
          iapp(i,n-2)=ip
          iapr(i,n-2)=ipr
          Order(i,n-2)=0
          Used(i)=0
        enddo
        ip=0
        do 1500i=1,n3
          if(Used(i).ne.0) go to 1500
          ip=ip+1
          Order(ip,n-2)=i
          Used(i)=1
          do 1400j=i+1,n3
            if(Used(j).ne.0.or.iapr(i,n-2).ne.iapr(j,n-2)) go to 1400
            ip=ip+1
            Order(ip,n-2)=j
            Used(j)=1
1400      continue
1500    continue
      enddo
      ln=NextLogicNumber()
      open(ln,file='Anh_common')
      Veta='      data ipec /'
      m=0
      do n=3,6
        mx=0
        n3=3**n
        do i=1,n3
          mx=max(mx,iapp(i,n-2))
        enddo
        i=Exponent10(float(mx))+1
        if(i.eq.2) then
          Frmt='(18(i2,'',''))'
          k=18
        else if(i.eq.3) then
          Frmt='(13(i3,'',''))'
          k=13
        else if(i.eq.4) then
          Frmt='(11(i4,'',''))'
          k=11
        endif
        do j=1,n3,k
          kk=min(k,n3-j+1)
          write(Veta(18:),Frmt)(iapp(Order(j+i-1,n-2),n-2),i=1,kk)
          if(j+kk-1.ge.n3.and.n.eq.6) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data idlc /'
      Frmt='(18(i2,'',''))'
      k=18
      m=0
      do n=3,6
        n3=3**n
        nn=1
        idlcn(nn,n-2)=0
        ipocn(nn,n-2)=1
        do j=1,n3
          i=Order(j,n-2)
          if(idlcn(nn,n-2).eq.0) then
            iv=iapr(i,n-2)
            idlcn(nn,n-2)=1
          else if(iapr(i,n-2).eq.iv) then
            idlcn(nn,n-2)=idlcn(nn,n-2)+1
          else
            nn=nn+1
            ipocn(nn,n-2)=ipocn(nn-1,n-2)+idlcn(nn-1,n-2)
            idlcn(nn,n-2)=1
            iv=iapr(i,n-2)
          endif
        enddo
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(idlcn(j+i-1,n-2),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.6) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data cmlt /'
      Frmt='(13(f3.0,'',''))'
      k=13
      m=0
      ioffmn(1)=0
      do n=3,6
        nn=TRank(n)
        if(n.lt.6) ioffmn(n-1)=ioffmn(n-2)+nn
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(float(idlcn(j+i-1,n-2)),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.6) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data ipoc /'
      m=0
      do n=3,6
        nn=TRank(n)
        if(n.le.4) then
          Frmt='(18(i2,'',''))'
          k=18
        else
          Frmt='(13(i3,'',''))'
          k=13
        endif
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(ipocn(j+i-1,n-2),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.6) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      write(Frmt,'(''/'',3(i5,'',''),i5,''/'')') invn
      call Zhusti(Frmt)
      Veta='      data inv'//Frmt(:idel(Frmt))
      write(Frmt,'(''/'',3(i5,'',''),i5,''/'')') ioffmn
      call Zhusti(Frmt)
      Veta=Veta(:idel(Veta))//',ioffm'//Frmt(:idel(Frmt))
      write(Frmt,'(''/'',3(i5,'',''),i5,''/'')') ioffvn
      call Zhusti(Frmt)
      Veta=Veta(:idel(Veta))//',ioffv'//Frmt(:idel(Frmt))//','
      write(ln,FormA) Veta(:idel(Veta))
      call CloseIfOpened(ln)
      i=FeChdir(CurrentDirO)
      deallocate(ia,im,iap,iar,iapp,iapr,Order,Used,idlcn,ipocn,invn,
     1           ioffmn,ioffvn)
      return
      end
      subroutine GenAnharmCoeff4
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension ia(:),im(:),iap(:,:),iar(:,:),iapp(:,:),iapr(:,:),
     1          idlcn(:,:),ipocn(:,:)
      integer Order(:,:),Used(:),Exponent10,FeChDir,TRankP
      character*80  Veta,Frmt
      character*256 CurrentDirO
      allocatable ia,im,iap,iar,iapp,iapr,Order,Used,idlcn,ipocn
      allocate(ia(6),im(6),iap(6,256),iar(6,256),iapp(256,4),
     1         iapr(256,4),Order(256,4),Used(256),idlcn(256,4),
     2         ipocn(256,4))
      CurrentDirO=CurrentDir
      i=FeChdir('C:\Jana2006\testy')
      call SetIntArrayTo(im,6,4)
      do n=4,4
        n4=4**n
        do 1200m=1,n4
          call RecUnpack(m,ia,im,n)
          do i=1,n
            iap(n-i+1,m)=ia(i)
          enddo
          k=0
          do i=1,4
            do j=1,n
              if(iap(j,m).eq.i) then
                k=k+1
                iar(k,m)=i
                if(k.eq.n) go to 1200
              endif
            enddo
          enddo
1150      pause
1200    continue
        do i=1,n4
          ip =0
          ipr=0
          do j=1,n
            ip =ip *4+iap(j,i)
            ipr=ipr*4+iar(j,i)
          enddo
          iapp(i,n-2)=ip
          iapr(i,n-2)=ipr
          Order(i,n-2)=0
          Used(i)=0
        enddo
        ip=0
        do 1500i=1,n4
          if(Used(i).ne.0) go to 1500
          ip=ip+1
          Order(ip,n-2)=i
          Used(i)=1
          do 1400j=i+1,n4
            if(Used(j).ne.0.or.iapr(i,n-2).ne.iapr(j,n-2)) go to 1400
            ip=ip+1
            Order(ip,n-2)=j
            Used(j)=1
1400      continue
1500    continue
      enddo
      ln=NextLogicNumber()
      open(ln,file='Anh_common')
      Veta='      data ipec4/'
      m=0
      do n=4,4
        mx=0
        n4=4**n
        do i=1,n4
          mx=max(mx,iapp(i,n-2))
        enddo
        i=Exponent10(float(mx))+1
        if(i.eq.2) then
          Frmt='(18(i2,'',''))'
          k=18
        else if(i.eq.3) then
          Frmt='(13(i3,'',''))'
          k=13
        else if(i.eq.4) then
          Frmt='(11(i4,'',''))'
          k=11
        endif
        do j=1,n4,k
          kk=min(k,n4-j+1)
          write(Veta(18:),Frmt)(iapp(Order(j+i-1,n-2),n-2),i=1,kk)
          if(j+kk-1.ge.n4.and.n.eq.4) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data idlc4/'
      Frmt='(18(i2,'',''))'
      k=18
      m=0
      do n=4,4
        n4=4**n
        nn=1
        idlcn(nn,n-2)=0
        ipocn(nn,n-2)=1
        do j=1,n4
          i=Order(j,n-2)
          if(idlcn(nn,n-2).eq.0) then
            iv=iapr(i,n-2)
            idlcn(nn,n-2)=1
          else if(iapr(i,n-2).eq.iv) then
            idlcn(nn,n-2)=idlcn(nn,n-2)+1
          else
            nn=nn+1
            ipocn(nn,n-2)=ipocn(nn-1,n-2)+idlcn(nn-1,n-2)
            idlcn(nn,n-2)=1
            iv=iapr(i,n-2)
          endif
        enddo
        TRankP=nn
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(idlcn(j+i-1,n-2),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.4) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data cmlt4/'
      Frmt='(13(f3.0,'',''))'
      k=13
      m=0
      do n=4,4
        nn=TRankP
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(float(idlcn(j+i-1,n-2)),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.4) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      Veta='      data ipoc4/'
      m=0
      do n=4,4
        nn=TRankP
        Frmt='(13(i3,'',''))'
        k=13
        do j=1,nn,k
          kk=min(k,nn-j+1)
          write(Veta(18:),Frmt)(ipocn(j+i-1,n-2),i=1,kk)
          if(j+kk-1.ge.nn.and.n.eq.4) Veta(idel(Veta):idel(Veta))='/'
          write(ln,FormA) Veta(:idel(Veta))
          m=mod(m+1,10)
          if(m.ne.0) then
            write(Veta,'(5x,i1)') m
          else
            write(Veta,'(5x,''a'')')
          endif
        enddo
      enddo
      do i=1,35
        call NToStrainString4(i,Veta)
        write(Cislo,'(i5)') i
        call FeWinMessage(Cislo,Veta)
      enddo
      call CloseIfOpened(ln)
      i=FeChdir(CurrentDirO)
      deallocate(ia,im,iap,iar,iapp,iapr,Order,Used,idlcn,ipocn)
      return
      end
      integer function GetSymmOrd(rm)
      dimension rm(3,3),rs(3,3),rp(3,3)
      logical MatRealEqUnitMat
      rs=rm
      GetSymmOrd=0
1100  GetSymmOrd=GetSymmOrd+1
      if(MatRealEqUnitMat(rs,3,.001)) go to 9999
      call Multm(rs,rm,rp,3,3,3)
      rs=rp
      go to 1100
9999  return
      end
      subroutine SetMatBlock(R,n,ip,jp,A,Phi,m)
      complex R(n,n),A(m,m),Factor
      Factor=exp(cmplx(0.,Phi))
      ii=0
      do i=ip,ip+m-1
        ii=ii+1
        jj=0
        do j=jp,jp+m-1
          jj=jj+1
          R(i,j)=Factor*A(ii,jj)
        enddo
      enddo
      return
      end
      subroutine Round2Closest(R)
      pom=abs(R)
      if(pom.lt..001) then
        R=0.
        go to 9999
      else if(abs(pom-.866).lt..001) then
        pom=sqrt(.75)
      else if(abs(pom-.5).lt..001) then
        pom=.5
      else if(abs(pom-1.).lt..001) then
        pom=1.
      endif
      R=sign(pom,R)
9999  return
      end
      subroutine FillStringArray(String,k,Veta)
      character*1 String(*)
      character*(*) Veta
      do i=1,idel(Veta)
        k=k+1
        String(k)=Veta(i:i)
      enddo
      return
      end
      subroutine TranslateToPostData(Veta,t256)
      character*(*) Veta,t256
      k=0
      do i=1,idel(Veta)
        k=k+1
        if(Veta(i:i).eq.',') then
          t256(k:)='%2C'
          k=k+2
        else if(Veta(i:i).eq.'/') then
          t256(k:)='%2F'
          k=k+2
        else if(Veta(i:i).eq.'/') then
          t256(k:)='%3B'
          k=k+2
        else if(Veta(i:i).eq.'+') then
          t256(k:)='%2B'
          k=k+2
        else
          t256(k:k)=Veta(i:i)
        endif
      enddo
      return
      end
      double precision function THester(fi2)
      real fi2
      double precision :: dfi2,dpid4=0.78539816339744825d0
      dfi2=fi2
      THester=dlog(dabs(dtan(dfi2*.5+dpid4)))
      return
      end
      double precision function QHesterOriginal(x,y)
      double precision :: pom1,pom2,snx,sny,Mez1=.999999999d0
      real x,y
      sny=dsin(dble(y))
      snx=dsin(dble(x))
      pom1=min((snx-sny**2)/((snx-1.d0)*sny),Mez1)
      pom1=max(pom1,-Mez1)
      pom2=min((snx+sny**2)/((snx+1.d0)*sny),Mez1)
      pom2=max(pom2,-Mez1)
      QHesterOriginal=.5d0*(-dasin(pom1)+dasin(pom2))
      return
      end
      double precision function QHester(x,y)
      double precision :: pom1,csy,sny,csx,snx,
     1                    dpid4=0.78539816339744825d0
      csy=dcos(dble(y))
      csx=dcos(dble(x))
      snx=dsin(dble(x))
      pom1=(csx/csy)**2
      if(pom1.lt.1.d0) pom1=1.d0
      pom1=dsqrt(pom1-1.)
      QHester=.5d0*datan2((-pom1**2+snx**2),2.*pom1*snx)+dpid4
      return
      end
      subroutine PisPo80(ln,Radka)
      character*(*) Radka
      character*80 t80
      if(Radka.eq.' ') go to 9999
      idl=idel(Radka)
      do i=1,idl
        if(Radka(i:i).ne.' ') exit
      enddo
      im=i-1
      ik=im
1100  ip=ik+1
      if(ip.gt.idl) go to 9999
      ik=ik+80-im
      if(ip.ne.im+1) ik=ik-1
      ik=min(ik,idl)
1200  if(ik.lt.idl) then
1300    if(Radka(ik:ik).eq.' ') then
          ik=ik-1
          go to 1300
        endif
      endif
      t80=' '
      if(ip.ne.im+1) then
        t80(im+1:)='&'//Radka(ip:ik)
      else
        t80(im+1:)=Radka(ip:ik)
      endif
      write(ln,'(a)') t80(:idel(t80))
      go to 1100
9999  return
      end
      subroutine SpherHarmKontrol(h,f,n,klic)
      include 'fepc.cmn'
      include 'basic.cmn'
      real h(3),f((n+1)**2)
      r=sqrt(h(1)**2+h(2)**2+h(3)**2)
      if(r.le.0.) then
        f=0.
        go to 9999
      endif
      costh=h(3)/r
      costh=min(costh, 1.)
      costh=max(costh,-1.)
      fi=atan2(h(1)/r,h(2)/r)
      i=0
      do l=1,n+1
        do m=1,l
          pom=(-1)**(m-1)/sqrt(2.*pi)*plgndr(l-1,m-1,costh)
          if(m.eq.1) then
            i=i+1
            f(i)=pom
          else
            csp=cos(float(m-1)*fi)
            i=i+1
            f(i)=pom*csp
            snp=sin(float(m-1)*fi)
            i=i+1
            f(i)=pom*snp
          endif
        enddo
      enddo
      if(Klic.eq.1) then
        do i=1,(n+1)**2
          f(i)=f(i)*Clm(i)
        enddo
      else if(Klic.eq.2) then
        do i=1,(n+1)**2
          f(i)=f(i)*Mlm(i)
        enddo
      else if(Klic.eq.3) then
        do i=1,(n+1)**2
          f(i)=f(i)*Llm(i)
        enddo
      endif
9999  return
      end
      FUNCTION plgndr(l,m,x)
      INTEGER l,m
      REAL plgndr,x
      INTEGER i,ll
      REAL fact,pll,pmm,pmmp1,somx2
      if(m.lt.0.or.m.gt.l.or.abs(x).gt.1.) pause
     1                                     'bad arguments in plgndr'
      pmm=1.
      if(m.gt.0) then
        somx2=sqrt((1.-x)*(1.+x))
        fact=1.
        do 11 i=1,m
          pmm=-pmm*fact*somx2
          fact=fact+2.
11      continue
      endif
      if(l.eq.m) then
        plgndr=pmm
      else
        pmmp1=x*(2*m+1)*pmm
        if(l.eq.m+1) then
          plgndr=pmmp1
        else
          do 12 ll=m+2,l
            pll=(x*(2*ll-1)*pmmp1-(ll+m-1)*pmm)/(ll-m)
            pmm=pmmp1
            pmmp1=pll
12        continue
          plgndr=pll
        endif
      endif
      return
      END
      subroutine od0do1(x,y,n)
      include 'fepc.cmn'
      dimension x(n),y(n)
      do i=1,n
        y(i)=x(i)-aint(x(i))
        if(y(i).lt.0.) y(i)=y(i)+1.
        if(y(i).gt..9999.or.y(i).lt..0001) y(i)=0.
      enddo
      return
      end
      subroutine od0don(x,y,nn,n)
      include 'fepc.cmn'
      dimension x(n),y(n),nn(n)
      do i=1,n
        pom=x(i)
1100    if(pom.lt.0.) then
          pom=pom+nn(i)
          go to 1100
        endif
        y(i)=mod(pom,float(nn(i)))
      enddo
      return
      end

