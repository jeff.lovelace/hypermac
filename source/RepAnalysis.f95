      subroutine UseIrreps
      use Atoms_mod
      use Molec_mod
      use Basic_mod
      use RepAnal_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      external RASelEpiKer,FeVoid
      real, allocatable :: rtwo(:,:),ScTwP(:)
      real xr(9),TrMatP(36),MetTensO(9),rmp(9),rmq(9),CellParP(6)
      real, allocatable :: vt6p(:,:)
      character*128 Veta
      character*125 Ven(3)
      character*80  FlnOld,FlnPom,t80
      character*12, allocatable ::  Names(:)
      character*8   SvFile
      character*8, allocatable :: AtomOld(:)
      character*2 :: SmbABCPrime(3)=(/'a''','b''','c'''/)
      complex, allocatable :: pa(:,:),pb(:)
      integer, allocatable :: AddGen(:)
      integer UseTabsIn,SbwItemSelQuest,SbwItemPointerQuest,
     1        CrSystemNew,FeChdir
      logical EqRV,EqRV0,EqRVM,FeYesNo,ExistFile,StructureExists,
     1        ContinueWithOldStructure,Poprve,MatRealEqUnitMat,
     2        FeYesNoHeader,EqIgCase,CrlAcceptTransformation
      NDimOrg=NDim(KPhase)
      if(ExistM90) then
        ParentM90=fln(:ifln)//'.m90'
      else
        ParentM90=' '
      endif
      FromRepAnalysis=.true.
      FromGoToSub=.false.
      ContinueWithOldStructure=.true.
      FlnOld=Fln
      call iom50(0,0,fln(:ifln)//'.m50')
      if(ParentStructure) then
        Veta=fln(:ifln)//'.m43'
        if(ExistFile(Veta)) call CopyFile(Veta,fln(:ifln)//'.m44')
      else
        call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.m44')
      endif
      call iom40Only(0,0,fln(:ifln)//'.m44')
1100  call RAGenIrrep(ich)
      if(QPul) call CopyMat(RCommen(1,1,KPhase),TrMatSuperCell,3)
      if(ich.ne.0) then
        if(ich.eq.2) then
          go to 9300
        else
          go to 9990
        endif
      endif
      if(OpSystem.ge.0) then
        SvFile='full.pcx'
      else
        SvFile='full.bmp'
      endif
      NIrrepSel=0
      call SetLogicalArrayTo(AddSymm,NSymmS,.false.)
      do IrrepSel=1,NIrrep
        if(NIrrepSel.le.0) NIrrepSel=IrrepSel
      enddo
      il=max(NIrrep+2,12)
      xqd=700.
      if(NDimI(KPhase).gt.0) il=max(il,15)
      id=NextQuestId()
      WizardId=id
      WizardMode=.true.
      WizardTitle=.true.
      WizardLength=xqd
      WizardLines=il
      call FeQuestCreate(id,-1.,-1.,xqd,il,'x',0,LightGray,0,0)
1350  id=NextQuestId()
      Veta='Representation analysis:'
      call FeQuestTitleMake(id,Veta)
      il=1
      tpom=5.
      Veta='Representation'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom1=tpom+5.
      tpom=tpom+FeTxLength(Veta)+15.
      Veta='Dimension'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom2=tpom+10.
      tpom=tpom+FeTxLength(Veta)+20.
      if(NDimI(KPhase).gt.0.and..not.QPul) then
        Cislo='superspace'
      else
        Cislo='space'
      endif
      Veta='Shubnikov '//Cislo(:idel(Cislo))//' group'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom3=tpom
      tpom=tpom+180.
      Veta='Axes'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom4=tpom-40.
      tpom=tpom+200.
      Veta='Origin shift'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom5=tpom
      ilp=il
      il=WizardLines
      Veta='%Display representations'
      dpomb=FeTxLengthUnder(Veta)+20.
      xpomb=(xqd-dpomb)*.5
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      nButtDisplayRep=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      LastAtom=1
      Poprve=.true.
1450  if(Poprve) then
        il=ilp
      else
        nLbl=nLblTauFirst
        nButt=nButtShowFirst
      endif
      Ven(1)='Details'
      dpomb=FeTxLength(Ven(1))+20.
      xpomb=640.
      do i=1,NIrrep
        il=il+1
        if(Poprve) then
          call FeQuestLblMake(id,tpom1,il,SmbIrrep(i),'L','N')
          if(i.eq.1) nLblTauFirst=LblLastMade
        endif
        write(Veta,106) NDimIrrep(i)
        if(Poprve) then
          call FeQuestLblMake(id,tpom2,il,Veta,'L','N')
        else
         nLbl=nLbl+1
         call FeQuestLblChange(nLbl,Veta)
         nLbl=nLbl+4
        endif
        if(Poprve) then
          if(NDimI(KPhase).eq.1.and..not.QPul) then
            nd=NDim(KPhase)
          else
            nd=3
          endif
          call FeQuestLblMake(id,tpom3,il,GrpKer(i),'L','N')
          if(QPul) then
            call MatBlock3(TrMatKer(1,1,i),xr,NDim(KPhase))
            call Multm(RCommen(1,1,KPhase),xr,TrMatP,3,3,3)
          else
            call MatBlock3(TrMatKer(1,1,i),TrMatP,NDim(KPhase))
          endif
          call TrMat2String(TrMatP,3,Veta)
          call FeQuestLblMake(id,tpom4,il,Veta,'L','N')
          call OriginShift2String(ShSgKer(1,i),nd,Veta)
          call FeQuestLblMake(id,tpom5,il,Veta,'L','N')
          call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Ven(1))
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          if(i.eq.1) nButtShowFirst=ButtonLastMade
        else
          call FeQuestButtonOpen(nButt,ButtonOff)
          nButt=nButt+1
        endif
      enddo
      if(Poprve) nButtShowLast=ButtonLastMade
      Poprve=.false.
      call FeQuestButtonDisable(ButtonEsc)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        CheckNumber.ge.nButtShowFirst.and.
     2        CheckNumber.le.nButtShowLast) then
        IrrepSel=CheckNumber-nButtShowFirst+1
        Veta=fln(:ifln)//'.pom'
        call OpenFile(lst,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9990
        LstOpened=.true.
        call NewPg(1)
        IEpi=0
        call RALstOfDetails(1,IrrepSel,IEpi)
        call CloseListing
        HCFileName=SvFile
        call FeSaveImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile)
        call FeListView(Veta,0)
        call FeLoadImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile,0)
        call DeleteFile(SvFile)
        call DeleteFile(Veta)
        HCFileName=' '
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtDisplayRep) then
        call RAWriteIrr(SvFile)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.ne.0) go to 9900
      if(NQMag(KPhase).gt.0) then
        call iom50(0,0,fln(:ifln)//'.m50')
        if(QPul) call RASetCommen
        if(ParentStructure) then
          Veta=fln(:ifln)//'.m43'
          if(ExistFile(Veta)) call CopyFile(Veta,fln(:ifln)//'.m44')
        endif
      endif
      call SetLogicalArrayTo(BratSymm(1,KPhase),NSymmS,.true.)
      NSymmP=NSymmS
      go to 2000
      entry CrlMagGoToSubgroupFinish(ichf)
      id=WizardId
      EpiSelFr=1
      EpiSelTo=EpiSelFr+NEpi-1
      ISel=1
      FromRepAnalysis=.false.
      FromGoToSub=.true.
      FlnOld=Fln
      Klic=1
      if(NTwin.gt.1) then
        allocate(rtwo(9,NTwin))
        do i=1,NTwin
          call CopyMat(rtw(1,i),rtwo(1,i),3)
        enddo
      endif
2000  NTwinS=NTwin
      NTwinO=NTwin
      j=mxscu
2050  if(sc(j,1).le.0.) then
        j=j-1
        if(j.gt.1) go to 2050
      endif
      mxscutw=max(j+NTwin-1,6)
      mxscu=mxscutw-NTwin+1
      if(FromGoToSub) go to 3000
      if(NDimI(KPhase).eq.1.and.QPul) then
        nn=2*NSymmS*NLattVec(KPhase)
        if(allocated(NSymmEpiComm))
     1    deallocate(NSymmEpiComm,NLattVecEpiComm,rmEpiComm,s6EpiComm,
     2               vt6EpiComm,ZMagEpiComm)
        allocate(NSymmEpiComm(NEpi),NLattVecEpiComm(NEpi),
     1           rmEpiComm(9,nn,NEpi),s6EpiComm(3,nn,NEpi),
     2           vt6EpiComm(3,nn,NEpi),ZMagEpiComm(nn,NEpi))
        do i=1,NEpi
          ns=0
          do j=1,NSymmS
            if(BratSymmEpi(j,i)) then
              ns=ns+1
              call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
              call CopyMat(rmr (1,j),rm (1,ns,1,KPhase),3)
              call CopyVek(s6Epi(1,j,i),s6(1,ns,1,KPhase),
     1                     NDim(KPhase))
              zmag(ns,1,KPhase)=ZMagEpi(j,i)
              IswSymm(ns,1,KPhase)=1
            endif
          enddo
          allocate(vt6p(6,NLattVec(KPhase)))
          vt6p(1:NDim(KPhase),1:NLattVec(KPhase))=
     1      vt6(1:NDim(KPhase),1:NLattVec(KPhase),1,KPhase)
          nvt6p=NLattVec(KPhase)
          NSymm(KPhase)=ns
          call RACompleteSymmPlusMagInv(ns,ich)
          call SetRealArrayTo(ShSgEpi(1,i),NDimI(KPhase),0.)
          SwitchedToComm=.false.
          call ComSym(0,0,ichp)
          call CopyMat(MetTens(1,1,KPhase),MetTensO,3)
          call TrMat(RCommen(1,1,KPhase),rmq,3,3)
          call MultM(rmq,MetTens(1,1,KPhase),rmp,3,3,3)
          call MultM(rmp,RCommen(1,1,KPhase),MetTens(1,1,KPhase),3,3,3)
          call SuperSGToSuperCellSG(ich)
          if(ich.eq.0) then
            CellParP(1:6)=CellPar(1:6,1,KPhase)
            call UnitMat(TrMatP,NDim(KPhase))
            call DRMatTrCell(RCommen(1,1,KPhase),CellPar(1,1,KPhase),
     1                       TrMatP)
            NDim(KPhase)=3
            NDimI(KPhase)=0
            NDimQ(KPhase)=NDim(KPhase)**2
            call CrlOrderMagSymmetry
            call SetIgnoreWTo(.true.)
            call SetIgnoreETo(.true.)
            call FindSmbSgTr(Veta,xr,GrpEpi(i),NGrpEpi(i),TrMatP,
     1                       ShSgEpi(1,i),ii,ii)
            if(NGrpEpi(i).eq.0)
     1        call CrlGetNGrupa(GrpEpi(i),NGrpEpi(i),ii)
            call ResetIgnoreW
            call ResetIgnoreE
            call MatFromBlock3(TrMatP,TrMatEpi(1,1,i),4)
            NSymmEpiComm(i)=NSymm(KPhase)
            NLattVecEpiComm(i)=NLattVec(KPhase)
            do j=1,NLattVec(KPhase)
              call CopyVek(vt6(1,j,1,KPhase),vt6EpiComm(1,j,i),3)
            enddo
            do j=1,NSymm(KPhase)
              call CopyMat(rm6(1,j,1,KPhase),rmEpiComm(1,j,i),3)
              call CopyVek( s6(1,j,1,KPhase),s6EpiComm(1,j,i),3)
              ZMagEpiComm(j,i)=ZMag(j,1,KPhase)
            enddo
            NDim(KPhase)=4
            NDimI(KPhase)=1
            NDimQ(KPhase)=NDim(KPhase)**2
            NLattVec(KPhase)=nvt6p
            vt6(1:NDim(KPhase),1:NLattVec(KPhase),1,KPhase)=
     1        vt6p(1:NDim(KPhase),1:NLattVec(KPhase))
            CellPar(1:6,1,KPhase)=CellParP(1:6)
          endif
          call CopyMat(MetTensO,MetTens(1,1,KPhase),3)
          deallocate(vt6p)
        enddo
        close(66)
        do i=1,NEpi-1
          do j=i+1,NEpi
            if(EqIgCase(GrpEpi(i),GrpEpi(j)).and.
     1         EqRV(TrMatEpi(1,1,i),TrMatEpi(1,1,j),9,.001).and.
     2         EqRV(ShSgEpi(1,i),ShSgEpi(1,j),3,.001)) then
              NLattVecEpiComm(i)=0
              exit
            endif
          enddo
        enddo
        j=0
        do i=1,NEpi
          if(NLattVecEpiComm(i).le.0.or.i.eq.j) cycle
          nd=NDimIrrep(KIrrepEpi(i))
          if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
            ndp=nd/2
          else
            ndp=nd
          endif
          j=j+1
          GrpEpi(j)=GrpEpi(i)
          NGrpEpi(j)=NGrpEpi(i)
          NEqEpi(j)=NEqEpi(i)
          NEqAEpi(j)=NEqAEpi(i)
          NEqPEpi(j)=NEqPEpi(i)
          NSymmEpi(j)=NSymmEpi(i)
          KIrrepEpi(j)=KIrrepEpi(i)
          TakeEpi(j)=TakeEpi(i)
          NCondEpi(j)=NCondEpi(i)
          NCondEpiAll(j)=NCondEpiAll(i)
          call CopyVek(TrMatEpi(1,1,i),TrMatEpi(1,1,j),NDimQ(KPhase))
          call CopyVek(ShSgEpi(1,i),ShSgEpi(1,j),NDim(KPhase))
          do k=1,NSymmS
            BratSymmEpi(k,j)=BratSymmEpi(k,i)
            ZMagEpi(k,j)=ZMagEpi(k,i)
            call CopyVek(s6Epi(1,k,i),s6Epi(1,k,j),NDim(KPhase))
          enddo
          EqEpi(1:72,j)=(0.,0.)
          do ii=1,NEqEpi(i)
            do jj=1,nd
              kk=ii+2*(jj-1)*nd
              EqEpi(kk,j)=EqEpi(kk,i)
            enddo
          enddo
          EqAEpi(1:18,j)=(0.,0.)
          do ii=1,NEqAEpi(i)
            do jj=1,ndp
              kk=ii+2*(jj-1)*ndp
              EqAEpi(kk,j)=EqAEpi(kk,i)
            enddo
          enddo
          EqPEpi(1:18,j)=(0.,0.)
          do ii=1,NEqPEpi(i)
            do jj=1,ndp
              kk=ii+2*(jj-1)*ndp
              EqPEpi(kk,j)=EqPEpi(kk,i)
            enddo
            EqPPEpi(ii,j)=EqPPEpi(ii,i)
          enddo
          call CopyVekC(CondEpi(1,1,i),CondEpi(1,1,j),TriN**2)
          call CopyVekC(CondEpiAll(1,1,i),CondEpiAll(1,1,j),TriN**2)
          NSymmEpiComm(j)=NSymmEpiComm(i)
          NLattVecEpiComm(j)=NLattVecEpiComm(i)
          do k=1,NSymmEpiComm(i)
            call CopyVek(rmEpiComm(1,k,i),rmEpiComm(1,k,j),9)
            call CopyVek(s6EpiComm(1,k,i),s6EpiComm(1,k,j),3)
            ZMagEpiComm(k,j)=ZMagEpiComm(k,i)
          enddo
          do k=1,NLattVecEpiComm(i)
            call CopyVek(vt6EpiComm(1,k,NEpi),vt6EpiComm(1,k,j),3)
          enddo
        enddo
        NEpi=j
      endif
      id=NextQuestId()
      Veta='List of kernels and epikernels:'
      call FeQuestTitleMake(id,Veta)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_EpiKernels.tmp','formatted',
     1              'unknown')
      NFam=0
      if(allocated(EpiFam)) deallocate(EpiFam,NFamEpi,FamEpi,
     1                                 NSymmFam,OrdFam,OrdEpi)
      allocate(EpiFam(NEpi),NFamEpi(NEpi),FamEpi(NEpi,NEpi),
     1         NSymmFam(NEpi),OrdFam(NEpi),OrdEpi(NEpi))
      call SetIntArrayTo(NFamEpi,NEpi,0)
      call SetIntArrayTo( EpiFam,NEpi,0)
      do i=1,NEpi
        if(EpiFam(i).gt.0) cycle
        NFam=NFam+1
        NFamEpi(NFam)=1
        FamEpi(1,NFam)=i
        EpiFam(i)=NFam
        n=0
        do j=1,NSymmS
          if(BratSymmEpi(j,i)) n=n+1
        enddo
        NSymmFam(NFam)=-NGrpEpi(i)
        do 2100j=i+1,NEpi
          if(EpiFam(j).gt.0) cycle
          do k=1,NSymmS
            if(BratSymmEpi(k,i).neqv.BratSymmEpi(k,j)) go to 2100
          enddo
          NFamEpi(NFam)=NFamEpi(NFam)+1
          FamEpi(NFamEpi(NFam),NFam)=j
          EpiFam(j)=NFam
2100    continue
      enddo
      call indexx(NFam,NSymmFam,OrdFam)
      n=0
      if(NDimI(KPhase).eq.1.and..not.QPul) then
        nd=NDim(KPhase)
      else
        nd=3
      endif
      do im=1,NFam
        ifm=OrdFam(im)
        do j=1,NEpi
          if(EpiFam(j).ne.ifm) cycle
          n=n+1
          OrdEpi(n)=j
          if(QPul) then
            call MatBlock3(TrMatEpi(1,1,j),xr,NDim(KPhase))
            call Multm(RCommen(1,1,KPhase),xr,TrMatP,3,3,3)
          else
            call MatBlock3(TrMatEpi(1,1,j),TrMatP,NDim(KPhase))
          endif
          call TrMat2String(TrMatP,3,t80)
          Veta=GrpEpi(j)(:idel(GrpEpi(j)))//Tabulator//
     1         t80(:idel(t80))
          call OriginShift2String(ShSgEpi(1,j),nd,t80)
          Veta=Veta(:idel(Veta))//Tabulator//t80(:idel(t80))
          Veta=Veta(:idel(Veta))//Tabulator//SmbIrrep(KIrrepEpi(j))
          write(ln,FormA) Veta(:idel(Veta))
          CrSystem(KPhase)=CrSystemS
        enddo
      enddo
      call CloseIfOpened(ln)
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call FeTabsAdd(140.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(350.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(490.,UseTabs,IdRightTab,' ')
c      call FeTabsAdd(610.,UseTabs,IdRightTab,' ')
      xpom=15.
      dpom=WizardLength-30.-SbwPruhXd
      il=1
      if(NDimI(KPhase).gt.0.and..not.QPul) then
        Cislo='superspace'
        Fract=3.
      else
        Cislo='space'
        Fract=3.
      endif
      tpom=xpom+5.
      Veta='Shubnikov '//Cislo(:idel(Cislo))//' group'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+180
      Veta='Axes'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+160.
      Veta='Origin shift'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+120.
      Veta='Representation'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+110.
c      Veta='# of conditions'
c      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=WizardLines-2
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwEpikernels=SbwLastMade
      call FeQuestSbwSelectOpen(SbwLastMade,fln(:ifln)//
     1                          '_EpiKernels.tmp')
      il=il+1
      Veta='%Show details'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtDetails=ButtonLastMade
      il=il+1
      tpom=10.
      Veta='Select from above kernels/epikernels one representative'//
     1     ' of a family of Shubnikov '//Cislo(:idel(Cislo))//
     2     ' groups for testing.'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
2500  call FeQuestEventWithCheck(id,ich,RASelEpiKer,FeVoid)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDetails)
     1 then
        i=OrdEpi(SbwItemPointerQuest(nSbwEpikernels))
        Veta=fln(:ifln)//'.pom'
        call OpenFile(lst,Veta,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        LstOpened=.true.
        call NewPg(1)
        call RALstOfDetails(-1,KIrrepEpi(i),i)
        call CloseListing
        HCFileName=SvFile
        call FeSaveImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile)
        call FeListView(Veta,0)
        call FeLoadImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                   SvFile,0)
        call DeleteFile(SvFile)
        call DeleteFile(Veta)
        HCFileName=' '
        go to 2500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(ich.ne.0) then
        if(ich.eq.-1) then
          go to 9900
        else
          go to 1350
        endif
      else
        do i=1,NEpi
          if(SbwItemSelQuest(i,nSbwEpikernels).gt.0) then
            FamSel=EpiFam(OrdEpi(i))
            EpiSelFr=i
            go to 2600
          endif
        enddo
        j=SbwItemPointerQuest(nSbwEpikernels)
        FamSel=EpiFam(OrdEpi(j))
        do i=1,NEpi
          if(EpiFam(OrdEpi(i)).eq.FamSel) then
            EpiSelFr=i
            go to 2600
          endif
        enddo
        FamSel=EpiFam(OrdEpi(1))
        EpiSelFr=1
      endif
2600  EpiSelTo=EpiSelFr+NFamEpi(FamSel)-1
      ISel=OrdEpi(EpiSelFr)
3000  call SetLogicalArrayTo(BratSymm(1,KPhase),NSymmS,.true.)
      NSymmP=NSymmS
      if(allocated(AddGen)) deallocate(AddGen)
      allocate(AddGen(NSymmP))
      call SetLogicalArrayTo(AddSymmOrg,NSymmP,.false.)
      ii=0
      do i=1,NSymmP
        if(BratSymm(i,KPhase)) then
          ii=ii+1
          if(BratSymmEpi(ii,ISel)) then
            AddSymmOrg(i)=.true.
          endif
        endif
      enddo
      call SetIntArrayTo(AddGen,NSymmP,0)
      indc=1
      ii=0
      do i=1,NSymmP
        if(BratSymm(i,KPhase)) ii=ii+1
        if(AddSymmOrg(i)) cycle
        if(BratSymm(i,KPhase)) then
          if(BratSymmEpi(ii,ISel)) then
            cycle
          endif
        endif
        AddGen(indc)=i
        indc=indc+1
        jj=0
        do j=1,NSymmP
          if(BratSymm(j,KPhase)) then
            jj=jj+1
            if(.not.BratSymmEpi(jj,ISel)) cycle
          else
            cycle
          endif
          k=PerTabOrg(i,j)
          AddSymmOrg(k)=.true.
        enddo
      enddo
      ntrans=indc-1
      if(ParentStructure) then
        if(NQMag(KPhase).gt.0) then
          MaxUsedKw(KPhase)=1
        else
          MaxUsedKw(KPhase)=0
        endif
      endif
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      NSymmL(KPhase)=0
      if(allocated(ScTwP)) deallocate(ScTwP)
      allocate(ScTwP(NTwin*indc))
      ScTwP=0.
      ScTwP(1)=1.
      if(isPowder) then
        NTwin=1
        sctw(2:NTwinS,KDatBlock)=0.
        sctw(1,KDatBlock)=1.
        NTwinS=1
      else
        ScTwP(1:NTwinS)=sctw(1:NTwinS,KDatBlock)
      endif
      do i=1,indc-1
        j=AddGen(i)
        call CopyMat(rm6r(1,j),TransMat(1,i,1),NDim(KPhase))
        call CopyVek( s6r(1,j),TransVec(1,i,1),NDim(KPhase))
        if(MagneticType(KPhase).gt.0) TransZM(i)=1.
        call MatBlock3(TransMat(1,i,1),xr,NDim(KPhase))
        if(.not.isPowder) then
          do k=1,NTwinS
            NTwin=NTwin+1
            ScTwP(NTwin)=sctw(k,KDatBlock)
            call multm(rtw(1,k),xr,rtw(1,NTwin),3,3,3)
          enddo
        endif
      enddo
      if(.not.isPowder) then
        j=mxscu
3050    if(sc(j,1).le.0.) then
          j=j-1
          if(j.gt.1) go to 3050
        endif
        mxscutw=max(j+NTwin-1,6)
        mxscu=mxscutw-NTwin+1
      endif
      ns=0
      do i=1,NSymmS
        if(BratSymmEpi(i,ISel)) then
          ns=ns+1
          call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
          call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
          call CopyVek(s6Epi(1,i,ISel),s6(1,ns,1,KPhase),
     1                 NDim(KPhase))
          zmag(ns,1,KPhase)=ZMagEpi(i,ISel)
          IswSymm(ns,1,KPhase)=1
        endif
      enddo
      call RACompleteSymmPlusMagInv(ns,ich)
      call FindSmbSg(Grupa(KPhase),ChangeOrderNo,1)
      if(NDimI(KPhase).gt.0.and.NSymmN(KPhase).lt.NSymm(KPhase)) then
        if(allocated(isa)) deallocate(isa)
        allocate(isa(NSymm(KPhase),NAtAll))
        do i=1,NAtAll
          do j=1,NSymm(KPhase)
            isa(j,i)=j
          enddo
        enddo
        do KPh=1,NPhase
          if(NDim(KPh).gt.3) then
            call SetIntArrayTo(kw(1,1,KPh),3*mxw,0)
            if(NDim(KPh).eq.4) then
              j=mxw
            else
              j=NDimI(KPh)
            endif
            do i=1,j
              kw(mod(i-1,NDimI(KPh))+1,i,KPh)=(i-1)/NDimI(KPh)+1
            enddo
          endif
        enddo
      endif
      NQMagOld=NQMag(KPhase)
      NQMag(KPhase)=0
      Veta=fln(:ifln)//'_test_mag'
      i=NTwin
      j=mxscu
      NTwin=1
      mxscu=6
      mxscutw=6
      call iom40Only(0,0,fln(:ifln)//'.m44')
      NTwin=i
3100  if(sc(j,1).le.0.) then
        j=j-1
        if(j.gt.1) go to 3100
      endif
      mxscutw=max(j+NTwin-1,6)
      mxscu=mxscutw-NTwin+1
      if(ParentStructure) then
        do i=1,NAtInd
          if(kswa(i).ne.KPhase.or.AtTypeMag(isf(i),KPhase).eq.' ') cycle
          if(NDimI(KPhase).eq.1) then
            MagPar(i)=2
          else
            MagPar(i)=1
          endif
          call ReallocateAtomParams(NAtInd,itfmax,IFrMax,KModAMax,
     1                              MagPar(i))
          do j=1,3
             sm0(j,i)=.1/CellPar(j,iswa(i),KPhase)
            ssm0(j,i)=0.
            if(NDimI(KPhase).eq.1) then
               smx(j,1,i)=sm0(j,i)
              ssmx(j,1,i)=0.
               smy(j,1,i)=sm0(j,i)
              ssmy(j,1,i)=0.
            endif
          enddo
        enddo
      endif
      call iom40Only(1,0,fln(:ifln)//'.m44')
      ParentStructure=.false.
      call GoToSubGrMag(Veta,indc,ScTwP,NTwinS)
      if(isPowder) then
        if(ExistFile(fln(:ifln)//'.m95')) then
          WizardMode=.false.
          ExistM90=.false.
          SilentRun=.true.
          call EM9CreateM90(0,ich)
          WizardMode=.true.
          SilentRun=.false.
          call iom90(0,fln(:ifln)//'.m90')
        endif
      endif
      if(QPul) then
        Veta=fln(:ifln)//'_pom'
        call TrSuperMag(0,Veta)
        idl=idel(Veta)
        call MoveFile(Veta(:idl)//'.m40',fln(:ifln)//'.m40')
        call MoveFile(Veta(:idl)//'.m50',fln(:ifln)//'.m50')
        call MoveFile(Veta(:idl)//'.m90',fln(:ifln)//'.m90')
        call MoveFile(Veta(:idl)//'.m95',fln(:ifln)//'.m95')
        if(isPowder)
     1    call MoveFile(Veta(:idl)//'.m41',fln(:ifln)//'.m41')
        call DeleteAllFiles(Veta(:idl)//'*')
        call iom50(0,0,fln(:ifln)//'.m50')
        if(isPowder) call iom41(0,0,fln(:ifln)//'.m41')
      endif
      call iom40(0,0,fln(:ifln)//'.m40')
      call RenameAtomsAccordingToAtomType(NAtCalc)
      call iom40(1,0,fln(:ifln)//'.m40')
      call CrlMagTester(ich)
      if(FromGoToSub) ichf=ich
      if(allocated(TransMat)) call DeallocateTrans
      if(ich.ne.0) then
        if(.not.FromGoToSub) then
          call DeleteAllFiles(Fln(:iFln)//'*')
          Fln=FlnOld
          IFln=idel(Fln)
          if(NQMag(KPhase).gt.0) call SSG2QMag(fln)
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
        endif
        if(ich.eq.-1) then
          go to 9900
        else
          call FeQuestButtonLabelChange(ButtonOK-ButtonFr+1,'Next')
          if(FromGoToSub) then
            go to 9900
          else
            if(NQMag(KPhase).gt.0) then
              call iom50(0,0,fln(:ifln)//'.m50')
              if(QPul) call RASetCommen
              if(ParentStructure) then
                Veta=fln(:ifln)//'.m43'
                if(ExistFile(Veta))
     1            call CopyFile(Veta,fln(:ifln)//'.m44')
              endif
            endif
            go to 2000
          endif
        endif
      else
        if(FromGoToSub) then
          Veta='Do you want to continue with the last magnetic'
          if(NDimI(KPhase).gt.0) then
            t80=' superspace group?'
          else
            t80=' space group?'
          endif
          Veta=Veta(:idel(Veta))//t80(:idel(t80))
        else
          Veta='Do you want to create test structure for the last '//
     1         'epikernel?'
        endif
        call FeFillTextInfo('crlmagtester.txt',0)
        if(FeYesNoHeader(-1.,-1.,Veta,1)) then
          n=0
5000      n=n+1
          write(Cislo,'(i2)') n
          if(Cislo(1:1).eq.' ') Cislo(1:1)='0'
          FlnPom=FlnOld(:idel(FlnOld))//'_'//Cislo(1:2)
          if(StructureExists(FlnPom)) go to 5000
5100      call FeFileManager('Select structure name',FlnPom,' ',1,
     1                       .true.,ich)
          if(ich.le.0) then
            call CrlOrderMagSymmetry
            if(StructureExists(FlnPom)) then
              call ExtractFileName(FlnPom,Veta)
              if(.not.FeYesNo(-1.,-1.,'The structure "'//
     1           Veta(:idel(Veta))//'" already exists, rewrite it?',
     2           0)) go to 5100
            endif
            i=idel(FlnPom)
            call MoveFile(fln(:ifln)//'.m40',FlnPom(:i)//'.m40')
            call MoveFile(fln(:ifln)//'.m50',FlnPom(:i)//'.m50')
            if(ExistFile(fln(:ifln)//'.m41'))
     1        call MoveFile(fln(:ifln)//'.m41',FlnPom(:i)//'.m41')
            ExistM90=ExistFile(fln(:ifln)//'.m90')
            ExistM95=ExistFile(fln(:ifln)//'.m95')
            if(ExistM95)
     1        call MoveFile(fln(:ifln)//'.m95',FlnPom(:i)//'.m95')
            if(isPowder) then
              if(ExistM90)
     1          call MoveFile(fln(:ifln)//'.m90',FlnPom(:i)//'.m90')
            else
              if(ExistM90)
     1          call MoveFile(fln(:ifln)//'.m90',FlnPom(:i)//'.m90')
            endif
            call DeletePomFiles
            call DeleteAllFiles(fln(:ifln)//'.*')
            call ExtractDirectory(FlnPom,Veta)
            i=FeChdir(Veta)
            call FeGetCurrentDir
            call ExtractFileName(FlnPom,Fln)
            ifln=idel(fln)
            call FeBottomInfo(' ')
            call DeletePomFiles
            KPhase=1
            KPhaseBasic=1
            KDatBlock=1
            call iom40(0,0,fln(:ifln)//'.m40')
            do i=1,NAtCalc
              if(itf(i).gt.1) call ZmTF21(i)
            enddo
            call iom40(1,0,fln(:ifln)//'.m40')
            call RefOpenCommands
            lni=NextLogicNumber()
            open(lni,file=fln(:ifln)//'_fixed.tmp')
            lno=NextLogicNumber()
            open(lno,file=fln(:ifln)//'_fixed_new.tmp')
5200        read(lni,FormA,end=5220) Veta
            write(lno,FormA) Veta(:idel(Veta))
            go to 5200
5220        call CloseIfOpened(lni)
            Veta='fixed xyz *'
            write(lno,FormA) Veta(:idel(Veta))
            Veta='restric *'
            if(NDimI(KPhase).gt.0) then
              Cislo=' 13'
            else
              Cislo=' 12'
            endif
            write(lno,FormA) Veta(:idel(Veta))//Cislo(:3)
            close(lno)
            call MoveFile(fln(:ifln)//'_fixed_new.tmp',
     1                    fln(:ifln)//'_fixed.tmp')
            NacetlInt(nCmdncykl)=100
            NacetlReal(nCmdtlum)=.1
            NacetlInt(nCmdLSMethod)=2
            NacetlReal(nCmdMarqLam)=.001
            call RefRewriteCommands(1)
            ContinueWithOldStructure=.false.
            call iom50(0,0,fln(:ifln)//'.m50')
            call iom40(0,0,fln(:ifln)//'.m40')
          else
            go to 7000
          endif
        else
          go to 7000
        endif
        if(NDimOrg.eq.3) then
          call CopyMat(TrMatEpi(1,1,EpiSel),TrMatP,3)
        else
          call MatBlock3(TrMatEpi(1,1,EpiSel),TrMatP,NDimOrg)
        endif
        if(.not.MatRealEqUnitMat(TrMatP,3,.001).or.
     1     .not.EqRV0(ShSgEpi(1,EpiSel),3,.0001)) then
          Veta='The following transformation can bring the structure '//
     1         'to the standard setting:'
          if(.not.CrlAcceptTransformation(Veta,TrMatP,ShSgEpi(1,EpiSel),
     1                                    3,SmbABC,0)) go to 7100
          if(QPul) then
            call MatBlock3(TrMatEpi(1,1,EpiSel),TrMatP,4)
          else
            call CopyMat(TrMatEpi(1,1,EpiSel),TrMatP,NDim(KPhase))
          endif
          call DRCellDefinedTrans(TrMatP)
          if(.not.EqRV0(ShSgEpi(1,EpiSel),3,.0001))
     1      call OriginDefinedShift(ShSgEpi(1,EpiSel))
          do i=1,NAtCalc
            if(kswa(i).ne.KPhase.or.MagPar(i).eq.0) cycle
            call CrlMagTesterSetAtom(i,.1,.false.)
          enddo
          call iom40(1,0,fln(:ifln)//'.m40')
        endif
      endif
      go to 7100
7000  call DeleteAllFiles(Fln(:iFln)//'*')
      ContinueWithOldStructure=.true.
7100  if(allocated(AddGen)) deallocate(AddGen)
      go to 9900
9300  call FeUnforeseenError('searching for physically irreducible '//
     1                       'representation faild.')
9900  call FeQuestRemove(id)
      WizardMode=.false.
      if(FromGoToSub) go to 9999
      if(allocated(KIrrepEpi))
     1  deallocate(KIrrepEpi,BratSymmEpi,GrpEpi,NGrpEpi,TrMatEpi,
     2             ShSgEpi,ZMagEpi,s6Epi,NCondEpi,CondEpi,NCondEpiAll,
     3             CondEpiAll,NSymmEpi,NEqEpi,EqEpi,NEqAEpi,EqAEpi,
     4             NEqPEpi,EqPEpi,EqPPEpi,TakeEpi)
9990  if(allocated(NDimIrrep))
     1  deallocate(NDimIrrep,RIrrep,EigenValIrrep,EigenVecIrrep,
     2             SmbIrrep,TraceIrrep,NBasFun,
     3             BratSymmKer,ZMagKer,s6Ker,GrpKer,NGrpKer,ShSgKer,
     4             TrMatKer,AddSymm,NCondKer,CondKer)
      if(allocated(pa)) deallocate(pa,pb)
      if(allocated(BasFun)) deallocate(BasFun,BasFunOrg,MultIrrep)
      if(allocated(IGen)) deallocate(IGen,KGen,StGen,IoGen,
     1                               PerTab,rm6r,rmr,s6r,ZMagR)
      if(allocated(EpiFam)) deallocate(EpiFam,NFamEpi,FamEpi,
     1                                 NSymmFam,OrdFam,OrdEpi)
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(allocated(AtomOld)) deallocate(AtomOld)
      if(allocated(Names)) deallocate(Names)
      if(allocated(PerTabOrg)) deallocate(PerTabOrg,AddSymmOrg,
     1                                    SymmOrdOrg,SymmPorOrg,
     2                                    KSymmLG)
      if(allocated(rtwo)) deallocate(rtwo)
      if(allocated(BratSub)) deallocate(BratSub,NBratSub,IPorSub,GenSub,
     1                                  NGenSub,TakeSub)
      if(allocated(NSymmEpiComm))
     1  deallocate(NSymmEpiComm,NLattVecEpiComm,rmEpiComm,s6EpiComm,
     2             vt6EpiComm,ZMagEpiComm)
      if(allocated(ScTwP)) deallocate(ScTwP)
9999  if(ContinueWithOldStructure) then
        call DeleteFile(PreviousM40)
        call DeleteFile(PreviousM50)
        Fln=FlnOld
        IFln=idel(Fln)
      else
        if(ExistM95) then
          call DeleteFile(Fln(:ifln)//'.m90')
          ExistM90=.false.
          SilentRun=.true.
          call EM9CreateM90(1,ich)
          SilentRun=.false.
        endif
      endif
      if(allocated(vt6p)) deallocate(vt6p)
      ParentM90=' '
      return
103   format(6(' (',f10.6,',',f10.6,')'))
106   format(i5)
      end
      subroutine RAGenIrrep(ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real xr(9),xi(9)
      complex xp(10),pomc
      character*80 Veta,PointGroup
      integer CrlCentroSymm,TriNPom,GetSymmOrd,iar(3)
      integer, allocatable :: IPorEpi(:),ShValEpi(:)
      logical EqRV,EqIgCase
      ich=0
      ln=0
      if(allocated(PerTabOrg)) deallocate(PerTabOrg,AddSymmOrg,
     1                                    SymmOrdOrg,SymmPorOrg,
     2                                    KSymmLG)
      allocate(PerTabOrg(NSymmN(KPhase),NSymmN(KPhase)),
     1         AddSymmOrg(NSymmN(KPhase)),SymmOrdOrg(NSymmN(KPhase)),
     2         SymmPorOrg(NSymmN(KPhase)),KSymmLG(NSymmN(KPhase)))
      if(.not.ParentStructure) then
        if(NDimI(KPhase).eq.1) then
          QMag(1:3,1,KPhase)=qu(1:3,1,1,KPhase)
          QMagIrr(1:3,1,KPhase)=qu(1:3,1,1,KPhase)
          do i=1,3
            if(abs(qui(i,1,KPhase)).gt..001)
     1        QMagIrr(i,1,KPhase)=1./sqrt(float(i+4))
          enddo
        else
          QMag(1:3,1,KPhase)=0.
          QMagIrr(1:3,1,KPhase)=0.
        endif
      endif
      do i=1,NSymmN(KPhase)
        SymmOrdOrg(i)=-GetSymmOrd(rm(1,i,1,KPhase))
        if(NDimI(KPhase).eq.1) then
          KSymmLG(i)=0
          call Multm(qu(1,1,1,KPhase),rm(1,i,1,KPhase),xr,1,3,3)
          do j=1,3
            xr(j)=xr(j)-qu(j,1,1,KPhase)
          enddo
          do j=1,3
            if(abs(xr(j)-anint(xr(j))).gt..001) go to 1050
          enddo
          do j=2,NLattVec(KPhase)
            pom=scalmul(xr,vt6(1,j,1,KPhase))
            if(abs(pom-anint(pom)).gt..001) go to 1050
          enddo
          KSymmLG(i)=1
        else
          KSymmLG(i)=1
        endif
1050    do j=1,NSymmN(KPhase)
          call MultM(rm(1,i,1,KPhase),rm(1,j,1,KPhase),xr,3,3,3)
          do k=1,NSymmN(KPhase)
            if(EqRV(xr,rm(1,k,1,KPhase),9,.01)) then
              PerTabOrg(i,j)=k
              exit
            endif
          enddo
        enddo
      enddo
      call Indexx(NSymmN(KPhase),SymmOrdOrg,SymmPorOrg)
      if(allocated(IGen)) deallocate(IGen,KGen,StGen,IoGen,PerTab,rm6r,
     1                               rmr,s6r,ZMagR)
      n=NSymmN(KPhase)
      if(NDimI(KPhase).gt.0.and.n.ge.NSymm(KPhase))
     1  call ReallocSymm(NDim(KPhase),2*n,NLattVec(KPhase),
     2                   NComp(KPhase),NPhase,0)
      invc=CrlCentroSymm()
      allocate(IGen(n),KGen(n),StGen(n),ioGen(3,n),
     1         PerTab(n,n),rm6r(36,n),rmr(9,n),s6r(6,n),ZMagR(n))
      do i=1,NSymmN(KPhase)
        call CopyMat(rm6(1,i,1,KPhase),rm6r(1,i),NDim(KPhase))
        call CopyMat(rm (1,i,1,KPhase),rmr (1,i),3)
        call CopyVek(s6 (1,i,1,KPhase),s6r (1,i),NDim(KPhase))
        ZMagR(i)=zmag(i,1,KPhase)
        do j=1,NSymmN(KPhase)
          call MultM(rmr(1,i),rm(1,j,1,KPhase),xr,3,3,3)
          do k=1,NSymmN(KPhase)
            if(EqRV(xr,rm(1,k,1,KPhase),9,.01)) then
              PerTab(i,j)=k
              exit
            endif
          enddo
        enddo
      enddo
      NSymmS=NSymmN(KPhase)
      NGrupaS=NGrupa(KPhase)
      GrupaS=Grupa(KPhase)
      call RAGenSubgroups
      NTwinS=1
      NTwin =1
      mxscu=6
      mxscutw=6
      CrSystemS=CrSystem(KPhase)
      if(NDimI(KPhase).le.0) then
        NSymmBlock=3
        NSymmLG=NSymmN(KPhase)
!        go to 1150
        ln=NextLogicNumber()
        call GetPointGroupFromSpaceGroup(PointGroup,i,j)
        if(OpSystem.le.0) then
          Veta=HomeDir(:idel(HomeDir))//'symmdat'//ObrLom//
     1         'pgroup.dat'
        else
          Veta=HomeDir(:idel(HomeDir))//'symmdat/pgroup.dat'
        endif
        call OpenFile(ln,Veta,'formatted','old')
        TriNPom=NSymmBlock*NSymmN(KPhase)
        do ik=0,1
          if(ik.eq.1) then
            n=6
            if(allocated(NDimIrrep))
     1        deallocate(NDimIrrep,RIrrep,EigenValIrrep,
     2                   SmbIrrep,TraceIrrep,NBasFun,
     3                   BratSymmKer,ZMagKer,s6Ker,GrpKer,NGrpKer,
     4                   ShSgKer,TrMatKer,AddSymm,NCondKer,CondKer)
            TriNPom=NSymmBlock*NSymmN(KPhase)
            allocate(NDimIrrep(NIrrep),RIrrep(n**2,NSymmS,NIrrep),
     1               EigenValIrrep(n,NSymmS,NIrrep),
     2               EigenVecIrrep(n**2,NSymmS,NIrrep),SmbIrrep(NIrrep),
     3               TraceIrrep(NSymmS,NIrrep),NBasFun(NIrrep),
     4               BratSymmKer(NSymmS,NIrrep),zmagKer(NSymmS,NIrrep),
     5               s6Ker(6,NSymmS,NIrrep),GrpKer(NIrrep),
     6               NGrpKer(NIrrep),ShSgKer(6,NIrrep),
     7               TrMatKer(NDim(KPhase),NDim(KPhase),NIrrep),
     8               AddSymm(NSymmS),NCondKer(NIrrep),
     9               CondKer(TriNPom,TriNPom,NIrrep))
          endif
          rewind ln
1100      read(ln,FormA,end=1120) Veta
          k=0
          call Kus(Veta,k,Cislo)
          if(.not.EqIgCase(Cislo,PointGroup).or.Veta(1:1).eq.' ')
     1       go to 1100
          call Kus(Veta,k,Cislo)
          call StToInt(Veta,k,iar,1,.false.,ich)
          if(ich.ne.0) go to 1120
          do i=1,iar(1)
            read(ln,FormA,end=1120) Veta
          enddo
          read(ln,FormA,end=1120) Veta
          k=0
          call StToInt(Veta,k,iar,1,.false.,ich)
          do i=1,iar(1)
            read(ln,FormA,end=1120) Veta
          enddo
          NIrrep=0
          NDimIrrepMax=0
1120      read(ln,FormA,end=9900) Veta
          k=0
          call kus(Veta,k,Cislo)
          if(EqIgCase(Cislo(1:2),'GM')) then
            NIrrep=NIrrep+1
            i=NIrrep
            if(ik.gt.0) SmbIrrep(NIrrep)=Cislo
            call kus(Veta,k,Cislo)
            if(EqIgCase(Cislo,'Dimension:')) then
              call StToInt(Veta,k,iar,1,.false.,ich)
              NDimP=iar(1)
              if(ik.eq.0) then
                do j=1,NSymmN(KPhase)
                  read(ln,'(i5,9f8.4/5x,9f8.4)',err=9900) jj,
     1              ((pom,pom,l=1,NDimP),k=1,NDimP)
                enddo
              else
                NDimIrrep(i)=iar(1)
                do j=1,NSymmN(KPhase)
                  read(ln,'(i5,9f8.4/5x,9f8.4)',err=9900) jj,
     1              ((xr(k+(l-1)*NDimIrrep(i)),
     2                xi(k+(l-1)*NDimIrrep(i)),l=1,NDimP),k=1,NDimP)
                  do k=1,NDimIrrep(i)**2
                    if(abs(abs(xr(k))-.866).lt..01)
     1                xr(k)=sign(sqrt(.75),xr(k))
                    if(abs(abs(xi(k))-.866).lt..01)
     1                xi(k)=sign(sqrt(.75),xi(k))
                    RIrrep(k,j,i)=cmplx(xr(k),xi(k))
                  enddo
                enddo
              endif
              go to 1120
            else
              go to 9900
            endif
          endif
        enddo
        call CloseIfOpened(ln)
        go to 1160
1150    call RAKarep(ich)
1160    QPul=.false.
      else
        NSymmBlock=6
        call RAKarep(ich)
        if(ich.ne.0) go to 9900
        QPul=.true.
        do i=1,3
          pom=qu(i,1,1,KPhase)*2.
          if(abs(anint(pom)-pom).gt..00001) then
            QPul=.false.
            exit
          endif
        enddo
        if(QPul) call RASetCommen
      endif
      MaxBasFun=NSymmBlock*NSymmS+1
      TriN=NSymmBlock*NSymmS
      NDimIrrepMax=0
      do i=1,NIrrep
        nd=NDimIrrep(i)
        NDimIrrepMax=max(NDimIrrepMax,nd)
        do j=1,NSymmS
          do k=1,nd**2
            if(abs(abs(real(RIrrep(k,j,i)))-.866).lt..01)
     1        RIrrep(k,j,i)=cmplx(sign(sqrt(.75),real(RIrrep(k,j,i))),
     2                            aimag(RIrrep(k,j,i)))
            if(abs(abs(aimag(RIrrep(k,j,i)))-.866).lt..01)
     1        RIrrep(k,j,i)=cmplx(real(RIrrep(k,j,i)),
     2                            sign(sqrt(.75),aimag(RIrrep(k,j,i))))
          enddo
          pomc=(0.,0.)
          do k=1,nd
            pomc=pomc+RIrrep(k+(k-1)*NDimIrrep(i),j,i)
          enddo
          TraceIrrep(j,i)=pomc
          call JacobiComplex(RIrrep(1,j,i),nd,EigenValIrrep(1,j,i),
     1                       EigenVecIrrep(1,j,i),NRot)
        enddo
        call RAGrSmbFromEigenVal(i,ich)
        if(ich.ne.0) then
          SmbIrrep(i)='???'
!        else
!          SmbIrrep(i)=' '
        endif
        call RAGetIrrSymbol(SmbIrrep(i),GrupaS,NGrupaS,GrpKer(i),
     1                      NGrpKer(i),i,ich)
        if(ich.ne.0) then
          write(SmbIrrep(i),'(''Tau'',i3)') i
          call Zhusti(SmbIrrep(i))
          ich=0
        endif
      enddo
      call RAGetBasFun
      NEpi=0
      NEpiMax=NIrrep
      n=NIrrep
      if(allocated(KIrrepEpi))
     1  deallocate(KIrrepEpi,BratSymmEpi,GrpEpi,NGrpEpi,TrMatEpi,
     2             ShSgEpi,ZMagEpi,s6Epi,NCondEpi,CondEpi,NCondEpiAll,
     3             CondEpiAll,NSymmEpi,NEqEpi,EqEpi,NEqAEpi,EqAEpi,
     4             NEqPEpi,EqPEpi,EqPPEpi,TakeEpi)
      TriNPom=NSymmBlock*NSymmS
      allocate(KIrrepEpi(n),BratSymmEpi(NSymmS,n),GrpEpi(n),NGrpEpi(n),
     1         TrMatEpi(NDim(KPhase),NDim(KPhase),n),
     2         ShSgEpi(NDim(KPhase),n),TakeEpi(n),
     3         ZMagEpi(NSymmS,n),s6Epi(6,NSymmS,n),
     4         NCondEpi(n),CondEpi(TriNPom,TriNPom,n),NCondEpiAll(n),
     5         CondEpiAll(TriNPom,TriNPom,n),NSymmEpi(n),
     6         NEqEpi(n),EqEpi(72,n),NEqAEpi(n),EqAEpi(72,n),
     7         NEqPEpi(n),EqPEpi(18,n),EqPPEpi(6,n))
      do i=1,NIrrep
        if(GrpKer(i)(1:4).ne.'----') call RAGenEpiKer(i)
      enddo
      if(allocated(IPorEpi)) deallocate(IPorEpi,ShValEpi)
      allocate(IPorEpi(NEpi),ShValEpi(NEpi))
      do i=1,NEpi
        n=0
        m=0
        do j=1,NDim(KPhase)
          if(abs(ShSgEpi(j,i)).gt..01) then
            n=n+1
            if(n.eq.1) m=j
          endif
        enddo
        ShValEpi(i)=-m-100*(NDim(KPhase)-n+1)-
     1              10000*(NIrrep-KIrrepEpi(i)+1)
      enddo
      call indexx(NEpi,ShValEpi,IPorEpi)
      allocate(KIrrepEpiO(NEpi),BratSymmEpiO(NSymmS,NEpi),
     1         GrpEpiO(NEpi),NGrpEpiO(NEpi),
     2         TrMatEpiO(NDim(KPhase),NDim(KPhase),NEpi),
     3         ShSgEpiO(NDim(KPhase),NEpi),
     4         ZMagEpiO(NSymmS,NEpi),s6EpiO(6,NSymmS,NEpi),
     5         NSymmEpiO(NEpi),TakeEpiO(NEpi),
     6         NEqEpiO(NEpi),EqEpiO(72,NEpi),
     7         NEqAEpiO(NEpi),EqAEpiO(18,NEpi),
     8         NEqPEpiO(NEpi),EqPEpiO(18,NEpi),EqPPEpiO(6,NEpi),
     9         NCondEpiO(NEpi),CondEpiO(TriN,TriN,NEpi),
     a         NCondEpiAllO(NEpi),CondEpiAllO(TriN,TriN,NEpi))
      do i=1,NEpi
        nd=NDimIrrep(KIrrepEpi(i))
        if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
          ndp=nd/2
        else
          ndp=nd
        endif
        GrpEpiO(i)=GrpEpi(i)
        NGrpEpiO(i)=NGrpEpi(i)
        NEqEpiO(i)=NEqEpi(i)
        NEqAEpiO(i)=NEqAEpi(i)
        NEqPEpiO(i)=NEqPEpi(i)
        NSymmEpiO(i)=NSymmEpi(i)
        KIrrepEpiO(i)=KIrrepEpi(i)
        TakeEpiO(i)=TakeEpi(i)
        NCondEpiO(i)=NCondEpi(i)
        NCondEpiAllO(i)=NCondEpiAll(i)
        call CopyVek(TrMatEpi(1,1,i),TrMatEpiO(1,1,i),
     1               NDimQ(KPhase))
        call CopyVek(ShSgEpi(1,i),ShSgEpiO(1,i),NDim(KPhase))
        do j=1,NSymmS
          BratSymmEpiO(j,i)=BratSymmEpi(j,i)
          ZMagEpiO(j,i)=ZMagEpi(j,i)
          call CopyVek(s6Epi(1,j,i),s6EpiO(1,j,i),NDim(KPhase))
        enddo
        EqEpiO(1:72,i)=(0.,0.)
        do ii=1,NEqEpi(i)
          do jj=1,nd
            kk=ii+2*(jj-1)*nd
            EqEpiO(kk,i)=EqEpi(kk,i)
          enddo
        enddo
        EqAEpiO(1:18,i)=(0.,0.)
        do ii=1,NEqAEpi(i)
          do jj=1,ndp
            kk=ii+2*(jj-1)*ndp
            EqAEpiO(kk,i)=EqAEpi(kk,i)
          enddo
        enddo
        EqPEpiO(1:18,i)=(0.,0.)
        do ii=1,NEqPEpi(i)
          do jj=1,ndp
            kk=ii+2*(jj-1)*ndp
            EqPEpiO(kk,i)=EqPEpi(kk,i)
          enddo
          EqPPEpiO(ii,i)=EqPPEpi(ii,i)
        enddo
        call CopyVekC(CondEpi,CondEpiO,TriN**2*NEpi)
        call CopyVekC(CondEpiAll,CondEpiAllO,TriN**2*NEpi)
      enddo
      n=0
      do 3100ip=1,NEpi
        i=IPorEpi(ip)
        pomo=0.
        PrvniO=0.
        do k=1,NDim(KPhase)
          pomo=pomo+abs(ShSgEpiO(k,i))
          if(abs(PrvniO).lt..001.and.
     1       abs(ShSgEpiO(k,i)).gt..001) PrvniO=ShSgEpiO(k,i)
        enddo
        do j=1,n
          if(KIrrepEpiO(i).ne.KIrrepEpi(j).or.
     1       (NGrpEpiO(i).gt.0.and.NGrpEpi(j).gt.0.and.
     2        NGrpEpiO(i).ne.NGrpEpi(j)).or.
     3        NSymmEpiO(i).ne.NSymmEpi(j)) cycle
          if(EqIgCase(GrpEpiO(i),GrpEpi(j))) then
            pom=0.
            Prvni=0.
            do k=1,NDim(KPhase)
              pom=pom+abs(ShSgEpi(k,j))
              if(abs(Prvni).lt..001.and.
     1           abs(ShSgEpi(k,j)).gt..001) Prvni=ShSgEpi(k,j)
            enddo
            if(pomo.lt.pom-.001.or.(abs(pomo-pom).le..001.and.
     1         PrvniO-Prvni.gt..001)) then
              m=j
              go to 3050
            else
              go to 3100
            endif
          endif
          do k=1,NSymmS
            if(BratSymmEpiO(k,i).neqv.BratSymmEpi(k,j)) go to 2950
          enddo
2950      do 3000itw=2,NSymmS
            if(BratSymmEpiO(itw,i)) go to 3000
            itwi=2
            do while(PerTabOrg(itw,itwi).ne.1)
              itwi=itwi+1
            enddo
            do k=2,NSymmS
              if(.not.BratSymmEpiO(k,i)) cycle
              m=PerTab(PerTabOrg(itw,k),itwi)
              if(.not.BratSymmEpi(m,j)) go to 3000
              if(NDimI(KPhase).eq.1) then
                if(abs(s6EpiO(4,k,i)-s6Epi(4,m,j)).gt..01)
     1            go to 3000
              else
                if(abs(ZMagEpiO(k,i)-ZMagEpi(m,j)).gt..01)
     1            go to 3000
              endif
            enddo
            go to 3100
3000      continue
        enddo
        n=n+1
        m=n
3050    nd=NDimIrrep(KIrrepEpi(i))
        if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
          ndp=nd/2
        else
          ndp=nd
        endif
        GrpEpi(m)=GrpEpiO(i)
        NGrpEpi(m)=NGrpEpiO(i)
        NEqEpi(m)=NEqEpiO(i)
        NEqAEpi(m)=NEqAEpiO(i)
        NEqPEpi(m)=NEqPEpiO(i)
        NSymmEpi(m)=NSymmEpiO(i)
        KIrrepEpi(m)=KIrrepEpiO(i)
        NCondEpi(m)=NCondEpiO(i)
        NCondEpiAll(m)=NCondEpiAllO(i)
        call CopyVek(TrMatEpiO(1,1,i),TrMatEpi(1,1,m),NDimQ(KPhase))
        call CopyVek(ShSgEpiO(1,i),ShSgEpi(1,m),NDim(KPhase))
        do j=1,NSymmS
          BratSymmEpi(j,m)=BratSymmEpiO(j,i)
          ZMagEpi(j,m)=ZMagEpiO(j,i)
          call CopyVek(s6EpiO(1,j,i),s6Epi(1,j,m),NDim(KPhase))
        enddo
        do ii=1,NEqEpiO(i)
          do jj=1,nd
            kk=ii+2*(jj-1)*nd
            EqEpi(kk,m)=EqEpiO(kk,i)
            EqEpi(kk,m)=EqEpiO(kk,i)
          enddo
        enddo
        do ii=1,NEqAEpiO(i)
          do jj=1,ndp
            kk=ii+(jj-1)*ndp
            EqAEpi(kk,m)=EqAEpiO(kk,i)
            EqAEpi(kk,m)=EqAEpiO(kk,i)
          enddo
        enddo
        do ii=1,NEqPEpiO(i)
          do jj=1,ndp
            kk=ii+2*(jj-1)*ndp
            EqPEpi(kk,m)=EqPEpiO(kk,i)
            EqPEpi(kk,m)=EqPEpiO(kk,i)
          enddo
          EqPPEpi(ii,m)=EqPPEpiO(ii,i)
        enddo
        call CopyVekC(CondEpiO(1,1,i),CondEpi(1,1,m),TriN**2)
        call CopyVekC(CondEpiAllO(1,1,i),CondEpiAll(1,1,m),TriN**2)
3100  continue
      NEpi=n
      deallocate(KIrrepEpiO,BratSymmEpiO,GrpEpiO,NGrpEpiO,
     1           TrMatEpiO,ShSgEpiO,ZMagEpiO,s6EpiO,NSymmEpiO,
     2           NEqEpiO,EqEpiO,NEqAEpiO,EqAEpiO,TakeEpiO,
     3           NEqPEpiO,EqPEpiO,EqPPEpiO,
     4           NCondEpiO,CondEpiO,NCondEpiAllO,CondEpiAllO)
      go to 9999
9300  ich=2
      go to 9999
9900  ich=1
9999  call CloseIfOpened(ln)
      if(allocated(IPorEpi)) deallocate(IPorEpi,ShValEpi)
      return
      end
      subroutine RASelEpiKer
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer SbwItemSelQuest,SbwItemPointerQuest,SbwItemFromQuest
      ItemFromOld=SbwItemFromQuest(nSbwEpikernels)
      m=SbwItemPointerQuest(nSbwEpikernels)
      if(SbwItemSelQuest(m,nSbwEpikernels).gt.0) then
        j=EpiFam(OrdEpi(m))
      else
        j=0
      endif
      do i=1,NEpi
        if(EpiFam(OrdEpi(i)).eq.j) then
          k=1
        else
          k=0
        endif
        call FeQuestSetSbwItemSel(i,nSbwEpikernels,k)
      enddo
      call FeQuestSbwShow(nSbwEpikernels,ItemFromOld)
      return
      end
      subroutine RAGrSmbFromEigenVal(NIrr,ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real xp(4),xpp(4),TrMatP(36),Shift(6),CellParP(6)
      complex Delta
      character*80 Veta,t80
      integer CrSystemNew,CrlCentroSymm
      logical EqRV
      call SetLogicalArrayTo(BratSymmKer(1,NIrr),NSymmS,.false.)
      call SetRealArrayTo(s6Ker(1,1,NIrr),6*NSymmS,0.)
      call SetRealArrayTo(ZMagKer(1,NIrr),NSymmS,1.)
      nd=NDimIrrep(NIrr)
      if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
        ndp=nd/2
      else
        ndp=nd
      endif
      do i=1,NSymmS
        call CopyVek(s6r(1,i),s6Ker(1,i,NIrr),NDim(KPhase))
      enddo
      do 1100i=1,NSymmS
        if(KSymmLG(i).eq.0) cycle
        Delta=EigenValIrrep(1,i,NIrr)
        do j=2,ndp
          if(abs(EigenValIrrep(j,i,NIrr)-Delta).gt..001) go to 1100
        enddo
        BratSymmKer(i,NIrr)=.true.
        if(NDimI(KPhase).le.0) then
          if(abs(Delta-(1.,0.)).lt..001) then
            ZMagKer(i,NIrr)= 1.
          else if(abs(Delta+(1.,0.)).lt..001) then
            ZMagKer(i,NIrr)=-1.
          else
            BratSymmKer(i,NIrr)=.false.
            go to 1100
          endif
        else
          call qbyx(s6r(1,i),xp,1)
          xp(1)=atan2(aimag(Delta),real(Delta))/(2.*pi)-xp(1)
          call od0do1(xp,xp,1)
          Phi=xp(1)
          call CopyVek(s6r(1,i),s6Ker(1,i,NIrr),NDim(KPhase))
          s6Ker(4,i,NIrr)=Phi
        endif
1100  continue
      if(nd.le.2) then
        do i=1,NSymmS
          if(KSymmLG(i).eq.0) then
            BratSymmKer(i,NIrr)=.true.
            exit
          endif
        enddo
      endif
      ns=0
      do i=1,NSymmS
        if(BratSymmKer(i,NIrr)) then
          ns=ns+1
          call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
          call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
          call CopyVek(s6Ker(1,i,NIrr),s6(1,ns,1,KPhase),NDim(KPhase))
          ZMag(ns,1,KPhase)=ZMagKer(i,NIrr)
          IswSymm(ns,1,KPhase)=1
        endif
      enddo
      NSymm(KPhase)=ns
      call RACompleteSymmPlusMagInv(ns,ich)
      if(NDimI(KPhase).eq.1.and..not.QPul) then
        icnt=CrlCentroSymm()
        if(icnt.gt.0) then
          xp=0.
          xp(4)=-s6(4,icnt,1,KPhase)*.5
          do i=1,NSymm(KPhase)
            call MultM(rm6(1,i,1,KPhase),xp,xpp,
     1                 NDim(KPhase),NDim(KPhase),1)
            do j=1,NDim(KPhase)
              s6(j,i,1,KPhase)=s6(j,i,1,KPhase)+xpp(j)-xp(j)
            enddo
            call od0do1(s6(1,i,1,KPhase),s6(1,i,1,KPhase),NDim(KPhase))
          enddo
        endif
      endif
      call SetRealArrayTo(ShSgKer(1,NIrr),6,0.)
      qu(1:3,1,1,KPhase)=QMagIrr(1:3,1,KPhase)
      call FindSmbSgTr(Veta,Shift,GrpKer(NIrr),NGrpKer(NIrr),
     1                 TrMatKer(1,1,NIrr),ShSgKer(1,NIrr),i,CrSystemNew)
      qu(1:3,1,1,KPhase)=QMag(1:3,1,KPhase)
      if(NDimI(KPhase).eq.1.and.QPul) then
        CellParP(1:6)=CellPar(1:6,1,KPhase)
        call UnitMat(TrMatP,NDim(KPhase))
        call DRMatTrCell(RCommen(1,1,KPhase),CellPar(1,1,KPhase),TrMatP)
        call EM50ShowSuperSG(1,t80,Shift,Veta,TrMatP,ShSgKer(1,NIrr))
        k=0
        call Kus(Veta,k,GrpKer(NIrr))
        call MatFromBlock3(TrMatP,TrMatKer(1,1,NIrr),4)
        CellPar(1:6,1,KPhase)=CellParP(1:6)
      endif
      CrSystem(KPhase)=CrSystemNew
      do i=1,NSymmS
        do j=1,NSymmN(KPhase)
          if(EqRV(rm6(1,j,1,KPhase),rm6r(1,i),NDimQ(KPhase),.001).and.
     1       (NDimI(KPhase).eq.0.or.ZMag(j,1,KPhase).gt..001)) then
            BratSymmKer(i,NIrr)=.true.
            call CopyVek(s6(1,j,1,KPhase),s6Ker(1,i,NIrr),
     1                   NDim(KPhase))
            ZMagKer(i,NIrr)=ZMag(j,1,KPhase)
            exit
          endif
        enddo
      enddo
      return
      end
      subroutine RAGenEpiKer(NIrr)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      complex Delta,poma,pbc(12),Block,xc(6),argc
      complex, allocatable :: EqA(:,:,:,:),pa(:,:),pb(:)
      integer, allocatable :: NSel(:),NEqA(:,:)
      logical Pridal,EqRV
      character*80 Veta,t80
      real :: pc(6)=(/0.,0.,0.,0.,0.,0./),xp(36),xm(36)
      real, allocatable :: X4Add(:,:),ZMagAdd(:,:),vt6p(:,:)
      integer MGen(3),KGenP(3)
      nd=NDimIrrep(NIrr)
      if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
        ndp=nd/2
      else
        ndp=nd
      endif
      allocate(NSel(NSymmS),NEqA(nd,NSymmS),X4Add(nd,NSymmS),
     1         ZMagAdd(nd,NSymmS),EqA(2*nd,nd,nd,NSymmS),
     2         pa(2*TriN,TriN),pb(2*TriN),vt6p(6,NLattVec(KPhase)))
      X4Add=0.
      ZMagAdd=1.
      EqA=(0.,0.)
      pbc=(0.,0.)
      NEqA=0
      do i=1,NSymmS
        NSel(i)=0
        if(KSymmLG(i).gt.0) then
          k=1
        else
          do k=1,ndp
            if(abs(EigenValIrrep(k,i,NIrr)-(1.,0.)).lt..001) go to 1200
          enddo
          cycle
        endif
1200    NSel(i)=NSel(i)+1
        Delta=EigenValIrrep(k,i,NIrr)
        if(NDimI(KPhase).le.0) then
          if(abs(Delta-(1.,0.)).gt..0001.and.
     1       abs(Delta-(1.,0.)).lt..01) then
            Delta=1.
          else if(abs(Delta+(1.,0.)).gt..0001.and.
     1            abs(Delta+(1.,0.)).lt..01) then
            Delta=-1.
          else if(abs(Delta).gt..0001.and.
     1            abs(Delta).lt..01) then
            Delta=0.
          endif
          if(abs(Delta-(1.,0.)).lt..001) then
            do j=1,NSel(i)-1
              if(abs(ZMagAdd(j,i)-1.).lt..001) then
                NSel(i)=NSel(i)-1
                go to 1300
              endif
            enddo
            ZMagAdd(NSel(i),i)= 1.
          else if(abs(Delta+(1.,0.)).lt..001) then
            do j=1,NSel(i)-1
              if(abs(ZMagAdd(j,i)+1.).lt..001) then
                NSel(i)=NSel(i)-1
                go to 1300
              endif
            enddo
            ZMagAdd(NSel(i),i)=-1.
          else
            NSel(i)=NSel(i)-1
            go to 1300
          endif
        else
          call qbyx(s6r(1,i),xp,1)
          xp(1)=atan2(aimag(Delta),real(Delta))/(2.*pi)-xp(1)
          call od0do1(xp,xp,1)
          xp(1)=anint(xp(1)*24.)/24.
          X4Add(NSel(i),i)=xp(1)
        endif
        do jj=1,nd
          do ii=1,nd
            EqA(ii,jj,NSel(i),i)=RIrrep(ii+(jj-1)*nd,i,NIrr)
            if(ii.eq.jj) then
              if(ii.le.ndp) then
                poma=Delta
              else
                poma=conjg(Delta)
              endif
              EqA(ii,jj,NSel(i),i)=EqA(ii,jj,NSel(i),i)-poma
            endif
          enddo
        enddo
        NEqA(NSel(i),i)=nd
1300    if(KSymmLG(i).gt.0) then
          if(k.lt.ndp) then
            k=k+1
            go to 1200
          endif
        endif
      enddo
      if(NDimI(KPhase).eq.1) then
        do i=1,NSymmS
          if(KSymmLG(i).gt.0) cycle
          NSel(i)=1
        enddo
      endif
      call SetIgnoreWTo(.true.)
      call SetIgnoreETo(.true.)
      do 3000ISub=NSub,1,-1
        ips=IPorSub(ISub)
        if(TakeSub(ips).ne.1) cycle
        do i=1,NSymmS
          if(BratSub(i,ips).le.0.and.BratSymmKer(i,NIrr)) go to 3000
        enddo
        MGenAll=1
        do i=1,NGenSub(ips)
          k=GenSub(i,ips)
          if(BratSymmKer(k,NIrr)) then
            MGen(i)=1
          else
            MGen(i)=NSel(k)
          endif
          MGenAll=MGenAll*MGen(i)
        enddo
        do 2000IGenP=1,MGenAll
          if(NEpi.ge.NEpiMax) then
            allocate(KIrrepEpiO(NEpi),BratSymmEpiO(NSymmS,NEpi),
     1               GrpEpiO(NEpi),NGrpEpiO(NEpi),
     2               TrMatEpiO(NDim(KPhase),NDim(KPhase),NEpi),
     3               ShSgEpiO(NDim(KPhase),NEpi),
     4               ZMagEpiO(NSymmS,NEpi),s6EpiO(6,NSymmS,NEpi),
     5               NSymmEpiO(NEpi),TakeEpiO(NEpi),
     6               NEqEpiO(NEpi),EqEpiO(72,NEpi),
     7               NEqAEpiO(NEpi),EqAEpiO(18,NEpi),
     8               NEqPEpiO(NEpi),EqPEpiO(18,NEpi),EqPPEpiO(6,NEpi),
     9               NCondEpiO(NEpi),CondEpiO(TriN,TriN,NEpi),
     a               NCondEpiAllO(NEpi),CondEpiAllO(TriN,TriN,NEpi))
            do i=1,NEpi
              nd=NDimIrrep(KIrrepEpi(i))
              if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
                ndp=nd/2
              else
                ndp=nd
              endif
              GrpEpiO(i)=GrpEpi(i)
              NGrpEpiO(i)=NGrpEpi(i)
              NEqEpiO(i)=NEqEpi(i)
              NEqAEpiO(i)=NEqAEpi(i)
              NEqPEpiO(i)=NEqPEpi(i)
              NSymmEpiO(i)=NSymmEpi(i)
              KIrrepEpiO(i)=KIrrepEpi(i)
              TakeEpiO(i)=TakeEpi(i)
              NCondEpiO(i)=NCondEpi(i)
              NCondEpiAllO(i)=NCondEpiAll(i)
              call CopyVek(TrMatEpi(1,1,i),TrMatEpiO(1,1,i),
     1                     NDimQ(KPhase))
              call CopyVek(ShSgEpi(1,i),ShSgEpiO(1,i),NDim(KPhase))
              do j=1,NSymmS
                BratSymmEpiO(j,i)=BratSymmEpi(j,i)
                ZMagEpiO(j,i)=ZMagEpi(j,i)
                call CopyVek(s6Epi(1,j,i),s6EpiO(1,j,i),NDim(KPhase))
              enddo
              EqEpiO(1:72,i)=(0.,0.)
              do ii=1,NEqEpi(i)
                do jj=1,nd
                  kk=ii+2*(jj-1)*nd
                  EqEpiO(kk,i)=EqEpi(kk,i)
                enddo
              enddo
              EqAEpiO(1:18,i)=(0.,0.)
              do ii=1,NEqAEpi(i)
                do jj=1,ndp
                  kk=ii+2*(jj-1)*ndp
                  EqAEpiO(kk,i)=EqAEpi(kk,i)
                enddo
              enddo
              EqPEpiO(1:18,i)=(0.,0.)
              do ii=1,NEqPEpi(i)
                do jj=1,ndp
                  kk=ii+2*(jj-1)*ndp
                  EqPEpiO(kk,i)=EqPEpi(kk,i)
                enddo
                EqPPEpiO(ii,i)=EqPPEpi(ii,i)
              enddo
              call CopyVekC(CondEpi,CondEpiO,TriN**2*NEpi)
              call CopyVekC(CondEpiAll,CondEpiAllO,TriN**2*NEpi)
            enddo
            if(allocated(KIrrepEpi))
     1        deallocate(KIrrepEpi,BratSymmEpi,GrpEpi,NGrpEpi,TrMatEpi,
     2                   ShSgEpi,ZMagEpi,s6Epi,NSymmEpi,TakeEpi,
     3                   NEqEpi,EqEpi,NEqAEpi,EqAEpi,
     4                   NEqPEpi,EqPEpi,EqPPEpi,
     5                   NCondEpi,CondEpi,NCondEpiAll,CondEpiAll)
            nn=2*NEpiMax
            allocate(KIrrepEpi(nn),BratSymmEpi(NSymmS,nn),GrpEpi(nn),
     1               NGrpEpi(nn),
     2               TrMatEpi(NDim(KPhase),NDim(KPhase),nn),
     3               ShSgEpi(NDim(KPhase),nn),ZMagEpi(NSymmS,nn),
     4               s6Epi(6,NSymmS,nn),NSymmEpi(nn),TakeEpi(nn),
     5               NEqEpi(nn),EqEpi(72,nn),
     6               NEqAEpi(nn),EqAEpi(18,nn),
     7               NEqPEpi(nn),EqPEpi(18,nn),EqPPEpi(6,nn),
     8               NCondEpi(nn),CondEpi(TriN,TriN,nn),
     9               NCondEpiAll(nn),CondEpiAll(TriN,TriN,nn))
            NEpiMax=nn
            do i=1,NEpi
              nd=NDimIrrep(KIrrepEpiO(i))
              if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
                ndp=nd/2
              else
                ndp=nd
              endif
              GrpEpi(i)=GrpEpiO(i)
              NGrpEpi(i)=NGrpEpiO(i)
              KIrrepEpi(i)=KIrrepEpiO(i)
              NEqEpi(i)=NEqEpiO(i)
              NEqAEpi(i)=NEqAEpiO(i)
              NEqPEpi(i)=NEqPEpiO(i)
              NSymmEpi(i)=NSymmEpiO(i)
              TakeEpi(i)=TakeEpiO(i)
              NCondEpi(i)=NCondEpiO(i)
              NCondEpiAll(i)=NCondEpiAllO(i)
              call CopyVek(TrMatEpiO(1,1,i),TrMatEpi(1,1,i),
     1                     NDimQ(KPhase))
              call CopyVek(ShSgEpiO(1,i),ShSgEpi(1,i),NDim(KPhase))
              do j=1,NSymmS
                BratSymmEpi(j,i)=BratSymmEpiO(j,i)
                ZMagEpi(j,i)=ZMagEpiO(j,i)
                call CopyVek(s6EpiO(1,j,i),s6Epi(1,j,i),NDim(KPhase))
              enddo
              EqEpi(1:72,i)=(0.,0.)
              do ii=1,NEqEpiO(i)
                do jj=1,nd
                  kk=ii+2*(jj-1)*nd
                  EqEpi(kk,i)=EqEpiO(kk,i)
                enddo
              enddo
              EqAEpi(1:18,i)=(0.,0.)
              do ii=1,NEqAEpi(i)
                do jj=1,ndp
                  kk=ii+2*(jj-1)*ndp
                  EqAEpi(kk,i)=EqAEpiO(kk,i)
                enddo
              enddo
              EqPEpi(1:18,i)=(0.,0.)
              do ii=1,NEqPEpi(i)
                do jj=1,ndp
                  kk=ii+2*(jj-1)*ndp
                  EqPEpi(kk,i)=EqPEpiO(kk,i)
                enddo
                EqPPEpi(ii,i)=EqPPEpiO(ii,i)
              enddo
            enddo
            call CopyVekC(CondEpiO,CondEpi,TriN**2*NEpi)
            call CopyVekC(CondEpiAllO,CondEpiAll,TriN**2*NEpi)
            deallocate(KIrrepEpiO,BratSymmEpiO,GrpEpiO,NGrpEpiO,
     1                 TrMatEpiO,ShSgEpiO,ZMagEpiO,s6EpiO,NSymmEpiO,
     2                 NEqEpiO,EqEpiO,NEqAEpiO,EqAEpiO,TakeEpiO,
     3                 NEqPEpiO,EqPEpiO,EqPPEpiO,
     4                 NCondEpiO,CondEpiO,NCondEpiAllO,CondEpiAllO)
            nd=NDimIrrep(NIrr)
            if(NDimI(KPhase).eq.1.and.NSymmLG.ne.NSymmS) then
              ndp=nd/2
            else
              ndp=nd
            endif
          endif
          NEpi=NEpi+1
          BratSymmEpi(1:NSymmS,NEpi)=.false.
          ZMagEpi(1:NSymmS,NEpi)=ZMagKer(1:NSymmS,NIrr)
          s6Epi(1:6,1:NSymmS,NEpi)=s6Ker(1:6,1:NSymmS,NIrr)
          KIrrepEpi(NEpi)=NIrr
          TakeEpi(NEpi)=TakeSub(ips)
          NEqEpi(NEpi)=0
          EqEpi(1:72,NEpi)=0.
          BratSymmEpi(1:NSymmS,NEpi)=BratSymmKer(1:NSymmS,NIrr)
          m=IGenP-1
          do i=NGenSub(ips),1,-1
            KGenP(i)=mod(m,MGen(i))+1
            m=m/MGen(i)
          enddo
          NEqAP=0
          EqEpi(1:2*nd**2,NEpi)=(0.,0.)
          do i=1,NGenSub(ips)
            k=GenSub(i,ips)
            if(.not.BratSymmKer(k,NIrr)) then
              if(NDimI(KPhase).eq.1) then
                s6Epi(4,k,NEpi)=X4Add(KGenP(i),k)
                ZMagEpi(k,NEpi)= 1.
              else
                ZMagEpi(k,NEpi)=ZMagAdd(KGenP(i),k)
              endif
              BratSymmEpi(k,NEpi)=.true.
              do ii=1,NEqA(KGenP(i),k)
                NEqAP=NEqAP+1
                do jj=1,nd
                  kk=NEqAP+2*(jj-1)*nd
                  EqEpi(kk,NEpi)=EqA(ii,jj,KGenP(i),k)
                enddo
              enddo
              call trianglC(EqEpi(1,NEpi),pbc,2*nd,nd,NEqAP)
              if(NEqAP.ge.nd) then
                NEpi=NEpi-1
                go to 2000
              endif
            endif
          enddo
          NEqEpi(NEpi)=NEqAP
          NEqAEpi(NEpi)=0
          EqAEpi(1:18,NEpi)=0.
          NEqPEpi(NEpi)=0
          EqPEpi(1:18,NEpi)=0.
          EqPPEpi(1:6,NEpi)=0.
          if(NDimI(KPhase).eq.1) then
            do i=1,NEqEpi(NEpi)
              xc=(0.,0.)
              poma=(0.,0.)
              do j=1,nd
                if(abs(EqEpi(i+2*(j-1)*nd,NEpi)).gt..001) then
                  poma=EqEpi(i+2*(j-1)*nd,NEpi)
                  jp=j
                  exit
                endif
              enddo
              if(abs(poma).gt..001) then
                do j=1,nd
                  xc(j)=EqEpi(i+2*(j-1)*nd,NEpi)/poma
                enddo
              else
                cycle
              endif
              NEqAEpi(NEpi)=NEqAEpi(NEpi)+1
              do j=1,ndp
                kk=NEqAEpi(NEpi)+2*ndp*(j-1)
                EqAEpi(kk,NEpi)=0.
              enddo
              do j=1,ndp
                kk=NEqAEpi(NEpi)+2*ndp*(j-1)
                EqAEpi(kk,NEpi)=0.
              enddo
              do j=1,nd
                kk=NEqAEpi(NEpi)+2*ndp*mod(j-1,ndp)
                if(j.eq.jp) then
                  EqAEpi(kk,NEpi)=1.
                else if(abs(xc(j)).gt..01) then
                  EqAEpi(kk,NEpi)=EqAEpi(kk,NEpi)-1.
                endif
              enddo
              call triangl(EqAEpi(1,NEpi),pc,2*ndp,ndp,NEqAEpi(NEpi))
              Pridal=.false.
              do j=jp+1,nd
                if(abs(xc(j)).gt..01) then
                  if(.not.Pridal) then
                    NEqPEpi(NEpi)=NEqPEpi(NEpi)+1
                    do k=1,ndp
                      kk=NEqPEpi(NEpi)+2*ndp*(k-1)
                      EqPEpi(kk,NEpi)=0.
                    enddo
                    EqPPEpi(NEqPEpi(NEpi),NEpi)=0.
                    kk=NEqPEpi(NEpi)+2*ndp*mod(jp-1,ndp)
                    if(jp.gt.ndp) then
                      pom=-1.
                    else
                      pom= 1.
                    endif
                    EqPEpi(kk,NEpi)=pom
                    Pridal=.true.
                  endif
                  kk=NEqPEpi(NEpi)+2*ndp*mod(j-1,ndp)
                  if(j.gt.ndp) then
                    pom=-1.
                  else
                    pom= 1.
                  endif
                  EqPEpi(kk,NEpi)=EqPEpi(kk,NEpi)-pom
                  EqPPEpi(NEqPEpi(NEpi),NEpi)=
     1              EqPPEpi(NEqPEpi(NEpi),NEpi)+
     2              atan2(-aimag(xc(j)),-real(xc(j)))/(2.*pi)
                endif
              enddo
              call triangl(EqPEpi(1,NEpi),EqPPEpi(1,NEpi),2*ndp,ndp,
     1                     NEqPEpi(NEpi))
            enddo
            if(ndp.eq.2) then
              if(NEqPEpi(NEpi).eq.2) then
                do j=1,ndp
                  kk=2*ndp*(j-1)+1
                  EqPEpi(kk,NEpi)=EqPEpi(kk,NEpi)-EqPEpi(kk+1,NEpi)
                  EqPEpi(kk+1,NEpi)=0.
                enddo
                EqPPEpi(1,NEpi)=EqPPEpi(1,NEpi)-EqPPEpi(2,NEpi)
                EqPPEpi(2,NEpi)=0.
                call od0do1(EqPPEpi(1,NEpi),EqPPEpi(1,NEpi),1)
                NEqPEpi(NEpi)=1
              else if(NEqPEpi(NEpi).eq.1) then
                pom=EqPEpi(1,NEpi)*EqPEpi(2*ndp+1,NEpi)
                if(pom.gt.-.01) then
                  EqPEpi(1:18,NEpi)=0.
                  EqPPEpi(1:6,NEpi)=0.
                  NEqPEpi(NEpi)=0
                else
                  call od0do1(EqPPEpi(1,NEpi),EqPPEpi(1,NEpi),1)
                endif
              endif
            endif
          endif
          NCondEpi(NEpi)=0
          NCondEpiAll(NEpi)=0
          call SetComplexArrayTo(CondEpiAll(1,1,NEpi),TriN**2,(0.,0.))
          call SetComplexArrayTo(CondEpi(1,1,NEpi),TriN**2,(0.,0.))
          nx=NCondKer(NIrr)
          if(nx.le.0) go to 1400
          call SetComplexArrayTo(pa,2*TriN**2,(0.,0.))
          call SetComplexArrayTo(pb,2*TriN,(0.,0.))
          do i=1,nx
            do j=1,TriN
              pa(i,j)=CondKer(i,j,NIrr)
c              CondEpiAll(i,j,NEpi)=CondKer(i,j,NIrr)
            enddo
          enddo
          do is=2,NSymmS
            if(.not.BratSymmEpi(is,NEpi).or.BratSymmKer(is,NIrr)) cycle
            call MatInv(rmr(1,is),xp,det,3)
            if(NDimI(KPhase).gt.0) then
              if(KSymmLG(is).eq.0) then
                eps=-1.
              else
                eps= 1.
              endif
              pom=det
              argc=exp(-cmplx(0.,Pi2*s6Epi(4,is,NEpi)))
            else
              eps=-1.
              pom=det*ZMagEpi(is,NEpi)
              argc=(1.,0.)
            endif
            if(pom.lt.0.) call RealMatrixToOpposite(xp,xp,3)
            do js=2,NSymmS
              if(.not.BratSymmEpi(js,NEpi)) cycle
              ks=PerTab(is,js)
              jp=(NSymmS+1-js)*NSymmBlock+1
              if(NDimI(KPhase).gt.0) jp=jp-3
              do j=1,3
                do i=1,3
                  if(i.eq.j) then
                    pa(nx+4-i,jp-j)=(1.,0.)
                  else
                    pa(nx+4-i,jp-j)=(0.,0.)
                  endif
                enddo
              enddo
              k=0
              kp=(NSymmS+1-ks)*NSymmBlock+1
              if(eps.gt.0.) kp=kp-3
              do j=1,3
                do i=1,3
                  k=k+1
                  pa(nx+4-i,kp-j)=-argc*cmplx(xp(k),0.)
                enddo
              enddo
              nx=nx+3
              if(NDimI(KPhase).gt.0) then
                jp=jp+3
                do j=1,3
                  do i=1,3
                    if(i.eq.j) then
                      pa(nx+4-i,jp-j)=(1.,0.)
                    else
                      pa(nx+4-i,jp-j)=(0.,0.)
                    endif
                  enddo
                enddo
                k=0
                kp=(NSymmS+1-ks)*NSymmBlock+1
                if(eps.lt.0.) kp=kp-3
                do j=1,3
                  do i=1,3
                    k=k+1
                    pa(nx+4-i,kp-j)=-conjg(argc)*cmplx(xp(k),0.)
                  enddo
                enddo
                nx=nx+3
              endif
              call TrianglC(pa,pb,2*TriN,TriN,nx)
              if(nx.ge.NSymmBlock*NSymmS) then
                NEpi=NEpi-1
                go to 2000
              endif
            enddo
          enddo
          do i=1,nx
            do j=1,TriN
              CondEpiAll(i,j,NEpi)=pa(i,j)
            enddo
          enddo
          NCondEpiAll(NEpi)=nx
          nn=0
          do i=1,nx
            m=0
            do j=1,TriN,NSymmBlock
              do k=j,j+NSymmBlock-1
                if(abs(pa(i,k)).gt..01) then
                  m=m+1
                  exit
                endif
              enddo
            enddo
            if(m.gt.2) then
              nn=nn+1
              do j=1,TriN
                CondEpi(nn,j,NEpi)=pa(i,j)
              enddo
            endif
          enddo
          NCondEpi(NEpi)=nn
1400      iz=0
1450      ns=0
          do i=1,NSymmS
            if(BratSymmEpi(i,NEpi)) then
              ns=ns+1
              call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
              call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
              call CopyVek(s6Epi(1,i,NEpi),s6(1,ns,1,KPhase),
     1                     NDim(KPhase))
              zmag(ns,1,KPhase)=ZMagEpi(i,NEpi)
            endif
          enddo
          NSymm(KPhase)=ns
          call RACompleteSymmPlusMagInv(ns,ich)
          if(ich.ne.0) then
            NEpi=NEpi-1
            go to 2000
          endif
          call CrlOrderMagSymmetry
          if(NDimI(KPhase).eq.1.and.QPul.and.iz.eq.0) then
            ns=NSymm(KPhase)
            vt6p(1:NDim(KPhase),1:NLattVec(KPhase))=
     1        vt6(1:NDim(KPhase),1:NLattVec(KPhase),1,KPhase)
            nvt6p=NLattVec(KPhase)
            SwitchedToComm=.false.
            call ComSym(0,0,ichp)
            call SuperSGToSuperCellSG(ich)
            NSymm(KPhase)=ns
            NDim(KPhase)=4
            NDimI(KPhase)=1
            NDimQ(KPhase)=NDim(KPhase)**2
            NLattVec(KPhase)=nvt6p
            vt6(1:NDim(KPhase),1:NLattVec(KPhase),1,KPhase)=
     1        vt6p(1:NDim(KPhase),1:NLattVec(KPhase))
            if(ich.ne.0) then
              NEpi=NEpi-1
              go to 2000
            endif
            iz=1
            go to 1450
          endif
          qu(1:3,1,1,KPhase)=QMagIrr(1:3,1,KPhase)
          call FindSmbSgTr(Veta,xp,GrpEpi(NEpi),NGrpEpi(NEpi),
     1                     TrMatEpi(1,1,NEpi),ShSgEpi(1,NEpi),i,i)
          qu(1:3,1,1,KPhase)=QMag(1:3,1,KPhase)
          NSymmEpi(NEpi)=NSymm(KPhase)
          MaxNSymm=NSymm(KPhase)
          do i=1,NSymmS
            do j=1,NSymmN(KPhase)
              if(EqRV(rmr(1,i),rm(1,j,1,KPhase),9,.01)) then
                BratSymmEpi(i,NEpi)=.true.
                call CopyVek(s6(1,j,1,KPhase),s6Epi(1,i,NEpi),
     1                       NDim(KPhase))
                ZMagEpi(i,NEpi)=ZMag(j,1,KPhase)
              endif
            enddo
          enddo
          do 1500i=1,NEpi-1
            if(KIrrepEpi(i).ne.NIrr) cycle
            if(NDimI(KPhase).eq.1) then
              if(NEqAEpi(i).ne.NEqAEpi(NEpi).or.
     1           NEqPEpi(i).ne.NEqPEpi(NEpi)) cycle
            else
              if(NEqEpi(i).ne.NEqEpi(NEpi)) cycle
            endif
            do j=1,NSymmS
              if(BratSymmEpi(j,NEpi)) then
                if(.not.BratSymmEpi(j,i)) go to 1500
                if(NDimI(KPhase).eq.1) then
                  if(abs(s6Epi(4,j,i)-s6Epi(4,j,NEpi)).gt..01)
     1              go to 1500
                else
                  if(abs(ZMagEpi(j,i)-ZMagEpi(j,NEpi)).gt..01)
     1              go to 1500
                endif
              endif
            enddo
            NEpi=NEpi-1
            go to 2000
1500      continue
2000    continue
3000  continue
      call SetIgnoreWTo(.false.)
      call SetIgnoreETo(.false.)
      if(allocated(NSel)) deallocate(NSel,NEqA,X4Add,ZMagAdd,EqA,pa,pb,
     1                               vt6p)
      return
      end
      subroutine RAOrderBasFun(Fun,io,n,m)
      complex Fun(n,m),FunP(:)
      dimension io(m)
      allocatable FunP
      allocate(FunP(n))
      Kam=0
      do i=1,m-1
        if(io(m).lt.io(i)) then
          Kam=i
          exit
        endif
      enddo
      if(Kam.gt.0) then
        iop=io(m)
        call CopyVekC(Fun(1,m),FunP,n)
        do i=m-1,Kam,-1
          call CopyVekC(Fun(1,i),Fun(1,i+1),n)
          io(i+1)=io(i)
        enddo
        io(Kam)=iop
        call CopyVekC(FunP,Fun(1,Kam),n)
      endif
      if(allocated(FunP)) deallocate(FunP)
      return
      end
      subroutine RAGetBasFun
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      complex, allocatable :: PerMat(:,:,:),Spin(:),SpinT(:),Trace(:),
     1                        pa(:,:),pb(:)
      complex cmp(3,3)
      integer, allocatable :: ioa(:)
      dimension xo(6),xp(6),rmp(9),xpp(6),xt(6),xr(3),xi(3),GammaM(3)
      character*256 Veta
      complex pomc
      logical EqIgCase,EpsMinus
      if(allocated(SymmCodes)) deallocate(SymmCodes,xyz)
      allocate(SymmCodes(NSymmS),xyz(3,NSymmS))
      if(allocated(pa)) deallocate(pa,pb)
      allocate(pa(2*TriN,TriN),pb(2*TriN))
      n=1
      SymmCodes(1)='x,y,z'
      do i=1,3
        xyz(i,1)=sqrt(float(i)*.1)
      enddo
      xp=0.
      xp(1:3)=xyz(1:3,1)
      do 3100i=1,NSymmS
        call Multm(rm6r(1,i),xp,xo,NDim(KPhase),NDim(KPhase),1)
        call AddVek(xo,s6r(1,i),xo,NDim(KPhase))
        call CodeSymm(rm6r(1,i),s6r(1,i),symmc(1,i,1,KPhase),0)
        do j=1,NLattVec(KPhase)
          call AddVek(xo,vt6(1,j,1,KPhase),xpp,NDim(KPhase))
          do 3050k=1,n
            do m=1,3
              pom=xpp(m)-xyz(m,k)
              if(abs(anint(pom)-pom).gt..0001) go to 3050
            enddo
            go to 3100
3050      continue
        enddo
        n=n+1
        call CopyVek(xo,xyz(1,n),3)
        call CodeSymm(rm6r(1,i),s6(1,i,1,KPhase),
     1                symmc(1,i,1,KPhase),0)
        Veta=' '
        do j=1,3
          Veta=Veta(:idel(Veta))//symmc(j,i,1,KPhase)
          if(j.ne.3) Veta=Veta(:idel(Veta))//','
        enddo
        if(NDimI(KPhase).gt.0) then
          do j=1,3
            do k=1,idel(Veta)-1
              if(EqIgCase(Veta(k:k+1),'x1')) Veta(k:k+1)='x '
              if(EqIgCase(Veta(k:k+1),'x2')) Veta(k:k+1)='y '
              if(EqIgCase(Veta(k:k+1),'x3')) Veta(k:k+1)='z '
            enddo
          enddo
          call Zhusti(Veta)
        endif
        SymmCodes(n)=Veta
3100  continue
      if(allocated(PerMat)) deallocate(PerMat,Spin,SpinT,ioa)
      if(allocated(BasFun)) deallocate(BasFun,BasFunOrg,MultIrrep)
      allocate(PerMat(TriN,TriN,NSymmS),Spin(TriN),SpinT(TriN),
     1         BasFun(TriN,MaxBasFun,NIrrep),
     2         BasFunOrg(TriN,MaxBasFun,NIrrep),ioa(MaxBasFun),
     3         MultIrrep(NIrrep))
      if(allocated(Trace)) deallocate(Trace)
      allocate(Trace(NSymmS))
      EpsMinus=.false.
      do is=1,NSymmS
        call matinv(rmr(1,is),rmp,det,3)
        if(NDimI(KPhase).gt.0) then
          if(KSymmLG(is).eq.0) then
            eps=-1.
          else
            eps= 1.
          endif
          if(eps.lt.0.) EpsMinus=.true.
          call MultM(qu(1,1,1,KPhase),rmr(1,is),GammaM,1,3,3)
          do i=1,3
            GammaM(i)=GammaM(i)-eps*qu(i,1,1,KPhase)
          enddo
          GammaMR=ScalMul(GammaM,xyz(1,1))
        endif
        call SetComplexArrayTo(PerMat(1,1,is),TriN**2,(0.,0.))
        do i=1,n
          call SetRealArrayTo(xp,6,0.)
          call CopyVek(xyz(1,i),xp,3)
          call Multm(rm6r(1,is),xp,xo,NDim(KPhase),NDim(KPhase),1)
          call AddVek(xo,s6r(1,is),xo,NDim(KPhase))
          do k=1,NLattVec(KPhase)
            call AddVek(xo,vt6(1,k,1,KPhase),xp,NDim(KPhase))
            call AddVek(vt6(1,k,1,KPhase),s6r(1,is),xt,NDim(KPhase))
            do 3150j=1,n
              do m=1,3
                pom=xyz(m,j)-xp(m)
                if(abs(anint(pom)-pom).gt..0001) go to 3150
              enddo
              go to 3200
3150        continue
          enddo
3200      k=0
          do jj=1,3
            do ii=1,3
              k=k+1
              cmp(ii,jj)=det*rmr(k,is)
            enddo
          enddo
          if(NDimI(KPhase).le.0) then
            jj=(j-1)*3+1
            ii=(i-1)*3+1
            call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,0.,3)
          else
            call qbyx(xt,xpp,1)
            delta=Pi2*xpp(1)
            if(eps.gt.0.) then
              jj=(j-1)*6+1
              ii=(i-1)*6+1
              call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,delta,3)
              jj=jj+3
              ii=ii+3
              delta=-delta
              call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,delta,3)
            else
              jj=(j-1)*6+1
              ii=(i-1)*6+4
              call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,delta,3)
              ii=ii-3
              jj=jj+3
              delta=-delta
              call SetMatBlock(PerMat(1,1,is),TriN,jj,ii,cmp,delta,3)
            endif
          endif
        enddo
        Trace(is)=(0.,0.)
        do i=1,TriN
          Trace(is)=Trace(is)+PerMat(i,i,is)
        enddo
      enddo
      m=0
      j=TriN*MaxBasFun*NIrrep
      call SetComplexArrayTo(BasFun,j,(0.,0.))
      do i=1,NIrrep
        NBasFun(i)=0
        pomc=0.
        do j=1,NSymmS
          pomc=pomc+Trace(j)*TraceIrrep(j,i)
        enddo
        MultIrrep(i)=nint(abs(pomc)/float(NSymmS))
        if(MultIrrep(i).gt.0) then
          m=m+1
          nf=0
          do kk=1,NSymmBlock
            call SetComplexArrayTo(Spin,TriN,(0.,0.))
            if(kk.le.TriN) Spin(kk)=(1.,0.)
            do ii=1,NDimIrrep(i)
              do 3500jj=1,NDimIrrep(i)
                nf=nf+1
                k=ii+(jj-1)*NDimIrrep(i)
                do j=1,NSymmS
                  call MultmC(PerMat(1,1,j),Spin,SpinT,TriN,TriN,1)
                  do l=1,TriN
                    if(abs(SpinT(l)).gt..001) then
c                      pomc=RIrrep(k,j,i)*SpinT(l)
                      pomc=conjg(RIrrep(k,j,i))*SpinT(l)
                    else
                      cycle
                    endif
                    if(NSymmBlock.eq.6) then
                      if(EpsMinus) then
                        if(mod(l-1,6).le.2) then
                          if(ii.le.NDimIrrep(i)/2.or.NDimIrrep(i).eq.1)
     1                      BasFun(l,nf,i)=BasFun(l,nf,i)+pomc
                        else
                          if(ii.gt.NDimIrrep(i)/2)
     1                      BasFun(l,nf,i)=BasFun(l,nf,i)+pomc
                        endif
                      else
                        if(mod(l-1,6).le.2) then
                          BasFun(l,nf,i)=BasFun(l,nf,i)+pomc
                        else
                          BasFun(l,nf,i)=BasFun(l,nf,i)+
     1                                   RIrrep(k,j,i)*SpinT(l)
                        endif
                      endif
                    else
                      BasFun(l,nf,i)=BasFun(l,nf,i)+pomc
                    endif
                  enddo
                enddo
3300            io=0
                do l=1,TriN
                  if(abs(BasFun(l,nf,i)).gt..001) then
                    io=l
                    exit
                  endif
                enddo
                if(io.le.0) then
                  nf=nf-1
                  go to 3500
                endif
                pomc=1./BasFun(io,nf,i)
                do l=io,TriN
                  BasFun(l,nf,i)=BasFun(l,nf,i)*pomc
                enddo
                call CopyVekC(BasFun(1,nf,i),BasFunOrg(1,nf,i),TriN)
                do k=1,nf-1
                  if(ioa(k).eq.io) then
                    do l=1,TriN
                      BasFun(l,nf,i)=BasFun(l,nf,i)-BasFun(l,k,i)
                    enddo
                    go to 3300
                  endif
                enddo
                ioa(nf)=io
                call RAOrderBasFun(BasFun(1,1,i),ioa,TriN,nf)
3500          continue
            enddo
            NBasFun(i)=nf
          enddo
          do j=2,nf
            io=0
            do l=1,TriN
              if(abs(BasFun(l,j,i)).gt..001) then
                io=l
                exit
              endif
            enddo
            do k=j-1,1,-1
              if(abs(BasFun(io,k,i)).gt..001) then
                pomc=-BasFun(io,k,i)/BasFun(io,j,i)
                do l=1,TriN
                  BasFun(l,k,i)=BasFun(l,k,i)+pomc*BasFun(l,j,i)
                enddo
              endif
            enddo
          enddo
        endif
        call RAGetBasFunSymm(i,pa,pb,nx,ich)
        do j=1,nx
          do k=1,TriN
            CondKer(j,k,i)=pa(j,k)
          enddo
        enddo
        NCondKer(i)=nx
      enddo
      if(allocated(PerMat)) deallocate(PerMat,Spin,SpinT,ioa)
      if(allocated(Trace)) deallocate(Trace)
      if(allocated(pa)) deallocate(pa,pb)
      return
      end
      subroutine RAGetBasFunSymm(IrrepSel,pa,pb,nx,ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real xr(9),Shift(6),TrMatP(36),CellParP(6)
      character*80 Veta,VetaP
      complex pa(2*TriN,TriN),pb(2*TriN),rmp(3,3,4),ScDiv(4),ScDivFinal,
     1        argc
      integer CrSystemNew,Znovu
      logical EqIgCase
      logical, allocatable :: BratSymmKerP(:)
      real xp(6),TrP(36),rmq(9)
      real, allocatable :: s6KerP(:,:),ZMagKerP(:)
      ich=0
      Znovu=0
      call RAEqFromBasFun(IrRepSel,TriN,pa,pb,nx)
      call SetComplexArrayTo(CondKer(1,1,IrrepSel),TriN**2,(0.,0.))
1100  if(nx.lt.TriN) then
        if(.not.allocated(BratSymmKerP))
     1    allocate(BratSymmKerP(NSymmS),s6KerP(6,NSymmS),
     2             ZMagKerP(NSymmS))
        BratSymmKerP=.false.
        s6KerP=0.
        do i=1,NSymmS
          s6KerP(1:NDim(KPhase),i)=s6r(1:NDim(KPhase),i)
        enddo
        ZMagKerP(1)=1.
        BratSymmKerP(1)=.true.
        do i=1,nx,NSymmBlock
          io1=0
          io2=0
          do k=1,TriN
            if(abs(pa(i,k)).gt..001) then
              if(io1.le.0) then
                io1=k
              else if(io2.le.0) then
                io2=k-mod(k-1,NSymmBlock)
              else
                call SetRealArrayTo(xp,6,0.)
              endif
            endif
          enddo
          isp=(NSymmBlock*NSymmS-io1)/NSymmBlock+1
          if(io2.ge.TriN-NSymmBlock+1) then
            do j=0,2
              do k=0,2
                rmp(3-j,3-k,1)=-pa(i+j,io2+k)
                if(NDimI(KPhase).eq.0) then
                  if(abs(aimag(pa(i+j,io2+k))).gt..001) go to 1300
                endif
              enddo
            enddo
            call MatInv(rmr(1,isp),xr,det,3)
            call ScalDivMatC(rmp(1,1,1),rmr(1,isp),3,3,ScDiv(1),ich)
            ijk=1
            if(ich.ne.0) go to 1250
            if(NDimI(KPhase).eq.0) then
              if(abs(ScDiv(1)).gt..001) then
                call CopyVek(s6r(1,isp),s6KerP(1,isp),NDim(KPhase))
                ZMagKerP(isp)=real(ScDiv(1))*det
                go to 1260
              else
                ijk=2
                go to 1250
              endif
            else
              if(KSymmLG(isp).eq.0) then
                eps=-1.
              else
                eps= 1.
              endif
              if(eps.gt.0.) ScDivFinal=ScDiv(1)
              do j=0,2
                do k=3,5
                  rmp(3-j,6-k,2)=-pa(i+j,io2+k)
                enddo
              enddo
              call ScalDivMatC(rmp(1,1,2),rmr(1,isp),3,3,ScDiv(2),ich)
              ijk=3
              if(ich.ne.0) go to 1250
              if(eps.lt.0.) ScDivFinal=ScDiv(2)
              do j=3,5
                do k=0,2
                  rmp(6-j,3-k,3)=-pa(i+j,io2+k)
                enddo
              enddo
              call ScalDivMatC(rmp(1,1,3),rmr(1,isp),3,3,ScDiv(3),ich)
              ijk=4
              if(ich.ne.0) go to 1250
              do j=3,5
                do k=3,5
                  rmp(6-j,6-k,4)=-pa(i+j,io2+k)
                enddo
              enddo
              call ScalDivMatC(rmp(1,1,4),rmr(1,isp),3,3,ScDiv(4),ich)
              ijk=5
              if(ich.ne.0) go to 1250
              call CopyVek(s6r(1,isp),s6KerP(1,isp),NDim(KPhase))
              if(abs(ScDivFinal).gt..001) then
                ScDivFinal=ScDivFinal*det
                call qbyx(s6r(1,isp),xp,1)
                pom=atan2(aimag(ScDivFinal),real(ScDivFinal))/(2.*pi)
                if(abs(abs(pom)-.5).lt..001) then
                  ZMagKerP(isp)=1.
                  s6KerP(4,isp)=.5
                else if(abs(pom).lt..001) then
                  ZMagKerP(isp)=1.
                  s6KerP(4,isp)=0.
                else
                  ZMagKerP(isp)=1.
                  s6KerP(4,isp)=pom
                  call od0do1(s6KerP(4,isp),s6KerP(4,isp),1)
                endif
                go to 1260
              else
                ijk=6
                go to 1250
              endif
            endif
1250        write(Cislo,'(2i5)') ijk
            if(VasekTest.eq.1) call FeWinMessage('Je zle',Cislo)
            ZMagKerP(isp)=1.
1260        BratSymmKerP(isp)=.true.
          endif
        enddo
        ns=0
        do i=1,NSymmS
          if(BratSymmKerP(i)) then
            ns=ns+1
            call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
            call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
            call CopyVek(s6KerP(1,i),s6(1,ns,1,KPhase),NDim(KPhase))
            ZMag(ns,1,KPhase)=ZMagKerP(i)
            IswSymm(ns,1,KPhase)=1
          endif
        enddo
        call RACompleteSymmPlusMagInv(ns,ich)
        xp=0.
        qu(1:3,1,1,KPhase)=QMagIrr(1:3,1,KPhase)
        call FindSmbSgTr(Veta,Shift,VetaP,n,TrP,xp,i,CrSystemNew)
        qu(1:3,1,1,KPhase)=QMag(1:3,1,KPhase)
        if(NDimI(KPhase).eq.1.and.QPul) then
          CellParP(1:6)=CellPar(1:6,1,KPhase)
          call UnitMat(TrMatP,NDim(KPhase))
          call DRMatTrCell(RCommen(1,1,KPhase),CellPar(1,1,KPhase),
     1                     TrMatP)
          call EM50ShowSuperSG(1,VetaP,Shift,Veta,TrP,xp)
          k=0
          call Kus(Veta,k,VetaP)
          CellPar(1:6,1,KPhase)=CellParP(1:6)
        endif
        do is=1,NSymmS
          if(BratSymmKerP(is)) then
            if(.not.BratSymmKer(is,IrrepSel)) then
              Veta='Operator ('//
     1             SymmCodes(is)(:idel(SymmCodes(is)))//
     2             ') was approved from basis functions, '
              call FeChybne(-1.,-1.,Veta,
     1                      'but not from order parameter analysis.'//
     2                      'Please contact authors.',SeriousError)
              go to 8000
            endif
          else
            if(BratSymmKer(is,IrrepSel)) then
              if(NDimI(KPhase).eq.0) then
                Veta='Operator ('//
     1               SymmCodes(is)(:idel(SymmCodes(is)))//
     2               ') was approved from order parameter analysis, '
                call FeChybne(-1.,-1.,Veta,
     1                        'but not from basis functions. Please '//
     2                        'contact authors.',SeriousError)
                go to 8000
              endif
              call MatInv(rmr(1,is),rmq,det,3)
              if(NDimI(KPhase).gt.0) then
                if(KSymmLG(is).eq.0) then
                  eps=-1.
                else
                  eps= 1.
                endif
                argc=exp(cmplx(0.,-Pi2*(s6Ker(4,is,IrrepSel))))
                pom=det
              else
                eps=-1.
                pom=det*ZMagKer(is,IrrepSel)
                argc=(1.,0.)
              endif
              if(pom.lt.0.) call RealMatrixToOpposite(rmq,rmq,3)
              do js=1,NSymmS
                if(.not.BratSymmKerP(js)) cycle
                ks=PerTab(is,js)
                jp=(NSymmS+1-js)*NSymmBlock+1
                if(NDimI(KPhase).gt.0) jp=jp-3
                do j=1,3
                  do i=1,3
                    if(i.eq.j) then
                      pa(nx+4-i,jp-j)=(1.,0.)
                    else
                      pa(nx+4-i,jp-j)=(0.,0.)
                    endif
                  enddo
                enddo
                k=0
                kp=(NSymmS+1-ks)*NSymmBlock+1
                if(eps.gt.0.) kp=kp-3
                do j=1,3
                  do i=1,3
                    k=k+1
                    pa(nx+4-i,kp-j)=-argc*cmplx(rmq(k),0.)
                  enddo
                enddo
                nx=nx+3
                if(NDimI(KPhase).gt.0) then
                  jp=jp+3
                  do j=1,3
                    do i=1,3
                      if(i.eq.j) then
                        pa(nx+4-i,jp-j)=(1.,0.)
                      else
                        pa(nx+4-i,jp-j)=(0.,0.)
                      endif
                    enddo
                  enddo
                  k=0
                  kp=(NSymmS+1-ks)*NSymmBlock+1
                  if(eps.lt.0.) kp=kp-3
                  do j=1,3
                    do i=1,3
                      k=k+1
                      pa(nx+4-i,kp-j)=-conjg(argc)*cmplx(rmq(k),0.)
                    enddo
                  enddo
                  nx=nx+3
                endif
                call TrianglC(pa,pb,2*TriN,TriN,nx)
              enddo
              Znovu=Znovu+1
              if(Znovu.le.3) go to 1100
            endif
          endif
        enddo
      endif
8000  if(.not.EqIgCase(VetaP,GrpKer(IrrepSel)).and.VasekTest.ne.0)
     1  call FeWinMessage('Nesouhlas: '//VetaP,GrpKer(IrrepSel))
      go to 9999
1300  ich=1
9999  if(allocated(BratSymmKerP)) deallocate(BratSymmKerP,s6KerP,
     1                                       ZMagKerP)
      return
      end
      subroutine RAEqFromBasFun(NIrr,nn,pa,pb,nx)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xr(3),xi(3),xrp(3),xip(3)
      complex pa(2*nn,nn),pb(2*nn),pomc
      call SetComplexArrayTo(pa,2*TriN**2,(0.,0.))
      call SetComplexArrayTo(pb,2*TriN,(0.,0.))
      do i=1,NBasFun(NIrr)
        j1=0
        do j=1,TriN
          if(abs(BasFun(j,i,NIrr)).gt..001) then
            j1=j
            pomc=BasFun(j,i,NIrr)
            exit
          endif
        enddo
        if(j1.le.0) cycle
        j2=TriN+1-j1
        do j=j1+1,TriN
          if(abs(BasFun(j,i,NIrr)).gt..001) then
            j3=TriN+1-j
            pa(j3,j2)=pa(j3,j2)-BasFun(j,i,NIrr)/pomc
          endif
        enddo
      enddo
      nx=TriN
      do i=1,TriN
        do j=1,TriN
          if(abs(pa(i,j)).gt..001) then
            pa(i,i)=(1.,0.)
            exit
          endif
        enddo
      enddo
      nx=TriN
      call TrianglC(pa,pb,2*nn,nn,nx)
      return
      end
      subroutine RAWriteIrr(SvFile)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) SvFile
      character*256 Veta
      character*80  t80
      character*3 t3
      character*2 t2m
      integer io(3)
      logical ComplexNumber,IntegerNumber
      call NewPg(1)
      call OpenFile(lst,fln(:ifln)//'.rep','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      LstOpened=.true.
      Uloha='List of irreducible representations'
      do i=1,NSymmS
        call CopyMat(rm6r(1,i),rm6(1,i,1,KPhase),NDim(KPhase))
        call CopyMat(rmr (1,i),rm (1,i,1,KPhase),3)
        call CopyVek(s6r(1,i),s6(1,i,1,KPhase),NDim(KPhase))
        zmag(i,1,KPhase)=ZMagR(i)
      enddo
      NSymm(KPhase)=NSymmS
      call inform50
      do i=1,NIrrep
        call NewLn(4)
        write(Veta,'(''  Dimension:'',i2)') NDimIrrep(i)
        Veta='Representation: '//SmbIrrep(i)(:idel(SmbIrrep(i)))//
     1       Veta(:idel(Veta))
        write(lst,FormA)
        write(lst,FormA) Veta(:idel(Veta))
        write(lst,FormA1)('-',j=1,idel(Veta))
        write(lst,FormA)
        ComplexNumber=.false.
        IntegerNumber=.true.
        do j=1,NSymmS
          do k=1,NDimIrrep(i)**2
            if(abs(aimag(RIrrep(k,j,i))).gt..001) ComplexNumber=.true.
            if(abs(anint(real(RIrrep(k,j,i)))-
     1                   real(RIrrep(k,j,i))).gt..001.or.
     2         abs(anint(aimag(RIrrep(k,j,i)))-
     3                   aimag(RIrrep(k,j,i))).gt..001)
     4        IntegerNumber=.false.
          enddo
        enddo
        nd=NDimIrrep(i)
        Veta='Symmetry operator      Symbol'
        if(ComplexNumber) then
          if(IntegerNumber) then
            idl1=9*nd
            idl2=9
          else
            idl1=15*nd
            idl2=15
          endif
        else
          if(IntegerNumber) then
            idl1=3*nd
            idl2=3
          else
            idl1=7*nd
            idl2=7
          endif
        endif
        if(nd.gt.1) idl1=idl1+2
        if(idl1.gt.6) then
          j=28+idl1/2+1
          ist1=31
        else
          j=31
          ist1=31+(1-idl1/2)
        endif
        ist2=ist1+max(idl1,6)+1
        Veta(j:)='Matrix'
        if(idl2.gt.9) then
          j=ist2-5+idl2/2
        else
          j=ist2
          ist2=ist2+max(3-idl2/2,0)
        endif
        Veta(j:)='Character'
        call NewLn(3)
        write(lst,FormA)
        write(lst,FormA) Veta(:idel(Veta))
        write(lst,FormA)
        do j=1,NSymmS
          call SmbOp(rm6r(1,j),s6r(1,j),1,1.,t3,t2m,io,nn,det)
          Veta=StSymmCard(j)
          Veta(26:)=t3
          call NewLn(nd+1)
          do k=1,nd
            if(ComplexNumber) then
              if(IntegerNumber) then
                write(t80,101)(nint(real(RIrrep(k+(l-1)*nd,j,i))),
     1                         nint(aimag(RIrrep(k+(l-1)*nd,j,i))),
     2                         l=1,nd)
              else
                write(t80,103)(real(RIrrep(k+(l-1)*nd,j,i)),
     1                         aimag(RIrrep(k+(l-1)*nd,j,i)),
     2                         l=1,nd)
              endif
              idl=idel(t80)
              if(t80(idl:idl).eq.'(') t80(idl:idl)=' '
            else
              if(IntegerNumber) then
                write(t80,102)
     1            (nint(real(RIrrep(k+(l-1)*nd,j,i))),l=1,nd)
              else
                write(t80,104)
     1            (real(RIrrep(k+(l-1)*nd,j,i)),l=1,nd)
              endif
            endif
            if(nd.gt.1) t80='|'//t80(:idel(t80))//'|'
            Veta(ist1:)=t80
            if(ComplexNumber) then
              if(IntegerNumber) then
                write(t80,101) nint(real(TraceIrrep(j,i))),
     1                         nint(aimag(TraceIrrep(j,i)))
              else
                write(t80,103) real(TraceIrrep(j,i)),
     1                         aimag(TraceIrrep(j,i))
              endif
              idl=idel(t80)
              if(t80(idl:idl).eq.'(') t80(idl:idl)=' '
            else
              if(IntegerNumber) then
                write(t80,102) nint(real(TraceIrrep(j,i)))
              else
                write(t80,104) real(TraceIrrep(j,i))
              endif
            endif
            Veta(ist2:)=t80
            write(lst,FormA) Veta(:idel(Veta))
            Veta=' '
          enddo
          write(lst,FormA)
        enddo


c        jp=1
c        do ifun=1,(NBasFun(i)-1)/6+1
c          jk=min(jp+5,NBasFun(i))
c          write(lst,FormA)
c          write(lst,'(''Basic function #:'',6(i10,8x))')
c     1               (j,j=jp,jk)
c          write(lst,FormA)
c          do k=1,TriN
c            write(Veta,'(''M'',a1,''#s'',i3)')
c     1        SmbX(mod(k-1,3)+1),(k-1)/NSymmBlock+1
c            call Zhusti(Veta)
c            if(NDimI(KPhase).le.0) then
c              idl=20
c            else
c              if(mod(k-1,NSymmBlock)/3+1.eq.1) then
c                Cislo='+'
c              else
c                Cislo='-'
c              endif
c              Veta=Veta(:idel(Veta))//'*exp('//Cislo(1:1)//'2pi.i.q*r)'
c              idl=25
c            endif
c            do j=jp,jk
c              write(Veta(idl:),103) real(BasFun(k,j,i)),
c     1                              aimag(BasFun(k,j,i))
c              ii=idel(Veta)
c              if(Veta(ii:ii).eq.'(') Veta(ii:ii)=' '
c              idl=idl+18
c            enddo
c            write(lst,FormA) Veta(:idel(Veta))
c          enddo
c          jp=jp+6
c        enddo
      enddo
      call CloseListing
      HCFileName=SvFile
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                 SvFile)
      call FeListView(fln(:ifln)//'.rep',0)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinGrWin,YMaxBasWin,
     1                 SvFile,0)
      call DeleteFile(SvFile)
      HCFileName=' '
9999  return
101   format(6('(',i3,',',i3,')'))
102   format(6i3)
103   format(6('(',f6.3,',',f6.3,')'))
104   format(6f7.3)
      end
      subroutine RAKarep(ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*3 t3
      character*2 t2m
      complex U(36),UPom(36),CPhi,Chi(:,:,:),Block1(36),Block2(36),
     1        exsc,PhGenLG(:)
      dimension xp(3),io(3),rmRep(9,3),sRep(9,3),rmRepInv(9,3),
     1          sRepInv(9,3),NRep(2),rmp(9),sp(3),rmt(9),st(3),
     2          rmq(9),rmqi(9),sq(3),sqi(3)
      dimension rmLG(:,:),sLG(:,:),rmGenLG(:,:),sGenLG(:,:)
      integer   SelLG(:),SelGenLG(:),DRep(12,2),DRepQ,DRepN,
     1          con(3),CrlCentroSymm,TriNPom
      logical   LogOrderLG(:,:),EqRV,REqR,BratRep(12),SkipNext
      allocatable rmLG,sLG,SelLG,SelGenLG,LogOrderLG,rmGenLG,sGenLG,
     1            Chi,PhGenLG
      ich=0
      n=NSymmN(KPhase)
      allocate(rmLG(9,n),sLG(3,n),rmGenLG(9,n),sGenLG(3,n),PhGenLG(n),
     1         SelLG(n),SelGenLG(n),LogOrderLG(5,n),
     2         Chi(12,n,2),Rep(36,12,n,2))
      call SetComplexArrayTo(Rep,36*12*2*n,(0.,0.))
      if(NDimI(KPhase).gt.1) then
        call FeChybne(-1.,-1.,'the representative analysis for more '//
     1                'than one modulation vector',
     2                'has not yest been implemented.',WarningWithESC)
        go to 9900
      endif
      NSymmLG=0
      InvCenter=0
      NFirst=0
      do i=1,NSymmN(KPhase)
        if(NDimI(KPhase).eq.1) then
          call Multm(qu(1,1,1,KPhase),rm(1,i,1,KPhase),xp,1,3,3)
          do j=1,3
            xp(j)=xp(j)-qu(j,1,1,KPhase)
          enddo
          do j=1,NLattVec(KPhase)
            call AddVek(xp,vt6(1,j,1,KPhase),sp,3)
            do k=1,3
              if(abs(sp(k)-anint(sp(k))).gt..001) go to 1050
            enddo
            go to 1100
1050      enddo
          if(NFirst.le.0.or.i.eq.CrlCentroSymm()) NFirst=i
          cycle
        endif
1100    NSymmLG=NSymmLG+1
        call CopyMat(rm(1,i,1,KPhase),rmLG(1,NSymmLG),3)
        call CopyVek(s6(1,i,1,KPhase),sLG(1,NSymmLG),3)
        SelLG(NSymmLG)=i
        call SetLogicalArrayTo(LogOrderLG(1,NSymmLG),5,.true.)
        call SmbOp(rm6r(1,i),s6r(1,i),1,1.,t3,t2m,io,nn,det)
        if(t3.eq.'-1') InvCenter=NSymmLG
        l=0
        do j=1,3
          do k=1,3
            l=l+1
            if(j.ne.k.and.abs(rmLG(l,NSymmLG)).gt..01) then
              LogOrderLG(1,NSymmLG)=.false.
              go to 1150
            endif
          enddo
        enddo
1150    LogOrderLG(2,NSymmLG)=nn.eq.3
        LogOrderLG(3,NSymmLG)=nn.eq.2.and.det.gt.0.
        LogOrderLG(5,NSymmLG)=det.gt.0.
      enddo
      if(NFirst.gt.0) then
        call CopyMat(rm(1,NFirst,1,KPhase),rmq,3)
        call CopyVek(s6(1,NFirst,1,KPhase),sq,3)
        call MatInv(rmq,rmqi,det,3)
        call MultM(rmqi,sq,sqi,3,3,1)
        call RealVectorToOpposite(sqi,sqi,3)
      endif
      call UnitMat(rmGenLG,3)
      call SetRealArrayTo(sGenLG,3,0.)
      if(allocated(IGenLG)) deallocate(IGenLG)
      allocate(IGenLG(0:NSymmLG))
      n=1
      NGenLG=0
      IGenLG(0)=1
      do i=1,4
        do j=2,NSymmLG
          if(LogOrderLG(i,j).and.LogOrderLG(4,j).and.LogOrderLG(5,j))
     1      then
            np=n+1
            call RAAddGen(rmLG,sLG,j,rmGenLG,sGenLG,n)
            NGenLG=NGenLG+1
            IGenLG(NGenLG)=np
            do k=np,n
              do l=2,NSymmLG
                LogOrderLG(4,l)=LogOrderLG(4,l).and.
     1            .not.EqRV(rmGenLG(1,k),rmLG(1,l),9,.01)
              enddo
            enddo
            LogOrderLG(4,j)=.false.
          endif
        enddo
      enddo
      if(n.lt.NSymmLG) then
        k=InvCenter
        if(k.eq.0) then
          do i=1,4
            do j=2,NSymmLG
              if(LogOrderLG(i,j).and.LogOrderLG(4,j)) then
                k=j
                go to 1300
              endif
            enddo
          enddo
        endif
1300    if(k.gt.0) then
          np=n+1
          call RAAddGen(rmLG,sLG,k,rmGenLG,sGenLG,n)
          NGenLG=NGenLG+1
          IGenLG(NGenLG)=np
        endif
      endif
      do i=1,n
        call RAGetSymmAndPhase(rmGenLG(1,i),sGenLG(1,i),SelGenLG(i),Phi)
        PhGenLG(i)=cmplx(cos(Phi),sin(Phi))
      enddo
      NRep(1)=1
      DRep(1,1)=1
      Rep(1,1,1,1)=(1.,0)
      Chi(1,1,1)=(1.,0.)
      iz1=1
      iz2=2
      if(allocated(IGenLGConj)) deallocate(IGenLGConj,GenLGConjDiff)
      allocate(IGenLGConj(3,NSymmLG),GenLGConjDiff(3,NSymmLG))
      do n=1,NGenLG
        NRep(iz2)=0
        i=IGenLG(n)
        nb=i-1
        if(n.eq.NGenLG) then
          np=NSymmLG
        else
          np=IGenLG(n+1)-1
        endif
        ind=nint(float(np)/float(nb))
        call SetLogicalArrayTo(BratRep,NRep(iz1),.true.)
        call CopyMat(rmGenLG(1,i),rmRep(1,1),3)
        call CopyVek(sGenLG(1,i),sRep(1,1),3)
        call InvSymmOp(rmRep(1,1),sRep(1,1),
     1                 rmRepInv(1,1),sRepInv(1,1),3)
        if(ind.gt.1) then
          call MultSymmOp(rmRep(1,1),sRep(1,1),rmRep(1,1),sRep(1,1),
     1                    rmRep(1,2),sRep(1,2),3)
          call MultSymmOp(rmRepInv(1,1),sRepInv(1,1),
     1                    rmRepInv(1,1),sRepInv(1,1),
     1                    rmRepInv(1,2),sRepInv(1,2),3)
          if(ind.eq.3) then
             call MultSymmOp(rmRep(1,1),sRep(1,1),rmRep(1,2),sRep(1,2),
     1                       rmRep(1,3),sRep(1,3),3)
             call MultSymmOp(rmRepInv(1,1),sRepInv(1,1),
     1                       rmRepInv(1,2),sRepInv(1,2),
     2                       rmRepInv(1,3),sRepInv(1,3),3)
          endif
        endif
        do j=1,ind
          do k=1,nb
            if(j.eq.1) then
              IGenLGConj(j,k)=k
              GenLGConjDiff(j,k)=(1.,0.)
            else
              call MultSymmOp(rmGenLG(1,k),sGenLG(1,k),
     1                        rmRep(1,j-1),sRep(1,j-1),rmp,sp,
     2                        3)
              call MultSymmOp(rmRepInv(1,j-1),sRepInv(1,j-1),
     1                        rmp,sp,rmt,st,3)
              IGenLGConj(j,k)=0
              do l=1,nb
                if(EqRV(rmt,rmGenLG(1,l),9,.01)) then
                  IGenLGConj(j,k)=l
                  if(NDimI(KPhase).gt.0) then
                    pom=pi2*(ScalMul(qu(1,1,1,KPhase),st)-
     1                       ScalMul(qu(1,1,1,KPhase),sGenLG(1,l)))
                  else
                    pom=0.
                  endif
                  GenLGConjDiff(j,k)=cmplx(cos(pom),sin(pom))
                  exit
                endif
              enddo
              if(IGenLGConj(j,k).le.0) then
                if(VasekTest.eq.1)
     1            call FeWinMessage('Zle maticko zle!!!','V Karep')
              endif
            endif
          enddo
        enddo
        do i=1,NRep(iz1)
          if(.not.BratRep(i)) cycle
          con(1)=i
          do j=2,ind
            do 1500k=i,NRep(iz1)
              if(DRep(i,iz1).eq.DRep(k,iz1)) then
                REqR=.true.
                do l=1,nb
                  kk=IGenLGConj(j,l)
                  exsc=Chi(i,IGenLGConj(j,l),iz1)/
     1                 GenLGConjDiff(j,l)
                  REqR=REqR.and.abs(exsc-Chi(k,l,iz1)).lt..01
                  if(.not.REqR) go to 1500
                enddo
                BratRep(k)=.false.
                con(j)=k
                go to 1550
              endif
1500        continue
          enddo
1550      do j=1,nb
            if(EqRV(rmRep(1,ind),rmGenLG(1,j),9,.01)) then
              nq23=j
              if(NDimI(KPhase).gt.0) then
                ex1=pi2*(ScalMul(qu(1,1,1,KPhase),sRep(1,ind))-
     1                   ScalMul(qu(1,1,1,KPhase),sGenLG(1,j)))
              else
                ex1=0.
              endif
              exit
            endif
          enddo
          if(i.eq.con(2)) then
            if(DRep(i,iz1).eq.1) then
              U(1)=(1.,0.)
              Ph=0.
              ij=1
              ji=1
            else
              call RAUnitRN(n-1,Drep(i,iz1),i,iz1,U,ij,ji,ph,ind)
            endif
            xx=real(Rep((ji-1)*DRep(i,iz1)+ij,i,nq23,iz1))
            yy=aimag(Rep((ji-1)*DRep(i,iz1)+ij,i,nq23,iz1))
            PhaseShift=atan2(yy,xx)-ph+ex1
            Mlt=ind
          else
            Mlt=1
          endif
          do j=1,Mlt
            NRep(iz2)=NRep(iz2)+1
            DRep(NRep(iz2),iz2)=ind*DRep(i,iz1)/Mlt
            DRepN=DRep(NRep(iz2),iz2)
            DRepQ=DRepN**2
            if(i.eq.con(2)) then
              phi=(pi2*float(j-1)+PhaseShift)/float(ind)
              CPhi=cmplx(cos(phi),sin(phi))
              do l=1,DRepQ
                UPom(l)=U(l)*CPhi
              enddo
            else
              call SetComplexArrayTo(UPom,DRepQ,(0.,0.))
              do l=1,ind
                k=mod(l+ind-2,ind)+1
                kk=0
                do ll=1,DRep(i,iz1)
                  kkk=(k-1)*DRepN+(l-1)*DRep(i,iz1)
                  do nn=1,DRep(i,iz1)
                    kk=kk+1
                    kkk=kkk+1
                    if(l.eq.1) then
                      UPom(kkk)=Rep(kk,i,nq23,iz1)*
     1                          cmplx(cos(ex1),sin(ex1))
                    else
                      if(ll.eq.nn) UPom(kkk)=(1.,0.)
                    endif
                  enddo
                enddo
              enddo
            endif
            do k=1,nb
              do jj=1,ind
                if(jj.eq.1) then
                  if(i.eq.con(2)) then
                    call CopyVekC(Rep(1,i,k,iz1),Rep(1,NRep(iz2),k,iz2),
     1                            DRepQ)
                  else
                    call SetComplexArrayTo(Rep(1,NRep(iz2),k,iz2),DRepQ,
     1                                     (0.,0.))
                    do l=1,ind
                      kk=0
                      do ll=1,DRep(i,iz1)
                        kkk=(l-1)*(DRepN+DRep(i,iz1))
                        do nn=1,DRep(i,iz1)
                          kk=kk+1
                          kkk=kkk+1
                          Rep(kkk,NRep(iz2),k,iz2)=
     1                       Rep(kk,i,IGenLGConj(l,k),iz1)*
     2                       GenLGConjDiff(l,k)
                        enddo
                      enddo
                    enddo
                  endif
                else
                  call MultMC(UPom,
     1                        Rep(1,NRep(iz2),k+(jj-2)*nb,iz2),
     2                        Rep(1,Nrep(iz2),k+(jj-1)*nb,iz2),
     3                        DRepN,DRepN,DRepN)
                endif
              enddo
            enddo
            do k=1,np
              chi(Nrep(iz2),k,iz2)=cmplx(0.,0.)
              do l=1,DRepN**2,DRepN+1
                chi(Nrep(iz2),k,iz2)=chi(Nrep(iz2),k,iz2)+
     1                               Rep(l,Nrep(iz2),k,iz2)
              enddo
            enddo
          enddo
        enddo
        iz1=mod(iz1,2)+1
        iz2=mod(iz2,2)+1
      enddo
      NIrrep=NRep(iz1)
      n=6
      if(allocated(NDimIrrep))
     1  deallocate(NDimIrrep,RIrrep,EigenValIrrep,EigenVecIrrep,
     2             SmbIrrep,TraceIrrep,NBasFun,
     3             BratSymmKer,ZMagKer,s6Ker,GrpKer,NGrpKer,ShSgKer,
     4             TrMatKer,AddSymm,NCondKer,CondKer)
       TriNPom=NSymmBlock*NSymmN(KPhase)
      allocate(NDimIrrep(NIrrep),RIrrep(n**2,NSymmS,NIrrep),
     1         EigenValIrrep(n,NSymmS,NIrrep),
     2         EigenVecIrrep(n**2,NSymmS,NIrrep),SmbIrrep(NIrrep),
     3         TraceIrrep(NSymmS,NIrrep),NBasFun(NIrrep),
     4         BratSymmKer(NSymmS,NIrrep),zmagKer(NSymmS,NIrrep),
     5         s6Ker(6,NSymmS,NIrrep),GrpKer(NIrrep),NGrpKer(NIrrep),
     6         ShSgKer(6,NIrrep),
     7         TrMatKer(NDim(KPhase),NDim(KPhase),NIrrep),
     8         AddSymm(NSymmS),NCondKer(NIrrep),
     9         CondKer(TriNPom,TriNPom,NIrrep))
      n=0
      nd=0
5000  nd=nd+1
      do i=1,NIrrep
        if(DRep(i,iz1).ne.nd) cycle
        n=n+1
        NDimIrrep(n)=nd
        do j=1,NSymmLG
          js=SelGenLG(j)
          k=0
          do ii=1,NDimIrrep(n)
            do jj=1,NDimIrrep(n)
              k=k+1
              pomr=real(Rep(k,i,j,iz1))
              pomi=aimag(Rep(k,i,j,iz1))
              call Round2Closest(pomr)
              call Round2Closest(pomi)
              RIrrep(k,js,n)=cmplx(pomr,pomi)/PhGenLG(j)
            enddo
          enddo
        enddo
      enddo
      if(n.lt.NIrrep.and.nd.le.3) go to 5000
      if(VasekTest.gt.0) call RAKontrola(0,SelGenLG)
      if(NDimI(KPhase).le.0) go to 9999
      allocate(RIrrepP(36,NSymmN(KPhase),NIrrep))
      if(NSymmLG.eq.NSymmN(KPhase)) then
        call RACheckRealIrr(NIrrep,NSymmN(KPhase),ubound(RIrrep,1),
     1                      NDimIrrep,KSymmLG,RIrrep,0,ich)
        if(ich.ne.0) go to 9300
        go to 9999
      else
        do i=1,NIrrep
          n=NDimIrrep(i)
          if(n.ne.2) cycle
          do j=1,NSymmN(KPhase)
            if(KSymmLG(j).gt.0) then
              if(abs(RIrrep(1,j,i)).gt..01.or.
     1           abs(RIrrep(4,j,i)).gt..01) cycle
              alpha=atan2(aimag(RIrrep(3,j,i)),
     1                     real(RIrrep(3,j,i)))
              beta=atan2(aimag(RIrrep(2,j,i)),
     1                    real(RIrrep(2,j,i)))
              delta=(beta-alpha)*.25
              U(1)=exp(cmplx(0., delta))
              U(2)=(0.,0.)
              U(3)=(0.,0.)
              U(4)=exp(cmplx(0.,-delta))
              Upom(1)=exp(cmplx(0.,-delta))
              Upom(2)=(0.,0.)
              Upom(3)=(0.,0.)
              Upom(4)=exp(cmplx(0.,delta))
              do k=1,NSymmN(KPhase)
                if(KSymmLG(k).gt.0) then
                  call MultMC(U,RIrrep(1,k,i),Block1,2,2,2)
                  call MultMC(Block1,Upom,RIrrep(1,k,i),2,2,2)
                endif
              enddo
              exit
            endif
          enddo
        enddo
        do i=1,NIrrep
          n=NDimIrrep(i)
          do j=1,NSymmN(KPhase)
            if(KSymmLG(j).gt.0)
     1        call CopyMatC(RIrrep(1,j,i),RIrrepP(1,j,i),n)
          enddo
        enddo
        do i=1,NIrrep
          call RAGenIrrFromLG(i,rmq,sq,rmqi,sqi,KSymmLG,ich)
        enddo
        if(ich.ne.0) go to 9100
        call RACheckRealIrr(NIrrep,NSymmN(KPhase),ubound(RIrrep,1),
     1                      NDimIrrep,KSymmLG,RIrrep,1,ich)
        if(ich.ne.0) go to 9300
        do i=1,NIrrep
          n=NDimIrrep(i)
          do j=1,NSymmN(KPhase)
            if(KSymmLG(j).gt.0) then
              call CopyMatC(RIrrep(1,j,i),RIrrepP(1,j,i),n)
            endif
          enddo
        enddo
        do 5200i=1,NIrrep
          n=NDimIrrep(i)
          kok=0
          do 5100k=1,NSymmN(KPhase)
            if(KSymmLG(k).ne.0) cycle
            call CopyMat(rm(1,k,1,KPhase),rmq,3)
            call CopyVek(s6(1,k,1,KPhase),sq,3)
            call MatInv(rmq,rmqi,det,3)
            call MultM(rmqi,sq,sqi,3,3,1)
            call RealVectorToOpposite(sqi,sqi,3)
            call RAGenIrrFromLG(i,rmq,sq,rmqi,sqi,KSymmLG,ich)
            if(ich.ne.0) go to 5100
            if(kok.eq.0) kok=k
            do j=1,NSymmN(KPhase)
              if(KSymmLG(j).eq.0) then
                i1=2
                j1=1
                i2=1
                j2=2
              else
                i1=1
                j1=1
                i2=2
                j2=2
              endif
              ip=(i1-1)*n+1
              jp=(j1-1)*n+1
              l=0
              do ii=ip,ip+n-1
                l=l+1
                m=0
                do jj=jp,jp+n-1
                  m=m+1
                  Block1(l+(m-1)*n)=RIrrep(ii+2*(jj-1)*n,j,i)
                enddo
              enddo
              l=0
              ip=(i2-1)*n+1
              jp=(j2-1)*n+1
              do ii=ip,ip+n-1
                l=l+1
                m=0
                do jj=jp,jp+n-1
                  m=m+1
                  U(l+(m-1)*n)=RIrrep(ii+2*(jj-1)*n,j,i)
                enddo
              enddo
              call MatConjg(U,Block2,n)
              do ii=1,n**2
                if(abs(Block1(ii)-Block2(ii)).gt..01) go to 5100
              enddo
            enddo
            go to 5200
5100      continue
          if(kok.gt.0) then
            call CopyMat(rm(1,kok,1,KPhase),rmq,3)
            call CopyVek(s6(1,kok,1,KPhase),sq,3)
            call MatInv(rmq,rmqi,det,3)
            call MultM(rmqi,sq,sqi,3,3,1)
            call RealVectorToOpposite(sqi,sqi,3)
            call RAGenIrrFromLG(i,rmq,sq,rmqi,sqi,KSymmLG,ich)
            NDimIrrep(i)=-NDimIrrep(i)
          else
            go to 9200
          endif
          ich=0
5200    continue
      endif
      do i=1,NIrrep
        NDimIrrep(i)=2*NDimIrrep(i)
      enddo
      do 5400i=1,NIrrep
        n=NDimIrrep(i)
        if(n.ge.0) go to 5400
        n=-n
        nq=n**2
        do 5300j=i,NIrrep
          if(NDimIrrep(j).ge.0.or.n.ne.iabs(NDimIrrep(j))) go to 5300
          do k=1,NSymmN(KPhase)
            Block1(1:nq)=RIrrep(1:nq,k,i)
                 U(1:nq)=RIrrep(1:nq,k,j)
            call MatConjg(U,Block2,n)
            do ii=1,n**2
              if(abs(Block1(ii)-Block2(ii)).gt..01) go to 5300
            enddo
          enddo
          do k=1,NSymmN(KPhase)
            Block1(1:nq)= RIrrep(1:nq,k,i)
            Block2(1:nq)= RIrrep(1:nq,k,j)
            RIrrep(1:36,k,i)=(0.,0.)
            ip=1
            jp=1
            call SetMatBlock(RIrrep(1,k,i),2*n,ip,jp,Block1,0.,n)
            ip=(n-1)*n+1
            jp=(n-1)*n+1
            call SetMatBlock(RIrrep(1,k,i),2*n,ip,jp,Block2,0.,n)
          enddo
          NDimIrrep(i)=2*n
          if(i.ne.j) NDimIrrep(j)=0
          go to 5400
5300    continue
        go to 9200
5400  continue
      NIrr=0
      do i=1,NIrrep
        n=NDimIrrep(i)
        if(n.le.0) cycle
        nq=n**2
        NIrr=NIrr+1
        NDimIrrep(NIrr)=n
        if(NIrr.eq.i) cycle
        do k=1,NSymmN(KPhase)
          RIrrep(1:nq,k,NIrr)=RIrrep(1:nq,k,i)
        enddo
      enddo
      NIrrep=NIrr
      do i=1,NSymmN(KPhase)
        SelGenLG(i)=i
      enddo
      call RAKontrola(1,SelGenLG)
      go to 9999
9100  call FeUnforeseenError('Error happened during inducing of '//
     1                       'irreps from the little group.')
      go to 9900
9200  call FeUnforeseenError('Conjugated basis could not be find.')
      go to 9900
9300  call FeUnforeseenError('searching for physically irreducible '//
     1                       'representation faild.')
      go to 9900
9900  ich=1
9999  deallocate(rmLG,sLG,rmGenLG,sGenLG,PhGenLG,SelLG,SelGenLG,
     1           LogOrderLG,Chi,Rep)
      if(allocated(IGenLG)) deallocate(IGenLG)
      if(allocated(IGenLGConj)) deallocate(IGenLGConj,GenLGConjDiff)
      if(allocated(RIrrepP))deallocate(RIrrepP)
      return
101   format(3(' (',i3,',',i3,')'))
103   format(6(' (',f10.6,',',f10.6,')'))
      end
      subroutine RAGetSymmAndPhase(rms,ss,IFound,Phi)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rms(*),ss(*),sp(3),xp(1)
      logical EqRV
      IFound=0
      do i=1,NSymmS
        if(EqRV(rms,rmr(1,i),9,.001)) then
          IFound=i
          if(NDimI(KPhase).gt.0) then
            do j=1,3
              sp(j)=ss(j)-s6r(j,i)
            enddo
            call qbyx(sp,xp,1)
            phi=pi2*xp(1)
          else
            phi=0.
          endif
        endif
      enddo
      return
      end
      subroutine RAGenIrrFromLG(Irr,rmq,sq,rmqi,sqi,SelSG,ich)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension rmq(*),sq(*),rmqi(*),sqi(*),rmp(9),sp(3),rmt(9),st(3)
      real :: XNull(3) = (/0.,0.,0./)
      integer SelSG(*)
      ich=0
      do j=1,NSymmN(KPhase)
        if(SelSG(j).eq.0) then
          i1=2
          j1=1
          call MultSymmOp(rmqi,sqi,rm(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                    rmp,sp,3)
          call RAGetSymmAndPhase(rmp,sp,k1,phi1)
          if(k1.le.0) then
            go to 9100
          else
            if(SelSG(k1).le.0) then
              go to 9100
            endif
          endif
          i2=1
          j2=2
          call MultSymmOp(rm(1,j,1,KPhase),s6(1,j,1,KPhase),rmq,sq,
     1                    rmp,sp,3)
          call RAGetSymmAndPhase(rmp,sp,k2,phi2)
          if(k2.le.0) then
            go to 9100
          else
            if(SelSG(k2).le.0) then
              go to 9100
            endif
          endif
        else
          i1=1
          j1=1
          k1=j
          Phi1=0.
          i2=2
          j2=2
          call MultSymmOp(rmqi,sqi,rm(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                    rmt,st,3)
          call MultSymmOp(rmt,st,rmq,sq,rmp,sp,3)
          call RAGetSymmAndPhase(rmp,sp,k2,phi2)
          if(k2.le.0) then
            go to 9100
          else
            if(SelSG(k2).le.0) go to 9100
          endif
        endif
        n=NDimIrrep(Irr)
        call SetComplexArrayTo(RIrrep(1,j,Irr),4*n**2,(0.,0.))
        ip=(i1-1)*n+1
        jp=(j1-1)*n+1
        call SetMatBlock(RIrrep(1,j,Irr),2*n,ip,jp,RIrrepP(1,k1,Irr),
     1                   Phi1,n)
        ip=(i2-1)*n+1
        jp=(j2-1)*n+1
        call SetMatBlock(RIrrep(1,j,Irr),2*n,ip,jp,RIrrepP(1,k2,Irr),
     1                   Phi2,n)
      enddo
      go to 9999
9100  ich=1
9999  return
      end
      subroutine RAAddGen(rmLG,sLG,n,rmGenLG,sGenLG,m)
      dimension rmLG(9,*),sLG(3,*),rmGenLG(9,*),sGenLG(3,*),rp(9),sp(3)
      logical EqRV
      call CopyMat(rmLG(1,n),rp,3)
      call CopyVek(sLG(1,n),sp,3)
      mold=m
1000  do i=1,m
        if(EqRV(rmGenLG(1,i),rp,9,.0001)) go to 9999
      enddo
      m=m+1
      mp=m
      call CopyMat(rp,rmGenLG(1,mp),3)
      call CopyVek(sp,sGenLG(1,mp),3)
      do i=2,mold
        m=m+1
        call MultSymmOp(rp,sp,rmGenLG(1,i),sGenLG(1,i),
     1                  rmGenLG(1,m),sGenLG(1,m),3)
      enddo
      call MultSymmOp(rmLG(1,n),sLG(1,n),rmGenLG(1,mp),sGenLG(1,mp),
     1                rp,sp,3)
      go to 1000
9999  return
      end
      subroutine RAUnitRN(ngb,DRep,NRep,z,U,ii,jj,ph,ind)
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer DRep,z
      integer r,q(45)
      complex d1(3,3,5),d2(3,3,5),a(:,:),U(DRep,DRep)
      allocatable a
      do k=1,ngb
        m=0
        do i=1,DRep
          do j=1,DRep
            m=m+1
            b=real(Rep(m,NRep,IGenLGConj(2,IGenLG(k)),z))
            p=aimag(Rep(m,NRep,IGenLGConj(2,IGenLG(k)),z))
            if(abs(b).lt.1.e-6) b=0.
            if(abs(p).lt.1.e-6) p=0.
            d1(i,j,k)=cmplx(b,p)*GenLGConjDiff(2,IGenLG(k))
            b=real(Rep(m,NRep,IGenLG(k),z))
            p=aimag(Rep(m,NRep,IGenLG(k),z))
            if(abs(b).lt.1.e-6)  b=0.
            if(abs(p).lt.1.e-6)  p=0.
            d2(i,j,k)=cmplx(b,p)
          enddo
        enddo
      enddo
      mg=DRep*DRep*ngb
      ng=DRep*DRep
      allocate(a(mg,ng))
      call SetComplexArrayTo(a,mg*ng,(0.,0.))
      kng=0
      do k=1,ngb
        nn=0
        do n=1,DRep
          in=n
          ni=nn+1
          do i=1,DRep
            nj=nn+1
            jn=n
            do j=1,DRep
              a(ni+kng,nj)=a(ni+kng,nj)+d1(j,i,k)
              a(in+kng,jn)=a(in+kng,jn)-d2(i,j,k)
              nj=nj+1
              jn=jn+DRep
            enddo
            in=in+DRep
            ni=ni+1
          enddo
          nn=nn+DRep
        enddo
        kng=kng+ng
      enddo
      call Gausz(a,mg,ng,r,q)
      call GauszGetOut(a,mg,ng,r,q,DRep)
      do n=1,ind-1
        do i=1,DRep
          do j=1,DRep
            a(i+n*DRep,j+n*DRep)=0.
            do k=1,DRep
              a(i+n*DRep,j+n*DRep)=a(i+n*DRep,j+n*DRep)+
     1                             a(i,k)*a(k+(n-1)*DRep,j+(n-1)*DRep)
            enddo
          enddo
        enddo
      enddo
      do i=1,DRep
        do j=1,DRep
          if(abs(a(i+(ind-1)*DRep,j+(ind-1)*DRep)).ne.0.) then
            ii=i
            jj=j
            go to 2000
          endif
        enddo
      enddo
2000  xx=real (a(i+(ind-1)*DRep,j+(ind-1)*DRep))
      yy=aimag(a(i+(ind-1)*DRep,j+(ind-1)*DRep))
      ph=atan2(yy,xx)
      do i=1,DRep
        do j=1,DRep
          U(i,j)=A(i,j)
        enddo
      enddo
      deallocate(a)
      return
      end
      subroutine RALstOfDetails(Klic,Irrep,IEpi)
      use Atoms_mod
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*80  StMag(6),Veta,CisloC
      character*20  StPhi
      character*20, allocatable :: StC(:)
      character*5   t5
      character*4   t4
      character*1   t1,cp1,cp2
      complex pomc
      complex, allocatable :: EqC(:,:),EqPPC(:),pa(:,:),pb(:)
      logical Prvni,Brat,MatRealEqUnitMat,Standard,EqIgCase
      integer CrlCentroSymm
      real TrMatP(3,3),TrPom(3,3)
      Uloha='List of details for selected epikernel'
      nd=NDimIrrep(Irrep)
      if(NDimI(KPhase).le.0) then
        ndp=NDimIrrep(Irrep)
      else
        ndp=NDimIrrep(Irrep)/2
      endif
      if(NDimI(KPhase).gt.0) then
        Veta='superspace'
      else
        Veta='space'
      endif
      Veta='ernel magnetic '//Veta(:idel(Veta))//
     1     ' group for the represenation '//
     2     SmbIrrep(Irrep)(:idel(SmbIrrep(Irrep)))
      if(Klic.ge.0) then
        Veta='K'//Veta(:idel(Veta))
      else
        Veta='Epik'//Veta(:idel(Veta))
      endif
      call TitulekVRamecku(Veta)
      if(Klic.lt.0) then
        if(NDimI(KPhase).eq.1) then
          if(nd.eq.2) then
            Veta='S*exp{2*pi*i*phi}'
          else
            if(NEqPEpi(IEpi).gt.0) then
              if(abs(EqPPEpi(1,IEpi)).lt..001) then
                StPhi='phi}]'
              else
                call ToFract(abs(1.-EqPPEpi(1,IEpi)),Cislo,9)
                StPhi='(phi+'//Cislo(:idel(Cislo))//')}]'
              endif
            endif
            if(NEqAEpi(IEpi).eq.ndp) then
              Veta='[0,0]'
            else if(NEqAEpi(IEpi).gt.1) then
              Veta='???'
            else if(NEqAEpi(IEpi).eq.1) then
              if(abs(EqAEpi(1,IEpi)).lt..001) then
                Veta='[S*exp{2*pi*i*phi},0]'
              else if(abs(EqAEpi(1+2*ndp,IEpi)).lt..001) then
                Veta='[0,S*exp{2*pi*i*phi}]'
              else
                if(NEqPEpi(IEpi).gt.0) then
                  Veta='[S*exp{2*pi*i*phi},S*exp{2*pi*i*'//StPhi
                else
                  Veta='[S*exp{2*pi*i*phi1},S*exp{2*pi*i*phi2}]'
                endif
              endif
            else if(NEqAEpi(IEpi).eq.0) then
              if(NEqPEpi(IEpi).gt.0) then
                Veta='[S1*exp{2*pi*i*phi},S2*exp{2*pi*i*'//StPhi
              else
                Veta='[S1*exp{2*pi*i*phi1},S2*exp{2*pi*i*phi2}]'
              endif
            endif
          endif
        else
          allocate(EqC(nd,nd),EqPPC(nd),StC(nd))
          do i=1,nd
            StC(i)=char(ichar('a')-1+i)
          enddo
          if(NEqEpi(IEpi).lt.nd) then
            EqPPC=(0.,0.)
            EqC=(0.,0.)
            NEqC=NEqEpi(IEpi)
            do i=1,NEqC
              do j=1,nd
                EqC(i,nd+1-j)=EqEpi(i+2*(j-1)*nd,IEpi)
              enddo
            enddo
            call trianglC(EqC,EqPPC,nd,nd,NEqC)
            kk=1
            do i=1,NEqC
              jp=0
              do j=1,nd
                if(abs(EqC(i,j)).gt..01) then
                  jp=j
                  go to 1300
                endif
              enddo
              cycle
1300          k=nd+1-jp
              StC(k)=' '
              do j=nd,jp+1,-1
                if(abs(EqC(i,j)).gt..01) then
                  pom=real(EqC(i,j))
                  write(Cislo,'(f15.4)') -pom
                  call ZdrcniCisla(Cislo,1)
                  if(Cislo.eq.'1') then
                    Cislo='+'
                  else if(Cislo.eq.'-1') then
                    Cislo='-'
                  else if(pom.gt.0.) then
                    Cislo='+'//Cislo(:idel(Cislo))
                  endif
                  StC(k)=StC(k)(:idel(StC(k)))//Cislo(:idel(Cislo))//
     1                    char(ichar('a')+nd-j)
                endif
              enddo
              if(StC(k).eq.' ') then
                StC(k)='0'
              else if(StC(k)(1:1).eq.'+') then
                StC(k)=StC(k)(2:)
              endif
            enddo
          else
            StC='0'
          endif
          cp1='a'
          cp2='b'
          do i=1,nd
            if(.not.EqIgCase(StC(i),cp1)) then
              do j=i,nd
                do k=1,idel(StC(j))
                  if(EqIgCase(StC(j)(k:k),cp2)) StC(j)(k:k)=cp1
                enddo
              enddo
            endif
            cp1=char(ichar(cp1)+1)
            cp2=char(ichar(cp2)+1)
          enddo
          Veta='('
          do i=1,nd
            Veta=Veta(:idel(Veta))//StC(i)(:idel(StC(i)))//','
          enddo
          deallocate(EqC,EqPPC,StC)
          idl=idel(Veta)
          Veta(idl:idl)=')'
        endif
        call NewLn(2)
        write(lst,'(''Order parameter: '',a/)') Veta(:idel(Veta))
      endif
      allocate(pa(2*TriN,TriN),pb(2*TriN))
      if(Klic.ge.0) then
        nx=NCondKer(Irrep)
        do i=1,nx
          do j=1,TriN
            pa(i,j)=CondKer(i,j,Irrep)
          enddo
        enddo
      else
        nx=NCondEpiAll(IEpi)
        do i=1,nx
          do j=1,TriN
            pa(i,j)=CondEpiAll(i,j,IEpi)
          enddo
        enddo
      endif
      Veta='a general position'
      Veta='Magnetic moments for '//Veta(:idel(Veta))
      call TitulekVRamecku(Veta)
      if(Klic.eq.0) then
        call NewLn(2)
        write(lst,'(1x,''Number'',7x,''Symmetry code'',13x,
     1              ''Coordinates'',25x,''Magnetic moments''/)')
      else
        call NewLn(2)
        write(lst,'(1x,''Number'',7x,''Symmetry code'',30x,
     1              ''Magnetic moments''/)')
      endif
      l=TriN+1
      m=nx
      io=0
      do i=1,NSymmS
        write(t5,106) i
        call Zhusti(t5)
        call SetStringArrayTo(StMag,NSymmBlock,' ')
        idl=0
        do j=1,NSymmBlock
          l=l-1
          if(m.gt.0) then
            if(io.eq.0) then
              do k=1,TriN
                if(abs(pa(m,k)).gt..001) then
                  io=k
                  exit
                endif
              enddo
            endif
          else
            io=0
          endif
          if(io.eq.l) then
            if(NSymmBlock.eq.3) then
              StMag(j)='M'//smbx(j)//t5(:idel(t5))//'='
            else
              if(j.le.3) then
                jp=j
                t1='p'
              else
                jp=j-3
                t1='m'
              endif
              StMag(j)='M'//smbx(jp)//t5(:idel(t5))//t1//'='
            endif
            Prvni=.true.
            do k=TriN,io+1,-1
              pomc=-pa(m,k)
              if(abs(pomc).gt..001) then
                if(abs(aimag(pomc)).lt..001.or.
     1             abs( real(pomc)).lt..001) then
                  if(abs(aimag(pomc)).lt..001) then
                    write(CisloC,105) real(pomc)
                  else
                    write(CisloC,105) aimag(pomc)
                  endif
                  call ZdrcniCisla(CisloC,1)
                  if(.not.Prvni.and.
     1               (real(pomc).gt..001.or.aimag(pomc).gt..001))
     2              CisloC='+'//CisloC
                  if(abs(abs(pomc)-1.).lt..001) then
                    if(Prvni.and.
     1                 ( real(pomc).gt..001.or.
     2                  aimag(pomc).gt..001)) then
                      CisloC=' '
                    else
                      CisloC(2:)=' '
                    endif
                  endif
                  if(abs(real(pomc)).lt..001)
     1              CisloC=CisloC(:idel(CisloC))//'i'
                else
                  write(CisloC,105) real(pomc)
                  call ZdrcniCisla(CisloC,1)
                  write(Cislo ,105) aimag(pomc)
                  call ZdrcniCisla(Cislo ,1)
                  if(aimag(pomc).gt..001) Cislo='+'//Cislo
                  if(abs(abs(aimag(pomc))-1.).lt..001) then
                    if(aimag(pomc).gt..001) Cislo(2:)=' '
                  endif
                  Cislo=Cislo(:idel(Cislo))//'i'
                  CisloC='('//CisloC(:idel(CisloC))//
     1                   Cislo(:idel(Cislo))//')'
                  if(.not.Prvni) CisloC='+'//CisloC
                endif
                StMag(j)=StMag(j)(:idel(StMag(j)))//
     1                   CisloC(:idel(CisloC))
                ii=TriN+1-k
                jj=mod(ii-1,NSymmBlock)+1
                if(jj.le.3) then
                  jp=jj
                  t1='p'
                else
                  jp=jj-3
                  t1='m'
                endif
                write(Cislo,106) (ii-1)/NSymmBlock+1
                call Zhusti(Cislo)
                Cislo='M'//smbx(jp)//Cislo(:idel(Cislo))
                if(NSymmBlock.gt.3) Cislo=Cislo(:idel(Cislo))//t1
                Prvni=.false.
                StMag(j)=StMag(j)(:idel(StMag(j)))//
     1                   Cislo(:idel(Cislo))
              endif
            enddo
            if(Prvni) StMag(j)=StMag(j)(:idel(StMag(j)))//'0'
            m=m-1
            io=0
          else
            if(j.le.3) then
              jp=j
              t1='p'
            else
              jp=j-3
              t1='m'
            endif
            Cislo='M'//smbx(jp)//t5(:idel(t5))
            if(NSymmBlock.gt.3) Cislo=Cislo(:idel(Cislo))//t1
            StMag(j)=Cislo(:idel(Cislo))//' ... free'
          endif
          idl=max(idl,idel(StMag(j)))
        enddo
        if(idl.gt.20) then
          call NewLn(1)
          write(lst,'(i4,10x,a20,5x,a80)') i,SymmCodes(i),StMag(1)
          do j=2,NSymmBlock
            call NewLn(1)
            write(lst,'(39x,a80)') StMag(j)
          enddo
        else
          call NewLn(1)
          write(lst,108) i,SymmCodes(i),(StMag(j),j=1,3)
          if(NSymmBlock.gt.3) then
            call NewLn(1)
            write(lst,110)(StMag(j),j=4,6)
          endif
        endif
      enddo
      if(Klic.eq.0) go to 9999
      ns=0
      do i=1,NSymmS
        if(Klic.ge.0) then
          ISel=Irrep
          Brat=BratSymmKer(i,ISel)
        else
          ISel=IEpi
          Brat=BratSymmEpi(i,ISel)
        endif
        if(Brat) then
          ns=ns+1
          call CopyMat(rm6r(1,i),rm6(1,ns,1,KPhase),NDim(KPhase))
          call CopyMat(rmr (1,i),rm (1,ns,1,KPhase),3)
          if(Klic.ge.0) then
            call CopyVek(s6Ker(1,i,ISel),s6(1,ns,1,KPhase),NDim(KPhase))
            zmag(ns,1,KPhase)=ZMagKer(i,ISel)
            IswSymm(ns,1,KPhase)=1
          else
            call CopyVek(s6Epi(1,i,ISel),s6(1,ns,1,KPhase),NDim(KPhase))
            zmag(ns,1,KPhase)=ZMagEpi(i,ISel)
            IswSymm(ns,1,KPhase)=1
          endif
        endif
      enddo
      NSymmN(KPhase)=ns
      call RACompleteSymmPlusMagInv(ns,ich)
      if(Klic.ge.0) then
        Veta=GrpKer(ISel)
      else
        Veta=GrpEpi(ISel)
      endif
      Veta='Centrosymmetric super-space group: '//Veta(:idel(Veta))
      if(NDimI(KPhase).eq.0) Veta(18:)=Veta(24:)
      if(CrlCentroSymm().le.0) Veta='Non-c'//Veta(2:)
      call newln(2)
      write(lst,FormA)
      write(lst,FormA) Veta(:idel(Veta))
      call newln(3)
      write(lst,'(/''List of centring vectors:''/)')
      do j=1,NLattVec(KPhase)
        call newln(1)
        write(lst,'(6f10.6)')(vt6(k,j,1,KPhase),k=1,NDim(KPhase))
      enddo
      call newln(3)
      write(lst,'(/''Symmetry operators:''/)')
      n=0
      do j=1,NSymmN(KPhase)
        call CodeSymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                symmc(1,j,1,KPhase),0)
        do k=1,NDim(KPhase)
          kk=idel(symmc(k,j,1,KPhase))+1
          if(symmc(k,j,1,KPhase)(1:1).ne.'-') kk=kk+1
          n=max(n,kk)
        enddo
      enddo
      if(n.lt.5) n=5
      do j=1,NSymmN(KPhase)
        Veta=' '
        k=5
        do l=1,NDim(KPhase)
          if(symmc(l,j,1,KPhase)(1:1).eq.'-') then
            kk=k
          else
            kk=k+1
          endif
          Veta(kk:)=symmc(l,j,1,KPhase)
           k=k+n
        enddo
        if(MagneticType(KPhase).ne.0) then
          if(zmag(j,1,KPhase).gt.0) then
            Veta(k+1:k+1)='m'
          else
            Veta(k:k+1)='-m'
          endif
        endif
        call newln(1)
        write(lst,FormA) Veta(:idel(Veta))
      enddo
      if(Klic.ge.0) then
        call MatBlock3(TrMatKer(1,1,ISel),TrPom,NDim(KPhase))
      else
        call MatBlock3(TrMatEpi(1,1,ISel),TrPom,NDim(KPhase))
      endif
      if(QPul) then
        call Multm(TrPom,RCommen(1,1,KPhase),TrMatP,3,3,3)
      else
        call CopyMat(TrPom,TrMatP,3)
      endif
      Standard=MatRealEqUnitMat(TrMatP,3,.001)
      if(.not.Standard) then
        call newln(3)
        write(lst,'(/''Symmetry operations are related to the '',
     1               ''original axes. The following transfomation '',
     2               ''brings it to the standard setting:''/)
     3               ')
        do i=1,3
          Veta='          '//SmbABC(i)//'''='
          idl=idel(Veta)
          Prvni=.true.
          do j=1,3
            if(Klic.ge.0) then
              pom=TrPom(j,i)
            else
              pom=TrPom(j,i)
            endif
            if(abs(pom).lt..001) cycle
            call ToFract(pom,t4,9)
            if(EqIgCase(t4,'1')) then
              if(Prvni) then
                t4=' '
              else
                t4=' +'
              endif
            else if(EqIgCase(t4,'-1')) then
              t4=' -'
            else if(pom.gt.0.) then
              if(.not.Prvni) then
                t4='+'//t4(:idel(t4))
              else
                t4=' '//t4(:idel(t4))
              endif
            endif
            if(index(t4,'/').le.0) t4='  '//t4(:idel(t4))
            Veta=Veta(:idl)//t4//SmbABC(j)
            Prvni=.false.
            idl=idl+5
          enddo
          call newln(1)
          write(lst,FormA) Veta(:idel(Veta))
        enddo
      endif
      call newln(3)
      write(lst,'(/''Additional restrictions of magnetic moments:''/)')
      if(Klic.gt.0) then
        call SetComplexArrayTo(pa,TriN**2,(0.,0.))
        mk=0
        do i=1,NCondKer(ISel)
          m=0
          do j=1,TriN,NSymmBlock
            do k=j,j+NSymmBlock-1
              if(abs(CondKer(i,k,Irrep)).gt..01) then
                m=m+1
                exit
              endif
            enddo
          enddo
          if(m.gt.2) then
            mk=mk+1
            do j=1,TriN
              pa(mk,j)=CondKer(i,j,Irrep)
            enddo
          endif
        enddo
      else
        mk=NCondEpi(ISel)
        do i=1,mk
          do j=1,TriN
            pa(i,j)=CondEpi(i,j,IEpi)
          enddo
        enddo
      endif
      mm=0
      idl=0
      do m=mk,1,-1
        io=0
        do k=1,TriN
          pomc=pa(m,k)
          if(abs(pomc).gt..001) then
            io=k
            exit
          endif
        enddo
        if(io.le.0) cycle
        mm=mm+1
        j=mod(TriN-io,NSymmBlock)+1
        write(t5,106) (TriN-io)/NSymmBlock+1
        call Zhusti(t5)
        if(NSymmBlock.eq.3) then
          StMag(mm)='M'//smbx(j)//t5(:idel(t5))//'='
        else
          if(j.le.3) then
            jp=j
            t1='p'
          else
            jp=j-3
            t1='m'
          endif
          StMag(mm)='M'//smbx(jp)//t5(:idel(t5))//t1//'='
        endif
        Prvni=.true.
        do k=TriN,io+1,-1
          pomc=-pa(m,k)
          if(abs(pomc).gt..001) then
            if(abs(aimag(pomc)).lt..001.or.
     1         abs( real(pomc)).lt..001) then
              if(abs(aimag(pomc)).lt..001) then
                write(CisloC,105) real(pomc)
              else
                write(CisloC,105) aimag(pomc)
              endif
              call ZdrcniCisla(CisloC,1)
              if(.not.Prvni.and.
     1            (real(pomc).gt..001.or.aimag(pomc).gt..001))
     2           CisloC='+'//CisloC
              if(abs(abs(pomc)-1.).lt..001) then
                if(Prvni.and.
     1             ( real(pomc).gt..001.or.
     2              aimag(pomc).gt..001)) then
                  CisloC=' '
                else
                  CisloC(2:)=' '
                endif
              endif
              if(abs(real(pomc)).lt..001)
     1          CisloC=CisloC(:idel(CisloC))//'i'
            else
              write(CisloC,105) real(pomc)
              call ZdrcniCisla(CisloC,1)
              write(Cislo ,105) aimag(pomc)
              call ZdrcniCisla(Cislo ,1)
              if(aimag(pomc).gt..001) Cislo='+'//Cislo
              if(abs(abs(aimag(pomc))-1.).lt..001) then
                if(aimag(pomc).gt..001) Cislo(2:)=' '
              endif
              Cislo=Cislo(:idel(Cislo))//'i'
              CisloC='('//CisloC(:idel(CisloC))//
     1               Cislo(:idel(Cislo))//')'
              if(.not.Prvni) CisloC='+'//CisloC
            endif
            StMag(mm)=StMag(mm)(:idel(StMag(mm)))//
     1               CisloC(:idel(CisloC))
            ii=TriN+1-k
            jj=mod(ii-1,NSymmBlock)+1
            if(jj.le.3) then
              jp=jj
              t1='p'
            else
              jp=jj-3
              t1='m'
            endif
            write(Cislo,106) (ii-1)/NSymmBlock+1
            call Zhusti(Cislo)
            Cislo='M'//smbx(jp)//Cislo(:idel(Cislo))
            if(NSymmBlock.gt.3) Cislo=Cislo(:idel(Cislo))//t1
            Prvni=.false.
            StMag(mm)=StMag(mm)(:idel(StMag(mm)))//
     1               Cislo(:idel(Cislo))
          endif
        enddo
        idl=max(idl,idel(StMag(mm)))
        if(mm.ge.3) then
          if(idl.gt.40) then
            call NewLn(3)
            write(lst,'(a80)')(StMag(j),j=1,mm)
          else
            call NewLn(1)
            write(lst,'(3(a40,2x))')(StMag(j),j=1,mm)
          endif
          idl=0
          mm=0
        endif
      enddo
9999  if(allocated(pa)) deallocate(pa,pb)
      return
105   format(f8.3)
106   format(i5)
107   format(i4,10x,a20,3f8.3,5x,3(a20,1x))
108   format(i4,10x,a20,5x,3(a20,1x))
109   format(63x,3(a20,1x))
110   format(39x,3(a20,1x))
      end
      subroutine RACompleteSymmPlusMagInv(ns,ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer CrlMagInver
      logical MatRealEqUnitMat
      ich=0
      NSymmN(KPhase)=ns
      MaxNSymmN=NSymm(KPhase)
      NSymm(KPhase)=ns
      call CompleteSymm(0,ich)
      if(ich.ne.0) go to 9999
      if(NDimI(KPhase).eq.1) then
        do i=1,ns
          if(MatRealEqUnitMat(rm6(1,i,1,KPhase),NDim(KPhase),.01).and.
     1      abs(s6(4,i,1,KPhase)-.5).lt..01.and.
     2      abs(ZMag(i,1,KPhase)+1.).lt..01) go to 1500
        enddo
        ns=ns+1
        call ReallocSymm(NDim(KPhase),ns,NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,0)
        call UnitMat(rm6(1,ns,1,KPhase),NDim(KPhase))
        call UnitMat(rm(1,ns,1,KPhase),3)
        call SetRealArrayTo(s6(1,ns,1,KPhase),NDim(KPhase),0.)
        s6(4,ns,1,KPhase)=.5
        ZMag(ns,1,KPhase)=-1.
        call CrlMakeRMag(ns,1)
        IswSymm(ns,1,KPhase)=1
        NSymm(KPhase)=ns
1500    call CompleteSymm(0,ich)
        if(ich.ne.0) go to 9999
      else
        if(CrlMagInver().gt.0) then
          ich=1
          go to 9999
        endif
      endif
      MaxNSymm=NSymm(KPhase)
      MaxNSymmN=NSymmN(KPhase)
9999  return
      end
      subroutine RACheckRealIrr(NIrr,ns,NDimQIrr,NDimIrr,UseLG,RIrr,
     1                          Klic,ich)
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer NDimIrr(*),UseLG(*)
      integer, allocatable :: ConjComb(:),NDimIrrP(:)
      complex U(36),UConj(36),V(26),RIrr(NDimQIrr,ns,NIrr),
     1        BlockUnit(36),Block(36),pomc
      complex, allocatable :: RIrrP(:,:,:),Herring(:)
      real s6p(3),xp(3)
      logical eqrvm
      allocate(Herring(NIrr),ConjComb(NIrr))
      ich=0
      do i=1,NIrr
        n=NDimIrr(i)
        Herring(i)=(0.,0.)
        do j=1,NSymmN(KPhase)
          if(NDimI(KPhase).eq.1) then
            call MultM(qu(1,1,1,KPhase),rmr(1,j),xp,1,3,3)
            if(.not.eqrvm(xp,qu(1,1,1,KPhase),3,.001)) cycle
          endif
!          if(UseLG(j).ne.0.and.NDimI(KPhase).eq.1) cycle
          is=PerTabOrg(j,j)
          if(NDimI(KPhase).eq.1) then
            s6p(1:3)=s6r(1:3,j)
            call CultM(rmr(1,j),s6r(1,j),s6p,3,3,1)
            pomc=exp(-pi2*cmplx(0.,ScalMul(s6p,qu(1,1,1,KPhase))))
          else
            pomc=(1.,0.)
          endif
          m=1
          do k=1,n
            Herring(i)=Herring(i)+RIrr(m,is,i)*pomc
            m=m+n+1
          enddo
        enddo
        Herring(i)=Herring(i)/cmplx(float(NSymmN(KPhase)),0.)
        if(NDimI(KPhase).eq.1) Herring(i)=2.*Herring(i)
      enddo
      if(NDimI(KPhase).eq.1.and.Klic.eq.1) then
        do i=1,NIrr
          n2=NDimIrr(i)*2
          n1=NDimIrr(i)
          do is=1,NSymmN(KPhase)
            call CopyMatC(RIrr(1,is,i),U,n2)
            k=0
            do jj=1,n1
              do ii=1,n1
                k=k+1
                RIrr(k,is,i)=U(ii+(jj-1)*n2)
              enddo
            enddo
          enddo
        enddo
      endif
      NConj=0
      do 1300i=1,NIrr-1
        n1=NDimIrr(i)
        if(abs(Herring(i)-(1.,0.)).lt..01) cycle
        if(abs(Herring(i)+(1.,0.)).lt..01) go to 9300
        do 1100j=i+1,NIrr
          n2=NDimIrr(j)
          if(abs(Herring(j)-(1.,0.)).lt..01.or.n1.ne.n2) cycle
          do is=1,NSymmN(KPhase)
            if(UseLG(is).eq.0) cycle
            do k=1,n1
              if(abs(RIrr(k,is,i)-conjg(RIrr(k,is,j))).gt..01)
     1          then
                go to 1100
              endif
            enddo
          enddo
          NConj=NConj+1
          ConjComb(NConj)=1000*i+j
          go to 1300
1100    continue
        do 1200j=i+1,NIrr
          n2=NDimIrr(j)
          if(abs(Herring(j)-(1.,0.)).lt..01.or.n1.ne.n2) cycle
          do is=1,NSymmN(KPhase)
            if(UseLG(is).eq.0) cycle
            do k=1,n1
              if(abs(RIrr(k,is,i)-RIrr(k,is,j)).gt..01)
     1          then
                go to 1150
              endif
            enddo
            go to 1175
1150        do k=1,n1
              if(abs(RIrr(k,is,i)+RIrr(k,is,j)).gt..01)
     1          then
                go to 1200
              endif
            enddo
          enddo
1175      NConj=NConj+1
          ConjComb(NConj)=1000*i+j
1200    continue
1300  continue
      if(NConj.eq.0.or.(NDimI(KPhase).eq.1.and.NDimIrr(i).gt.1))
     1  go to 9999
      do i=1,NConj
        ii=ConjComb(i)/1000
        jj=mod(ConjComb(i),1000)
        n=NDimIrr(ii)
        if(n.le.0.or.NDimIrr(jj).le.0) cycle
        n2=2*n
        nq=n**2
        call UnitMatC(BlockUnit,n)
        do j=1,nq
          Block(j)=cmplx(rsqrt2,0.)*BlockUnit(j)
        enddo
        call SetMatBlock(U,n2,1,1,Block,0.,n)
        call SetMatBlock(U,n2,n+1,n+1,Block,0.,n)
        do j=1,nq
          Block(j)=cmplx(0.,rsqrt2)*BlockUnit(j)
        enddo
        call SetMatBlock(U,n2,1,n+1,Block,0.,n)
        call SetMatBlock(U,n2,n+1,1,Block,0.,n)
        call MatTransConjg(U,UConj,n2)
        do is=1,NSymmN(KPhase)
          if(UseLG(is).eq.0) cycle
          call CopyMatC(RIrr(1,is,ii),V,n)
          RIrr(1:n2**2,is,ii)=(0.,0.)
          call SetMatBlock(RIrr(1,is,ii),n2,1,1,V,0.,n)
          call CopyMatC(RIrr(1,is,jj),V,n)
          call SetMatBlock(RIrr(1,is,ii),n2,n+1,n+1,V,0.,n)
          call MultMC(RIrr(1,is,ii),UConj,V,n2,n2,n2)
          call MultMC(U,V,RIrr(1,is,ii),n2,n2,n2)
        enddo
        NDimIrr(ii)=-n2
        NDimIrr(jj)=0
      enddo
      allocate(NDimIrrP(NIrr),RIrrP(36,NSymmN(KPhase),NIrr))
      mmx=0
      do i=1,NIrr
        NDimIrr(i)=abs(NDimIrr(i))
        mmx=max(NDimIrr(i),mmx)
        n=NDimIrr(i)
        NDimIrrP(i)=n
        do is=1,NSymmN(KPhase)
          if(UseLG(is).eq.0) cycle
          call CopyMatC(RIrr(1,is,i),RIrrP(1,is,i),n)
        enddo
      enddo
      j=0
      do m=1,mmx
        do i=1,NIrr
          n=NDimIrrP(i)
          if(n.ne.m) cycle
          j=j+1
          do is=1,NSymmN(KPhase)
            if(UseLG(is).eq.0) cycle
            call CopyMatC(RIrrP(1,is,i),RIrr(1,is,j),n)
          enddo
          NDimIrr(j)=NDimIrrP(i)
        enddo
      enddo
      NIrr=j
      deallocate(NDimIrrP,RIrrP)
      go to 9999
9300  ich=1
9999  if(allocated(Herring)) deallocate(Herring,ConjComb)
      return
      end
      subroutine RAGenSubgroups
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: SelSub(:),is(:),SelPom(:),NGrpSub(:),
     1                        BratSubO(:,:),NBratSubO(:),GenSubO(:,:),
     2                        NGenSubO(:),NRev(:),NRevO(:)
      integer CrlCentroSymm,io(3)
      logical EqIgCase
      real rmi(36),xp(9),Shift(6)
      character*80 Veta,Grp,t80
      character*3  t3
      character*2  t2m
      allocate(SelSub(NSymmS),is(NSymmS),SelPom(NSymmS))
      NSubMax=10
      allocate(BratSub(NSymmS,NSubMax),NBratSub(NSubMax),
     1         GenSub(4,NSubMax),NGenSub(NSubMax),NRev(NSubMax))
      invc=CrlCentroSymm()
      SelPom=0
      nss=0
      do i=1,NSymmS
        if(i.ne.invc) then
          nss=nss+1
          SelPom(nss)=i
        endif
      enddo
      NSub=0
      do i1=1,nss
        i1a=SelPom(i1)
        do i2=1,i1
          if(i1.eq.i2.and.i2.ne.1) cycle
          i2a=SelPom(i2)
          do 5000i3=1,i2
            if((i1.eq.i3.or.i2.eq.i3).and.i3.ne.1) cycle
            i3a=SelPom(i3)
            is=0
            SelSub=0
            SelSub(1)=1
            ns=1
            if(i1.gt.1) then
              ns=ns+1
              SelSub(ns)=i1a
            endif
            if(i2.gt.1) then
              ns=ns+1
              SelSub(ns)=i2a
            endif
            if(i3.gt.1) then
              ns=ns+1
              SelSub(ns)=i3a
            endif
            ic=0
            nsg=ns
1400        is=0
1500        i=0
2000        if(i.ge.ns) go to 2500
            i=i+1
            j=is(i)
2100        if(j.ge.ns) go to 2000
            j=j+1
            is(i)=j
            k=PerTabOrg(SelSub(i),SelSub(j))
            do m=1,ns
              if(k.eq.SelSub(m)) go to 2100
            enddo
            ns=ns+1
            SelSub(ns)=k
            go to 1500
2500        if(NSub.ge.NSubMax) then
              allocate(BratSubO(NSymmS,NSub),NBratSubO(NSub),
     1                 GenSubO(4,NSub),NGenSubO(NSub),NRevO(NSub))
              NBratSubO=NBratSub
              NGenSubO=NGenSub
              NRevO=NRev
              do i=1,NSub
                BratSubO(1:NSymmS,i)=BratSub(1:NSymmS,i)
                GenSubO(1:NGenSub(i),i)=GenSub(1:NGenSub(i),i)
              enddo
              deallocate(BratSub,NBratSub,GenSub,NGenSub,NRev)
              n=NSubMax*2
              allocate(BratSub(NSymmS,n),NBratSub(n),GenSub(4,n),
     1                 NGenSub(n),NRev(n))
              NBratSub(1:NSub)=NBratSubO
              NGenSub(1:NSub)=NGenSubO
              NRev(1:NSub)=NRevO
              do i=1,NSub
                BratSub(1:NSymmS,i)=BratSubO(1:NSymmS,i)
                GenSub(1:NGenSub(i),i)=GenSubO(1:NGenSub(i),i)
              enddo
              NSubMax=n
              deallocate(BratSubO,NBratSubO,GenSubO,NGenSubO,NRevO)
            endif
            NSub=NSub+1
            BratSub(1:NSymmS,NSub)=0
            do i=1,ns
              BratSub(SelSub(i),NSub)=1
            enddo
            NRev(NSub)=0
            if(NDimI(KPhase).eq.1) then
              do i=2,nsg
                if(KSymmLG(SelSub(i)).gt.0) NRev(NSub)=NRev(NSub)+1
              enddo
            endif
            NBratSub(NSub)=ns
            if(NSub.eq.1) go to 3000
            if(invc.gt.0.and.ic.eq.0) then
              if(BratSub(invc,NSub).gt.0) then
                NSub=NSub-1
                go to 5000
              endif
            endif
            do 2600i=1,NSub-1
              do j=1,NSymmS
                if(BratSub(j,NSub).ne.BratSub(j,i)) go to 2600
              enddo
              if(nsg-1.lt.NGenSub(i).or.
     1           (nsg-1.eq.NGenSub(i).and.NRev(NSub).gt.NRev(i))) then
                NGenSub(i)=nsg-1
                do j=2,nsg
                  GenSub(j-1,i)=SelSub(j)
                enddo
                NRev(i)=NRev(NSub)
              endif
              NSub=NSub-1
              go to 3500
2600        continue
3000        NGensub(NSub)=nsg-1
            do i=2,nsg
              GenSub(i-1,NSub)=SelSub(i)
            enddo
3500        if(invc.gt.0.and.ic.eq.0) then
              do i=1,ns
                if(SelSub(i).eq.invc) go to 5000
              enddo
              nsg=nsg+1
              ns=nsg
              SelSub(ns)=invc
              ic=1
              go to 1400
            endif
5000      continue
        enddo
      enddo
      allocate(NGrpSub(NSub),IPorSub(NSub),TakeSub(NSub))
      do i=1,NSub
        ns=0
        do j=1,NSymmS
          if(BratSub(j,i).gt.0) then
            ns=ns+1
            call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
            call CopyMat(rmr (1,j),rm  (1,ns,1,KPhase),3)
            call CopyMat(rmr (1,j),rmag(1,ns,1,KPhase),3)
            call CopyVek(s6r(1,j),s6(1,ns,1,KPhase),
     1                   NDim(KPhase))
            zmag(ns,1,KPhase)=1.
            IswSymm(ns,1,KPhase)=1
          endif
        enddo
        NSymmN(KPhase)=ns
        NSymm(KPhase)=ns
        qu(1:3,1,1,KPhase)=QMagIrr(1:3,1,KPhase)
        call FindSmbSgTr(Veta,Shift,Grp,NGrpSub(i),rmi,xp,kk,kk)
        qu(1:3,1,1,KPhase)=QMag(1:3,1,KPhase)
        NGrpSub(i)=NGrpSub(i)*1000000+30
        call MatBlock3(rmi,xp,NDim(KPhase))
        do j=1,9,4
          NGrpSub(i)=NGrpSub(i)-nint(xp(j))
        enddo
        i6m=0
        i3=0
        do 6000j=1,NSymmN(KPhase)
          call SmbOp(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),1,1.,
     1               t3,t2m,io,nn,pom)
          if(EqIgCase(t3,'-6')) then
            do k=1,NGenSub(i)
              if(GenSub(k,i).eq.j) i6m=j
            enddo
          else if(EqIgCase(t3,'3')) then
            do k=1,NGenSub(i)
              if(GenSub(k,i).eq.j) go to 6000
            enddo
            i3=j
          endif
          if(i6m.gt.0.and.i3.gt.0) then
            NGenSub(i)=NGenSub(i)+1
            GenSub(NGenSub(i),i)=i3
            exit
          endif
6000    continue
      enddo
      TakeSub=1
      do i=1,NSub
        if(TakeSub(i).lt.1) cycle
        NGrpSub(i)=NGrpSub(i)+1000*i
c        write(66,'(70(''=''))')
c        write(66,'('' Rad podgrupy:'',3i5)') NBratSub(i),i
c        write(66,'('' Generatory:'',10i5)')
c     1   (GenSub(j,i),j=1,NGenSub(i))
c        write(66,'(i15)') NGrpSub(i)
c        write(66,'(70(''=''))')
        do 7100j=i+1,NSub
          if(TakeSub(j).lt.1) cycle
          if(NGrpSub(i)/1000000.ne.NGrpSub(j)/1000000) cycle
          do 7000itw=2,NSymmS
            if(BratSub(itw,i).eq.1) go to 7000
            itwi=2
            do while(PerTabOrg(itw,itwi).ne.1)
              itwi=itwi+1
            enddo
            do k=2,NSymmS
              if(BratSub(k,i).ne.1) cycle
              m=PerTab(PerTabOrg(itw,k),itwi)
              if(BratSub(m,j).ne.1) go to 7000
            enddo
            NGrpSub(j)=NGrpSub(j)+1000*i
            TakeSub(j)=0
c            write(66,'('' Eqivalence:'',2i4)') i,j
c            write(66,'('' Rad podgrupy:'',3i5)') NBratSub(j),j
c            write(66,'('' Generatory:'',10i5)')
c     1       (GenSub(jj,j),jj=1,NGenSub(j))
c            write(66,'(i15)') NGrpSub(j)
c            write(66,'(70(''-''))')
            go to 7100
7000      continue
7100    continue
      enddo
      call indexx(NSub,NGrpSub,IPorSub)
      NGrpSubLast=0
      do i=1,NSub
        ii=IPorSub(i)
        k=NGrpSub(ii)/1000
        if(k.ne.NGrpSubLast) then
          TakeSub(ii)=1
          NGrpSubLast=k
        else
          TakeSub(ii)=0
        endif
c        write(66,'('' Rad podgrupy:'',4i5)') NBratSub(ii),i,ii,
c     1                                       TakeSub(ii)
c        write(66,'('' Generatory:'',10i5)')
c     1   (GenSub(j,ii),j=1,NGenSub(ii))
c        write(66,'('' KSymmLG:   '',10i5)')
c     1   (KSymmLG(GenSub(j,ii)),j=1,NGenSub(ii))
        ns=0
        do j=1,NSymmS
          if(BratSub(j,ii).gt.0) then
            ns=ns+1
            SelSub(ns)=j
            call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
            call CopyMat(rmr (1,j),rm (1,ns,1,KPhase),3)
            call CopyVek(s6r (1,j),s6(1,ns,1,KPhase),
     1                   NDim(KPhase))
            zmag(ns,1,KPhase)=1.
            IswSymm(ns,1,KPhase)=1
          endif
        enddo
        NSymmN(KPhase)=ns
        NSymm(KPhase)=ns
        call FindSmbSgTr(Veta,Shift,Grp,NGrTr,rmi,xp,kk,kk)
        call TrMat2String(rmi,NDim(KPhase),t80)
        Veta=Grp(:idel(Grp))//'  '//t80(:idel(t80))
        call OriginShift2String(xp,3,t80)
        Veta=Veta(:idel(Veta))//'   '//t80(:idel(t80))
      enddo
      ns=0
      do j=1,NSymmS
        ns=ns+1
        call CopyMat(rm6r(1,j),rm6(1,ns,1,KPhase),NDim(KPhase))
        call CopyMat(rmr (1,j),rm (1,ns,1,KPhase),3)
        call CopyVek(s6r (1,j),s6(1,ns,1,KPhase),
     1               NDim(KPhase))
        zmag(ns,1,KPhase)=1.
        IswSymm(ns,1,KPhase)=1
      enddo
      NSymmN(KPhase)=ns
      NSymm(KPhase)=ns
      call FindSmbSgTr(Veta,Shift,Grp,NGrupa(KPhase),rmi,xp,kk,kk)
      deallocate(SelSub,is,SelPom,NGrpSub,NRev)
      return
      end
      subroutine RAGetIrrSymbol(SmbIrr,GrpPar,NGrpPar,Grp,NGrp,IPor,ich)
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) SmbIrr,GrpPar,Grp
      character*10  StPom
      integer Type
      logical EqIgCase
      ich=0
      idlg=idel(Grp)
      Type=0
      if(Grp(1:1).eq.'?'.or.QPul) go to 9000
      if(NDimI(KPhase).eq.1) then
        i1=index(Grp,'(')
        i2=index(Grp,')')
        if(i1.le.0.or.i2.le.0.or.i2.lt.i1) go to 9000
        if(NGrpPar.eq.1) then
          SmbIrr='GP1GP1'
        else if(NGrpPar.eq.2) then
          SmbIrr='GP1'
        else if(NGrpPar.ge.3.and.NGrpPar.le.15) then
          if(EqIgCase(Grp(i1+1:i2-1),'a00').or.
     1       EqIgCase(Grp(i1+1:i2-1),'0b0').or.
     2       EqIgCase(Grp(i1+1:i2-1),'00g')) then
            SmbIrr='LD'
            Type=1
          else if(EqIgCase(Grp(i1+1:i2-1),'a1/21/2').or.
     1            EqIgCase(Grp(i1+1:i2-1),'1/2b1/2').or.
     2            EqIgCase(Grp(i1+1:i2-1),'1/21/2g')) then
            SmbIrr='U'
            Type=1
          else if(EqIgCase(Grp(i1+1:i2-1),'a1/20').or.
     1            EqIgCase(Grp(i1+1:i2-1),'0b1/2').or.
     2            EqIgCase(Grp(i1+1:i2-1),'1/20g')) then
            if(EqIgCase(Grp(1:1),'P')) then
              SmbIrr='V'
            else
              SmbIrr='U'
            endif
            Type=1
          else if(EqIgCase(Grp(i1+1:i2-1),'a01/2').or.
     1            EqIgCase(Grp(i1+1:i2-1),'1/2b0').or.
     2            EqIgCase(Grp(i1+1:i2-1),'01/2g')) then
            if(EqIgCase(Grp(1:1),'P')) then
              SmbIrr='W'
            else
              SmbIrr='U'
            endif
            Type=1
          else if(EqIgCase(Grp(i1+1:i2-1),'a0g').or.
     1            EqIgCase(Grp(i1+1:i2-1),'ab0').or.
     2            EqIgCase(Grp(i1+1:i2-1),'0bg')) then
            if(EqIgCase(Grp(1:1),'P')) then
              SmbIrr='F'
            else
              SmbIrr='B'
            endif
            Type=2
          else if(EqIgCase(Grp(i1+1:i2-1),'a1/2g').or.
     1            EqIgCase(Grp(i1+1:i2-1),'ab1/2').or.
     2            EqIgCase(Grp(i1+1:i2-1),'1/2bg')) then
            if(EqIgCase(Grp(1:1),'P')) then
              SmbIrr='G'
            else
              SmbIrr='B'
            endif
            Type=2
          else
            go to 9000
          endif
          if(NGrpPar.le.9) then
            ip=i2+1
          else
            if(Type.eq.1) then
              ip=i2+1
            else
              ip=i2+2
            endif
          endif
          if(EqIgCase(Grp(ip:ip),'0')) then
            SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
          else if(EqIgCase(Grp(ip:ip),'s')) then
            SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
          else
            go to 9000
          endif
          if((NGrpPar.ge.3.and.NGrpPar.le.5.and.Type.eq.1).or.
     1       (NGrpPar.ge.6.and.NGrpPar.le.8.and.Type.eq.2)) go to 3000
        else if(NGrpPar.ge.16.and.NGrpPar.le.74) then
          if(EqIgCase(Grp(1:1),'P')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='SM'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a1/20')) then
              SmbIrr='C'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a01/2')) then
              SmbIrr='A'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a1/21/2')) then
              SmbIrr='E'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'1/2b0')) then
              SmbIrr='D'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'0b1/2')) then
              SmbIrr='B'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'1/2b1/2')) then
              SmbIrr='P'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='LD'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'1/20g')) then
              SmbIrr='G'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'01/2g')) then
              SmbIrr='H'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'1/21/2g')) then
              SmbIrr='Q'
              Type=3
            else
              go to 9000
            endif
          else if(EqIgCase(Grp(1:1),'C')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='SM'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a01/2')) then
              SmbIrr='A'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'0b1/2')) then
              SmbIrr='B'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='LD'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'10g').or.
     1              EqIgCase(Grp(i1+1:i2-1),'01g')) then
              SmbIrr='H'
              Type=3
            else
              go to 9000
            endif
          else if(EqIgCase(Grp(1:1),'A')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='LD'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a01').or.
     1              EqIgCase(Grp(i1+1:i2-1),'a10')) then
              SmbIrr='H'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'1/2b0')) then
              SmbIrr='B'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='SM'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'1/20g')) then
              SmbIrr='A'
              Type=3
            else
              go to 9000
            endif
          else if(EqIgCase(Grp(1:1),'F')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='SM'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'a01').or.
     1              EqIgCase(Grp(i1+1:i2-1),'a10')) then
              SmbIrr='A'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'0b1').or.
     1              EqIgCase(Grp(i1+1:i2-1),'1b0')) then
              SmbIrr='B'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='LD'
              Type=3
            else if(EqIgCase(Grp(i1+1:i2-1),'10g').or.
     1              EqIgCase(Grp(i1+1:i2-1),'01g')) then
              SmbIrr='H'
              Type=3
            else
              go to 9000
            endif
          else if(EqIgCase(Grp(1:1),'I')) then
            if(EqIgCase(Grp(i1+1:i2-1),'a00')) then
              SmbIrr='SM'
              Type=1
            else if(EqIgCase(Grp(i1+1:i2-1),'0b0')) then
              SmbIrr='DT'
              Type=2
            else if(EqIgCase(Grp(i1+1:i2-1),'00g')) then
              SmbIrr='LD'
              Type=3
            else
              go to 9000
            endif
          endif
          if(NGrpPar.ge.16.and.NGrpPar.le.24) then
            ip=i2+Type
            if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip),'s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else
              go to 9000
            endif
          else if(NGrpPar.ge.25.and.NGrpPar.le.46) then
            if(Type.eq.3) then
              ip=i2+1
              if(EqIgCase(Grp(ip:ip+2),'000').or.
     1           EqIgCase(Grp(ip:ip+2),'qq0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip+2),'ss0').or.
     1                EqIgCase(Grp(ip:ip+4),'-q-q0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+2),'0ss').or.
     1                EqIgCase(Grp(ip:ip+3),'-qqs')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip+2),'s0s').or.
     1                EqIgCase(Grp(ip:ip+3),'q-qs')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
              go to 3000
            else
              ip=i2+3-Type
              if(EqIgCase(Grp(ip:ip),'0').or.
     1           EqIgCase(Grp(ip:ip),'q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip),'s').or.
     1                EqIgCase(Grp(ip:ip+1),'-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else
                go to 9000
              endif
            endif
          else if(NGrpPar.ge.47.and.NGrpPar.le.74) then
            ip=i2+1
            if(Type.eq.3) then
              if(EqIgCase(Grp(ip:ip+2),'000').or.
     1                EqIgCase(Grp(ip:ip+2),'qq0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip+2),'ss0').or.
     1           EqIgCase(Grp(ip:ip+4),'-q-q0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+2),'0s0').or.
     1                EqIgCase(Grp(ip:ip+3),'q-q0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip+2),'s00').or.
     1                EqIgCase(Grp(ip:ip+3),'-qq0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
            else if(Type.eq.2) then
              if(EqIgCase(Grp(ip:ip+2),'000').or.
     1           EqIgCase(Grp(ip:ip+2),'q0q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip+2),'s0s').or.
     1                EqIgCase(Grp(ip:ip+4),'-q0-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+2),'00s').or.
     1                EqIgCase(Grp(ip:ip+3),'-q0q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip+2),'s00').or.
     1                EqIgCase(Grp(ip:ip+3),'q0-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
            else if(Type.eq.1) then
              if(EqIgCase(Grp(ip:ip+2),'000').or.
     1                EqIgCase(Grp(ip:ip+2),'0qq')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip+2),'0ss').or.
     1           EqIgCase(Grp(ip:ip+4),'0-q-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+2),'00s').or.
     1                EqIgCase(Grp(ip:ip+3),'0q-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip+2),'0s0').or.
     1                EqIgCase(Grp(ip:ip+3),'0-qq')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
            endif
          else
            go to 9000
          endif
        else if(NGrpPar.ge.75.and.NGrpPar.le.142) then
          if(EqIgCase(Grp(i1+1:i2-1),'00g').or.
     1       EqIgCase(Grp(i1+1:i2-1),'0b0')) then
            SmbIrr='LD'
          else if(EqIgCase(Grp(i1+1:i2-1),'1/21/2g').or.
     1            EqIgCase(Grp(i1+1:i2-1),'1/2b1/2')) then
            if(EqIgCase(GrpPar(1:1),'I')) then
              SmbIrr='W'
            else
              SmbIrr='V'
            endif
          else
            go to 9000
          endif
          if(NGrpPar.ge.75.and.NGrpPar.le.80) then
            ip=i2+1
            if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip),'s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+1),'-q')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip),'q')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
            go to 3000
          else if(NGrpPar.ge.81.and.NGrpPar.le.82) then
            if(NGrp.lt.81) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              go to 3000
            else
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            endif
          else if(NGrpPar.ge.83.and.NGrpPar.le.88) then
            ip=i2+1
            if(NGrp.lt.81) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'//
     1               SmbIrr(:idel(SmbIrr))//'4'
            else if(EqIgCase(Grp(ip:ip+1),'00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.89.and.NGrpPar.le.98) then
            ip=i2+1
            if(NGrp.lt.89) then
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'//
     1                 SmbIrr(:idel(SmbIrr))//'2'
              else
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'//
     1                 SmbIrr(:idel(SmbIrr))//'4'
              endif
            else
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              else if(EqIgCase(Grp(ip:ip),'s')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
              else if(EqIgCase(Grp(ip:ip+1),'-q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              else if(EqIgCase(Grp(ip:ip),'q')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
              endif
            endif
          else if(NGrpPar.ge.99.and.NGrpPar.le.110) then
            ip=i2+1
            if(NGrp.lt.99) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'5'
            else if(EqIgCase(Grp(ip:ip+2),'000').or.
     1              EqIgCase(Grp(ip:ip+4),'-q-qs')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'s0s').or.
     1              EqIgCase(Grp(ip:ip+3),'q-q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+2),'ss0').or.
     1              EqIgCase(Grp(ip:ip+2),'qqs')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'0ss').or.
     1              EqIgCase(Grp(ip:ip+3),'-qq0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
            go to 3000
          else if(NGrpPar.ge.111.and.NGrpPar.le.122) then
            ip=i2+1
            if(NGrp.lt.111) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'//
     1               SmbIrr(:idel(SmbIrr))//'4'
            else if(EqIgCase(Grp(ip:ip+2),'000').or.
     1              EqIgCase(Grp(ip:ip+3),'0-q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'00s').or.
     1              EqIgCase(Grp(ip:ip+2),'0q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.123.and.NGrpPar.le.142) then
            ip=i2+1
            if(NGrp.lt.123) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'5'
            else if(EqIgCase(Grp(ip:ip+3),'0000').or.
     1              EqIgCase(Grp(ip:ip+5),'-q0-qs')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+3),'s00s').or.
     1              EqIgCase(Grp(ip:ip+4),'q0-q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+3),'s0s0').or.
     1              EqIgCase(Grp(ip:ip+4),'-q0q0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+3),'00ss').or.
     1              EqIgCase(Grp(ip:ip+3),'q0qs')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
          endif
        else if(NGrpPar.ge.143.and.NGrpPar.le.167) then
          n=0
          do i=1,3
            if(abs(qu(i,1,1,KPhase)).lt..001) n=n+1
          enddo
          if(EqIgCase(Grp(i1+1:i2-1),'00g').or.
     1       (EqIgCase(Grp(i1+1:i2-1),'abg').and.n.ge.2)) then
            if(EqIgCase(GrpPar(1:1),'R')) then
              SmbIrr='LD'
            else
              SmbIrr='DT'
            endif
          else if(EqIgCase(Grp(i1+1:i2-1),'1/31/3g').or.
     1            EqIgCase(Grp(i1+1:i2-1),'abg')) then
            SmbIrr='P'
          else
            if(EqIgCase(GrpPar(1:1),'R')) then
              SmbIrr='LD'
            else
              SmbIrr='DT'
            endif
          endif
          if(NGrpPar.ge.143.and.NGrpPar.le.146) then
            ip=i2+1
            if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+1),'-t')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip),'t')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            endif
            go to 3000
          else if(NGrpPar.ge.147.and.NGrpPar.le.148) then
            if(NGrp.lt.147) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'//
     1               SmbIrr(:idel(SmbIrr))//'3'
            else
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
              go to 3000
            endif
          else if(NGrpPar.ge.149.and.NGrpPar.le.154) then
            ip=i2+1
            if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+3),'-t00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+2),'t00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            endif
          else if(NGrpPar.ge.155.and.NGrpPar.le.155) then
            ip=i2+1
            if(EqIgCase(Grp(ip:ip+1),'00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'-t0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+1),'t0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            endif
          else if(NGrpPar.ge.156.and.NGrpPar.le.159) then
            ip=i2+1
            if(NGrp.lt.156) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'0s0').or.
     1              EqIgCase(Grp(ip:ip+2),'00s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
            go to 3000
          else if(NGrpPar.ge.160.and.NGrpPar.le.161) then
            ip=i2+1
            if(NGrp.lt.160) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+1),'00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+1),'0s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
            go to 3000
          else if(NGrpPar.ge.162.and.NGrpPar.le.163) then
            ip=i2+1
            if(NGrp.lt.162) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'00s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.164.and.NGrpPar.le.165) then
            ip=i2+1
            if(NGrp.lt.164) then
              if(EqIgCase(SmbIrr(1:1),'P')) then
                SmbIrr='P2P3'
              else
                SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
              endif
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'0s0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.166.and.NGrpPar.le.167) then
            ip=i2+1
            if(NGrp.lt.166) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+1),'00')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+1),'0s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          endif
        else if(NGrpPar.ge.168.and.NGrpPar.le.194) then
          if(EqIgCase(Grp(i1+1:i2-1),'00g').or.
     1       EqIgCase(Grp(i1+1:i2-1),'abg').or.
     2       EqIgCase(Grp(i1+1:i2-1),'0b0')) then
            SmbIrr='DT'
          else
            go to 9000
          endif
          ip=i2+1
          if(NGrpPar.ge.168.and.NGrpPar.le.173.or.
     1       NGrpPar.ge.177.and.NGrpPar.le.182) then
            if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip),'s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip),'t')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip),'h')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            else if(EqIgCase(Grp(ip:ip+1),'-t')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'5'
            else if(EqIgCase(Grp(ip:ip+1),'-h')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'6'
            else
              go to 9000
            endif
            if(NGrpPar.lt.177) go to 3000
          else if(NGrpPar.ge.174.and.NGrpPar.le.174) then
            if(NGrp.lt.174) then
              SmbIrr='DT2DT3'
            else if(EqIgCase(Grp(ip:ip),'0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            endif
          else if(NGrpPar.ge.175.and.NGrpPar.le.176) then
            if(NGrp.lt.175) then
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr='DT3DT5'
              else
                SmbIrr='DT4DT6'
              endif
            else if(EqIgCase(Grp(ip:ip+1),'00')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+1),'s0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.183.and.NGrpPar.le.186) then
            if(NGrp.lt.183) then
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr='DT5'
              else
                SmbIrr='DT6'
              endif
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'0ss')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+2),'ss0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'s0s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
            go to 3000
          else if(NGrpPar.ge.187.and.NGrpPar.le.190) then
            if(NGrp.lt.187) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+2),'000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+2),'00s').or.
     1              EqIgCase(Grp(ip:ip+2),'0s0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            endif
          else if(NGrpPar.ge.191.and.NGrpPar.le.194) then
            if(NGrp.lt.191) then
              if(EqIgCase(Grp(ip:ip),'0')) then
                SmbIrr=SmbIrr(:idel(SmbIrr))//'5'
              else
                SmbIrr=SmbIrr(:idel(SmbIrr))//'6'
              endif
            else if(EqIgCase(Grp(ip:ip+3),'0000')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'1'
            else if(EqIgCase(Grp(ip:ip+3),'00ss')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'2'
            else if(EqIgCase(Grp(ip:ip+3),'s00s')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'3'
            else if(EqIgCase(Grp(ip:ip+3),'s0s0')) then
              SmbIrr=SmbIrr(:idel(SmbIrr))//'4'
            endif
          endif
        else
          go to 9000
        endif
      endif
      go to 9900
3000  SmbIrr=SmbIrr(:idel(SmbIrr))//SmbIrr(:idel(SmbIrr))
      go to 9900
9000  ich=1
      go to 9999
9800  SmbIrr='GM'//SmbIrr(:idel(SmbIrr))
9900  SmbIrr='m'//SmbIrr(:idel(SmbIrr))
9999  return
      end
      subroutine RAKontrola(Klic,SelGenLG)
      use Basic_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer SelGenLG(*)
      real rmp(9),sp(3),xp(1)
      complex RIrrMult(64),pomc
      logical EqRV,EqCV
      do n=1,NIrrep
        nd=NDimIrrep(n)
        do i=1,NSymmLG
          is=SelGenLG(i)
          do 1100j=1,NSymmLG
            js=SelGenLG(j)
            call MultSymmOp(rmr(1,is),s6r(1,is),rmr(1,js),s6r(1,js),
     1                      rmp,sp,3)
            do k=1,NSymmLG
              ks=SelGenLG(k)
              if(EqRV(rmp,rmr(1,ks),9,.01)) then
                call MultmC(RIrrep(1,is,n),RIrrep(1,js,n),
     1                      RIrrMult,nd,nd,nd)
                do m=1,3
                  sp(m)=s6r(m,ks)-sp(m)
                enddo
                call qbyx(sp,xp,1)
                Delta=pi2*xp(1)
                pomc=cmplx(cos(Delta),sin(Delta))
                m=0
                do ii=1,nd
                  do jj=1,nd
                    m=m+1
                    if(Klic.eq.0.or.jj.le.nd/2) then
                      RIrrMult(m)=RIrrMult(m)*pomc
                    else
                      RIrrMult(m)=RIrrMult(m)*conjg(pomc)
                    endif
                  enddo
                enddo
                if(.not.EqCV(RIrrMult,RIrrep(1,ks,n),nd**2,.001)) then
                  call FeChybne(-1.,-1.,
     1                          'the set of irrep matrices does not '//
     2                          'follow group multiplication table.',
     3                          'Please contact authors.',SeriousError)
                  go to 9999
                endif
                go to 1100
              endif
            enddo
1100      continue
        enddo
      enddo
9999  return
      end
      subroutine RASetCommen
      include 'fepc.cmn'
      include 'basic.cmn'
      logical EqIgCase
      call UnitMat(RCommen(1,1,KPhase),3)
      is=0
      ip=0
      in=0
      do i=1,3
        pom=QMag(i,1,KPhase)*2.
        if(abs(pom-anint(pom)).gt..0001) then
          k=0
        else
          k=mod(nint(QMag(i,1,KPhase)*2.),2)
        endif
        if(k.eq.1) then
          is=is+1
          ip=i
        else
          in=i
        endif
      enddo
      if(is.eq.1) then
        RCommen(ip+(ip-1)*3,1,KPhase)=2.
      else if(is.eq.2) then
        if(in.eq.0) in=3
        i1=mod(in,3)+1
        i2=mod(in+1,3)+1
        RCommen(i1+(i2-1)*3,1,KPhase)= 1.
        RCommen(i2+(i1-1)*3,1,KPhase)=-1.
      else if(is.eq.3) then
        if(EqIgCase(Lattice(KPhase),'P')) then
          RCommen(1,1,KPhase)= 1.
          RCommen(2,1,KPhase)=-1.
          RCommen(3,1,KPhase)= 0.
          RCommen(4,1,KPhase)= 0.
          RCommen(5,1,KPhase)= 1.
          RCommen(6,1,KPhase)=-1.
          RCommen(7,1,KPhase)= 2.
          RCommen(8,1,KPhase)= 2.
          RCommen(9,1,KPhase)= 2.
        else if(EqIgCase(Lattice(KPhase),'F')) then
          RCommen(1,1,KPhase)= .5
          RCommen(2,1,KPhase)=-.5
          RCommen(3,1,KPhase)= 0.
          RCommen(4,1,KPhase)= 0.
          RCommen(5,1,KPhase)= .5
          RCommen(6,1,KPhase)=-.5
          RCommen(7,1,KPhase)= 2.
          RCommen(8,1,KPhase)= 2.
          RCommen(9,1,KPhase)= 2.
        else if(EqIgCase(Lattice(KPhase),'I')) then
          RCommen(1,1,KPhase)= 2.
          RCommen(2,1,KPhase)= 0.
          RCommen(3,1,KPhase)= 0.
          RCommen(4,1,KPhase)= 0.
          RCommen(5,1,KPhase)= 2.
          RCommen(6,1,KPhase)= 0.
          RCommen(7,1,KPhase)= 0.
          RCommen(8,1,KPhase)= 0.
          RCommen(9,1,KPhase)= 2.
!        else if(EqIgCase(Lattice(KPhase),'C')) then
!          RCommen(1,1,KPhase)= 2.
!          RCommen(2,1,KPhase)= -1.
!          RCommen(3,1,KPhase)= 0.
!          RCommen(4,1,KPhase)= 0.
!          RCommen(5,1,KPhase)= 1.
!          RCommen(6,1,KPhase)= 0.
!          RCommen(7,1,KPhase)= 0.
!          RCommen(8,1,KPhase)= 0.
!          RCommen(9,1,KPhase)= 2.
        else
          RCommen(1,1,KPhase)= 2.
          RCommen(2,1,KPhase)= 0.
          RCommen(3,1,KPhase)= 0.
          RCommen(4,1,KPhase)= -1.
          RCommen(5,1,KPhase)= 1.
          RCommen(6,1,KPhase)= 0.
          RCommen(7,1,KPhase)=-1.
          RCommen(8,1,KPhase)= 0.
          RCommen(9,1,KPhase)= 1.
        endif
      endif
      trez(1,1,KPhase)=0.
      KCommen(KPhase)=1
      KCommenMax=1
      ICommen(KPhase)=1
      call SetCommen(1,NDimI(KPhase))
      return
      end
      subroutine RAIsodistort
      use Atoms_mod
      use RepAnal_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm9.cmn'
      include 'refine.cmn'
      character*256 :: Veta,Filter='*.cif *.mcif *.magcif *.txt',
     1                 FileName
      character*256, allocatable :: FileNameArr(:),FileNameArrO(:)
      character*128 ListDir,ListFile
      character*80,  allocatable :: GrpArr(:),BasArr(:),OriginArr(:),
     1                              IrepArr(:),
     2                              GrpArrO(:),BasArrO(:),OriginArrO(:),
     3                              IrepArrO(:),
     4                              StMag(:,:,:)
      character*80 t80,FlnPom,FlnOld,FlnOrg
      character*8 SvFile
      character*8, allocatable :: AtMag(:)
      integer SbwLnQuest,ip(9),SbwItemPointerQuest,FeChdir
      integer, allocatable :: ipor(:),NGrp(:),iporO(:),NGrpO(:)
      logical StructureExists,FeYesNo,ExistFile,EqIgCase
      real QAct(3,3),QPom(3),TrMatP(9)
      real, allocatable :: TrMatArr(:,:,:),TrMatArrO(:,:,:)
      FromRepAnalysis=.true.
      FromGoToSub=.false.
      LstOpened=.false.
      FlnOrg=Fln
      call SSG2QMag(fln)
      NQMagIn=NQMag(KPhase)
      if(NQMagIn.gt.0) then
        ISymmFull=0
        call CrlStoreSymmetry(ISymmFull)
      endif
      call iom40Only(0,0,fln(:ifln)//'.m40')
      Veta=fln(:ifln)//'.cif'
      MagneticType(KPhase)=0
      call CIFMakeBasicTemplate(Veta,0)
      call CIFUpdate(Veta,ParentStructure,0)
      MagneticType(KPhase)=1
      call QMag2SSG(Fln,0)
      call iom40Only(0,0,fln(:ifln)//'.m44')
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call FeTmpFilesAdd(ListDir)
      ListFile='jfile'
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListFile)
      id=NextQuestId()
      Veta='ISODISTORT => Jana2006'
      Id=NextQuestId()
      xqd=600.
      ilm=17
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,Veta,0,LightGray,-1,-1)
      QPul=.true.
      do j=1,NQMagIn
        do i=1,3
          pom=QMag(i,j,KPhase)*2.
          if(abs(anint(pom)-pom).gt..00001) then
            QPul=.false.
            exit
          endif
        enddo
      enddo
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      call FeTabsAdd(140.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(350.,UseTabs,IdRightTab,' ')
      call FeTabsAdd(490.,UseTabs,IdRightTab,' ')
      xpom=15.
      dpom=xqd-30.-SbwPruhXd
      il=1
      if(NDimI(KPhase).gt.0.and..not.QPul) then
        Cislo='superspace'
        Fract=3.
      else
        Cislo='space'
        Fract=3.
      endif
      tpom=xpom+5.
      Veta='Shubnikov '//Cislo(:idel(Cislo))//' group'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+180
      Veta='Axes'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+160.
      Veta='Origin shift'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+120.
      Veta='Representation'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+110.
      il=ilm-4
      call FeQuestSbwMake(id,xpom,il,dpom,il-1,1,CutTextFromLeft,
     1                    SbwVertical)
      nSbwSubgroups=SbwLastMade
      il=il+1
      Veta='Run %ISODISTORT'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtIsodistort=ButtonLastMade
      Veta='%Refresh the list'
      xpom=xpom+dpom+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRefresh=ButtonLastMade
      il=il+1
      Veta='%Start graphic simulation'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=xqd*.5-1.5*dpom-15.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          nButtGraphic=ButtonLastMade
          Veta='S%tart profile simulation'
        else if(i.eq.2) then
          nButtPowder=ButtonLastMade
          Veta='Sho%w details'
        else if(i.eq.3) then
          nButtDetails=ButtonLastMade
        endif
        if(i.eq.2.and..not.isPowder)
     1    call FeQuestButtonDisable(ButtonLastMade)
        xpom=xpom+dpom+20.
      enddo
      il=il+1
      Veta='%Continue with the selected isotropic subgroup'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRun=ButtonLastMade
      xpom=xpom+dpom+15.
      Veta='%Quit the ISODISTORT tool'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtQuit=ButtonLastMade
      nMagCIFMax=10
      if(allocated(FileNameArr)) deallocate(FileNameArr,GrpArr,BasArr,
     1                                      OriginArr,IrepArr,NGrp,ipor,
     2                                      TrMatArr)
      allocate(FileNameArr(nMagCIFMax),GrpArr(nMagCIFMax),
     1         BasArr(nMagCIFMax),OriginArr(nMagCIFMax),
     2         IRepArr(nMagCIFMax),NGrp(nMagCIFMax),ipor(nMagCIFMax),
     3         TrMatArr(3,3,nMagCIFMax))
1100  nMagCIF=0
      call FeDir(ListDir,ListFile,Filter,0)
      ln=NextLogicNumber()
      call OpenFile(ln,ListFile,'formatted','old')
      if(ErrFlag.ne.0) go to 1500
      lno=NextLogicNumber()
      call OpenFile(lno,fln(:ifln)//'_magCIF.tmp','formatted',
     1              'unknown')
      if(ErrFlag.ne.0) go to 1500
      lni=NextLogicNumber()
1110  read(ln,FormA,end=1200,err=1200) FileName
      call OpenFile(lni,FileName,'formatted','unknown')
1120  read(lni,FormA,end=1160,err=1160) Veta
      if(LocateSubstring(Veta,'ISODISTORT',.false.,.true.).gt.0) then
        if(nMagCIF.ge.nMagCIFMax) then
          allocate(FileNameArrO(nMagCIF),GrpArrO(nMagCIF),
     1             BasArrO(nMagCIF),OriginArrO(nMagCIF),
     2             IRepArrO(nMagCIF),NGrpO(nMagCIF),iporO(nMagCIF),
     3             TrMatArrO(3,3,nMagCIF))
          FileNameArrO(1:nMagCIF)=FileNameArr(1:nMagCIF)
          GrpArrO(1:nMagCIF)=GrpArr(1:nMagCIF)
          BasArrO(1:nMagCIF)=BasArr(1:nMagCIF)
          OriginArrO(1:nMagCIF)=OriginArr(1:nMagCIF)
          IrepArrO(1:nMagCIF)=IRepArr(1:nMagCIF)
          NGrpO(1:nMagCIF)=NGrp(1:nMagCIF)
          iporO(1:nMagCIF)=ipor(1:nMagCIF)
          TrMatArrO(1:3,1:3,1:nMagCIF)=TrMatArr(1:3,1:3,1:nMagCIF)
          deallocate(FileNameArr,GrpArr,BasArr,
     1               OriginArr,IrepArr,NGrp,ipor,TrMatArr)
          n=nMagCIFMax+10
          allocate(FileNameArr(n),GrpArr(n),BasArr(n),OriginArr(n),
     1             IRepArr(n),NGrp(n),ipor(n),TrMatArr(3,3,n))
          FileNameArr(1:nMagCIF)=FileNameArrO(1:nMagCIF)
          GrpArr(1:nMagCIF)=GrpArrO(1:nMagCIF)
          BasArr(1:nMagCIF)=BasArrO(1:nMagCIF)
          OriginArr(1:nMagCIF)=OriginArrO(1:nMagCIF)
          IrepArr(1:nMagCIF)=IRepArrO(1:nMagCIF)
          NGrp(1:nMagCIF)=NGrpO(1:nMagCIF)
          ipor(1:nMagCIF)=iporO(1:nMagCIF)
          TrMatArr(1:3,1:3,1:nMagCIF)=TrMatArrO(1:3,1:3,1:nMagCIF)
          deallocate(FileNameArrO,GrpArrO,BasArrO,OriginArrO,IrepArrO,
     1               NGrpO,iporO,TrMatArrO)
          nMagCIFMax=nMagCIF+10
        endif
        nMagCIF=nMagCIF+1
        FileNameArr(nMagCIF)=FileName
        BasArr(nMagCIF)='(1,0,0|0,1,0|0,0,1)'
        GrpArr(nMagCIF)='???'
        OriginArr(nMagCIF)='(0,0,0)'
        IRepArr(nMagCIF)='???'
        NGrp(nMagCIF)=0
        ipor(nMagCIF)=0
        call UnitMat(TrMatArr(1,1,nMagCIF),3)
        n=0
1130    read(lni,FormA,end=1160,err=1160) Veta
        if(LocateSubstring(Veta,'IR:',.false.,.true.).gt.0) then
          n=n+1
          k=0
          do i=1,3
            call Kus(Veta,k,IRepArr(nMagCIF))
          enddo
          i=index(IRepArr(nMagCIF),',')
          if(i.gt.0) IRepArr(nMagCIF)(i:i)=' '
        endif
        if(LocateSubstring(Veta,'basis=',.false.,.true.).gt.0) then
          n=n+10
          k=0
          do i=1,4
            call Kus(Veta,k,t80)
          enddo
          nn=1
          do i=1,idel(t80)
            if(t80(i:i).eq.'.') then
              t80(i:i)=' '
              nn=nn+1
            endif
          enddo
          kk=0
          call StToInt(t80,kk,ip,nn,.false.,ich)
          if(ich.ne.0) then
            NGrp(nMagCIF)=0
          else
            m=0
            do i=1,nn
              m=m*1000+ip(i)
            enddo
            NGrp(nMagCIF)=-m
          endif
          call Kus(Veta,k,GrpArr(nMagCIF))
          if(GrpArr(nMagCIF)(2:2).eq.'_')
     1      GrpArr(nMagCIF)=GrpArr(nMagCIF)(1:1)//'['//
     2                      GrpArr(nMagCIF)(3:3)//']'//
     3                      GrpArr(nMagCIF)(4:idel(GrpArr(nMagCIF)))
          idl=idel(GrpArr(nMagCIF))
          if(GrpArr(nMagCIF)(idl:idl).eq.',') then
            GrpArr(nMagCIF)(idl:idl)=' '
            idl=idl-1
          endif
          t80=GrpArr(nMagCIF)
          GrpArr(nMagCIF)=' '
          j=0
          do i=1,idl
            if(t80(i:i).eq.'_') cycle
            j=j+1
            GrpArr(nMagCIF)(j:j)=t80(i:i)
          enddo
          call Kus(Veta,k,BasArr(nMagCIF))
          i1=index(BasArr(nMagCIF),'{')
          i1=max(i1+1,1)
          i2=index(BasArr(nMagCIF),'}')-1
          if(i2.le.0) i2=idel(BasArr(nMagCIF))
          BasArr(nMagCIF)=BasArr(nMagCIF)(i1:i2)
          t80=BasArr(nMagCIF)
          idl=idel(t80)
          BasArr(nMagCIF)='('
          j=1
          do i=2,idl-1
            if(t80(i:i).eq.')') then
              j=j+1
              BasArr(nMagCIF)(j:j)='|'
            else if(t80(i:i).eq.'(') then
              j=j-1
              cycle
            else
              j=j+1
              BasArr(nMagCIF)(j:j)=t80(i:i)
            endif
          enddo
          idl=idel(BasArr(nMagCIF))+1
          BasArr(nMagCIF)(idl:idl)=')'
          do i=1,idel(t80)
            if(t80(i:i).eq.')'.or.t80(i:i).eq.'('.or.
     1         t80(i:i).eq.',') then
              t80(i:i)=' '
            endif
          enddo
          kk=0
          call StToReal(t80,kk,TrMatP,9,.false.,ich)
          if(ich.eq.0) call CopyMat(TrMatP,TrMatArr(1,1,nMagCIF),3)
          call Kus(Veta,k,OriginArr(nMagCIF))
          i1=index(OriginArr(nMagCIF),'(')
          i1=max(i1,1)
          i2=index(OriginArr(nMagCIF),')')
          OriginArr(nMagCIF)=OriginArr(nMagCIF)(i1:i2)
          do i=1,4
            call Kus(Veta,k,t80)
          enddo
          nn=0
          do i=1,idel(t80)
            if(t80(i:i).eq.'('.or.t80(i:i).eq.',') then
              t80(i:i)=' '
            else if(t80(i:i).eq.')') then
              nn=nn+1
              t80(i:i)=' '
            endif
          enddo
          if(nn.lt.NQMagIn) go to 1160
          if(NQMagIn.gt.0) then
            i1p=-2
            i1k= 2
          else
            i1p= 0
            i1k= 0
          endif
          if(NQMagIn.gt.1) then
            i2p=-2
            i2k= 2
          else
            i2p= 0
            i2k= 0
          endif
          if(NQMagIn.gt.2) then
            i3p=-2
            i3k= 2
          else
            i3p= 0
            i3k= 0
          endif
          k=0
          do i=1,nn
            call StToReal(t80,k,QAct(1,i),3,.false.,ich)
            if(ich.ne.0) go to 1160
            do i3=i3p,i3k
              do i2=i2p,i2k
                do i1=i1p,i1k
                  do m=1,3
                    QPom(m)=float(i1)*QMag(m,1,KPhase)
                  enddo
                  if(NQMagIn.gt.1) then
                    do m=1,3
                      QPom(m)=QPom(m)+float(i2)*QMag(m,2,KPhase)
                    enddo
                    if(NQMagIn.gt.2) then
                      do m=1,3
                        QPom(m)=QPom(m)+float(i3)*QMag(m,3,KPhase)
                      enddo
                    endif
                  endif
                  do m=1,3
                    pom=QPom(m)-QAct(m,i)
                    if(abs(pom-anint(pom)).gt..001) go to 1140
                  enddo
                  go to 1150
1140            enddo
              enddo
            enddo
          enddo
          go to 1160
        endif
1150    if(n.lt.11) go to 1130
      endif
      do i=1,nMagCIF-1
        if(EqIgCase(GrpArr(i),GrpArr(nMagCIF)).and.
     1     EqIgCase(BasArr(i),BasArr(nMagCIF)).and.
     2     EqIgCase(OriginArr(i),OriginArr(nMagCIF)).and.
     3     EqIgCase(IRepArr(i),IRepArr(nMagCIF))) go to 1160
      enddo
      go to 1170
1160  nMagCIF=nMagCIF-1
1170  call CloseIfOpened(lni)
      go to 1110
1200  call Indexx(nMagCIF,NGrp,ipor)
      do j=1,nMagCIF
        i=ipor(j)
        write(lno,'(4(a,a1))') GrpArr(i)(:idel(GrpArr(i))),Tabulator,
     1                         BasArr(i)(:idel(BasArr(i))),Tabulator,
     2                   OriginArr(i)(:idel(OriginArr(i))),Tabulator,
     3                       IRepArr(i)(:idel(IRepArr(i))),Tabulator
      enddo
      call CloseIfOpened(ln)
      call CloseIfOpened(lno)
      call FeQuestSbwSelectOpen(nSbwSubgroups,fln(:ifln)//'_magCIF.tmp')
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtIsodistort)
     1  then
        Veta='http://stokes.byu.edu/isodistort.html'
        call FeShellExecute(Veta)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRefresh)
     1  then
        call CloseIfOpened(SbwLnQuest(nSbwSubgroups))
        go to 1100
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtQuit.or.CheckNumber.eq.nButtRun.or.
     2         CheckNumber.eq.nButtPowder.or.
     3         CheckNumber.eq.nButtGraphic.or.
     4         CheckNumber.eq.nButtDetails))then
        if(CheckNumber.eq.nButtQuit) then
          Fln=FlnOrg
          iFln=idel(Fln)
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
        else
          FlnOld=Fln
          n=0
2000      n=n+1
          write(Cislo,'(i2)') n
          if(Cislo(1:1).eq.' ') Cislo(1:1)='0'
          FlnPom=Fln(:ifln)//'_'//Cislo(1:2)
          if(StructureExists(FlnPom)) go to 2000
          if(CheckNumber.ne.nButtRun) go to 2110
2100      call FeFileManager('Select structure name',FlnPom,' ',1,
     1                       .true.,ich)
          if(ich.ne.0) go to 1500
          if(StructureExists(FlnPom)) then
            call ExtractFileName(FlnPom,Veta)
            if(.not.FeYesNo(-1.,-1.,'The structure "'//
     1         Veta(:idel(Veta))//'" already exists, rewrite it?',
     2         0)) go to 2100
          endif
2110      if(ExistFile(fln(:ifln)//'.m41'))
     1      call CopyFile(fln(:ifln)//'.m41',
     2                    FlnPom(:idel(FlnPom))//'.m41')
          ISel=ipor(SbwItemPointerQuest(nSbwSubgroups))
          if(QPul.and.NQMagIn.gt.0) then
            call UnitMat(RCommen(1,1,KPhase),3)
            do i=1,3
              k=0
              do j=1,3
                if(abs(TrMatArr(i,j,ISel)).le.0) cycle
                k=k+1
                ip(k)=nint(TrMatArr(i,j,ISel))
              enddo
            enddo
            call MinMultMaxFract(ip,k,MinMult,MaxFract)
            do i=1,9,4
              RCommen(i,1,KPhase)=MinMult
            enddo
            trez(1:3,1,KPhase)=0.
            KCommen(KPhase)=1
            KCommenMax=1
            ICommen(KPhase)=1
            call SetCommen(1,NDimI(KPhase))
            NQMag(KPhase)=0
            ParentStructure=.false.
            ParentM90=fln(:ifln)//'.m90'
            call TrSuperMag(0,FlnPom)
          else
            i=idel(FlnPom)
            call CopyFile(fln(:ifln)//'.m40',FlnPom(:i)//'.m40')
            call CopyFile(fln(:ifln)//'.m50',FlnPom(:i)//'.m50')
            ExistM90=ExistFile(fln(:ifln)//'.m90')
            ExistM95=ExistFile(fln(:ifln)//'.m95')
            if(ExistM95)
     1        call CopyFile(fln(:ifln)//'.m95',FlnPom(:i)//'.m95')
            if(ExistM90)
     1        call CopyFile(fln(:ifln)//'.m90',FlnPom(:i)//'.m90')
          endif
          call DeletePomFiles
          call ExtractDirectory(FlnPom,Veta)
          i=FeChdir(Veta)
          call FeGetCurrentDir
          call ExtractFileName(FlnPom,Fln)
          ifln=idel(fln)
          call iom50(0,0,fln(:ifln)//'.m50')
          if(QPul.and.NQMagIn.gt.0) then
            call CrlRestoreSymmetry(ISymmFull)
            call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
          endif
          call RAImportFromIsodistort(FileNameArr(ISel))
          call iom50(0,0,fln(:ifln)//'.m50')
          call iom40(0,0,fln(:ifln)//'.m40')
          call CrlOrderMagSymmetry
          if(CheckNumber.eq.nButtGraphic) then
            call OpenFile(lst,fln(:ifln)//'.rep','formatted','unknown')
            LstOpened=.true.
            do i=1,NAtCalc
              if(kswa(i).ne.KPhase) cycle
              if(MagPar(i).ne.0)
     1          call CrlMagTesterSetAtom(i,2.,.false.)
              neq=0
              call AtSpec(i,i,0,0,0)
            enddo
            close(lst,status='delete')
            LstOpened=.false.
            MakeCIFForGraphicViewer=.true.
            call iom50(1,0,fln(:ifln)//'.m50')
            call iom40Only(1,0,fln(:ifln)//'.m40')
            call CrlPlotStructure
            MakeCIFForGraphicViewer=.false.
            go to 3000
          else if(CheckNumber.eq.nButtDetails.or.
     1            CheckNumber.eq.nButtPowder) then
            if(OpSystem.ge.0) then
              SvFile='full.pcx'
            else
              SvFile='full.bmp'
            endif
            NAtMag=0
            do i=1,NAtAll
              if(MagPar(i).gt.0) NAtMag=NAtMag+1
            enddo
            if(allocated(AtMag)) deallocate(AtMag,StMag)
            allocate(AtMag(NAtMag),StMag(6,0:MagParMax-1,NAtMag))
            j=0
            do i=1,NAtAll
              if(MagPar(i).gt.0) then
                j=j+1
                AtMag(j)=Atom(i)
              endif
            enddo
            if(CheckNumber.eq.nButtDetails) then
              i=1
            else
              i=0
            endif
            call CrlMagTesterAction(i,SvFile,AtMag,StMag,NAtMag,
     1                              MagParMax-1)
            go to 3000
          else
            call RefOpenCommands
            lni=NextLogicNumber()
            open(lni,file=fln(:ifln)//'_fixed.tmp')
            lno=NextLogicNumber()
            open(lno,file=fln(:ifln)//'_fixed_new.tmp')
2200        read(lni,FormA,end=2220) Veta
            write(lno,FormA) Veta(:idel(Veta))
            go to 2200
2220        call CloseIfOpened(lni)
            Veta='fixed xyz *'
            write(lno,FormA) Veta(:idel(Veta))
            Veta='restric *'
            if(NDimI(KPhase).gt.0) then
              Cislo=' 13'
            else
              Cislo=' 12'
            endif
            write(lno,FormA) Veta(:idel(Veta))//Cislo(:3)
            close(lno)
            call MoveFile(fln(:ifln)//'_fixed_new.tmp',
     1                    fln(:ifln)//'_fixed.tmp')
            NacetlInt(nCmdncykl)=100
            NacetlReal(nCmdtlum)=.1
            NacetlInt(nCmdLSMethod)=2
            NacetlReal(nCmdMarqLam)=.001
            call RefRewriteCommands(1)
            call iom50(0,0,fln(:ifln)//'.m50')
            call iom40(0,0,fln(:ifln)//'.m40')
            do i=1,NAtCalc
              if(kswa(i).ne.KPhase) cycle
              if(MagPar(i).ne.0)
     1          call CrlMagTesterSetAtom(i,.1,.false.)
            enddo
            call iom40(1,0,fln(:ifln)//'.m40')
            call iom40(0,0,fln(:ifln)//'.m40')
          endif
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 3100
3000  call DeleteAllFiles(Fln(:iFln)//'*')
      Fln=FlnOld
      iFln=idel(Fln)
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      go to 1500
3100  call FeQuestRemove(id)
      call DeleteFile(ListDir)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListDir)
      call FeTmpFilesClear(ListFile)
      if(allocated(FileNameArr)) deallocate(FileNameArr,GrpArr,BasArr,
     1                                      OriginArr,IrepArr,NGrp,ipor,
     2                                      TrMatArr)
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      if(ISymmFull.gt.0) call CrlCleanSymmetry
      return
      end
      subroutine RAImportFromIsodistort(FileNameIn)
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*(*) FileNameIn
      character*80 Veta,FileIn
      character*60 GrupaOrg,GrupaImp,GrupaTr
      character*1   :: IndicesCapital(6) = (/'H','K','L','M','N','P'/)
      character*6 :: CellParOrgC(3) = (/'a(org)','b(org)','c(org)'/)
      character*6 :: CellParImpC(3) = (/'a(imp)','b(imp)','c(imp)'/)
      character*7, allocatable :: AtTypeMagO(:),AtTypeMagJO(:)
      dimension CellTr(6),CellImp(6),QuImp(3,3),TrM3(9),TrM3I(9),
     1          TrM(36),TrP(36),xp(36),xpp(36)
      integer CrwStateQuest
      logical UseOriginalCell,CrwLogicQuest,IdenticalSymmetry,EqRV,
     1        MatRealEqUnitMat,ExistFile,WasPowder
      integer, allocatable :: ips(:),itrl(:),TypeFFMagO(:)
      real, allocatable :: RmOld(:,:),RmNew(:,:),FFnO(:),FFniO(:),
     1                     FFMagO(:,:)
      WasPowder=IsPowder
      NDimOrg=NDim(KPhase)
      GrupaOrg=Grupa(KPhase)
      call CopyVek(CellPar(1,1,KPhase),CellParOrg,6)
      call CopyVek(Qu(1,1,1,KPhase),QuOrg,3*NDimI(KPhase))
      if(allocated(RmOld)) deallocate(RmOld,ips,itrl)
      allocate(RmOld(9,NSymm(KPhase)),ips(NSymm(KPhase)),
     1         itrl(NSymm(KPhase)))
      NSymmOld=0
      do i=1,NSymm(KPhase)
        call MatBlock3(Rm6(1,i,1,1),xp,NDim(KPhase))
        do j=1,NSymmOld
          if(EqRV(xp,RmOld(1,j),9,.001)) go to 1100
        enddo
        NSymmOld=NSymmOld+1
        RmOld(1:9,NSymmOld)=xp(1:9)
1100  enddo
      if(allocated(AtTypeMagO))
     1  deallocate(AtTypeMagO,AtTypeMagJO,TypeFFMagO,FFnO,FFniO,FFMagO)
      nAtType=NAtFormula(KPhase)
      allocate(AtTypeMagO(nAtType),AtTypeMagJO(nAtType),
     1         TypeFFMagO(nAtType),FFnO(nAtType),FFniO(nAtType),
     2         FFMagO(7,nAtType))
      do i=1,nAtType
        AtTypeMagO(i)=AtTypeMag(i,KPhase)
        AtTypeMagJO(i)=AtTypeMagJ(i,KPhase)
        TypeFFMagO(i)=TypeFFMag(i,KPhase)
        FFnO(i)=FFn(i,KPhase)
        FFniO(i)=FFni(i,KPhase)
        FFMagO(1:7,i)=FFMag(1:7,i,KPhase)
      enddo
1120  if(FileNameIn.eq.' ') then
        FileIn=fln(:ifln)//'.cif'
        call FeFileManager('Select input CIF file',FileIn,
     1                   '*.cif *.mcif',0,.true.,ich)
        if(ich.ne.0) go to 9000
      else
        FileIn=FileNameIn
      endif
      if(.not.ExistFile(FileIn)) then
        if(FileNameIn.eq.' ') then
          Veta='Try it again.'
        else
          Veta=' '
        endif
        call FeChybne(-1.,YBottomMessage,'the file "'//
     1                FileIn(:idel(FileIn))//'" does not exist.',Veta,
     2                SeriousError)
        if(FileNameIn.eq.' ') then
          go to 1120
        else
          go to 9000
        endif
      endif
      call ReadSpecifiedCIF(FileIn,5)
      if(ErrFlag.ne.0) go to 9000
      do i=1,nAtType
        AtTypeMag(i,KPhase)=AtTypeMagO(i)
        AtTypeMagJ(i,KPhase)=AtTypeMagJO(i)
        TypeFFMag(i,KPhase)=TypeFFMagO(i)
        FFn(i,KPhase)=FFnO(i)
        FFni(i,KPhase)=FFniO(i)
        FFMag(1:7,i,KPhase)=FFMagO(1:7,i)
      enddo
      IsPowder=WasPowder
      if(NDim(KPhase).ne.3.and.NDim(KPhase).ne.NDimOrg) then
        call FeChybne(-1.,-1.,'the model is neither regular nor '//
     1                'modulated with the same dimensionality.',' ',
     2                SeriousError)
        go to 9000
      endif
!   nacteni matice transformace, kdyz ne tak
      call FindCellTrans(CellPar(1,1,KPhase),CellParOrg,TrM3,ich)
      if(ich.ne.0) then
        call FeFillTextInfo('importmodel.txt',0)
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        call UnitMat(TrM3,3)
      endif
      call CopyVek(CellPar(1,1,KPhase),CellImp,6)
      call CopyVek(Qu(1,1,1,KPhase),QuImp,3*NDimI(KPhase))
      call UnitMat(TrP,NDimOrg)
      if(allocated(RmNew)) deallocate(RmNew)
      allocate(RmNew(9,NSymm(KPhase)))
      NSymmNew=0
      do i=1,NSymm(KPhase)
        call MatBlock3(Rm6(1,i,1,1),xp,NDim(KPhase))
        do j=1,NSymmNew
          if(EqRV(xp,RmNew(1,j),9,.001)) go to 1200
        enddo
        NSymmNew=NSymmNew+1
        RmNew(1:9,NSymmNew)=xp(1:9)
1200  enddo
      if(NDimOrg.gt.3) then
        call MatFromBlock3(TrM3,TrM,NDimOrg)
      else
        call CopyMat(TrM3,TrM,NDimOrg)
      endif
      call CopyVek(CellParOrg,CellPar(1,1,KPhase),6)
      call CopyVek(QuOrg,Qu(1,1,1,KPhase),3*(NDimOrg-3))
      call MatInv(TrM3,TrP,det,3)
      call SetRealArrayTo(xp,9,0.)
      call UnitMat(xpp,NDim(KPhase))
      if(NDimOrg.gt.3) then
        call DRMatTrCellQ(TrP,CellPar(1,1,KPhase),Qu(1,1,1,KPhase),xp,
     1                    xpp)
      else
        call DRMatTrCell(TrP,CellPar(1,1,KPhase),xpp)
      endif
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      call FeEdit(fln(:ifln)//'.m50')
      do ia=1,NAtInd
        if(kswa(ia).ne.KPhase) cycle
        if(AtTypeMag(isf(ia),NPhase).eq.' ') then
          MagParP=0
        else
          MagParP=1
        endif
        call ZmMag(ia,MagParP)
      enddo
      call iom40Only(1,0,fln(:ifln)//'.m40')
      call iom40Only(0,0,fln(:ifln)//'.m40')
      call FeEdit(fln(:ifln)//'.m40')
      call FeEdit(fln(:ifln)//'.m50')
      if(IsPowder) then
        call iom90(0,fln(:ifln)//'.m90')
        call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        call iom41(0,0,fln(:ifln)//'.m41')
        call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
        call CopyVek(Qu(1,1,1,KPhase),QuPwd(1,1,KPhase),3*NDimI(KPhase))
        call iom41(1,0,fln(:ifln)//'.m41')
        call iom41(0,0,fln(:ifln)//'.m41')
      else
        if(NSymmOld.gt.NSymmNew) then
          call MatInv(TrM3,TrM3I,VolRatio,3)
          do i=1,NSymmOld
            call multm(TrM3,RmOld(1,i),xp,3,3,3)
            call multm(xp,TrM3I,RmOld(1,i),3,3,3)
          enddo
          ips=0
          n=0
          do i=1,NSymmOld
            do j=1,NSymmNew
              if(EqRV(RmOld(1,i),RmNew(1,j),9,.001)) then
                n=n+1
                ips(i)=1
                exit
              endif
            enddo
          enddo
          n=1
          do i=1,NSymmOld
            if(ips(i).ne.0) cycle
            n=n+1
            do 2650j=1,NSymmOld
              if(ips(j).eq.1) then
                call MultM(RmOld(1,i),RmOld(1,j),xp,3,3,3)
                do k=1,NSymmOld
                  if(ips(k).eq.0.and.eqrv(RmOld(1,k),xp,9,.001)) then
                    ips(k)=n
                    itrl(n)=k
                    go to 2650
                  endif
                enddo
              endif
2650        continue
          enddo
          NTwin=n
          pom=1./float(NTwin)
          do i=2,NTwin
            rtw(1:9,i)=RmOld(1:9,itrl(i))
            sctw(i,KDatBlock)=pom
          enddo
          mxscutw=max(mxscu+NTwin-1,6)
          call iom40Only(1,0,fln(:ifln)//'.m40')
          call iom40Only(0,0,fln(:ifln)//'.m40')
          call iom50(1,0,fln(:ifln)//'.m50')
          call iom50(0,0,fln(:ifln)//'.m50')
        else if(NSymmOld.eq.NSymmNew) then
          NTwin=1
        else
          call FeChybne(-1.,-1.,'inconsistency: the number of '//
     1                  'symmetry operations of the parent phase '//
     2                  'larger than that for the subgroup.',' ',
     3                  SeriousError)
          go to 9000
        endif
      endif
      if(ExistM95) then
        SilentRun=.true.
        call MatInv(TrM,TrP,det,NDim(KPhase))
        call Multm(TrMP,TrP,TrM,NDim(KPhase),NDim(KPhase),NDim(KPhase))
        call CopyMat(TrM,TrMP,NDim(KPhase))
        call CompleteM95(0)
        ExistM90=.false.
        call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
        call EM9CreateM90(0,ich)
      endif
      call DeleteFile(PreviousM40)
      if(isPowder) call DeleteFile(PreviousM41)
      call DeleteFile(PreviousM50)
      go to 9999
9000  call MoveFile(PreviousM40,fln(:ifln)//'.m40')
      if(isPowder) call MoveFile(PreviousM41,fln(:ifln)//'.m41')
      call MoveFile(PreviousM50,fln(:ifln)//'.m50')
9999  if(allocated(RmOld)) deallocate(RmOld,ips,itrl)
      if(allocated(RmNew)) deallocate(RmNew)
      if(allocated(AtTypeMagO))
     1  deallocate(AtTypeMagO,AtTypeMagJO,TypeFFMagO,FFnO,FFniO,FFMagO)
      return
100   format(3(a1,f10.4),3(a1,f10.3))
      end
      subroutine RAImportFromIsodistortInteractive
      use Basic_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*80 Veta,FileIn
      character*60 GrupaOrg,GrupaImp,GrupaTr
      character*1   :: IndicesCapital(6) = (/'H','K','L','M','N','P'/)
      character*6 :: CellParOrgC(3) = (/'a(org)','b(org)','c(org)'/)
      character*6 :: CellParImpC(3) = (/'a(imp)','b(imp)','c(imp)'/)
      real CellTr(6),CellImp(6),QuImp(3,3),TrM3(9),TrM3I(9),TrM(36),
     1     TrP(36),xp(36),xpp(36)
      integer UseTabsIn,UseTabsCell,UseTabsSG,CrwStateQuest
      integer, allocatable :: ips(:),itrl(:)
      logical UseOriginalCell,CrwLogicQuest,IdenticalSymmetry,EqRV,
     1        MatRealEqUnitMat,ExistFile
      real, allocatable :: RmOld(:,:),RmNew(:,:)
      UseTabsIn=UseTabs
      UseTabsCell=-1
      UseTabsSG=-1
      NDimOrg=NDim(KPhase)
      GrupaOrg=Grupa(KPhase)
      call CopyVek(CellPar(1,1,KPhase),CellParOrg,6)
      call CopyVek(Qu(1,1,1,KPhase),QuOrg,3*NDimI(KPhase))
      if(allocated(RmOld)) deallocate(RmOld,ips,itrl)
      allocate(RmOld(9,NSymm(KPhase)),ips(NSymm(KPhase)),
     1         itrl(NSymm(KPhase)))
      NSymmOld=0
      write(Cislo,'(i5)') NSymm(KPhase)
      call FeWinMessage(Cislo,' ')
      do i=1,NSymm(KPhase)
        call MatBlock3(Rm6(1,i,1,1),xp,NDim(KPhase))
        do j=1,NSymmOld
          if(EqRV(xp,RmOld(1,j),9,.001)) go to 1100
        enddo
        NSymmOld=NSymmOld+1
        RmOld(1:9,NSymmOld)=xp(1:9)
        call PisOperator(rmOld(1,NSymmOld),s6(1,1,1,1),1.,3)
        write(6,'('' --------------------------------------'')')
1100  enddo
      IStSymmOrg=0
      call CrlStoreSymmetry(IStSymmOrg)
      if(KCommen(KPhase).gt.0) call iom50(0,0,fln(:ifln)//'.m50')
      FileIn=fln(:ifln)//'.cif'
1120  call FeFileManager('Select input CIF file',FileIn,
     1                   '*.cif *.mcif',0,.true.,ich)
      if(ich.ne.0) go to 9000
      if(.not.ExistFile(FileIn)) then
        call FeChybne(-1.,YBottomMessage,'the file "'//
     1                FileIn(:idel(FileIn))//
     1                '" does not exist, try again.',' ',SeriousError)
        go to 1120
      endif
      call ReadSpecifiedCIF(FileIn,5)
      if(ErrFlag.ne.0) go to 9000
      if(NDim(KPhase).ne.3.and.NDim(KPhase).ne.NDimOrg) then
        call FeChybne(-1.,-1.,'the model is neither regular nor '//
     1                'modulated with the same dimensionality.',' ',
     2                SeriousError)
        go to 9000
      endif
!   nacteni matice transformace, kdyz ne tak
      call FindCellTrans(CellPar(1,1,KPhase),CellParOrg,TrM3,ich)
      if(ich.ne.0) then
        call FeFillTextInfo('importmodel.txt',0)
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        call UnitMat(TrM3,3)
      endif
      call CopyVek(CellPar(1,1,KPhase),CellImp,6)
      call CopyVek(Qu(1,1,1,KPhase),QuImp,3*NDimI(KPhase))
      call UnitMat(TrP,NDimOrg)
      IStSymmImp=0
      call CrlStoreSymmetry(IStSymmImp)
      call CrlRestoreSymmetry(IStSymmImp)
      if(allocated(RmNew)) deallocate(RmNew)
      allocate(RmNew(9,NSymm(KPhase)))
      NSymmNew=0
      do i=1,NSymm(KPhase)
        call MatBlock3(Rm6(1,i,1,1),xp,NDim(KPhase))
        do j=1,NSymmNew
          if(EqRV(xp,RmNew(1,j),9,.001)) go to 1200
        enddo
        NSymmNew=NSymmNew+1
        RmNew(1:9,NSymmNew)=xp(1:9)
1200  enddo
      GrupaImp=Grupa(KPhase)
      ich=0
      id=NextQuestId()
      xqd=500.
      il=15
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      UseTabsCell=NextTabs()
      UseTabs=UseTabsCell
      xpom=0.
      do i=1,6
        call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        xpom=xpom+50.
      enddo
      il=-10
      tpom=5.
      xpom=tpom+100.
      Veta='Cell parameters:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      il=il-7
      Veta='Imported:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      write(Veta,100)(Tabulator,CellImp(i),i=1,6)
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      il=il-7
      Veta='Transformed:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=' '
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      nLblCell=LblLastMade
      il=il-7
      Veta='Original:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      write(Veta,100)(Tabulator,CellParOrg(i),i=1,6)
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      il=il-12
      Veta='Space group:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      UseTabsSG=NextTabs()
      UseTabs=UseTabsSG
      call FeTabsAdd(100.,UseTabs,IdRightTab,' ')
      il=il-7
      Veta='Imported:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=Tabulator//GrupaImp
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il-7
      Veta='Transformed:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=Tabulator//' '
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblGrupa=LblLastMade
      il=il-7
      Veta='Original:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      Veta=Tabulator//GrupaOrg
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il-14
      Veta='Transformation imported cell -> original cell:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      XShow=70.+QuestXMin(id)
      YShow=QuestYPosition(id,nint(float(-il)*.1)+4)+QuestYMin(id)
      il=il-45
      Veta='Matrix %calculator'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtMatCalc=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il-10
      call FeQuestLinkaMake(id,il)
      Veta='Transform the model into the %original cell'
      xpom=5.
      tpom=xpom+CrwgXd+5.
      do i=1,2
        il=il-10
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        if(i.eq.1) then
          nCrwTrans=CrwLastMade
          Veta='Transform reflections files into the i%mported cell'
        endif
      enddo
      UseOriginalCell=.true.
      go to 1400
1300  call CrlMatEqHide
1400  call CopyVek(CellImp,CellTr,6)
      call CopyVek(QuImp,Qu(1,1,1,KPhase),3*NDimI(KPhase))
      call SetRealArrayTo(xp,9,0.)
      call UnitMat(xpp,NDim(KPhase))
      if(NDimI(KPhase).gt.0) then
        call DRMatTrCellQ(TrM3,CellTr,Qu(1,1,1,KPhase),xp,xpp)
      else
        call DRMatTrCell(TrM3,CellTr,xpp)
      endif
      call CopyVek(CellTr,CellPar(1,1,KPhase),6)
      write(Veta,100)(Tabulator,CellTr(i),i=1,6)
      call FeQuestLblChange(nLblCell,Veta)
      call CrlRestoreSymmetry(IStSymmImp)
      if(NDim(KPhase).gt.3) then
        call MatFromBlock3(TrM3,TrM,NDim(KPhase))
      else
        call CopyMat(TrM3,TrM,NDim(KPhase))
      endif
      call CrlTransSymm(TrM,ichp)
      if(ichp.ne.0) then
        ich=0
        go to 1600
      endif
      call FindSmbSg(GrupaTr,ChangeOrderYes,1)
      call CrlRestoreSymmetry(IStSymmImp)
      Veta=Tabulator//GrupaTr
      call FeQuestLblChange(nLblGrupa,Veta)
      call CrlMatEqShow(XShow,YShow,3,TrM3,3,CellParOrgC,CellParImpC,3)
      nCrw=nCrwTrans
      call MatInv(TrM3,TrP,det,3)
      do i=1,2
        if(MatRealEqUnitMat(TrM3,3,.001).or.abs(det-1.).gt..001) then
          call FeQuestCrwDisable(nCrw)
          if(i.eq.1) UseOriginalCell=abs(det-1.).le..001
        else
          call FeQuestCrwOpen(nCrw,i.eq.1.eqv.UseOriginalCell)
        endif
        nCrw=nCrw+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtMatCalc) then
        call TrMat(TrM3,TrP,3,3)
        call MatrixCalculatorInOut(TrP,3,3,ichp)
        if(ichp.eq.0) then
          call TrMat(TrP,TrM3,3,3)
          go to 1300
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
1600  if(ich.eq.0) then
        if(CrwStateQuest(nCrwTrans).eq.CrwDisabled) then
          UseOriginalCell=abs(det-1.).le..001
        else
          UseOriginalCell=CrwLogicQuest(nCrwTrans)
        endif
      endif
      call CrlMatEqHide
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9000
      if(NDimOrg.gt.3) then
        call MatFromBlock3(TrM3,TrM,NDimOrg)
      else
        call CopyMat(TrM3,TrM,NDimOrg)
      endif
      call CopyVek(CellParOrg,CellPar(1,1,KPhase),6)
      call CopyVek(QuOrg,Qu(1,1,1,KPhase),3*(NDimOrg-3))
      MaxNLattVec=max(MaxNLattVec,NLattVec(KPhase))
      if(NDimOrg.gt.NDim(KPhase)) then
        call ReallocSymm(NDimOrg,NSymm(KPhase),NLattVec(KPhase),
     1                   NComp(KPhase),NPhase,0)
        call CrlCompareSymmetry(IStSymmOrg,IdenticalSymmetry)
        if(IdenticalSymmetry) then
          call CrlCompleteSymmetry(IStSymmOrg)
        else
          call CrlCompIntSymm(NDimOrg,ich)
          if(ich.ne.0) go to 9000
        endif
        NDim(KPhase)=NDimOrg
        NDimI(KPhase)=NDimOrg-3
        NDimQ(KPhase)=NDimOrg**2
        MaxNDim =NDim (KPhase)
        MaxNDimI=NDimI(KPhase)
      endif
      call iom40Only(0,0,fln(:ifln)//'.m40')
      if(UseOriginalCell) then
        call CrlTransSymm(TrM,ichp)
        if(ichp.ne.0) then
          ich=0
          go to 1600
        endif
        if(.not.MatRealEqUnitMat(TrM3,3,.001)) then
          if(allocated(TransMat)) call DeallocateTrans
          NTrans=1
          call AllocateTrans
          call MatInv(TrM,TransMat,det,NDim(KPhase))
          call SetRealArrayTo(TransVec,NDim(KPhase),0.)
          TransZM(1)=0
          call EM40SetTr(0)
          call EM40TransAtFromTo(1,NAtInd,ich)
          if(allocated(TransMat)) call DeallocateTrans
        endif
      else
        call CrlRestoreSymmetry(IStSymmImp)
        call MatInv(TrM3,TrP,det,3)
        call SetRealArrayTo(xp,9,0.)
        call UnitMat(xpp,NDim(KPhase))
        if(NDimOrg.gt.3) then
          call DRMatTrCellQ(TrP,CellPar(1,1,KPhase),Qu(1,1,1,KPhase),xp,
     1                      xpp)
        else
          call DRMatTrCell(TrP,CellPar(1,1,KPhase),xpp)
        endif
      endif
      call FindSmbSg(Grupa(KPhase),ChangeOrderYes,1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call iom50(0,0,fln(:ifln)//'.m50')
      if(isPowder) then
        call iom90(0,fln(:ifln)//'.m90')
        call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        call iom41(0,0,fln(:ifln)//'.m41')
        call CopyVek(CellPar(1,1,KPhase),CellPwd(1,KPhase),6)
        call CopyVek(Qu(1,1,1,KPhase),QuPwd(1,1,KPhase),3*NDimI(KPhase))
        call iom41(1,0,fln(:ifln)//'.m41')
        call iom41(0,0,fln(:ifln)//'.m41')
      else
        if(NSymmOld.gt.NSymmNew) then
          call MatInv(TrM3,TrM3I,VolRatio,3)
          if(UseOriginalCell) then
            do i=1,NSymmNew
              call multm(TrM3I,RmNew(1,i),xp,3,3,3)
              call multm(xp,TrM3,RmNew(1,i),3,3,3)
            enddo
          else
            do i=1,NSymmOld
              call multm(TrM3,RmOld(1,i),xp,3,3,3)
              call multm(xp,TrM3I,RmOld(1,i),3,3,3)
            enddo
          endif
          ips=0
          n=0
          do i=1,NSymmOld
            do j=1,NSymmNew
              if(EqRV(RmOld(1,i),RmNew(1,j),9,.001)) then
                n=n+1
                ips(i)=1
                exit
              endif
            enddo
          enddo
          n=1
          do i=1,NSymmOld
            if(ips(i).ne.0) cycle
            n=n+1
            do 2650j=1,NSymmOld
              if(ips(j).eq.1) then
                call MultM(RmOld(1,i),RmOld(1,j),xp,3,3,3)
                do k=1,NSymmOld
                  if(ips(k).eq.0.and.eqrv(RmOld(1,k),xp,9,.001)) then
                    ips(k)=n
                    itrl(n)=k
                    go to 2650
                  endif
                enddo
              endif
2650        continue
          enddo
          NTwin=n
          pom=1./float(NTwin)
          do i=2,NTwin
            rtw(1:9,i)=RmOld(1:9,itrl(i))
            sctw(i,KDatBlock)=pom
          enddo
          mxscutw=max(mxscu+NTwin-1,6)
          call iom40Only(1,0,fln(:ifln)//'.m40')
          call iom40Only(0,0,fln(:ifln)//'.m40')
          call iom50(1,0,fln(:ifln)//'.m50')
          call iom50(0,0,fln(:ifln)//'.m50')
        else if(NSymmOld.eq.NSymmNew) then
          NTwin=1
        else
          call FeChybne(-1.,-1.,'inconsistency: the number of '//
     1                  'symmetry operations of the parent phase '//
     2                  'larger than that for the subgroup.',
     3                  SeriousError)
          go to 9000
        endif
      endif
      if(UseOriginalCell) then
        if(.not.MatRealEqUnitMat(TrM3,3,.001)) then
          call iom40Only(1,0,fln(:ifln)//'.m40')
          call iom40Only(0,0,fln(:ifln)//'.m40')
        endif
        if(ExistM95) then
          call CrlCompRefExtSymmetry(IStSymmOrg,IdenticalSymmetry)
          if(.not.IdenticalSymmetry) then
            call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
            call EM9CreateM90(0,ich)
          endif
        endif
      else
        if(ExistM95) then
          call MatInv(TrM,TrP,det,NDim(KPhase))
          call Multm(TrMP,TrP,TrM,NDim(KPhase),NDim(KPhase),
     1               NDim(KPhase))
          call CopyMat(TrM,TrMP,NDim(KPhase))
          call CompleteM95(0)
          ExistM90=.false.
          call SetLogicalArrayTo(ModifyDatBlock,NDatBlock,.true.)
          call EM9CreateM90(0,ich)
        endif
      endif
      call DeleteFile(PreviousM40)
      call DeleteFile(PreviousM50)
      go to 9999
9000  call MoveFile(PreviousM40,fln(:ifln)//'.m40')
      if(isPowder) call MoveFile(PreviousM41,fln(:ifln)//'.m41')
      call MoveFile(PreviousM50,fln(:ifln)//'.m50')
9999  if(UseTabsCell.ge.0) call FeTabsReset(UseTabsCell)
      if(UseTabsSG.ge.0) call FeTabsReset(UseTabsSG)
      UseTabs=UseTabsIn
      call CrlCleanSymmetry
      if(allocated(RmOld)) deallocate(RmOld,ips,itrl)
      if(allocated(RmNew)) deallocate(RmNew)
      return
100   format(3(a1,f10.4),3(a1,f10.3))
      end
