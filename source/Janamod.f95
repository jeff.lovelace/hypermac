      module Basic_mod


!  ---------------------------------------------------------------------

      dimension rmgc(:,:,:,:),rm6gc(:,:,:,:),s6gc(:,:,:,:),
     1          KwSymGC(:,:,:,:),rtgc(:,:,:,:),rc3gc(:,:,:,:),
     2          rc4gc(:,:,:,:),rc5gc(:,:,:,:),rc6gc(:,:,:,:),
     3          iswGC(:,:,:),RMagGC(:,:,:,:),ZMagGC(:,:,:)
      integer GammaIntGC(:,:,:,:)
      logical SwitchedToComm
      allocatable rmgc,rm6gc,s6gc,KwSymGC,GammaIntGC,rtgc,rc3gc,rc4gc,
     1            rc5gc,rc6gc,iswGC,RMagGC,ZMagGC
!  ---------------------------------------------------------------------

      integer, allocatable :: KwSym(:,:,:,:)
      integer :: NSatGroups=0,HSatGroups(1:3,0:19)

!  ---------------------------------------------------------------------

      real, allocatable :: rm6(:,:,:,:),rm(:,:,:,:),s6(:,:,:,:),
     1                     vt6(:,:,:,:),rmag(:,:,:,:),zmag(:,:,:)
      integer, allocatable :: ISwSymm(:,:,:),InvMag(:),ISymmMolec(:),
     1                        CenterMag(:)
      integer ISymmBasic, ISymmMolecMax
      character*80, allocatable :: StSymmCard(:)
      character*30, allocatable :: symmc(:,:,:,:)
      logical, allocatable :: BratSymm(:,:)

!  ---------------------------------------------------------------------

      dimension rm6l(:,:,:,:),rml(:,:,:,:),s6l(:,:,:,:),ISwSymmL(:,:,:),
     1          KwSymL(:,:,:,:),rtl(:,:,:,:),rc3l(:,:,:,:),
     2          rc4l(:,:,:,:),rc5l(:,:,:,:),rc6l(:,:,:,:),
     3          XYZMode(:,:,:)
      character*8 LXYZMode(:,:)
      integer GammaIntL(:,:,:,:),NXYZMode(:),MXYZMode(:)
      allocatable rm6l,s6l,rml,ISwSymmL,KwSymL,rtl,rc3l,rc4l,rc5l,rc6l,
     1            GammaIntL,XYZMode,LXYZMode,NXYZMode,MXYZMode

!  ---------------------------------------------------------------------

      real, allocatable :: AtWeight(:,:),AtRadius(:,:),AtNum(:,:),
     1                     AtMult(:,:),
     2                     FFBasic(:,:,:),FFCore(:,:,:),FFVal(:,:,:),
     3                     FFCoreD(:,:,:),FFValD(:,:,:),
     4                     FFra(:,:,:),FFia(:,:,:),FFrRef(:,:,:),
     5                     FFiRef(:,:,:),sFFrRef(:,:,:),sFFiRef(:,:,:),
     6                     FFn(:,:),FFni(:,:),FFMag(:,:,:),FFEl(:,:,:),
     7                     FFa(:,:,:,:),FFae(:,:,:,:),fx(:),fm(:),
     8                     ZSlater(:,:,:),HZSlater(:,:),
     9                     TypicalDist(:,:,:),DTypicalDist(:,:),
     a                     ZSTOA(:,:,:,:,:),CSTOA(:,:,:,:,:)
      integer :: NTypicalDistMax = (0)
      integer, allocatable :: NTypicalDist(:),PopCore(:,:,:),
     1                        PopVal(:,:,:),HNSlater(:,:),TypeCore(:,:),
     2                        TypeVal(:,:),AtVal(:,:),AtColor(:,:),
     3                        NSlater(:,:,:),NSTOA(:,:,:,:,:),
     4                        NCoefSTOA(:,:,:,:),TypeFFMag(:,:)
      character*256, allocatable :: CoreValSource(:,:)
      character*7, allocatable :: AtTypeFull(:,:),AtTypeMag(:,:),
     1                            AtTypeMenu(:),AtTypeMagJ(:,:)
      character*2, allocatable :: AtType(:,:),AtTypicalDist(:,:,:)

!  ---------------------------------------------------------------------

      parameter (nCIFAtX=14,nCIFAtT=8,nCIFAtSM=4,nCIFAtXM=5,nCIFAtTM=5,
     1           nCIFAtCM=5,nCIFPwdProf=5,nCIFPwdTOF=5,nCIFSingleRfl=4,
     2           nCIFElectronRfl=5,
     3           nCIFPwdFit=15,nCIFSingleFit=18,nCIFHBonds=11,
     4           nCIFRestDist=6,nCIFRestAngle=8,nCIFRestTorsion=10,
     5           nCIFRestEqDist=6,nCIFRestEqAngle=8,
     6           nCIFRestEqTorsion=10)
      integer :: CIFAtX(nCIFAtX) =
     1           (/33,46,30,31,32,1,48,106,41,21,42,28,29,52/)
      integer :: CIFAtT(nCIFAtT) = (/8,10,11,14,16,12,13,15/)
      integer :: CIFAtSM(nCIFAtSM) = (/68,70,71,75/)
      integer :: CIFAtXM(nCIFAtXM) = (/54,55,57,58,62/)
      integer :: CIFAtTM(nCIFAtTM) = (/88,90,91,92,96/)
      integer :: CIFAtCM(nCIFAtCM) = (/164,166,167,168,172/)
      integer :: CIFPwdProf(nCIFPwdProf) = (/180,114,130,148,147/)
      integer :: CIFPwdTOF(nCIFPwdProf) = (/180,118,130,148,147/)
      integer :: CIFSingleRfl(nCIFSingleRfl) = (/9,10,11,20/)
      integer :: CIFElectronRfl(nCIFElectronRfl) = (/16,17,18,24,20/)
      integer :: CIFPwdFit(nCIFPwdFit) =
     1           (/23,38,22,37,12,19,36,35,32,33,1,2,11,18,21/)
      integer :: CIFSingleFit(nCIFSingleFit) =
     1           (/23,38,22,40,15,13,20,19,36,35,32,33,1,2,11,18,21,34/)
      integer :: CIFHBonds(nCIFHBonds) =
     1           (/23,24,25,30,31,32,26,27,28,22,29/)
      integer :: CIFRestDist(nCIFRestDist) =
     1           (/12,16,13,17,18,19/)
      integer :: CIFRestAngle(nCIFRestAngle) =
     1           (/2,7,3,8,4,9,10,11/)
      integer :: CIFRestTorsion(nCIFRestTorsion) =
     1           (/104,110,105,111,106,112,107,113,114,115/)
      integer :: CIFRestEqDist(nCIFRestEqDist) =
     1           (/47,51,48,52,57,58/)
      integer :: CIFRestEqAngle(nCIFRestEqAngle) =
     1           (/33,38,34,39,35,40,45,46/)
      integer :: CIFRestEqTorsion(nCIFRestEqTorsion) =
     1           (/59,65,60,66,61,67,62,68,73,74/)
      integer CIFFlag
      character*80  CIFKey(:,:)
      character*128 CIFArray(:)
      character*256 CIFLastReadRecord,CIFArrayUpdate(:)
      integer CIFKeyFlag(:,:),nCIFArray,nCIFUsed,nCIFArrayUpdate
      logical :: CIFNewFourierWaves = .true.
      allocatable CIFKey,CIFKeyFlag,CIFArray,CIFArrayUpdate

!  ---------------------------------------------------------------------

      real RMatCalc(36,0:6)
      integer NRowCalc(0:6),NColCalc(0:6)

!  ---------------------------------------------------------------------

      integer      neq,neqs,mxe,mxep,lnp(:),lnpo(:),pnp(:,:),npa(:)
      real         pko(:,:),pab(:)
      character*20 lat(:),pat(:,:)
      character*12 lpa(:),ppa(:,:)
      logical      eqhar(:)
      allocatable lnp,lnpo,pnp,npa,pko,pab,lat,pat,lpa,ppa,eqhar

!  ---------------------------------------------------------------------

      character*12 :: IdM50(72) =
     1   (/'title       ','cell        ','ndim        ','ncomp       ',
     2     'qi          ','qr          ','wmatrix     ','commen      ',
     3     'tzero       ','centro      ','symmetry    ','lattice     ',
     4     'lattvec     ','spgroup     ','unitsnumb   ','sgshift     ',
     5     'formtab     ','esdcell     ','densities   ','atom        ',
     6     'atmag       ','localsymm   ','twin        ','refofanom   ',
     7     'chemform    ','roundmethod ','wavefile    ','phase       ',
     8     'phasetwin   ','magnetic    ','lambda      ','radtype     ',
     9     'lpfactor    ','nalpha      ','monangle    ','kalpha1     ',
     a     'kalpha2     ','kalphar     ','perfmono    ','slimits     ',
     1     'flimits     ','powder      ','noofref     ','datcolltemp ',
     2     'qmag        ','parentstr   ','typdist     ','xyzmode     ',
     3     'atmagj      ','#r50        ',
     4     'atradius    ','fneutron    ','fneutronim  ','FFBasic     ',
     5     'FFCore      ','FFVal       ','nslater     ','DzSlater    ',
     6     'ZSlater     ','color       ','f''          ','f"          ',
     7     'FFMag       ','atweight    ','formula     ','fffree      ',
     8     'dmax        ','formtab     ','alphagmono  ','betagmono   ',
     9     'wffile      ','stofunction '/)
      character*80  :: MenuWF(7) = (/'wavefj.dat          ',
     1                               'wavefn.dat          ',
     2                               'wavefc.dat          ',
     3                               'xd.bnk_RHF_CR       ',
     4                               'xd.bnk_RHF_BBB      ',
     5                               'xd.bnk_RDF_SCM      ',
     6                               'xd.bnk_PBE-QZ4P-ZORA'/)

C  ---------------------------------------------------------------------

      dimension XSavedPoint(:,:)
      character*80 StSavedPoint(:)
      integer :: NSavedPoints = 0, MaxSavedPoints = 0
      allocatable XSavedPoint,StSavedPoint

C  ---------------------------------------------------------------------

      dimension ihmin(6),ihmax(6)

C  ---------------------------------------------------------------------

      character*35  :: SpecMatrixName(14) =
     1             (/'from#F#to#P              ',
     2               'from#R obverse#to#P#(1)  ',
     3               'from#R obverse#to#P#(2)  ',
     4               'from#R obverse#to#P#(3)  ',
     5               'from#R reverse#to#P#(1)  ',
     6               'from#R reverse#to#P#(2)  ',
     7               'from#R reverse#to#P#(3)  ',
     8               'from#P#to#F              ',
     9               'from#P#to#R obverse#(1)  ',
     a               'from#P#to#R obverse#(2)  ',
     1               'from#P#to#R obverse#(3)  ',
     2               'from#P#to#R reverse#(1)  ',
     3               'from#P#to#R reverse#(2)  ',
     4               'from#P#to#R reverse#(3)  '/)
      real :: SpecMatrix(9,14) = reshape
     1    ((/0.,.5,.5,.5, 0.,.5,.5,.5,0.,
     2        .666667, .333333, .333333,
     2       -.333333, .333333, .333333,
     2       -.333333,-.666667, .333333,
     3       -.333333,-.666667, .333333,
     3        .666667, .333333, .333333,
     3       -.333333, .333333, .333333,
     4       -.333333, .333333, .333333,
     4       -.333333,-.666667, .333333,
     4        .666667, .333333, .333333,
     5       -.666667,-.333333, .333333,
     5        .333333,-.333333, .333333,
     5        .333333, .666667, .333333,
     6        .333333, .666667, .333333,
     6       -.666667,-.333333, .333333,
     6        .333333,-.333333, .333333,
     7        .333333,-.333333, .333333,
     7        .333333, .666667, .333333,
     7       -.666667,-.333333, .333333,
     8       -1., 1., 1., 1.,-1., 1., 1., 1., 1.,
     9        1.,-1., 0., 0., 1.,-1., 1., 1., 1.,
     a        0., 1.,-1.,-1., 0., 1., 1., 1., 1.,
     1       -1., 0., 1., 1.,-1., 0., 1., 1., 1.,
     2       -1., 1., 0., 0.,-1., 1., 1., 1., 1.,
     3        0.,-1., 1., 1., 0.,-1., 1., 1., 1.,
     4        1., 0.,-1.,-1., 1., 0., 1., 1., 1./),
     5         shape(SpecMatrix))

C  ---------------------------------------------------------------------

      character*44 :: MapType(10) = (/
     1             'F(obs)**2 - Patterson                      ',
     2             'F(calc)**2 - checking Patterson            ',
     3             'F(obs)**2-F(calc)**2 - difference Patterson',
     4             'F(obs) - Fourier                           ',
     5             'F(calc) - checking Fourier                 ',
     6             'F(obs)-F(calc) - difference Fourier        ',
     7             'dynamic multipole deformation map          ',
     8             'static multipole deformation map           ',
     9             '0/1 - shape function                       ',
     a             'Difference between two Fourier maps        '/)

C  ---------------------------------------------------------------------

      parameter (NMon=4)
      character*10 :: MonName(NMon) =
     1                (/'Si      ',
     2                  'Ge      ',
     3                  'Graphite',
     4                  'Other   '/)
      real :: MonCell(6,NMon) = reshape
     1        ((/5.431,5.431,5.431,90.,90.,90.,
     2           5.657,5.657,5.657,90.,90.,90.,
     3           2.464,2.464,6.7079,90.,90.,120.,
     4           5.43,5.43,5.43,90.,90.,90./),
     5           shape(MonCell))
      integer :: MonH(3,NMon) = reshape
     1        ((/1,1,1,1,1,1,0,0,2,1,0,0/),shape(MonH)),
     2        MonSel=3

C  ---------------------------------------------------------------------

      character*256 :: CallSIR(6) = (/' ',' ',' ',' ',' ',' '/),
     1                 DirSIR(6)  = (/' ',' ',' ',' ',' ',' '/)
      character*4   :: SIRName(6) = (/'97  ','2002','2004','2008',
     1                                '2011','2014'/)
      integer       :: SIRDefined(6) = (/0,0,0,0,0,0/)


      character*256 :: CallExpo(5) = (/' ',' ',' ',' ',' '/),
     1                 DirExpo(5)  = (/' ',' ',' ',' ',' '/)
      character*4   :: ExpoName(5) = (/'----','2004','2009','2013',
     1                                 '2014'/)
      integer       :: ExpoDefined(5) = (/0,0,0,0,0/)

      character*256 :: DCREDFile=' ',CallGeminography=' ',
     1                 CallShelxt=' ',CommandLineShelxt=' ',XDBnkDir=' '

C  ---------------------------------------------------------------------

      real :: AvogadroNumber=6.02214129E23

C  ---------------------------------------------------------------------

      character*256, allocatable :: CyclicRefFile(:)
      character*256 :: CyclicRefMasterFile=' '
      character*8 CyclicRefTypeUnits
      integer :: ICyclicRefFile=1,NCyclicRefFile,CyclicRefType,
     1           CyclicRefTypeTemp=1,CyclicRefTypePress=2,
     2           CyclicRefTypeTime=2
      integer, allocatable :: CyclicRefStart(:),CyclicRefUse(:),
     1                        CyclicRefUpDown(:),CyclicRefOrder(:)
      logical :: CyclicRefMode=.false.,CyclicBack=.false.,
     1           CyclicWizardHotovo=.false.
      real, allocatable :: CyclicRefParam(:)
      end
      module Contour_mod
      real :: CellParCon(6),CPCrit=(.0001),CPMaxStep=(.05),
     1        CPRhoMin=(0.),CPDmin= (0.), CPDmax=(2.),
     2        CPDGeom=(2.),teplota,toev,tpdfp,tpdfk,ntpdf,
     3        CutOffDist=(4.),fmzn,fmzp,cutpos,cutneg
      real, allocatable :: ActualMap(:),DenCore(:,:),DenVal(:,:),
     1                     DenCoreDer(:,:,:),DenValDer(:,:,:),
     2                     XfChd(:,:),XoChd(:,:),popasChd(:,:),Toll(:),
     3                     RCapture(:),rcla(:,:),xcpa(:,:),CPRho(:),
     4                     CPGrad(:),CPDiag(:,:),xpdf(:,:),
     5                     popaspdf(:,:),dpdf(:),fpdf(:),ypdf(:,:),
     6                     ContourArray(:)
      integer :: NActualMap,DrawPos,DrawNeg,NaChd,IPlaneOld=0,
     1           CPNIter = (40),ncp,npdf,MaxPDF,ContourType,
     2           ContourTypeErr,ContourNumber,ContourAIMOrder=0,
     3           IVarka,NZeroNeighAll,NumberOfPlanes,NumberOfPlanesMax
      integer, allocatable :: FlagMap(:),IaChd(:),IsChd(:),ireach(:),
     1                        iapdf(:),ispdf(:),ipor(:),idpdf(:)
      character*6, allocatable :: CPType(:)
      character*20, allocatable ::  LabelPlane(:),LabelPlaneO(:)
      character*20 LabelPlaneNew
      character*80, allocatable :: scpdf(:)
      logical :: OldNorm,NewAtoms = .false., NewPoints = .false.,
     1           ModifiedAtoms = .false.
      logical, allocatable :: atbrat(:),selpdf(:),BratAnharmPDF(:,:),
     1                        AtBratAnharm(:,:)
      character*30 :: MenDensity(7) = (/'total density            ',
     1                                  'valence density          ',
     2                                  'deformation density      ',
     3                                  'Laplacian-total density  ',
     4                                  'Laplacian-valence density',
     5                                  'inversed gradient vector ',
     6                                  'gradient vector          '/)
      end
      module Datred_mod
      real :: abskou(19,101) = reshape ((/
     1 1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     2 1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     3 1.16,1.16,1.16,1.16,1.16,1.16,1.16,1.16,1.16,1.16,
     4 1.16,1.16,1.16,1.16,1.16,1.16,1.16,1.16,1.16,
     5 1.35,1.35,1.35,1.34,1.34,1.34,1.34,1.34,1.34,1.34,
     6 1.33,1.33,1.33,1.33,1.33,1.33,1.33,1.33,1.33,
     7 1.56,1.56,1.56,1.55,1.55,1.55,1.55,1.54,1.54,1.53,
     8 1.53,1.53,1.52,1.52,1.51,1.51,1.51,1.51,1.51,
     9 1.80,1.80,1.80,1.79,1.79,1.78,1.78,1.77,1.76,1.75,
     a 1.74,1.73,1.73,1.72,1.71,1.70,1.70,1.70,1.70,
     1 2.08,2.07,2.07,2.06,2.06,2.05,2.03,2.02,2.01,1.99,
     2 1.97,1.96,1.94,1.93,1.92,1.91,1.90,1.90,1.90,
     3 2.39,2.39,2.38,2.37,2.36,2.34,2.32,2.30,2.27,2.25,
     4 2.23,2.20,2.18,2.16,2.14,2.13,2.11,2.11,2.11,
     5 2.75,2.74,2.73,2.72,2.70,2.67,2.64,2.60,2.57,2.53,
     6 2.50,2.46,2.43,2.40,2.37,2.35,2.33,2.32,2.32,
     7 3.15,3.15,3.13,3.11,3.07,3.03,2.99,2.94,2.89,2.83,
     8 2.78,2.73,2.69,2.65,2.61,2.58,2.56,2.55,2.55,
     9 3.61,3.60,3.58,3.54,3.50,3.44,3.37,3.30,3.23,3.16,
     a 3.09,3.02,2.96,2.91,2.86,2.82,2.80,2.78,2.77,
     1 4.12,4.11,4.08,4.03,3.96,3.88,3.79,3.70,3.60,3.50,
     2 3.41,3.33,3.25,3.18,3.12,3.07,3.04,3.02,3.01,
     3 4.70,4.69,4.64,4.57,4.48,4.37,4.25,4.12,3.99,3.87,
     4 3.75,3.64,3.55,3.46,3.39,3.33,3.28,3.26,3.25,
     5 5.35,5.33,5.27,5.17,5.05,4.90,4.74,4.57,4.41,4.25,
     6 4.11,3.97,3.85,3.75,3.66,3.59,3.54,3.50,3.49,
     7 6.08,6.05,5.97,5.84,5.67,5.47,5.27,5.06,4.85,4.66,
     8 4.48,4.32,4.17,4.04,3.94,3.85,3.79,3.75,3.73,
     9 6.90,6.86,6.75,6.57,6.35,6.10,5.83,5.57,5.32,5.08,
     a 4.86,4.67,4.49,4.34,4.22,4.12,4.05,4.00,3.98,
     1 7.80,7.75,7.60,7.38,7.09,6.77,6.44,6.11,5.81,5.52,
     2 5.26,5.03,4.83,4.65,4.51,4.39,4.31,4.26,4.23,
     3 8.81,8.74,8.55,8.25,7.89,7.49,7.08,6.69,6.32,5.98,
     4 5.67,5.40,5.17,4.97,4.80,4.67,4.57,4.51,4.48,
     5 9.92,9.83,9.59,9.21,8.76,8.26,7.76,7.29,6.85,6.45,
     6 6.10,5.78,5.51,5.28,5.09,4.94,4.83,4.77,4.74,
     7 11.2,11.0,10.7,10.3,9.69,9.08,8.48,7.92,7.40,6.94,
     8 6.53,6.17,5.87,5.60,5.39,5.22,5.10,5.02,4.99,
     9 12.5,12.4,12.0,11.4,10.7,9.95,9.24,8.58,7.98,7.44,
     a 6.97,6.57,6.22,5.93,5.69,5.50,5.37,5.28,5.25,
     1 14.0,13.8,13.3,12.6,11.8,10.9,10.0,9.26,8.57,7.96,
     2 7.43,6.97,6.59,6.26,5.99,5.78,5.63,5.54,5.50,
     3 15.6,15.4,14.8,13.9,12.9,11.8,10.9,9.97,9.18,8.49,
     4 7.89,7.38,6.95,6.59,6.30,6.07,5.90,5.80,5.76,
     5 17.4,17.1,16.4,15.3,14.1,12.9,11.7,10.7,9.80,9.03,
     6 8.36,7.80,7.33,6.93,6.61,6.36,6.17,6.06,6.02,
     7 19.4,19.0,18.1,16.8,15.3,13.9,12.6,11.4,10.4,9.57,
     8 8.84,8.22,7.70,7.27,6.92,6.64,6.45,6.32,6.28,
     9 21.5,21.0,19.9,18.4,16.7,15.0,13.5,12.2,11.1,10.1,
     a 9.32,8.65,8.08,7.61,7.23,6.93,6.72,6.59,6.54,
     1 23.8,23.3,21.9,20.0,18.1,16.2,14.5,13.0,11.7,10.7,
     2 9.81,9.07,8.46,7.95,7.54,7.23,6.99,6.85,6.80,
     3 26.3,25.6,24.0,21.8,19.5,17.3,15.4,13.8,12.4,11.3,
     4 10.3,9.51,8.85,8.30,7.86,7.52,7.27,7.12,7.06,
     5 29.0,28.2,26.2,23.7,21.0,18.6,16.4,14.6,13.1,11.8,
     6 10.8,9.94,9.23,8.64,8.17,7.81,7.54,7.38,7.33,
     7 31.9,30.9,28.6,25.6,22.6,19.8,17.4,15.4,13.8,12.4,
     8 11.3,10.4,9.62,8.99,8.49,8.10,7.82,7.65,7.59,
     9 35.0,33.9,31.2,27.7,24.2,21.1,18.5,16.3,14.5,13.0,
     a 11.8,10.8,10.0,9.34,8.81,8.40,8.10,7.92,7.85,
     1 38.4,37.0,33.9,29.9,25.9,22.4,19.5,17.1,15.2,13.6,
     2 12.3,11.3,10.4,9.70,9.13,8.70,8.38,8.18,8.11,
     3 42.0,40.4,36.7,32.1,27.7,23.8,20.6,18.0,15.9,14.2,
     4 12.8,11.7,10.8,10.0,9.45,8.99,8.66,8.45,8.38,
     5 45.8,43.9,39.7,34.4,29.5,25.2,21.7,18.9,16.6,14.8,
     6 13.4,12.2,11.2,10.4,9.77,9.29,8.94,8.72,8.64,
     7 49.9,47.7,42.8,36.8,31.3,26.6,22.8,19.8,17.4,15.4,
     8 13.9,12.6,11.6,10.8,10.1,9.59,9.21,8.99,8.90,
     9 54.3,51.7,46.0,39.3,33.2,28.1,23.9,20.7,18.1,16.1,
     a 14.4,13.1,12.0,11.1,10.4,9.88,9.49,9.25,9.17,
     1 58.9,56.0,49.5,41.9,35.2,29.5,25.1,21.6,18.9,16.7,
     2 14.9,13.5,12.4,11.5,10.7,10.2,9.77,9.52,9.43,
     3 63.8,60.4,53.0,44.6,37.1,31.0,26.2,22.5,19.6,17.3,
     4 15.5,14.0,12.8,11.8,11.1,10.5,10.1,9.79,9.69,
     5 69.0,65.1,56.8,47.3,39.2,32.6,27.4,23.4,20.4,17.9,
     6 16.0,14.5,13.2,12.2,11.4,10.8,10.3,10.1,9.96,
     7 74.6,70.1,60.6,50.2,41.2,34.1,28.6,24.4,21.1,18.6,
     8 16.5,14.9,13.6,12.6,11.7,11.1,10.6,10.3,10.2,
     9 80.4,75.3,64.6,53.1,43.3,35.7,29.8,25.3,21.9,19.2,
     a 17.1,15.4,14.0,12.9,12.0,11.4,10.9,10.6,10.5,
     1 86.5,80.7,68.8,56.0,45.5,37.2,31.0,26.3,22.7,19.8,
     2 17.6,15.8,14.4,13.3,12.4,11.7,11.2,10.9,10.8,
     3 93.0,86.4,73.1,59.1,47.6,38.8,32.2,27.2,23.4,20.5,
     4 18.2,16.3,14.8,13.6,12.7,12.0,11.5,11.1,11.0,
     5 99.8,92.4,77.5,62.2,49.9,40.5,33.4,28.2,24.2,21.1,
     6 18.7,16.8,15.2,14.0,13.0,12.3,11.7,11.4,11.3,
     7 107.,98.6,82.1,65.3,52.1,42.1,34.7,29.2,25.0,21.8,
     8 19.3,17.2,15.7,14.4,13.4,12.6,12.0,11.7,11.5,
     9 114.,105.,86.8,68.6,54.4,43.7,35.9,30.1,25.8,22.4,
     a 19.8,17.7,16.1,14.7,13.7,12.9,12.3,11.9,11.8,
     1 122.,112.,91.7,71.9,56.7,45.4,37.2,31.1,26.6,23.1,
     2 20.3,18.2,16.5,15.1,14.0,13.2,12.6,12.2,12.1,
     3 130.,119.,96.7,75.2,59.0,47.1,38.4,32.1,27.4,23.7,
     4 20.9,18.7,16.9,15.5,14.3,13.5,12.9,12.5,12.3,
     5 139.,126.,102.,78.6,61.3,48.8,39.7,33.1,28.1,24.4,
     6 21.4,19.1,17.3,15.8,14.7,13.8,13.2,12.8,12.6,
     7 148.,134.,107.,82.1,63.7,50.5,41.0,34.1,28.9,25.0,
     8 22.0,19.6,17.7,16.2,15.0,14.1,13.4,13.0,12.9,
     9 157.,141.,112.,85.6,66.1,52.2,42.2,35.1,29.7,25.7,
     a 22.6,20.1,18.1,16.6,15.3,14.4,13.7,13.3,13.1,
     1 167.,150.,118.,89.1,68.5,53.9,43.5,36.1,30.5,26.4,
     2 23.1,20.6,18.5,16.9,15.7,14.7,14.0,13.6,13.4,
     3 177.,158.,124.,92.7,71.0,55.6,44.8,37.1,31.3,27.0,
     4 23.7,21.0,19.0,17.3,16.0,15.0,14.3,13.8,13.7,
     5 188.,167.,129.,96.4,73.4,57.4,46.1,38.1,32.2,27.7,
     6 24.2,21.5,19.4,17.7,16.3,15.3,14.6,14.1,13.9,
     7 199.,176.,135.,100.,75.9,59.2,47.4,39.1,33.0,28.3,
     8 24.8,22.0,19.8,18.0,16.7,15.6,14.9,14.4,14.2,
     9 210.,185.,141.,104.,78.4,60.9,48.8,40.1,33.8,29.0,
     a 25.3,22.5,20.2,18.4,17.0,15.9,15.1,14.6,14.5,
     1 222.,194.,147.,108.,80.9,62.7,50.1,41.1,34.6,29.7,
     2 25.9,22.9,20.6,18.8,17.3,16.2,15.4,14.9,14.7,
     3 234.,204.,153.,111.,83.5,64.5,51.4,42.2,35.4,30.3,
     4 26.4,23.4,21.0,19.1,17.7,16.5,15.7,15.2,15.0,
     5 247.,214.,160.,115.,86.0,66.3,52.7,43.2,36.2,31.0,
     6 27.0,23.9,21.5,19.5,18.0,16.8,16.0,15.5,15.3,
     7 260.,224.,166.,119.,88.6,68.1,54.1,44.2,37.0,31.7,
     8 27.6,24.4,21.9,19.9,18.3,17.2,16.3,15.7,15.5,
     9 274.,235.,173.,123.,91.2,69.9,55.4,45.2,37.9,32.3,
     a 28.1,24.9,22.3,20.3,18.7,17.5,16.6,16.0,15.8,
     1 288.,246.,179.,127.,93.8,71.7,56.8,46.3,38.7,33.0,
     2 28.7,25.3,22.7,20.6,19.0,17.8,16.8,16.3,16.1,
     3 303.,257.,186.,131.,96.4,73.6,58.1,47.3,39.5,33.7,
     4 29.3,25.8,23.1,21.0,19.3,18.1,17.1,16.5,16.3,
     5 318.,269.,193.,135.,99.1,75.4,59.5,48.4,40.3,34.4,
     6 29.8,26.3,23.6,21.4,19.7,18.4,17.4,16.8,16.6,
     7 334.,280.,200.,139.,102.,77.2,60.8,49.4,41.2,35.0,
     8 30.4,26.8,24.0,21.7,20.0,18.7,17.7,17.1,16.9,
     9 350.,293.,207.,143.,104.,79.1,62.2,50.4,42.0,35.7,
     a 31.0,27.3,24.4,22.1,20.3,19.0,18.0,17.4,17.1,
     1 366.,305.,214.,148.,107.,81.0,63.5,51.5,42.8,36.4,
     2 31.5,27.8,24.8,22.5,20.7,19.3,18.3,17.6,17.4,
     3 383.,318.,221.,152.,110.,82.8,64.9,52.5,43.6,37.1,
     4 32.1,28.2,25.2,22.9,21.0,19.6,18.5,17.9,17.7,
     5 401.,331.,229.,156.,112.,84.7,66.3,53.6,44.5,37.8,
     6 32.7,28.7,25.7,23.2,21.3,19.9,18.8,18.2,17.9,
     7 419.,344.,236.,160.,115.,86.6,67.7,54.6,45.3,38.4,
     8 33.2,29.2,26.1,23.6,21.7,20.2,19.1,18.4,18.2,
     9 438.,357.,244.,165.,118.,88.5,69.0,55.7,46.1,39.1,
     a 33.8,29.7,26.5,24.0,22.0,20.5,19.4,18.7,18.4,
     1 457.,371.,251.,169.,121.,90.4,70.4,56.7,47.0,39.8,
     2 34.4,30.2,26.9,24.3,22.3,20.8,19.7,19.0,18.7,
     3 477.,385.,259.,173.,123.,92.2,71.8,57.8,47.8,40.5,
     4 34.9,30.7,27.3,24.7,22.7,21.1,20.0,19.3,19.0,
     5 498.,400.,267.,177.,126.,94.1,73.2,58.9,48.7,41.2,
     6 35.5,31.1,27.8,25.1,23.0,21.4,20.3,19.5,19.2,
     7 519.,415.,275.,182.,129.,96.0,74.6,59.9,49.5,41.8,
     8 36.1,31.6,28.2,25.5,23.4,21.7,20.5,19.8,19.5,
     9 540.,430.,283.,186.,132.,98.0,76.0,61.0,50.3,42.5,
     a 36.6,32.1,28.6,25.8,23.7,22.1,20.8,20.1,19.8,
     1 563.,445.,291.,191.,135.,99.9,77.4,62.0,51.2,43.2,
     2 37.2,32.6,29.0,26.2,24.0,22.4,21.1,20.3,20.0,
     3 585.,460.,299.,195.,137.,102.,78.8,63.1,52.0,43.9,
     4 37.8,33.1,29.4,26.6,24.4,22.7,21.4,20.6,20.3,
     5 609.,476.,307.,200.,140.,104.,80.2,64.2,52.8,44.6,
     6 38.4,33.6,29.9,27.0,24.7,23.0,21.7,20.9,20.6,
     7 633.,493.,315.,204.,143.,106.,81.6,65.2,53.7,45.3,
     8 38.9,34.1,30.3,27.3,25.0,23.3,22.0,21.2,20.8,
     9 657.,509.,324.,209.,146.,108.,83.0,66.3,54.5,45.9,
     a 39.5,34.6,30.7,27.7,25.4,23.6,22.3,21.4,21.1,
     1 683.,526.,332.,213.,149.,110.,84.4,67.4,55.4,46.6,
     2 40.1,35.0,31.1,28.1,25.7,23.9,22.5,21.7,21.4,
     3 709.,543.,341.,218.,152.,111.,85.8,68.4,56.2,47.3,
     4 40.6,35.5,31.6,28.5,26.0,24.2,22.8,22.0,21.6,
     5 735.,560.,349.,222.,154.,113.,87.2,69.5,57.1,48.0,
     6 41.2,36.0,32.0,28.8,26.4,24.5,23.1,22.2,21.9,
     7 762.,578.,358.,227.,157.,115.,88.6,70.6,57.9,48.7,
     8 41.8,36.5,32.4,29.2,26.7,24.8,23.4,22.5,22.2,
     9 790.,596.,367.,232.,160.,117.,90.1,71.7,58.8,49.4,
     a 42.4,37.0,32.9,29.6,27.1,25.1,23.7,22.8,22.4,
     1 819.,614.,375.,236.,163.,119.,91.5,72.7,59.6,50.1,
     2 42.9,37.5,33.3,30.0,27.4,25.4,24.0,23.1,22.7,
     3 848.,632.,384.,241.,166.,121.,92.9,73.8,60.4,50.7,
     4 43.5,38.0,33.7,30.3,27.7,25.7,24.3,23.3,23.0,
     5 878.,651.,393.,246.,169.,123.,94.3,74.9,61.3,51.4,
     6 44.1,38.5,34.1,30.7,28.1,26.1,24.5,23.6,23.2,
     7 909.,670.,402.,250.,172.,125.,95.8,76.0,62.1,52.1,
     8 44.7,39.0,34.6,31.1,28.4,26.4,24.8,23.9,23.5,
     9 940.,690.,411.,255.,175.,127.,97.2,77.1,63.0,52.8,
     a 45.2,39.5,35.0,31.5,28.7,26.7,25.1,24.2,23.8,
     1 972.,709.,420.,260.,178.,129.,98.6,78.2,63.9,53.5,
     2 45.8,39.9,35.4,31.8,29.1,27.0,25.4,24.4,24.0,
     31005.,729.,429.,264.,180.,131.,100.,79.3,64.7,54.2,
     4 46.4,40.4,35.8,32.2,29.4,27.3,25.7,24.7,24.3,
     51038.,749.,439.,269.,183.,133.,101.,80.4,65.6,54.9,
     6 46.9,40.9,36.2,32.6,29.7,27.6,26.0,25.0,24.6,
     71072.,770.,448.,274.,186.,135.,103.,81.5,66.4,55.6,
     8 47.5,41.3,36.6,32.9,30.1,27.9,26.3,25.2,24.8,
     91107.,791.,457.,279.,189.,137.,104.,82.6,67.3,56.3,
     a 48.0,41.8,37.0,33.3,30.4,28.2,26.5,25.5,25.1,
     11143.,812.,467.,284.,192.,139.,106.,83.7,68.2,57.0,
     2 48.6,42.2,37.4,33.6,30.7,28.5,26.8,25.8,25.4,
     31180.,833.,476.,288.,195.,141.,107.,84.8,69.1,57.7,
     4 49.1,42.7,37.7,33.9,31.0,28.8,27.1,26.1,25.6,
     51217.,855.,485.,293.,198.,143.,109.,85.9,69.9,58.4,
     6 49.6,43.1,38.1,34.3,31.4,29.1,27.4,26.3,25.9,
     71255.,877.,495.,298.,201.,145.,110.,87.0,70.8,59.1,
     8 50.2,43.5,38.4,34.6,31.7,29.5,27.7,26.6,26.2,
     91294.,899.,505.,303.,204.,147.,112.,88.2,71.7,59.8,
     a 50.7,43.9,38.7,34.9,32.0,29.8,28.0,26.9,26.4,
     11333.,921.,514.,308.,207.,149.,113.,89.3,72.6,60.5,
     2 51.2,44.2,39.0,35.2,32.3,30.1,28.3,27.2,26.7/),
     3         shape(abskou))

C  ---------------------------------------------------------------------

      integer :: SEType,SESpHarm=1,SEScales=2,SEAll=3,CovTest,
     1           NUniXShape,NAllXShape,ScFrMethodStepLike=0,
     2           ScFrMethodInterpol=1,
     2           NRefMaxXShape=-1,NCyklSemiEmpir=500,LSMethodSemiEmpir=2
      integer, allocatable :: RunScM(:,:),RunScGrN(:,:)
      logical MameDirCos
      logical, allocatable :: ScBrat(:,:)
      real, allocatable :: ScFrame(:,:),SpHAlm(:,:)
      character*80 :: FormatEqv='(.i4,2f8.2,i4,6f8.5,f15.3)'
      real ::  CutMinXShape=-1.,CutMaxXShape=-1.,FLimCullXShape=-1.,
     1         TlumSemiEmpir=1.,MarqLamSemiEmpir=.001

C  ---------------------------------------------------------------------

      integer, allocatable :: ihar(:,:),irec(:),irecp(:),ireca(:),
     1                        ipor(:),inca(:),KPhAr(:),MltAr(:),
     2                        NAveRedAr(:)
      real, allocatable :: riar(:),rsar(:),gridp(:,:),RIAveAr(:),
     1                     RSAveAr(:),DiffAveAr(:),RunCCDAr(:)

C  ---------------------------------------------------------------------

      integer, allocatable :: HDr(:,:),HDrTw(:),ColDr(:),PorDr(:),
     1                        FlgAr(:),HCondAr(:,:),HCondAveAr(:),
     2                        Ave2Org(:,:),HCondWdFAr(:),FoInd2Ave(:),
     3                        RefBlockAr(:),ICullAr(:),IXDr(:),OrderX(:)
      integer NDrAlloc,SimNDr,NRefRead,NRefReadObs,NRefAlloc,
     1        NAveRedMax,NAveMax,NAveRed,NAve,NTotallyCulled,GasketUsed,
     2        DiamondUsed,DisplaySelect,NRefWdF,NRefWdFMax,XUse,YUse,
     3        MaxRefBlockAr,NRefBlockAr,ReflUse,XDNDat
      real, allocatable :: WdFAr(:),FoAr(:),FsAr(:),FcAr(:),RIDr(:),
     1                     RSDr(:),XDr(:),YDr(:),RadDr(:),IcAr(:)
      real RadDrMax,GasketTheta(22),GasketDeltaTheta,DiamondTheta(22),
     1     DiamondDeltaTheta,ScSgTest,ObsLevelCull
      logical :: CulledExist,OnceMeasuredExist,TwiceMeasuredExist,
     1           CompletelyExcludedExist,Zoomed,XDCteI,XLinear=.true.

C  ---------------------------------------------------------------------

      logical :: UseInSGTest(:),TakeSGTwin = .false.,
     1           TakeOldTwin = .false.
      allocatable UseInSGTest
      integer iLaueString,nLaueString,OLaueString(:),RLaueString(:),
     1        nSelLaue,IdSymmFull,IdSymmFullTr
      integer :: TakeSGTwinOld=-1, TakeOldTwinOld=-1
      character*80 LaueString(:)
      allocatable LaueString,OLaueString,RLaueString
      real TrMPOrg(36)

C  ---------------------------------------------------------------------

      integer :: SimType=1,SimDir(3)=(/0,0,1/),SimDirSel=3,
     1           SimMainLayer=0,SimSatLayer(3)=(/0,0,0/),
     2           SimColorMain(3),SimColorSat,SimColorPlus,SimColorMinus,
     3           SimColorUnobs,SimIZ,IdSim,SimColorTwin(18)
      logical :: SimAve=.true.,SimComplete=.true.,SimUseShading=.false.,
     1           SimUseScaledRad=.true.,SimDrawSat=.true.,
     2           SimDrawUnobs=.true.,SimProjectSat=.false.,
     3           SimDrawTwins=.false.,SimUseDefaultNet=.true.
      real ::    SimRadMin=1.,SimRadMax=5.,SimScope=50.,
     1           SimValRed=20.,SimDepth=0.,
     2           SinThLMax,SimValMax,SimDrLayer,SimTr(3,3),SimTrI(3,3),
     3           SimScale,SimLayer,
     4           XMinBasVec,XMaxBasVec,YMinBasVec,YMaxBasVec,
     5           SimNet(2,2)=reshape((/1.,0.,0.,1./),shape(SimNet))

C  ---------------------------------------------------------------------

      integer NRunSENJUMax,NRunSENJU,RunSENJU(:)
      allocatable RunSENJU
      end

      module Dist_mod
      real, allocatable :: oi(:),oj(:),ok(:),xdst(:,:),xdi(:,:),
     1          xdj(:,:),xdk(:,:),sxdi(:,:),sxdj(:,:),sxdk(:,:),
     2          BetaDst(:,:),dxm(:),um(:,:),dum(:),dam(:),dums(:),
     3          dams(:),dumm(:),dhm(:,:),dhms(:,:),TypicalDistUse(:,:),
     4          DirRef(:,:)
      logical, allocatable :: BratPrvni(:),BratDruhyATreti(:)
      integer :: iselPrvni,iselDruhyATreti,NDirRef=0,NDirRefMax=0
      character*8, allocatable :: aselPrvni(:),aselDruhyATreti(:)
      logical :: CalcHBonds = (.false.)
      end

      module EditM40_mod
      integer NAtActive,NAtActiveSel,NAtomicSel
      dimension IAtActive(:)
      logical LAtActive(:)
      character*8 NameAtActive(:)
      allocatable IAtActive,LAtActive,NameAtActive
      integer nan
      dimension kmodxn(:),xn(:,:),uxn(:,:,:),uyn(:,:,:),
     1          Rho(:)
      character*8 atomn(:)
      allocatable atomn,kmodxn,xn,uxn,uyn,Rho
      character*12 MolMenu(:)
      integer MaxMolPos
      allocatable MolMenu
      integer NTrans,TransKwSym(:,:)
      dimension TransMat(:,:,:),TransVec(:,:,:),TransX(:,:,:),
     1          TransM(:,:,:),TransZM(:),
     2          TransTemp(:,:,:),TransTempS(:,:,:),TransC3(:,:,:),
     3          TransC4(:,:,:),TransC5(:,:,:),TransC6(:,:,:),
     4          SignTransX(:)
      allocatable TransMat,TransVec,TransX,TransM,TransZM,TransTemp,
     1            TransTempS,TransC3,TransC4,TransC5,TransC6,SignTransX,
     2            TransKwSym
      logical AtBrat(:),MolAtBrat(:)
      integer isfn(:),isfhp,MolAt(:),MolAtisf(:)
      allocatable AtBrat,MolAtBrat,isfn,MolAt,MolAtisf
      end
      module Fourier_mod
      real xplus(3)
      real, allocatable :: SinTable(:,:,:),CosTable(:,:,:)
      logical Pridavat(3)
      integer TypeOfSearch
      integer, allocatable :: RhoIntArr(:),RhoPointArr(:),NRho(:)
      character*256 :: FouRefStr = ' '
      end
      module Grapht_mod
      real, allocatable :: DrawT(:),DrawValue(:,:),DrawOcc(:,:),
     1                     DrawOccP(:,:),XMod(:)
      logical :: GrtDrawX4 = .false.,GrtShowCrenel = .false.,
     1           GrtPlaneDir = .false., GrtESD = .true.
      character*256 :: GrtPlane(2) = (/' ',' '/)
      character*256, allocatable :: DrawAtom(:,:)
      character*80, allocatable :: DrawSymSt(:),DrawSymStU(:),
     1                             AtomArr(:)
      character*8 :: SmbU(7) =
     1  (/'U11    ','U22    ','U33    ','Ueq    ',
     2    'U(max) ','U(mean)','U(min) '/)
      integer :: PocetParType=12,NAtPlane=0,NAtCenter=0,DrawN,
     1           DrawNMax=0,NAtomArr=0
      integer, allocatable  :: DrawColor(:),DrawParam(:)
      character*20  :: MenuParType(12) =
     1                 (/'Occupancy           ',
     2                   'Position            ',
     3                   'Displacement        ',
     4                   'Angles              ',
     5                   'Torsion angle       ',
     6                   'Plane               ',
     7                   'ADP parameter       ',
     8                   'Distances           ',
     9                   'Individual distances',
     a                   'Individual angles   ',
     1                   'Bond valence sum    ',
     2                   'Magnetic moment     '/)
      end
      module Powder_mod
      integer, allocatable :: PeakXPosI(:),PeakPor(:),YfPwd(:),
     1                        KPhArr(:),TMapTOF(:,:,:),ClckWdtTOF(:),
     2                        NPointsTimeMaps(:),KlicArr(:),indArr(:,:),
     3                        PhaseGr(:)
      integer :: KPhaseSolve = 0,ipointA = 0,iorderA = 0,
     1                           ipointI = 0,iorderI = 0, NIdM41 = 42,
     2           NTimeMaps,NPointsTimeMapsMax,BraggLabel = 0
      logical :: IncSpectNorm = .false.
      real :: TwoThetaLimits(3)=(/3.,177.,0.01/), XPwdMin(10),
     1        XPwdMax(10)
      real, allocatable :: XManBackg(:,:),YManBackg(:,:),MultArr(:),
     1                     LeBailPrfArr(:),ICalcArr(:),CalcPArr(:),
     2                     CalcPPArr(:),BkgArr(:),DifArr(:),BraggArr(:),
     3                     ShiftArr(:),FWHMArr(:),ypeakArr(:),
     4                     PeakXPos(:),PeakInt(:),PeakFWHM(:),
     5                     PeakBckg(:,:),PeakEta(:),RDeg1Arr(:),
     6                     RDeg2Arr(:),CutCoef1Arr(:),CutCoef2Arr(:),
     7                     XPwd(:),YoPwd(:),YcPwd(:),YbPwd(:),YsPwd(:),
     8                     YiPwd(:),YcPwdInd(:,:)
      character*10 :: IdM41(50) =
     1        (/'bckgtype ','bckgnum  ','manbckg  ','wtlebail ',
     2          'absor    ','mir      ','skipfrto ','phase    ',
     3          'proffun  ','asymm    ','partbroad','strain   ',
     4          'prefor   ','prefsum  ','cutoff   ','dirpref  ',
     5          'dirbroad ','maxsat   ','satfrmod ','skipfrdl ',
     6          'rough    ','datblock ','reflam   ','useinvx  ',
     7          'radprim  ','radsec   ','usersw   ','usefdsa  ',
     8          'usepsoll ','usessoll ','lamfile  ','cutoffmn ',
     9          'cutoffmx ','usetofab ','useds    ','illum    ',
     a          'focusBB  ','uklebail ','tofjason ','tofkey   ',
     1          'usehs    ','broadHKL ','#r43     ','#r44     ',
     2          '#r45     ','#r46     ','#r47     ','#r48     ',
     3          '#r49     ','#r50     '/)
      end


      module Refine_mod

C  ---------------------------------------------------------------------

      integer ICykl,ICyklMod6,mic,NCyklNew,nPowder,nSingle,ngrid(3),
     1        nxxn,nxxp(20),nFlowChart,NUseDatBlockActual,NRestDistAng,
     2        LSMethodNew
      logical, allocatable :: UseDatBlockActual(:)
      logical DelejStatistiku,CalcDer,Okraj,ContrExceedMessage,
     1        VisiMsgSc,VisiMsgRf,MakeItPermanent,RefSeriousWarnings
      real XdQuestRef,MarqLamNew,TlumNew
      integer :: ksimul = 1
      logical :: AnnouncedTwinProblem = .false.,
     1           FinalInver = .false., ExistMagnetic = .false.,
     2           RefKeyBoardInput=.false.
      character*256 :: FileSimul = ' '
      real, allocatable :: ScRes(:,:)


C  ---------------------------------------------------------------------

      double precision cosij,sinij,SumaYoPwd,SumaYcPwd
      real, allocatable :: yct(:,:),ycq(:),act(:,:),bct(:,:),afour(:,:),
     1                     bfour(:,:),snt(:),yctm(:,:),ycqm(:),
     2                     yctmp(:,:),ycqmp(:),F000(:,:),F000N(:,:)
      real fta,expij,ftacos,ftasin,cosijc,sinijc,Fobs,Fcalc,FcalcNucl,
     1     dy,dyp,wyoq,wdyq,a,b,affree,bffree,cosijm(3),sinijm(3),
     2     derme(12),afst,bfst,affreest,bffreest,sinthl,sinthlq,wt,yctw,
     3     yctwm,yctwmp,FCalcMag,FCalcMagPol,AMag(3),BMag(3),AMagR(3),
     4     BMagR(3),HMag(3,3),HMagR(3,3),RefDirCos(3,2),CReal,CImag,
     5     PhiAng,hj(3),HCoefj(84),phfp(3),XPwdDerTest,BroadHKL,
     6     UBRef(3,3),UBRefI(3,3)
      integer itw,itwr,iq,ihread(6),mj,nj,pj,mmabs(3),mmabsm,
     1        Maxmmabsm,ihref(6,3),HDerTest(6),DatBlockDerTest,
     2        IBroadHKL
      logical Nulova,HKLF5File,nicova,nic,Imag,MinusReal,MinusImag,
     1        atomic,nemod,okr(3),CorrScPwd

C  ---------------------------------------------------------------------

      character*137 RefHeader,RefHeaderMagPol
      character*128 ivenr(4)
      character*35 format3,format3p

C  ---------------------------------------------------------------------

      integer nfixpopv,npfixpopv

C  ---------------------------------------------------------------------

      real rxs,rxm,rxa(11)
      integer kxa(11),InvDel

C  ---------------------------------------------------------------------

      integer ihov(6),nchkr
      real difh(6),thdif(2),omdif(2),chidif(2)

C  ---------------------------------------------------------------------

      integer RefRandomSeed
      logical RefRandomize
      real    RefRandomDiff


C  ---------------------------------------------------------------------

      integer iab,ExtTensor(:),ExtType(:),ExtDistr(:)
      real aa(10),ba(10),dadmi(19),efpip(7),RefLam,extkor,extkorm,
     1     ExtRadius(:)
      allocatable ExtTensor,ExtType,ExtDistr,ExtRadius

C  ---------------------------------------------------------------------

      dimension ParSup(:),snw(:,:),csw(:,:),skfx1(:),skfx2(:),skfx3(:),
     1          x4low(:,:),x4length(:,:),XGauss(:,:,:),WGauss(:,:,:),
     2          snsinps(:), cssinps(:), sncosps(:),cscosps(:),
     3          snsinpsc(:),cssinpsc(:),sncospsc(:),cscospsc(:),
     4          rh(:,:),rk(:,:),rl(:,:),ath(:,:),atk(:,:),atl(:,:),t(:),
     5          imnp(:,:,:),HCoef(:,:,:),trezm(:,:),
     6          sngc(:,:,:),csgc(:,:,:),ScSup(:,:)
      integer PrvniParSup(:),DelkaParSup(:),NSuper(:,:,:)
      allocatable ParSup,snw,csw,skfx1,skfx2,skfx3,PrvniParSup,
     1            DelkaParSup,x4low,x4length,XGauss,WGauss,
     2            snsinps,cssinps,sncosps,cscosps,snsinpsc,cssinpsc,
     3            sncospsc,cscospsc,ScSup,NSuper,sngc,csgc,
     4            rh,rk,rl,ath,atk,atl,t,imnp,HCoef,trezm


C  ---------------------------------------------------------------------

      dimension KFixOrigin(:)
      character*12 AtFixOrigin(:),AtFixX4(:),PromAtFixX4(:)
      character*5 ::  cfix(10) = (/'all  ','xyz  ','u    ','beta ',
     1                             'mod  ','chden','pol  ','x4   ',
     2                             'ind  ','value'/)
      integer NFixOrigin,NFixX4,KFixX4(:)
      logical RizikoX4
      allocatable KFixOrigin,AtFixOrigin,AtFixX4,PromAtFixX4,KFixX4

C  ---------------------------------------------------------------------

      real, allocatable :: par(:),ader(:),bder(:),aderm(:,:),bderm(:,:),
     1                     der(:),dc(:),derm(:,:),dcm(:,:),dertw(:),
     2                     dertwm(:),dcp(:),derpol(:,:),LSMat(:),
     3                     LSRS(:),LSSol(:)
      integer, allocatable :: NSingArr(:),ki(:)

C  ---------------------------------------------------------------------

      dimension dori(:,:)
      allocatable dori

C  ---------------------------------------------------------------------

      dimension isaRef(:,:),ismRef(:),CentrRef(:),NSymmRef(:),
     1          NCSymmRef(:),ZCSymmRef(:)
      allocatable isaRef,ismRef,CentrRef,NSymmRef,NCSymmRef,ZCSymmRef

C  ---------------------------------------------------------------------

      integer nvai
      integer :: nvaiMax = 0
      integer, allocatable :: nai(:,:),naixb(:),nails(:),mai(:)
      real, allocatable :: sumai(:)
      character*12, allocatable :: atvai(:,:),RestType(:)

C  ---------------------------------------------------------------------

      integer, allocatable :: ihm(:,:),ihma(:,:),ihsn(:,:),ieqn(:),
     1                        imdn(:),ihsv(:,:),ieqv(:),imdv(:)
      integer :: NSkrtMax=0,NSkrt
      character*20, allocatable :: Skupina(:),Nevzit(:),Vzit(:)
      logical, allocatable ::  DontUse(:)

C  ---------------------------------------------------------------------

      integer, allocatable :: iqs(:),ihms(:,:),ihmas(:,:),
     1        ihssn(:,:),ieqsn(:),imdsn(:),
     2        ihssv(:,:),ieqsv(:),imdsv(:)
      integer :: NScalesMax=0,NScales
      character*20, allocatable :: scsk(:),scvzit(:),scnevzit(:)

C  ---------------------------------------------------------------------

      integer, allocatable  :: RFactMatrix(:,:),RFactVector(:,:),
     1                         CondRFactIncl(:,:),AbsRFactIncl(:),
     2                         ModRFactIncl(:),CondRFactExcl(:,:),
     3                         AbsRFactExcl(:),ModRFactExcl(:),
     4                         nRPartAll(:),nRPartObs(:)
      integer :: NRFactorsMax = 0,NRFactors,nRPartAllAll,nRPartAllObs
      double precision, allocatable ::  RDenPartAll(:), RNumPartAll(:),
     1                                  RDenPartObs(:), RNumPartObs(:),
     2                                 wRDenPartAll(:),wRNumPartAll(:),
     3                                 wRDenPartObs(:),wRNumPartObs(:)
      double precision  RDenPartAllAll, RNumPartAllAll,
     1                  RDenPartAllObs, RNumPartAllObs,
     2                 wRDenPartAllAll,wRNumPartAllAll,
     3                 wRDenPartAllObs,wRNumPartAllObs
      character*20, allocatable :: StRFact(:),StRFactIncl(:),
     1                             StRFactExcl(:)

C  ---------------------------------------------------------------------

      integer, allocatable  :: nRZoneAll(:,:),nRZoneObs(:,:)
      double precision, allocatable ::
     1   RDenZoneAll(:,:), RNumZoneAll(:,:),
     2   RDenZoneObs(:,:), RNumZoneObs(:,:),
     3  wRDenZoneAll(:,:),wRNumZoneAll(:,:),
     4  wRDenZoneObs(:,:),wRNumZoneObs(:,:)

C  ---------------------------------------------------------------------

      character*80 WhatHasMaxChange(6)
      character*9 :: StRFac(6)=(/'R(obs)=  ','wR(obs)= ','wR2(obs)=',
     1                           'R(all)=  ','wR(all)= ','wR2(all)='/)
      double precision RNumObs(:), RNumAll(:), RDenObs(:), RDenAll(:),
     1                wRNumObs(:),wRNumAll(:),wRDenObs(:),wRDenAll(:),
     2                RINumObs, RINumAll, RIDenObs, RIDenAll,
     3               wRINumObs,wRINumAll,wRIDenObs,wRIDenAll,
     4                RNumProf,  RDenProf,wRNumProf,wRDenProf,
     5               cRDenProf,wcRDenProf,
     6                RNumOverall,RNumOverallObs,
     7                RDenOverall,RDenOverallObs,
     8                RFacOverall,RFacOverallObs,
     9                wRNumOverall,wRNumOverallObs,
     a                wRDenOverall,wRDenOverallObs,
     1                cRDenOverall,wcRDenOverall,
     2                 SumaDyQAll,SumaDyQQAll,SumaSigQAll,
     3                 SumaDyQObs,SumaDyQQObs,SumaSigQObs
      real  RFacObs(:,:,:,:),  RFacAll(:,:,:,:),
     1     wRFacObs(:,:,:,:), wRFacAll(:,:,:,:),
     2     RIFacObs(:,:,:),   RIFacAll(:,:,:),
     3     wRIFacObs(:,:,:),  wRIFacAll(:,:,:),
     4      RFacProf(:,:),     wRFacProf(:,:),
     5     cRFacProf(:,:),    wcRFacProf(:,:),
     6     eRFacProf(:),
     7     GOFProf(:,:),GOFObs(:,:),GOFAll(:,:),
     8     wRFacOverall,wRFacOverallObs,cRFacOverall,wcRFacOverall,
     9     eRFacOverall,GOFOverall,GOFOverAllObs,DampFac(6),
     a     ChngSUAve(6),ChngSUMax(6),wdyOld,wdyqOld,RLast(8),TlumOrigin
      integer nRObs(:),nRAll(:), nRFacObs(:,:,:,:), nRFacAll(:,:,:,:),
     1                          nRIFacObs(:,:,:),  nRIFacAll(:,:,:),
     2        nRFacProf(:),NSkipRef(:,:),NBadOverRef(:,:),
     3        KDatBlockUsed,NDynRed,nRLast,LstRef,LstSing,
     4        nRFacOverall,nRFacOverallObs,IStRFac(6)
      allocatable RNumObs, RNumAll, RDenObs, RDenAll,
     1            wRNumObs,wRNumAll,wRDenObs,wRDenAll,
     2            RFacObs,RFacAll,wRFacObs,wRFacAll,
     3            RIFacObs,RIFacAll,wRIFacObs,wRIFacAll,
     4            RFacProf,wRFacProf,cRFacProf,wcRFacProf,
     5           eRFacProf,GOFProf,GOFObs,GOFAll,nRObs,nRAll,nRFacObs,
     6           nRFacAll,nRIFacObs,nRIFacAll,nRFacProf,NSkipRef,
     7           NBadOverRef

C  ---------------------------------------------------------------------

      integer nLSRS,npsPwd,nLSMat,NSing,NPisSing,NParRef,nzz,noread,
     1        MaxDerAtom,LastNp,NConstrain,NRestrain,
     2        NParStr(:),NParData(:),NParPwd(:,:),NParPwdAll(:),
     3        NParStrAll(:),NDerNemodFirst,NDerNemodLast,
     4        NDerModFirst,NDerModLast,
     5        NDerMagFirst,NDerMagLast,NDerAll
      real    BerarS,BerarCorr
      logical JenSc
      allocatable NParStr,NParData,NParPwd,NParPwdAll,NParStrAll

C  ---------------------------------------------------------------------

      dimension snls(:,:,:),csls(:,:,:)
      allocatable snls,csls

C  ---------------------------------------------------------------------

      integer AtDisableNOld,AtDisableNNew
      real AtDisableFact(:),fyr(:)
      logical AtDisable(:)
      allocatable AtDisable,AtDisableFact,fyr

C  ---------------------------------------------------------------------

      dimension RMagDer(:,:,:)
      allocatable RMagDer

C  ---------------------------------------------------------------------

      character*8 ::  CKeepType(3) = (/'hydro','geom ','ADP  '/)
      character*8 ::  CKeepGeom(3) = (/'plane   ','rigid   ',
     1                                 'rigidmod'/)
      character*8 ::  CKeepHydro(3) =
     1                (/'tetrahed','triang  ','apical  '/)
      character*8 ::  CKeepADP(2) = (/'riding','      '/)
      character*80, allocatable :: KeepAt(:,:),KeepAtCentr(:),
     1                             KeepAtNeigh(:,:),KeepAtH(:,:),
     2                             KeepAtAnchor(:)
      integer :: NAtMax,NKeep,NKeepAt,lnda,NKeepMax=0,NKeepAtMax=0,
     1           NKeepRigidAll=0
      integer, allocatable :: KeepType(:),KeepN(:),KeepNAt(:,:),
     1                        KeepKiX(:,:),KeepNAtCentr(:),
     2                        KeepKiXCentr(:),KeepNNeigh(:),
     3                        KeepNAtNeigh(:,:),KeepKiXNeigh(:,:),
     4                        KeepNH(:),KeepNAtH(:,:),KeepKiXH(:,:),
     5                        KeepNAtAnchor(:),KeepKiXAnchor(:),
     6                        NKeepRigid(:)
      logical UseAnchor
      real :: KeepDistHDefault(3)=(/-1.,-1.,-1./)
      real, allocatable :: KeepDistH(:),KeepAngleH(:),KeepADPExtFac(:)

C  ---------------------------------------------------------------------

      integer, allocatable :: MagneticRFac(:)

C  ---------------------------------------------------------------------

      character*12 :: lk1 = 'kappa',lk2 = 'kappa''',lTOFAbsPwd='TOFAbs',
     1                lPrefPwd = 'pref', lRoughPwd = 'rough',
     2                lBackgPwd = 'bckg',lLamPwd = 'WLen',lStPwd='St',
     3                lZetaPwd='Zeta',lAsymPwd='asym',
     4                lBroadHKL='BroadHKL'
      character*12 :: lBasicPar(12) = (/'ai    ','x     ','y     ',
     1                                  'z     ','phi   ','chi   ',
     2                                  'psi   ','Uiso  ','RhoIso',
     3                                  'GIso  ','Phason','o     '/)
      character*12 :: lcell(6) = (/'a    ','b    ','c    ','alpha',
     1                             'beta ','gamma'/)
      character*12 :: lShiftPwd(3) = (/'shift','sycos','sysin'/)
      character*12 :: lTOF1Pwd(3) = (/'zero','difc','difa'/)
      character*12 :: lTOF2Pwd(8) = (/'ZeroT ','CT    ','AT    ',
     1                                'ZeroE ','CE    ',
     2                                'Wcross','Tcross','nic   '/)
      character*12 :: lGaussPwd(4) = (/'GU','GV','GW','GP'/)
      character*12 :: lGaussPwdF(4) = (/'CSizeG  ','CSizeGA ',
     1                                 'StrainG ','StrainGA'/)
      character*12 :: lGaussPwdTOF(3) = (/'Sig0','Sig1','Sig2'/)
      character*12 :: lLorentzPwd(5) = (/'LX ','LXe','LY ','LYe','   '/)
      character*12 :: lLorentzPwdF(5) = (/'CSizeL  ','CSizeLA ',
     1                                   'StrainL ','StrainLA',
     2                                   '        '/)
      character*12 :: lLorentzPwdTOF(5) = (/'Gam0 ','Gam1 ','Gam2 ',
     4                                      'Gam1e','Gam2e'/)
      character*12 :: lAsymPwdD(4) = (/'H/L  ','S/L  ','HpS/L','HmS/L'/)
      character*12 :: lAsymPwdF(8) = (/'RSW  ','FDSA ','XLen ','SLen ',
     1                                'RLen ','PSoll','SSoll','VDSL '/)
      character*12 :: lAsymTOF1Pwd(4)= (/'alpha0','alpha1','beta0 ',
     1                                   'beta1 '/)
      character*12 :: lAsymTOF2Pwd(8)= (/'alpha0e','alpha1e','beta0e ',
     1                                   'beta1e ',
     2                                   'alpha0t','alpha1t','beta0t ',
     3                                   'beta1t '/)
      character*12 :: lAsymPwdDI(6) = (/'RelBWidth ',
     1                                  'PixSize   ',
     2                                  'BeamHeight',
     3                                  'CapDiam   ',
     4                                  'BeamDiv   ',
     5                                  'RadS2I    '/)
      character*12 :: lEDVar(6) = (/'EDScale',
     1                              'EDThick',
     2                              'EDXNorm',
     3                              'EDYNorm',
     4                              'EDPhi  ',
     5                              'EDTheta'/)
      end
      module Bessel_mod
      dimension besp(:,:),dbfdu1(:,:),dbfdu2(:,:),
     1          dchidu1(:),dchidu2(:),dsdu1(:),dsdu2(:),chi(:),
     2          dadu1(:),dadu2(:),dbdu1(:),dbdu2(:),dsdchi(:),
     3          sb(:),dsbdax(:),dsbday(:),ksi(:),dksidax(:),
     4          dksiday(:),dadax(:),daday(:),dbdax(:),dbday(:),
     5          besb(:,:),dbfdb1(:,:),dbfdb2(:,:),
     3          detadb1(:),detadb2(:),dsdb1(:),dsdb2(:),eta(:),
     4          dadb1(:),dadb2(:),dbdb1(:),dbdb2(:),dsdeta(:),
     5          dsdb3(:),dsdu3(:),dbfdb3(:,:),dbfdu3(:,:),
     6          daxddlt(:),dayddlt(:),daxdx40(:,:),daydx40(:,:),
     7          daxdfct(:,:),daydfct(:,:),mx(:),mxs(:),mxkk(:),
     8          mb(:),mbs(:),mbk(:),mxdkw1(:),mxdkw2(:),mxdkw3(:),
     9          mbdkw1(:),mbdkw2(:),mbdkw3(:),accur(:)
      real ksi
      allocatable besp,dbfdu1,dbfdu2,dchidu1,dchidu2,dsdu1,dsdu2,chi,
     1          dadu1,dadu2,dbdu1,dbdu2,dsdchi,sb,dsbdax,dsbday,ksi,
     2          dksidax,dksiday,dadax,daday,dbdax,dbday,besb,dbfdb1,
     3          dbfdb2,detadb1,detadb2,dsdb1,dsdb2,eta,dadb1,dadb2,
     4          dbdb1,dbdb2,dsdeta,dsdb3,dsdu3,dbfdb3,dbfdu3,daxddlt,
     5          dayddlt,daxdx40,daydx40,daxdfct,daydfct,mx,mxs,mxkk,
     6          mb,mbs,mbk,mxdkw1,mxdkw2,mxdkw3,mbdkw1,mbdkw2,mbdkw3,
     7          accur
      integer :: mxb = 50
      logical kolaps
      end
      module RefPowder_mod
      dimension ihPwdArr(:,:),KPhPwdArr(:),IQPwdArr(:),FCalcPwdArr(:),
     1          ypeaka(:,:),peaka(:,:),pcota(:,:),rdega(:,:,:),
     2          shifta(:,:),fnorma(:,:),tntsima(:,:),ntsima(:,:),
     3          sqsga(:,:),fwhma(:,:),sigpa(:,:),etaPwda(:,:),
     4          dedffga(:,:),dedffla(:,:),dfwdga(:,:),dfwdla(:,:),
     5          coef(:,:,:),coefp(:,:,:),coefq(:,:,:),Prof0(:),
     6          AxDivProfA(:,:,:),DerAxDivProfA(:,:,:,:),FDSProf(:,:,:),
     7          cotg2tha(:,:),cotgtha(:,:),cos2tha(:,:),cos2thqa(:,:),
     8          Alpha12a(:,:),Beta12a(:,:),IBroadHKLa(:)
      integer :: MxRefPwd
      real MultPwdArr(:)
      allocatable ihPwdArr,KPhPwdArr,IQPwdArr,MultPwdArr,FCalcPwdArr,
     1            ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,ntsima,
     2            sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,dfwdga,
     3            dfwdla,coef,coefp,coefq,Prof0,AxDivProfA,
     4            DerAxDivProfA,FDSProf,cotg2tha,cotgtha,cos2tha,
     5            cos2thqa,Alpha12a,Beta12a,IBroadHKLa
      end
      module Direct_mod
      real, allocatable :: fo(:),rho(:),ew(:),ex(:),EEE(:)
      integer, allocatable :: hkl(:),ids(:),ipr(:),ix1(:),ix2(:),ir1(:),
     1                        loc(:),NSig(:),NPsi(:),Ph1(:),Ph2(:)
      integer NTriplets,NTripletsMax
      end
      module GoToSubGr_mod
      integer, allocatable :: io(:,:),ios(:,:),ips(:),ipr(:),
     1                        IswSymms(:),ipri(:),iopc(:),itrl(:),
     2                        SelSubGr(:,:),NGrpSubGr(:),PorGrpSubGr(:)
      real, allocatable :: rm6s(:,:),s6s(:,:),vt6s(:,:),ZMagS(:),
     1                     RMagS(:,:)
      integer NSubGr,ind,indc,nvtp
      real :: StSymL=(400.)
      character*80, allocatable ::  StSym(:),GrpSubGr(:),SysSubGr(:)
      character*5, allocatable :: OpSmb(:),OpSmbS(:)
      logical, allocatable :: BratSymmSub(:)
!      allocatable ips,ipr,rm6s,s6s,ipri,iopc,itrl,StSym,BratSymmSub,
!     1            vt6s,io,OpSmb,ios,OpSmbS,IswSymms,SelSubGr,GrpSubGr,
!     2            SysSubGr,NGrpSubGr,PorGrpSubGr,ZMagS,RMagS
      end
      module Chargeden_mod
      logical AtBer(:),AtBerEDMA(:)
      allocatable AtBer,AtBerEDMA
      end


      module Atoms_mod
      integer lasmaxm,ifrmax,itfmax,MagParMax,
     1        PrvniKiAtInd,PosledniKiAtInd,
     2        PrvniKiAtPos,PosledniKiAtPos,
     3        PrvniKiAtMol,PosledniKiAtMol,
     4        PrvniKiAtXYZMode,PosledniKiAtXYZMode,
     5        PrvniKiAtMagMode,PosledniKiAtMagMode,
     6        PrvniKi,
     7        NPolar,KModAMax(7),NAtAllocMod,
     8        NRenameAt,NRenameItems
      integer, allocatable :: isf(:),itf(:),lasmax(:),ifr(:),
     1        kmol(:),iswa(:),kswa(:),KFA(:,:),KModA(:,:),kmodao(:,:),
     2        MagPar(:),TypeModFun(:),KUsePolar(:),
     3        isa(:,:),isam(:,:),WyckoffMult(:),NPGAt(:),
     4        PrvniKiAtomu(:),DelkaKiAtomu(:),KiA(:,:)
      character*27, allocatable :: LocAtSystSt(:,:)
      character*8, allocatable ::  Atom(:),SmbPGAt(:),NamePolar(:),
     1             AtomRenameOld(:),
     1             AtomRenameNew(:,:)
      character*2, allocatable :: LocAtSystAx(:)
      character*1, allocatable :: LocAtSense(:),WyckoffSmb(:)
      real, allocatable :: ai(:),sai(:),qcnt(:,:),AtSiteMult(:),
     1     TrAt(:,:),TriAt(:,:),TroAt(:,:),TroiAt(:,:),
     2     a0(:),ax(:,:),ay(:,:),sa0(:),sax(:,:),say(:,:),
     3     x(:,:),ux(:,:,:),uy(:,:,:),sx(:,:),sux(:,:,:),suy(:,:,:),
     4     beta(:,:),bx(:,:,:),by(:,:,:),sbeta(:,:),sbx(:,:,:),
     5                                              sby(:,:,:),
     6     c3(:,:),c3x(:,:,:),c3y(:,:,:),sc3(:,:),sc3x(:,:,:),
     7                                            sc3y(:,:,:),
     8     c4(:,:),c4x(:,:,:),c4y(:,:,:),sc4(:,:),sc4x(:,:,:),
     9                                            sc4y(:,:,:),
     a     c5(:,:),c5x(:,:,:),c5y(:,:,:),sc5(:,:),sc5x(:,:,:),
     1                                            sc5y(:,:,:),
     2     c6(:,:),c6x(:,:,:),c6y(:,:,:),sc6(:,:),sc6x(:,:,:),
     3                                            sc6y(:,:,:),
     4     phf(:),xfr(:),sphf(:),sxfr(:),
     5     sm0(:,:),smx(:,:,:),smy(:,:,:),ssm0(:,:),ssmx(:,:,:),
     6     ssmy(:,:,:),RPGAt(:,:,:),
     7     kapa1(:),kapa2(:),skapa1(:),skapa2(:),
     8     popc(:),spopc(:),popv(:),spopv(:),popas(:,:),spopas(:,:),
     9     LocAtSystX(:,:,:)

! ---------------------------------------------------------------------

      real, allocatable :: OrthoX40(:),OrthoDelta(:),OrthoEps(:),
     1     OrthoMat(:,:),OrthoMatI(:,:)
      integer OrthoOrd
      integer, allocatable ::  OrthoSel(:,:)

! ---------------------------------------------------------------------

      real, allocatable :: tztl(:,:),tzts(:,:),durdr(:,:)

! ---------------------------------------------------------------------

      integer nor,mxo,mxda
      real,allocatable :: orx40(:),ordel(:),oreps(:)
      character*12, allocatable :: ora(:)
      logical SwitchedToHarm

! ---------------------------------------------------------------------

      logical XYZModeInAtoms,MagModeInAtoms
      character*8, allocatable :: LXYZAMode(:),LAtXYZMode(:,:),
     1            LMagAMode(:),LAtMagMode(:,:)
      integer NXYZAMode,NMagAMode,NAtXYZMode,NAtMagMode
      integer, allocatable :: MXYZAMode(:),MMagAMode(:),
     1        MAtXYZMode(:),NMAtXYZMode(:),IAtXYZMode(:,:),
     2        KiAtXYZMode(:,:),KAtXYZMode(:,:),JAtXYZMode(:),
     3        MAtMagMode(:),NMAtMagMode(:),IAtMagMode(:,:),
     4        KiAtMagMode(:,:),KAtMagMode(:,:),JAtMagMode(:)
      integer :: NXYZAModeMax = (0), MXYZAModeMax = (0),
     1           NAtXYZModeMax = (0),
     2           NMagAModeMax = (0), MMagAModeMax = (0),
     1           NAtMagModeMax = (0)
      real, allocatable ::  XYZAMode(:,:),MagAMode(:,:),
     1     AtXYZMode(:,:),SAtXYZMode(:,:),FAtXYZMode(:,:),
     2     AtMagMode(:,:),SAtMagMode(:,:),FAtMagMode(:,:)

! ---------------------------------------------------------------------

      integer :: NAtSplitMax=0,NAtSplit,MAtSplitMax=0
      integer, allocatable :: MAtSplit(:),IAtSplit(:,:)
      character*80, allocatable :: AtSplit(:,:)


      end


      module Molec_mod
      character*27, allocatable :: LocMolSystSt(:,:,:),LocPGSystSt(:,:),
     1             StRefPoint(:)
      character*8, allocatable :: MolName(:),AtTrans(:),SmbPGMol(:)
      character*2, allocatable :: LocMolSystAx(:,:),LocPGSystAx(:)
      integer mxm,mxp,mxpm,mxdm,PrvniKiMol,PosledniKiMol,NMolec,KTLSMax,
     1        KModMMax(3)
      integer, allocatable :: iswmol(:),kswmol(:),ktls(:),RefPoint(:),
     2        KModM(:,:),KFM(:,:),kimol(:,:),
     3        LocMolSystType(:),PrvniKiMolekuly(:),DelkaKiMolekuly(:),
     4        RotSign(:),iam(:),mam(:),
     5        NPoint(:),KPoint(:),IPoint(:,:),TypeModFunMol(:)
      logical, allocatable :: UsePGSyst(:)
      real, allocatable :: aimol(:),xm(:,:),a0m(:),axm(:,:),aym(:,:),
     1     trans(:,:),utx(:,:,:),uty(:,:,:),
     2     euler(:,:),urx(:,:,:),ury(:,:,:),
     3     tt(:,:),ttx(:,:,:),tty(:,:,:),
     4     tl(:,:),tlx(:,:,:),tly(:,:,:),
     5     ts(:,:),tsx(:,:,:),tsy(:,:,:),
     6     phfm(:),
     7     saimol(:),sa0m(:),saxm(:,:),saym(:,:),
     8     strans(:,:),sutx(:,:,:),suty(:,:,:),
     9     seuler(:,:),surx(:,:,:),sury(:,:,:),
     a     stt(:,:),sttx(:,:,:),stty(:,:,:),
     1     stl(:,:),stlx(:,:,:),stly(:,:,:),
     2     sts(:,:),stsx(:,:,:),stsy(:,:,:),
     3     sphfm(:),
     4     RotMol(:,:),RotiMol(:,:),TrMol(:,:),
     5     TriMol(:,:),drotf(:,:),drotc(:,:),
     6     drotp(:,:),rotb(:,:),drotbf(:,:),
     7     drotbc(:,:),drotbp(:,:),durdx(:,:,:),
     8     rpoint(:,:,:),spoint(:,:,:),tpoint(:,:,:),TrPG(:,:),
     9     TriPG(:,:),LocPGSystX(:,:,:),LocMolSystX(:,:,:,:),
     a     OrthoX40Mol(:),OrthoDeltaMol(:),OrthoEpsMol(:)

! ---------------------------------------------------------------------

      integer :: NMolSplitMax=0,NMolSplit,MMolSplitMax=0
      integer, allocatable :: MMolSplit(:),IMolSplit(:,:)
      character*80, allocatable :: MolSplit(:,:)

      end


      module RepAnal_mod
      integer, allocatable :: NSymmEpi(:),NEqvEpi(:),NBasFun(:),
     1                        NDimIrrep(:),MultIrrep(:),IGen(:),KGen(:),
     2                        IoGen(:,:),KSymmLG(:),TakeEpi(:),
     3                        NCondKer(:),PerTab(:,:),NCondEpi(:),
     4                        NCondEpiAll(:),KIrrepEpi(:),NFamEpi(:),
     5                        FamEpi(:,:),EpiFam(:),NSymmFam(:),
     6                        OrdFam(:),OrdEpi(:),PerTabOrg(:,:),
     7                        SymmOrdOrg(:),SymmPorOrg(:),NGrpEpi(:),
     8                        NGrpKer(:),BratSub(:,:),NBratSub(:),
     9                        IPorSub(:),GenSub(:,:),NGenSub(:),
     a                        TakeSub(:),NEqAEpi(:),NEqPEpi(:),
     1                        NEqEpi(:),IGenLGConj(:,:),IGenLG(:),
     2                        SymmMagMult(:),NSymmEpiComm(:),
     3                        NLattVecEpiComm(:),
     4                        NGrpEpiO(:),KIrrepEpiO(:),NEqEpiO(:),
     5                        NSymmEpiO(:),NEqAEpiO(:),NEqPEpiO(:),
     6                        TakeEpiO(:),NCondEpiO(:),NCondEpiAllO(:)
      integer MaxBasFun,NIrrep,TriN,NSymmBlock,NSymmLG,NSymmS,NGrupaS,
     1        NGen,invcOrg,NEpi,NEpiMax,NPackArr(10),
     2        NFam,nSbwEpikernels,SelFamEpi,NTwinS,EpiSelFr,EpiSelTo,
     3        FamSel,CrSystemS,NSub,NSubMax,nSbwGroup,nSbwAtoms,
     4        NGrpMenuLast,nLblAtom,NAtomMenuLast,NSymmEpiCommMax,
     5        EpiSel,NLattVecEpiCommMax
      real TrMatSuperCell(3,3)
      real, allocatable :: EqAEpi(:,:),EqPEpi(:,:),EqPPEpi(:,:),
     1                     xyz(:,:),rm6r(:,:),rmr(:,:),s6r(:,:),
     2                     ZMagR(:),s6Ker(:,:,:),ZMagKer(:,:),
     3                     s6Epi(:,:,:),ZMagEpi(:,:),TrMatKer(:,:,:),
     4                     TrMatEpi(:,:,:),ShSgKer(:,:),ShSgEpi(:,:),
     5                     ZMagGen(:,:),s6Gen(:,:,:),PhaseX4(:),
     6                     rmEpiComm(:,:,:),s6EpiComm(:,:,:),
     7                     ZMagEpiComm(:,:),vt6EpiComm(:,:,:),
     8                     s6EpiO(:,:,:),ZMagEpiO(:,:),TrMatEpiO(:,:,:),
     9                     ShSgEpiO(:,:),EqAEpiO(:,:),EqPEpiO(:,:),
     a                     EqPPEpiO(:,:)
      complex, allocatable :: Rep(:,:,:,:),RIrrep(:,:,:),RIrrepP(:,:,:),
     1                        TraceIrrep(:,:),BasFun(:,:,:),EqEpi(:,:),
     2                        BasFunOrg(:,:,:),CondKer(:,:,:),
     3                        CondEpi(:,:,:),CondEpiAll(:,:,:),
     4                        GenLGConjDiff(:,:),EigenVecIrrep(:,:,:),
     5                        EigenValIrrep(:,:,:),EqEpiO(:,:),
     6                        CondEpiO(:,:,:),CondEpiAllO(:,:,:)
      character*80, allocatable :: GrpKer(:),GrpEpi(:),GrpMenu(:),
     1                             GrpEpiO(:)
      character*80 GrupaS
      character*20, allocatable :: StGen(:),SmbIrrep(:),SymmCodes(:)
      logical, allocatable :: BratSymmKer(:,:),BratSymmEpi(:,:),
     1                        AddSymm(:),AddSymmOrg(:),BratSymmEpiO(:,:)
      logical QPul,FromRepAnalysis,FromGoToSub
      end
      module DRIndex_mod
      character*256 FileIndex
      character*6 :: IndexWinfLabel(6) =
     1             (/'a    ','b    ','c    ',
     2               'alpha','beta ','gamma'/)
      integer, allocatable :: FlagS(:),FlagI(:),FlagK(:),FlagU(:),
     1         ProjMX(:),ProjMY(:),iha(:,:)
      integer :: GrIndexNAll,GrIndexColor,ProjNX,ProjNY,ProjNMaxX,
     1           ProjNMaxY,NCell,NCellMax,BasVec(4,25),OBasVec(25),
     2           MBasVec(25),NBasVec,IndexedSel,IndexedAll,
     3           IndexedSelSat,IndexedAllSat,NDimIndex,NTripl,MTripl,
     4           PorTripl(20),NIndSelTripl(20),GrIndexNSel,
     5           NRefForIndex=3000,NRefForTripl=20,
     6           MMin(3)=(/-1,-1,-1/),MMax(3)=(/1,1,1/),
     7           IndexCoorSystem=1,CrSystemRest=1,
     8           IH2d(3,2)=reshape((/0,0,0,0,0,0/),shape(IH2d)),
     9           GrIndexSuper(3)=(/1,1,1/)
      real, allocatable :: ProjX(:),ProjY(:),Int(:),Slozky(:,:),ha(:,:),
     1                     Uhel(:,:),Euler(:,:),Delka(:),SlozkyO(:,:),
     2                     IntO(:)
      real :: GrIndexDMax,GrIndexSMin,GrIndexSMax,GrIndexIMin,
     1        GrIndexIMax,GrIndexRad,CellDir(3,3),CellVolume,
     2        CellDirI(3,3),QPer(3),QBas(3),CellFor2d(6),Cell2d(3),
     3        XDirection(3),UBTripl(3,3,20),UBITripl(3,3,20),VTripl(20),
     4        ViewDirection(3),ViewMatrix(3,3),ViewMatrixI(3,3),
     5        CellTripl(6,20),VolMax=5000.,VolAMin=.1,DiffMax=0.01,
     6        CellParRest(6) = (/-1.,-1.,-1.,-1.,-1.,-1./)
      logical GrIndexUseSLim,GrIndexUseILim,LatticeOn,BasVecFlag(25),
     1        CellProjection,GrIndexSkipIndexed,Index2d
      logical :: AllowScalCell = .false.
      end
      module MatStore_mod
      integer :: NSymmStore(:),NSymmStoreO(:),
     1           NLattVecStore(:),NLattVecStoreO(:),
     2           NDimStore(:),NDimStoreO(:),
     3           NExtRefCondStore(:),NExtRefCondStoreO(:),
     4           NStore=0,NDimStoreMax=0,NLattVecStoreMax=0,
     5           NSymmStoreMax=0,NExtRefCondStoreMax=0
      real RM6Store(:,:,:),RM6StoreO(:,:,:),
     1      RMStore(:,:,:), RMStoreO(:,:,:),
     2     S6Store(:,:,:),S6StoreO(:,:,:),
     3     VT6Store(:,:,:),VT6StoreO(:,:,:),
     4     ProjStore(:,:,:),ProjStoreO(:,:,:),
     5     ExtRefGroupStore(:,:,:),ExtRefGroupStoreO(:,:,:),
     6     ExtRefCondStore(:,:,:),ExtRefCondStoreO(:,:,:),
     7     rmp(36),rms(36),xp(6),xq(6),xr(6)
      allocatable RM6Store,RM6StoreO,
     1             RMStore, RMStoreO,
     2            S6Store,S6StoreO,
     3            ProjStore,ProjStoreO,
     4            VT6Store,VT6StoreO,
     5            ExtRefGroupStore,ExtRefGroupStoreO,
     6            ExtRefCondStore,ExtRefCondStoreO,
     7            NSymmStore,NSymmStoreO,
     8            NLattVecStore,NLattVecStoreO,
     9            NDimStore,NDimStoreO,
     a            NExtRefCondStore,NExtRefCondStoreO
      end
      module EditM50_mod
      real, allocatable :: RTwIn(:,:)
      logical AllowChargeDensities
      end

      module EDZones_mod
      character*8 :: IdM42(20) =
     1        (/'nzones  ','ormat   ','omega   ','gmax    ','sgmax   ',
     2          'sgmaxr  ','csgmaxr ','intsteps','calcdyn ','commands',
     3          'usewks  ','absflag ','end     ','Refblock',
     4          'threads ','tiltcorr','iedt    ','scalefc ',
     5          'r#19    ','r#20    '/)
      integer :: NMaxEDZone=0,EDNPar=6,NRefED,ActionED=1,AbsFlagED=1,
     1           NRefBlockED=0,ActionDatBlock
      character*256 :: EDCommands=' '
      logical :: CalcDyn=.false.,UseWKS=.true.,RescaleToFCalc=.true.
      real E0ED,UisoED,GammaED
      integer, allocatable :: NEDZone(:),EDIntSteps(:),EDThreads(:),
     1                        EDTiltcorr(:),EDGeometryIEDT(:)
      real, allocatable :: OrMatEDZone(:,:,:),GMaxEDZone(:),
     1                     SGMaxMEDZone(:),SGMaxREDZone(:),
     2                     CSGMaxREDZone(:),OmEDZone(:)
      integer, allocatable :: KiED(:,:,:),IHED(:,:),FlagED(:),
     1                        NThickEDZone(:,:)
      real, allocatable :: HEDZone(:,:,:),AlphaEDZone(:,:),
     1  BetaEDZone(:,:),PrAngEDZone(:,:),ScEDZone(:,:),ScEDZoneS(:,:),
     2  RFacEDZone(:,:),ThickEDZone(:,:),ThickEDZoneS(:,:),
     3  XNormEDZone(:,:),XNormEDZoneS(:,:),
     4  YNormEDZone(:,:),YNormEDZoneS(:,:),
     5  PhiEDZone(:,:),PhiEDZoneS(:,:),
     6  ThetaEDZone(:,:),ThetaEDZoneS(:,:),
     7  ADerED(:,:),BDerED(:,:),IDerED(:,:),ACalcED(:),BCalcED(:),
     8  SinThLED(:),IObsED(:),SIObsED(:),FCalcED(:),ExcitErrED(:)
      logical, allocatable :: UseEDZone(:,:)
      end

      module FileDirList_mod
        character*260 FileList(:),DirList(:)
        integer NFiles,NDirs,NFilesMax,NDirsMax
        allocatable FileList,DirList
      end

      module ELMAM_mod
      character*80, allocatable :: DBText(:),DBWild(:)
      character*80 AtLocSyst(2),AtNeigh(4)
      character*8, allocatable :: DBLabel(:),DBLocSyst(:),
     1                            DBLocSystAtType(:,:)
      integer nSbwSelect,nLblLocSystem,nDB,iDB,iDBOld,NNeigh,NAtOffSet,
     1        NAtSel,iswELMAM,NAtActive,AtNeighN(4),nLblAtLocSyst(2)
      integer, allocatable :: DBLAsMax(:)
      real, allocatable :: DBPopVal(:),DBKappa(:,:),DBPopAs(:,:)
      end
