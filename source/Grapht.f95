      subroutine GraphMapa
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      include 'dist.cmn'
      integer Color,ColorOrder
      dimension xs(4),xp(4),xpoint(6),x2(6),x3(6),x4dif(3),tdif(3)
     1         ,GammaIntInv(9),uxi(3,mxw),uyi(3,mxw),xpp(4),x40(3),
     2          dx4(3),nx4(3),occ(1),smp(36),rmp(9),rm6p(36),occp(1)
      logical First,CrwLogicQuest,PopsatOsy,DrawPoints,ExistFile,
     1        DrawLattice,DrawTCuts
      character*80 CoorLabel,t80
      character*12 Labels(11)
      data First/.true./,ix/1/
      data Labels/'%Quit','%Print','%Save','New %map','%Atoms',
     1            'x4-%length','T%ips on','La%ttice on','Reverse x%4',
     2            't-c%uts on','P%oints %on'/
      data PopsatOsy,DrawLattice,DrawTCuts,DrawPoints/4*.false./
      Tiskne=.false.
      ln=0
      call TrOrtho(0)
      if(First) then
        ReverseX4=.true.
        ee(1)=10.
        nsubs=1
        First=.false.
        call SetRealArrayTo(xmin,4,0.)
        call SetRealArrayTo(xmax,4,1.)
        call SetRealArrayTo(dx,4,.1)
        call SetIntArrayTo(nx,4,0)
        obecny=.false.
        tmapy=.false.
        xymap=.false.
        cx(2)='x4'
        iorien(2)=4
      endif
      call DefaultContour
      call NactiContour
      call ConReadKeys
      MapExists=.false.
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      j=9
      if(kcommen(KPhase).gt.0) then
        j=j+2
      else
        nButtDrawTCuts=0
        nButtDrawPoints=0
      endif
      do i=1,j
        if(i.eq.7) then
          if(PopsatOsy) then
            t80='T%ips off'
          else
            t80='T%ips on'
          endif
        else if(i.eq.8) then
          if(DrawLattice) then
            t80='La%ttice off'
          else
            t80='La%ttice on'
          endif
        else if(i.eq.10) then
          if(DrawTCuts) then
            t80='t-c%uts off'
          else
            t80='t-c%uts on'
          endif
        else if(i.eq.11) then
          if(DrawPoints) then
            t80='P%oints off'
          else
            t80='P%oints on'
          endif
        else
          t80=Labels(i)
        endif
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,t80)
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtNew=ButtonLastMade
        else if(i.eq.5) then
          nButtAtoms=ButtonLastMade
        else if(i.eq.6) then
          nButtX4Length=ButtonLastMade
        else if(i.eq.7) then
          nButtPopsatOsy=ButtonLastMade
        else if(i.eq.8) then
          nButtDrawLattice=ButtonLastMade
        else if(i.eq.9) then
          nButtReverseX4=ButtonLastMade
        else if(i.eq.10) then
          nButtDrawTCuts=ButtonLastMade
        else if(i.eq.11) then
          nButtDrawPoints=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.3.or.i.eq.5.or.i.eq.j) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      CheckType=EventButton
      CheckNumber=nButtNew
      go to 3010
1100  if(kcommen(KPhase).gt.0) then
        t0=trez(1,nsubs,KPhase)
        dtp=1./float(NCommQ(1,nsubs,KPhase)*NCommQ(2,nsubs,KPhase)*
     1               NCommQ(3,nsubs,KPhase))
      endif
      if(xmax(1)-xmin(1).gt.1.1.or.xmax(2)-xmin(2).gt.1.1) then
        call FeQuestButtonOff(nButtDrawLattice)
      else
        DrawLattice=.false.
        call FeQuestButtonLabelChange(nButtDrawLattice,'La%ttice on')
        call FeQuestButtonDisable(nButtDrawLattice)
      endif
      if(kcommen(KPhase).gt.0) then
        tmin=xmin(2)-max(xmin(1)*qu(ix,1,nsubs,KPhase),
     1                   xmax(1)*qu(ix,1,nsubs,KPhase))
        pom=(tmin-t0)/dtp
        ntmin=pom
        if(pom.gt.0.) ntmin=ntmin+1
        tmax=xmax(2)-min(xmin(1)*qu(ix,1,nsubs,KPhase),
     1                   xmax(1)*qu(ix,1,nsubs,KPhase))
        pom=(tmax-t0)/dtp
        ntmax=pom
        if(pom.lt.0.) ntmax=ntmax-1
      endif
      j=2
      do i=1,3
        if(i.eq.ix) then
          k=1
        else
          j=j+1
          k=j
        endif
        iorien(k)=i
        write(cx(k),'(''x'',i1)') i
      enddo
      call SetTr
1200  call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call FeMakeAcFrame
        if(PopsatOsy) then
          call FeMakeAxisLabels(1,xmin(1),xmax(1),xmin(2),xmax(2),cx(1))
          call FeMakeAxisLabels(2,xmin(2),xmax(2),xmin(1),xmax(1),cx(2))
        endif
        if(DrawLattice) then
          ixp=xmin(1)
          ixk=xmax(1)
          do i=ixp,ixk
            xpom=i
            if(xpom.gt.xmin(1).and.xpom.lt.xmax(1)) then
              xp(1)=i
              xp(2)=xmin(2)
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(1)=xo(1)
              yu(1)=xo(2)
              xp(2)=xmax(2)
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(2)=xo(1)
              yu(2)=xo(2)
              call FePolyLine(2,xu,yu,White)
            endif
          enddo
          iyp=xmin(2)
          iyk=xmax(2)
          do i=iyp,iyk
            ypom=i
            if(ypom.gt.xmin(2).and.ypom.lt.xmax(2)) then
              xp(2)=i
              xp(1)=xmin(1)
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(1)=xo(1)
              yu(1)=xo(2)
              xp(1)=xmax(1)
              xp(3)=0.
              call FeXf2X(xp,xo)
              xu(2)=xo(1)
              yu(2)=xo(2)
              call FePolyLine(2,xu,yu,White)
            endif
          enddo
        endif
        if(DrawTCuts) then
          do i=ntmin,ntmax
            t=t0+float(i)*dtp
            x1(1)=xmin(1)
            x1(2)=t+xmin(1)*qu(ix,1,nsubs,KPhase)
            x1(3)=0.
            x2(1)=xmax(1)
            x2(2)=t+xmax(1)*qu(ix,1,nsubs,KPhase)
            x2(3)=0.
            if(x2(2).gt.xmax(2)) then
              x2(1)=x1(1)+(xmax(2)-x1(2))/(x2(2)-x1(2))*(x2(1)-x1(1))
              x2(2)=xmax(2)
            endif
            if(x1(2).lt.xmin(2)) then
              x1(1)=x2(1)+(xmin(2)-x2(2))/(x1(2)-x2(2))*(x1(1)-x2(1))
              x1(2)=xmin(2)
            endif
            call FeXf2X(x1,xo)
            xu(1)=xo(1)
            yu(1)=xo(2)
            call FeXf2X(x2,xo)
            xu(2)=xo(1)
            yu(2)=xo(2)
            call FeLineType(DashedLine)
            call FePolyLine(2,xu,yu,White)
          enddo
          call FeLineType(NormalLine)
        endif
        call SetRealArrayTo(xp,3,0.)
        call ConDrawAtoms(xp)
        if(DrawPoints) then
          if(ExistFile(fln(:ifln)//'.xyz')) then
            ln=NextLogicNumber()
            call OpenFile(ln,fln(:ifln)//'.xyz','formatted','old')
            if(ErrFlag.ne.0) then
              ErrFlag=0
              go to 1600
            endif
1400        read(ln,FormA,end=1600) t80
            if(t80.eq.' ') go to 1600
            if(t80(1:1).eq.'#') go to 1400
            k=0
            call Kus(t80,k,Cislo)
            call StToReal(t80,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 1600
            call Multm(RCommen(1,1,KPhase),xp,xpoint,3,3,1)
            call qbyx(xpoint,xpoint(4),1)
            do m=4,NDim(KPhase)
              xpoint(m)=xpoint(m)+trez(m-3,1,KPhase)
            enddo
            Radius=3.
            if(k.lt.80) then
              call StToReal(t80,k,xp,1,.false.,ich)
              if(ich.ne.0) go to 1500
              Radius=xp(1)
            endif
            if(k.lt.80) then
              call Kus(t80,k,Cislo)
              Color=ColorNumbers(ColorOrder(Cislo))
            endif
1500        do isym=1,NSymmN(KPhase)
              call multm(rm6(1,isym,1,KPhase),xpoint,x1,NDim(KPhase),
     1                   NDim(KPhase),1)
              do m=1,NDim(KPhase)
                x2(m)=x1(m)+s6(m,isym,1,KPhase)
              enddo
              do icenter=1,NLattVec(KPhase)
                do m=1,NDim(KPhase)
                  x3(m)=x2(m)+vt6(m,icenter,1,KPhase)
                enddo
                call od0do1(x3,x3,NDim(KPhase))
                xp(1)=x3(ix)
                xp(2)=x3(4)
                xp(3)=0.
                call FeXf2X(xp,xo)
                xm=xo(1)
                ym=xo(2)
                MarkerType=-1
                if(Radius.lt.1.2) MarkerType=-MarkerType
                call FeMarker(xm,ym,Radius,MarkerType,Color)
              enddo
            enddo
            go to 1400
1600        if(ln.ne.0) call CloseIfOpened(ln)
          else
            do 1800i=1,DrawAtN
              if(DrawAtSkip(i)) go to 1800
              call atsymi(DrawAtName(i),ia,xs,x4dif,tdif,isym,ich,*1800)
              if(DrawAtColor(i).eq.0) then
                Color=AtColor(isf(ia),KPhase)
              else
                Color=ColorNumbers(DrawAtColor(i))
              endif
              isw=ISwSymm(isym,iswa(ia),KPhase)
              call DistSetComp(isw,NSubs)
              if(isw.eq.iswa(ia)) then
                call CopyMat(rm (1,isym,isw,KPhase),rmp ,3)
                call CopyMat(rm6(1,isym,isw,KPhase),rm6p,NDim(KPhase))
              else
                call multm(zv(1,isw,KPhase),zvi(1,iswa(ia),KPhase),smp,
     1                     NDim(KPhase),NDim(KPhase),NDim(KPhase))
                call multm(smp,rm6(1,isym,iswa(ia),KPhase),rm6p,
     1                     NDim(KPhase),NDim(KPhase),NDim(KPhase))
                call MatBlock3(rm6p,rmp,NDim(KPhase))
              endif
              ksw=kswa(ia)
              call GetGammaIntInv(rm6p,GammaIntInv)
              do l=1,KModA(2,ia)
                call multm(rmp,ux(1,l,ia),uxi(1,l),3,3,1)
                if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
                  call multm(rmp,uy(1,l,ia),uyi(1,l),3,3,1)
                else
                  call CopyVek(uy(1,l,ia),uyi(1,l),3)
                endif
              enddo
              do ntp=-100,100
                t=t0+float(ntp)*dtp
                xpp(1)=TNaT(1)*t
                call CopyVek(qcnt(1,ia),x40,NDimI(KPhase))
                call AddVek(tdif,xpp,xpp,NDimI(KPhase))
                call multm(GammaIntInv,xpp,xp,NDimI(KPhase),
     1                     NDimI(KPhase),1)
                nx4(1)=1
                dx4(1)=0.
                if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
                  call MakeOccMod(occ,x40,xp,nx4,dx4,a0(ia),ax(1,ia),
     1                            ay(1,ia),KModA(1,ia),KFA(1,ia),
     2                            GammaIntInv)
                  if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
                    call MakeOccModSawTooth(occp,x40,xp,nx4,dx4,
     1                uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),KFA(2,ia),
     2                GammaIntInv)
                    if(occp(1).le.0.) occ(1)=0.
                  endif
                else
                  call MakeOccModPol(occ,x40,xp,nx4,dx4,
     1                               ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                               ax(KModA(1,ia),ia),a0(ia)*.5,
     3                               GammaIntInv,TypeModFun(ia))
                endif
                if(TypeModFun(ia).le.1) then
                  call MakePosMod(xpp,x40,xp,nx4,dx4,uxi,uyi,
     1                            KModA(2,ia),KFA(2,ia),GammaIntInv)
                else
                  call MakePosModPol(xpp,x40,xp,nx4,dx4,uxi,uyi,
     1                               KModA(2,ia),ax(KModA(1,ia),ia),
     2                               a0(ia)*.5,GammaIntInv,
     3                               TypeModFun(ia))
                endif
                if(occ(1).lt..1) cycle
                call AddVek(xs,xpp,xp,3)
                call qbyx(xp,xp(4),isw)
                xp(4)=xp(4)+TNaT(1)*t
                call multm(WPrI,xp,xpp,NDim(KPhase),NDim(KPhase),1)
                xpp(1)=xpp(ix)
                xpp(2)=xpp(4)
                xpp(3)=0.
                if(xpp(1).lt.xmin(1).or.xpp(1).gt.xmax(1).or.
     1             xpp(2).lt.xmin(2).or.xpp(2).gt.xmax(2)) cycle
                call FeXf2X(xpp,xo)
                call FeCircle(xo(1),xo(2),3.,Color)
              enddo
1800        continue
          endif
        endif
        MapExists=.true.
      endif
      call FeHardCopy(HardCopy,'close')
2900  HardCopy=0
3000  call FeQuestEvent(id,ich)
3010  if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          call FeQuestRemove(id)
          go to 9999
        else if(CheckNumber.eq.nButtNew) then
          xpomt=120.
          xd=250.
          dpom=xd-xpomt-15.
          tpom=5.
          i=3
          if(NComp(KPhase).gt.1) i=i+1
          idp=NextQuestId()
          t80='Coordinate to draw'
          call FeQuestCreate(idp,-1.,-1.,xd,i,t80,0,LightGray,0,0)
          il=1
          xpom=(xd-CrwgXd)*.5-xd*.25
          do i=1,3
            call FeQuestCrwMake(idp,xpom,il,xpom+5.,il,'%'//smbx(i),'C',
     1                          CrwgXd,CrwgYd,1,1)
            if(i.eq.1) nCrwX=CrwLastMade
            call FeQuestCrwOpen(i,i.eq.ix)
            xpom=xpom+xd*.25
          enddo
          write(CoorLabel,100) ix,ix
          il=il+1
          call FeQuestEdwMake(idp,tpom,il,xpomt,il,CoorLabel,'L',dpom,
     1                        EdwYd,0)
          nedwLim3=EdwLastMade
          xp(1)=xmin(1)
          xp(2)=xmax(1)
          call FeQuestRealAEdwOpen(nedwLim3,xp,2,.false.,.false.)
          il=il+1
          call FeQuestEdwMake(idp,tpom,il,xpomt,il,'x%4min,x4max','L',
     1                        dpom,EdwYd,0)
          nedwLim4=EdwLastMade
          xp(1)=xmin(2)
          xp(2)=xmax(2)
          call FeQuestRealAEdwOpen(nedwLim4,xp,2,.false.,.false.)
          if(NComp(KPhase).gt.1) then
            il=il+1
            dpom=50.
            call FeQuestEdwMake(idp,tpom,il,xpomt,il,
     1                          '%Subsystem number','L',dpom,EdwYd,0)
            nedwSubs=EdwLastMade
            call FeQuestIntEdwOpen(nedwSubs,nsubs,.false.)
          endif
3500      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventCrw) then
            do i=nCrwX,nCrwX+2
              if(CrwLogicQuest(i)) then
                if(ix.ne.i) then
                  ix=i
                  write(CoorLabel,100) ix,ix
                  call FeQuestEdwLabelChange(nEdwLim3,CoorLabel)
                endif
                EventType=EventEdw
                EventNumber=nEdwLim3
                go to 3500
              endif
            enddo
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 3500
          endif
          if(ich.eq.0) then
            call FeQuestRealAFromEdw(nedwLim3,xp)
            xmin(1)=xp(1)
            xmax(1)=xp(2)
            call FeQuestRealAFromEdw(nedwLim4,xp)
            xmin(2)=xp(1)
            xmax(2)=xp(2)
            if(NComp(KPhase).gt.1)
     1        call FeQuestIntFromEdw(nedwSubs,nsubs)
          endif
          call FeQuestRemove(idp)
          if(ich.ne.0) then
            go to 3000
          else if(kcommen(KPhase).gt.0) then
            do i=1,3
              if(i.ne.ix) then
                if(qu(i,1,nsubs,KPhase).ne.0.) then
                  call FeQuestButtonDisable(nButtDrawTCuts)
                  go to 1100
                endif
              endif
            enddo
            call FeQuestButtonOff(nButtDrawTCuts)
          endif
          go to 1100
        else if(CheckNumber.eq.nButtAtoms) then
          call ConDrawAtomsEdit
          go to 1100
        else if(CheckNumber.eq.nButtX4Length) then
          idp=NextQuestId()
          call FeQuestCreate(idp,-1.,-1.,xd,1,' ',0,LightGray,0,0)
          tpom=15.
          dpom=150.
          t80='%Length of 4th vector'
          xpomt=tpom+FeTxLengthUnder(t80)+10.
          dpom=xd-xpomt-15.
          call FeQuestEdwMake(idp,tpom,1,xpomt,1,t80,'L',dpom,EdwYd,0)
          nedw4=EdwLastMade
          call FeQuestRealEdwOpen(nedw4,ee(1),.false.,.false.)
3700      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 3700
          endif
          if(ich.eq.0) call FeQuestRealFromEdw(nedw4,ee(1))
          call FeQuestRemove(idp)
          go to 1100
        else if(CheckNumber.eq.nButtPopsatOsy) then
          PopsatOsy=.not.PopsatOsy
          if(PopsatOsy) then
            t80='T%ips off'
          else
            t80='T%ips on'
          endif
          go to 3900
        else if(CheckNumber.eq.nButtDrawLattice) then
          DrawLattice=.not.DrawLattice
          if(DrawLattice) then
            t80='La%ttice off'
          else
            t80='La%ttice on'
          endif
          go to 3900
        else if(CheckNumber.eq.nButtReverseX4) then
          ReverseX4=.not.ReverseX4
          go to 1100
        else if(CheckNumber.eq.nButtDrawTCuts) then
          DrawTCuts=.not.DrawTCuts
          if(DrawTCuts) then
            t80='t-c%uts off'
          else
            t80='t-c%uts on'
          endif
          go to 3900
        else if(CheckNumber.eq.nButtDrawPoints) then
          DrawPoints=.not.DrawPoints
          if(DrawPoints) then
            t80='P%oints off'
          else
            t80='P%oints on'
          endif
          go to 3900
        else if(CheckNumber.eq.nButtPrint.or.CheckNumber.eq.nButtSave)
     1    then
          HardCopy=0
          if(CheckNumber.eq.nButtPrint) then
            call FePrintPicture(ich)
            if(ich.ne.0) HardCopy=0
          else
            call FeSavePicture('graph',6,0)
          endif
          if(HardCopy.eq.0) go to 3000
        endif
        go to 1200
3900    call FeQuestButtonLabelChange(CheckNumber,t80)
        go to 1100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 3000
      endif
9999  if(NDim(KPhase).gt.3) call TrOrtho(1)
      call ConWriteKeys
      return
100   format('x%',i1,'min,x',i1,'max')
      end
      subroutine Grapht
      use Contour_mod
      use Grapht_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'grapht.cmn'
      dimension sngc(:,:,:),csgc(:,:,:)
      logical iesd
      allocatable sngc,csgc
      if(allocated(xdst)) deallocate(xdst,BetaDst,dxm,BratPrvni,
     1                 BratDruhyATreti,aselPrvni,aselDruhyATreti)
      n=NAtCalc+5
      allocate(xdst(3,n),BetaDst(6,n),dxm(n),BratPrvni(n),
     1         BratDruhyATreti(n),aselPrvni(n),aselDruhyATreti(n))
      call SetLogicalArrayTo(BratDruhyATreti,n,.true.)
      nt=0
      do i=1,NDimI(KPhase)
        do j=1,NComp(KPhase)
          nt(i,j)=1
        enddo
      enddo
      do j=1,NComp(KPhase)
        ntall(j)=1
        do i=1,NDimI(KPhase)
          ntall(j)=ntall(j)*nt(i,j)
        enddo
      enddo
      call inm40(iesd)
      call GrtRightButtons
      if(KCommenMax.gt.0) call DeleteFile(PreviousM50)
      call DeleteFile(PreviousM40)
      call CloseIfOpened(l80)
      call DeleteFile(fln(:ifln)//'.l80')
9999  if(Allocated(DrawT)) deallocate(DrawT,DrawValue,DrawOcc,DrawOccP,
     1                                XMod)
      if(allocated(ActualMap)) deallocate(ActualMap)
      if(allocated(xdst)) deallocate(xdst,BetaDst,dxm,BratPrvni,
     1                 BratDruhyATreti,aselPrvni,aselDruhyATreti)
      if(allocated(oi)) deallocate(oi,oj,ok,xdi,xdj,xdk,sxdi,sxdj,
     1                             sxdk)
      if(allocated(um)) deallocate(um,dum,dam,dums,dams,dumm,dhm,dhms)
      if(allocated(AtomArr)) deallocate(AtomArr)
      NAtomArr=0
      return
      end
      subroutine GrtRightButtons
      use Contour_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      include 'dist.cmn'
      character*80 Veta
      character*17 :: Labels(12) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Sho%w it in DPlot',
     5                '%New/Edit        ',
     6                'G%-              ',
     7                'G%+              ',
     8                '%Go to           ',
     9                '%Movie           ',
     a                '%Contours        ',
     1                'Cur%ves          ',
     2                '%Options         '/)
      integer ButtonStateQuest,RecPack,FeGetSystemTime,HardCopyOld
      logical GraphExists,FeYesNo
      Tiskne=.false.
      nLblMovie=0
      VolaToDist=.false.
      DrawPos=1
      DrawNeg=1
      call SetLogicalArrayTo(BratDruhyATreti,NAtCalc,.true.)
      GraphExists=.false.
      if(NDimI(KPhase).gt.1) then
        l80=NextLogicNumber()
        call OpenFile(l80,fln(:ifln)//'.l80','unformatted','unknown')
      endif
      GraphtQuest=NextQuestId()
      ContourQuest=GraphtQuest
      CheckMouse=.true.
      call FeQuestAbsCreate(GraphtQuest,0.,0.,XMaxBasWin,YMaxBasWin,
     1                      ' ',0,0,-1,-1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(60.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      ContourYSep(1)=pom
      k=1
      ik=12
      do i=1,ik
        ActiveButtBas(i)=.false.
        call FeQuestAbsButtonMake(GraphtQuest,xpom,ypom,
     1                            wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
        else if(i.eq.4) then
          nButtDPlot=ButtonLastMade
        else if(i.eq.5) then
          nButtNewGraph=ButtonLastMade
          wpom=wpom/2.-5.
          nButtBasFr=ButtonLastMade
        else if(i.eq.6) then
          nButtMinus=ButtonLastMade
          xpom=xpom+wpom+10.
          ypom=ypom+dpom
        else if(i.eq.7) then
          nButtPlus=ButtonLastMade
          wpom=80.
          xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
        else if(i.eq.8) then
          nButtGoTo=ButtonLastMade
        else if(i.eq.9) then
          nButtMovie=ButtonLastMade
        else if(i.eq.10) then
          nButtContours=ButtonLastMade
        else if(i.eq.11) then
          nButtCurves=ButtonLastMade
        else if(i.eq.12) then
          nButtOptions=ButtonLastMade
        endif
        if(i.le.5.or.NDimI(KPhase).gt.1.or.i.eq.12) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        ActiveButtBas(i)=.true.
        call FeQuestButtonOpen(ButtonLastMade,j)
1050    if(i.eq.3.or.i.eq.7.or.i.eq.9.or.i.eq.10) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
          k=k+1
          ContourYSep(k)=ypom
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      fpom=FeTxLength('XXX.XXX')
      dhl1=FeTxLength(ConWinfLabel(1))
      do i=1,3
        if(i.eq.1) then
          dpom=2.*fpom+5.
          xpom=XCenGrWin-.5*dhl1-dpom-20.
          ypom=YMaxBasWin-20.
        else if(i.eq.2) then
          dpom=3.*fpom+5.
          xpom=XCenGrWin+.5*dhl1+20.
        else if(i.eq.3) then
          xpom=XMaxGrWin-100.
          ypom=YMinGrWin-25.
          dpom=FeTxLength('XXXXXXX.XX')+5.
        endif
        if(i.gt.2) then
          call FeQuestAbsLblMake(ContourQuest,xpom-10.,YBottomText,
     1                           'Value','R','N')
          call FeQuestLblOff(LblLastMade)
          nLblWinf(i)=LblLastMade
        endif
        call FeWinfMake(i,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      enddo
      CheckNumber=nButtNewGraph
2000  if(GraphExists) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      call FeQuestButtonOpen(nButtSave,j)
      call FeQuestButtonOpen(nButtPrint,j)
      if(CallDPlot.ne.' ') then
        call FeQuestButtonOpen(nButtDPlot,j)
      else
        call FeQuestButtonOpen(nButtDPlot,ButtonDisabled)
      endif
      if(WhatToDraw.eq.1) then
        call FeQuestButtonOpen(nButtCurves,j)
        call FeQuestButtonOpen(nButtContours,j)
      else
        call FeQuestButtonOpen(nButtCurves,ButtonDisabled)
        call FeQuestButtonOpen(nButtContours,ButtonDisabled)
      endif
      if(NDimI(KPhase).gt.1) then
        if(NMapGrt.gt.1) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtMinus,j)
        if(NMapGrt.lt.NxMapMx) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtPlus,j)
      endif
      if(ButtonStateQuest(nButtPlus ).eq.ButtonOff.or.
     1   ButtonStateQuest(nButtMinus).eq.ButtonOff) then
        call FeQuestButtonOff(nButtGoto)
        call FeQuestButtonOff(nButtMovie)
      else
        call FeQuestButtonDisable(nButtGoto)
        call FeQuestButtonDisable(nButtMovie)
      endif
      call FeQuestMouseToButton(CheckNumber)
2090  if(GraphExists) then
        if(WhatToDraw.eq.0) then
          call GrtLocator
        else
          call PCurves(1)
        endif
      else
        call FeQuestEvent(GraphtQuest,ich)
      endif
2100  HardCopy=0
      ErrFlag=0
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          do i=1,3
            call FeWInfRemove(i)
          enddo
          call FeQuestRemove(GraphtQuest)
          go to 9999
        else if(CheckNumber.eq.nButtNewGraph) then
2200      call GrtDefParams(ich)
          if(ich.eq.0) then
            call GrtDrawCurveOrMap(0,1,GraphExists)
            if(ErrFlag.ne.0) then
              call FeClearGrWin
              GraphExists=.false.
              ErrFlag=0
            else
              if(.not.GraphExists) then
                call FeClearGrWin
                call FeChybne(-1.,-1.,'no curve to be drawn, please '//
     1                        'try again',' ',Warning)
                call ReallocateGrtDraw(10)
                DrawN=1
                go to 2200
              endif
            endif
          endif
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            call GrtDrawCurveOrMap(0,0,GraphExists)
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
            HardCopy=0
          endif
        else if(CheckNumber.eq.nButtSave) then
          if(NDimI(KPhase).gt.1) then
            i=0
          else
            i=1
          endif
          call FeSavePicture('graph',7,i)
          if(HardCopy.lt.0) HardCopy=0
          if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                            HardCopy.ne.HardCopyPCX)) then
            call GrtDrawCurveOrMap(0,0,GraphExists)
          else
            call FeHardCopy(HardCopy,'open')
            call FeHardCopy(HardCopy,'close')
          endif
        else if(CheckNumber.eq.nButtDPlot) then
          call GrtDPlot
        else if(CheckNumber.eq.nButtMinus) then
          call GrtDrawCurveOrMap(-1,0,GraphExists)
        else if(CheckNumber.eq.nButtPlus) then
          call GrtDrawCurveOrMap( 1,0,GraphExists)
        else if(CheckNumber.eq.nButtGoto) then
          call GrtGoTo(ich)
          if(ich.eq.0) call GrtDrawCurveOrMap(0,0,GraphExists)
        else if(CheckNumber.eq.nButtMovie) then
          call GrtDefMovie(ich)
          if(ich.ne.0) go to 2000
          if(nLblMovie.le.0) then
            xpom=350.
            ypom=YBottomMargin*.5
            Veta='Press Esc to interrupt the movie'
            call FeQuestAbsLblMake(ContourQuest,xpom,ypom,Veta,
     1                             'L','B')
            nLblMovie=LblLastMade
          else
            call FeQuestLblOn(nLblMovie)
          endif
          JedeMovie=.true.
          irpt=0
          isv=0
          HardCopyOld=HardCopy
3000      irpt=irpt+1
          if(irpt.gt.MovieRepeat.and.MovieRepeat.ne.0) go to 3200
          call CopyVekI(nxMovieFr,nxdraw,NDimI(KPhase)-ndGrt)
3050      if(irpt.eq.1.and.MovieFileName.ne.' ') then
            isv=isv+1
            write(HCFileName,'(''_'',i4)') isv
            do i=1,idel(HCFileName)
              if(HCFileName(i:i).eq.' ') HCFileName(i:i)='0'
            enddo
            HCFileName=MovieFileName(:idel(MovieFileName))//
     1               HCFileName(:idel(HCFileName))//
     2               HCExtension(HardCopy)(:idel(HCExtension(HardCopy)))
          else
            HardCopy=0
          endif
          NMapGrt=RecPack(NxDraw,NxMap,NDimI(KPhase)-ndGrt)
          call GrtDrawCurveOrMap(0,0,GraphExists)
          if(ErrFlag.ne.0) go to 3200
          if(HardCopy.ne.0) then
            i=HardCopy
            HardCopy=0
            call KresliMapu( 0)
            if(ErrFlag.ne.0) go to 3200
            HardCopy=i
          endif
          TimeStart=FeGetSystemTime()
3100      call FeEvent(1)
          if(EventType.eq.EventKey.and.EventNumber.eq.JeEscape) then
            if(FeYesNo(-1.,-1.,
     1                  'Do you want really to interrupt it?',1))
     2        go to 3200
          else if(EventType.eq.EventAscii.and.
     1            char(EventNumber).eq.' ') then
3120        call FeEvent(1)
            if(EventType.ne.EventAscii.or.char(EventNumber).ne.' ')
     1        go to 3120
          else
            if(FeGetSystemTime()-TimeStart.le.
     1         nint(DelayTimeForMovie*1000.)) go to 3100
          endif
          do i=1,NDim(KPhase)-2
            if(nxdraw(i).lt.nxMovieTo(i)) go to 3150
          enddo
          go to 3000
3150      nxdraw(1)=nxdraw(1)+1
          if(nxdraw(1).gt.nxMovieTo(1)) then
            nxdraw(1)=nxMovieFr(1)
            nxdraw(2)=nxdraw(2)+1
            if(nxdraw(2).gt.nxMovieTo(2)) then
               call CopyVekI(nxMovieFr,nxdraw,2)
            endif
          endif
          go to 3050
3200      if(MovieFileName.ne.' ') then
            TextInfo(1)='The movie maps were recorded to files: '//
     1        MovieFileName(:idel(MovieFileName))//'_####'//
     2        HCExtension(HardCopyOld)(:idel(HCExtension(HardCopyOld)))
            NInfo=1
            WaitTime=10000
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
          endif
          call FeQuestLblOff(nLblMovie)
        else if(CheckNumber.eq.nButtContours) then
          call DefContour(ich)
          if(ich.eq.0) call GrtDrawMap(0)
        else if(CheckNumber.eq.nButtOptions) then
          call GrtReadOptions(ich)
          if(ich.eq.0.and.GraphExists)
     1      call GrtDrawCurveOrMap(0,1,GraphExists)
        else if(CheckNumber.eq.nButtCurves) then
          call PCurves(0)
        endif
        call FeQuestButtonOff(CheckNumber)
        go to 2000
      else
        go to 2090
      endif
      go to 2000
9999  if(NDim(KPhase).gt.5) close(l80,status='delete')
      return
      end
      subroutine GrtDefParams(ich)
      use Atoms_mod
      use Grapht_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'grapht.cmn'
      dimension xsym(3),x4dif(3),tdif(3),gxo(3,3),gyo(2)
      character*256 EdwStringQuest,Veta,GrtStPom(3)
      character*80  Header,HeaderOld,t80
      character*3 :: smbm(4) = (/'Mx ','My ','Mz ','|M|'/)
      character*2   nty
      integer ColorOrder,WhatToDrawOld,EdwStateQuest,CrwStateQuest,
     1        RolMenuSelectedQuest,PocetParTypeAct
      logical CrwLogicQuest,BratAtom(:)
      allocatable BratAtom
      data icolor/-1/
      if(allocated(BratAtom)) deallocate(BratAtom)
      allocate(BratAtom(NAtCalc))
      BratAtom=.false.
      call GrtSaveDrawAtom
      HardCopy=0
      WhatToDrawOld=-1
      NParOld=-1
      NAtomsOld=0
      id=NextQuestId()
      xdq=520.
      xdqp=xdq*.5
      if(NDimI(KPhase).gt.1) then
        il=14+NDimI(KPhase)
      else
        il=14
      endif
      call FeQuestCreate(id,-1.,-1.,xdq,il,' ',1,LightGray,0,0)
      il=1
      Veta='%Parameter to draw'
      tpom=120.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=0.
      if(MagneticType(KPhase).gt.0) then
        PocetParTypeAct=PocetParType
      else
        PocetParTypeAct=PocetParType-1
      endif
      do i=1,PocetParTypeAct
        dpom=max(dpom,FeTxLength(MenuParType(i)))
      enddo
      dpom=dpom+EdwYd+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      xpom1=xpom+dpom+5.
      nRolMenuParType=RolMenuLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,xdqp,il,' ','C','B')
      nLblType=LblLastMade
      HeaderOld=' '
      dpom=60.
      xpom=xdqp-dpom-90.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'P%revious')
      nButtPrevious=ButtonLastMade
      xpom=xdqp+90.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'Ne%xt')
      nButtNext=ButtonLastMade
      nCrwGroup=0
      if(NDimI(KPhase).gt.1) then
        nCrwGroup=nCrwGroup+1
        il=il+1
        tpom=5.
        Veta='Type of graph :'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nLblTypeGr=LblLastMade
        call FeQuestLblOn(nLblTypeGr)
        tpom=FeTxLengthUnder(Veta)+20.
        do i=0,1
          if(i.eq.0) then
            Veta='1d %graph'
            dpom=FeTxLengthUnder(Veta)+10.
          else
            Veta='2d %map'
          endif
          xpom=tpom+dpom
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,nCrwGroup)
          call FeQuestCrwOpen(CrwLastMade,WhatToDraw.eq.i)
          if(i.eq.0) nCrwTypeFirst=CrwLastMade
          tpom=xpom+30.
        enddo
        nCrwTypeLast=CrwLastMade
      else
        nCrwTypeFirst=0
        nCrwTypeLast=0
        WhatToDraw=0
      endif
      il=il+1
      dpom=50.
      posun=dpom+20.
      tpom=5.
      xpom=tpom+FeTxLengthUnder('XX')+5.+dpom*.5
      do i=1,3
        if(i.eq.1) then
          Veta='Minimum'
        else if(i.eq.2) then
          Veta='Maximum'
        else if(i.eq.3) then
          Veta='Step'
        endif
        call FeQuestLblMake(id,xpom,il,Veta,'C','N')
        call FeQuestLblOn(LblLastMade)
        xpom=xpom+posun
      enddo
      call CopyVek(gx,gxo,9)
      ilp=il
      do i=1,NDimI(KPhase)+1
        il=il+1
        tpom=5.
        xpom=tpom+FeTxLengthUnder('XX')+10.
        if(i.le.NDimI(KPhase)) then
          k=3
          if(GrtDrawX4) then
            Veta=smbx6(i+3)
          else
            Veta=smbt(i)
          endif
        else
          k=2
          Veta='p'
        endif
        do j=1,3
          if(j.le.k) then
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          0)
            if(i.eq.1.and.j.eq.1) then
              nEdwTLim=EdwLastMade
            else if(i.eq.NDimI(KPhase)+1.and.j.eq.1) then
              nEdwPLim=EdwLastMade
            endif
            if(i.le.NDimI(KPhase)) then
              pom=gx(j,i)
            else
              pom=gy(j)
            endif
            call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
          endif
          tpom=tpom+posun
          xpom=xpom+posun
          Veta=' '
        enddo
      enddo
      if(NDimI(KPhase).gt.1) then
        xpom=xpom+5.
        do i=1,NDimI(KPhase)
          nCrwGroup=nCrwGroup+1
          il=ilp
          write(Veta,'(i1,a2)') i,nty(i)
          call FeQuestLblMake(id,xpom+10.,il,Veta,'C','N')
          call FeQuestLblOn(LblLastMade)
          do j=1,NDimI(KPhase)
            il=il+1
            call FeQuestCrwMake(id,xpom,il,xpom,il,' ','C',CrwgXd,
     1                          CrwgYd,1,nCrwGroup)
            if(i.eq.1.and.j.eq.1) nCrwOrientFirst=CrwLastMade
          enddo
          xpom=xpom+25.
        enddo
        nCrwOrientLast=CrwLastMade
        il=il+1
        Veta='%Reference level explicitly'
        xpom=8.
        tpom=xpom+CrwXd+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwRefLevel=CrwLastMade
        tpom=tpom+FeTxLengthUnder(Veta)+10.
        Veta='=>'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwRefLevel=EdwLastMade
      else
        nCrwOrientFirst=0
        nCrwOrientLast=0
        nCrwRefLevel=0
        nEdwRefLevel=0
      endif
      xpom=xdq*.5+90.
      tpom=xpom+CrwXd+5.
      Veta='%Use x4,x5,x6 instead of t,u,v'
      call FeQuestCrwMake(id,tpom,ilp,xpom,ilp,Veta,'L',CrwXd,CrwYd,
     1                    1,0)
      nCrwDrawX4=CrwLastMade
      dpom=0.
      do i=1,PocetBarev
        dpom=max(dpom,FeTxLength(ColorNames(i)))
      enddo
      dpom=dpom+EdwYd+10.
      Veta='%Color'
      tpom=xpom+dpom+10.
      call FeQuestRolMenuMake(id,tpom,ilp+1,xpom,ilp+1,Veta,'L',dpom,
     1                        EdwYd,1)
      nRolMenuColor=RolMenuLastMade
      Veta='Draw n%on-modulated curves'
      tpom=xpom+CrwXd+5.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwNonMod=CrwLastMade
      il=il+1
      nCrwGroup=nCrwGroup+1
      xpom=5.
      Veta='Diagonal:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      nLblDiagonal=LblLastMade
      xpomp=xpom+FeTxLength(Veta)+10.
      xpom=xpomp
      Veta=' '
      dpom=70.
      do i=1,4
        tpom=xpom+CrwgXd+3.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      nCrwGroup)
        if(i.eq.1) nCrwParFirst=CrwLastMade
        xpom=xpom+dpom
      enddo
      il=il+1
      xpom=5.
      Veta='Principal:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      nLblPrincipal=LblLastMade
      xpom=xpomp
      Veta=' '
      do i=1,3
        tpom=xpom+CrwgXd+3.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      nCrwGroup)
        xpom=xpom+dpom
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      do i=1,4
        il=il+1
        write(Veta,'(''%'',i1,a2,'' atom'')') i,nty(i)
        tpom=5.
        if(i.eq.1) xpoma=tpom+FeTxLengthUnder(Veta)+55.
        xpom=xpoma
        dpom=150.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        if(i.eq.1) nEdwName=EdwLastMade
        xpom=xpom+dpom+5.
        Veta='List'
        dpomp=FeTxLengthUnder(Veta)+20.
        xpom=xpom+10.
        call FeQuestButtonMake(id,xpom,il,dpomp,ButYd,Veta)
        if(i.eq.1) then
          nButtAtomList=ButtonLastMade
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          ilp=il
        endif
      enddo
      il=il-3
      Veta='Pl%ane #1'
      xpom=tpom+100.
      dpom=320.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwPlane1=EdwLastMade
      xpomp=xpom+dpom+10.
      Veta='List'
      call FeQuestButtonMake(id,xpomp,il,dpomp,ButYd,Veta)
      nButtPlane1=ButtonLastMade
      il=il+1
      Veta='Plane #2/directi%on'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwPlane2=EdwLastMade
      Veta='List'
      call FeQuestButtonMake(id,xpomp,il,dpomp,ButYd,Veta)
      nButtPlane2=ButtonLastMade
      il=il+1
      Veta='Plane #3/directi%on'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwPlane3=EdwLastMade
      Veta='List'
      call FeQuestButtonMake(id,xpomp,il,dpomp,ButYd,Veta)
      nButtPlane3=ButtonLastMade
      il=il+1
      Veta='D%efine neighbour atoms'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xdq-dpom-5.
      call FeQuestButtonMake(id,xpom,ilp,dpom,ButYd,Veta)
      nButtNeighbours=ButtonLastMade
      Veta='De%fine bond valence coefficients'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xdq-dpom-5.
      ilp=ilp+1
      call FeQuestButtonMake(id,xpom,ilp,dpom,ButYd,Veta)
      nButtBondVal=ButtonLastMade
      tpom=5.
      Veta='Ma%ximal distance'
      dpom=80.
      call FeQuestEdwMake(id,tpom,ilp,xpoma,ilp,Veta,'L',dpom,EdwYd,0)
      nEdwDMax=EdwLastMade
      ilp=ilp+1
      call FeQuestEdwMake(id,tpom,ilp,xpoma,ilp,Veta,'L',dpom,EdwYd,0)
      nEdwDMaxIndDist=EdwLastMade
      il=il+1
      dpom=80.
      xpom=xdqp-20.-dpom
      Veta='Ne%w item'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtNew=ButtonLastMade
      xpom=xdqp+20.
      Veta='%Delete item'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtDelete=ButtonLastMade
      ia=1
      iao=0
      if(DrawN.gt.0) go to 1220
      call ReallocateGrtDraw(10)
      DrawN=1
1200  IColor=ColorOrder('White')
      DrawColor(ia)=IColor
      if(ia.eq.1) then
        call SetStringArrayTo(DrawAtom(1,ia),4,' ')
        DrawSymSt(ia)=' '
        DrawSymStU(ia)=' '
        DrawParam(ia)=31
      else
        DrawParam(ia)=NPar*10+1
        DrawAtom(1:4,ia)=DrawAtom(1:4,ia-1)
        DrawSymSt(ia)=DrawSymSt(ia-1)
        DrawSymStU(ia)=DrawSymStU(ia-1)
      endif
1220  NPar=DrawParam(ia)/10
      IPar=max(mod(DrawParam(ia),10),1)
      if(NPar.eq.IdDrawTorsion) then
        NAtoms=4
      else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
        NAtoms=2
      else if(NPar.eq.IdDrawPlane) then
        NAtoms=-1
      else
        NAtoms=1
      endif
      if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1   NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawADP.or.
     2   NPar.eq.IdMagMoment) then
        call FeQuestCrwOpen(nCrwDrawX4,GrtDrawX4)
      else
        call FeQuestCrwClose(nCrwDrawX4)
      endif
      if(WhatToDraw.eq.0.and.
     1   (NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     2    NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     3    NPar.eq.IdDrawADP.or.NPar.eq.IdDrawVal.or.
     4    NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng)) then
        if(WhatToDraw.ne.WhatToDrawOld) then
          if(NDimI(KPhase).gt.1) then
            call FeQuestEdwClose(nEdwRefLevel)
            call FeQuestCrwClose(nCrwRefLevel)
          endif
          call FeQuestCrwClose(nCrwNonMod)
        endif
      else
        if(WhatToDraw.ne.WhatToDrawOld) then
          if((NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1        NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane).and.
     2        WhatToDraw.eq.0) then
            if(NDimI(KPhase).gt.1.and.NPar.ne.IdDrawVal) then
              call FeQuestCrwClose(nCrwRefLevel)
              call FeQuestEdwClose(nEdwRefLevel)
            endif
            if(NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane)
     1        call FeQuestCrwClose(nCrwNonMod)
          else
            call FeQuestCrwClose(nCrwNonMod)
            call FeQuestEdwClose(nEdwPLim)
            if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng)
     1        call FeQuestRealFromEdw(nEdwPLim+1,DrawDMax)
            call FeQuestEdwClose(nEdwPLim+1)
          endif
        endif
      endif
      if(NPar.ne.NParOld) then
        if(NDimI(KPhase).gt.1) then
          tpom=5.
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1       NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane.or.
     2       NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
            nCrw=nCrwTypeFirst
            if(NPar.ne.IdDrawVal) then
              call FeQuestLblOff(nLblTypeGr)
              do i=0,1
                call FeQuestCrwClose(nCrw)
                nCrw=nCrw+1
              enddo
            endif
          endif
        endif
        if(NAtoms.ne.NAtomsOld) then
          if(NAtomsOld.le.0.and.NAtoms.gt.0) then
            call FeQuestEdwClose(nEdwPlane1)
            call FeQuestEdwClose(nEdwPlane2)
            call FeQuestEdwClose(nEdwPlane3)
            call FeQuestButtonClose(nButtPlane1)
            call FeQuestButtonClose(nButtPlane2)
            call FeQuestButtonClose(nButtPlane3)
          endif
          if(NAtoms.lt.NAtomsOld) then
            nEdw=nEdwName
            if(NAtoms.gt.0) then
              nn=NAtoms+1
              nEdw=nEdw+NAtoms
            else
              nn=1
            endif
            nButt=nButtAtomList
            if(NAtoms.gt.0) nButt=nButt+NAtoms
            do i=nn,NAtomsOld
              call FeQuestButtonClose(nButt)
              nButt=nButt+1
              call FeQuestEdwClose(nEdw)
              nEdw=nEdw+1
            enddo
          endif
        endif
        if(NPar.ne.IdDrawDist.and.NPar.ne.IdDrawVal.and.
     1     NPar.ne.IdDrawAngles)
     2    call FeQuestButtonClose(nButtNeighbours)
      endif
      if(WhatToDraw.ne.0.or.
     1   (NPar.ne.IdDrawOcc.and.NPar.ne.IdDrawXYZ.and.
     2    NPar.ne.IdDrawDeltaXYZ.and.NPar.ne.IdDrawTorsion.and.
     3    NPar.ne.IdDrawADP.and.NPar.ne.IdMagMoment.and.
     4    NPar.ne.IdDrawIndDist.or.NPar.ne.IdDrawIndAng)) then
        call FeQuestButtonClose(nButtNext)
        call FeQuestButtonClose(nButtPrevious)
      endif
      if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        ik=3
      else if(NPar.eq.IdMagMoment) then
        ik=4
      else if(NPar.eq.IdDrawADP) then
        ik=7
      else
        ik=0
      endif
      nCrw=nCrwParFirst
      do i=ik+1,7
        if(i.le.ik) then

        else
          call FeQuestCrwClose(nCrw)
        endif
        nCrw=nCrw+1
      enddo
      if(ik.ne.7) then
        call FeQuestLblOff(nLblDiagonal)
        call FeQuestLblOff(nLblPrincipal)
      endif
      if((NParOld.eq.IdDrawVal.or.NParOld.eq.IdDrawAngles).and.
     1   NPar.ne.NParOld) then
        call FeQuestEdwClose(nEdwDMax)
        if(NParOld.eq.IdDrawVal) call FeQuestButtonClose(nButtBondVal)
      else if(NParOld.eq.IdDrawIndDist.and.NParOld.eq.IdDrawIndAng.and.
     1        NPar.ne.NParOld) then
        call FeQuestEdwClose(nEdwDMaxIndDist)
      endif
      if(NDimI(KPhase).gt.1) then
        nCrw=nCrwOrientFirst-1
        do i=1,NDimI(KPhase)
         do 1370j=1,NDimI(KPhase)
            nCrw=nCrw+1
            do k=1,i-1
              if(ior(k).eq.j) then
                call FeQuestCrwClose(nCrw)
                go to 1370
              endif
            enddo
1370      continue
        enddo
      endif
      if(WhatToDraw.eq.0.and.
     1   (NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     2    NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     3    NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment.or.
     4    NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng)) then
        write(Header,'(i2,''/'',i2)') ia,DrawN
        call zhusti(Header)
        Header=Header(:idel(Header))//' item to be drawn'
        if(WhatToDraw.ne.WhatToDrawOld) then
          call FeQuestButtonOpen(nButtNew,ButtonOff)
          call FeQuestButtonOpen(nButtDelete,ButtonOff)
          call FeQuestRolMenuOpen(nRolMenuColor,ColorNames,PocetBarev,
     1                            ColorOrder('White'))
          if(nEdwPLim.gt.0) call GrtSetParLimits(nEdwPLim,NPar)
          call CopyVek(gy,gyo,2)
        endif
      else
          call FeQuestButtonClose(nButtNew)
          call FeQuestButtonClose(nButtDelete)
          call FeQuestRolMenuClose(nRolMenuColor)
        if(NPar.eq.IdDrawAngles) then
          Header='Bond angles related to the central atom'
        else if(NPar.eq.IdDrawDist) then
          Header='Distances related to the central atom'
        else if(NPar.eq.IdDrawVal) then
          Header='Bond valence sum for the central atom'
        else if(NPar.eq.IdDrawPlane) then
          Header='Angle between two planes or between plane and '//
     1           'selected direction'
        endif
        if(WhatToDraw.ne.WhatToDrawOld) then
          if((NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal).and.
     1        WhatToDraw.eq.0) then
            call FeQuestCrwOpen(nCrwNonMod,DrawNonMod)
          endif
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles.or.
     1       WhatToDraw.eq.0) then
            call GrtSetParLimits(nEdwPLim,NPar)
            call CopyVek(gy,gyo,2)
          else if(WhatToDraw.ne.0) then
            call FeQuestCrwOpen(nCrwRefLevel,UseRefLevel)
            if(UseReflevel)
     1        call FeQuestRealEdwOpen(nEdwRefLevel,RefLevel,.false.,
     2                                .false.)
          endif
        endif
      endif
      if(NPar.ne.NParOld) then
        if(NDimI(KPhase).gt.1) then
          tpom=5.
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1       NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane.or.
     2       NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
            if(NPar.ne.IdDrawVal) then
               call FeQuestLblOff(nLblTypeGr)
            endif
          endif
          if(NPar.ne.IdDrawDist.and.NPar.ne.IdDrawAngles) then
            nCrw=nCrwTypeFirst
            call FeQuestLblOn(nLblTypeGr)
            do i=0,1
              call FeQuestCrwOpen(nCrw,WhatToDraw.eq.i)
              nCrw=nCrw+1
            enddo
          endif
        endif
        if(NAtoms.ne.NAtomsOld.or.NPar.eq.IdDrawIndDist.or.
     1     NPar.eq.IdDrawIndAng) then
          if(NAtoms.gt.NAtomsOld) then
            nEdw=nEdwName
            if(NAtomsOld.gt.0) then
              nn=NAtomsOld+1
              nEdw=nEdw+NAtomsOld
            else
              nn=1
            endif
            nButt=nButtAtomList
            if(NAtomsOld.gt.0) nButt=nButt+NAtomsOld
            do i=nn,NAtoms
              call FeQuestButtonOpen(nButt,ButtonOff)
              nButt=nButt+1
              call FeQuestStringEdwOpen(nEdw,' ')
              nEdw=nEdw+1
            enddo
          endif
          if(NAtoms.gt.0) then
            if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
              if(EdwStateQuest(nEdwName).eq.EdwOpened) then
                call FeQuestEdwClose(nEdwName)
                call FeQuestButtonClose(nButtAtomList)
              endif
              if(EdwStateQuest(nEdwName+1).eq.EdwOpened) then
                call FeQuestEdwClose(nEdwName+1)
                call FeQuestButtonClose(nButtAtomList+1)
              endif
              if(EdwStateQuest(nEdwPlane1).ne.EdwOpened) then
                call FeQuestStringEdwOpen(nEdwPlane1,DrawAtom(1,1))
                call FeQuestStringEdwOpen(nEdwPlane2,DrawAtom(2,1))
                if(NPar.eq.IdDrawIndAng)
     1             call FeQuestStringEdwOpen(nEdwPlane3,DrawAtom(3,1))
              else if(NParOld.eq.IdDrawIndDist.and.
     1                NPar.eq.IdDrawIndAng) then
                call FeQuestStringEdwOpen(nEdwPlane3,DrawAtom(3,1))
              else if(NParOld.eq.IdDrawIndAng.and.
     1                NPar.eq.IdDrawIndDist) then
                call FeQuestEdwClose(nEdwPlane3)
                call FeQuestButtonClose(nButtPlane3)
              endif
              call FeQuestEdwLabelChange(nEdwPlane1,'Center/Atom#1')
              call FeQuestEdwLabelChange(nEdwPlane2,'Center/Atom#2')
              if(NPar.eq.IdDrawIndAng)
     1          call FeQuestEdwLabelChange(nEdwPlane3,'Center/Atom#3')
              call FeQuestButtonOff(nButtPlane1)
              call FeQuestButtonOff(nButtPlane2)
              if(NPar.eq.IdDrawIndAng)
     1          call FeQuestButtonOff(nButtPlane3)
            else
              if(EdwStateQuest(nEdwPlane1).eq.EdwOpened) then
                call FeQuestEdwClose(nEdwPlane1)
                call FeQuestEdwClose(nEdwPlane2)
                call FeQuestEdwClose(nEdwPlane3)
                call FeQuestButtonClose(nButtPlane1)
                call FeQuestButtonClose(nButtPlane2)
                call FeQuestButtonClose(nButtPlane3)
              endif
              if(EdwStateQuest(nEdwName).ne.EdwOpened) then
                call FeQuestStringEdwOpen(nEdwName,DrawAtom(1,1))
                call FeQuestButtonOff(nButtAtomList)
              endif
              if(NAtoms.le.2) then
                Veta='Central %atom'
              else
                Veta='%1st atom'
              endif
              call FeQuestEdwLabelChange(nEdwName,Veta)
              if(NAtoms.eq.2) then
                Veta='%Neighbor atom'
              else
                Veta='%2nd atom'
              endif
              call FeQuestEdwLabelChange(nEdwName+1,Veta)
            endif
          else
            if(NAtoms.eq.-1) then
              call FeQuestStringEdwOpen(nEdwPlane1,GrtPlane(1))
              call FeQuestStringEdwOpen(nEdwPlane2,GrtPlane(2))
              call FeQuestEdwLabelChange(nEdwPlane1,'Pl%ane #1')
              call FeQuestEdwLabelChange(nEdwPlane2,
     1                                   'Plane #2/directi%on')
              if(EdwStateQuest(nEdwPlane3).eq.EdwOpened) then
                call FeQuestEdwClose(nEdwPlane3)
                call FeQuestButtonClose(nButtPlane3)
              endif
            endif
            call FeQuestButtonOff(nButtPlane1)
            call FeQuestButtonOff(nButtPlane2)
          endif
        endif
        if(NAtoms.eq.1.and.NAtoms.ne.NAtomsOld)
     1    call FeQuestEdwLabelChange(nEdwName,'Central %atom')
        if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1     NPar.eq.IdDrawAngles) then
          call FeQuestButtonOff(nButtNeighbours)
        else
          call FeQuestButtonClose(nButtNeighbours)
        endif
      endif
      if(Header.ne.HeaderOld) then
        call FeQuestLblChange(nLblType,Header)
        HeaderOld=Header
      endif
      if(ia.gt.DrawN) then
        IColor=ColorOrder('White')
      else
        IColor=DrawColor(ia)
      endif
      if(ia.ne.iao) then
        if(NPar.ne.IdDrawIndDist.and.NPar.ne.IdDrawIndAng) then
          nEdw=nEdwName
          do i=1,NAtoms
            call FeQuestStringEdwOpen(nEdw,DrawAtom(i,ia))
            nEdw=nEdw+1
          enddo
        else
          call FeQuestStringEdwOpen(nEdwPlane1,DrawAtom(1,ia))
          call FeQuestStringEdwOpen(nEdwPlane2,DrawAtom(2,ia))
          if(NPar.eq.IdDrawIndAng)
     1      call FeQuestStringEdwOpen(nEdwPlane3,DrawAtom(3,ia))
        endif
        if(ia.gt.DrawN) then
          icolor=ColorOrder('White')
        else
          icolor=DrawColor(ia)
        endif
        NPar=DrawParam(ia)/10
        if(NPar.eq.IdDrawTorsion) then
          NAtoms=4
        else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
          NAtoms=2
        else if(NPar.eq.IdDrawPlane) then
          NAtoms=-1
        else
          NAtoms=1
        endif
        IPar=max(mod(DrawParam(ia),10),1)
        if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1     NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2     NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
          if(WhatToDraw.eq.0)
     1      call FeQuestRolMenuOpen(nRolMenuColor,ColorNames,PocetBarev,
     2                              icolor)
        endif
        call FeQuestRolMenuOpen(nRolMenuParType,MenuParType,
     1                          PocetParTypeAct,NPar)
      endif
      if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        ik=3
      else if(NPar.eq.IdDrawADP) then
        ik=7
        call FeQuestLblOn(nLblDiagonal)
        call FeQuestLblOn(nLblPrincipal)
      else if(NPar.eq.IdMagMoment) then
        ik=4
      else
        ik=0
      endif
      nCrw=nCrwParFirst
      do i=1,7
        if(i.le.ik) then
          if(NPar.eq.IdDrawXYZ) then
            call FeQuestCrwLabelChange(nCrw,smbx(i))
          else if(NPar.eq.IdDrawDeltaXYZ) then
            call FeQuestCrwLabelChange(nCrw,'d'//smbx(i))
          else if(NPar.eq.IdDrawADP) then
            call FeQuestCrwLabelChange(nCrw,smbu(i))
          else if(NPar.eq.IdMagMoment) then
            call FeQuestCrwLabelChange(nCrw,smbm(i))
          endif
          call FeQuestCrwOpen(nCrw,i.eq.IPar)
        endif
        nCrw=nCrw+1
      enddo
      if((NPar.eq.IdDrawVal.or.NPar.eq.IdDrawAngles).and.
     1   NPar.ne.NParOld) then
        call FeQuestRealEdwOpen(nEdwDMax,DrawDMax,.false.,.false.)
        if(NPar.eq.IdDrawVal) call FeQuestButtonOff(nButtBondVal)
      else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndDist) then
        if(WhatToDraw.eq.0) then
          call FeQuestEdwClose(nEdwDMaxIndDist)
        else
          call FeQuestRealEdwOpen(nEdwDMaxIndDist,DrawDMax,.false.,
     1                            .false.)
        endif
      endif
      if(NDimI(KPhase).gt.1) then
        nCrw=nCrwOrientFirst-1
        do i=1,NDimI(KPhase)
         do 1380j=1,NDimI(KPhase)
            nCrw=nCrw+1
            do k=1,i-1
              if(ior(k).eq.j) go to 1380
            enddo
            if(ior(1).gt.0) call FeQuestCrwOpen(nCrw,j.eq.ior(i))
1380      continue
        enddo
      endif
      if(WhatToDraw.eq.0.and.
     1   (NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     2    NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     3    NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment.or.
     4    NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng)) then
        if(ia.lt.DrawN) then
          call FeQuestButtonOff(nButtNext)
        else
          call FeQuestButtonDisable(nButtNext)
        endif
        if(ia.gt.1) then
          call FeQuestButtonOff(nButtPrevious)
        else
          call FeQuestButtonDisable(nButtPrevious)
        endif
      else
        call FeQuestButtonClose(nButtNext)
        call FeQuestButtonClose(nButtPrevious)
      endif
      iao=ia
      WhatToDrawOld=WhatToDraw
      NParOld=NPar
      NAtomsOld=NAtoms
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        if(NPar.eq.IdDrawPlane.or.NPar.eq.IdDrawIndDist.or.
     1     NPar.eq.IdDrawIndAng) then
          Veta=' '
          GrtStPom(1)=EdwStringQuest(nEdwPlane1)
          if(GrtStPom(1).eq.' ') then
            if(NPar.eq.IdDrawPlane) then
              Veta='the Plane#1 not defined.'
            else
              Veta='the Atom/Center#1 not defined.'
            endif
          endif
          if(Veta.eq.' ') then
            GrtStPom(2)=EdwStringQuest(nEdwPlane2)
            if(GrtStPom(2).eq.' ') then
              if(NPar.eq.IdDrawPlane) then
                Veta='the Plane#2/Dirtection not defined.'
              else
                Veta='the Atom/Center#2 not defined.'
              endif
            endif
          endif
          if(NPar.eq.IdDrawIndAng.and.Veta.eq.' ') then
            GrtStPom(3)=EdwStringQuest(nEdwPlane3)
            if(GrtStPom(3).eq.' ')
     1        Veta='the Atom/Center#3 not defined.'
          endif
          if(Veta.ne.' ') then
            EventType=EventEdw
            call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
            if(GrtStPom(1).eq.' ') then
              EventNumber=nEdwPlane1
            else
              EventNumber=nEdwPlane2
            endif
            if(NPar.eq.IdDrawPlane) then
              GrtPlane(1:2)=GrtStPom(1:2)
            else
              DrawAtom(1:2,ia)=GrtStPom(1:2)
            endif
            go to 1500
          endif
        else
          nEdw=nEdwName
          do i=1,NAtoms
            if(EdwStringQuest(nEdw).eq.' ') then
              if(NAtoms.eq.1) then
                Veta='no atom defined, try again'
              else
                write(Veta,'(''Atom #'',i1,'' not defined, try again'')
     1                ') i
              endif
              call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
              go to 1500
            endif
            nEdw=nEdw+1
          enddo
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawVal.or.
     1       NPar.eq.IdDrawAngles) then
            do i=1,NAtCalc
              if(BratDruhyATreti(i)) go to 1540
            enddo
            call FeChybne(-1.,YBottomMessage,'no neighbour atom '//
     1                    'defined',' ',SeriousError)
            go to 1500
          endif
        endif
1540    if(WhatToDraw.eq.0) then
          ik=NDimI(KPhase)+1
          RefLevel=0.
          UseRefLevel=.false.
        else
          ik=NDimI(KPhase)
          if(UseRefLevel)
     1      call FeQuestRealFromEdw(nEdwRefLevel,RefLevel)
        endif
        if(NPar.eq.IdDrawVal.or.NPar.eq.IdDrawAngles) then
          call FeQuestRealFromEdw(nEdwDMax,DrawDMax)
        else if(NPar.eq.IdDrawIndDist) then
          call FeQuestRealFromEdw(nEdwDMaxIndDist,DrawDMax)
        endif
        nEdw=nEdwTLim
        do i=1,ik
          if(i.le.NDimI(KPhase)) then
            k=3
          else
            k=2
          endif
          do j=1,k
            call FeQuestRealFromEdw(nEdw,pom)
            if(i.le.NDimI(KPhase)) then
              gx(j,i)=pom
              if(j.eq.2) then
                if(gx(1,i).ge.gx(2,i)) then
                  Veta=smbt(i)//'(min)>='//smbt(i)//'(max), try again'
                  call FeChybne(-1.,YBottomMessage,Veta,' ',
     1                          SeriousError)
                  go to 1500
                endif
              endif
            else
              gy(j)=pom
              if(j.eq.2) then
                if(gy(1).ge.gy(2)) then
                  Veta='p(min)>=p(max), try again'
                  call FeChybne(-1.,YBottomMessage,Veta,' ',
     1                          SeriousError)
                  go to 1500
                endif
              endif
            endif
            nEdw=nEdw+1
          enddo
        enddo
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.ge.nEdwName.and.
     1        CheckNumber.le.nEdwName+NAtoms-1) then
        nEdw=CheckNumber
        ip=CheckNumber-nEdwName+1
        call GrtUpdateDrawAtom(ia)
        if(DrawAtom(ip,ia).ne.' '.and.
     1     (EventType.ne.EventButton.or.EventNumber.lt.nButtAtomList.or.
     2      EventNumber.gt.nButtAtomList+NAtoms-1))
     3   call atsymi(DrawAtom(ip,ia),i,xsym,x4dif,tdif,isym,k,*1560)
        go to 1500
1560    EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.ge.nButtAtomList
     1        .and.CheckNumber.le.nButtAtomList+NAtoms-1) then
        ip=CheckNumber-nButtAtomList+1
        nEdw=nEdwName+ip-1
        if(DrawAtom(ip,ia).eq.' ') then
          i=1
        else
          i=ktat(atom,NAtCalc,DrawAtom(ip,ia))
        endif
        call SelOneAtomFromPhase('Select atom from the list',Atom,kswa,
     1                           i,NAtCalc,ich)
        if(ich.eq.0) then
          call FeQuestStringEdwOpen(nEdw,Atom(i))
          call GrtUpdateDrawAtom(ia)
        endif
        EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwPlane1.or.CheckNumber.eq.nEdwPlane2
     2         .or.CheckNumber.eq.nEdwPlane3)) then
        ik=CheckNumber-nEdwPlane1+1
        Veta=EdwStringQuest(CheckNumber)
        if(Veta.eq.' ') go to 1500
        if(ik.eq.2.and.NPar.eq.IdDrawPlane) then
          do i=1,idel(Veta)
            if(Veta(i:i).eq.' ') cycle
            if(index(Cifry,Veta(i:i)).le.0) then
              GrtPlaneDir=.false.
              go to 1572
            endif
          enddo
          GrtPlaneDir=.true.
          n=0
          k=0
1570      if(k.lt.len(Veta)) then
            call Kus(Veta,k,Cislo)
            n=n+1
            go to 1570
          endif
          if(n.ne.3) then
            call FeChybne(-1.,YBottomMessage,
     1                    'the direction should be defined by three'//
     2                    ' coordinates.',' ',SeriousError)
            go to 1580
          endif
          go to 1575
        endif
1572    if(NAtomArr.le.0) then
          allocate(AtomArr(1))
          NAtomArr=1
        endif
        call SplitToAtomStrings(Veta,AtomArr,n,0,t80)
        if(t80.ne.' ') then
          call FeChybne(-1.,YBottomMessage,t80,' ',SeriousError)
          go to 1580
        endif
1575    if(NPar.eq.IdDrawPlane) then
          NAtPlane=max(NAtPlane,n)
          if(ik.eq.1.or..not.GrtPlaneDir) then
            if(n.le.2) then
              call FeChybne(-1.,YBottomMessage,
     1                      'the number of atoms defining plane have '//
     2                      'to larger than 2.',' ',SeriousError)
              go to 1580
            endif
          endif
          GrtPlane(ik)=Veta
        else
          DrawAtom(ik,ia)=Veta
        endif
        go to 1500
1580    EventType=EventEdw
        EventNumber=CheckNumber
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNew) then
        call GrtUpdateDrawAtom(ia)
        call ReallocateGrtDraw(10)
        DrawN=DrawN+1
        ia=DrawN
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDelete)
     1  then
        do i=ia,DrawN-1
          DrawAtom(1,i)=DrawAtom(1,i+1)
          DrawSymSt(i)=DrawSymSt(i+1)
          DrawSymStU(i)=DrawSymStU(i+1)
          DrawParam(i)=DrawParam(i+1)
          DrawColor(i)=DrawColor(i+1)
        enddo
        DrawN=max(0,DrawN-1)
        ia=min(ia,DrawN)
        ia=max(ia,1)
        iao=0
        go to 1220
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtNeighbours) then
        call SelAtomsFromPhase('Select atoms for distance calculation',
     1                         Atom,BratDruhyATreti,kswa,isf,NAtCalc,
     2                         ich)
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtBondVal) then
        call DistSetCommandsFromGrapht
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColor) then
        i=RolMenuSelectedQuest(nRolMenuColor)
        if(i.le.0) i=ColorOrder('White')
        if(icolor.ne.i) then
          DrawColor(ia)=i
          IColor=i
        endif
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuParType) then
        NPar=RolMenuSelectedQuest(nRolMenuParType)
        if(NPar.le.0) NPar=IdDrawXYZ
        if(NPar.ne.NParOld) then
          DrawParam(ia)=NPar*10+1
          iao=0
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles) WhatToDraw=0
          WhatToDrawOld=-1
          go to 1220
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNext)
     1  then
        call GrtUpdateDrawAtom(ia)
        ia=min(ia+1,DrawN)
        go to 1220
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtPrevious) then
        call GrtUpdateDrawAtom(ia)
        ia=max(ia-1,1)
        go to 1220
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwTypeFirst.and.
     2        CheckNumber.le.nCrwTypeLast) then
        WhatToDraw=CheckNumber-nCrwTypeFirst
        if(WhatToDraw.eq.0) then
          ia=1
        else
          UseRefLevel=.false.
        endif
        go to 1220
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwParFirst.and.
     2        CheckNumber.le.nCrwParFirst+ik-1) then
        IPar=CheckNumber-nCrwParFirst+1
        go to 1500
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwOrientFirst.and.
     2        CheckNumber.le.nCrwOrientLast) then
        j=CheckNumber-nCrwOrientFirst
        i=j/NDimI(KPhase)+1
        j=mod(j,NDimI(KPhase))+1
        ipp=ior(i)
        ior(i)=j
        do k=i+1,NDimI(KPhase)
          if(ior(k).eq.j) then
            ior(k)=ipp
            go to 1220
          endif
        enddo
        go to 1220
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRefLevel)
     1  then
        if(CrwLogicQuest(nCrwRefLevel)) then
          if(EdwStateQuest(nEdwRefLevel).ne.EdwOpened) then
            RefLevel=GrtSetRefLevel(ia,isw,NPar,IPar)
            call FeQuestRealEdwOpen(nEdwRefLevel,RefLevel,.false.,
     1                              .false.)
            UseRefLevel=.true.
            EventType=EventEdw
            EventNumber=nEdwRefLevel
          endif
        else
          call FeQuestEdwClose(nEdwRefLevel)
          UseRefLevel=.false.
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDrawX4) then
        nEdw=nEdwTLim
        do i=1,NDimI(KPhase)
          if(CrwLogicQuest(nCrwDrawX4)) then
            Veta=smbx6(i+3)
          else
            Veta=smbt(i)
          endif
          call FeQuestEdwLabelChange(nEdw,Veta)
          nEdw=nEdw+3
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPlane1.or.
     2         CheckNumber.eq.nButtPlane2)) then
        call SelAtomsFromPhase('Select atoms for distance calculation',
     1                         Atom,BratAtom,kswa,isf,NAtCalc,ich)
        if(ich.ne.0) go to 1500
        Veta=' '
        do i=1,NAtCalc
          if(BratAtom(i)) then
            Veta=Veta(:idel(Veta))//' '//Atom(i)(:idel(Atom(i)))
          endif
        enddo
        if(CheckNumber.eq.nButtPlane1) then
          nEdw=nEdwPlane1
        else
          nEdw=nEdwPlane2
        endif
        call FeQuestStringEdwOpen(nEdw,Veta(2:))
        EventType=EventEdw
        EventNumber=nEdw
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call ReallocateGrtDraw(10)
        DrawN=max(ia,DrawN)
        if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ.or.
     1     NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
          if(NPar.eq.IdDrawADP) then
            ik=7
          else if(NPar.eq.IdMagMoment) then
            ik=4
          else
            ik=3
          endif
          nCrw=nCrwParFirst
          do i=1,ik
            if(CrwLogicQuest(nCrw)) go to 1660
            nCrw=nCrw+1
          enddo
          i=1
        else
          i=0
        endif
1660    DrawParam(ia)=10*NPar+i
        DrawColor(ia)=icolor
        if(CrwStateQuest(nCrwNonMod).ne.CrwClosed)
     1    DrawNonMod=CrwLogicQuest(nCrwNonMod)
        if(CrwStateQuest(nCrwDrawX4).ne.CrwClosed) then
          GrtDrawX4=CrwLogicQuest(nCrwDrawX4)
        else
          GrtDrawX4=.false.
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        call CopyVek(gxo,gx,9)
        call CopyVek(gyo,gy,2)
        call GrtRestoreDrawAtom
        go to 9999
      endif
      call SetRealArrayTo(DxMod,3,0.)
      call SetIntArrayTo(NxMod,3,1)
      call SetIntArrayTo(NxMap,2,1)
      NxMapMx=1
      if(WhatToDraw.eq.0) then
        ndGrt=1
      else
        ndGrt=2
      endif
      DrawPoints=1
      do i=1,NDimI(KPhase)
        j=ior(i)
        n=nint((gx(2,j)-gx(1,j))/gx(3,j))+1
        if(i.le.ndGrt) then
          DrawPoints=DrawPoints*n
          DxMod(j)=gx(3,j)
          NxMod(j)=n
          if(WhatToDraw.eq.0) then
            xomn=gx(1,j)
            xomx=gx(2,j)
            yomn=gy(1)
            yomx=gy(2)
          endif
        else
          NxMapMx=NxMapMx*n
          NxMap(i-ndGrt)=n
        endif
      enddo
      if(Allocated(DrawT)) deallocate(DrawT,DrawValue,DrawOcc,DrawOccP,
     1                                XMod)
      if(allocated(BratAtom)) deallocate(BratAtom)
      allocate(DrawT(DrawPoints),DrawValue(DrawPoints,10),
     1         DrawOcc(DrawPoints,10),DrawOccP(DrawPoints,10),
     2         XMod(6*DrawPoints))
      NMapGrt=1
9999  return
      end
      subroutine GrtDrawCurves(Klic,jmap,isw)
      use Basic_mod
      use Grapht_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension tp(3),xp(3),xo(3),Gamma(9)
      integer Color
      character*8 YLabel,LabelP
      character*80 Veta
      logical Prazdno,FileAlreadyOpened
      save Prazdno
      ich=0
      if(Klic.ne.0) go to 2000
      call UnitMat(F2O,3)
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      if(HardCopy.ne.0) then
        if(HardCopy.lt.100.or.jmap.eq.1) then
          call FeHardCopy(HardCopy,'open')
          if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                            HardCopy.ne.HardCopyPCX)) then
            if(HardCopy.eq.HardCopyNum+100) then
              if(.not.FileAlreadyOpened(HCFileName)) then
                call OpenFile(85,HCFileName,'formatted','unknown')
                if(ErrFlag.ne.0) go to 9999
              endif
            endif
          endif
        endif
      else
        HardCopy=HardCopyNum
        open(85,file=fln(:ifln)//'.l66')
      endif
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call FeOutSt(0,XCenGrWin,(YMaxGrWin+YMaxAcWin)*.5,GraphtLabel,
     1               'C',White)
        if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1    write(85,'(''# '',a)') GraphtLabel(:idel(GraphtLabel))
        call FeMakeAcFrame
        if(GrtDrawX4) then
          Veta=smbx6(ior(1)+3)
        else
          Veta=smbt(ior(1))
        endif
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,Veta)
        if((HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1     .and.DrawN.gt.0) then
          NPar=DrawParam(1)/10
          if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles) then
             if(NPar.eq.IdDrawDist) then
               Veta='distance'
             else
               Veta='angle'
             endif
             Veta='# '//Veta(:idel(Veta))//'-t plot for '//
     1            DrawAtom(1,1)(:idel(DrawAtom(1,1)))
             write(85,FormA) Veta(:idel(Veta))
          endif
        endif
        Prazdno=.true.
      endif
2000  YLabel=' '
      do 3000i=1,DrawN
        do j=1,DrawPoints
          if(DrawOcc(j,i).gt.OccLimit.and.DrawValue(j,i).ge.yomn
     1                               .and.DrawValue(j,i).lt.yomx)
     2      go to 2020
        enddo
        go to 3000
2020    NPar=DrawParam(i)/10
        IPar=max(mod(DrawParam(i),10),1)
        jc=DrawColor(i)
        Color=ColorNumbers(iabs(jc))
        if(jc.gt.0) then
          LineTypeDraw=NormalLine
        else
          LineTypeDraw=DashedLine
        endif
        if(NPar.eq.IdDrawOcc) then
          LabelP='occ'
        else if(NPar.eq.IdDrawXYZ) then
          LabelP=smbx(IPar)
        else if(NPar.eq.IdDrawDeltaXYZ) then
          LabelP='d'//smbx(IPar)
        else if(NPar.eq.IdDrawADP) then
          LabelP=SmbU(IPar)
        else if(NPar.eq.IdDrawTorsion) then
          LabelP='Tors'
        else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
          LabelP='Dist'
        else if(NPar.eq.IdDrawVal) then
          LabelP='Valence'
        else if(NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane.or.
     1          NPar.eq.IdDrawIndAng) then
          LabelP='Angle'
        else if(NPar.eq.IdMagMoment) then
          if(IPar.le.3) then
            LabelP='M'//smbx(IPar)
          else
            LabelP='|M|'
          endif
        endif
        if(YLabel.eq.' ') then
          YLabel=LabelP
        else if(YLabel.ne.LabelP) then
          if(NPar.eq.IdDrawXYZ) then
            YLabel='xyz'
          else if(NPar.eq.IdDrawDeltaXYZ) then
            YLabel='dxyz'
          else if(NPar.eq.IdDrawADP) then
            YLabel='U'
          endif
        endif
        if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1    then
          if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1       NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2       NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
            Veta='# '//LabelP(:idel(LabelP))//'-'//
     1          smbt(ior(1))//' plot for '//
     2          DrawAtom(1,i)(:idel(DrawAtom(1,i)))
            write(85,FormA) Veta(:idel(Veta))
          else if(NPar.eq.IdDrawVal) then
            Veta='# '//LabelP(:idel(LabelP))//'-'//
     1          smbt(ior(1))//' plot for '//
     2          DrawAtom(1,i)(:idel(DrawAtom(1,i)))
            write(85,FormA) Veta(:idel(Veta))
            if(jc.gt.0) then
              write(85,'(''# curve includes modulation'')')
            else
              write(85,'(''# curve doesn''''t include modulation'')')
            endif
          else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles) then
            Veta='# to '//DrawSymSt(i)(:idel(DrawSymSt(i)))
            if(NPar.eq.IdDrawAngles)
     1        Veta=Veta(:idel(Veta))//' and '//
     2             DrawSymStU(i)(:idel(DrawSymStU(i)))
            write(85,FormA) Veta(:idel(Veta))
            if(jc.gt.0) then
              write(85,'(''# curve includes modulation'')')
            else
              write(85,'(''# curve doesn''''t include modulation'')')
            endif
          endif
          write(85,'(''# Color : '',i15,i5)') Color,LineTypeDraw
        endif
        call GrtDrawPerPartes(DrawT,DrawValue(1,i),DrawOcc(1,i),
     1                        DrawPoints,LineTypeDraw,NormalPlotMode,
     2                        Color)
        Prazdno=.false.
        if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100)
     1    write(85,'(''# end of curve'')')
3000  continue
      if(DrawTCommen) then
        n=NCommQ(1,isw,KPhase)*(ngc(KPhase)+1)
        fn=1./float(n)
        xp(3)=0.
        call FeLineType(DashedLine)
        do k=0,0
          if(k.eq.0) then
            call CopyVek(trez(1,isw,KPhase),tp,NDimI(KPhase))
          else
            do j=4,NDim(KPhase)
              call CopyVek(rm6gc(4+(j-1)*NDim(KPhase),k,1,KPhase),
     1                     Gamma(1+(j-4)*NDimI(KPhase)),NDimI(KPhase))
            enddo
            call Multm(Gamma,trez(1,1,KPhase),tp,NDimI(KPhase),
     1                 NDimI(KPhase),1)
            call AddVek(tp,s6gc(4,k,1,KPhase),tp,NDimI(KPhase))
            tp(1)=tp(1)-scalmul(s6gc(1,k,1,KPhase),qu(1,1,1,KPhase))
          endif
          pom=tp(1)
          do i=1,n
            xp(1)=pom
3100        if(xp(1).gt.xomn) then
              xp(1)=xp(1)-1.
              go to 3100
            endif
3200        if(xp(1).lt.xomx) then
              if(xp(1).ge.xomn) then
                xp(2)=yomn
                call multm(F2O,xp,xo,3,3,1)
                xu(1)=FeXo2X(xo(1))
                yu(1)=FeYo2Y(xo(2))
                xp(2)=yomx
                call multm(F2O,xp,xo,3,3,1)
                xu(2)=FeXo2X(xo(1))
                yu(2)=FeYo2Y(xo(2))
                call FePolyLine(2,xu,yu,White)
              endif
              xp(1)=xp(1)+1.
              go to 3200
            endif
            pom=pom+fn
          enddo
        enddo
        call FeLineType(NormalLine)
      endif
      if(Klic.eq.0) call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,YLabel)
9999  return
      end
      subroutine GrtDrawCurveOrMap(Klic,NovaMapa,GraphExists)
      use Basic_mod
      use Atoms_mod
      use Grapht_mod
      use Dist_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      real, allocatable :: fna(:,:,:)
      dimension xsym(3),x4dif(3),tdif(3),nd(3),uxi(3,mxw),it(3),xx(3),
     1          uyi(3,mxw),bxi(6,mxw),byi(6,mxw),xpp(3),xp(6),rmb(36),
     2          GammaInt(9),GammaIntInv(9),bi(6),tfirstg(3),XDstSym(6),
     3          TrPom(36),upom(9),cpom(3,3),rpom(3,3),qcnta(3),sm(3)
      character*80 t80,u80,DruhySymmString
      character*8 atp1,atp2,at1,at2
      character*2 Label
      integer ColorOrder,RecModToRecMap,DrawNP,HardCopyIn,CrlCentroSymm,
     1        n123(3)
      logical GraphExists,EqIgCase
      data tpisq/19.7392088/
      HardCopyIn=HardCopy
      GraphExists=.false.
      if(NDimI(KPhase).gt.1) then
        if(Klic.gt.0) then
          NMapGrt=min(NMapGrt+1,NxMapMx)
        else if(Klic.lt.0) then
          NMapGrt=max(NMapGrt-1,1)
        endif
        if(WhatToDraw.ne.0.and.NovaMapa.eq.0) go to 6000
      else
        tfirstg(1)=gx(1,1)
      endif
      NPar=DrawParam(1)/10
      if(WhatToDraw.ne.0) then
        nmap=NxMapMx
        rewind l80
        if(nmap.gt.0)
     1    call FeFlowChartOpen(-1.,-1.,1,nmap,'Calculation of maps',' ',
     2                         ' ')
      else
        if(HardCopy.gt.100) then
          nmap=NxMapMx
        else
          nmap=1
        endif
      endif
      if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1   NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2   NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment.or.
     3   NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
        if(WhatToDraw.eq.0) then
          DrawNP=DrawN
        else
          DrawNP=1
        endif
        if(NPar.eq.IdDrawTorsion) then
          NAtoms=4
        else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
          NAtoms=2
        else
          NAtoms=1
        endif
      else
        DrawNP=1
      endif
      do nm=1,nmap
        if(WhatToDraw.eq.0.and.HardCopy.lt.100) then
          jmap=NMapGrt
        else
          jmap=nm
        endif
        call RecUnpack(jmap,nd,NxMap,NDimI(KPhase)-ndGrt)
        do k=1,NDimI(KPhase)
          j=ior(k)
          if(k.le.ndGrt) then
            tfirstg(j)=gx(1,j)
          else
            tfirstg(j)=gx(1,j)+(nd(k-ndGrt)-1)*gx(3,j)
          endif
        enddo
        call GrtSetDistKeys(NPar,tfirstg)
        CrenelX40=-33333.
        CrenelDelta=-33333.
        CrenelQcnt=-33333.
        do 5000i=1,DrawNP
          if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1       NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2       NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
            IPar=max(mod(DrawParam(i),10),1)
            do j=1,NAtoms
              call AtomSymCode(DrawAtom(j,i),ia,isym,l,XDstSym,it,ich)
              if(ich.ne.0) go to 5000
              call atsymi(DrawAtom(j,i),ia,xsym,x4dif,tdif,isym,k,*5000)
              if(j.eq.1) then
                isw=ISwSymm(isym,iswa(ia),KPhase)
                call multm(rm(1,isym,isw,KPhase),XDst(1,ia),xp,3,3,1)
                if(isym.lt.0) call RealVectorToOpposite(xp,xp,3)
                call AddVek(xp,XDstSym,XDstSym,3)
                call srotb(TrToOrtho(1,isw,KPhase),
     1                     TrToOrtho(1,isw,KPhase),TrPom)
              else
                isw=iswa(ia)
                if(isw.ne.iswa(ia)) then


                endif
              endif
            enddo
            if(NPar.ne.IdDrawTorsion) then
              call GetGammaIntInv(RM6(1,isym,isw,KPhase),GammaIntInv)
              call MatInv(GammaIntInv,GammaInt,det,NDimI(KPhase))
              if(NPar.eq.IdDrawXYZ.or.
     1           NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawOcc) then
                do l=1,KModA(2,ia)
                  call multm(rm(1,isym,isw,KPhase),ux(1,l,ia),uxi(1,l),
     1                       3,3,1)
                  if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0) then
                     call multm(rm(1,isym,isw,KPhase),uy(1,l,ia),
     1                          uyi(1,l),3,3,1)
                  else
                     call CopyVek(uy(1,l,ia),uyi(1,l),3)
                  endif
                  if(isym.lt.0) then
                    do j=1,3
                      uxi(j,l)=-uxi(j,l)
                      if(l.ne.KModA(2,ia).or.KFA(2,ia).eq.0)
     1                  uyi(j,l)=-uyi(j,l)
                    enddo
                  endif
                enddo
              else if(NPar.eq.IdMagMoment) then
                call multm(rmag(1,isym,isw,KPhase),sm0(1,ia),sm,3,3,1)
                if(isym.lt.0.and.
     1            ZMag(CrlCentroSymm(),isw,KPhase).lt.0) then
                  do j=1,3
                    sm(j)=-sm(j)
                  enddo
                endif
                do l=1,MagPar(ia)-1
                  call multm(rmag(1,isym,isw,KPhase),smx(1,l,ia),
     1                       uxi(1,l),3,3,1)
                  call multm(rmag(1,isym,isw,KPhase),smy(1,l,ia),
     1                          uyi(1,l),3,3,1)
                  if(isym.lt.0.and.
     1              ZMag(CrlCentroSymm(),isw,KPhase).lt.0) then
                    do j=1,3
                      uxi(j,l)=-uxi(j,l)
                      uyi(j,l)=-uyi(j,l)
                    enddo
                  endif
                enddo
              else if(NPar.eq.IdDrawADP) then
                call srotb(rm(1,isym,isw,KPhase),rm(1,isym,isw,KPhase),
     1                     rmb)
                call multm(rmb,beta(1,ia),bi,6,6,1)
                do j=1,KModA(3,ia)
                  call multm(rmb,bx(1,j,ia),bxi(1,j),6,6,1)
                  call multm(rmb,by(1,j,ia),byi(1,j),6,6,1)
                enddo
              endif
            endif
          else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles.or.
     1            NPar.eq.IdDrawVal) then
            if(WhatToDraw.eq.0) then
              do j=1,10
                DrawColor(j)=ColorOrder('White')
                if(DrawNonMod.and.mod(j,2).eq.0)
     1            DrawColor(j)=-DrawColor(j)
              enddo
            endif
            j=index(DrawAtom(1,1),'#')
            if(j.gt.0.and.EqIgCase(DrawAtom(1,1)(j+1:),'s1')) then
              DrawAtom(1,1)=DrawAtom(1,1)(:j-1)
              j=0
            endif
            if(j.gt.0) then
              PrvniSymmString=DrawAtom(1,1)
              atp1=DrawAtom(1,1)(:j-1)
            else
              PrvniSymmString=' '
              atp1=DrawAtom(1,1)
            endif
            if(NPar.le.IdDrawVal.and.DrawNonMod)
     1        DrawAtom(1,2)=DrawAtom(1,1)
            call atsymi(DrawAtom(1,1),ia,xsym,x4dif,tdif,isym,k,*5000)
            IPar=max(mod(DrawParam(1),10),1)
            isw=ISwSymm(isym,iswa(ia),KPhase)
            call SetLogicalArrayTo(BratPrvni,NAtCalc,.false.)
            BratPrvni(ia)=.true.
          endif
          if(GrtDrawX4) then
            tdif=x4dif
            qcnta=0.
          else
            if(NPar.ne.IdDrawPlane.and.NPar.ne.IdDrawIndDist.and.
     1         NPar.ne.IdDrawIndAng)
     2        call CopyVek(qcnt(1,ia),qcnta,NDimI(KPhase))
          endif
          if(NPar.eq.IdDrawOcc.or.NPar.eq.IdDrawXYZ.or.
     1       NPar.eq.IdDrawDeltaXYZ.or.NPar.eq.IdDrawTorsion.or.
     2       NPar.eq.IdDrawADP.or.NPar.eq.IdMagMoment) then
            if(NPar.ne.IdDrawTorsion) then
              call AddVek(tdif,tfirstg,xpp,NDimI(KPhase))
              call multm(GammaIntInv,xpp,xp,NDimI(KPhase),NDimI(KPhase),
     1                   1)
              if(TypeModFun(ia).le.1.or.KModA(1,ia).le.1) then
                call MakeOccMod(DrawOcc(1,i),qcnta,xp,NxMod,DxMod,
     1                          a0(ia),ax(1,ia),ay(1,ia),KModA(1,ia),
     2                          KFA(1,ia),GammaIntInv)
              else
                call MakeOccModPol(DrawOcc(1,i),qcnta,xp,NxMod,DxMod,
     1                             ax(1,ia),ay(1,ia),KModA(1,ia)-1,
     2                             ax(KModA(1,ia),ia),a0(ia)*.5,
     3                             GammaIntInv,TypeModFun(ia))
              endif
              call SetRealArrayTo(xx,3,0.)
              kk=0
              if(KFA(1,ia).ne.0) then
                xx(1)=GammaIntInv(1)*ax(1,ia)+s6(4,isym,isw,KPhase)
                if(CrenelDelta.lt.0.) then
                  CrenelDelta=a0(ia)
                  CrenelQcnt=qcnta(1)
                  CrenelX40=xx(1)
                endif
                kk=1
              endif
              if(KFA(2,ia).gt.0.and.KModA(2,ia).gt.0) then
                call MakeOccModSawTooth(DrawOccP(1,i),qcnta,xp,
     1            NxMod,DxMod,uyi(1,KModA(2,ia)),uyi(2,KModA(2,ia)),
     2            KFA(2,ia),GammaIntInv)
                do k=1,NxMod(1)*NxMod(2)*NxMod(3)
                  if(DrawOccP(k,i).gt.0) then
                    DrawOcc(k,i)=DrawOccP(k,i)
                  else
                    DrawOcc(k,i)=0.
                  endif
                enddo
                xx(1)=uyi(1,KModA(2,ia))
                kk=1
              endif
              call SpecPos(x(1,ia),xx,kk,iswa(ia),.05,nocc)
              if(EqIgCase(Atom(ia)(1:3),'Dum')) then
                pom=float(nocc)
              else
                pom=ai(ia)*float(nocc)
              endif
              do k=1,NxMod(1)*NxMod(2)*NxMod(3)
                DrawOcc(k,i)=DrawOcc(k,i)*pom
              enddo
              if(NPar.eq.IdDrawOcc) then
                NRank=1
              else if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
                if(TypeModFun(ia).le.1) then
                  call MakePosMod(XMod,qcnta,xp,NxMod,DxMod,uxi,
     1                            uyi,KModA(2,ia),KFA(2,ia),GammaIntInv)
                else
                  call MakePosModPol(XMod,qcnta,xp,NxMod,DxMod,uxi,
     1                               uyi,KModA(2,ia),ax(KModA(1,ia),ia),
     2                               a0(ia)*.5,GammaIntInv,
     3                               TypeModFun(ia))
                endif
                NRank=3
              else if(NPar.eq.IdMagMoment) then
                call MakeMagMod(XMod,qcnta,xp,NxMod,DxMod,sm,uxi,uyi,
     1                          MagPar(ia)-1,GammaIntInv)
                NRank=3
              else if(NPar.eq.IdDrawADP) then
                if(TypeModFun(ia).le.1) then
                  call MakeBetaMod(XMod,qcnta,xp,NxMod,DxMod,bxi,
     1                             byi,KModA(3,ia),KFA(3,ia),
     2                             GammaIntInv)
                else
                  call MakeBetaModPol(XMod,qcnta,xp,NxMod,DxMod,
     1                                bxi,byi,KModA(3,ia),
     2                                ax(KModA(1,ia),ia),a0(ia)*.5,
     3                                GammaIntInv,TypeModFun(ia))
                endif
                NRank=6
              endif
            else
              call DistTorsionAngle(DrawAtom(1,i),' ')
              DrawSymSt(i)=' '
              DrawSymStU(i)=' '
              rewind 78
              if(WhatToDraw.eq.0) t=xomn
1900          read(78,end=2000) k,pom,Occ
              if(WhatToDraw.eq.0) then
                DrawT(k)=t
                kk=k
                t=t+DxMod(ior(1))
              else
                kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
              endif
              DrawValue(kk,i)=pom
              DrawOcc(kk,i)=Occ
              go to 1900
2000          IPar=1
              NRank=1
              close(78,status='delete')
              go to 5000
            endif
            if(WhatToDraw.eq.0) t=xomn
            if(IPar.le.3) then
              j=IPar
            else
              j=1
            endif
            do k=1,DrawPoints
              if(WhatToDraw.eq.0) then
                DrawT(k)=t
                kk=k
              else
                kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
              endif
              if(NPar.eq.IdDrawOcc) then
                DrawValue(kk,i)=DrawOcc(k,i)
                if(WhatToDraw.eq.0) DrawOcc(k,i)=1.
              else if(NPar.eq.IdDrawXYZ) then
                DrawValue(kk,i)=XMod(j)+xsym(IPar)
              else if(NPar.eq.IdDrawDeltaXYZ) then
                DrawValue(kk,i)=(XMod(j)+XSym(IPar)-XDstSym(IPar))*
     1                          CellPar(IPar,isw,KPhase)
              else if(NPar.eq.IdDrawADP) then
                if(IPar.le.3) then
                  DrawValue(kk,i)=(XMod(j)+bi(IPar))/
     1                            urcp(IPar,isw,KPhase)
                else if(IPar.eq.4) then
                  call AddVek(XMod(j),bi,XMod(j),6)
                  call boueq(XMod(j),sbeta(1,ia),0,DrawValue(kk,i),pom,
     1                       isw)
                else
                  call AddVek(XMod(j),bi,XMod(j),6)
                  call MultM(trpom,XMod(j),upom,6,6,1)
                  do m=1,6
                    upom(m)=upom(m)/tpisq
                  enddo
                  do m=1,6
                    call indext(m,ii,jj)
                    cpom(ii,jj)=upom(m)
                    if(ii.ne.jj) cpom(jj,ii)=upom(m)
                  enddo
                  call qln(cpom,rpom,upom,3)
                  pmin=upom(1)
                  pmax=upom(1)
                  mmin=1
                  mmax=1
                  do m=2,3
                    if(upom(m).gt.pmax) then
                      pmax=max(pmax,upom(m))
                      mmax=m
                    endif
                    if(upom(m).lt.pmin) then
                      pmin=min(pmin,upom(m))
                      mmin=m
                    endif
                  enddo
                  mmean=6-mmax-mmin
                  if(IPar.eq.5) then
                    DrawValue(kk,i)=upom(mmax)
                  else if(IPar.eq.6) then
                    DrawValue(kk,i)=upom(mmean)
                  else if(IPar.eq.7) then
                    DrawValue(kk,i)=upom(mmin)
                  endif
                endif
              else if(NPar.eq.IdMagMoment) then
                if(IPar.le.3) then
                  DrawValue(kk,i)=XMod(j)*CellPar(IPar,isw,KPhase)
                else
                  call MultM(TrToOrtho(1,isw,KPhase),XMod(j),sm,3,3,1)
                  DrawValue(kk,i)=VecOrtLen(sm,3)
                endif
              endif
              DrawValue(kk,i)=DrawValue(kk,i)-RefLevel
              if(WhatToDraw.eq.0) t=t+DxMod(ior(1))
              j=j+NRank
            enddo
          else if(NPar.eq.IdDrawPlane) then
            allocate(fna(3,DrawPoints,2))
            if(GrtPlaneDir) then
              idm=1
            else
              idm=2
            endif
            DrawOcc=0.
            if(NAtPlane.gt.NAtomArr) then
              if(allocated(AtomArr)) deallocate(AtomArr)
              allocate(AtomArr(NAtPlane))
              NAtomArr=NAtPlane
            endif
            do 2130id=1,idm
              call CloseIfOpened(78)
              call SplitToAtomStrings(GrtPlane(id),AtomArr,n,1,u80)
              if(id.eq.1) then
                Cislo=AtomArr(1)
                k=index(Cislo,'#')
                if(k.gt.0) Cislo(k:)=' '
                k=ktat(Atom,NAtCalc,Cislo)
                isw=iswa(k)
              endif
              nr=0
              call DistBestPlane(AtomArr,n,n,GrtESD,isw,nr,cpom,rpom)
              rewind 78
              if(WhatToDraw.eq.0) t=xomn
2120          read(78,end=2130) k,xpp,Occ
              if(WhatToDraw.eq.0) then
                DrawT(k)=t
                kk=k
                t=t+DxMod(ior(1))
              else
                kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
              endif
              call CopyVek(xpp,fna(1,kk,id),3)
              DrawOcc(kk,i)=max(Occ,DrawOcc(kk,i))
              go to 2120
2130        continue
            close(78,status='delete')
            if(GrtPlaneDir) then
              k=0
              call StToReal(GrtPlane(2),k,xpp,3,.false.,ich)
              if(ich.ne.0) then
                call FeChybne(-1.,-1.,'numerical problem in '//
     1                        'the direction string',GrtPlane(2),
     2                        SeriousError)
                go to 5000
              endif
              call multm(MetTensI(1,isw,KPhase),xpp,xp,3,3,1)
              call VecNor(xpp,xp)
            endif
            xp=0.
            do kt=1,ntall(isw)
              if(DrawOcc(kt,i).le.ocut) cycle
              if(.not.GrtPlaneDir) call CopyVek(fna(1,kt,2),xpp,3)
              call DistDihedralAngle(fna(1,kt,1),xpp,xp,xp,isw,
     1                               DrawValue(kt,i),pom)
            enddo
            close(78)
            deallocate(fna)
          else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
            if(NAtomArr.le.0) then
              if(allocated(AtomArr)) deallocate(AtomArr)
              allocate(AtomArr(1))
              NAtomArr=1
            endif
            if(NPar.eq.IdDrawIndDist) then
              idm=2
            else
              idm=3
            endif
            n=0
            do id=1,idm
              call SplitToAtomStrings(DrawAtom(id,i),AtomArr,n123(id),
     1                                0,u80)
              n=n+n123(id)
            enddo
            n1=n123(1)
            n2=n123(1)+n123(2)
            n3=n
            if(n.gt.NAtomArr) then
              if(allocated(AtomArr)) deallocate(AtomArr)
              allocate(AtomArr(n))
              NAtomArr=n
            endif
            ip=1
            do id=1,idm
              call SplitToAtomStrings(DrawAtom(id,i),AtomArr(ip),
     1                                n123(id),1,u80)
              ip=ip+n123(id)
            enddo
            Cislo=AtomArr(1)
            k=index(Cislo,'#')
            if(k.gt.0) Cislo(k:)=' '
            k=ktat(Atom,NAtCalc,Cislo)
            isw=iswa(k)
            if(NPar.eq.IdDrawIndDist) then
              call DistCenter(AtomArr,n1,n2,isw)
            else
              call AngleCenter(AtomArr,n1,n2,n3,isw)
            endif
            rewind 78
            if(WhatToDraw.eq.0) t=xomn
2220        read(78,end=2230) k,pom,Occ
            if(WhatToDraw.eq.0) then
              DrawT(k)=t
              kk=k
              t=t+DxMod(ior(1))
            else
              kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
            endif
            DrawOcc(kk,i)=Occ
            DrawValue(kk,i)=pom
            go to 2220
2230        close(78)
          else
            IColor=ColorOrder('White')
            ocut=-1111.
            call DistAt
            rewind 78
            DrawN=0
            iprv=0
            if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles.or.
     1         NPar.eq.IdDrawVal) call GrtMakeLabel(jmap,tfirstg)
            NPar10=NPar*10
3300        read(78,end=4500) Label,At1,isfi,j,(pom,j=1,7),t80
            if(Label.ne.'#1'.or..not.EqIgCase(At1,atp1)) go to 3300
!            if(NPar.eq.IdDrawIndDist) then
!              j=index(t80,'#')
!              if(j.lt.idel(t80)) then
!                u80=At1(:idel(At1))//t80(j:)
!              else
!                u80=' '
!              endif
!              if(.not.EqIgCase(PrvniSymmString,u80)) go to 3300
!            endif
3310        read(78,end=4500) Label,At2,isfj,j,aij,(pom,j=1,6),t80
3315        if(Label.eq.'#2'.or.Label.eq.'#3') then
!              if(NPar.eq.IdDrawIndDist) then
!                if(.not.EqIgCase(At2,atp2)) go to 3310
!                j=index(t80,'#')
!                if(j.lt.idel(t80)) then
!                  u80=At2(:idel(At2))//t80(j:)
!                else
!                  u80=' '
!                endif
!              endif
              t80=At2(:idel(At2))//'#'//t80(:idel(t80))
            endif
            if(Label.eq.'#2') then
              if(NPar.eq.IdDrawAngles) then
                u80=t80
                go to 3310
!              else if(NPar.eq.IdDrawIndDist) then
!                if(.not.EqIgCase(DruhySymmString,u80)) go to 3310
              else
                PomRho=dBondVal(isfi,isfj)
                PomB=cBondVal(isfi,isfj)
                go to 3320
              endif
            else if(Label.eq.'#3') then
              if(NPar.eq.IdDrawAngles) then
                go to 3320
              else
                go to 3310
              endif
            else
              go to 3310
            endif
3320        if(NPar.ne.IdDrawVal.or.DrawN.eq.0) then
              call ReallocateGrtDraw(10)
              DrawN=DrawN+1
              if(UseRefLevel) then
                pom=-RefLevel
              else
                pom=0.
              endif
              call SetRealArrayTo(DrawOcc(1,DrawN),DrawPoints,0.)
              call SetRealArrayTo(DrawValue(1,DrawN),DrawPoints,pom)
              if(DrawNonMod) then
                call SetRealArrayTo(DrawOcc(1,DrawN+1),DrawPoints,0.)
                call SetRealArrayTo(DrawValue(1,DrawN+1),DrawPoints,pom)
              endif
            endif
            if(Label.eq.'#2') then
              DrawSymSt(DrawN)=t80
              DrawSymStU(DrawN)=' '
              DrawParam(DrawN)=NPar10
              DrawColor(DrawN)=IColor
              if(DrawNonMod) then
                DrawSymSt(DrawN+1)=t80
                DrawSymStU(DrawN+1)=' '
                DrawParam(DrawN+1)=NPar10
                DrawColor(DrawN+1)=-IColor
              endif
            else if(Label.eq.'#3') then
              DrawSymSt(DrawN)=u80
              DrawSymStU(DrawN)=t80
              DrawParam(DrawN)=NPar10
              DrawColor(DrawN)=IColor
              if(DrawNonMod) then
                DrawSymSt(DrawN+1)=u80
                DrawSymStU(DrawN+1)=t80
                DrawParam(DrawN+1)=NPar10
                DrawColor(DrawN+1)=-IColor
              endif
            endif
3325        read(78,end=4500) Label,At2,k,j,pomn,pom,spom,occi,occj,
     1                        occk,pp,t80
            if(Label.ne.' ') then
              isfj=k
              aij=pomn
              if(DrawNonMod.and.
     1           (NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles))
     2          DrawN=DrawN+1
              if(DrawN.ge.10) then
                call GrtDrawCurves(iprv,jmap,isw)
                iprv=1
                DrawN=0
                GraphExists=.true.
              endif
              go to 3315
            endif
            call RecUnpack(k,nd,nt(1,1),NDimI(KPhase))
            if(WhatToDraw.eq.0) then
              kk=k
            else
              kk=RecModToRecMap(k,NxMod,ior,NDimI(KPhase))
            endif
            if(WhatToDraw.eq.0) then
              DrawT(kk)=tfirst(ior(1),1)+(nd(ior(1))-1)*dt(ior(1),1)
              DrawOcc(kk,DrawN)=1.
              if(DrawNonMod) DrawOcc(kk,DrawN+1)=1.
            endif
            if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawAngles) then
              if(occi.le.OccLimit.or.occj.le.OccLimit.or.
     1           (NPar.eq.IdDrawAngles.and.occk.le.OccLimit)) then
                DrawOcc(kk,DrawN)=0.
                go to 3325
              endif
              if(UseReflevel) then
                DrawValue(kk,DrawN)=pom-RefLevel
              else
                DrawValue(kk,DrawN)=pom
              endif
              if(DrawNonMod) DrawValue(kk,DrawN+1)=pomn
            else
              if(occi.le.OccLimit) then
                DrawOcc(kk,DrawN)=0.
                go to 3325
              endif
              if(PomRho.le.0) go to 3325
              DrawValue(kk,DrawN)=DrawValue(kk,DrawN)+
     1                            exp((PomRho-pom)/PomB)*aij*occj
              if(DrawNonMod)
     1          DrawValue(kk,DrawN+1)=DrawValue(kk,DrawN+1)+
     2                                exp((PomRho-pomn)/PomB)*aij*occj
            endif
            go to 3325
4500        close(78,status='delete')
            if(WhatToDraw.eq.0) then
              if(DrawNonMod.and.DrawN.gt.0.and.
     1           NPar.ne.IdDrawDist.and.NPar.ne.IdDrawAngles)
     2          DrawN=DrawN+1
              if(DrawN.gt.0) then
                call GrtDrawCurves(iprv,jmap,isw)
                GraphExists=.true.
              endif
            endif
          endif
          if(WhatToDraw.ne.0) then
            write(l80)(DrawValue(k,1),k=1,DrawPoints)
            nmp=nm
            call FeFlowChartEvent(nmp,ie)
            if(ie.ne.0) then
              call FeBudeBreak
              if(ErrFlag.ne.0) go to 9999
            endif
          endif
          call FeReleaseOutput
          call FeDeferOutput
5000    continue
        if(NPar.ne.IdDrawDist.and.NPar.ne.IdDrawAngles.and.
     1     NPar.ne.IdDrawVal) then
          if(WhatToDraw.eq.0) then
            call GrtMakeLabel(jmap,tfirstg)
            call GrtDrawCurves(0,jmap,isw)
            call FeReleaseOutput
            call FeDeferOutput
            GraphExists=DrawN.gt.0
          endif
        else
          DrawParam(1)=NPar10
          NPar=NPar10/10
          call ReallocateGrtDraw(10)
          DrawN=1
        endif
      enddo
6000  if(NPar.ne.IdDrawDist.and.NPar.ne.IdDrawAngles.and.
     1   WhatToDraw.ne.0) then
        call FeFlowChartRemove
        call GrtSetTr
        call GrtDrawMap(1)
        GraphExists=.true.
      else
        call FeFlowChartRemove
      endif
      if(CrenelDelta.gt.0.and.GrtShowCrenel) then
       xp(1)=CrenelX40-.5*CrenelDelta-CrenelQcnt
        call FeLineType(DottedLine)
        write(85,'(''# Crenel'')')
        do ii=1,2
          xp(2)=yomn
          xp(3)=0.
          write(85,'(2f15.6)') xp(1),xp(2)
          call FeXf2X(xp,xx)
          xu(1)=xx(1)
          yu(1)=xx(2)
          xp(2)=yomx
          write(85,'(2f15.6)') xp(1),xp(2)
          call FeXf2X(xp,xx)
          xu(2)=xx(1)
          yu(2)=xx(2)
          call FePolyLine(2,xu,yu,White)
          xp(1)=xp(1)+CrenelDelta
          write(85,'(''# end of curve'')')
        enddo
        call FeLineType(NormalLine)
      endif
      if(WhatToDraw.eq.0.and.(HardCopy.eq.HardCopyNum.or.
     1   HardCopy.eq.HardCopyNum+100)) write(85,'(''# end of file'')')
      call FeHardCopy(HardCopy,'close')
      if(JedeMovie) then
        call FeReleaseOutput
        call FeDeferOutput
      endif
9999  HardCopy=HardCopyIn
      return
133   format(a2,1x,a8,i6,3f10.4,1x,a80)
      end
      subroutine GrtMakeLabel(n,tfirstg)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension nd(3),tfirstg(3)
      call RecUnpack(n,nd,NxMap,NDimI(KPhase)-ndGrt)
      do i=1,NDimI(KPhase)
        j=ior(i)
        if(i.le.ndGrt) then
          tfirstg(j)=gx(1,j)
        else
          tfirstg(j)=gx(1,j)+(nd(i-ndGrt)-1)*gx(3,j)
        endif
      enddo
      if(NDimI(KPhase).gt.ndGrt) then
        write(GraphtLabel,'(3(a1,''='',f10.3,'',''))')
     1    (smbt(ior(j)),tfirstg(ior(j)),j=ndGrt+1,NDimI(KPhase))
        call Zhusti(GraphtLabel)
        j=idel(GraphtLabel)
        GraphtLabel(j:j)=' '
      else
        GraphtLabel=' '
      endif
      return
      end
      subroutine GrtUpdateDrawAtom(ia)
      use Grapht_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      character*256 EdwStringQuest,AtPom
      logical CrwLogicQuest
      NPar=DrawParam(ia)/10
      if(NPar.eq.IdDrawTorsion) then
        NAtoms=4
      else if(NPar.eq.IdDrawIndDist.or.NPar.eq.IdDrawIndAng) then
        AtPom=EdwStringQuest(nEdwPlane1)
        DrawAtom(1,ia)=AtPom
        AtPom=EdwStringQuest(nEdwPlane2)
        DrawAtom(2,ia)=AtPom
        if(NPar.eq.IdDrawIndAng) then
          AtPom=EdwStringQuest(nEdwPlane3)
          DrawAtom(3,ia)=AtPom
        endif
        DrawSymSt(ia)=' '
        DrawSymStU(ia)=' '
        NAtoms=0
      else
        NAtoms=1
      endif
      nEdw=nEdwName
      do i=1,NAtoms
        AtPom=EdwStringQuest(nEdw)
        nEdw=nEdw+1
        j=index(AtPom,'#')
        if(j.le.0) then
          j=idel(AtPom)
        else
          j=j-1
        endif
        if(AtPom(:j).ne.' ') call UprAt(AtPom(:j))
        DrawAtom(i,ia)=AtPom
        DrawSymSt(ia)=' '
        DrawSymStU(ia)=' '
      enddo
      if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        ik=3
      else if(NPar.eq.IdMagMoment) then
        ik=4
      else if(NPar.eq.IdDrawADP) then
        ik=7
      else
        ik=0
      endif
      nCrw=nCrwParFirst
      do i=1,ik
        if(CrwLogicQuest(nCrw)) go to 2000
        nCrw=nCrw+1
      enddo
      i=1
2000  DrawParam(ia)=10*NPar+i
      return
      end
      subroutine GrtSaveDrawAtom
      use Grapht_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension iorSave(3)
      character*80, allocatable :: DrawSymStSave(:),DrawSymStUSave(:)
      character*256, allocatable :: DrawAtomSave(:,:)
      integer, allocatable :: DrawColorSave(:),DrawParamSave(:)
      save DrawNSave,DrawAtomSave,DrawColorSave,DrawParamSave,
     1     DrawSymStSave,DrawSymStUSave,iorSave
      DrawNSave=DrawN
      if(allocated(DrawAtomSave))
     1  deallocate(DrawAtomSave,DrawSymStSave,DrawSymStUSave,
     2             DrawColorSave,DrawParamSave)
      allocate(DrawAtomSave(4,DrawN),DrawSymStSave(DrawN),
     1         DrawSymStUSave(DrawN),DrawColorSave(DrawN),
     2         DrawParamSave(DrawN))
      do i=1,DrawN
        DrawAtomSave(1:4,i)=DrawAtom(1:4,i)
        DrawParamSave(i)=DrawParam(i)
        DrawColorSave(i)=DrawColor(i)
        DrawSymStSave(i)=DrawSymSt(i)
        DrawSymStUSave(i)=DrawSymStU(i)
      enddo
      do i=1,NDimI(KPhase)
        iorSave(i)=ior(i)
      enddo
      go to 9999
      entry GrtRestoreDrawAtom
      DrawN=DrawNSave
      do i=1,DrawN
        DrawAtom(1:4,i)=DrawAtomSave(1:4,i)
        DrawParam(i)=DrawParamSave(i)
        DrawColor(i)=DrawColorSave(i)
        DrawSymSt(i)=DrawSymStSave(i)
        DrawSymStU(i)=DrawSymStUSave(i)
      enddo
      do i=1,NDimI(KPhase)
        ior(i)=iorSave(i)
      enddo
      if(allocated(DrawAtomSave))
     1  deallocate(DrawAtomSave,DrawSymStSave,DrawSymStUSave,
     2             DrawColorSave,DrawParamSave)
9999  return
      end
      subroutine GrtDrawMap(Klic)
      use Grapht_mod
      use Contour_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      character*80 Veta
      character*8  LabelP
      real tfirstg(3)
      if(Klic.eq.0) go to 2000
      VolaToContour=.false.
      if(UseRefLevel) then
        ContourRefLevel=RefLevel
      else
        ContourRefLevel=0.
      endif
      DensityType=2
      do i=1,NDimI(KPhase)
        if(GrtDrawX4) then
          cx(i)=smbx6(ior(i)+3)
        else
          cx(i)=smbt(ior(i))
        endif
        iorien(i)=ior(i)
        ee(i)=1.
      enddo
      iorien(3)=6-iorien(1)-iorien(2)
      zmap(1)=0.
      do i=4,NDim(KPhase)
        iorien(i)=i
        zmap(i-2)=0.
      enddo
      call GrtSetTr
      DMin= 999.
      DMax=-999.
      rewind l80
      do j=1,NxMapMx
        read(l80)(DrawValue(i,1),i=1,DrawPoints)
        do i=1,DrawPoints
          pom=DrawValue(i,1)
          DMin=min(pom,DMin)
          DMax=max(pom,DMax)
        enddo
      enddo
      do i=1,2
        nx(i)=NxMod(ior(i))
      enddo
      nxny=DrawPoints
      call SetContours(0)
      Obecny=.false.
      call UnitMat(tm3,3)
      do i=1,NDimI(KPhase)
        do j=1,NDimI(KPhase)
          if(j.eq.ior(i)) then
            tm3(i,j)=1.
          else
            tm3(i,j)=0.
          endif
        enddo
      enddo
      call MatInv(tm3,tm3i,pom,3)
2000  call FeHardCopy(HardCopy,'open')
      call GrtMakeLabel(NMapGrt,tfirstg)
      if(allocated(ActualMap)) deallocate(ActualMap)
      allocate(ActualMap(DrawPoints))
      call CopyVek(DrawValue,ActualMap,DrawPoints)
      NActualMap=DrawPoints
      if(HardCopy.eq.HardCopyNum.or.HardCopy.eq.HardCopyNum+100) then
        NPar=DrawParam(1)/10
        IPar=max(mod(DrawParam(1),10),1)
        if(NPar.eq.IdDrawOcc) then
          LabelP='occ'
        else if(NPar.eq.IdDrawXYZ) then
          LabelP=smbx(IPar)
        else if(NPar.eq.IdDrawDeltaXYZ) then
          LabelP='d'//smbx(IPar)
        else if(NPar.eq.IdDrawADP) then
          LabelP=SmbU(IPar)
        else if(NPar.eq.IdDrawTorsion) then
          LabelP='Tors'
        else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
          LabelP='Dist'
        else if(NPar.eq.IdDrawVal) then
          LabelP='Valence'
        else if(NPar.eq.IdDrawAngles.or.NPar.eq.IdDrawPlane.or.
     1          NPar.eq.IdDrawIndAng) then
          LabelP='Angle'
        else if(NPar.eq.IdMagMoment) then
          if(IPar.le.3) then
            LabelP='M'//smbx(IPar)
          else
            LabelP='|M|'
          endif
        endif
        Veta=cx(1)(:idel(cx(1)))//'-'//cx(2)(:idel(cx(2)))//' map'
        Veta='# '//LabelP(:idel(LabelP))//' '//Veta(:idel(Veta))//
     1       ' for '//DrawAtom(1,1)(:idel(DrawAtom(1,1)))
        write(85,FormA) Veta(:idel(Veta))
        Veta='# columns: '//cx(1)(:idel(cx(1)))//','//
     1                      cx(2)(:idel(cx(2)))//','
        if(NDim(KPhase).gt.5) Veta=Veta(:idel(Veta))//
     1                             cx(3)(:idel(cx(3)))//','
        Veta=Veta(:idel(Veta))//LabelP
        write(85,FormA) Veta(:idel(Veta))
        if(HardCopy.eq.HardCopyNum+100) then
          rewind l80
          pom3=xmin(3)
          do i3=1,NxMapMx
            read(l80)(DrawValue(i,1),i=1,DrawPoints)
            k=0
            pom2=xmin(2)
            do i1=1,nx(2)
              pom1=xmin(1)
              do i2=1,nx(1)
                k=k+1
                if(NDimI(KPhase).gt.5) then
                  write(85,100) pom1,pom2,pom3,DrawValue(k,1)
                else
                  write(85,100) pom1,pom2,DrawValue(k,1)
                endif
                pom1=pom1+dx(1)
              enddo
              pom2=pom2+dx(2)
            enddo
            pom3=pom3+dx(3)
          enddo
        endif
      endif
      rewind l80
      do i=1,NMapGrt
        read(l80)(DrawValue(k,1),k=1,DrawPoints)
      enddo
      if(HardCopy.eq.HardCopyNum) then
        k=0
        pom2=xmin(2)
        do i1=1,nx(2)
          pom1=xmin(1)
          do i2=1,nx(1)
            k=k+1
            write(85,'(3f10.5)') pom1,pom2,DrawValue(k,1)
            pom1=pom1+dx(1)
          enddo
          pom2=pom2+dx(2)
        enddo
        go to 9000
      endif
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeClearGrWin
        call FeMakeAcFrame
        pom=min(FeTxLength(GraphtLabel),
     1          abs(XCornAcWin(1,2)-XCornAcWin(1,3)))*.5
        call FeOutSt(0,(XCornAcWin(1,2)+XCornAcWin(1,3))*.5,
     1               (XCornAcWin(2,2)+XCornAcWin(2,3))*.5+15.-
     2               pom*abs(XCornAcWin(2,2)-XCornAcWin(2,3))/
     3               (XCornAcWin(1,2)-XCornAcWin(1,3)),GraphtLabel,'C',
     4               White)
        call FeMakeAxisLabels(1,xmin(1),xmax(1),xmin(2),xmax(2),cx(1))
        call FeMakeAxisLabels(2,xmin(2),xmax(2),xmin(1),xmax(1),cx(2))
        call CopyVek(DrawValue,ActualMap,DrawPoints)
        if(DrawPos.gt.0) then
          call Vrstvy( 1)
          if(ErrFlag.ne.0) go to 9999
        endif
        if(DrawNeg.gt.0) then
          call Vrstvy(-1)
          if(ErrFlag.ne.0) go to 9999
        endif
        if(HardCopy.eq.HardCopyNum) then

        endif
      endif
9000  call FeHardCopy(HardCopy,'close')
9999  return
100   format(4f10.5)
      end
      subroutine GrtGoTo(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension nd(2)
      character*5 t5
      il=NDimI(KPhase)-ndGrt
      call RecUnpack(NMapGrt,nd,NxMap,il)
      id=NextQuestId()
      xqd=150.
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Next graph to be drawn',0,
     1                   LightGray,0,0)
      il=0
      do i=ndGrt+1,NDimI(KPhase)
        il=il+1
        j=ior(i)
        t5='%'//smbt(j)//' ='
        call FeQuestEudMake(id,5.,il,30.,il,t5,'L',90.,EdwYd,0)
        pom=gx(1,j)+(nd(i-ndGrt)-1)*gx(3,j)
        if(i.eq.NDimI(KPhase)) nEdwLast=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
        call FeQuestEudOpen(EdwLastMade,1,1,1,gx(1,j),gx(2,j),gx(3,j))
      enddo
      nEdwLast=EdwLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwLast
        NMapGrt=0
        do i=NDimI(KPhase),ndGrt+1,-1
          j=ior(i)
          call FeQuestRealFromEdw(nEdw,pom)
          if(gx(3,j).ne.0.) then
            n=nint((pom-gx(1,j))/gx(3,j))+1
          else
            n=1
          endif
          NMapGrt=NMapGrt*NxMap(i-ndGrt)+n-1
          nEdw=nEdw-1
        enddo
        NMapGrt=NMapGrt+1
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine GrtDefMovie(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      character*256 EdwStringQuest
      character*80 Veta
      character*10 Label
      integer EdwStateQuest
      logical CrwLogicQuest
      data Label/'Record'/
      MovieFileName=' '
      il=NDim(KPhase)+8
      id=NextQuestId()
      xqd=250.
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Define parameters for movie',0,LightGray,0,0)
      il=1
      tpom=50.
      call FeQuestLblMake(id,tpom,il,'Start at','C','N')
      tpom=tpom+120.
      call FeQuestLblMake(id,tpom,il,'End at','C','N')
      tpom=5.
      xpom=30.
      dpom=80.
      call SetIntArrayTo(nxMovieFr,2,1)
      call CopyVekI(NxMod(2),nxMovieTo,2)
      do i=ndGrt+1,NDimI(KPhase)
        j=ior(i)
        il=il+1
        call FeQuestEudMake(id,tpom,il,xpom,il,smbt(j),'L',dpom,EdwYd,0)
        if(i.eq.ndGrt+1) nEdwLimFr=EdwLastMade
        call FeQuestRealEdwOpen(EdwLastMade,gx(1,j),.false.,.false.)
        call FeQuestEudOpen(EdwLastMade,1,1,1,gx(1,j),gx(2,j),gx(3,j))
        call FeQuestEudMake(id,tpom,il,xpom+dpom+40.,il,' ','L',dpom,
     1                      EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,gx(2,j),.false.,.false.)
        call FeQuestEudOpen(EdwLastMade,1,1,1,gx(1,j),gx(2,j),gx(3,j))
      enddo
      nEdwLimTo=EdwLastMade
      il=il+1
      Veta='%Delay time in sec'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=60.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwDelayTime=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelayTimeForMovie,.false.,
     1                        .false.)
      il=il+1
      Veta='%Repeat factor'
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRepeat=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,MovieRepeat,.false.)
      nEdwRepeat=EdwLastMade
      call FeQuestEudOpen(EdwLastMade,0,9999,1,1.,1.,1.)
      il=il+1
      Veta='Record the sequence of %maps'
      xpom=5.
      tpom=xpom+CrwXd+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwRecord=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,MovieFileName.ne.' ')
      il=il+1
      xpom=tpom+3.+FeTxLengthUnder(Veta)
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      Veta='%Name radix'
      dpom=110.
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFileName=EdwLastMade
      xpom=xpom+dpom+10.
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      il=il+1
      ilp=il
      tpom=FeTxLength(Label)+7.
      xpom=xqd-CrwgXd-100.
      do i=1,5
        call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                      HCMenu(i)(:idel(HCMenu(i)))//'s','L',CrwgXd,
     2                      CrwgYd,0,1)
        if(i.eq.1) nCrwTypeFirst=CrwLastMade
        il=il+1
      enddo
      call FeQuestLblMake(id,5.,ilp,Label,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLbl=LblLastMade
1400  if(CrwLogicQuest(nCrwRecord)) then
        if(EdwStateQuest(nEdwFileName).ne.EdwOpened) then
          HardCopy=HardCopyPCX
          call FeQuestStringEdwOpen(nEdwFileName,MovieFileName)
          call FeQuestButtonOpen(nButtBrowse,ButtonOff)
          call FeQuestLblOn(nLbl)
          nCrw=nCrwTypeFirst
          do i=1,5
            call FeQuestCrwOpen(nCrw,i.eq.HardCopy)
            nCrw=nCrw+1
          enddo
          EventType=EventEdw
          EventNumber=nEdwFileName
        endif
      else
        HardCopy=0
        call FeQuestEdwClose(nEdwFileName)
        call FeQuestButtonClose(nButtBrowse)
        nCrw=nCrwTypeFirst
        do i=1,5
          call FeQuestCrwClose(nCrw)
          nCrw=nCrw+1
        enddo
        call FeQuestLblOff(nLbl)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRecord) then
        go to 1400
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        call FeFileManager('Select file to derive the radix',Veta,'*.*',
     1                     0,.true.,ich)
        EventType=EventEdw
        EventNumber=nEdwFileName
        if(ich.eq.0.and.Veta.ne.' ') then
          i=idel(Veta)
1520      if(Veta(i:i).ne.'.') then
            i=i-1
            if(i.gt.1) then
              go to 1520
            else
              go to 1540
            endif
          endif
          Veta(i:)=' '
          i=i-1
1530      if(index(Cifry(:10),Veta(i:i)).gt.0) then
            i=i-1
            if(i.gt.1) then
              go to 1530
            else
              go to 1540
            endif
          endif
          if(Veta(i:i).eq.'_') Veta(i:)=' '
1540      call FeQuestStringEdwOpen(nEdwFileName,Veta)
        endif
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwDelayTime,DelayTimeForMovie)
        call FeQuestIntFromEdw(nEdwRepeat,MovieRepeat)
        i=1
        do nEdw=nEdwLimFr,nEdwLimTo
          k=(i-1)/2+1
          j=ior(k)
          call FeQuestRealFromEdw(nEdw,pom)
          if(gx(3,j).ne.0.) then
            n=nint((pom-gx(1,j))/gx(3,j))+1
          else
            n=1
          endif
          if(mod(i,2).eq.1) then
            nxMovieFr(k)=n
          else
            nxMovieTo(k)=n
          endif
          i=i+1
        enddo
        if(CrwLogicQuest(nCrwRecord)) then
          MovieFileName=EdwStringQuest(nEdwFileName)
          if(MovieFileName.eq.' ') then
            call FeChybne(-1.,-1.,'the file prefix not defined.',' ',
     1                    SeriousError)
            call FeQuestButtonOff(ButtonOk-ButtonFr+1)
            go to 1400
          endif
          nCrw=nCrwTypeFirst
          do i=1,5
            if(CrwLogicQuest(nCrw)) then
              HardCopy=i
              go to 5000
            endif
            nCrw=nCrw+1
          enddo
        else
          MovieFileName=' '
          HardCopy=0
        endif
      endif
5000  call FeQuestRemove(id)
      return
      end
      subroutine GrtSetParLimits(nEdwPLim,NPar)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      data NParSave/-1/
      nEdw=nEdwPLim
      if(NPar.eq.NParSave) then
        pom=EdwRealQuest(1,nEdw)
      else if(NPar.eq.IdDrawOcc) then
        pom=-.1
      else if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        pom=-.5
      else if(NPar.eq.IdDrawADP) then
        pom=-.05
      else if(NPar.eq.IdDrawTorsion) then
        pom=-180.
      else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
        pom=1.
      else if(NPar.eq.IdDrawVal) then
        pom=1.
      else if(NPar.eq.IdDrawAngles) then
        pom=90.
      else if(NPar.eq.IdDrawPlane.or.NPar.eq.IdDrawIndAng) then
        pom=-90.
      else if(NPar.eq.IdMagMoment) then
        pom=-3.
      endif
      call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
      gy(1)=pom
      nEdw=nEdw+1
      if(NPar.eq.NParSave) then
        pom=EdwRealQuest(1,nEdw)
      else if(NPar.eq.IdDrawOcc) then
        pom=1.1
      else if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        pom=.5
      else if(NPar.eq.IdDrawADP) then
        pom=.05
      else if(NPar.eq.IdDrawTorsion) then
        pom=180.
      else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
        pom=4.
      else if(NPar.eq.IdDrawVal) then
        pom=3.
      else if(NPar.eq.IdDrawAngles) then
        pom=180.
      else if(NPar.eq.IdDrawPlane.or.NPar.eq.IdDrawIndAng) then
        pom=90.
      else if(NPar.eq.IdMagMoment) then
        pom=3.
      endif
      call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
      gy(2)=pom
      NParSave=NPar
      return
      end
      function GrtSetRefLevel(ia,isw,NPar,IPar)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      if(NPar.eq.IdDrawOcc) then
        GrtSetRefLevel=0.
      else if(NPar.eq.IdDrawXYZ.or.NPar.eq.IdDrawDeltaXYZ) then
        GrtSetRefLevel=0.
      else if(NPar.eq.IdDrawADP) then
        if(IPar.le.3) then
          GrtSetRefLevel=beta(IPar,ia)/urcp(IPar,isw,KPhase)
        else
          call boueq(beta(1,ia),sbeta(1,ia),0,pom,spom,isw)
          GrtSetRefLevel=spom
        endif
      else if(NPar.eq.IdDrawDist.or.NPar.eq.IdDrawIndDist) then
        if(WhatToDraw.eq.0) then
          GrtSetRefLevel=0.
        else
          GrtSetRefLevel=2.
        endif
      else if(NPar.eq.IdDrawVal) then
        if(WhatToDraw.eq.0) then
          GrtSetRefLevel=0.
        else
          GrtSetRefLevel=2.
        endif
      else if(NPar.eq.IdDrawTorsion) then
        GrtSetRefLevel=0.
      else if(NPar.eq.IdDrawAngles) then
        GrtSetRefLevel=0.
      endif
      return
      end
      integer function RecModToRecMap(k,nx,ior,n)
      dimension ior(n),nx(n),nd(3)
      call RecUnpack(k,nd,nx,n)
      RecModToRecMap=0
      do i=n,1,-1
        j=ior(i)
        RecModToRecMap=RecModToRecMap*nx(j)+nd(j)-1
      enddo
      RecModToRecMap=RecModToRecMap+1
      return
      end
      subroutine GrtSetTr
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'contour.cmn'
      include 'grapht.cmn'
      c=1.
      alfa=90.
      bett=90.
      i=ior(1)
      j=ior(2)
      k=1
      NSubs=1
      call ConSetCellParCon
      a=sqrt(MetTens6(i+(i-1)*NDim(KPhase),NSubs,KPhase))
      b=sqrt(MetTens6(j+(j-1)*NDim(KPhase),NSubs,KPhase))
      c=sqrt(MetTens6(k+(k-1)*NDim(KPhase),NSubs,KPhase))
      pom=MetTens6(i+(j-1)*NDim(KPhase),NSubs,KPhase)/(a*b)
      gama=acos(pom)
      if(i.gt.3) then
        gaman=atan(a/ee(i-3)*tan(gama))
        a=ee(i-3)
        b=b*sin(gama)/sin(gaman)
        gama=gaman
      endif
      if(j.gt.3) then
        gaman=atan(b/ee(j-3)*tan(gama))
        b=ee(j-3)
        a=a*sin(gama)/sin(gaman)
        gama=gaman
      endif
      gama=gama/ToRad
      csa=cos(torad*alfa)
      csb=cos(torad*bett)
      csg=cos(torad*gama)
      sng=sin(torad*gama)
      volume=sqrt(1.-csa**2-csb**2-csg**2+2.*csa*csb*csg)
      F2O(1)=a*sng
      F2O(4)=0.
      F2O(2)=a*csg
      F2O(5)=b
      F2O(3)=0.
      F2O(6)=0.
      F2O(7)=c*csb
      F2O(8)=c*(csa-csb*csg)/sng
      F2O(9)=c*volume/sng
      call matinv(F2O,O2F,pom,3)
      do i=1,NDimI(KPhase)
        j=ior(i)
        xmin(i)=gx(1,j)
        xmax(i)=gx(2,j)
        dx(i)=gx(3,j)
      enddo
      call FeMakeAcWin(60.,20.,30.,30.)
      call FeSetTransXo2X(xmin(1),xmax(1),xmin(2),xmax(2),.true.)
      return
      end
      subroutine GrtDrawPerPartes(x,y,o,nn,LineTypeDraw,PlotMode,Color)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      dimension x(nn),y(nn),o(nn)
      integer Color,PlotMode
      n=0
      np=1
      do i=1,nn
        if(o(i).gt.OccLimit) then
          n=n+1
        else
          if(n.gt.0) then
            call FeXYPlot(x(np),y(np),n,LineTypeDraw,PlotMode,Color)
            if(HardCopy.eq.HardCopyNum.or.
     1         HardCopy.eq.HardCopyNum+100) write(85,'(''# skip'')')
            np=i
            n=0
          endif
          np=np+1
        endif
      enddo
      if(n.gt.0) call FeXYPlot(x(np),y(np),n,LineTypeDraw,PlotMode,
     1                         Color)
      return
      end
      subroutine GrtSetDistKeys(NPar,xpp)
      use Dist_mod
      use Grapht_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'dist.cmn'
      include 'grapht.cmn'
      dimension xpp(3)
      logical ExistFile
      call TrOrthoS(1)
      call DefaultDist
      call NactiDist
      call CloseIfOpened(55)
      if(ExistFile(fln(:ifln)//'_torpl.tmp'))
     1  call Deletefile(fln(:ifln)//'_torpl.tmp')
      call SetRealArrayTo(dt,9,0.)
      call SetRealArrayTo(tfirst,9,0.)
      call SetRealArrayTo(tlast,9,1.)
      call SetIntArrayTo(nt,9,1)
      call CopyVek(xpp,tfirst,NDimI(KPhase))
      call CopyVek(xpp,tlast,NDimI(KPhase))
      call CopyVekI(NxMod,nt,NDimI(KPhase))
      call CopyVek (DxMod,dt,NDimI(KPhase))
      if(NPar.eq.IdDrawDist) then
        dmdk=0.
        if(WhatToDraw.eq.0) then
          dmhk=yomx
        else
          dmhk=DrawDMax
        endif
        iuhl=0
      else if(NPar.eq.IdDrawVal) then
        dmdk=0.
        dmhk=DrawDMax
        iuhl=0
      else if(NPar.eq.IdDrawAngles) then
        dmdk=0.
        dmhk=DrawDMax
        iuhl=1
      endif
      call SetRealArrayTo(dmh,NAtFormula(KPhase),dmhk)
      do i=1,NDimI(KPhase)
        do j=2,NComp(KPhase)
          nt(i,j)=nt(i,1)
          tfirst(i,j)=tfirst(i,1)
          tlast(i,j)=tlast(i,1)
          dt(i,j)=dt(i,1)
        enddo
      enddo
      do j=1,NComp(KPhase)
        ntall(j)=1
        do i=1,NDimI(KPhase)
          ntall(j)=ntall(j)*nt(i,j)
        enddo
      enddo
      ieach=1
      fullcoo=1
      VolaToDist=.false.
      call inm40(GrtESD)
      return
      end
      subroutine GrtLocator
      use Grapht_mod
      parameter (mxcurves=5)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      include 'contour.cmn'
      dimension xcur(:,:),ycur(:,:),n(mxcurves+1),xp(2)
      integer Color,ColorA(mxcurves+1),LineTypeDrawA(mxcurves+1)
      character*80 Veta,PrvniAtom,DruhyAtom
      character*8  SvFile
      logical Skip,JeUvnitr
      allocatable xcur,ycur
      allocate(xcur(DrawPoints,mxcurves+1),ycur(DrawPoints,mxcurves+1))
      if(OpSystem.ge.0) then
        SvFile='full.pcx'
      else
        SvFile='full.bmp'
      endif
      call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile)
      XPosO=FeX2Xo(XPos)
      YPosO=FeY2Yo(YPos)
      XPosO=max(xomn,XPosO)
      XPosO=min(xomx,XPosO)
      YPosO=max(yomn,YPosO)
      YPosO=min(yomx,YPosO)
      XPosP=FeXo2X(XPosO)
      YPosP=FeYo2Y(YPosO)
      JeUvnitr=abs(Xposp-Xpos).le..5.and.abs(Yposp-Ypos).le..5
      if(JeUvnitr) then
        i=1
        write(Cislo,100) XPosO,YPosO
        call FeWInfWrite(1,Cislo)
      else
        i=0
      endif
      call FeMouseShape(i)
      AllowChangeMouse=.false.
      TakeMouseMove=.true.
      CheckMouse=.true.
      NPar=DrawParam(1)/10
1000  call FeQuestEvent(GraphtQuest,ich)
      m=1
      if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeLeftDown) then
        if(NPar.eq.IdDrawPlane) go to 1000
        ln=NextLogicNumber()
        open(ln,file=fln(:ifln)//'.l66')
        PrvniAtom=' '
1100    read(ln,FormA,end=1500) Veta
        if(Veta(1:1).ne.'#') go to 1100
        TextInfo(m)=' '
1120    if(index(Veta,'# distance-t plot for').eq.1) then
          k=22
          call kus(Veta,k,PrvniAtom)
          call DeleteXYZFromAtomName(PrvniAtom)
          LabelType=IdDrawDist
        else if(index(Veta,'# angle-t plot for').eq.1) then
          k=19
          call kus(Veta,k,PrvniAtom)
          call DeleteXYZFromAtomName(PrvniAtom)
          LabelType=IdDrawAngles
        else if(index(Veta,'# to').eq.1.and.PrvniAtom.ne.' ') then
          Veta=Veta(6:)
          if(LabelType.eq.IdDrawDist) then
            call DeleteXYZFromAtomName(Veta)
            TextInfo(m)='Distance '//PrvniAtom(:idel(PrvniAtom))//
     1                  ' - '//Veta(:idel(Veta))
          else if(LabelType.eq.IdDrawAngles) then
            i=index(Veta,'and')
            if(i.le.0) i=idel(Veta)+1
            DruhyAtom=Veta(:i-2)
            call DeleteXYZFromAtomName(DruhyAtom)
            TextInfo(m)='Angle '//DruhyAtom(:idel(DruhyAtom))//' - '//
     1                  PrvniAtom(:idel(PrvniAtom))//' - '
            Veta=Veta(i+3:)
            call DeleteXYZFromAtomName(Veta)
            TextInfo(m)=TextInfo(m)(:idel(TextInfo(m)))//
     1                  Veta(:idel(Veta))
          endif
        else if(index(Veta,'# Color : ').eq.1) then
          k=10
          call StToReal(Veta,k,xp,2,.false.,ich)
          if(ich.eq.0) then
            Color=nint(xp(1))
            LineTypeDraw=nint(xp(2))
          endif
        else if(index(Veta,'plot for').gt.0) then
          TextInfo(m)=Veta(3:)
        endif
        read(ln,FormA,end=1500) Veta
        if(Veta(1:1).eq.'#') go to 1120
1130    n(m)=1
        skip=.false.
1140    k=0
        call StToReal(Veta,k,xp,2,.false.,ich)
        if(ich.ne.0) go to 1000
        xcur(n(m),m)=xp(1)
        ycur(n(m),m)=xp(2)
        read(ln,FormA,end=1500) Veta
        if(Veta(1:1).ne.'#') then
          n(m)=n(m)+1
          go to 1140
        else if(Veta.eq.'# skip') then
          skip=.true.
          go to 1200
        endif
1160    if(Veta.eq.'# end of curve') go to 1200
        read(ln,FormA,end=1500) Veta
        go to 1160
1200    if(n(m).le.1) go to 1245
        XPosO=FeXo2X(xcur(1,m))
        YPosO=FeYo2Y(ycur(1,m))
        do 1240i=1,n(m)
          XPosP=FeXo2X(xcur(i,m))
          YPosP=FeYo2Y(ycur(i,m))
          if(XPos.lt.XPosO.or.XPos.gt.XPosP) then
            XPosO=XPosP
            YPosO=YPosP
            go to 1240
          endif
          YPosP=(XPos-XPosO)/(XPosP-XPosO)*(YPosP-YPosO)+YPosO
          if(abs(YPos-YPosP).lt.3.) then
            call FeDeferOutput
            call FeXYPlot(xcur(1,m),ycur(1,m),n(m),LineTypeDraw,
     1                    NormalPlotMode,Black)
            call FeXYPlot(xcur(1,m),ycur(1,m),n(m),DottedLine,
     1                    NormalPlotMode,Color)
            ColorA(m)=Color
            LineTypeDrawA(m)=LineTypeDraw
            call FeReleaseOutput
            if(m.gt.mxcurves) then
              XPosP=XPos
              YPosP=YPos
              write(Veta,'(i5)') MxCurves
              call Zhusti(Veta)
              call FeChybne(-1.,-1.,'the maximum number of '//
     1                      Veta(:idel(Veta))//' curves reached.',
     2                      'The list will not be complete.',Warning)
              XPos=XposP
              YPos=YPosP
              go to 1500
            endif
            m=m+1
            go to 1100
          else
            go to 1245
          endif
1240    continue
1245    if(Skip) then
1250      read(ln,FormA,end=1500) Veta
          if(Veta(1:1).ne.'#') then
            go to 1130
          else
            if(Veta.eq.'# end of curve') then
              Skip=.false.
              go to 1200
            else
              go to 1250
            endif
          endif
        else
          go to 1100
        endif
1500    close(ln)
        m=m-1
        if(m.gt.0) then
          NInfo=m
          call FeInfoWin(-1.,-1.)
1520      call FeQuestEvent(GraphtQuest,ich)
          if(CheckType.eq.EventMouse) then
            if(CheckNumber.eq.JeMove) then
              XPosO=FeX2Xo(XPos)
              YPosO=FeY2Yo(YPos)
              XPosO=max(xomn,XPosO)
              XPosO=min(xomx,XPosO)
              YPosO=max(yomn,YPosO)
              YPosO=min(yomx,YPosO)
              XPosP=FeXo2X(XPosO)
              YPosP=FeYo2Y(YPosO)
              JeUvnitr=abs(Xposp-Xpos).le..5.and.abs(Yposp-Ypos).le..5
              if(JeUvnitr) then
                i=1
              else
                i=0
              endif
              call FeMouseShape(i)
              go to 1520
            else if(CheckNumber.eq.JeLeftUp.or.
     1              CheckNumber.eq.JeRightUp) then
              go to 1520
            endif
          else if(CheckType.eq.EventKey) then
            if(CheckNumber.ne.JeEscape) go to 1520
          else if(CheckType.eq.EventButton) then
            go to 1540
          else
            go to 1520
          endif
1540      call FeKillInfoWin
          do j=1,m
            call FeXYPlot(xcur(1,j),ycur(1,j),n(j),DottedLine,
     1                    NormalPlotMode,Black)
          enddo
          do j=1,m
            call FeXYPlot(xcur(1,j),ycur(1,j),n(j),LineTypeDrawA(j),
     1                    NormalPlotMode,ColorA(j))
          enddo
        endif
        go to 1000
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeMove) then
        XPosO=FeX2Xo(XPos)
        YPosO=FeY2Yo(YPos)
        XPosO=max(xomn,XPosO)
        XPosO=min(xomx,XPosO)
        YPosO=max(yomn,YPosO)
        YPosO=min(yomx,YPosO)
        XPosP=FeXo2X(XPosO)
        YPosP=FeYo2Y(YPosO)
        if(abs(Xposp-Xpos).gt..5.or.abs(Yposp-Ypos).gt..5) then
          if(JeUvnitr) then
            call FeMouseShape(0)
            JeUvnitr=.false.
            call FeWinfClose(1)
          endif
        else
          if(.not.JeUvnitr) then
            call FeMouseShape(1)
            JeUvnitr=.true.
          endif
          write(Cislo,100) XPosO,YPosO
          call FeWInfWrite(1,Cislo)
        endif
      else if(CheckType.eq.EventMouse.and.CheckNumber.eq.JeRightDown)
     1  then
        go to 5000
      else if(CheckType.eq.EventButton) then
        go to 5000
      endif
      go to 1000
5000  TakeMouseMove=.false.
      call FeMouseShape(0)
      call FePlotMode('N')
      call FeDeferOutput
      call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SvFile,0)
      deallocate(xcur,ycur)
      return
100   format(f7.3,f8.3)
      end
      subroutine GrtReadOptions(ich)
      use Grapht_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      character*80 Veta
      logical CrwLogicQuest
      ich=0
      id=NextQuestId()
      xdq=200.
      xdqp=xdq*.5
      il=3
      call FeQuestCreate(id,-1.,-1.,xdq,il,' ',0,LightGray,0,0)
      il=1
      xpom=5.
      tpom=xpom+CrwXd+5.
      Veta='Indicate %commensurate t sections'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,DrawTCommen)
      nCrwTSections=CrwLastMade
      if(kcommen(KPhase).le.0) call FeQuestCrwDisable(CrwLastMade)
      il=il+1
      Veta='%Show crenel limits'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,GrtShowCrenel)
      nCrwShowCrenel=CrwLastMade
      il=il+1
      tpom=5.
      Veta='%Occupancy limit'
      dpom=60.
      xpom=xdq-dpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwOccLimit=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,OccLimit,.false.,.false.)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(kcommen(KPhase).gt.0)
     1    DrawTCommen=CrwLogicQuest(nCrwTSections)
        GrtShowCrenel=CrwLogicQuest(nCrwShowCrenel)
        call FeQuestRealFromEdw(nEdwOccLimit,OccLimit)
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine GrtDPlot
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'grapht.cmn'
      character*256 Veta
      character*80 t80
      real :: xp(2)
      real, allocatable :: xmn(:),xmx(:)
      integer, allocatable :: npt(:)
      logical EqIgCase
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.l66','formatted','old')
      n=0
1100  read(ln,FormA,end=1150) Veta
      if(EqIgCase(Veta,'# end of curve')) n=n+1
      go to 1100
1150  allocate(xmn(n),xmx(n),npt(n))
      rewind ln
      i=1
      npt(1)=0
1200  read(ln,FormA,end=1250) Veta
      if(EqIgCase(Veta,'# end of curve')) then
        i=i+1
        if(i.gt.n) go to 1250
        npt(i)=0
      endif
      if(Veta(1:1).eq.'#') go to 1200
      npt(i)=npt(i)+1
      k=0
      call StToReal(Veta,k,xp,2,.false.,ich)
      if(ich.eq.0) then
        if(npt(i).eq.1) xmn(i)=xp(1)
        xmx(i)=xp(1)
      endif
      go to 1200
1250  rewind ln
      lni=NextLogicNumber()
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'DPlot'//ObrLom//
     1      'Grapht_template.grf'
      else
        Veta=HomeDir(:idel(HomeDir))//'dplot/Grapht_template.grf'
      endif
      call OpenFile(lni,Veta,'formatted','old')
      lno=NextLogicNumber()
      call OpenFile(lno,fln(:ifln)//'_tmp.grf','formatted','unknown')
1260  read(lni,FormA) Veta
      if(.not.EqIgCase(Veta,'#$%place to insert data#$%')) then
        write(lno,FormA) Veta(:idel(Veta))
        go to 1260
      endif
      write(Cislo,FormI15) n
      call Zhusti(Cislo)
      write(lno,FormA) Cislo(:idel(Cislo))
      i=1
1300  write(Cislo,FormI15) npt(i)
      call Zhusti(Cislo)
      write(lno,FormA) Cislo(:idel(Cislo))
      m=0
      t80=' '
1320  read(ln,FormA,end=1250) Veta
      if(EqIgCase(Veta,'# end of curve')) then
        if(m.ne.0) write(lno,FormA) t80(:idel(t80))
        write(t80,100) xmn(i),xmx(i)
        call Zhusti(t80)
        write(lno,FormA) t80(:idel(t80))
        write(lno,'(i2,i5)') 1,0
        write(lno,FormA)
        write(lno,FormA)
        i=i+1
        if(i.gt.n) go to 2000
        go to 1300
      endif
      if(Veta(1:1).eq.'#') go to 1320
      k=0
      call StToReal(Veta,k,xp,2,.false.,ich)
      write(Cislo,'(f15.6)') xp(2)
      call ZdrcniCisla(Cislo,1)
      m=m+1
      if(t80.eq.' ') then
        t80=Cislo
      else
        t80=t80(:idel(t80))//','//Cislo(:idel(Cislo))
      endif
      if(m.ge.5) then
        write(lno,FormA) t80(:idel(t80))
        m=0
        t80=' '
      endif
      go to 1320
2000  read(lni,FormA,end=2100) Veta
      if(Veta(1:3).eq.'###') then
        t80=Veta(4:idel(Veta)-3)
        idl=idel(t80)
        Veta=t80
        do i=2,n
          j=idel(Veta)
          if(j+idl.gt.110) then
            write(lno,FormA) Veta(:idel(Veta))
            Veta=t80
          else
            Veta=Veta(:j)//','//t80(:idl)
          endif
        enddo
      endif
      if(EqIgCase(Veta,'#$%x,y-min#$%')) then
        write(Veta,100) xomn,yomn
        call Zhusti(Veta)
      else if(EqIgCase(Veta,'#$%x,y-max#$%')) then
        write(Veta,100) xomx,yomx
        call Zhusti(Veta)
      endif
      write(lno,FormA) Veta(:idel(Veta))
      go to 2000
2100  call CloseIfOpened(lni)
      call CloseIfOpened(lno)
      call CloseIfOpened(ln)
      Veta='"'//CallDPlot(:idel(CallDPlot))//'" '//
     1     fln(:ifln)//'_tmp.grf'
      call FeSystem(Veta)
      if(allocated(xmn)) deallocate(xmn,xmx,npt)
      return
100   format(f15.4,',',f15.4)
      end
