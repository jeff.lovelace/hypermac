      subroutine AllocateTrans
      use EditM40_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      allocate(TransMat(NDimQ(KPhase),NTrans,NComp(KPhase)),
     1         TransVec(NDim (KPhase),NTrans,NComp(KPhase)),
     2         TransX    ( 9,NTrans,NComp(KPhase)),
     3         TransM    ( 9,NTrans,NComp(KPhase)),
     4         TransTemp (36,NTrans,NComp(KPhase)),
     5         TransTempS(81,NTrans,NComp(KPhase)),
     6         SignTransX(NTrans),TransZM(NTrans),
     7         TransC3(100,NTrans,NComp(KPhase)),
     8         TransC4(225,NTrans,NComp(KPhase)),
     9         TransC5(441,NTrans,NComp(KPhase)),
     a         TransC6(784,NTrans,NComp(KPhase)))
      if(MaxUsedKw(KPhase).gt.0)
     1  allocate(TransKwSym(MaxUsedKw(KPhase),NTrans))
      end
      subroutine DeallocateTrans
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      deallocate(TransMat,TransVec,TransX,TransM,TransTemp,TransTempS,
     1           SignTransX,TransZM,TransC3,TransC4,TransC5,TransC6)
      if(allocated(TransKwSym)) deallocate(TransKwSym)
      end
      subroutine ZmTF12(i)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer PrvKi
      itf(i)=2
      pom=beta(1,i)
      spom=sbeta(1,i)
      PrvKi=5
      do j=1,6
        beta(j,i)=pom*prcp(j,iswa(i),KPhase)
        sbeta(j,i)=spom*prcp(j,iswa(i),KPhase)
        KiA(PrvKi,i)=1
        PrvKi=PrvKi+1
      enddo
      return
      end
      subroutine ZmTF21(i)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension KModAP(7)
      itf(i)=1
      call boueq(beta(1,i),sbeta(1,i),1,pom,spom,iswa(i))
      beta(1,i)=pom
      sbeta(1,i)=spom
      call SetRealArrayTo( beta(2,i),5,0.)
      call SetRealArrayTo(sbeta(2,i),5,0.)
      call SetIntArrayTo(KiA(6,i),5,0)
      call SetIntArrayTo(KModAP,7,0)
      KModAP(1)=KModA(1,i)
      KModAP(2)=KModA(2,i)
      call ShiftKiAt(i,itf(i),ifr(i),lasmax(i),KModAP,MagPar(i),
     1               .false.)
      do j=3,7
        KModA(j,i)=0
      enddo
      return
      end
      subroutine ZmTF20(ia)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension KModAP(7)
      l=(kmol(ia)-1)/mxp+1
      call ReallocateMoleculeParams(NMolec,mxp,1,KModMMax)
      if(KTLS(l).le.0) then
        do k=kmol(ia),kmol(ia)+mam(l)-1
          call ShiftKiMol(k,1,KModM(1,k),KModM(2,k),KModM(3,k),
     1                    .false.)
          if(itf(ia).eq.1) call ZmTF12(ia)
          call CopyVek(beta(1,ia),tt(1,k),6)
          call SetRealArrayTo(tt(4,k),3,0.)
          call SetRealArrayTo(stt(1,k),6,0.)
          call SetRealArrayTo(tl(1,k),6,0.)
          call SetRealArrayTo(stl(1,k),6,0.)
          call SetRealArrayTo(ts(1,k),9,0.)
          call SetRealArrayTo(sts(1,k),9,0.)
          call SetIntArrayTo(KiMol(8,k),21,1)
        enddo
        KTLS(l)=1
      else
        KTLS(l)=KTLS(l)+1
      endif
      call SetRealArrayTo(beta(1,ia),6,0.)
      call SetRealArrayTo(sbeta(1,ia),6,0.)
      call SetIntArrayTo(KiA(5,ia),6,0)
      call SetIntArrayTo(KModAP,7,0)
      KModAP(1)=KModA(1,ia)
      KModAP(2)=KModA(2,ia)
      call ShiftKiAt(ia,itf(ia),ifr(ia),lasmax(ia),KModAP,MagPar(ia),
     1               .false.)
      itf(ia)=0
      KModA(3,ia)=0
      return
      end
      subroutine ZmTF02(i)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      k=kmol(i)
      l=(k-1)/mxp+1
      itf(i)=2
      do j=1,6
        beta(j,i)=beta(j,i)+tt(j,k)
        sbeta(j,i)=0.
      enddo
      n=NAtMolFr(1,1)-1
      call cultm(tztl(1,i-n),tl(1,k),beta(1,i),6,6,1)
      call cultm(tzts(1,i-n),ts(1,k),beta(1,i),6,9,1)
      if(ktls(l).le.1) then
        do k=kmol(i),kmol(i)+mam(l)-1
          call ShiftKiMol(k,0,KModM(1,k),KModM(2,k),
     1                    KModM(3,k),.false.)
        enddo
      endif
      ktls(l)=ktls(l)-1
      call SetIntArrayTo(KiA(5,i),6,1)
      return
      end
      subroutine ZmAnhi(i,itfn)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do k=itfn+2,itf(i)+1
        KFA(k,i)=0
        KModA(k,i)=0
      enddo
      call ReallocateAtomParams(NAtAll,itfn,IFrMax,KModAMax,MagParMax)
      call ShiftKiAt(i,itfn,ifr(i),lasmax(i),KModA(1,i),MagPar(i),
     1               .false.)
      m=TRankCumul(itf(i))
      do k=itf(i)+1,itfn
        if(k.eq.2) then
          pom=beta(1,i)
          spom=sbeta(1,i)
        endif
        nrank=TRank(k)
        do j=1,nrank
          if(k.eq.2) then
            beta(j,i)=pom*prcp(j,iswa(i),KPhase)
            sbeta(j,i)=spom*prcp(j,iswa(i),KPhase)
          else if(k.eq.3) then
            c3(j,i)=0.
            sc3(j,i)=0.
          else if(k.eq.4) then
            c4(j,i)=0.
            sc4(j,i)=0.
          else if(k.eq.5) then
            c5(j,i)=0.
            sc5(j,i)=0.
          else
            c6(j,i)=0.
            sc6(j,i)=0.
          endif
          m=m+1
          KiA(m,i)=1
        enddo
      enddo
      if(itf(i).gt.1.and.itfn.eq.1) call ZmTF21(i)
      itf(i)=itfn
      itfmax=max(itfmax,itfn)
      return
      end
      subroutine ZmMag(ia,MagParA)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer PrvKiMag
      call ReallocateAtomParams(NAtAll,itfmax,IFrMax,KModAMax,MagParA)
      call ShiftKiAt(ia,itf(ia),ifr(ia),lasmax(ia),KModA(1,ia),MagParA,
     1               .false.)
      if(MagParA.gt.MagPar(ia)) then
        kip=PrvKiMag(ia)
        if(MagPar(ia).le.0) then
          call SetRealArrayTo( sm0(1,ia),3,.1)
          call SetRealArrayTo(ssm0(1,ia),3,.0)
          call SetIntArrayTo(KiA(kip,ia),3,1)
        endif
        i=max(MagPar(ia),1)
        n=(MagParA-i)*3
        kip=kip+3+(i-1)*6
        if(n.gt.0) then
          call SetRealArrayTo( smx(1,i,ia),n,.1)
          call SetRealArrayTo(ssmx(1,i,ia),n,.0)
          call SetRealArrayTo( smy(1,i,ia),n,.1)
          call SetRealArrayTo(ssmy(1,i,ia),n,.0)
          call SetIntArrayTo(KiA(kip,ia),2*n,1)
        endif
      endif
      MagPar(ia)=MagParA
      return
      end
      subroutine AtModi(ia,kmodap)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension kmodanp(7),kmn(7),kmodap(7),kiold(:)
      real :: xp(1)=(/0./)
      integer PrvKi,PrvKiOld
      allocatable kiold
      i1=ia
      i2=ia
      AtBrat(ia)=.true.
      itfp=itf(ia)+1
      call CopyVekI(kmodap,kmodanp,7)
      nalloc=0
      do i=i1,i2
        if((i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)).or..not.AtBrat(i)) cycle
        k=max(TRankCumul(itf(i)),10)
        PrvKi=k+1
        PrvKiOld=1
        nkiold=DelkaKiAtomu(i)-k
        if(nkiold.gt.nalloc) then
          if(allocated(kiold)) deallocate(kiold)
          allocate(kiold(nkiold))
          nalloc=nkiold
        endif
        if(nkiold.gt.0) call CopyVekI(KiA(PrvKi,i),kiold,nkiold)
        do j=1,7
          if(kmodanp(j).ge.0) then
            kmn(j)=kmodanp(j)
          else
            kmn(j)=KModA(j,i)
          endif
        enddo
        call ReallocateAtomParams(NAtAll,itfmax,IFrMax,kmn,MagParMax)
        call ShiftKiAt(i,itf(i),ifr(i),lasmax(i),kmn,MagPar(i),.false.)
        do j=1,itfp
          if(KModA(j,i).gt.0) then
            KiA(DelkaKiAtomu(i),i)=kiold(nkiold)
            go to 1450
          endif
        enddo
        do j=1,itfp
          if(kmn(j).gt.0) then
            phf(i)=0.
            sphf(i)=0.
            KiA(DelkaKiAtomu(i),i)=0
            go to 1450
          endif
        enddo
1450    km=kmol(i)
        do j=1,3
          if(km.gt.0) then
            kmn(j)=KModM(j,km)
          else
            kmn(j)=0
          endif
        enddo
        if(kmodanp(1).gt.0) then
          if(KModA(1,i).le.0) then
            call SpecPos(x(1,i),xp,0,iswa(i),.05,n)
            pom=1./float(n)
            a0(i)=ai(i)/pom
            ai(i)=pom
            sa0(i)=0.
            KiA(PrvKi,i)=0
            KiA(1,i)=0
          endif
        else if(kmodanp(1).eq.0) then
          if(KModA(1,i).gt.0) then
            ai(i)=ai(i)*a0(i)
            KiA(1,i)=0
          endif
        endif
        do 2000n=1,itfp
          j1=max(KModA(n,i),0)
          j2=kmodanp(n)
          nrank=TRank(n-1)
          nrank2=2*nrank
          if(j2.lt.0) then
            j2=j1
          else if(j2.eq.0) then
            go to 2000
          endif
          if(j1.eq.j2) go to 1800
          if(KFA(n,i).ne.0.and.j1.ge.1) then
            if(nkiold.gt.0) then
              l1=PrvKiOld+nrank2*(j1-1)
              if(n.eq.1) l1=l1+1
              k1=PrvKi+nrank2*(j2-1)
              if(n.eq.1) k1=k1+1
              call CopyVekI(kiold(l1),KiA(k1,i),nrank2)
            endif
            if(n.eq.1) then
              ax(j2,i)=ax(j1,i)
              ay(j2,i)=ay(j1,i)
              sax(j2,i)=sax(j1,i)
              say(j2,i)=say(j1,i)
            else if(n.eq.2) then
              call CopyVek(ux(1,j1,i),ux(1,j2,i),nrank)
              call CopyVek(uy(1,j1,i),uy(1,j2,i),nrank)
              call CopyVek(sux(1,j1,i),sux(1,j2,i),nrank)
              call CopyVek(suy(1,j1,i),suy(1,j2,i),nrank)
            else if(n.eq.3) then
              call CopyVek(bx(1,j1,i),bx(1,j2,i),nrank)
              call CopyVek(by(1,j1,i),by(1,j2,i),nrank)
              call CopyVek(sbx(1,j1,i),sbx(1,j2,i),nrank)
              call CopyVek(sby(1,j1,i),sby(1,j2,i),nrank)
            else if(n.eq.4) then
              call CopyVek(c3x(1,j1,i),c3x(1,j2,i),nrank)
              call CopyVek(c3y(1,j1,i),c3y(1,j2,i),nrank)
              call CopyVek(sc3x(1,j1,i),sc3x(1,j2,i),nrank)
              call CopyVek(sc3y(1,j1,i),sc3y(1,j2,i),nrank)
            else if(n.eq.5) then
              call CopyVek(c4x(1,j1,i),c4x(1,j2,i),nrank)
              call CopyVek(c4y(1,j1,i),c4y(1,j2,i),nrank)
              call CopyVek(sc4x(1,j1,i),sc4x(1,j2,i),nrank)
              call CopyVek(sc4y(1,j1,i),sc4y(1,j2,i),nrank)
            else if(n.eq.6) then
              call CopyVek(c5x(1,j1,i),c5x(1,j2,i),nrank)
              call CopyVek(c5y(1,j1,i),c5y(1,j2,i),nrank)
              call CopyVek(sc5x(1,j1,i),sc5x(1,j2,i),nrank)
              call CopyVek(sc5y(1,j1,i),sc5y(1,j2,i),nrank)
            else
              call CopyVek(c6x(1,j1,i),c6x(1,j2,i),nrank)
              call CopyVek(c6y(1,j1,i),c6y(1,j2,i),nrank)
              call CopyVek(sc6x(1,j1,i),sc6x(1,j2,i),nrank)
              call CopyVek(sc6y(1,j1,i),sc6y(1,j2,i),nrank)
            endif
            j1=j1-1
            j2=j2-1
          endif
          l1=PrvKi+nrank2*j1
          if(n.eq.1) l1=l1+1
          l2=l1+nrank
          do j=j1+1,j2
            call SetIntArrayTo(KiA(l1,i),nrank,1)
            call SetIntArrayTo(KiA(l2,i),nrank,1)
            if(j.gt.kmn(n)) then
              if(n.eq.1) then
                pom=.01
              else if(n.eq.2) then
                pom=.001
              else if(n.eq.3) then
                pom=.0001
              else
                pom=.00001
              endif
            else
              pom=0.
            endif
            if(n.eq.1) then
              ax(j,i)=pom
              ay(j,i)=pom
              sax(j,i)=.0
              say(j,i)=.0
            else if(n.eq.2) then
              call SetRealArrayTo( ux(1,j,i),nrank,pom)
              call SetRealArrayTo( uy(1,j,i),nrank,pom)
              call SetRealArrayTo(sux(1,j,i),nrank,0.)
              call SetRealArrayTo(suy(1,j,i),nrank,0.)
            else if(n.eq.3) then
              call SetRealArrayTo( bx(1,j,i),nrank,pom)
              call SetRealArrayTo( by(1,j,i),nrank,pom)
              call SetRealArrayTo(sbx(1,j,i),nrank,0.)
              call SetRealArrayTo(sby(1,j,i),nrank,0.)
            else if(n.eq.4) then
              call SetRealArrayTo( c3x(1,j,i),nrank,pom)
              call SetRealArrayTo( c3y(1,j,i),nrank,pom)
              call SetRealArrayTo(sc3x(1,j,i),nrank,0.)
              call SetRealArrayTo(sc3y(1,j,i),nrank,0.)
            else if(n.eq.5) then
              call SetRealArrayTo( c4x(1,j,i),nrank,pom)
              call SetRealArrayTo( c4y(1,j,i),nrank,pom)
              call SetRealArrayTo(sc4x(1,j,i),nrank,0.)
              call SetRealArrayTo(sc4y(1,j,i),nrank,0.)
            else if(n.eq.6) then
              call SetRealArrayTo( c5x(1,j,i),nrank,pom)
              call SetRealArrayTo( c5y(1,j,i),nrank,pom)
              call SetRealArrayTo(sc5x(1,j,i),nrank,0.)
              call SetRealArrayTo(sc5y(1,j,i),nrank,0.)
            else
              call SetRealArrayTo( c6x(1,j,i),nrank,pom)
              call SetRealArrayTo( c6y(1,j,i),nrank,pom)
              call SetRealArrayTo(sc6x(1,j,i),nrank,0.)
              call SetRealArrayTo(sc6y(1,j,i),nrank,0.)
            endif
            l1=l1+nrank2
            l2=l2+nrank2
          enddo
          if(KFA(n,i).ne.0.and.n.ne.1) j2=j2+1
1800      PrvKi   =PrvKi   +nrank2*j2
          PrvKiOld=PrvKiOld+nrank2*j2
          if(n.eq.1.and.j2.gt.0) then
            PrvKi   =PrvKi   +1
            PrvKiOld=PrvKiOld+1
          endif
2000    continue
        do j=1,7
          if(kmodanp(j).ge.0) KModA(j,i)=kmodanp(j)
        enddo
      enddo
9999  return
      end
      subroutine MolModi(jii,imi,KModNew)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension kmodanp(3),kmn(3),KModNew(*),kiold(:),xp(3)
      integer PrvKi,PrvKiOld,PrvKiMod
      logical BratMol(mxpm)
      allocatable kiold
      im=imi
      ji=jii
      isw=iswmol(im)
      i1=1
      i2=1
      BratMol(1)=.true.
      call CopyVekI(KModNew,kmodanp,3)
      call ReallocateMoleculeParams(NMolec,mxp,KTLS(im),KModNew)
1200  nalloc=0
      do i=i1,i2
        PrvKi=1
        if(ktls(im).gt.0) then
          PrvKiMod=PrvKi+28
          k=28
        else
          PrvKiMod=PrvKi+7
          k=7
        endif
        PrvKiOld=1
        n=DelkaKiMolekuly(ji)-k
        if(n.gt.nalloc) then
          if(allocated(kiold)) deallocate(kiold)
          allocate(kiold(n))
          nalloc=n
        endif
        if(n.gt.0) call CopyVekI(KiMol(PrvKiMod,ji),kiold,n)
        do j=1,3
          if(kmodanp(j).le.mxw) then
            kmn(j)=kmodanp(j)
          else
            kmn(j)=KModM(j,ji)
          endif
        enddo
        call ShiftKiMol(ji,ktls(im),kmn(1),kmn(2),kmn(3),.false.)
        do j=1,3
          if(KModM(j,ji).gt.0) then
            KiMol(DelkaKiMolekuly(ji),ji)=kiold(n)
            go to 1450
          endif
        enddo
        do j=1,3
          if(kmn(j).gt.0) then
            phfm(ji)=0.
            sphfm(ji)=0.
            KiMol(DelkaKiMolekuly(ji),ji)=0
            go to 1450
          endif
        enddo
1450    j1=KModM(1,ji)
        j2=kmn(1)
        if(j2.gt.0) then
          if(j1.eq.0) then
            call SpecPos(xm(1,i),xp,0,isw,.05,n1)
            n2=9999
            do ia=NAtPosFrAll(KPhase),NAtPosToAll(KPhase)
              if(kswa(ia).ne.KPhase.or.iswa(ia).ne.isw.or.
     1           kmol(ia).ne.ji) cycle
              call SpecPos(x(1,ia),xp,0,isw,.05,n2)
              exit
            enddo
            pom=1./float(min(n1,n2))
            a0m(ji)=aimol(ji)/pom
            aimol(ji)=pom
            sa0m(ji)=0.
            KiMol(PrvKiMod+1,ji)=0
            KiMol(PrvKi,ji)=0
          endif
          l=PrvKiMod+2+2*j1
          do j=j1+1,j2
            KiMol(l  ,ji)=1
            KiMol(l+1,ji)=1
            axm(j,ji)=.01
            aym(j,ji)=.01
            saxm(j,ji)=.0
            saym(j,ji)=.0
            l=l+2
          enddo
        endif
        if(kmn(2).ne.KModM(2,ji)) then
          j1=KModM(2,ji)
          j2=kmn(2)
          l=PrvKiMod
          lold=PrvKiOld
          if(kmn(1).gt.0) l=l+1+2*kmn(1)
          if(KModM(1,ji).gt.0) lold=lold+1+2*KModM(1,ji)
          if(KFM(2,ji).ne.0.and.j2.gt.0.and.j1.gt.0) then
            l1=lold+6*(j1-1)+1
            l2=l+6*(j2-1)+1
            l3=l1+6*j1
            l4=l2+6*j2
            call CopyVekI(kiOld(l1),KiMol(l2,ji),6)
            call CopyVekI(kiOld(l3),KiMol(l4,ji),6)
            call CopyVek( utx(1,j1,ji), utx(1,j2,ji),3)
            call CopyVek( uty(1,j1,ji), uty(1,j2,ji),3)
            call CopyVek(sutx(1,j1,ji),sutx(1,j2,ji),3)
            call CopyVek(suty(1,j1,ji),suty(1,j2,ji),3)
            call CopyVek( urx(1,j1,ji), urx(1,j2,ji),3)
            call CopyVek( ury(1,j1,ji), ury(1,j2,ji),3)
            call CopyVek(surx(1,j1,ji),surx(1,j2,ji),3)
            call CopyVek(sury(1,j1,ji),sury(1,j2,ji),3)
            j2=j2-1
            j1=j1-1
          endif
          if(kmn(2).gt.KModM(2,ji)) then
            n=3*(j2-j1)
            call SetRealArrayTo( utx(1,j1+1,ji),n,.0001)
            call SetRealArrayTo( uty(1,j1+1,ji),n,.0001)
            call SetRealArrayTo(sutx(1,j1+1,ji),n,.0)
            call SetRealArrayTo(suty(1,j1+1,ji),n,.0)
            call SetRealArrayTo( urx(1,j1+1,ji),n,.0001)
            call SetRealArrayTo( ury(1,j1+1,ji),n,.0001)
            call SetRealArrayTo(surx(1,j1+1,ji),n,.0)
            call SetRealArrayTo(sury(1,j1+1,ji),n,.0)
            n=2*n
            l1=l+6*j1+1
            l3=l1+6*kmn(2)
            call SetIntArrayTo(KiMol(l1,ji),n,1)
            call SetIntArrayTo(KiMol(l3,ji),n,1)
          endif
        endif
2301    if(kmn(3).gt.KModM(3,ji)) then
          j1=KModM(3,ji)
          j2=kmn(3)
          l=PrvKiMod
          if(kmn(1).gt.0) l=l+1+2*kmn(1)
          l=l+12*kmn(2)
          if(KFM(3,ji).ne.0) then
            l1=l+12*(j1-1)
            l2=l+12*(j2-1)
            l3=l1+12*j1
            l4=l2+12*j2
            do m=1,6
              KiMol(l2+m  ,ji)=KiMol(l1+m  ,ji)
              KiMol(l2+m+3,ji)=KiMol(l1+m+3,ji)
              KiMol(l4+m  ,ji)=KiMol(l3+m  ,ji)
              KiMol(l4+m+3,ji)=KiMol(l3+m+3,ji)
              ttx(m,j2,ji)=ttx(m,j1,ji)
              tty(m,j2,ji)=tty(m,j1,ji)
              sttx(m,j2,ji)=sttx(m,j1,ji)
              stty(m,j2,ji)=stty(m,j1,ji)
              tlx(m,j2,ji)=tlx(m,j1,ji)
              tly(m,j2,ji)=tly(m,j1,ji)
              stlx(m,j2,ji)=stlx(m,j1,ji)
              stly(m,j2,ji)=stly(m,j1,ji)
            enddo
            l1=l+24*j1+18*(j1-1)
            l2=l+24*j2+18*(j2-1)
            do m=1,9
              KiMol(l2+m  ,ji)=KiMol(l1+m  ,ji)
              KiMol(l2+m+3,ji)=KiMol(l1+m+3,ji)
              tsx(m,j2,ji)=tsx(m,j1,ji)
              tsy(m,j2,ji)=tsy(m,j1,ji)
              stsx(m,j2,ji)=stsx(m,j1,ji)
              stsy(m,j2,ji)=stsy(m,j1,ji)
            enddo
            j1=j1-1
            j2=j2-1
          endif
          l1=l+12*j1
          l2=l1+12*j2
          do j=j1+1,j2
            do m=1,6
              KiMol(l1+m  ,ji)=1
              KiMol(l1+m+6,ji)=1
              KiMol(l2+m  ,ji)=1
              KiMol(l2+m+6,ji)=1
              ttx(m,j,ji)=.00001
              tty(m,j,ji)=.00001
              sttx(m,j,ji)=.0
              stty(m,j,ji)=.0
              tlx(m,j,ji)=.00001
              tly(m,j,ji)=.00001
              stlx(m,j,ji)=.0
              stly(m,j,ji)=.0
            enddo
            l1=l1+12
            l2=l2+12
          enddo
          l1=l+24*kmn(3)+18*j1
          do j=j1+1,j2
            do m=1,9
              KiMol(l1+m  ,ji)=1
              KiMol(l1+m+9,ji)=1
              tsx(m,j,ji)=.00001
              tsy(m,j,ji)=.00001
              stsx(m,j,ji)=.0
              stsy(m,j,ji)=.0
            enddo
            l1=l1+18
          enddo
        endif
        do j=1,3
          KModM(j,ji)=kmn(j)
        enddo
      enddo
9999  return
      end
      subroutine EM40GetAngles(rot,irot,euler)
      include 'fepc.cmn'
      dimension rot(9),euler(3),rotp(9)
      call matinv(rot,rotp,pom,3)
      if(pom.ge.0.) then
        zn=1.
      else
        zn=-1.
      endif
      if(irot.eq.0) then
        if(abs(zn*rot(9)).gt..99995) then
          euler(2)=90.-sign(90.,zn*rot(9))
          euler(3)=0.
          ps=zn*rot(2)
          pc=zn*rot(1)
        else
          euler(2)=acos(zn*rot(9))/torad
          if(abs(rot(3)).le..0001.and.abs(rot(6)).le..0001) then
            euler(3)=0.
          else
            euler(3)=atan2(zn*rot(3),zn*rot(6))/torad
          endif
          ps= zn*rot(7)
          pc=-zn*rot(8)
        endif
      else
        if(abs(zn*rot(3)).gt..99995) then
          euler(2)=-sign(90.,zn*rot(3))
          euler(3)=0.
          ps=-zn*rot(4)
          pc= zn*rot(5)
        else
          euler(2)=-asin(zn*rot(3))/torad
          if(abs(rot(6)).le..0001.and.abs(rot(9)).le..0001) then
            euler(3)=0.
          else
            euler(3)=atan2(zn*rot(6),zn*rot(9))/torad
          endif
          ps=zn*rot(2)
          pc=zn*rot(1)
        endif
      endif
      euler(1)=atan2(ps,pc)/torad
      return
      end
      subroutine DelMol
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*11 jmeno
      i=1
1000  if(i.gt.NMolec) go to 9999
      if(iam(i)/KPoint(i).le.0) then
        isw=iswmol(i)
        KPh=kswmol(i)
        do j=1,mam(i)
          jmeno=MolName(i)
          write(jmeno(9:11),'(''#'',i2)') j
          call zhusti(jmeno)
        enddo
        call molsun(i+1,NMolec,i)
        do j=KPh,KPhase
          if(j.eq.KPh) then
            kp=isw
          else
            kp=1
          endif
          do k=kp,NComp(j)
            if(j.ne.KPh.or.k.ne.kp) NMolecFr(k,j)=NMolecFr(k,j)-1
            NMolecTo(k,j)=NMolecTo(k,j)-1
          enddo
          if(j.ne.KPh) NMolecFrAll(j)=NMolecFrAll(j)-1
          NMolecToAll(j)=NMolecToAll(j)-1
        enddo
        NMolecLen(isw,KPh)=NMolecLen(isw,KPh)-1
        NMolec=NMolec-1
      else
        i=i+1
      endif
      go to 1000
9999  call EM40UpdateMolecLimits
      return
      end
      subroutine MolSun(iod,ido,jod)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(jod.le.iod) then
        i1=iod
        i2=ido
        id=1
      else
        i1=ido
        i2=iod
        id=-1
      endif
      do i=i1,i2,id
        k=i+jod-iod
        molname(k)=molname(i)
        iswmol(k)=iswmol(i)
        kswmol(k)=kswmol(i)
        StRefPoint(k)=StRefPoint(i)
        iam(k)=iam(i)
        mam(k)=mam(i)
        npoint(k)=npoint(i)
        kpoint(k)=kpoint(i)
        SmbPGMol(k)=SmbPGMol(i)
        do j=1,npoint(i)
          call CopyVek(rpoint(1,j,i),rpoint(1,j,k),9)
          call CopyVek(tpoint(1,j,i),rpoint(1,j,k),36)
          ipoint(j,k)=ipoint(j,i)
        enddo
        call CopyVek(xm(1,i),xm(1,k),3)
        jk=(k-1)*mxp
        ji=(i-1)*mxp
        do j=1,mam(k)
          ji=ji+1
          jk=jk+1
          aimol(jk)=aimol(ji)
          saimol(jk)=saimol(ji)
          TypeModFunMol(jk)=TypeModFunMol(ji)
          call CopyVekI(KFM(1,ji),KFM(1,jk),3)
          call CopyVekI(KModM(1,ji),KModM(1,jk),3)
          call CopyVekI(KiMol(1,ji),KiMol(1,jk),DelkaKiMolekuly(ji))
          RotSign(jk)=RotSign(ji)
          AtTrans(jk)=AtTrans(ji)
          call CopyVek( trans(1,ji), trans(1,jk),3)
          call CopyVek(strans(1,ji),strans(1,jk),3)
          call CopyVek( euler(1,ji), euler(1,jk),3)
          call CopyVek(seuler(1,ji),seuler(1,jk),3)
          LocMolSystType(jk)=LocMolSystType(ji)
          do k=1,LocMolSystType(ji)
            LocMolSystAx(k,jk)=LocMolSystAx(k,ji)
            do l=1,2
              LocMolSystSt(l,k,jk)=LocMolSystSt(l,k,ji)
              call CopyVek(LocMolSystX(1,l,k,ji),LocMolSystX(1,l,k,jk),
     1                     3)
            enddo
          enddo
          if(ktls(i).ne.0) then
            call CopyVek( tt(1,ji), tt(1,jk),6)
            call CopyVek(stt(1,ji),stt(1,jk),6)
            call CopyVek( tl(1,ji), tl(1,jk),6)
            call CopyVek(stl(1,ji),stl(1,jk),6)
            call CopyVek( ts(1,ji), ts(1,jk),9)
            call CopyVek(sts(1,ji),sts(1,jk),9)
          endif
          if(KModM(1,jk).gt.0) then
            a0m(jk)=a0m(ji)
            sa0m(jk)=sa0m(ji)
            k=KModM(1,jk)
            call CopyVek( axm(1,ji), axm(1,jk),k)
            call CopyVek(saxm(1,ji),saxm(1,jk),k)
            call CopyVek( aym(1,ji), aym(1,jk),k)
            call CopyVek(saym(1,ji),saym(1,jk),k)
          endif
          k=3*KModM(2,jk)
          if(k.gt.0) then
            call CopyVek( utx(1,1,ji), utx(1,1,jk),k)
            call CopyVek(sutx(1,1,ji),sutx(1,1,jk),k)
            call CopyVek( uty(1,1,ji), uty(1,1,jk),k)
            call CopyVek(suty(1,1,ji),suty(1,1,jk),k)
            call CopyVek( urx(1,1,ji), urx(1,1,jk),k)
            call CopyVek(surx(1,1,ji),surx(1,1,jk),k)
            call CopyVek( ury(1,1,ji), ury(1,1,jk),k)
            call CopyVek(sury(1,1,ji),sury(1,1,jk),k)
          endif
          k=6*KModM(3,jk)
          if(k.gt.0) then
            call CopyVek( ttx(1,1,ji), ttx(1,1,jk),k)
            call CopyVek(sttx(1,1,ji),sttx(1,1,jk),k)
            call CopyVek( tty(1,1,ji), tty(1,1,jk),k)
            call CopyVek(stty(1,1,ji),stty(1,1,jk),k)
            call CopyVek( tlx(1,1,ji), tlx(1,1,jk),k)
            call CopyVek(stlx(1,1,ji),stlx(1,1,jk),k)
            call CopyVek( tly(1,1,ji), tly(1,1,jk),k)
            call CopyVek(stly(1,1,ji),stly(1,1,jk),k)
            k=9*KModM(3,jk)
            call CopyVek( tsx(1,1,ji), tsx(1,1,jk),k)
            call CopyVek(stsx(1,1,ji),stsx(1,1,jk),k)
            call CopyVek( tsy(1,1,ji), tsy(1,1,jk),k)
            call CopyVek(stsy(1,1,ji),stsy(1,1,jk),k)
          endif
          if(KModM(1,jk).gt.0.or.KModM(2,jk).gt.0.or.
     1       KModM(3,jk).gt.0) then
            phfm(jk)=phfm(ji)
            sphfm(jk)=sphfm(ji)
          endif
          PrvniKiMolekuly(jk)=PrvniKiMolekuly(ji)
          DelkaKiMolekuly(jk)=DelkaKiMolekuly(ji)
        enddo
      enddo
      return
      end
      subroutine EM40GetXFromAtName(AtName,ia,XAt,UxAt,UyAt,x40,RmiiAt,
     1                              ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension XAt(3),dsym(6),xp(6),RmAt(9),Rm6At(36),RMiiAt(9),ic(3),
     1          xpp(6),x40(3),UxAt(3,*),UyAt(3,*)
      character*(*) AtName
      if(NDimI(KPhase).gt.0) then
        call AtomSymCode(AtName,ia,ISym,ICentr,dsym,ic,ich)
        if(ich.eq.0) then
          call SetRealArrayTo(UxAt,KModA(2,ia)*3,0.)
          call SetRealArrayTo(UyAt,KModA(2,ia)*3,0.)
          call SetRealArrayTo(x40,NDimI(KPhase),0.)
          call UnitMat(RMiiAt,NDimI(KPhase))
        endif
      else
        call atsym(AtName,ia,XAt,dsym,xp,ISym,ich)
      endif
      if(ich.eq.0) then
        ksw=kswa(ia)
        isw=iswa(ia)
      endif
      if(ich.ne.0) then
        if(NMolec.gt.0) then
          ia=ktat(Atom(NAtMolFrAll(1)),NAtMol,AtName)
          if(ia.gt.0) then
            ia=ia+NAtMolFrAll(1)-1
            call CopyVek(x(1,ia),XAt,3)
            if(NDimI(KPhase).gt.0) then
              call CopyVek(ux(1,1,ia),UxAt,3*KModA(2,ia))
              call CopyVek(uy(1,1,ia),UyAt,3*KModA(2,ia))
              call CopyVek(qcnt(1,ia),x40,NDimI(KPhase))
            endif
            call UnitMat(RMiiAt,NDimI(KPhase))
            ich=0
          endif
        endif
      else if(NDimI(KPhase).gt.0) then
        j=iabs(ISym)
        call CopyMat(rm(1,j,isw,ksw),RmAt,3)
        call CopyMat(rm6(1,j,isw,ksw),Rm6At,NDim(KPhase))
        if(ISym.lt.0) then
          call RealMatrixToOpposite(RmAt,RmAt,3)
          call RealMatrixToOpposite(Rm6At,Rm6At,NDim(KPhase))
        endif
        call GetGammaIntInv(rm6(1,j,isw,ksw),RmiiAt)
        call CopyVek(x(1,ia),xp,3)
        call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
        call multm(Rm6At,xp,xpp,NDim(KPhase),NDim(KPhase),1)
        call AddVek(xpp,dsym,xp,NDim(KPhase))
        call CopyVek(xp,XAt,3)
        call qbyx(dsym,xp,isw)
        do j=1,NDimI(KPhase)
          xp(j)=xp(j)-dsym(j+3)
        enddo
        call CopyVek(qcnt(1,ia),x40,NDimI(KPhase))
        call cultm(RMiiAt,xp,x40,NDimI(KPhase),NDimI(KPhase),1)

        do j=1,KModA(2,ia)
          call multm(RmAt,ux(1,j,ia),UxAt(1,j),3,3,1)
          call multm(RmAt,uy(1,j,ia),UyAt(1,j),3,3,1)
        enddo
      endif
9999  return
      end
      subroutine AtSave(n,Klic)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*8 atoms
      integer DelkaKiAtomuS,AtSiteMultS
      dimension kfas(7),kmodas(7),axs(mxw),ays(mxw),
     1          xs(3),uxs(3,mxw),uys(3,mxw),
     2          betas(6),bxs(6,mxw),bys(6,mxw),
     3          c3s(10),c3xs(10,mxw),c3ys(10,mxw),
     4          c4s(15),c4xs(15,mxw),c4ys(15,mxw),
     5          c5s(21),c5xs(21,mxw),c5ys(21,mxw),
     6          c6s(28),c6xs(28,mxw),c6ys(28,mxw),
     7          saxs(mxw),says(mxw),sxs(3),suxs(3,mxw),suys(3,mxw),
     8          sbetas(6),sbxs(6,mxw),sbys(6,mxw),
     9          sc3s(10),sc3xs(10,mxw),sc3ys(10,mxw),
     a          sc4s(15),sc4xs(15,mxw),sc4ys(15,mxw),
     1          sc5s(21),sc5xs(21,mxw),sc5ys(21,mxw),
     2          sc6s(28),sc6xs(28,mxw),sc6ys(28,mxw),
     3          popass(64),isas(:),kmodaos(7),
     4          durdrs(9),KiAs(:)
      allocatable isas,KiAs
      save atoms,isfs,itfs,ifrs,iswas,kswas,kmols,lasmaxs,kfas,
     1     kmodas,ais,sais,a0s,sa0s,axs,ays,saxs,says,xs,sxs,uxs,uys,
     2     suxs,suys,betas,sbetas,bxs,bys,sbxs,sbys,
     3     c3s,c3xs,c3ys,c4s,c4xs,c4ys,c5s,c5xs,c5ys,c6s,c6xs,c6ys,
     4     sc3s,sc3xs,sc3ys,sc4s,sc4xs,sc4ys,sc5s,sc5xs,sc5ys,
     5     sc6s,sc6xs,sc6ys,phfs,sphfs,AtSiteMultS,KUsePolarS,
     6     popass,popvs,kapa1s,kapa2s,DelkaKiAtomuS,kias,
     7     MagParS,TypeModFuns,isas,kmodaos,durdrs
      if(Klic.eq.0) then
        if(allocated(isas)) deallocate(isas,KiAS)
        allocate(isas(MaxNSymm),KiAS(mxda))
        Atoms=Atom(n)
        isfs=isf(n)
        itfs=itf(n)
        ifrs=ifr(n)
        iswas=iswa(n)
        kswas=kswa(n)
        kmols=kmol(n)
        MagParS=MagPar(n)
        if(MagPar(n).gt.0) KUsePolarS=KUsePolar(n)
        lasmaxs=lasmax(n)
        TypeModFuns=TypeModFun(n)
        AtSiteMultS=AtSiteMult(n)
        call CopyVekI(KFA(1,n),kfas,7)
        call CopyVekI(KModA(1,n),kmodas,7)
        call CopyVekI(KModAO(1,n),kmodaos,7)
        call CopyVekI(isa(1,n),isas,MaxNSymm)
         ais= ai(n)
        sais=sai(n)
         a0s= a0(n)
        sa0s=sa0(n)
        if(NMolec.gt.0) call CopyVek(durdr(1,n),durdrs,9)
        call CopyVekI(KiA(1,n),KiAS,DelkaKiAtomu(n))
        if(kmodas(1).gt.0) then
          i=kmodas(1)
          if(i.gt.0) then
            call CopyVek(ax(1,n),axs,i)
            call CopyVek(ay(1,n),ays,i)
            call CopyVek(sax(1,n),saxs,i)
            call CopyVek(say(1,n),says,i)
          endif
        endif
        i=3
        call CopyVek(x(1,n),xs,i)
        call CopyVek(sx(1,n),sxs,i)
        i=kmodas(2)*i
        if(i.gt.0) then
          call CopyVek(ux(1,1,n),uxs,i)
          call CopyVek(uy(1,1,n),uys,i)
          call CopyVek(sux(1,1,n),suxs,i)
          call CopyVek(suy(1,1,n),suys,i)
        endif
        i=6
        call CopyVek(beta(1,n),betas,i)
        call CopyVek(sbeta(1,n),sbetas,i)
        i=kmodas(3)*i
        if(i.gt.0) then
          call CopyVek(bx(1,1,n),bxs,i)
          call CopyVek(by(1,1,n),bys,i)
          call CopyVek(sbx(1,1,n),sbxs,i)
          call CopyVek(sby(1,1,n),sbys,i)
        endif
        if(itfs.gt.2) then
          i=10
          call CopyVek(c3(1,n),c3s,i)
          call CopyVek(sc3(1,n),sc3s,i)
          i=kmodas(2)*i
          if(i.gt.0) then
            call CopyVek(c3x(1,1,n),c3xs,i)
            call CopyVek(c3y(1,1,n),c3ys,i)
            call CopyVek(sc3x(1,1,n),sc3xs,i)
            call CopyVek(sc3y(1,1,n),sc3ys,i)
          endif
          if(itfs.gt.3) then
            i=15
            call CopyVek(c4(1,n),c4s,i)
            call CopyVek(sc4(1,n),sc4s,i)
            i=kmodas(2)*i
            if(i.gt.0) then
              call CopyVek(c4x(1,1,n),c4xs,i)
              call CopyVek(c4y(1,1,n),c4ys,i)
              call CopyVek(sc4x(1,1,n),sc4xs,i)
              call CopyVek(sc4y(1,1,n),sc4ys,i)
            endif
            if(itfs.gt.4) then
              i=21
              call CopyVek(c5(1,n),c5s,i)
              call CopyVek(sc5(1,n),sc5s,i)
              i=kmodas(2)*i
              if(i.gt.0) then
                call CopyVek(c5x(1,1,n),c5xs,i)
                call CopyVek(c5y(1,1,n),c5ys,i)
                call CopyVek(sc5x(1,1,n),sc5xs,i)
                call CopyVek(sc5y(1,1,n),sc5ys,i)
              endif
              if(itfs.gt.5) then
                i=28
                call CopyVek(c6(1,n),c6s,i)
                call CopyVek(sc6(1,n),sc6s,i)
                i=kmodas(2)*i
                if(i.gt.0) then
                  call CopyVek(c6x(1,1,n),c6xs,i)
                  call CopyVek(c6y(1,1,n),c6ys,i)
                  call CopyVek(sc6x(1,1,n),sc6xs,i)
                  call CopyVek(sc6y(1,1,n),sc6ys,i)
                endif
              endif
            endif
          endif
          if(NDimI(kswa(n)).gt.0) then
            phfs=phf(n)
            sphfs=sphf(n)
          endif
        endif
        if(ChargeDensities) then
          kapa1s=kapa1(n)
          kapa2s=kapa2(n)
          popcs=popc(n)
          popvs=popv(n)
          if(lasmax(n).gt.1)
     1      call CopyVek(popas(1,n),popass,lasmax(n)**2)
        endif
1750    DelkaKiAtomuS=DelkaKiAtomu(n)
      else
        Atom(n)=Atoms
        isf(n)=isfs
        itf(n)=itfs
        ifr(n)=ifrs
        iswa(n)=iswas
        kswa(n)=kswas
        kmol(n)=kmols
        MagPar(n)=MagParS
        if(MagParS.gt.0) KUsePolar(n)=KUsePolarS
        lasmax(n)=lasmaxs
        TypeModFun(n)=TypeModFuns
        AtSiteMult(n)=AtSiteMultS
        call CopyVekI(kfas,KFA(1,n),7)
        call CopyVekI(kmodas,KModA(1,n),7)
        call CopyVekI(kmodaos,KModAO(1,n),7)
        call CopyVekI(isas,isa(1,n),MaxNSymm)
         ai(n)= ais
        sai(n)=sais
         a0(n)= a0s
        sa0(n)=sa0s
        if(NMolec.gt.0) call CopyVek(durdrs,durdr(1,n),9)
        call CopyVekI(kias,KiA(1,n),DelkaKiAtomuS)
        if(kmodas(1).gt.0) then
          i=kmodas(1)
          if(i.gt.0) then
            call CopyVek(axs,ax(1,n),i)
            call CopyVek(ays,ay(1,n),i)
            call CopyVek(saxs,sax(1,n),i)
            call CopyVek(says,say(1,n),i)
          endif
        endif
        i=3
        call CopyVek(xs,x(1,n),i)
        call CopyVek(sxs,sx(1,n),i)
        i=kmodas(2)*i
        if(i.gt.0) then
          call CopyVek(uxs,ux(1,1,n),i)
          call CopyVek(uys,uy(1,1,n),i)
          call CopyVek(suxs,sux(1,1,n),i)
          call CopyVek(suys,suy(1,1,n),i)
        endif
        i=6
        call CopyVek(betas,beta(1,n),i)
        call CopyVek(sbetas,sbeta(1,n),i)
        i=kmodas(2)*i
        if(i.gt.0) then
          call CopyVek(bxs,bx(1,1,n),i)
          call CopyVek(bys,by(1,1,n),i)
          call CopyVek(sbxs,sbx(1,1,n),i)
          call CopyVek(sbys,sby(1,1,n),i)
        endif
        if(itfs.gt.2) then
          i=10
          call CopyVek(c3s,c3(1,n),i)
          call CopyVek(sc3s,sc3(1,n),i)
          i=kmodas(2)*i
          if(i.gt.0) then
            call CopyVek(c3xs,c3x(1,1,n),i)
            call CopyVek(c3ys,c3y(1,1,n),i)
            call CopyVek(sc3xs,sc3x(1,1,n),i)
            call CopyVek(sc3ys,sc3y(1,1,n),i)
          endif
          if(itfs.gt.3) then
            i=15
            call CopyVek(c4s,c4(1,n),i)
            call CopyVek(sc4s,sc4(1,n),i)
            i=kmodas(2)*i
            if(i.gt.0) then
              call CopyVek(c4xs,c4x(1,1,n),i)
              call CopyVek(c4ys,c4y(1,1,n),i)
              call CopyVek(sc4xs,sc4x(1,1,n),i)
              call CopyVek(sc4ys,sc4y(1,1,n),i)
            endif
            if(itfs.gt.4) then
              i=21
              call CopyVek(c5s,c5(1,n),i)
              call CopyVek(sc5s,sc5(1,n),i)
              i=kmodas(2)*i
              if(i.gt.0) then
                call CopyVek(c5xs,c5x(1,1,n),i)
                call CopyVek(c5ys,c5y(1,1,n),i)
                call CopyVek(sc5xs,sc5x(1,1,n),i)
                call CopyVek(sc5ys,sc5y(1,1,n),i)
              endif
              if(itfs.gt.5) then
                i=28
                call CopyVek(c6s,c6(1,n),i)
                call CopyVek(sc6s,sc6(1,n),i)
                i=kmodas(2)*i
                if(i.gt.0) then
                  call CopyVek(c6xs,c6x(1,1,n),i)
                  call CopyVek(c6ys,c6y(1,1,n),i)
                  call CopyVek(sc6xs,sc6x(1,1,n),i)
                  call CopyVek(sc6ys,sc6y(1,1,n),i)
                endif
              endif
            endif
          endif
          if(NDimI(kswa(n)).gt.0) then
            phf(n)=phfs
            sphf(n)=sphfs
          endif
        endif
2750    call ShiftKiAt(n,itfs,ifrs,lasmaxs,kmodas,MagPars,.false.)
      endif
      return
      end
      subroutine EM40NewMol(ich)
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*256 EdwStringQuest,FileNameM45,FileNameM45Old,
     1              FileNameJPG,Veta,CurrentDirO
      character*80  AtP(3),StRefPointP,PositionName,t80
      character*8 AtM(3),NewMolName,ScDistSt
      character*8, allocatable :: AtActual(:)
      character*2 nty
      integer FeMenu,EdwStateQuest,Actual,UseTabsIn,FeChdir,
     1        RolMenuSelectedQuest
      integer, allocatable :: IAtActual(:),ip(:)
      logical FeYesNo,ExistFile,CrwLogicQuest,k45,ShowCoinc,EqIgCase,
     1        EqRV,Znovu,lpom
      real par(6),pa(9),pb(9),pc(9),trl(9),xx(3,3),xo(3,3),xxo(3,3),
     1     paa(3),pbb(3),paav(3),pbbv(3),trp(9),qcmol(3),AiModel(3),
     2     AiActual(3)
      real, allocatable :: xp(:,:)
      external EM40NewMolUpdateQuest,FeVoid
      common/EM40NewMolC/ nEdwFirstPoint,nEdwSelect,nButtSelect,nEdwOcc,
     1                    nButtCalc,nButtShowCoinc,Model(3),Actual(3),
     2                    nButtApplyNext,nButtApplyEnd,nButtPoint,Znovu
      data dco/0.3/
      save /EM40NewMolC/
      allocate(xp(3,3000),ip(3000))
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      if(NMolecLenAll(KPhase).le.0) irot(KPhase)=1
      isw=1
      k45=NAtIndLenAll(KPhase).le.0
      FileNameM45=' '
      FileNameM45Old=' '
      WizardId=NextQuestId()
      WizardMode=.true.
      WizardTitle=.false.
      WizardLength=450.
      WizardLines=11
      call FeQuestCreate(WizardId,-1.,-1.,WizardLength,WizardLines,
     1                   ' ',0,LightGray,0,0)
1100  id=NextQuestId()
      call FeQuestTitleMake(id,'Atoms of the new molecule from:')
      QuestCheck(id)=1
      il=1
      do i=1,2
        if(i.eq.1) then
          Veta='Mo%del file'
          tpom=WizardLength*.5-50.
        else
          Veta='%Atomic part'
          tpom=WizardLength*.5+50.
        endif
        xpom=tpom-CrwgXd
        call FeQuestCrwMake(id,tpom,il,xpom,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,1,2)
        if(i.eq.1) then
          nCrwFromM45=CrwLastMade
        else
          nCrwFromM40=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
        if(k45) call FeQuestCrwDisable(CrwLastMade)
      enddo
      il=il+2
      call FeQuestLinkaMake(id,il)
      tpom=5.
      Veta='%Name of the molecule'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=80.
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+10.
      pom=WizardLength-dpom-5.
      call FeQuestButtonMake(id,pom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      Veta='Model %filename'
      dpom=pom-10.-xpom
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      call FeQuestStringEdwOpen(EdwLastMade,FileNameM45)
      nEdwFileNameM45=EdwLastMade
      il=il+1
      Veta='%Predefined models'
      dpom=FeTxLengthUnder(Veta)+10.
      pom=WizardLength-dpom-5.
      call FeQuestButtonMake(id,pom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtPreDefined=ButtonLastMade
      Veta='%Scaling distance'
      pom=1.5
      xpom1=tpom+FeTxLength(Veta)
      dpom=100.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
      nEdwScDist=EdwLastMade
      il=il+1
      Veta='Sho%w the model molecule'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom1=(WizardLength-dpom)*.5
      call FeQuestButtonMake(id,xpom1,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtShowModel=ButtonLastMade
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='%Composite part'
        dpom=50.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwComp=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
        call FeQuestEudOpen(EdwLastMade,1,NComp(KPhase),1,0.,0.,0.)
        if(NComp(KPhase).le.1) call FeQuestEdwDisable(EdwLastMade)
      else
        nEdwComp=0
      endif
      call FeQuestButtonDisable(ButtonEsc)
      ScDist=-1.
      ScDistSt=' '
1150  if(k45) then
        call FeQuestStringEdwOpen(nEdwFileNameM45,FileNameM45)
        call FeQuestButtonOff(nButtBrowse)
        call FeQuestButtonOff(nButtPreDefined)
        if(FileNameM45.ne.' ') then
          if(ExistFile(FileNameM45).and.
     1       .not.EqIgCase(FileNameM45,FileNameM45Old)) then
            ScDist=-1.
            ScDistSt=' '
            n=0
            call GetPureFileName(FileNameM45,FileNameJPG)
            FileNameJPG=FileNameJPG(:idel(FileNameJPG))//'.jpg'
            call OpenFile(45,FileNameM45,'formatted','old')
            if(ErrFlag.ne.0) go to 1170
1160        read(45,FormA,end=1170) Veta
            k=0
            call kus(Veta,k,Cislo)
            if(EqIgCase(Cislo,'scdist')) then
              n=n+1
              call kus(Veta,k,ScDistSt)
              call StToReal(Veta,k,pa,1,.false.,ichp)
              if(ichp.ne.0) then
                ScDist=-1.
                ScDistSt=' '
              else
                ScDist=pa(1)
              endif
            else
              go to 1160
            endif
1170        call CloseIfOpened(45)
            FileNameM45Old=FileNameM45
          endif
        else if(.not.ExistFile(FileNameM45)) then
          ScDist=-1.
          ScDistSt=' '
          FileNameJPG=' '
          FileNameM45Old=' '
        endif
        if(ScDist.gt.0.) then
          Veta='%Scaling distance '//ScDistSt(:idel(ScDistSt))
          call FeQuestEdwLabelChange(nEdwScDist,Veta)
          call FeQuestRealEdwOpen(nEdwScDist,ScDist,.false.,.false.)
        else
          call FeQuestEdwDisable(nEdwScDist)
        endif
        if(ExistFile(FileNameJPG)) then
          call FeQuestButtonOff(nButtShowModel)
        else
          call FeQuestButtonDisable(nButtShowModel)
        endif
      else
        call FeQuestEdwDisable(nEdwFileNameM45)
        call FeQuestButtonDisable(nButtBrowse)
        call FeQuestButtonDisable(nButtPreDefined)
        call FeQuestButtonDisable(nButtShowModel)
        call FeQuestRealFromEdw(nEdwScDist,ScDist)
        call FeQuestEdwDisable(nEdwScDist)
      endif
1200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        NewMolName=EdwStringQuest(nEdwName)
        call zhusti(NewMolName)
        call uprat(NewMolName)
        call atcheck(NewMolName,ichp,i)
        if(ichp.ne.0.or.NewMolName.eq.' ') then
          if(ichp.eq.1.or.NewMolName.eq.' ') then
            Veta='Unacceptable symbol in the name "'//
     1           NewMolName(:idel(NewMolName))//'"'
          else if(ichp.eq.2) then
            Veta='The name "'//NewMolName(:idel(NewMolName))//
     1           '" already exists'
          endif
          call FeChybne(-1.,-1.,Veta(:idel(Veta))//', try again.',' ',
     1                  SeriousError)
          go to 1250
        endif
        if(NAtIndLenAll(KPhase).gt.0) k45=CrwLogicQuest(nCrwFromM45)
        if(k45) then
          FileNameM45=EdwStringQuest(nEdwFileNameM45)
          if(.not.ExistFile(FileNameM45)) then
            Veta='The model file "'//FileNameM45(:idel(FileNameM45))//
     1           '" doesn''t exist'
            call FeChybne(-1.,-1.,Veta(:idel(Veta))//', try again.',' ',
     1                    SeriousError)
            go to 1250
          endif
        endif
        QuestCheck(id)=0
        go to 1200
1250    EventType=EventEdw
        EventNumber=nEdwName
        go to 1200
      else if(CheckType.eq.EventCrw.and.
     1       (CheckNumber.eq.nCrwFromM45.or.CheckNumber.eq.nCrwFromM40))
     2  then
        k45=CrwLogicQuest(nCrwFromM45)
        go to 1150
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtBrowse.or.
     2         CheckNumber.eq.nButtPreDefined)) then
        if(CheckNumber.eq.nButtPreDefined) then
          CurrentDirO=CurrentDir
          i=FeChdir(HomeDir(:idel(HomeDir))//'Molecular patterns\')
          lpom=.false.
        else
          lpom=.true.
        endif
        call FeFileManager('Select model file',FileNameM45,'*.m45',0,
     1                     lpom,ich)
        if(ich.eq.0) then
          if(CheckNumber.eq.nButtPreDefined.and.
     1       index(FileNameM45,'\').le.0)
     2      FileNameM45=CurrentDir(:idel(CurrentDir))//
     3                  FileNameM45(:idel(FileNameM45))
          call FeQuestStringEdwOpen(nEdwFileNameM45,FileNameM45)
        endif
        EventType=EventEdw
        EventNumber=nEdwFileNameM45
        if(CheckNumber.eq.nButtPreDefined)
     1    i=FeChdir(CurrentDirO)
        go to 1150
      else if(CheckType.eq.EventEdw.and.
     1        CheckNumber.eq.nEdwFileNameM45) then
        FileNameM45=EdwStringQuest(nEdwFileNameM45)
        go to 1150
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtShowModel) then
        call FeShellExecute(FileNameJPG)
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1200
      endif
      if(ich.eq.0) then
        if(NComp(KPhase).gt.1) call FeQuestIntFromEdw(nEdwComp,isw)
        if(EdwStateQuest(nEdwScDist).eq.EdwOpened)
     1    call FeQuestRealFromEdw(nEdwScDist,ScDist)
        nap=NAtIndFr(isw,KPhase)
        nak=NAtIndTo(isw,KPhase)
        if(NMolec.ge.mxm) then
          if(NMolec.le.0) then
            if(allocated(iam)) deallocate(iam,mam)
            allocate(iam(1),mam(1))
            iam(1)=0
            mam(1)=1
            call AllocateMolecules(1,1)
          else
            call ReallocateMolecules(1,0)
          endif
        endif
        nm=NMolecTo(isw,KPhase)+1
        call MolSun(nm,NMolec,nm+1)
        do j=KPhase,NPhase
          if(j.eq.KPhase) then
            kp=isw
          else
            kp=1
          endif
          do k=kp,NComp(j)
            if(j.ne.KPhase.or.k.ne.kp) NMolecFr(k,j)=NMolecFr(k,j)+1
            NMolecTo(k,j)=NMolecTo(k,j)+1
          enddo
          if(j.ne.KPhase) NMolecFrAll(j)=NMolecFrAll(j)+1
          NMolecToAll(j)=NMolecToAll(j)+1
        enddo
        NMolecLen(isw,KPhase)=NMolecLen(isw,KPhase)+1
        NMolecLenAll(KPhase)=NMolecLenAll(KPhase)+1
        NMolec=NMolec+1
        MolName(nm)=NewMolName
        KTLS(nm)=0
        SmbPGMol(nm)='1'
        ISwMol(nm)=isw
        KSwMol(nm)=KPhase
        NPoint(nm)=1
        KPoint(nm)=1
        IPoint(1,nm)=1
        call UnitMat(RPoint(1,1,nm),3)
        call UnitMat(TrPG(1,nm),3)
        call UnitMat(TrIPG(1,nm),3)
        call SetRealArrayTo(SPoint(1,1,nm),3,0.)
        call srotb(RPoint(1,1,nm),RPoint(1,1,nm),TPoint(1,1,nm))
        UsePGSyst(nm)=.false.
        LocPGSystAx(nm)='xy'
        LocPGSystSt(1,nm)=' 1.000000 0.000000 0.000000'
        LocPGSystSt(2,nm)=' 0.000000 1.000000 0.000000'
        call SetRealArrayTo(LocPGSystX(1,1,nm),6,0.)
        LocPGSystX(1,1,nm)=1.
        LocPGSystX(2,2,nm)=1.
        nk=NAtMolTo(isw,KPhase)
        np=nk+1
      endif
      if(ich.ne.0) then
        call FeQuestRemove(WizardId)
        go to 9999
      endif
      if(k45) then
        ScDistSt=' '
        isfmx=0
        call OpenFile(45,FileNameM45,'formatted','old')
        if(ErrFlag.ne.0) go to 9999
        n=0
1400    read(45,FormA,end=1450) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'cell')) then
          n=n+1
          read(Veta(k+1:),*,err=9100) par
        else if(EqIgCase(Cislo,'pgroup')) then
          n=n+1
          call kus(Veta,k,SmbPGMol(nm))
          if(SmbPGMol(nm).ne.' ') then
            call GenPg(SmbPGMol(nm),rpoint(1,1,nm),npoint(nm),ich)
            call SetIntArrayTo(ipoint(1,nm),npoint(nm),1)
            kpoint(nm)=npoint(nm)
            call SetIntArrayTo(ipoint(1,nm),npoint(nm),1)
c            pa=0.
c            call SpecPg(pa,rpoint(1,1,nm),isw,npoint(i))
c            call SetIntArrayTo(ipoint(1,nm),npoint(nm),1)
c            do k=2,n
c              do 2800l=1,npoint(i)
c                if(ipoint(l,i).le.0) cycle
c                call multm(rpgp(1,k),rpoint(1,l,i),px,3,3,3)
c                do m=2,npoint(i)
c                  if(ipoint(m,i).le.0) cycle
c                  if(eqrv(px,rpoint(1,m,i),9,.0001)) then
c                    ipoint(m,i)=0
c                    go to 2800
c                  endif
c                enddo
c2800          continue
c            enddo
c            k=0
c            do l=1,npoint(i)
c              if(ipoint(l,i).gt.0) k=k+1
c              call srotb(rpoint(1,l,i),rpoint(1,l,i),tpoint(1,l,i))
c              call multm(rpoint(1,l,i),xm(1,i),spoint(1,l,i),3,3,1)
c              do m=1,3
c                spoint(m,l,i)=xm(m,i)-spoint(m,l,i)
c              enddo
c            enddo
c            kpoint(i)=k
c            iam(i)=iam(i)*k
          endif
        else if(EqIgCase(Cislo,'scdist')) then
          n=n+1
          call kus(Veta,k,ScDistSt)
          call StToReal(Veta,k,pa,1,.false.,ichp)
          ScDistO=pa(1)
          if(ichp.ne.0) go to 9100
        endif
        if(n.lt.3) then
          go to 1400
        else
          go to 1460
        endif
1450    rewind 45
        read(45,*,err=9100,end=9200) par
1460    do i=4,6
          par(i)=cos(par(i)*torad)
        enddo
        sng=sqrt(1.-par(6)**2)
        volume=sqrt(1.-par(4)**2-par(5)**2-par(6)**2
     1              +2.*par(4)*par(5)*par(6))
        if(ScDistSt.eq.' ') then
          pom=1.
        else
          pom=ScDist/ScDistO
        endif
        pa(1)=par(1)*pom
        pa(2)=0.
        pa(3)=0.
        pa(4)=par(2)*par(6)*pom
        pa(5)=par(2)*sng*pom
        pa(6)=0.
        pa(7)=par(3)*par(5)*pom
        pa(8)=par(3)*pom*(par(4)-par(5)*par(6))/sng
        pa(9)=par(3)*pom*volume/sng
        call matinv(TrToOrtho(1,isw,KPhase),pb,pom,3)
        call multm(pb,pa,trl,3,3,3)
      else
        call SetLogicalArrayTo(AtBrat(nap),nak-nap+1,.false.)
        WizardMode=.false.
        call SelAtoms('Select atoms for the molecule',Atom(nap),
     1                AtBrat(nap),isf(nap),nak-nap+1,ich)
        WizardMode=.true.
        if(ich.ne.0) then
          call FeQuestRemove(WizardId)
          go to 9900
        endif
      endif
      i=nap
      iamp=0
      nas=0
      call ReallocateAtoms(0)
      AiMax=-999999.
2000  if(k45) then
        read(45,FormA,end=2100) Veta
        if(Veta.eq.' ') go to 2000
        read(Veta,'(a8,i3,7x,4f9.6,i3)') atm(1),isfp,aip,paa,ksymm
        if(ksymm.le.0) nas=nas+1
      else
        if(i.gt.nak) go to 2100
        if(isf(i).le.0.or..not.AtBrat(i)) then
          i=i+1
          go to 2000
        endif
      endif
      if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
      nk=nk+1
      call atsun(nk,NAtMol,nk+1)
      iamp=iamp+1
      NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+1
      NAtCalcBasic=NAtCalc
      call EM40UpdateAtomLimits
      call SetBasicKeysForAtom(nk)
      if(k45) then
        Atom(nk)=atm(1)
        call multm(trl,paa,x(1,nk),3,3,1)
        beta(1,nk)=3.
        isfmx=max(isfmx,isfp)
        isf(nk)=-isfp-1000*ksymm
        ai(nk)=aip
      else
        Atom(nk)=Atom(i)
        iswa(nk)=isw
        kswa(nk)=KPhase
        kmol(nk)=nm
        isf(nk)=isf(i)
        isf(i)=-isf(i)
        itf(nk)=itf(i)
        itf(i)=-itf(i)
        ifr(nk)=ifr(i)
        call CopyVekI(KModA(1,i),KModA(1,nk),3)
        call CopyVekI(KFA(1,i),KFA(1,nk),3)
        if(NDimI(KPhase).gt.0)
     1    call CopyVek(qcnt(1,i),qcnt(1,nk),NDimI(KPhase))
        ai(nk)=ai(i)
        call CopyVek(x(1,i),x(1,nk),3)
        call CopyVek(beta(1,i),beta(1,nk),6)
        k=KModA(1,i)
        j=0
        if(k.gt.0) then
          a0(nk)=a0(i)
          call CopyVek(ax(1,i),ax(1,nk),k)
          call CopyVek(ay(1,i),ay(1,nk),k)
          call SetRealArrayTo(sax(1,nk),k,0.)
          call SetRealArrayTo(say(1,nk),k,0.)
          j=1
        endif
        k=3*KModA(2,i)
        if(k.gt.0) then
          call CopyVek(ux(1,1,i),ux(1,1,nk),k)
          call CopyVek(uy(1,1,i),uy(1,1,nk),k)
          call SetRealArrayTo(sux(1,1,nk),k,0.)
          call SetRealArrayTo(suy(1,1,nk),k,0.)
          j=1
        endif
        k=6*KModA(3,i)
        if(k.gt.0) then
          call CopyVek(bx(1,1,i),bx(1,1,nk),k)
          call CopyVek(by(1,1,i),by(1,1,nk),k)
          call SetRealArrayTo(sbx(1,1,nk),k,0.)
          call SetRealArrayTo(sby(1,1,nk),k,0.)
          j=1
        endif
        if(j.gt.0) then
          phf(nk)=phf(i)
          sphf(nk)=sphf(i)
        endif
      endif
      AiMax=max(AiMax,ai(nk))
      if(nk.eq.NAtMolFr(1,1)) then
        if(NAtCalc.eq.0) then
          PrvniKiAtomu(nk)=ndoff
        else
          PrvniKiAtomu(nk)=PrvniKiAtomu(NAtCalc)+
     1                     DelkaKiAtomu(NAtCalc)
        endif
      else
        PrvniKiAtomu(nk)=PrvniKiAtomu(nk-1)+DelkaKiAtomu(nk-1)
      endif
      DelkaKiAtomu(nk)=0
      i=i+1
      go to 2000
2100  if(iamp.le.0) then
        Veta='The atom selection failed, no atoms for the new '//
     1       'molecule read in.'
        call FeQuestRemove(WizardId)
        go to 9900
      endif
      iampp=iamp
      if(k45) then
        iamp=kpoint(nm)*nas
        if(NAtAll+iamp-iampp.ge.MxAtAll)
     1    call ReallocateAtoms(NAtAll+iamp-iampp)
        do i=nk+1,nk+iamp-iampp
          NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+1
          NAtCalcBasic=NAtCalc
          call EM40UpdateAtomLimits
          call SetBasicKeysForAtom(i)
          write(Cislo,'(i5)') i
          call Zhusti(Cislo)
          Atom(i)='#nic'//Cislo(:idel(Cislo))
        enddo
      else

      endif
      if(NAtAll+iamp.ge.MxAtAll) call ReallocateAtoms(NAtAll+iamp)
      call atsun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+iamp)
      np=np+iamp
      nk=nk+iamp
      NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+iamp
      NAtCalcBasic=NAtCalc
      call EM40UpdateAtomLimits
      if(k45) then
        id=NextQuestId()
        Veta='Specify atomic types in the model molecule:'
        call FeQuestTitleMake(id,Veta)
        il=0
        xpom=5.
        dpom=50.
        tpom=xpom+dpom+10.
        do i=1,isfmx
          il=il+1
          Veta=' '
          k=1
          t80=' '
          do j=np,nk
            if(-mod(-isf(j),1000).eq.-i) then
              Veta(k:)=Atom(j)
              k=idel(Veta)+2
              if(t80.eq.' ') then
                do l=1,idel(Atom(j))
                  if(index(Cifry(1:10),Atom(j)(l:l)).le.0)
     1              t80=t80(:idel(t80))//Atom(j)(l:l)
                enddo
              endif
              if(k.gt.50) then
                Veta(k:)=' ... '
                exit
              endif
            endif
          enddo
          idl=min(idel(t80),2)
          do j=idl,1,-1
            k=LocateInStringArray(AtTypeMenu,NAtFormula(KPhase),t80(:j),
     1                            IgnoreCaseYes)
            if(k.gt.0) exit
          enddo
          if(k.le.0) k=1
          call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                            EdwYd,0)
          call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                            NAtFormula(KPhase),k)
          if(i.eq.1) nRolMenuAtType=RolMenuLastMade
        enddo
2500    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 2500
        endif
        nRolMenu=nRolMenuAtType
        if(ich.gt.0) then
          go to 1100
        else if(ich.lt.0) then
          call FeQuestRemove(WizardId)
          go to 9900
        endif
        do i=1,isfmx
          isfp=RolMenuSelected(nRolMenu)
          do j=np,nk
            if(isf(j).eq.-i) isf(j)=isfp
          enddo
          nRolMenu=nRolMenu+1
        enddo
      endif
      iam(nm)=iamp
      do i=np,nk
        ai(i)=ai(i)/AiMax
      enddo
      mamp=0
      id=NextQuestId()
      call FeQuestTitleMake(id,'Define the molecular reference point:')
      QuestCheck(id)=1
      il=1
      spom=90.
      tpom=WizardLength*.5-spom
      do i=1,3
        if(i.eq.1) then
          Veta='%Explicit'
        else if(i.eq.2) then
          Veta='%Gravity center'
        else
          Veta='Ge%om. center'
        endif
        xpom=tpom-CrwgXd*.5
        call FeQuestCrwMake(id,tpom,il,xpom,il+1,Veta,'C',CrwgXd,
     1                      CrwgYd,1,3)
        if(i.eq.1) then
          nCrwCenterFirst=CrwLastMade
        else if(i.eq.3) then
          nCrwCenterLast=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
        tpom=tpom+spom
      enddo
      il=il+1
      tpom=5.
      Veta='%Reference point'
      xpom=tpom+FeTxLength(Veta)+15.
      dpom=150.
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRef=EdwLastMade
      call FeQuestStringEdwOpen(nEdwRef,' ')
      xpom=xpom+dpom+15.
      Veta='%Select from the list'
      dpom=FeTxLengthUnder(Veta)+15.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtList=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
2600  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=nCrwCenterFirst,nCrwCenterLast
          if(CrwLogicQuest(i)) then
            RefPoint(nm)=i-nCrwCenterFirst
            exit
          endif
        enddo
        if(RefPoint(nm).le.0) then
          StRefPointP=EdwStringQuest(nEdwRef)
          if(StRefPointP.eq.' ') then
            Veta='Please fill the reference point'
            go to 2650
          endif
          k=0
          call StToReal(StRefPointP,k,xm(1,nm),3,.false.,ichp)
          if(ichp.eq.0) then
            StRefPointP=' '
          else
            call zhusti(StRefPointP)
            call uprat(StRefPointP)
            k=ktat(Atom(np),iamp,StRefPointP)
            if(k.le.0) then
              if(index(Cifry,StRefPointP(1:1)).gt.0) then
                Veta='in the numerical string defining the reference '//
     1               'point'
              else
                Veta='The atom defining the reference point is n''t '//
     1               'an atom of the molecule'
              endif
              go to 2650
            endif
            k=k+np-1
            do i=1,3
              xm(i,nm)=x(i,k)
            enddo
          endif
          StRefPoint(nm)=StRefPointP
        else
          suma=0.
          call SetRealArrayTo(xm(1,nm),3,0.)
          do i=np,nk
            if(RefPoint(nm).eq.1) then
              pom=AtWeight(mod(isf(i),1000),KPhase)
            else
              pom=1.
            endif
            do j=1,3
              xm(j,nm)=xm(j,nm)+pom*x(j,i)
            enddo
            suma=suma+pom
          enddo
          do i=1,3
            xm(i,nm)=xm(i,nm)/suma
          enddo
          StRefPoint(nm)=' '
        endif
        QuestCheck(id)=0
        go to 2600
2650    call FeChybne(-1.,-1.,Veta(:idel(Veta))//', try again.',' ',
     1                SeriousError)
        EventType=EventEdw
        EventNumber=nEdwRef
        go to 2600
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwCenterFirst.and.
     2        CheckNumber.le.nCrwCenterLast) then
        i=EdwStateQuest(nEdwRef)
        if(EdwStateQuest(nEdwRef).ne.EdwOpened.and.
     1    CheckNumber.eq.nCrwCenterFirst) then
          call FeQuestStringEdwOpen(nEdwRef,' ')
          call FeQuestButtonOpen(nButtList,ButtonOff)
          EventType=EventEdw
          EventNumber=nEdwRef
        else if(CheckNumber.ne.nCrwCenterFirst) then
          call FeQuestEdwDisable(nEdwRef)
          call FeQuestButtonDisable(nButtList)
        endif
        go to 2600
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtList)
     1  then
        WizardMode=.false.
        call SelOneAtom('Select the reference atom:',Atom(np),ia,iampp,
     1                  ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwRef,Atom(np+ia-1))
        WizardMode=.true.
        go to 2600
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2600
      endif
      if(ich.eq.0) then
        if(NDimI(KPhase).gt.0) then
          call qbyx(xm,qcmol,isw)
          do i=np,nk
            kk=0
            do k=1,KModA(1,i)
              if(k.gt.KModA(1,i)-NDimI(KPhase).and.KFA(1,i).ne.0) then
                kk=kk+1
                ax(k,i)=ax(k,i)+qcmol(kk)-qcnt(kk,i)
              else
                fik=0.
                do m=1,NDimI(KPhase)
                  fik=fik+(qcmol(m)-qcnt(m,i))*float(kw(m,k,KPhase))
                enddo
                sinfik=sin(pi2*fik)
                cosfik=cos(pi2*fik)
                xpp=ax(k,i)
                ypp=ay(k,i)
                ax(k,i)= xpp*cosfik+ypp*sinfik
                ay(k,i)=-xpp*sinfik+ypp*cosfik
              endif
            enddo
            do k=1,KModA(2,i)
              if(k.eq.KModA(2,i).and.KFA(2,i).ne.0) then
                uy(1,k,i)=uy(1,k,i)+qcmol(1)-qcnt(1,i)
              else
                fik=0.
                do m=1,NDimI(KPhase)
                  fik=fik+(qcmol(m)-qcnt(m,i))*float(kw(m,k,KPhase))
                enddo
                sinfik=sin(pi2*fik)
                cosfik=cos(pi2*fik)
                do l=1,3
                  xpp=ux(l,k,i)
                  ypp=uy(l,k,i)
                  ux(l,k,i)= xpp*cosfik+ypp*sinfik
                  uy(l,k,i)=-xpp*sinfik+ypp*cosfik
                enddo
              endif
            enddo
            do k=1,KModA(3,i)
              fik=0.
              do m=1,NDimI(KPhase)
                fik=fik+(qcmol(m)-qcnt(m,i))*float(kw(m,k,KPhase))
              enddo
              sinfik=sin(pi2*fik)
              cosfik=cos(pi2*fik)
              do l=1,6
                xpp=bx(l,k,i)
                ypp=by(l,k,i)
                bx(l,k,i)= xpp*cosfik+ypp*sinfik
                by(l,k,i)=-xpp*sinfik+ypp*cosfik
              enddo
            enddo
          enddo
        endif
      else if(ich.gt.0) then
        go to 1100
      else if(ich.lt.0) then
        call FeQuestRemove(WizardId)
        go to 9900
      endif
      call FeQuestRemove(WizardId)
      go to 3500
      entry EM40NewMolPos(ich)
      allocate(xp(3,3000),ip(3000))
      k45=.false.
      if(NMolec.gt.1) then
        id=NextQuestId()
        xqd=300.
        il=NMolec
        Veta='Select the molecule for adding a new position'
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        xpom=5.
        tpom=xpom+CrwgXd+15.
        do il=1,NMolec
          call FeQuestCrwMake(id,tpom,il,xpom,il,MolName(il),'L',
     1                        CrwgXd,CrwgYd,0,1)
          call FeQuestCrwOpen(CrwLastMade,il.eq.1)
          if(il.eq.1) nCrwFirst=CrwLastMade
        enddo
3100    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 3100
        endif
        if(ich.eq.0) then
          do nm=1,NMolec
            if(CrwLogicQuest(nm)) exit
          enddo
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9999
      else
        nm=1
      endif
      isw=iswmol(nm)
      mamp=mam(nm)
      iampp=iam(nm)
      np=NAtMolFr(1,1)
      do i=1,nm-1
        np=np+iam(i)
      enddo
      nk=np+iampp-1
      AiMax=-999999.
      do i=np,nk
        AiMax=max(AiMax,ai(i))
      enddo
      nap=NAtIndFr(isw,KPhase)
      nak=NAtIndTo(isw,KPhase)
3500  NAtActual=nak-nap+1
      allocate(AtActual(NAtActual),IAtActual(NAtActual))
      n=0
      do i=nap,nak
        n=n+1
         AtActual(n)=Atom(i)
        IAtActual(n)=i
      enddo
      mampp=mamp+1
      ji=mxp*(nm-1)+mamp
4000  Znovu=.true.
      mamp=mamp+1
      if(mamp.gt.mxp) then
        call ReallocateMolecules(0,1)
        ji=mxp*(nm-1)+mamp-1
        call ReallocateAtoms(iam(nm))
      endif
      if(mamp.eq.mampp) then
        call SetStringArrayTo(AtM,3,' ')
        call SetStringArrayTo(AtP,3,' ')
        call SetIntArrayTo(Model,3,0)
        call SetIntArrayTo(Actual,3,0)
      endif
      ji=ji+1
      call UnitMat(RotMol (1,ji),3)
      call UnitMat(RotIMol(1,ji),3)
      RotSign(ji)=1
       aimol(ji)=AiMax
       AiPom=AiMax
      saimol(ji)=0.
       a0m(ji)=1.
      sa0m(ji)=0.
      LocMolSystType(ji)=0
      TypeModFunMol(ji)=0
      call SetRealArrayTo(DRotF(1,ji),9,0.)
      call SetRealArrayTo(DRotC(1,ji),9,0.)
      call SetRealArrayTo(DRotP(1,ji),9,0.)
      call SetRealArrayTo(RotB(1,ji),36,0.)
      call SetRealArrayTo(DRotBF(1,ji),36,0.)
      call SetRealArrayTo(DRotBC(1,ji),36,0.)
      call SetRealArrayTo(DRotBP(1,ji),36,0.)
      call SetStringArrayTo(LocMolSystAx(1,ji),2,' ')
      call SetStringArrayTo(LocMolSystSt(1,1,ji),4,' ')
      call SetRealArrayTo(LocMolSystX(1,1,1,ji),12,0.)
      AtTrans(ji)=' '
      call CopyMat(TrToOrtho(1,isw,KPhase),TrMol(1,ji),3)
      call CopyMat(TrToOrtho(1,isw,KPhase),trp,3)
      call matinv(trp,TriMol(1,ji),pom,3)
      if(KTLS(nm).gt.0) then
        call SetRealArrayTo(tt(1,ji),6,0.)
        call SetRealArrayTo(stt(1,ji),6,0.)
        call SetRealArrayTo(tl(1,ji),6,0.)
        call SetRealArrayTo(stl(1,ji),6,0.)
        call SetRealArrayTo(ts(1,ji),9,0.)
        call SetRealArrayTo(sts(1,ji),9,0.)
      endif
      if(NDimI(KPhase).gt.0) then
        phfm(ji)=0.
        sphfm(ji)=0.
        OrthoX40Mol(ji)=0.
        OrthoDeltaMol(ji)=0.
        OrthoEpsMol(ji)=0.
      endif
      write(PositionName,'('' molecular position #'',i2)') mamp
      if(mamp.eq.1.and..not.k45) then
        call SetRealArrayTo(euler(1,ji),3,0.)
        call SetRealArrayTo(trans(1,ji),3,0.)
      endif
      id=NextQuestId()
      xqd=400.
      if(mamp.gt.1.or.k45) then
        Veta='Define and complete'
      else
        Veta='Complete'
      endif
      Veta=Veta(:idel(Veta))//PositionName(:idel(PositionName))//':'
      if(mamp.gt.1.or.k45) then
        il=17
      else
        il=5
      endif
      call FeQuestCreate(Id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
      il=1
      if(mamp.gt.1.or.k45) then
        xpom=5.
        tpom=xpom+CrwgXd+15.
        Veta='Apply %inversion'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,RotSign(ji).ne.1)
        nCrwInversion=CrwLastMade
        il=il+1
        call FeQuestLblMake(id,110.,il,'Model atom','C','N')
        call FeQuestLblMake(id,240.,il,'Actual position/atom','C','N')
        tpom=5.
        xpom1=tpom+FeTxLength('XXXXXXXXX')+10
        dpom1=100.
        xpom2=xpom1+dpom1+10.
        dpom2=200.
        do i=1,3
          write(Veta,'(i1,a2,'' point'')') i,nty(i)
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom1,il,Veta,'L',dpom1,EdwYd,
     1                      1)
          if(i.eq.1) nEdwFirstPoint=EdwLastMade
          call FeQuestStringEdwOpen(EdwLastMade,AtM(i))
          call FeQuestEdwMake(id,tpom,il,xpom2,il,' ','L',dpom2,EdwYd,1)
          call FeQuestStringEdwOpen(EdwLastMade,AtP(i))
        enddo
        nEdwLastPoint=EdwLastMade
        il=il+1
        Veta='%Select the model atom'
        dpom=FeTxLengthUnder(Veta)+15.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtSelect=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='Fill by a saved %point'
        dpom=FeTxLengthUnder(Veta)+15.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtPoint=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        Veta='Options for removing of atoms from atomic block'
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
        il=-10*il-7
        Veta='coinciding with those generated from'//
     1       PositionName(:idel(PositionName))//':'
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
        Veta='Ma%ximal coincidence distance'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=80.
        il=il-10
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwMaxCoinc=EdwLastMade
        call FeQuestRealEdwOpen(nEdwMaxCoinc,dco,.false.,.false.)
        xpom=xpom+dpom+15.
        Veta='Sho%w coinciding atoms'
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtShowCoinc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il-10
        Veta='%Calculate molecular parameters'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtCalc=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il-10
        call FeQuestLinkaMake(id,il)
        il=il-10
        ncoinc=0
        call SetRealArrayTo(AiModel,3,0.)
        call SetRealArrayTo(AiActual,3,0.)
      else
        nEdwFirstPoint=0
        nEdwLastPoint=0
        nCrwInversion=0
        nButtCalc=0
        nButtSelect=0
        nEdwMaxCoinc=0
        nButtShowCoinc=0
        il=-10*il
        ncoinc=iamp
      endif
      tpom=5.
      Veta='Molecular parameters:'
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      nLblMolPar=LblLastMade
      if(mamp.gt.1.or.k45) call FeQuestLblOff(LblLastMade)
      il=il-7
      Veta=' '
      if(mamp.eq.1.and..not.k45)
     1  write(Veta,102)(euler(i,ji),i=1,3),RotSign(ji)
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblRot=LblLastMade
      if(mamp.eq.1.and..not.k45)
     1  write(Veta,103)(trans(i,ji),i=1,3)
      il=il-7
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      nLblTrans=LblLastMade
      tpom=5.
      dpom=80.
      il=il-10
      Veta='%Occupancy'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=80.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwOcc=EdwLastMade
      if(mamp.eq.1.and..not.k45) then
        call FeQuestRealEdwOpen(nEdwOcc,aimol(ji),.false.,.false.)
        AiMolPom=aimol(ji)
      endif
      xpom=xpom+dpom+30.
      Veta='Coincidence ratio:'
      write(Cislo,104) ncoinc,iampp
      call Zhusti(Cislo)
      Veta(idel(Veta)+2:)=Cislo
      call FeQuestLblMake(id,xpom,il,Veta,'L','N')
      if(mamp.gt.1.or.k45) call FeQuestLblOff(LblLastMade)
      nLblCoinc=LblLastMade
      il=il-15
      Veta='%Apply+Next position'
      dpom=FeTxLengthUnder(Veta)+10.
      pom=dpom+10.
      xpom=xqd*.5-dpom*1.5-10.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.1) then
          nButtApplyNext=ButtonLastMade
          Veta='Appl%y+End'
        else if(i.eq.2) then
          Veta='%Quit'
          nButtApplyEnd=ButtonLastMade
        else if(i.eq.3) then
          nButtQuit=ButtonLastMade
        endif
        xpom=xpom+pom
      enddo
4100  call FeQuestEventWithCheck(id,ich,EM40NewMolUpdateQuest,FeVoid)
      Znovu=.false.
      iw=nEdwSelect-nEdwFirstPoint+1
      iwp=(iw-1)/2+1
      iwm=mod(iw-1,2)+1
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit.or.
     1   CheckType.eq.EventKey.and.CheckNumber.eq.JeEscape) then
        Konec=3
        go to 4350
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtApplyNext.or.
     2         CheckNumber.eq.nButtApplyEnd)) then
        if(CheckNumber.eq.nButtApplyNext) then
          Konec=1
        else
          Konec=2
        endif
        if(mamp.gt.1.or.k45) then
          do i=nEdwFirstPoint,nEdwLastPoint
            if(EdwStringQuest(i).eq.' ') then
              call FeChybne(-1.,-1.,'Information isn''t complete.',' ',
     1                      SeriousError)
              EventType=EventEdw
              EventNumber=i
              go to 4100
            endif
          enddo
          ShowCoinc=.false.
          go to 4200
        else
          go to 4300
        endif
      else if(CheckType.eq.EventEdw.and.
     1        CheckNumber.ge.nEdwFirstPoint.and.
     2        CheckNumber.le.nEdwLastPoint) then
        if(iwm.eq.1) then
          AtM(iwp)=EdwStringQuest(CheckNumber)
          if(EventTypeSave.eq.EventButton.and.
     2       (EventNumberSave.eq.nButtSelect.or.
     3         EventNumberSave.eq.nButtPoint)) go to 4100
          if(AtM(iwp).ne.' ') then
            call zhusti(AtM(iwp))
            call uprat(AtM(iwp))
            Model(iwp)=ktat(Atom(np),iampp,AtM(iwp))+np-1
            AiModel(iwp)=Ai(Model(iwp))
            ichp=0
            if(Model(iwp).le.np-1) then
              call FeChybne(-1.,-1.,'Atom "'//AtM(iwp)(:idel(AtM(iwp)))
     1           //'" doesn''t exist in the molecule, try again.',' ',
     2             SeriousError)
              ichp=1
            endif
            if(ichp.eq.0) then
              do i=1,3
                if(Model(iwp).eq.Model(i).and.iwp.ne.i) then
                  call FeChybne(-1.,-1.,'duplicite occurence, '//
     1                          'try again.',' ',SeriousError)
                  ichp=1
                  exit
                endif
              enddo
            endif
          else
            Model(iwp)=0
          endif
        else
          Veta=EdwStringQuest(CheckNumber)
          if(EventTypeSave.eq.EventButton.and.
     1       (EventNumberSave.eq.nButtSelect.or.
     2        EventNumberSave.eq.nButtPoint)) go to 4100
          if(Veta.ne.' ') then
            if(Veta(1:1).ne.'%') then
              i=NAtCalc
              NAtCalc=NAtInd
              pom=YBottomMessage
              YBottomMessage=-1.
              call CtiAt(Veta,xx(1,iwp),ichp)
              YBottomMessage=pom
              NAtCalc=i
              if(ichp.eq.-1) then
                AtP(iwp)=Veta
                i=index(Veta,'#')-1
                if(i.lt.0) i=idel(Veta)
                k=KtAt(AtActual,NAtActual,Veta(:i))
                if(k.gt.0) then
                  AiActual(iwp)=ai(IAtActual(k))
                  Actual(iwp)=1
                endif
                ichp=0
              else if(ichp.eq.0) then
                AtP(iwp)=' '
                AiActual(iwp)=1.
                Actual(iwp)=1
              endif
            else
              ichp=1
              do i=1,NSavedPoints
                if(EqIgCase(StSavedPoint(i),Veta(2:))) then
                  AtP(iwp)=Veta
                  call CopyVek(XSavedPoint(1,i),xx(1,iwp),3)
                  AiActual(iwp)=1.
                  Actual(iwp)=1
                  ichp=0
                endif
              enddo
              if(ichp.ne.0) then
                Veta='the saved point with label "'//Veta(2:idel(Veta))
     1             //'" is not present, try again.'
                call FeChybne(-1.,-1.,Veta,' ',SeriousError)
              endif
            endif
            if(ichp.eq.0) then
              do i=1,3
                if(Actual(i).le.0) cycle
                if(iwp.ne.i.and.(EqIgCase(Veta,AtP(i)).or.
     1                           EqRV(xx(1,iwp),xx(1,i),3,.00001))) then
                  call FeChybne(-1.,-1.,'duplicite occurence, '//
     1                          'try again.',' ',SeriousError)
                  ichp=1
                  exit
                endif
              enddo
            endif
          else
            Actual(iwp)=0
            AtP(iwp)=' '
          endif
        endif
        if(ichp.ne.0) then
          AtP(iwp)=' '
          EventType=EventEdw
          EventNumber=iw
          ichp=0
        endif
        go to 4100
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelect)
     1  then
        if(iwm.eq.1) then
4120      call SelOneAtom('Select the model atom:',Atom(np),ia,iampp,
     1                    ichp)
          ia=np+ia-1
          if(ichp.eq.0) then
            do i=1,3
              if(i.eq.iwp) cycle
              if(ia.eq.Model(i)) then
                call FeChybne(-1.,-1.,'duplicite occurence, try again.',
     1                        ' ',SeriousError)
                go to 4120
              endif
            enddo
            AtM(iwp)=Atom(ia)
            call FeQuestStringEdwOpen(nEdwSelect,AtM(iwp))
            Model(iwp)=ia
            AiModel(iwp)=Ai(ia)
          endif
        else
4140      call SelOneAtom('Select the actual atom:',AtActual,ia,
     1                    NAtActual,ichp)
          ia=IAtActual(ia)
          AiActual(iwp)=ai(ia)
          if(ichp.eq.0) then
            do i=1,3
              if(i.eq.iwp) cycle
              if(EqIgCase(Atom(ia),AtP(i))) then
                call FeChybne(-1.,-1.,'duplicite occurence, try again.',
     1                        ' ',SeriousError)
                go to 4140
              endif
            enddo
            AtP(iwp)=Atom(ia)
            call ctiat(AtP(iwp),xx(1,iwp),ichp)
            Actual(iwp)=1
            call FeQuestStringEdwOpen(nEdwSelect,Atom(ia))
          endif
        endif
        EventType=EventEdw
        if(iw.lt.5) then
          EventNumber=iw+nEdwFirstPoint+1
        else if(iw.eq.5) then
          EventNumber=nEdwFirstPoint+1
        else
          EventNumber=nEdwMaxCoinc
        endif
        go to 4100
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPoint)
     1  then
        if(iwm.ne.1) then
          write(Cislo,'(i1,a2)') iwp,nty(iwp)
          call SelSavedPoint('Select the '//Cislo(:3)//
     1                       ' actual position:',nsel,paa)
          if(nsel.gt.0) then
            call CopyVek(paa,xx(1,iwp),3)
            Actual(iwp)=1
            AtP(iwp)='%'//StSavedPoint(nsel)(:idel(StSavedPoint(nsel)))
            call FeQuestStringEdwOpen(nEdwSelect,AtP(iwp))
          endif
        endif
        EventType=EventEdw
        if(iw.lt.5) then
          EventNumber=iw+nEdwFirstPoint+1
        else if(iw.eq.5) then
          EventNumber=nEdwFirstPoint+1
        else
          EventNumber=nEdwMaxCoinc
        endif
        go to 4100
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtCalc)
     1  then
        ShowCoinc=.false.
        Konec=0
        go to 4200
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtShowCoinc) then
        ShowCoinc=.true.
        Konec=0
        go to 4200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 4100
      endif
      go to 4300
4200  if(CrwLogicQuest(nCrwInversion)) then
        RotSign(ji)=-1
      else
        RotSign(ji)= 1
      endif
      do i=1,3
        k=model(i)
        if(i.eq.1) it=k-np+1
        call multm(TrMol(1,ji),x(1,k),xo(1,i),3,3,1)
        if(RotSign(ji).lt.0)
     1    call RealVectorToOpposite(xo(1,i),xo(1,i),3)
        call multm(trp,xx(1,i),xxo(1,i),3,3,1)
      enddo
      do i=1,3
        paa(i)=xxo(i,2)-xxo(i,1)
        pbb(i)=xxo(i,3)-xxo(i,1)
        paav(i)=xo(i,2)-xo(i,1)
        pbbv(i)=xo(i,3)-xo(i,1)
      enddo
      call VecOrtNorm(paa ,3)
      call VecOrtNorm(paav,3)
      do i=1,3
        pa(i)=paa(i)
        pb(i)=paav(i)
      enddo
      fs=-scalmul(paa,pbb)
      fsv=-scalmul(paav,pbbv)
      do i=1,3
        pbb(i)=pbb(i)+fs*paa(i)
        pbbv(i)=pbbv(i)+fsv*paav(i)
      enddo
      call VecOrtNorm(pbb ,3)
      call VecOrtNorm(pbbv,3)
      do i=1,3
        pa(i+3)=pbb(i)
        pb(i+3)=pbbv(i)
      enddo
      call vecmul(paa,pbb,pa(7))
      call VecOrtNorm(pa(7),3)
      call vecmul(paav,pbbv,pb(7))
      call VecOrtNorm(pb(7),3)
      call matinv(pb,pc,pom,3)
      call multm(pa,pc,RotMol(1,ji),3,3,3)
      call VecOrtNorm(RotMol(1,ji),3)
      call VecOrtNorm(RotMol(4,ji),3)
      call VecOrtNorm(RotMol(7,ji),3)
      call EM40GetAngles(RotMol(1,ji),irot(KPhase),euler(1,ji))
      k=0
      do i=np,nk
        k=k+1
        do j=1,3
          pbb(j)=x(j,i)-xm(j,nm)
        enddo
        call multm(TrMol(1,ji),pbb,paa,3,3,1)
        call multm(RotMol(1,ji),paa,pbb,3,3,1)
        call multm(TriMol(1,ji),pbb,paa,3,3,1)
        do j=1,3
          if(RotSign(ji).lt.0) paa(j)=-paa(j)
          xp(j,k)=paa(j)+xm(j,nm)
        enddo
      enddo
      do i=1,3
        trans(i,ji)=xx(i,1)-xp(i,it)
      enddo
      call FeQuestLblOn(nLblMolPar)
      write(Veta,102)(euler(i,ji),i=1,3),RotSign(ji)
      call FeQuestLblChange(nLblRot,Veta)
      write(Veta,103)(trans(i,ji),i=1,3)
      call FeQuestLblChange(nLblTrans,Veta)
      call FeQuestRealFromEdw(nEdwMaxCoinc,dco)
      k=0
      Ninfo=1
      TextInfo(1)=' '
      TextInfo(1)(23:)='Individual atomic positions'
      TextInfo(1)(57:)='distance'
      TextInfo(1)(69:)='to'
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      xpom=55.
      do i=1,4
        call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        if(i.lt.3) then
          xpom=xpom+55.
        else if(i.eq.3) then
          xpom=xpom+60.
        else
          xpom=xpom+30.
        endif
      enddo
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      ncoinc=0
      do i=np,nk
        k=k+1
        ip(k)=0
        do j=1,3
          xp(j,k)=xp(j,k)+trans(j,ji)
        enddo
        Ninfo=Ninfo+1
        write(Veta,'(a8,3(a1,f10.6))') Atom(i),(Tabulator,xp(j,k),j=1,3)
        call Zhusti(Veta)
        if(mamp.ne.1.or.k45) then
          j=koinc(xp(1,k),x,nap,nak,dco,dst,isw,iswa)
          jp=0
          if(j.gt.0) then
            if(isf(j).ne.0) then
              jp=j
              dstp=dst
            endif
          endif
4250      if(j.ne.0) then
            if(isf(j).gt.0) then
              if(ai(i).gt.0.) then
                isf(j)=-isf(j)-100
                ip(k)=j
              endif
            else
              if(j.lt.nak) then
                j=koinc(xp(1,k),x,j+1,nak,dco,dst,isw,iswa)
                go to 4250
              endif
            endif
            write(Veta(idel(Veta)+1:),105)
     1            Tabulator,dst,Tabulator,Atom(j)
            call Zhusti(Veta)
            ncoinc=ncoinc+1
          else
            if(jp.gt.0) then
              write(Veta(idel(Veta)+1:),105)
     1              Tabulator,dstp,Tabulator,Atom(jp)
              call Zhusti(Veta)
              ip(k)=jp
              ncoinc=ncoinc+1
            else
              Veta(idel(Veta)+1:)='     ----------------'
            endif
          endif
          TextInfo(Ninfo)=Veta
        endif
        if(Ninfo.ge.15) then
          if(Ninfo.gt.1.and.ShowCoinc) call FeInfoOut(-1.,-1.,' ','L')
          Ninfo=1
        endif
      enddo
      if(Ninfo.gt.1.and.ShowCoinc) call FeInfoOut(-1.,-1.,' ','L')
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      Veta='Coincidence ratio:'
      write(Cislo,104) ncoinc,iampp
      call Zhusti(Cislo)
      Veta(idel(Veta)+2:)=Cislo
      call FeQuestLblChange(nLblCoinc,Veta)
      if(EdwStateQuest(nEdwOcc).ne.EdwOpened) then
        AiMol(ji)=1.
        do i=1,3
          if(AiModel(i).ne.0.) then
            if(AiActual(i).ne.0.) then
              if(k45) then
                pa=0.
                call SpecPos(x(1,IAtActual(i)),pa,0,isw,.01,nocc)
                AiMol(ji)=AiActual(i)*float(nocc)
              else
                AiMol(ji)=AiActual(i)/AiModel(i)
              endif
              exit
            endif
          endif
        enddo
        AiPom=AiMol(ji)
        AiMol(ji)=AiMol(ji)*AiMax
        call FeQuestRealEdwOpen(nEdwOcc,aimol(ji),.false.,.false.)
      endif
      if(Konec.le.0) then
        k=0
        do i=np,nk
          k=k+1
          if(ip(k).gt.0) isf(ip(k))=-isf(ip(k))-100
        enddo
        go to 4100
      endif
4300  if(ich.eq.0) then
        if(Konec.ne.3) then
          call FeQuestRealFromEdw(nEdwOcc,aimol(ji))
          AiMax=aimol(ji)/aipom
        endif
      endif
4350  call FeQuestRemove(id)
      if(Konec.eq.3) then
        mamp=mamp-1
        if(mamp.ge.mampp) then
          Veta='Do you want discard'
          if(mamp.eq.mampp) then
            Veta(idel(Veta)+1:)=' new molecular position?'
          else
            Veta(idel(Veta)+1:)=' all new molecular positions?'
          endif
          if(FeYesNo(-1.,-1.,Veta,0)) then
            call iom40(0,0,fln(:ifln)//'.m40')
            go to 9999
          else
            go to 5000
          endif
        else
          call iom40(0,0,fln(:ifln)//'.m40')
          go to 9999
        endif
      endif
      call SetRealArrayTo(seuler(1,ji),3,0.)
      call SetRealArrayTo(strans(1,ji),3,0.)
      call SetIntArrayTo(KModM(1,ji),3,0)
      call SetIntArrayTo(KFM(1,ji),3,0)
      mam(nm)=mamp
      if(ji.eq.1) then
        PrvniKiMolekuly(ji)=1
      else
        if(mod(ji,mxp).eq.1) then
          jim=ji-mxp+mam(nm-1)-1
        else
          jim=ji-1
        endif
        PrvniKiMolekuly(ji)=PrvniKiMolekuly(jim)+DelkaKiMolekuly(jim)
      endif
      DelkaKiMolekuly(ji)=0
      call ShiftKiMol(ji,ktls(nm),KModM(1,ji),KModM(2,ji),
     1                KModM(3,ji),.true.)
      call SetIntArrayTo(KiMol(1,ji),DelkaKiMolekuly(ji),0)
      do i=np,nk
        call ShiftKiAt(i,itf(i),ifr(i),lasmax(i),KModA(1,i),MagPar(i),
     1                 .true.)
      enddo
      do i=nap,nak
        if(isf(i).lt.-100) isf(i)=isf(i)+100
      enddo
      if(Konec.eq.1) then
        Konec=0
        go to 4000
      endif
5000  if(mamp.le.0) go to 9900
      if(k45) then
        j=np-1
        do i=np,nk
          if(isf(i).gt.0) then
            j=j+1
            if(i.ne.j) call AtCopy(i,j)
          endif
        enddo
      endif
      k=0
      nma=NAtMolFr(1,1)
      do i=nak,nap,-1
        if(isf(i).lt.0) then
          do j=1,mam(nm)
            ji=(nm-1)*mxp+j
            if(AtTrans(ji).eq.Atom(i)) AtTrans(ji)=' '
          enddo
!   Skrtani atomu je sporne. Podminky, ktere platin pro puvodni atomy
!   mohou mit vyznam i pro molekuly. Napr. reference point. Sktrnout by
!   se mely jen ty co delaji restrikce mezi atomy molekuly a
!   individualnimi atomy.
          if(itf(i).gt.0) call CrlAtomNamesSave(Atom(i),'#delete#',1)
          call atsun(i+1,NAtInd-k,i)
          call atsun(nma,NAtAll-k,nma-1)
          nma=nma-1
          k=k+1
        endif
      enddo
      NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)-k
      NAtCalcBasic=NAtCalc
      call EM40UpdateAtomLimits
      call CrlAtomNamesApply
      call CrlAtomNamesIni
      ich=0
      call DeleteFile(fln(:ifln)//'_tmp.cif')
      go to 9999
9100  call FeReadError(45)
      go to 9800
9200  call FeChybne(-1.,YBottomMessage,'the file M45 doesn''t contain'//
     1              ' any atom.',' ',SeriousError)
9800  call CloseIfOpened(45)
      call FeQuestRemove(WizardId)
9900  ich=1
9999  if(allocated(xp)) deallocate(xp,ip)
      if(allocated(AtActual)) deallocate(AtActual,IAtActual)
      return
100   format(i2)
101   format(3f9.6)
102   format('Phi =',f8.2,' Chi =',f8.2,' Psi =',f8.2,' determinant =',
     1       i2)
103   format('Translation vector : ',3f10.6)
104   format(i5,'/',i5)
105   format(a1,f8.3,a1,a8)
      end
      subroutine EM40NewMolUpdateQuest(Void)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      common/EM40NewMolC/ nEdwFirstPoint,nEdwSelect,nButtSelect,nEdwOcc,
     1                    nButtCalc,nButtShowCoinc,Model(3),Actual(3),
     2                    nButtApplyNext,nButtApplyEnd,nButtPoint,Znovu
      character*80 Veta
      integer ButtonStateQuest,EdwStateQuest,Actual
      logical Znovu
      external Void
      save /EM40NewMolC/
      if(nButtCalc.le.0) go to 9999
      if(Znovu) go to 1100
      do i=1,3
        if(Model(i).le.0) go to 1100
        if(Actual(i).le.0) go to 1100
      enddo
      call FeQuestButtonOff(nButtCalc)
      call FeQuestButtonOff(nButtShowCoinc)
      if(EdwStateQuest(nEdwOcc).eq.EdwOpened) then
        call FeQuestButtonOff(nButtApplyNext)
        call FeQuestButtonOff(nButtApplyEnd)
      else
        call FeQuestButtonDisable(nButtApplyNext)
        call FeQuestButtonDisable(nButtApplyEnd)
      endif
      go to 1200
1100  call FeQuestButtonDisable(nButtCalc)
      call FeQuestButtonDisable(nButtApplyNext)
      call FeQuestButtonDisable(nButtApplyEnd)
1200  if(nButtSelect.le.0) go to 9999
      nEdwSelect=EdwActive-EdwFr+1
      i=nEdwSelect-nEdwFirstPoint+1
      j=mod(i,2)
      if(i.lt.1.or.i.gt.nEdwFirstPoint+5) then
        if(ButtonStateQuest(nButtSelect).ne.ButtonClosed) then
          call FeQuestButtonDisable(nButtSelect)
          go to 9999
        endif  
        if(ButtonStateQuest(nButtSelect).ne.ButtonClosed) then
          call FeQuestButtonDisable(nButtSelect)
          go to 9999
        endif  
      else if(j.eq.1) then
        Veta='%Select the model atom'
        call FeQuestButtonLabelChange(nButtSelect,Veta)
        call FeQuestButtonDisable(nButtPoint)
        go to 2000
      else if(j.eq.0) then
        Veta='%Select the actual atom'
        call FeQuestButtonLabelChange(nButtSelect,Veta)
        if(NSavedPoints.gt.0) then
          call FeQuestButtonOff(nButtPoint)
        else
          call FeQuestButtonDisable(nButtPoint)
        endif
        go to 2000
      endif
2000  if(ButtonStateQuest(nButtSelect).ne.ButtonOff)
     1   call FeQuestButtonOff(nButtSelect)
9999  return
      end
      subroutine EM40EditParameters(ParType,ParBlock)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'main.cmn'
      include 'datred.cmn'
      character*256 InputM40,InputM50
      integer ParType,ParBlock
      logical FileDiff,FeYesNo,Diff,DiffM95,PwdCheckTOFInD
      call PwdSetTOF(KDatBlock)
      if(ParType.eq.IdParamExt.or.ParType.eq.IdParamAnom.or.
     1   ParType.eq.IdParamScale) call RefOpenCommands
      if(ParBlock.ne.IdParamAtomsNew) then
        InputM40='jm40'
        call CreateTmpFile(InputM40,i,0)
        call FeTmpFilesAdd(InputM40)
        InputM50='jm50'
        call CreateTmpFile(InputM50,i,0)
        call FeTmpFilesAdd(InputM50)
        call CopyFile(fln(:ifln)//'.m40',InputM40)
        call CopyFile(fln(:ifln)//'.m50',InputM50)
        if(ParentStructure) then
          call SSG2QMag(fln)
          call iom40Only(0,0,fln(:ifln)//'.m40')
        else
          call iom40(0,0,fln(:ifln)//'.m40')
        endif
      endif
      if(Allocated(KWSym)) deallocate(KwSym)
      if(NDim(KPhase).gt.3) then
        allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
        call SetSymmWaves
      endif
      if(allocated(AtBrat)) deallocate(AtBrat,MolAtBrat,isfn,MolAt,
     1                                 MolAtisf)
      n=NAtAll
      allocate(AtBrat(n),MolAtBrat(n),isfn(n),MolAt(n),MolAtisf(n))
      ich=0
      ied=0
      DiffM95=.false.
      if(NAtXYZMode.gt.0) call TrXYZMode(1)
      if(NAtMagMode.gt.0) call TrMagMode(1)
      if(ParType.eq.IdParamOptions) then
        call EM40ParamOptions(ich)
      else if(ParType.eq.IdParamScale) then
        call EM40EditScales(ich)
      else if(ParType.eq.IdParamTwin) then
        call EM40EditTwVols(ich)
      else if(ParType.eq.IdParamExt) then
        call EM40Extinction(ich)
      else if(ParType.eq.IdParamAnom) then
        call EM40EditAnom(ich)
      else if(ParType.eq.IdParamPowder) then
        if(ExistM95.and..not.isTOF.and..not.isED)
     1    call iom95(1,fln(:ifln)//'.z95')
        call PwdOptions(ich)
      else if(ParType.eq.IdParamAtoms) then
        if(ParBlock.eq.IdParamAtomsNew) then
          call EM40NewAt(ich)
        else if(ParBlock.eq.IdParamAtomsEdit) then
          call EM40Atoms(ich)
        endif
      else if(ParType.eq.IdParamMolec) then
        if(ParBlock.eq.IdParamMolecNew) then
          call EM40NewMol(ich)
        else if(ParBlock.eq.IdParamMolecPosNew) then
          call EM40NewMolPos(ich)
        else if(ParBlock.eq.IdParamMolecTrans) then
          call EM40TransMol(ich)
        else if(ParBlock.eq.IdParamMolecExpand) then
          call EM40ExpandMol(ich)
        else if(ParBlock.eq.IdParamMolecEdit) then
          call EM40Molecules(ich)
        endif
      else if(ParType.eq.IdParamMagPolar) then
        call EM40DefMagPolar(ich)
      else if(ParType.eq.IdParamElDiff) then
        call EM40ElDiff(ich)
        ied=1
      endif
      if(ich.eq.0) then
        if(NAtXYZMode.gt.0) call TrXYZMode(0)
        if(NAtMagMode.gt.0) call TrMagMode(0)
        if(NMolec.gt.0.and.ISymmBasic.gt.0) then
          call CrlRestoreSymmetry(ISymmBasic)
          call CrlCleanSymmetry
        endif
        call iom40only(1,0,fln(:ifln)//'.m40')
!        if(ParentStructure) call QMag2SSG(Fln,0)
        call iom50(1,0,fln(:ifln)//'.m50')
        if(ExistPowder.and.ExistM41) call iom41(1,0,fln(:ifln)//'.m41')
        if(ParType.eq.IdParamExt.or.ParType.eq.IdParamAnom.or.
     1     ParType.eq.IdParamScale) then
          call RefRewriteCommands(1)
        else if(ParType.eq.IdParamPowder) then
          if(ExistM95.and..not.isTOF.and..not.isED) then
            LamAveRefBlock(KRefBlock)=LamAve(KDatBlock)
            LamA1RefBlock(KRefBlock)=LamA1(KDatBlock)
            LamA2RefBlock(KRefBlock)=LamA2(KDatBlock)
            NAlfaRefBlock(KRefBlock)=NAlfa(KDatBlock)
            LamRatRefBlock(KRefBlock)=LamRat(KDatBlock)
            PolarizationRefBlock(KRefBlock)=LpFactor(KDatBlock)
            AngleMonRefBlock(KRefBlock)=AngleMon(KDatBlock)
            AlphaGMonRefBlock(KRefBlock)=AlphaGMon(KDatBlock)
            BetaGMonRefBlock(KRefBlock)=BetaGMon(KDatBlock)
            FractPerfMonRefBlock(KRefBlock)=FractPerfMon(KDatBlock)
            RadiationRefBlock(KRefBlock)=Radiation(KDatBlock)
            if(ExistM95) then
              call iom95(1,fln(:ifln)//'.l95')
              DiffM95=FileDiff(fln(:ifln)//'.z95',fln(:ifln)//'.l95')
            else
              DiffM95=.false.
            endif
          endif
        else if(ParBlock.eq.IdParamAtomsNew) then
          go to 9999
        endif
      else
        if(ParBlock.eq.IdParamAtomsNew) go to 9000
      endif
      if(ied.eq.1) go to 9999
      call MoveFile(InputM40,PreviousM40)
      call MoveFile(InputM50,PreviousM50)
      call FeTmpFilesClear(InputM40)
      call FeTmpFilesClear(InputM50)
      Diff=FileDiff(fln(:ifln)//'.m40',PreviousM40).or.
     1     FileDiff(fln(:ifln)//'.m50',PreviousM50)
      if(.not.Diff.and.ExistPowder)
     1  Diff=Diff.or.FileDiff(fln(:ifln)//'.m41',PreviousM41)
      Diff=Diff.or.DiffM95
      if(ich.ne.0) then
        if(Diff) then
          if(.not.FeYesNo(-1.,-1.,'Do you want to discard made '//
     1                    'changes?',0)) go to 9100
        endif
      else
        if(Diff) then
          if(FeYesNo(-1.,-1.,'Do you want to rewrite changed files?',
     1               1)) go to 9100
        endif
      endif
9000  call CopyFile(PreviousM40,fln(:ifln)//'.m40')
      call DeleteFile(PreviousM40)
      call CopyFile(PreviousM50,fln(:ifln)//'.m50')
      call DeleteFile(PreviousM50)
      if(ExistPowder) then
        call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        call DeleteFile(PreviousM41)
      endif
      go to 9999
9100  if(ParType.eq.IdParamPowder.and.ExistM95.and.DiffM95) then
        call CompleteM95(0)
        call CompleteM90
        call iom90(0,fln(:ifln)//'.m90')
      endif
9999  if(allocated(AtBrat)) deallocate(AtBrat,MolAtBrat,isfn,MolAt,
     1                                 MolAtisf)
      call DeleteFile(fln(:ifln)//'.z95')
      call DeleteFile(fln(:ifln)//'.l95')
      return
      end
      subroutine EM40ParamOptions(ich)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,EdwStringQuest,FileName,CurrentDirO,Directory
      integer FeChdir
      logical CrwLogicQuest
      dimension px(9),py(9),pz(9)
      id=NextQuestId()
      xqd=500.
      il=9
      if(NDimI(KPhase).gt.0) il=il+2
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Options',0,LightGray,
     1                   0,OKForBasicFiles)
      il=1
      xpom=5.
      Veta='Atomic displacemt parameters:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      tpom=xpom+CrwgXd+10.
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='use %U'
        else
          Veta='use %beta'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) nCrwUseU=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,i-1.eq.lite(KPhase))
      enddo
      il=1
      xpom=xpom+xqd*.5
      Veta='Rotation angles for molecules:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      tpom=xpom+CrwgXd+10.
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='use a%xial angles'
        else
          Veta='use %Eulerian angles'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      2)
        if(i.eq.1) nCrwAxial=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,i.eq.2-irot(KPhase))
      enddo
      il=il+1
      xpom=5.
      tpom=xpom+CrwgXd+3
      Veta='Rounding procedure:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      do i=1,3
        il=il+1
        if(i.eq.1) then
          Veta='s.u.''s 2-19 for %Acta Cryst.'
        else if(i.eq.2) then
          nCrwRound=CrwLastMade
          Veta='s.u.''s 2-15 for some other crystallographic journals'
        else
          Veta='%one s.u. digit'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      3)
        call FeQuestCrwOpen(CrwLastMade,i.eq.RoundMethod)
      enddo
      il=il+1
      xpom=5.
      Veta='CIF options:'
      call FeQuestLblMake(id,xpom,il,Veta,'L','B')
      il=il+1
      Veta='%CIF specific file:'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=330.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwCIFSpecFile=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,CIFSpecificFile)
      xpom=xpom+dpom+10.
      Veta='Bro%wse'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(NDimI(KPhase).gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='De%fine wave vectors'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtDefWaves=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtDefWaves=0
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtDefWaves) then
        call EM40DefWaves(ich)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse)
     1  then
        CurrentDirO=CurrentDir
        Veta=EdwStringQuest(nEdwCIFSpecFile)
        call ExtractDirectory(Veta,Directory)
        call ExtractFileName(Veta,FileName)
        i=FeChdir(Directory)
        call FeGetCurrentDir
        call FeFileManager('Define specific CIF file',FileName,'*.dat',
     2                     0,.true.,ich)
        if(ich.eq.0) then
          if(index(FileName,DirectoryDelimitor).le.0)
     1      FileName=CurrentDir(:idel(CurrentDir))//
     1           FileName(:idel(FileName))
          call FeQuestStringEdwOpen(nEdwCIFSpecFile,FileName)
        endif
        i=FeChdir(CurrentDirO)
        call FeGetCurrentDir
        EventType=EventEdw
        EventNumber=nEdwCIFSpecFile
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        iroto=irot(KPhase)
        if(CrwLogicQuest(nCrwAxial)) then
          irot(KPhase)=1
        else
          irot(KPhase)=0
        endif
        if(iroto.ne.irot(KPhase)) then
          do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
            do j=1,mam(i)
              ji=j+(i-1)*mxp
              call matinv(TriMol(1,ji),py,pom,3)
              call matinv(TrMol(1,ji),pz,pom,3)
              call multm(py,RotMol(1,ji),px,3,3,3)
              call multm(px,pz,py,3,3,3)
              if(RotSign(ji).lt.0) call RealMatrixToOpposite(py,py,3)
              call EM40GetAngles(py,irot(KPhase),euler(1,ji))
            enddo
          enddo
        endif
        if(CrwLogicQuest(nCrwUseU)) then
          lite(KPhase)=0
        else
          lite(KPhase)=1
        endif
        nCrw=nCrwRound
        do i=1,3
          if(CrwLogicQuest(nCrw)) then
            RoundMethod=i
            exit
          endif
          nCrw=nCrw+1
        enddo
        CIFSpecificFile=EdwStringQuest(nEdwCIFSpecFile)
        if(index(CIFSpecificFile,DirectoryDelimitor).le.0)
     1    CIFSpecificFile=CurrentDir(:idel(CurrentDir))//
     2                    CIFSpecificFile(:idel(CIFSpecificFile))
        call FeInOutIni(1,HomeDir(:idel(HomeDir))//
     1                  MasterName(:idel(MasterName))//'.ini')
      endif
      call FeQuestRemove(id)
9999  return
      end
      subroutine EM40EditScales(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'refine.cmn'
      dimension tpoma(3),xpoma(3)
      character*80 t80
      character*12 at,pn
      logical Zpet
      KDatBlockIn=KDatBlock
1040  xqd=450.
      mxscs=36-NTwin+1
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      mxscuold=mxscu
      if(NDatBlock.gt.1) then
        dpomr=0.
        do i=1,NDatBlock
          MenuDatBlockUse(i)=1
          dpomr=max(dpomr,FeTxLength(MenuDatBlock(i)))
        enddo
        dpomr=dpomr+2.*EdwMarginSize+EdwYd
      endif
      if(isPowder) then
        n=1
        if(KManBackg(KDatBlock).gt.0) n=n+1
      else
        n=mxscs
      endif
      il=(n-1)/3+5
      if(NDatBlock.gt.1) il=il+1
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Edit scale parameters',0,
     1                   LightGray,0,OKForBasicFiles)
      Zpet=.false.
      il=1
      Cislo='TOverall'
      call FeMakeParEdwCrw(id,tpoma(1),xpoma(1),il,Cislo,nEdwOver,
     1                     nCrwOver)
      kip=MxSc+1
      call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
      call FeOpenParEdwCrw(nEdwOver,nCrwOver,pn,pom,KiS(kip,KDatBlock),
     1                     .false.)
      call FeMakeParEdwCrw(id,tpoma(2),xpoma(2),il,Cislo,nEdwLam2,
     1                     nCrwLam2)
      kip=MxSc+4
      call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
      call FeOpenParEdwCrw(nEdwLam2,nCrwLam2,pn,pom,KiS(kip,KDatBlock),
     1                     .false.)
      if(Lam2Corr.ne.1.or.Radiation(KDatBlock).ne.XRayRadiation.or.
     1   isPowder) then
        call FeQuestEdwDisable(nEdwLam2)
        call FeQuestCrwDisable(nCrwLam2)
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      t80='%Maximal number of scales'
      xpom=xqd*.5-20.
      tpom=xpom-8.
      dpom=50.
      call FeQuestEudMake(id,tpom,il,xpom,il,t80,'R',dpom,EdwYd,1)
      nEdwNumber=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,mxscu,.false.)
      call FeQuestEudOpen(EdwLastMade,7-itwph,mxsc-itwph,1,0.,0.,0.)
      if(isPowder) call FeQuestEdwDisable(EdwLastMade)
      xpom=xqd-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Previous')
      nButtPrevious=ButtonLastMade
      if(isPowder) call FeQuestButtonDisable(ButtonLastMade)
      il=4
      j=1
      do i=1,n
        k=0
        Cislo='Scale11'
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,Cislo,nEdw,nCrw)
        if(i.eq.1) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        endif
        if(mod(j,3).eq.0) then
          j=1
          il=il+1
        else
          j=j+1
        endif
      enddo
      if(n.le.2) il=il+1
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      if(isPowder) call FeQuestButtonDisable(ButtonLastMade)
      dpom=60.
      pom=20.
      xpom=(xqd-2.*dpom-pom)*.5
      do i=1,2
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
      if(NDatBlock.gt.1) then
        xpom=(xqd-dpomr)*.5
        tpom=xpom
        ilp=-10*il-3
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,' ','L',dpomr,
     1                          EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1    MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
      else
        nRolMenuDatBlock=0
      endif
      mp=1
1250  kip=mp
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      do i=1,n
        call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
        if(i.le.mxscu) then
          k=KiS(kip,KDatBlock)
        else
          k=0
        endif
        if(i.le.mxscu) then
          call FeOpenParEdwCrw(nEdw,nCrw,pn,pom,k,.false.)
        else
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
        nEdw=nEdw+1
        nCrw=nCrw+1
        kip=kip+1
      enddo
1450  if(.not.isPowder) then
        if(mxscu.gt.n) then
          call FeQuestButtonOpen(nButtPrevious,ButtonOff)
          call FeQuestButtonOpen(nButtNext,ButtonOff)
        else
          call FeQuestButtonClose(nButtPrevious)
          call FeQuestButtonClose(nButtNext)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNumber) then
        call FeQuestIntFromEdw(nEdwNumber,mxscu)
        if(mxscu.gt.mxscuold) then
          nn=mxscu-mxscuold
          do i=mxscuold+NTwin-1,mxscuold+1,-1
            KiS(i+nn,KDatBlock)=KiS(i,KDatBlock)
            KiS(i,KDatBlock)=0
          enddo
        else if(mxscu.lt.mxscuold) then
          nn=mxscuold-mxscu
          do i=mxscuold+1,mxscuold+NTwin-1
            KiS(i-nn,KDatBlock)=KiS(i,KDatBlock)
            KiS(i,KDatBlock)=0
          enddo
        endif
        mxscutw=mxscu+itwph-1
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        kip=mp
        do i=1,n
          if(kip.gt.mxscuold.and.kip.le.mxscu) then
            call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
            k=KiS(kip,KDatBlock)
            call FeOpenParEdwCrw(nEdw,nCrw,pn,pom,k,.false.)
          else if(kip.gt.mxscu) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          kip=kip+1
        enddo
        mxscuold=mxscu
        go to 1450
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          k=1
        else
          k=0
        endif
        call SetIntArrayTo(KiS,mxscu*MxDatBlock,k)
        nCrw=nCrwPrv
        kip=mp
        do i=1,36
          if(kip.le.mxscu) then
            call FeQuestCrwOpen(nCrw,k.eq.1)
          else
            go to 1450
          endif
          nCrw=nCrw+1
          kip=kip+1
        enddo
        go to 1450
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPrevious.or.
     1         CheckNumber.eq.nButtNext)) then
        mpp=mp
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        call FeUpdateParamAndKeys(nEdw,nCrw,sc(mp,KDatBlock),
     1                            KiS(mp,KDatBlock),mp+n)
        if(CheckNumber.eq.nButtNext) then
           i=mp+n
           if(i.lt.mxscu) then
             mp=i
           else
             go to 1500
           endif
        else if(CheckNumber.eq.nButtPrevious) then
           i=mp-n
           if(i.gt.0) then
             mp=i
           else
             go to 1500
           endif
        endif
        go to 1250
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelected(nRolMenuDatBlock)
        if(KDatBlock.ne.KDatBlockNew) then
          Zpet=.true.
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        kip=MxSc+1
        call FeUpdateParamAndKeys(nEdwOver,nCrwOver,OverAllB(KDatBlock),
     1                            KiS(kip,KDatBlock),1)
        if(Lam2Corr.eq.1.and.Radiation(KDatBlock).eq.XRayRadiation.and.
     1     .not.isPowder) then
          kip=MxSc+4
          call FeUpdateParamAndKeys(nEdwLam2,nCrwLam2,ScLam2(KDatBlock),
     1                              KiS(kip,KDatBlock),1)
        endif
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        call FeUpdateParamAndKeys(nEdw,nCrw,sc(mp,KDatBlock),
     1                            KiS(mp,KDatBlock),mp+MxScU-1)
        call iom40(1,0,fln(:ifln)//'.m40')
      endif
      call FeQuestRemove(id)
      if(Zpet) then
        KDatBlock=KDatBlockNew
        go to 1040
      endif
      KDatBlock=KDatBlockIn
      return
      end
      subroutine EM40EditTwVols(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(3),xpoma(3)
      character*80 Veta
      character*12 at,pn
      integer TwShow,UseTabsIn
      logical lpom,Zpet
      KDatBlockIn=KDatBlock
1040  xqd=450.
      il=3+(itwph-2)/3
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      id=NextQuestId()
      if(isPowder) then
        Veta='Edit phase fractions'
      else
        Veta='Edit twin fractions'
      endif
      if(NDatBlock.gt.1) then
        dpomr=0.
        do i=1,NDatBlock
          MenuDatBlockUse(i)=1
          dpomr=max(dpomr,FeTxLength(MenuDatBlock(i)))
        enddo
        dpomr=dpomr+2.*EdwMarginSize+EdwYd
        il=il+1
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,
     1                   OKForBasicFiles)
      Zpet=.false.
      il=1
      kip=mxscu+1
      j=1
      do i=2,itwph
        call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiS(kip,KDatBlock),
     1                       .false.)
        EdwCheck(nEdw+EdwFr-1)=1
        if(i.eq.2) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        endif
        if(mod(j,3).eq.0) then
          j=1
          if(i.ne.itwph) il=il+1
        else
          j=j+1
        endif
        kip=kip+1
      enddo
      il=-10*(il+1)-3
      dpom=60.
      pom=20.
      xpom=(xqd-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        else if(i.eq.3) then
          at='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      il=il-10
      Veta='Show %twinning matrix'
      dpom=FeTxLength(Veta)+10.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtShowTwinMatrix=ButtonLastMade
      if(isPowder) call FeQuestButtonDisable(ButtonLastMade)
      if(NDatBlock.gt.1) then
        xpom=(xqd-dpomr)*.5
        tpom=xpom
        ilp=il-10
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,' ','L',dpomr,
     1                          EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1    MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
      else
        nRolMenuDatBlock=0
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=2,itwph
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        dpom=1./float(itwph)
        do i=2,itwph
          call FeQuestRealEdwOpen(nEdw,dpom,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventEdw) then
        TwShow=CheckNumber-nEdwPrv+2
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtShowTwinMatrix) then
        UseTabsIn=UseTabs
        UseTabs=NextTabs()
        m=0
        do i=1,3
          TextInfo(i)=Indices(i)//'='
          if(i.eq.1) pomt=FeTxLength(TextInfo(i))
          do j=1,3
            m=m+1
            pom=Rtw(j+(i-1)*3,TwShow)
            if(i.eq.1) then
              if(j.eq.1) pomc=FeTxLength('XXXXXXX*X')
              pomt=pomt+pomc
              call FeTabsAdd(pomt,0,IdLeftTab,' ')
            endif
            write(At,'(f7.3,''*'',a1)') pom,Indices(j)
            call Velka(At)
            if(pom.ge.0..and.j.ne.1) then
              l=1
1600          if(At(l:l).eq.' ') then
                l=l+1
                go to 1600
              endif
              l=l-1
              if(l.gt.0) At(l:l)='+'
            endif
            TextInfo(i)=TextInfo(i)(:idel(TextInfo(i)))//
     1                  Tabulator//At(:idel(At))
          enddo
        enddo
        NInfo=3
        write(Cislo,'(''matrix#'',i2)') TwShow
        call Zhusti(Cislo)
        Veta='Twinning '//Cislo(:idel(Cislo))
        call FeInfoOut(-1.,-1.,Veta,'L')
        call FeTabsReset(UseTabs)
        UseTabsIn=UseTabs
        EventType=EventEdw
        EventNumber=TwShow-2+nEdwPrv
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelected(nRolMenuDatBlock)
        if(KDatBlock.ne.KDatBlockNew) then
          Zpet=.true.
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        kip=mxscu+1
        call FeUpdateParamAndKeys(nEdw,nCrw,sctw(2,KDatBlock),
     1                            KiS(kip,KDatBlock),itwph-1)
        call iom40(1,0,fln(:ifln)//'.m40')
      endif
      call FeQuestRemove(id)
      if(Zpet) then
        KDatBlock=KDatBlockNew
        go to 1040
      endif
      KDatBlock=KDatBlockIn
      return
      end
      subroutine EM40Extinction(ich)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension tpoma(3),xpoma(3)
      character*12 at,pn
      character*80 Veta
      dimension ECOld(6,2),ECPom(12)
      integer EdwStateQuest,ExtTypeOld,ExtTensorOld(2),KiPom(12),
     1        CrwStateQuest
      logical CrwLogicQuest,lpom,Zpet
      ECOld=0.
      ECOld(1,1)=.01
      ECOld(1,2)=.01
      ExtTensorOld=0
      if(ExtTensor(KDatBlock).eq.1) then
        if(ExtType(KDatBlock).eq.1) then
          ECOld(1,2)=ec(7,KDatBlock)
          ExtTensorOld(2)=1
        else if(ExtType(KDatBlock).eq.2) then
          ECOld(1,1)=ec(1,KDatBlock)
          ExtTensorOld(1)=1
        else
          ECOld(1,2)=ec(7,KDatBlock)
          ECOld(1,1)=ec(1,KDatBlock)
          ExtTensorOld(1)=1
          ExtTensorOld(2)=1
        endif
      else if(ExtTensor(KDatBlock).eq.2) then
        if(ExtType(KDatBlock).eq.1) then
          call CopyVek(ec(7,KDatBlock),ECOld(1,2),6)
          ExtTensorOld(2)=2
        else if(ExtType(KDatBlock).eq.2) then
          call CopyVek(ec(1,KDatBlock),ECOld(1,1),6)
          ExtTensorOld(1)=2
        endif
      endif
      KDatBlockIn=KDatBlock
      xqd=450.
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      n=0
      if(NDatBlock.gt.1) then
        dpomr=0.
        KDatB=0
        do i=1,NDatBlock
          if(DataType(i).eq.2) then
            MenuDatBlockUse(i)=0
          else
            if(KDatB.le.0) KDatB=i
            MenuDatBlockUse(i)=1
            n=n+1
          endif
          dpomr=max(dpomr,FeTxLength(MenuDatBlock(i)))
        enddo
        dpomr=dpomr+2.*EdwMarginSize+EdwYd
      endif
      if(DataType(KDatBlock).ne.1) KDatBlock=KDatB
      if(n.gt.1) then
        il=11
      else
        il=10
      endif
      if(MaxMagneticType.gt.0) il=il+3
1040  id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Extinction model:',
     2                   0,LightGray,0,OKForBasicFiles)
      Zpet=.false.
      xpom=65.
      Veta='%None'
      do i=1,3
        call FeQuestCrwMake(id,5.,i,xpom,i,Veta,'L',CrwgXd,CrwgYd,1,1)
        call FeQuestCrwOpen(CrwLastMade,i-1.eq.ExtTensor(KDatBlock))
        if(i.eq.1) then
          nCrwExtFirst=CrwLastMade
          Veta='%Isotropic'
        else
          Veta='%Anisotropic'
        endif
      enddo
      nCrwExtLast=CrwLastMade
      tpom=xpom+80.
      Veta='Type %1'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      do i=1,3
        call FeQuestCrwMake(id,tpom,i,xpom,i,Veta,'L',CrwgXd,CrwgYd,1,
     1                      2)
        call FeQuestCrwOpen(CrwLastMade,i.eq.ExtType(KDatBlock))
        if(i.eq.1) then
          Veta='Type %2'
          nCrwType1=CrwLastMade
        else if(i.eq.2) then
          Veta='%Mixed'
          nCrwType2=CrwLastMade
        else
          nCrwMixed=CrwLastMade
        endif
      enddo
      tpom=xpom+80.
      xpom=tpom+55.
      il=1
      Veta='%Gaussian'
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      3)
        call FeQuestCrwOpen(CrwLastMade,i.eq.ExtDistr(KDatBlock))
        if(i.eq.1) then
          nCrwGauss=CrwLastMade
          Veta='%Lorentzian'
        else if(i.eq.2) then
          nCrwLorentz=CrwLastMade
        endif
        il=il+1
      enddo
      il=il+1
      tpom=5.
      Veta='Ra%dius [cm]'
      xpom=FeTxLengthUnder(Veta)+15.
      dpom=80.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwRadius=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,ExtRadius(KDatBlock),
     1                        .false.,.false.)
      tpom=xpom+dpom+10.
      Veta='used only if tbar not present on M90 file'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Extinction parameters:'
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      il=il+1
      kip=MxSc+7
      j=1
      do i=1,6
        call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,' ',nEdw,nCrw)
        if(i.eq.1) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        endif
        if(mod(j,3).eq.0) then
          j=1
          if(i.ne.6) il=il+1
        else
          j=j+1
        endif
        kip=kip+1
      enddo
      if(MaxMagneticType.gt.0) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        Veta='Correction factors for magnetic reflections:'
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
        kip=MxSc+2
        il=il+1
        j=1
        do i=1,2
          call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
          call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
          if(i.eq.1) then
            nEdwPrvMag=nEdw
            nCrwPrvMag=nCrw
          endif
          kip=kip+1
          j=j+1
        enddo
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      dpom=60.
      pom=20.
      xpom=(xqd-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        else if(i.eq.3) then
          at='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,60.,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
      if(n.gt.1) then
        xpom=(xqd-dpomr)*.5
        tpom=xpom
        ilp=-10*il-3
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,' ','L',dpomr,
     1                          EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1    MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
      else
        nRolMenuDatBlock=0
      endif
      ExtTypeOld=-1
c      if(ExtType(KDatBlock).eq.1) then
c        ec(1,KDatBlock)=.01
c      else if(ExtType(KDatBlock).eq.2) then
c        ec(7,KDatBlock)=.01
c      endif
1500  nCrw=nCrwExtFirst
      do i=1,3
        if(ExtType(KDatBlock).eq.3.and.i.eq.3) then
          call FeQuestCrwDisable(nCrw)
        else
          call FeQuestCrwOpen(nCrw,i-1.eq.ExtTensor(KDatBlock))
        endif
        nCrw=nCrw+1
      enddo
      if(ExtType(KDatBlock).eq.1) then
        iec=7
        iecp=2
      else
        iec=1
        iecp=1
      endif
      nCrw=nCrwType1
      do i=1,3
        if(ExtTensor(KDatBlock).eq.0.or.
     1     (ExtTensor(KDatBlock).eq.2.and.i.eq.3)) then
          call FeQuestCrwDisable(nCrw)
        else
          call FeQuestCrwOpen(nCrw,i.eq.ExtType(KDatBlock))
        endif
        nCrw=nCrw+1
      enddo
1600  if((ExtType(KDatBlock).eq.1.or.ExtType(KDatBlock).eq.3).and.
     1    .not.CrwLogicQuest(nCrwExtFirst)) then
        nCrw=nCrwGauss
        do i=1,2
          call FeQuestCrwOpen(nCrw,i.eq.ExtDistr(KDatBlock))
          nCrw=nCrw+1
        enddo
      else
        do i=nCrwGauss,nCrwLorentz
          if(CrwStateQuest(i).ne.CrwClosed.and.
     1       CrwStateQuest(i).ne.CrwDisabled) then
            if(CrwLogicQuest(i)) ExtDistr(KDatBlock)=i-nCrwGauss+1
          endif
          call FeQuestCrwDisable(i)
        enddo
      endif
      if(ExtTypeOld.gt.0) then
        if(ExtTypeOld.eq.2.or.ExtTypeOld.eq.3) then
          j=1
        else if(ExtTypeOld.eq.1) then
          j=2
        else
          j=0
        endif
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        do i=1,6
          if(EdwStateQuest(nEdw).eq.EdwOpened.and.j.ne.0) then
            if(ExtTypeOld.ne.ExtType(KDatBlock)) then
              call FeQuestRealFromEdw(nEdw,ECOld(i,j))
              if(ExtTypeOld.eq.3) j=2
            endif
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
      if(ExtType(KDatBlock).eq.1) then
        kip=MxSc+13
        iec=7
        iecp=2
      else
        kip=MxSc+7
        iec=1
        iecp=1
      endif
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      if(ExtTensor(KDatBlock).eq.0) then
        imx=0
      else if(ExtTensor(KDatBlock).eq.1) then
        imx=1
      else if(ExtTensor(KDatBlock).eq.2) then
        imx=6
      endif
      if(ExtType(KDatBlock).eq.3.and.ExtTensor(KDatBlock).gt.0) imx=2
      k=0
      do i=1,imx
        k=k+1
        call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
        call FeQuestEdwLabelChange(nEdw,pn)
        call FeQuestRealEdwOpen(nEdw,ECOld(k,iecp),.false.,.false.)
        call FeQuestCrwOpen(nCrw,.true.)
        if(imx.ne.2) then
          kip=kip+1
        else
          kip=kip+6
          k=0
          iec=7
          iecp=2
        endif
        nEdw=nEdw+1
        nCrw=nCrw+1
      enddo
      if(MaxMagneticType.gt.0) then
        if(ExtTypeOld.gt.0) then
          if(ExtTypeOld.eq.1) then
            kip=MxSc+3
          else
            kip=MxSc+2
          endif
          nEdw=nEdwPrvMag
          nCrw=nCrwPrvMag
          do i=1,2
            if(EdwStateQuest(nEdw).eq.EdwOpened) then
              call FeQuestRealFromEdw(nEdw,pom)
              if(CrwLogicQuest(nCrw)) then
                KiS(kip,KDatBlock)=1
              else
                KiS(kip,KDatBlock)=0
              endif
              if(ExtTypeOld.eq.1) then
                ecMag(2,KDatBlock)=pom
              else if(ExtTypeOld.eq.2) then
                ecMag(1,KDatBlock)=pom
              else
                ecMag(i,KDatBlock)=pom
              endif
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            kip=kip+1
          enddo
        endif
        if(ExtType(KDatBlock).eq.1) then
          kip=MxSc+3
        else
          kip=MxSc+2
        endif
        if(ExtType(KDatBlock).eq.0) then
          imxm=0
        else
        endif
        if(ExtTensor(KDatBlock).eq.0) then
          imxm=0
        else
          if(ExtType(KDatBlock).eq.3) then
            imxm=2
          else
            imxm=1
          endif
        endif
        nEdw=nEdwPrvMag
        nCrw=nCrwPrvMag
        do i=1,imxm
          call kdoco(kip+(KDatBlock-1)*MxScAll,at,pn,1,pom,spom)
          call FeQuestEdwLabelChange(nEdw,pn)
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          call FeQuestCrwOpen(nCrw,KiS(kip,KDatBlock).gt.0)
          nEdw=nEdw+1
          nCrw=nCrw+1
          kip=kip+1
        enddo
        call FeReleaseOutput
        call FeDeferOutput
      endif
      ExtTypeOld=ExtType(KDatBlock)
      if(ExtTypeOld.eq.1) then
        ExtTensorOld(2)=ExtTensor(KDatBlock)
      else if(ExtTypeOld.eq.2) then
        ExtTensorOld(1)=ExtTensor(KDatBlock)
      endif
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.le.nCrwExtLast) then
        if(ExtTypeOld.eq.1) then
          ExtTensorOld(2)=ExtTensor(KDatBlock)
        else if(ExtTypeOld.eq.2) then
          ExtTensorOld(1)=ExtTensor(KDatBlock)
        endif
        do i=nCrwExtFirst,nCrwExtLast
          if(CrwLogicQuest(i)) then
            ExtTensor(KDatBlock)=i-nCrwExtFirst
            exit
          endif
        enddo
        if(ExtTensor(KDatBlock).eq.0) then
          go to 1500
        else if(ExtTensor(KDatBlock).eq.1) then
          if(ExtTensorOld(iecp).le.1) then
            go to 1500
          else
            go to 3000
          endif
        else if(ExtTensor(KDatBlock).eq.2) then
          if(ExtTensorOld(iecp).eq.0) then
            go to 3100
          else if(ExtTensorOld(iecp).eq.1) then
            call FeQuestRealFromEdw(nEdwPrv,ECOld(1,iecp))
            go to 3100
          else if(ExtTensorOld(iecp).eq.2) then
            go to 1500
          endif
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.le.nCrwMixed)
     1  then
        do i=nCrwType1,nCrwMixed
          if(CrwLogicQuest(i)) then
            ExtType(KDatBlock)=i-nCrwType1+1
            exit
          endif
        enddo
        if(ExtType(KDatBlock).eq.1) then
          iec=7
          iecp=2
        else if(ExtType(KDatBlock).eq.2) then
          iec=1
          iecp=1
        else if(ExtType(KDatBlock).eq.3) then
          iec=8-iec
          iecp=3-iecp
        endif
        if(max(ExtTensorOld(iecp),1).eq.ExtTensor(KDatBlock)) then
          go to 1500
        else
          if(ExtTensorOld(iecp).le.1) then
            go to 3100
          else
            go to 3020
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.le.nCrwLorentz) then
        do i=nCrwGauss,nCrwLorentz
          if(CrwLogicQuest(i)) then
            ExtDistr(KDatBlock)=i-nCrwGauss+1
            exit
          endif
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,imx
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        nCrw=nCrwPrvMag
        do i=1,imxm
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 2500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        ECOld=0.
        if(ExtTensor(KDatBlock).eq.1) then
          ECOld=0.
          ec(iec,KDatBlock)=0.01
          if(ExtType(KDatBlock).eq.3) ec(8-iec,KDatBlock)=.01
        else if(ExtTensor(KDatBlock).eq.2) then
          pom=0.0001
          if(ExtType(KDatBlock).eq.2) pom=1./pom
          call SetRealArrayTo(ec(iec,KDatBlock),3,pom)
          call SetRealArrayTo(ec(iec+3,KDatBlock),3,0.)
        endif
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelected(nRolMenuDatBlock)
        if(KDatBlock.ne.KDatBlockNew) then
          Zpet=.true.
        else
          go to 2500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
      go to 3200
3000  nEdw=nEdwPrv
      do i=1,6
        call FeQuestRealFromEdw(nEdw,ECOld(i,iecp))
        nEdw=nEdw+1
      enddo
3020  det=ECOld(1,iecp)*ECOld(2,iecp)*ECOld(3,iecp)+
     1    2.*ECOld(4,iecp)*ECOld(5,iecp)*ECOld(6,iecp)-
     2    ECOld(1,iecp)*ECOld(6,iecp)**2-
     3    ECOld(2,iecp)*ECOld(5,iecp)**2-
     4    ECOld(3,iecp)*ECOld(4,iecp)**2
      if(det.gt.0.) then
        ECOld(1,iecp)=.001*(CellVol(1,KPhase)**2/det)**(.1666667)
      else
        ECOld(1,iecp)=0.01
      endif
      ExtTensorOld(iecp)=1
3050  call SetRealArrayTo(ECOld(2,iecp),5,0.)
      call SetIntArrayTo(KiS(MxSc+7,KDatBlock),12,0)
      KiS(MxSc+iec+6,KDatBlock)=1
      go to 1500
3100  pom=1./(ECOld(1,iecp)*1000.)**2
      ECOld(1,iecp)=MetTens(1,1,KPhase)*pom
      ECOld(2,iecp)=MetTens(5,1,KPhase)*pom
      ECOld(3,iecp)=MetTens(9,1,KPhase)*pom
      ECOld(4,iecp)=MetTens(2,1,KPhase)*pom
      ECOld(5,iecp)=MetTens(3,1,KPhase)*pom
      ECOld(6,iecp)=MetTens(6,1,KPhase)*pom
      ExtTensorOld(iecp)=2
      go to 1500
3200  if(ich.eq.0) then
        do i=nCrwGauss,nCrwLorentz
          if(CrwLogicQuest(i)) then
            ExtDistr(KDatBlock)=i-nCrwGauss+1
            exit
          endif
        enddo
3250    call FeQuestRealFromEdw(nEdwRadius,ExtRadius(KDatBlock))
        call SetRealArrayTo(ec(1,KDatBlock),12,0.)
        call SetIntArrayTo(KiS(MxSc+7,KDatBlock),12,0)
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,EcPom,KiPom,imx)
        if(ExtTensor(KDatBlock).eq.1) then
          if(ExtType(KDatBlock).eq.1) then
            ec(7,KDatBlock)=EcPom(1)
            KiS(MxSc+13,KDatBlock)=KiPom(1)
          else if(ExtType(KDatBlock).eq.2) then
            ec(1,KDatBlock)=EcPom(1)
            KiS(MxSc+7 ,KDatBlock)=KiPom(1)
          else if(ExtType(KDatBlock).eq.3) then
            ec(1,KDatBlock)=EcPom(1)
            ec(7,KDatBlock)=EcPom(2)
            KiS(MxSc+7 ,KDatBlock)=KiPom(1)
            KiS(MxSc+13,KDatBlock)=KiPom(2)
          endif
        else if(ExtTensor(KDatBlock).eq.2) then
          if(ExtType(KDatBlock).eq.1) then
            ec(7:12,KDatBlock)=EcPom(1:6)
            KiS(MxSc+13:MxSc+18,KDatBlock)=KiPom(1:6)
          else if(ExtType(KDatBlock).eq.2) then
            ec(1:6,KDatBlock)=EcPom(1:6)
            KiS(MxSc+7:MxSc+12,KDatBlock)=KiPom(1:6)
          endif
        endif
        if(MaxMagneticType.gt.0) then
          call SetRealArrayTo(ecMag(1,KDatBlock),2,1.)
          call SetIntArrayTo(KiS(MxSc+2,KDatBlock),2,0)
          if(ExtType(KDatBlock).eq.1) then
            j=2
            kip=MxSc+3
          else if(ExtType(KDatBlock).eq.2.or.ExtType(KDatBlock).eq.3)
     1      then
            j=1
            kip=MxSc+2
          endif
          call FeUpdateParamAndKeys(nEdwPrvMag,nCrwPrvMag,
     1         ecMag(j,KDatBlock),KiS(kip,KDatBlock),imxm)
        endif
      endif
      call FeQuestRemove(id)
      if(Zpet) then
        KDatBlock=KDatBlockNew
        go to 1040
      endif
      KDatBlock=KDatBlockIn
      return
      end
      subroutine EM40EditAnom(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xp(1),kip(1)
      character*80 Veta
      character*12 at
      integer RolMenuSelectedQuest
      logical lpom,Zpet
      KDatBlockIn=KDatBlock
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      if(NDatBlock.gt.1) then
        dpomr=0.
        do i=1,NDatBlock
          MenuDatBlockUse(i)=1
          dpomr=max(dpomr,FeTxLength(MenuDatBlock(i)))
        enddo
        dpomr=dpomr+2.*EdwMarginSize+EdwYd
      endif
      if(kanref(KDatBlock).eq.0)
     1  call SetIntArrayTo(KiS(MxSc+19,KDatBlock),2*MaxNAtFormula,0)
1040  xqd=500.
      il=(NAtFormula(KPhase)-1)/2+4
      if(NDatBlock.gt.1) il=il+1
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,
     1                   OKForBasicFiles)
      Zpet=.false.
      Veta='%Allow f'',f" refinement'
      xpom=(xqd+FeTxLength(Veta))*.5+10.
      il=1
      call FeQuestCrwMake(id,xqd*.5,il,xpom,il,Veta,'C',CrwXd,CrwYd,1,0)
      nCrwAllow=CrwLastMade
      call FeQuestCrwOpen(nCrwAllow,kanref(KDatBlock).ne.0)
      il=il+1
      ip=MxSc+18+(KPhase-1)*2*MaxNAtFormula
      do i=1,NAtFormula(KPhase)
        ip=ip+1
        if(mod(i,2).eq.1) then
          xpom=25.
          il=il+1
        else
          xpom=xqd*.5+25.
        endif
        tpom=xpom-5.
        call FeQuestLblMake(id,xpom+30.,il-1,'f''','C','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblFirst=LblLastMade
        call FeMakeParEdwCrw(id,tpom,xpom,il,AtTypeMenu,nEdw,nCrw)
        if(KAnRef(KDatBlock).le.0) then
          pom=FFra(i,KPhase,KDatBlock)
        else
          pom=FFrRef(i,KPhase,KDatBlock)
        endif
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiS(ip,KDatBlock),
     1                       .false.)
        if(i.eq.1) then
          nCrwPrv=nCrw
          nEdwPrv=nEdw
        endif
        xpom=xpom+120.
        call FeQuestLblMake(id,xpom+30.,il-1,'f"','C','N')
        call FeQuestLblOff(LblLastMade)
        call FeMakeParEdwCrw(id,tpom,xpom,il,AtTypeMenu(i),nEdw,nCrw)
        if(KAnRef(KDatBlock).le.0) then
          pom=FFia(i,KPhase,KDatBlock)
        else
          pom=FFiRef(i,KPhase,KDatBlock)
        endif
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,
     1                       KiS(ip+MaxNAtFormula,KDatBlock),.false.)
      enddo
      il=il+1
      dpom=60.
      pom=20.
      xpom=(xqd-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        else if(i.eq.3) then
          at='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
      if(NDatBlock.gt.1) then
        xpom=(xqd-dpomr)*.5
        tpom=xpom
        il=il+1
        ilp=-10*il-3
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,' ','L',dpomr,
     1                          EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1    MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
      else
        nRolMenuDatBlock=0
      endif
1200  nLbl=nLblFirst
      do i=1,2
        if(kanref(KDatBlock).eq.1) then
          call FeQuestLblOn(nLbl)
        else
          call FeQuestLblOff(nLbl)
        endif
        nLbl=nLbl+1
        if(kanref(KDatBlock).eq.1) then
          call FeQuestLblOn(nLbl)
        else
          call FeQuestLblOff(nLbl)
        endif
        nLbl=nLbl+1
      enddo
      nedw=nEdwPrv
      ncrw=nCrwPrv
      j=0
      ip=MxSc+18+(KPhase-1)*2*MaxNAtFormula
      do i=1,2*NAtFormula(KPhase)
        if(kanref(KDatBlock).eq.1) then
          if(mod(i,2).eq.0) then
            pom=FFiRef(j,KPhase,KDatBlock)
            k=k+MaxNAtFormula
          else
            j=j+1
            k=ip+j
            pom=FFrRef(j,KPhase,KDatBlock)
          endif
          call FeQuestRealEdwOpen(nedw,pom,.false.,.false.)
          call FeQuestCrwOpen(ncrw,KiS(k,KDatBlock).ne.0)
        else
          call FeQuestEdwClose(nedw)
          call FeQuestCrwClose(ncrw)
        endif
        nedw=nedw+1
        ncrw=ncrw+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAllow) then
        k=kanref(KDatBlock)
        n=0
        do KDatB=1,NDatBlock
          n=n+kanref(KDatB)
        enddo
        if(n.le.0) then
          if(.not.allocated(FFrRef))
     1      allocate( FFrRef(MaxNAtFormula,NPhase,NDatBlock),
     2                FFiRef(MaxNAtFormula,NPhase,NDatBlock),
     3               sFFrRef(MaxNAtFormula,NPhase,NDatBlock),
     4               sFFiRef(MaxNAtFormula,NPhase,NDatBlock))
        endif
        kanref(KDatBlock)=1-kanref(KDatBlock)
        if(kanref(KDatBlock).eq.0) then
          do KPh=1,NPhase
            call CopyVek(FFrRef(1,KPh,KDatBlock),FFra(1,KPh,KDatBlock),
     1                   NAtFormula(KPh))
            call CopyVek(FFiRef(1,KPh,KDatBlock),FFia(1,KPh,KDatBlock),
     1                   NAtFormula(KPh))
          enddo
        else
          do KPh=1,NPhase
            call CopyVek(FFra(1,KPh,KDatBlock),FFrRef(1,KPh,KDatBlock),
     1                   NAtFormula(KPh))
            call CopyVek(FFia(1,KPh,KDatBlock),FFiRef(1,KPh,KDatBlock),
     1                   NAtFormula(KPh))
            call SetRealArrayTo(sFFrRef(1,KPh,KDatBlock),
     1                          NAtFormula(KPh),0)
            call SetRealArrayTo(sFFiRef(1,KPh,KDatBlock),
     1                          NAtFormula(KPh),0)
          enddo
        endif
        go to 1200
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,2*NAtFormula(KPhase)
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        go to 1200
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelectedQuest(nRolMenuDatBlock)
        if(KDatBlock.ne.KDatBlockNew) then
          Zpet=.true.
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nedw=nEdwPrv
        ncrw=nCrwPrv
        if(kanref(KDatBlock).eq.1) then
          do i=1,2*NAtFormula(KPhase)
            j=(i+1)/2
            k=mod(i-1,2)+1
            call FeUpdateParamAndKeys(nEdw,nCrw,xp,kip,1)
            if(k.eq.1) then
              FFrRef(j,KPhase,KDatBlock)=xp(1)
            else
              FFiRef(j,KPhase,KDatBlock)=xp(1)
            endif
            ip=MxSc+18+j+(k-1)*MaxNAtFormula+(KPhase-1)*2*MaxNAtFormula
            KiS(ip,KDatBlock)=kip(1)
            nedw=nedw+1
            ncrw=ncrw+1
          enddo
        endif
      endif
      call FeQuestRemove(id)
      if(Zpet) then
        KDatBlock=KDatBlockNew
        go to 1040
      endif
      KDatBlock=KDatBlockIn
      return
      end
      subroutine EM40ElDiff(ich)
      use EDZones_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real tpoma(4),xpoma(4),xp(6),pomc(6)
      character*256 EdwStringQuest
      character*80 Veta,VetaPrev
      character*2  nty
      integer :: SbwLnQuest,SbwItemSelQuest,EdwStateQuest,CrwStateQuest,
     1           kip(6),kic(6),RolMenuSelectedQuest,NFrom=1,NEach=3,
     2           ip(1)
      integer, allocatable :: NThickEDZoneO(:)
      logical FeYesNo,FileDiff,CrwLogicQuest,CalcDynOld,ExistFile,
     1        EqIgCase,RefineEnd,Prvni,lpom
      logical, allocatable :: UseEDZoneO(:),LZone(:),LZoneO(:),LZoneR(:)
      if(allocated(LZone)) deallocate(LZone)
      n=0
      do i=1,NRefBlock
        n=max(n,NEDZone(i))
      enddo
      allocate(LZone(n),LZoneO(n))
      KDatBlockO=KDatBlock
      KRefBlockO=KRefBlock
      LZone=.false.
      CalcDynOld=CalcDyn
      call DeleteFile(fln(:ifln)//'.edout_all')
1100  xqd=700.
      pom=(xqd-4.*75.-10.)/4.
      pom=(xqd-80.-pom)/3.
      xpoma(4)=xqd-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      il=18
      id=NextQuestId()
      Veta='Parameters for electron diffraction data'
      if(NRefBlock.gt.1) then
        write(Cislo,'(i5)') KRefBlock
        call Zhusti(Cislo)
        Veta=Veta(:idel(Veta))//' - Refblock#'//Cislo
        il=il+1
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,
     1                   OKForBasicFiles)
      il=1
      tpom=5.
      Veta='Orientation matrix:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      tpom=tpom+FeTxLength(Veta)+25.
      xpom=tpom+25.
      dpom=70.
      do i=1,3
        xpomp=xpom
        tpomp=tpom
        do j=1,3
          write(Cislo,'(''U'',2i1)') i,j
          call FeQuestEdwMake(id,tpomp,il,xpomp,il,Cislo,'L',dpom,EdwYd,
     1                        0)
          call FeQuestRealEdwOpen(EdwLastMade,
     1                            OrMatEDZone(i,j,KRefBlock),.false.,
     2                            .false.)
          if(i.eq.1.and.j.eq.1) nEdwUB=EdwLastMade
          xpomp=xpomp+110.
          tpomp=tpomp+110.
        enddo
        il=il+1
      enddo
      tpom=5.
      Veta='Maximal diffraction vector g%(max):'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,GMaxEDZone(KRefBlock),.false.,
     1                        .false.)
      nEdwGMax=EdwLastMade
      tpomp=tpom+xqd*.5-70.
      xpomp=xpom+xqd*.5-90.
      dpomp=40.
      Veta='%Number integration steps:'
      call FeQuestEdwMake(id,tpomp,il,xpomp,il,Veta,'L',dpomp,EdwYd,0)
      call FeQuestIntEdwOpen(EdwLastMade,EDIntSteps(KRefBlock),.false.)
      nEdwIntSteps=EdwLastMade
      xpompp=xpomp+dpomp+40.
      tpompp=xpompp+CrwgXd+10.
      Veta='Geometry %PEDT'
      call FeQuestCrwMake(id,tpompp,il,xpompp,il,Veta,'L',CrwXd,CrwYd,0,
     1                    1)
      call FeQuestCrwOpen(CrwLastMade,EDGeometryIEDT(KRefBlock).le.0)
      nCrwPEDT=CrwLastMade
      il=il+1
      Veta='Maximal %excitation error (Matrix):'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,SGMaxMEDZone(KRefBlock),
     1                        .false.,.false.)
      nEdwSGMaxM=EdwLastMade
      Veta='%Use dynamic approach:'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,CalcDyn)
      nCrwCalcDyn=CrwLastMade
      Veta='Geometry %IEDT'
      call FeQuestCrwMake(id,tpompp,il,xpompp,il,Veta,'L',CrwXd,CrwYd,0,
     1                    1)
      call FeQuestCrwOpen(CrwLastMade,EDGeometryIEDT(KRefBlock).gt.0)
      nCrwIEDT=CrwLastMade
      il=il+1
      Veta='Maximal e%xcitation error (Refine):'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,SGMaxREDZone(KRefBlock),
     1                        .false.,.false.)
      nEdwSGMaxR=EdwLastMade
      Veta='%Apply correction for crystal tilt:'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,EDTiltCorr(KRefBlock).eq.1)
      nCrwTiltCorr=CrwLastMade
      il=il+1
      Veta='%Limit on RSg:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestRealEdwOpen(EdwLastMade,CSGMaxREDZone(KRefBlock),
     1                        .false.,.false.)
      nEdwCSGMaxR=EdwLastMade
      Veta='For Fourier rescale to %Fcalc:'
      call FeQuestCrwMake(id,tpomp,il,xpomp,il,Veta,'L',CrwXd,CrwYd,0,0)
      call FeQuestCrwOpen(CrwLastMade,RescaleToFCalc)
      nCrwRescaleToFCalc=CrwLastMade
      il=il+1
      Veta='Nu%mber of threads:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestIntEdwOpen(EdwLastMade,EDThreads(KRefBlock),.false.)
      nEdwThreads=EdwLastMade
      Veta='Select %zones for refinement'
      dpom=FeTxLength(Veta)+10.
      call FeQuestButtonMake(id,tpomp,il,dpom,ButYd,Veta)
      if(NEDZone(KRefBlock).gt.1) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,j)
      nButtSelect=ButtonLastMade
      tpomp=tpomp+dpom+20.
      Veta='Define zones of e%qual thicknesses'
      dpom=FeTxLength(Veta)+10.
      call FeQuestButtonMake(id,tpomp,il,dpom,ButYd,Veta)
      if(NEDZone(KRefBlock).gt.1) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,j)
      nButtThick=ButtonLastMade
      il=il+1
      Veta='%Dyngo commands:'
      dpom=xqd-xpom-5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      call FeQuestStringEdwOpen(EdwLastMade,EDCommands)
      nEdwCommands=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='%Run optimalizations'
      dpom=FeTxLengthUnder(Veta)+20.
      tpom=70.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(NAtCalc.le.0) call FeQuestButtonDisable(ButtonLastMade)
      nButtOptimalization=ButtonLastMade
      tpom=tpom+dpom+20.
      Veta='except of scale optimize also:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      if(NAtCalc.le.0) call FeQuestLblDisable(LblLastMade)
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      tpom=xpom+CrwgXd+10.
      Veta='%Thickness'
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        call FeQuestCrwOpen(CrwLastMade,.false.)
        if(NAtCalc.le.0) call FeQuestCrwDisable(CrwLastMade)
        if(i.eq.1) then
          nCrwOptThick=CrwLastMade
          tpomp=tpom+FeTxLengthUnder(Veta)+20.
          Veta='%Show thickness plots'
          dpom=FeTxLengthUnder(Veta)+20.
          call FeQuestButtonMake(id,tpomp,il,dpom,ButYd,Veta)
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          nButtGraphThick=ButtonLastMade
          Veta='%Orientation'
        else
          nCrwOptOrient=CrwLastMade
        endif
        il=il+1
      enddo
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Zone%#:'
      nz=1
      LZone(1)=.true.
      NZones=1
      dpom=40.
      tpom=240.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=40.
      xpom=xpoma(1)
      tpom=tpoma(1)
      ilp=-10*il+3
      call FeQuestEudMake(id,tpom,ilp,xpom,ilp,Veta,'R',dpom,EdwYd,1)
      call FeQuestIntEdwOpen(EdwLastMade,nz,.false.)
      call FeQuestEudOpen(EdwLastMade,1,NEDZone(KRefBlock),1,0.,0.,0.)
      nEdwNZ=EdwLastMade
      tpom=xpom+100.
      call FeQuestLblMake(id,tpom,ilp,'R(all)= ---------','L','N')
      nLblRFac=LblLastMade
      Veta='Sele%ct zones for editing'
      tpom=tpom+100.
      dpom=FeTxLength(Veta)+10.
      call FeQuestButtonMake(id,tpom,ilp,dpom,ButYd,Veta)
      if(NEDZone(KRefBlock).gt.1) then
        j=ButtonOff
      else
        j=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,j)
      nButtEdit=ButtonLastMade
      il=il+1
      kip=1
      Cislo='H'
      j=0
      dpom=70.
      ilp=-10*il+3
      do i=1,6
        j=mod(j,4)+1
        call FeQuestEdwMake(id,tpoma(j),ilp,xpoma(j),ilp,Cislo,'R',dpom,
     1                      EdwYd,0)
        if(i.eq.1) then
          nEdwH=EdwLastMade
          Cislo='K'
        else if(i.eq.2) then
          Cislo='L'
        else if(i.eq.3) then
          Cislo='prec.angle'
        else if(i.eq.4) then
          nEdwPrecAngle=EdwLastMade
          ilp=ilp-10
          Cislo='alpha'
        else if(i.eq.5) then
          nEdwAplha=EdwLastMade
          Cislo='beta'
        else if(i.eq.6) then
          nEdwBeta=EdwLastMade
        endif
      enddo
      tpom=5.
      Veta='Selected zones:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLblTitleSelected=LblLastMade
      tpom=tpom+FeTxLength(Veta)+3.
      Veta=' '
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      call FeQuestLblOff(LblLastMade)
      nLblSelected=LblLastMade
      il=il+1
      j=0
      il=il+1
      do i=1,MxEDRef
        j=mod(j,4)+1
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,lEDVar(i),nEdw,
     1                       nCrw)
        if(i.eq.1) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        else if(i.eq.4) then
          il=il+1
        endif
      enddo
      if(NRefBlock.gt.1) then
        ilp=-10*il-12
        Veta=' '
        dpom=70.
        xpom=(xqd-dpom)*.5
        tpom=xpom
        call FeQuestRolMenuMake(id,tpom,ilp,xpom,ilp,Veta,'L',dpom,
     1                          EdwYd,1)
        call FeQuestRolMenuOpen(RolMenuLastMade,RefBlockName,NRefBlock,
     1                          KRefBlock)
        nRolMenuRefBlock=RolMenuLastMade
      else
        nRolMenuRefBlock=0
      endif
      nz=1
      LZone=.false.
      LZone(1)=.true.
      NZones=1
      nzo=-1
1400  if(NZones.le.1) then
        if(nz.eq.nzo) go to 1500
      else
        if(NZones.eq.NZonesO) then
          do i=1,NEDZone(KRefBlock)
            if(LZone(i).neqv.LZoneO(i)) go to 1420
          enddo
          go to 1500
        endif
      endif
1420  nEdw=nEdwH
      if(NZones.le.1) then
        call FeQuestLblOff(nLblTitleSelected)
        call FeQuestLblOff(nLblSelected)
        call FeQuestIntEdwOpen(nEdwNZ,nz,.false.)
        call FeQuestEudOpen(nEdwNZ,1,NEDZone(KRefBlock),1,0.,0.,0.)
        if(RFacEDZone(nz,KRefBlock).le.0.) then
          Veta='R(all)= ---------'
        else
          write(Cislo,'(f8.4)') RFacEDZone(nz,KRefBlock)
          Veta='R(all)='//Cislo(:idel(Cislo))//'%'
        endif
        call FeQuestLblChange(nLblRFac,Veta)
        do i=1,6
          if(i.eq.1) then
            pom=HEDZone(1,nz,KRefBlock)
          else if(i.eq.2) then
            pom=HEDZone(2,nz,KRefBlock)
          else if(i.eq.3) then
            pom=HEDZone(3,nz,KRefBlock)
          else if(i.eq.4) then
            pom=PrAngEDZone(nz,KRefBlock)
          else if(i.eq.5) then
            pom=AlphaEDZone(nz,KRefBlock)
          else if(i.eq.6) then
            pom=BetaEDZone(nz,KRefBlock)
          endif
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
        enddo
      else
        call FeQuestEdwDisable(nEdwNZ)
        Veta='R(all)= ---------'
        call FeQuestLblChange(nLblRFac,Veta)
        do i=1,6
          call FeQuestEdwClose(nEdw)
          nEdw=nEdw+1
        enddo
        call FeQuestLblOn(nLblTitleSelected)
        Veta=' '
        i1=0
        i2=0
        do i=1,NEDZone(KRefBlock)
          if(LZone(i)) then
            if(i1.gt.0) then
              i2=i
            else
              i1=i
              i2=i
            endif
          endif
          if(.not.LZone(i).or.i.eq.NEDZone(KRefBlock)) then
            if(i1.gt.0) then
              if(i2.gt.i1) then
                write(Cislo,'(i5,''-'',i5)') i1,i2
              else
                write(Cislo,'(i5)') i1
              endif
              call Zhusti(Cislo)
              idl=idel(Veta)
              if(idl.le.0) then
                Veta=Cislo
              else
                Veta=Veta(:idl)//','//Cislo
              endif
              i1=0
              i2=0
            endif
          endif
        enddo
        call FeQuestLblOn(nLblTitleSelected)
        call FeQuestLblChange(nLblSelected,Veta)
      endif
      if(nz.gt.0) then
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        do i=1,MxEDRef
          Prvni=.true.
          do j=1,NEDZone(KRefBlock)
            if(.not.LZone(j)) cycle
            if(i.eq.1) then
              pom=ScEDZone(j,KRefBlock)
            else if(i.eq.2) then
              pom=ThickEDZone(j,KRefBlock)
            else if(i.eq.3) then
              pom=XNormEDZone(j,KRefBlock)
            else if(i.eq.4) then
              pom=YNormEDZone(j,KRefBlock)
            else if(i.eq.5) then
              pom=PhiEDZone(j,KRefBlock)
            else
              pom=ThetaEDZone(j,KRefBlock)
            endif
            if(Prvni) then
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
              call FeQuestCrwOpen(nCrw,KiED(i,j,KRefBlock).ne.0)
              pomc(i)=pom
              kic(i)=KiED(i,j,KRefBlock)
              Prvni=.false.
            else
              if(EdwStateQuest(nEdw).ne.EdwLocked.and.
     1           abs(pom-pomc(i)).gt..0001)
     2          call FeQuestEdwLock(nEdw)
              if(CrwStateQuest(nCrw).ne.CrwLocked.and.
     1           KiED(i,j,KRefBlock).ne.kic(i))
     2          call FeQuestCrwLock(nCrw)
            endif
          enddo
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
      if(NZones.le.1) then
        nzo=nz
      else
        nzo=-1
      endif
      LZoneO=LZone
      NZonesO=NZones
1450  if(CalcDyn) then
        call FeQuestCrwOff(nCrwOptOrient)
        call FeQuestCrwOff(nCrwOptThick)
      else
        call FeQuestCrwDisable(nCrwOptOrient)
        call FeQuestCrwDisable(nCrwOptThick)
      endif
      if(ExistFile(fln(:ifln)//'.edout_thick')) then
        call FeQuestButtonOff(nButtGraphThick)
      else
        call FeQuestButtonDisable(nButtGraphThick)
      endif
      Navrat=0
1500  call FeQuestEvent(id,ich)
1510  if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNZ) then
        call FeQuestIntFromEdw(nEdwNZ,nz)
        LZone=.false.
        LZone(nz)=.true.
        NZones=1
        Navrat=2
        go to 3000
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCalcDyn) then
        if(.not.CalcDynOld) then
          CalcDyn=.true.
          call FeFillTextInfo('em40eldiff1.txt',0)
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          call DeleteFile(fln(:ifln)//'.m90')
          ExistM90=.false.
          call EM9CreateM90(1,ich)
          CalcDynOld=.true.
        endif
        go to 1450
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtOptimalization) then
        ActionDatBlock=RefDatCorrespond(KRefBlock)
        if(Navrat.eq.0) then
          Navrat=1
          go to 3000
        endif
        if(CrwLogicQuest(nCrwOptThick)) then
          if(CrwLogicQuest(nCrwOptOrient)) then
            ActionED=5
          else
            ActionED=4
          endif
        else
          if(CrwLogicQuest(nCrwOptOrient)) then
            ActionED=3
          else
            ActionED=2
          endif
        endif
        Veta=fln(:ifln)//'.edout_all'
        call DeleteFile(Veta)
        call DeleteFile(fln(:ifln)//'.edout')
        call iom90(0,fln(:ifln)//'.m90')
        KDatBlock=ActionDatBlock
        call Refine(0,RefineEnd)
        if(ErrFlag.ne.0) go to 1450
        if(ActionED.eq.4) call CopyFile(Veta,fln(:ifln)//'.edout_thick')
        ln=NextLogicNumber()
        call OpenFile(ln,Veta,'formatted','unknown')
1530    read(ln,FormA,end=1540) Veta
        if(LocateSubstring(Veta,'NaN',.false.,.true.).gt.0) then
          go to 1590
        else
          go to 1530
        endif
1540    rewind ln
        n=0
        xp=0
        VetaPrev='NicTamNeni'
1550    read(ln,FormA,end=1600) Veta
        if(Veta(1:3).eq.'===') then
1560      n=n+1
          if(n.le.NEdZone(KRefBlock)) then
            if(.not.UseEdZone(n,KRefBlock)) go to 1560
          endif
          if(EqIgCase(VetaPrev,'NicTamNeni')) go to 1600
          read(VetaPrev,100,err=1600) xp
          if(ActionED.eq.2) then
            RFacEDZone(n,KRefBlock)=xp(2)
            ScEDZone(n,KRefBlock)=xp(3)
          else if(ActionED.eq.3) then
            PhiEDZone(n,KRefBlock)=xp(1)
            ThetaEDZone(n,KRefBlock)=xp(2)
            ScEDZone(n,KRefBlock)=xp(3)
            RFacEDZone(n,KRefBlock)=xp(4)
          else if(ActionED.eq.4) then
            ThickEDZone(n,KRefBlock)=xp(1)
            RFacEDZone(n,KRefBlock)=xp(2)
            ScEDZone(n,KRefBlock)=xp(3)
          else if(ActionED.eq.5) then
            PhiEDZone(n,KRefBlock)=xp(1)
            ThetaEDZone(n,KRefBlock)=xp(2)
            ScEDZone(n,KRefBlock)=xp(3)
            ThickEDZone(n,KRefBlock)=xp(4)
            RFacEDZone(n,KRefBlock)=xp(5)
          endif
        endif
        VetaPrev=Veta
        go to 1550
1590    call FeReadError(ln)
1600    call CloseIfOpened(ln)
        nzo=0
        go to 1400
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtGraphThick) then
        call EM40GrThick
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtSelect.or.
     2         CheckNumber.eq.nButtThick.or.
     3         CheckNumber.eq.nButtEdit)) then
        CheckNumberMain=CheckNumber
        if(CheckNumberMain.eq.nButtSelect) then
          if(allocated(UseEDZoneO)) deallocate(UseEDZoneO)
          allocate(UseEDZoneO(NEDZone(KRefBlock)))
          UseEDZoneO(1:NEDZone(KRefBlock))=
     1      UseEDZone(1:NEDZone(KRefBlock),KRefBlock)
          Veta='Select zones for refinement'
        else if(CheckNumber.eq.nButtThick) then
          if(allocated(NThickEDZoneO)) deallocate(NThickEDZoneO)
          allocate(NThickEDZoneO(NEDZone(1)))
          NThickEDZoneO(1:NEDZone(KRefBlock))=
     1      NThickEDZone(1:NEDZone(KRefBlock),KRefBlock)
          NThick=1
          Veta='Define zones of equal thichnesses'
        else if(CheckNumber.eq.nButtEdit) then
          if(allocated(LZoneR)) deallocate(LZoneR)
          allocate(LZoneR(NEDZone(KRefBlock)))
          LZoneR=LZone
          NZonesR=NZones
          Veta='Define zones for editing'
        endif
        idp=NextQuestId()
        xqd=500.
        il=15
        if(CheckNumberMain.eq.nButtThick) il=il+1
        call FeQuestCreate(idp,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
        il=12
        ild=12
        xpom=5.
        dpom=xqd-10.-SbwPruhXd
        call FeQuestSbwMake(idp,xpom,il,dpom,ild,3,CutTextFromRight,
     1                       SbwHorizontal)
        nSbwSel=SbwLastMade
        ln=NextLogicNumber()
        call DeleteFile(fln(:ifln)//'_zones.tmp')
        call OpenFile(ln,fln(:ifln)//'_zones.tmp','formatted','unknown')
        do i=1,NEDZone(KRefBlock)
          Veta=' - ['
          do j=1,3
            write(Cislo,'(f15.3)') HEDZone(j,i,KRefBlock)
            call ZdrcniCisla(Cislo,1)
            Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))//','
          enddo
          idl=idel(Veta)
          Veta(idl:idl)=']'
          write(Cislo,FormI15) i
          call Zhusti(Cislo)
          Veta='Zone#'//Cislo(:idel(Cislo))//Veta(:idl)
          write(ln,FormA) Veta(:idel(Veta))
        enddo
        call CloseIfOpened(ln)
        il=il+1
        if(CheckNumberMain.eq.nButtThick) then
          il=il+1
          Veta='Thickness %#'
          dpom=50.
          xpom=.5*xqd-dpom*.5-7.
          tpom=xpom-5.
          ilp=-10*il+4
          call FeQuestEudMake(idp,tpom,ilp,xpom,ilp,Veta,'R',dpom,
     1                        EdwYd,1)
          nEdwThick=EdwLastMade
          call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
        endif
        ilp=-il*10-7
        Veta='select each'
        dpomb=FeTxLengthUnder(Veta)+10.
        xpomb=(xqd-dpomb)*.5
        call FeQuestButtonMake(idp,xpomb,ilp,dpomb,ButYd,Veta)
        nButtSelectEach=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='From the zone#'
        dpom=50.
        xpom=xpomb-dpom-10.
        tpom=xpom-FeTxLengthUnder(Veta)-4.
        call FeQuestEdwMake(idp,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,0)
        nEdwSelectFrom=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NFrom,.false.)
        Veta=' '
        xpom=xpomb+dpomb+10.
        dpom=30.
        call FeQuestEudMake(idp,tpom,ilp,xpom,ilp,Veta,'L',dpom,EdwYd,1)
        nEdwSelectEach=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,NEach,.false.)
        call FeQuestEudOpen(EdwLastMade,1,NEDZone(KRefBlock),1,0.,0.,0.)
        Veta=nty(NEach)//' zone'
        tpom=xpom+dpom+20.
        call FeQuestLblMake(idp,tpom,ilp,Veta,'L','N')
        nLblSelectEach=LblLastMade
        il=il+2
        dpom=120.
        xpom=.5*xqd-1.*dpom-10.
        Veta='Select %all'
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtAll=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
        Veta='%Refresh'
        call FeQuestButtonMake(idp,xpom,il,dpom,ButYd,Veta)
        nButtRefresh=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
2100    call CloseIfOpened(SbwLnQuest(nSbwSel))
        NThickMax=NEDZone(KRefBlock)
        do i=1,NEDZone(KRefBlock)
          if(CheckNumberMain.eq.nButtSelect) then
            if(UseEDZone(i,KRefBlock)) then
              j=1
            else
              j=0
            endif
          else if(CheckNumberMain.eq.nButtThick) then
            if(NThickEDZone(i,KRefBlock).eq.NThick) then
              j=1
            else
              j=0
            endif
            NThickMax=max(NThickMax,NThickEDZone(i,KRefBlock)+1)
          else if(CheckNumberMain.eq.nButtEdit) then
            if(LZone(i)) then
              j=1
            else
              j=0
            endif
          endif
          call FeQuestSetSbwItemSel(i,nSbwSel,j)
        enddo
        call FeQuestSbwSelectOpen(nSbwSel,fln(:ifln)//'_zones.tmp')
        if(CheckNumberMain.eq.nButtThick)
     1    call FeQuestEudOpen(nEdwThick,1,NThickMax,1,0.,0.,0.)
2500    call FeQuestEvent(idp,ich)
        if(CheckType.eq.EventButton.and.
     1     (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2    then
          if(CheckNumberMain.eq.nButtSelect) then
            lpom=CheckNumber.eq.nButtAll
            do i=1,NEDZone(KRefBlock)
              UseEDZone(i,KRefBlock)=lpom
            enddo
          else if(CheckNumberMain.eq.nButtThick) then
            do i=1,NEDZone(KRefBlock)
              if(CheckNumber.eq.nButtAll) then
                NThickEDZone(i,KRefBlock)=NThick
              else
                if(NThickEDZone(i,KRefBlock).eq.NThick)
     1             NThickEDZone(i,KRefBlock)=0
              endif
            enddo
          else if(CheckNumberMain.eq.nButtEdit) then
            LZone=CheckNumber.eq.nButtAll
            if(.not.LZone(1)) then
              LZone(1)=.true.
              nz=1
              NZones=1
            endif
          endif
          go to 2100
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtSelectEach) then
          call FeQuestIntFromEdw(nEdwSelectEach,NEach)
          call FeQuestIntFromEdw(nEdwSelectFrom,NFrom)
          do i=1,NEDZone(KRefBlock)
            if(CheckNumberMain.eq.nButtSelect) then
              UseEDZone(i,KRefBlock)=.false.
            else if(CheckNumberMain.eq.nButtEdit) then
              LZone(i)=.false.
            endif
          enddo
          do i=NFrom,NEDZone(KRefBlock),NEach
            if(CheckNumberMain.eq.nButtSelect) then
              UseEDZone(i,KRefBlock)=.true.
            else if(CheckNumberMain.eq.nButtThick) then
              NThickEDZone(i,KRefBlock)=NThick
            else if(CheckNumberMain.eq.nButtEdit) then
              LZone(i)=.true.
            endif
          enddo
          go to 2100
        else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwThick) then
          n=0
          do i=1,NEDZone(KRefBlock)
            if(SbwItemSelQuest(i,nSbwSel).eq.1) then
              NThickEDZone(i,KRefBlock)=NThick
              n=n+1
            else
              if(NThickEDZone(i,KRefBlock).eq.NThick)
     1          NThickEDZone(i,KRefBlock)=0
            endif
          enddo
          call FeQuestIntFromEdw(nEdwThick,m)
          if(n.gt.0.or.m.le.NThick) then
            NThick=m
            go to 2100
          else
            call FeQuestIntEdwOpen(nEdwThick,NThick,.false.)
            call FeQuestEudOpen(nEdwThick,1,NThickMax,1,0.,0.,0.)
            go to 2500
          endif
        else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwSelectEach)
     1    then
          call FeQuestIntFromEdw(nEdwSelectEach,NEach)
          Veta=nty(NEach)//' zone'
          call FeQuestLblChange(nLblSelectEach,Veta)
          go to 2500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 2500
        endif
        if(ich.eq.0) then
          if(CheckNumberMain.eq.nButtEdit) NZones=0
          do i=1,NEDZone(KRefBlock)
            if(SbwItemSelQuest(i,nSbwSel).eq.1) then
              if(CheckNumberMain.eq.nButtSelect) then
                UseEDZone(i,KRefBlock)=.true.
              else if(CheckNumberMain.eq.nButtThick) then
                NThickEDZone(i,KRefBlock)=NThick
              else if(CheckNumberMain.eq.nButtEdit) then
                NZones=NZones+1
                LZone(i)=.true.
              endif
            else
              if(CheckNumberMain.eq.nButtSelect) then
                UseEDZone(i,KRefBlock)=.false.
              else if(CheckNumberMain.eq.nButtThick) then
                if(NThickEDZone(i,KRefBlock).eq.NThick)
     1             NThickEDZone(i,KRefBlock)=0
              else if(CheckNumberMain.eq.nButtEdit) then
                LZone(i)=.false.
              endif
            endif
          enddo
        else
          if(CheckNumberMain.eq.nButtSelect) then
            UseEDZone(1:NEDZone(KRefBlock),KRefBlock)=
     1        UseEDZoneO(1:NEDZone(KRefBlock))
          else if(CheckNumberMain.eq.nButtThick) then
            NThickEDZone(1:NEDZone(KRefBlock),KRefBlock)=
     1        NThickEDZoneO(1:NEDZone(KRefBlock))
          else if(CheckNumberMain.eq.nButtEdit) then
            NZones=NZonesR
            LZone=LZoneR
          endif
        endif
        call FeQuestRemove(idp)
        if(allocated(UseEDZoneO)) deallocate(UseEDZoneO)
        if(allocated(NThickEDZoneO)) deallocate(NThickEDZoneO)
        if(allocated(LZoneR)) deallocate(LZoneR)
        if(CheckNumberMain.eq.nButtEdit) then
          nz=0
          do i=1,NEDZone(KRefBlock)
            if(LZone(i)) then
              nz=i
              exit
            endif
          enddo
          if(nz.le.0) nz=1
          Navrat=2
          go to 3000
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdwUnlock.and.
     1        (CheckNumber.ge.nEdwPrv.and.CheckNumber.le.nEdwPrv+5))
     2  then
        nEdw=CheckNumber
        i=nEdw-nEdwPrv+1
        call FeQuestRealEdwOpen(nEdw,pomc(i),.false.,.false.)
        go to 1500
      else if(CheckType.eq.EventCrwUnlock.and.
     1        (CheckNumber.ge.nCrwPrv.and.CheckNumber.le.nCrwPrv+5))
     2  then
        nCrw=CheckNumber
        i=nCrw-nCrwPrv+1
        call FeQuestCrwOpen(nCrw,kic(i).gt.0)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuRefBlock) then
        KRefBlockNew=RolMenuSelectedQuest(nRolMenuRefBlock)
        Navrat=3
        go to 3000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      Navrat=0
3000  Veta=fln(:ifln)//'.m42'
      if(ich.eq.0) then
        CalcDyn=CrwLogicQuest(nCrwCalcDyn)
        RescaleToFCalc=CrwLogicQuest(nCrwRescaleToFCalc)
        if(NZonesO.le.1) then
          nEdw=nEdwH
          do i=1,6
            call FeQuestRealFromEdw(nEdw,pom)
            if(i.eq.1) then
              HEDZone(1,nzo,KRefBlock)=pom
            else if(i.eq.2) then
              HEDZone(2,nzo,KRefBlock)=pom
            else if(i.eq.3) then
              HEDZone(3,nzo,KRefBlock)=pom
            else if(i.eq.4) then
              PrAngEDZone(nzo,KRefBlock)=pom
            else if(i.eq.5) then
              AlphaEDZone(nzo,KRefBlock)=pom
            else
              BetaEDZone(nzo,KRefBlock)=pom
            endif
            nEdw=nEdw+1
          enddo
        endif
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,xp,kip,6)
        do i=1,MxEDRef
          do j=1,NEDZone(KRefBlock)
            if(LZoneO(j)) then
              if(mod(kip(i),100).lt.10)
     1          KiED(i,j,KRefBlock)=mod(kip(i),10)
              if(kip(i)/100.eq.0) then
                if(i.eq.1) then
                  ScEDZone(j,KRefBlock)=xp(i)
                else if(i.eq.2) then
                  ThickEDZone(j,KRefBlock)=xp(i)
                else if(i.eq.3) then
                  XNormEDZone(j,KRefBlock)=xp(i)
                else if(i.eq.4) then
                  YNormEDZone(j,KRefBlock)=xp(i)
                else if(i.eq.5) then
                  PhiEDZone(j,KRefBlock)=xp(i)
                else  if(i.eq.6) then
                  ThetaEDZone(j,KRefBlock)=xp(i)
                endif
              endif
            endif
          enddo
        enddo
        nEdw=nEdwUB
        do i=1,3
          do j=1,3
            call FeQuestRealFromEdw(nEdw,OrMatEDZone(i,j,KRefBlock))
            nEdw=nEdw+1
          enddo
        enddo
        call FeQuestRealFromEdw(nEdwGMax,GMaxEDZone(KRefBlock))
        call FeQuestRealFromEdw(nEdwSGMaxM,SGMaxMEDZone(KRefBlock))
        call FeQuestRealFromEdw(nEdwSGMaxR,SGMaxREDZone(KRefBlock))
        call FeQuestRealFromEdw(nEdwCSGMaxR,CSGMaxREDZone(KRefBlock))
        call FeQuestIntFromEdw(nEdwIntSteps,EDIntSteps(KRefBlock))
        call FeQuestIntFromEdw(nEdwThreads,EDThreads(KRefBlock))
        if(CrwLogicQuest(nCrwTiltCorr)) then
          EDTiltCorr(KRefBlock)=1
        else
          EDTiltCorr(KRefBlock)=0
        endif
        if(CrwLogicQuest(nCrwPEDT)) then
          EDGeometryIEDT(KRefBlock)=0
        else
          EDGeometryIEDT(KRefBlock)=1
        endif
        EDCommands=EdwStringQuest(nEdwCommands)
        if(Navrat.eq.1) then
          Navrat=-1
          go to 1510
        else if(Navrat.eq.2) then
          Navrat=0
          go to 1400
        endif
        call iom42(1,0,Veta)
        if(Navrat.eq.3) then
          call FeQuestRemove(id)
          KRefBlock=KRefBlockNew
          go to 1100
        endif
        if(FileDiff(Veta,PreviousM42)) then
          if(.not.FeYesNo(-1.,-1.,'Do you want to rewrite M42 file?',
     1                    1)) then
            call CopyFile(PreviousM42,Veta)
            ich=1
            go to 5000
          endif
          if(CalcDynOld.neqv.CalcDyn) then
            call FeFillTextInfo('em40eldiff1.txt',0)
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
            call DeleteFile(fln(:ifln)//'.m90')
            ExistM90=.false.
            call EM9CreateM90(1,ich)
            if(.not.CalcDyn) then
              do i=1,mxsc
                if(i.le.NoOfScales(KRefBlock)) then
                  if(sc(i,KRefBlock).le.0.) sc(i,KRefBlock)=1.
                  kis(i,KRefBlock)=1
                else
                  sc(i,KRefBlock)=0.
                  kis(i,KRefBlock)=0
                endif
              enddo
            endif
          endif
        endif
      else if(Navrat.eq.2) then
        Navrat=0
        go to 1400
      endif
5000  call FeQuestRemove(id)
      if(ich.ne.0) call iom42(0,0,Veta)
      ActionED=1
      if(allocated(LZone)) deallocate(LZone,LZoneO)
      KDatBlock=KDatBlockO
      KRefBlock=KRefBlockO
      return
100   format(6f14.6)
      end
      subroutine EM40GrThick
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xpp(3),xo(3),xp(2),yp(2),ih(3)
      dimension RFak(:),Thick(:)
      character*256 Veta
      character*17 :: Labels(6) =
     1              (/'%Quit            ',
     2                '%Print           ',
     3                '%Save            ',
     4                'Z%-              ',
     5                'Z%+              ',
     6                '%Go to           '/)
      allocatable RFak,Thick
      Tiskne=.false.
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(80.,40.,30.,40.)
      call FeBottomInfo('#prazdno#')
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,6
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
          wpom=wpom/2.-5.
          xsave=xpom
          ysave=ypom
        else if(i.eq.4) then
          nButtZMinus=ButtonLastMade
          xpom=xpom+wpom+10.
          ypom=ypom+dpom
        else if(i.eq.5) then
          nButtZPlus=ButtonLastMade
          wpom=80.
          xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
        else if(i.eq.6) then
          nButtGoto=ButtonLastMade
        endif
        if(i.le.1) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.6) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      NZoneMax=0
      do i=1,NEDZone(1)
        if(UseEdZone(i,KRefBlock)) NZoneMax=NZoneMax+1
      enddo
      NZone=1
1000  ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'.edout_thick','formatted','unknown')
      NZoneL=0
      m=0
      do i=1,NEDZone(KRefBlock)
        if(UseEdZone(i,KRefBlock)) then
          m=m+1
          if(m.eq.NZone) then
            NZoneL=i
            exit
          endif
        endif
      enddo
      do i=1,NZone-1
1100    read(ln,FormA,end=1150) Veta
        if(Veta(1:3).ne.'===') go to 1100
      enddo
1150  n=0
1200  read(ln,FormA,end=1300) Veta
      if(Veta(1:3).eq.'===') go to 1300
      n=n+1
      go to 1200
1300  rewind ln
      do i=1,NZone-1
1350    read(ln,FormA,end=1400) Veta
        if(Veta(1:3).ne.'===') go to 1350
      enddo
1400  if(allocated(Thick)) deallocate(Thick,RFak)
      allocate(RFak(n),Thick(n))
      rewind ln
      do i=1,NZone-1
1450    read(ln,FormA,end=1500) Veta
        if(Veta(1:3).ne.'===') go to 1450
      enddo
1500  n=0
      YMax=0.
      XMax=0.
      YMin=0.
      XMin=99999999999999.
1550  read(ln,FormA,end=1600) Veta
      if(Veta.ne.' ') then
        n=n+1
        read(Veta,100) Thick(n),RFak(n)
        YMax=max(YMax,RFak(n))
        XMin=min(XMin,Thick(n))
        XMax=max(XMax,Thick(n))
        go to 1550
      endif
      read(ln,100) ThickOpt,RFakOpt
1600  close(ln)
      call FeQuestButtonOff(nButtPrint)
      call FeQuestButtonOff(nButtSave)
      if(NZoneMax.gt.1) then
        if(NZone.le.1) then
          call FeQuestButtonDisable(nButtZMinus)
        else
          call FeQuestButtonOff(nButtZMinus)
        endif
        if(NZone.ge.NZoneMax) then
          call FeQuestButtonDisable(nButtZPlus)
        else
          call FeQuestButtonOff(nButtZPlus)
        endif
        call FeQuestButtonOff(nButtGoTo)
      endif
2100  call FeHardCopy(HardCopy,'open')
      call FeClearGrWin
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        xomn=XMin
        xomx=XMax
        yomn=YMin
        yomx=YMax
        call UnitMat(F2O,3)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeMakeAcFrame
        write(Cislo,FormI15) NZoneL
        call Zhusti(Cislo)
        Veta='R-factor/Thickess for zone #'//Cislo(:idel(Cislo))
        call FeOutSt(0,(XCornAcWin(1,2)+XCornAcWin(1,3))*.5,
     1                 (XCornAcWin(2,2)+XCornAcWin(2,3))*.5+15.-
     2                 pom*abs(XCornAcWin(2,2)-XCornAcWin(2,3))/
     3                 (XCornAcWin(1,2)-XCornAcWin(1,3)),Veta,'C',White)
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Thinkness')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'R-factor')
        xpp(3)=0.
        do i=1,n
          xpp(1)=Thick(i)
          xpp(2)=RFak(i)
          call FeXf2X(xpp,xo)
          call FeCircleOpen(xo(1),xo(2),3.,White)
        enddo
2300    xpp(1)=ThickOpt
        xpp(2)=YMin
        call FeXf2X(xpp,xo)
        xp(1)=xo(1)
        yp(1)=xo(2)
        xpp(1)=ThickOpt
        xpp(2)=YMax
        call FeXf2X(xpp,xo)
        xp(2)=xo(1)
        yp(2)=xo(2)
        call FePolyLine(2,xp,yp,Red)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',6,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtZMinus) then
          NZone=max(NZone-1,1)
          go to 1000
        else if(CheckNumber.eq.nButtZPlus) then
          NZone=min(NZone+1,NZoneMax)
          go to 1000
        else if(CheckNumber.eq.nButtGoto) then
          idp=NextQuestId()
          xqd=200.
          call FeQuestCreate(idp,-1.,-1.,xqd,1,' ',1,LightGray,0,0)
          tpom=5.
          dpom=50.
          Veta='Next zone to be drawn:'
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          call FeQuestEudMake(idp,tpom,1,xpom,1,Veta,'L',dpom,EdwYd,0)
          call FeQuestIntEdwOpen(EdwLastMade,NZoneL,.false.,.false.)
          call FeQuestEudOpen(EdwLastMade,1,NEDZone,1,1.,1.,1.)
          nEdwGoto=EdwLastMade
3500      call FeQuestEvent(idp,ich)
          if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK)
     1      then
            call FeQuestIntFromEdw(nEdwGoto,NZoneL)
            if(UseEDZone(NZoneL,KRefBlock)) then
              QuestCheck(idp)=0
              NZone=0
              do i=1,NZoneL
                if(UseEdZone(i,KRefBlock)) NZone=NZone+1
              enddo
            else
              write(Cislo,FormI15) NZoneL
              call Zhusti(Cislo)
              Veta='thinkness for the zone #'//Cislo(:idel(Cislo))//
     1             ' has not been optimized in the last run.'
              call FeChybne(-1.,300.,Veta,' ',SeriousError)
              call FeQuestButtonOff(ButtonOK-ButtonFr+1)
            endif
            go to 3500
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 3500
          endif
          call FeQuestRemove(idp)
          if(ich.eq.0) then
            go to 1000
          else
            go to 2500
          endif
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      call CloseIfOpened(ln)
      if(allocated(Thick)) deallocate(Thick,RFak)
9999  return
100   format(3f14.6)
      end
      subroutine EM40Atoms(ich)
      use Basic_mod
      use EditM40_mod
      use Atoms_mod
      use Molec_mod
      parameter (NMenuCoDal=12)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension IAtActiveOld(:)
      character*256 EdwStringQuest
      character*80 Veta
      character*50 MenuCoDal(NMenuCoDal)
      character*8  NameAtActiveOld(:)
      external EM40AtomsUpdateListek,EM40SelAtomsCheck,FeVoid,
     1         EM40AtomsUpdate
      integer FeMenu,SbwLnQuest,RolMenuSelectedQuest,
     1        SbwItemPointerQuest,FeMenuNew,CoDal,
     2        MenuCoDalEnable(NMenuCoDal),SbwItemFromQuest
      logical lpom,EqWild,BackToAdvanced,Poprve,
     1        LAtActiveOld(:),FeYesNo,FromCursorPosition
      allocatable IAtActiveOld,LAtActiveOld,NameAtActiveOld
      equivalence (IdNumbers(1) ,IdDeleteAtoms),
     1            (IdNumbers(2) ,IdMergeAtoms),
     2            (IdNumbers(3) ,IdTransAtoms),
     3            (IdNumbers(4) ,IdExpandAtoms),
     4            (IdNumbers(5) ,IdMolToAt),
     5            (IdNumbers(6) ,IdAddH),
     6            (IdNumbers(7) ,IdRename),
     7            (IdNumbers(8) ,IdRenameMan),
     8            (IdNumbers(9) ,IdMotifs),
     9            (IdNumbers(10),IdAtSplit),
     a            (IdNumbers(11),IdResetModMag),
     1            (IdNumbers(12),IdEditAtoms)
      data MenuCoDal/'%Delete selected atoms',
     1               'Mer%ge selected atoms',
     2               '%Transform selected atoms',
     3               'E%xpand selected atoms',
     4               'Atoms from m%olecule to atomic part',
     5               '%Adding of hydrogen atoms',
     6               '%Rename selected atoms to "atom_type"+number',
     7               'Re%name selected atoms manually',
     8               'Make symmetrically contiguous moti%fs',
     9               '%Split atomic position',
     a               'Reset %modulation/magnetic parameters',
     1               '%Edit/Define atoms'/
      allocate(MolMenu(mxpm))
      call SetIntArrayTo(MenuCoDalEnable,NMenuCoDal,1)
      if(StructureLocked)
     1  call SetIntArrayTo(MenuCoDalEnable,NMenuCoDal-1,0)
      KPhaseIn=KPhase
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      mxe=100
      mxep=5
      allocate(lnp(mxe),pnp(mxep,mxe),npa(mxe),pko(mxep,mxe),
     1         lat(mxe),pat(mxep,mxe),lpa(mxe),ppa(mxep,mxe),
     2         lnpo(mxe),pab(mxe),eqhar(mxe))
      Poprve=.true.
      ich=0
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
1000  if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      call EM40SwitchMag(0)
1100  if(allocated(IAtActiveOld))
     1  deallocate(IAtActiveOld,LAtActiveOld,NameAtActiveOld)
      if(.not.Poprve.and.ich.eq.0) call CrlAtomNamesApply
      Poprve=.false.
      call CrlAtomNamesIni
      NMolMenu=0
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        NMolMenu=NMolMenu+1
        MolMenu(NMolMenu)=MolName(i)
      enddo
      il=17
      id=NextQuestId()
      xqd=650.
      Veta=' '
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,
     1                   OKForBasicFiles)
      il=1
      tpom=5.
      LabelSel='Step #1: Select atoms to be used'
      if(NPhase.gt.1)
     1  LabelSel=LabelSel(:idel(LabelSel)+1)//'; Phase : '//
     1           PhaseName(KPhase)(:idel(PhaseName(KPhase)))
      DelLabelSel=idel(LabelSel)
      LabelSel(DelLabelSel+2:)=' -> 0 selected'
      call FeQuestLblMake(id,tpom,il,LabelSel,'L','N')
      nLblLabelSel=LblLastMade
      xpom=5.
      dpom=xqd-10.-SbwPruhXd
      il=11
      ild=10
      call FeQuestSbwMake(id,xpom,il,dpom,ild,7,CutTextFromRight,
     1                    SbwHorizontal)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      SbwRightClickAllowed=.true.
      nSbwSel=SbwLastMade
      il=il+2
      dpom=120.
      xpom=.5*xqd-1.5*dpom-20.
      Veta='Select %all'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAll=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      Veta='Select re%jected'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRejected=ButtonLastMade
      xpom=xpom+dpom+20.
      Veta='%Refresh'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtRefresh=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      if(NPhase.gt.1) then
        n=3
      else
        n=2
      endif
      xpom=.5*xqd-dpom*float(n)*.5-10.*float(n-1)
      if(NPhase.gt.1) then
        Veta='S%witch phase'
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtPhase=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+20.
      else
        nButtPhase=0
      endif
      Veta='Select a%dvanced'
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAdvanced=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xpom+dpom+20.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,1)
      nEdwInclude=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,' ')
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=5.
      Veta='Step #2: Select action by right mouse click or by this '//
     1     'button:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpomp=5.+FeTxLengthSpace(Veta(:9))
      xpom=tpom+FeTxLength(Veta)+10.
      Veta='A%ction'
      dpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtAction=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='Left mouse double click starts the Edit/Define action'
      call FeQuestLblMake(id,tpomp,il,Veta,'L','N')
      NAtActiveOld=0
      BackToAdvanced=.false.
      ItemSelOld=1
1200  call CloseIfOpened(SbwLnQuest(nSbwSel))
      ln=NextLogicNumber()
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      call OpenFile(ln,fln(:ifln)//'_atoms.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      NAtActive=0
      NAtRejected=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        write(ln,FormA) Atom(i)(:idel(Atom(i)))
        NAtActive=NAtActive+1
        if(ai(i).eq.0.) NAtRejected=NAtRejected+1
      enddo
      if(NMolecLenAll(KPhase).gt.0) then
        iak=NAtMolFrAll(KPhase)-1
        do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          iap=iak+1
          iak=iak+iam(im)/KPoint(im)
          do ia=iap,iak
            write(Veta,'('' #$color'',i10)') Blue
            write(ln,FormA) Atom(ia)(:idel(Atom(ia)))//
     1                      Veta(:idel(Veta))
            NAtActive=NAtActive+1
            if(ai(ia).eq.0.) NAtRejected=NAtRejected+1
          enddo
          iak=iak+(KPoint(im)-1)*(iak-iap+1)
        enddo
      endif
      call CloseIfOpened(ln)
      if(NAtActive.ne.NAtActiveOld.or.NAtActive.eq.0) then
        if(allocated(IAtActive))
     1    deallocate(IAtActive,LAtActive,NameAtActive)
        n=max(NAtActive,1)
        allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
      endif
      j=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        j=j+1
        IAtActive(j)=i
        if(NAtActive.ne.NAtActiveOld.or.NAtActive.eq.0)
     1    LAtActive(j)=.false.
        NameAtActive(j)=Atom(i)
      enddo
      if(NMolecLenAll(KPhase).gt.0) then
        iak=NAtMolFrAll(KPhase)-1
        do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          iap=iak+1
          iak=iak+iam(im)/KPoint(im)
          do ia=iap,iak
            j=j+1
            IAtActive(j)=ia
            if(NAtActive.ne.NAtActiveOld.or.NAtActive.eq.0)
     1        LAtActive(j)=.false.
            NameAtActive(j)=Atom(ia)
          enddo
          iak=iak+(KPoint(im)-1)*(iak-iap+1)
        enddo
      endif
      if(NAtRejected.gt.0) then
        i=ButtonOff
      else
        i=ButtonDisabled
      endif
      call FeQuestButtonOpen(nButtRejected,i)
      go to 1255
1250  call CloseIfOpened(SbwLnQuest(nSbwSel))
1255  NAtActiveSel=0
      NAtomicSel=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          NAtActiveSel=NAtActiveSel+1
          if(IAtActive(i).lt.NAtMolFr(1,1)) NAtomicSel=NAtomicSel+1
          j=1
        else
          j=0
        endif
        call FeQuestSetSbwItemSel(i,nSbwSel,j)
      enddo
      write(Cislo,'(i10)') NSel
      call Zhusti(Cislo)
      LabelSel(DelLabelSel+2:)=' -> '//Cislo(:idel(Cislo))//' selected'
      call FeQuestLblChange(nLblLabelSel,LabelSel)
      NAtActiveOld=NAtActive
      ItemFromOld=SbwItemFromQuest(nSbwSel)
      call FeQuestSbwSelectOpen(nSbwSel,fln(:ifln)//'_atoms.tmp')
      call FeQuestSbwShow(nSbwSel,ItemFromOld)
      call FeQuestSbwItemOff(nSbwSel,SbwItemPointerQuest(nSbwSel))
      call FeQuestSbwItemOn(nSbwSel,ItemSelOld)
1500  if(BackToAdvanced) then
        CheckType=EventButton
        CheckNumber=nButtAdvanced
        BackToAdvanced=.false.
        go to 1505
      endif
      call FeQuestEventWithCheck(id,ich,EM40AtomsUpdate,FeVoid)
1505  if(CheckType.eq.EventButton.and.
     1   (CheckNumber.eq.nButtAll.or.CheckNumber.eq.nButtRefresh))
     2  then
        lpom=CheckNumber.eq.nButtAll
        call SetLogicalArrayTo(LAtActive,NAtActive,lpom)
        ItemSelOld=SbwItemPointerQuest(nSbwSel)
        go to 1250
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtRejected)
     1  then
        do i=1,NAtActive
          LAtActive(i)=ai(IAtActive(i)).eq.0.
        enddo
        go to 1250
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAdvanced)
     1  then
        idp=NextQuestId()
        xqdp=300.
        if(NMolMenu.gt.0) then
          il=9
          jk=3
        else
          il=6
          jk=2
          nRolMenuMol=0
          nButtInclMol=0
          nButtExclMol=0
        endif
        call FeQuestCreate(idp,-1.,5.,xqdp,il,' ',0,LightGray,0,-1)
        il=0
        xpom=5.
        dpom=110.
        tpom=xpom+dpom+10.
        xpomp=tpom+28.
        dpomp=60.
        do j=1,jk
          if(j.eq.1) then
            Veta='Atoms (wildcard characters "?","*" allowed)'
          else if(j.eq.2) then
            Veta='Atom types'
          else if(j.eq.3) then
            Veta='Molecules'
          endif
          il=il+1
          call FeQuestLblMake(idp,xqdp*.5,il,Veta,'C','B')
          ilp=-10*(il+1)-5
          if(j.eq.1) then
            call FeQuestEdwMake(idp,tpom,ilp,xpom,ilp,'=>','L',dpom,
     1                          EdwYd,0)
            nEdwAtoms=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
          else
            call FeQuestRolMenuMake(idp,tpom,ilp,xpom,ilp,'=>','L',
     1                              dpom,EdwYd,0)
            if(j.eq.2) then
              nRolMenuTypes=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                                NAtFormula(KPhase),0)
            else
              nRolMenuMol=RolMenuLastMade
              call FeQuestRolMenuOpen(RolMenuLastMade,MolMenu,NMolMenu,
     1                                0)
            endif
          endif
          Veta='Include'
          do i=1,2
            il=il+1
            call FeQuestButtonMake(idp,xpomp,il,dpomp,ButYd,Veta)
            if(i.eq.1) then
              if(j.eq.1) then
                nButtInclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtInclTypes=ButtonLastMade
              else
                nButtInclMol=ButtonLastMade
              endif
              Veta='Exclude'
            else
              if(j.eq.1) then
                nButtExclAtoms=ButtonLastMade
              else if(j.eq.2) then
                nButtExclTypes=ButtonLastMade
              else
                nButtExclMol=ButtonLastMade
              endif
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonDisabled)
          enddo
        enddo
1600    call FeQuestEventWithCheck(idp,ich,EM40SelAtomsCheck,FeVoid)
        if(CheckType.eq.EventButton.and.
     1    (CheckNumber.eq.nButtInclAtoms.or.
     2     CheckNumber.eq.nButtExclAtoms)) then
          lpom=CheckNumber.eq.nButtInclAtoms
          Veta=EdwStringQuest(nEdwAtoms)
          do i=1,NAtActive
            if(EqWild(Atom(IAtActive(i)),Veta,.false.))
     1        LAtActive(i)=lpom
          enddo
          call FeQuestStringEdwOpen(nEdwAtoms,' ')
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtInclTypes.or.
     2           CheckNumber.eq.nButtExclTypes)) then
          lpom=CheckNumber.eq.nButtInclTypes
           isfp=RolMenuSelectedQuest(nRolMenuTypes)
          do i=1,NAtActive
            if(isf(IAtActive(i)).eq.isfp) LAtActive(i)=lpom
          enddo
          call FeQuestRolMenuOpen(nRolMenuTypes,AtTypeMenu,
     1                            NAtFormula(KPhase),0)
        else if(CheckType.eq.EventButton.and.
     1          (CheckNumber.eq.nButtInclMol.or.
     2           CheckNumber.eq.nButtExclMol)) then
          lpom=CheckNumber.eq.nButtInclMol
          kmolp=mxp*(RolMenuSelectedQuest(nRolMenuMol)-1)+1
          do i=1,NAtActive
            if(kmol(IAtActive(i)).eq.kmolp) LAtActive(i)=lpom
          enddo
          call FeQuestRolMenuOpen(nRolMenuTypes,AtTypeMenu,
     1                            NAtFormula(KPhase),0)
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1600
        endif
        call FeQuestRemove(idp)
        if(ich.ne.0) then
          go to 1500
        else
          BackToAdvanced=.true.
          go to 1250
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwInclude) then
        Veta=EdwStringQuest(nEdwInclude)
        do i=1,NAtActive
          if(EqWild(Atom(IAtActive(i)),Veta,.false.))
     1      LAtActive(i)=.true.
        enddo
        call FeQuestStringEdwOpen(nEdwInclude,' ')
        go to 1250
      else if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtAction)
     1        .or.CheckType.eq.EventSbwRightClick.or.
     2            CheckType.eq.EventSbwDoubleClick) then
        if(NAtActiveSel.le.0) then
          call SetLogicalArrayTo(LAtActive,NAtActive,.false.)
          i=SbwItemPointerQuest(nSbwSel)
          LAtActive(i)=.true.
          NAtActiveSel=1
          if(IAtActive(i).lt.NAtMolFr(1,1)) NAtomicSel=1
          FromCursorPosition=.true.
        else
          FromCursorPosition=.false.
        endif
        if(NMolMenu.le.0.or.NAtomicSel.gt.0) then
          MenuCoDalEnable(IdMolToAt)=0
        else
          MenuCoDalEnable(IdMolToAt)=1
        endif
        if(NDimI(KPhase).le.0.and.MaxMagneticType.le.0) then
          MenuCoDalEnable(IdResetModMag)=0
        else
          MenuCoDalEnable(IdResetModMag)=1
        endif
        if(CheckType.eq.EventSbwDoubleClick) then
          CoDal=IdEditAtoms
        else
          CoDal=FeMenuNew(-1.,-1.,-1.,MenuCoDal,MenuCoDalEnable,1,
     1                    NMenuCoDal,1,0)
        endif
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPhase)
     1  then
        i=FeMenu(-1.,-1.,PhaseName,1,NPhase,1,0)
        if(i.gt.0.and.i.ne.KPhase) then
          if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
          KPhase=i
          call FeDeferOutput
          call FeQuestRemove(id)
          go to 1000
        else
          go to 1500
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(allocated(IAtActiveOld)) then
          if(NAtActiveOld.eq.NAtActive) then
            do i=1,NAtActive
              if(LAtActiveOld(i).neqv.LAtActive(i).or.
     1           IAtActiveOld(i).ne.  IAtActive(i)) go to 1810
            enddo
            go to 1820
          endif
        else
          if(NAtActiveSel.le.0) go to 1820
        endif
1810    Veta='Your selection will be lost. Do you really want to '//
     1       'leave the form?'
        if(.not.FeYesNo(-1.,YBottomMessage,Veta,0)) then
          call FeQuestButtonOff(ButtonOK-ButtonFr+1)
          go to 1500
        endif
      endif
1820  call FeQuestRemove(id)
      go to 9999
2000  ItemSelOld=max(SbwItemPointerQuest(nSbwSel),1)
      idal=1100
      if(CoDal.eq.IdDeleteAtoms) then
        call EM40DeleteAtoms
        ItemSelOld=1
      else if(CoDal.eq.IdMergeAtoms) then
        call EM40MergeAtoms(ich)
      else if(CoDal.eq.IdTransAtoms) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40TransAtSelected(ich)
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdExpandAtoms) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40ExpandAtSelected(ich)
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdMolToAt) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40MolAtomsToAtomic
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdAddH) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40NewAtH(ich)
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdRename) then
        call EM40RenameAccordingToAtomType(ich)
      else if(CoDal.eq.IdRenameMan) then
        call EM40RenameManually(ich)
      else if(CoDal.eq.IdMotifs) then
        call EM40MakeMotifs
      else if(CoDal.eq.IdAtSplit) then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
        call EM40SplitAtoms
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      else if(CoDal.eq.IdResetModMag) then
        call EM40ResetModMag
      else if(CoDal.eq.IdEditAtoms) then
        go to 2200
      else
        go to 1500
      endif
2050  if(allocated(IAtActiveOld))
     1  deallocate(IAtActiveOld,LAtActiveOld,NameAtActiveOld)
      if(FromCursorPosition) then
        call SetLogicalArrayTo(LAtActive,NAtActive,.false.)
        NAtActiveSel=0
        NAtomicSel=0
      else
        allocate(IAtActiveOld(NAtActive),LAtActiveOld(NAtActive),
     1           NameAtActiveOld(NAtActive))
        do i=1,NAtActive
          IAtActiveOld(i)=IAtActive(i)
          LAtActiveOld(i)=LAtActive(i)
          NameAtActiveOld(i)=NameAtActive(i)
        enddo
      endif
      if(idal.eq.1100) then
        call FeQuestRemove(id)
        go to 1100
      else if(idal.eq.1200) then
        go to 1200
      endif
2200  XdQuestEM40=600.
      if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
      if(NAtXYZMode.gt.0) call TrXYZMode(0)
      if(NAtMagMode.gt.0) call TrMagMode(0)
      call iom40(1,0,fln(:ifln)//'.l40')
      if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
      if(NAtXYZMode.gt.0) call TrXYZMode(1)
      if(NAtMagMode.gt.0) call TrMagMode(1)
      il=12
      call FeKartCreate(-1.,-1.,XdQuestEM40,il,'Atom edit',0,
     1                  OKForBasicFiles)
      call FeCreateListek('Define',1)
      KartIdDefine=KartLastId
      call EM40AtomsDefineMake(KartIdDefine)
      call FeCreateListek('Edit',1)
      KartIdBasic=KartLastId
      call EM40AtomsEditMake(KartIdBasic)
      call FeCompleteKart(1)
3000  call FeQuestEventWithKartUpdate(KartId,ich,
     1                                EM40AtomsUpdateListek)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        do i=KartFirstId,NKart+KartFirstId-1
          QuestCheck(i)=0
        enddo
        go to 3000
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdDefine) then
          call EM40AtomsDefineUpdate
        else if(KartId.eq.KartIdBasic) then
          call EM40AtomsEditUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdDefine) then
          call EM40AtomsDefineCheck
        else if(KartId.eq.KartIdBasic) then
          call EM40AtomsEditCheck
        endif
        KartIdOld=KartId
        go to 3000
      endif
      if(ich.eq.0) then
        if(KartId.eq.KartIdDefine) then
          call EM40AtomsDefineUpdate
        else if(KartId.eq.KartIdBasic) then
          call EM40AtomsEditUpdate
        endif
        call CrlAtomNamesApply
        call CrlAtomNamesIni
      else
        Veta=fln(:ifln)//'.l40'
        call iom40(0,0,Veta)
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
        if(NAtXYZMode.gt.0) call TrXYZMode(1)
        if(NAtMagMode.gt.0) call TrMagMode(1)
        call DeleteFile(Veta)
      endif
      call FeDestroyKart
      idal=1200
      go to 2050
9999  if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
      KPhase=KPhaseIn
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(allocated(IAtActiveOld))
     1  deallocate(IAtActiveOld,LAtActiveOld,NameAtActiveOld)
      deallocate(MolMenu)
      if(allocated(lnp)) deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,
     1                              lnpo,pab,eqhar)
      call DeleteFile(fln(:ifln)//'_atoms.tmp')
      call EM40SwitchMag(1)
      return
      end
      subroutine EM40AtomsUpdate
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      integer SbwItemSelQuest
      Klic=0
      NAtActiveSel=0
      NAtomicSel=0
      do i=1,NAtActive
        if(SbwItemSelQuest(i,nSbwSel).eq.1) then
          NAtActiveSel=NAtActiveSel+1
          LAtActive(i)=.true.
          if(IAtActive(i).lt.NAtMolFr(1,1)) NAtomicSel=NAtomicSel+1
        else
          LAtActive(i)=.false.
        endif
      enddo
      write(Cislo,'(i10)') NAtActiveSel
      call Zhusti(Cislo)
      LabelSel(DelLabelSel+2:)=' -> '//Cislo(:idel(Cislo))//' selected'
      call FeQuestLblChange(nLblLabelSel,LabelSel)
      return
      end
      subroutine EM40MergeAtoms(ich)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*80 Veta
      data dmez/.1/
      Klic=0
      id=NextQuestId()
      Veta='Merging distance [A]'
      call FeBoldFont
      xqd=FeTxLength(Veta)+10.
      call FeNormalFont
      call FeQuestCreate(id,-1.,-1.,xqd,1,Veta,0,LightGray,0,0)
      dpom=50.
      xpom=(xqd-dpom)*.5
      call FeQuestEdwMake(id,0.,1,xpom,1,' ','C',dpom,EdwYd,0)
      nEdwDMez=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,dmez,.false.,.false.)
1000  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1000
      endif
      if(ich.eq.0) call FeQuestRealFromEdw(nEdwDMez,dmez)
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      go to 1050
      entry EM40MergeAtomsRun(DMezI,ich)
      Klic=1
      dmez=dmezi
1050  do im=0,NMolec
        if(im.gt.0) then
          if(kswmol(im).ne.KPhase) cycle
        endif
        if(im.eq.0) then
          NaFrom=1
          NaTo=NAtInd
        else
          if(im.eq.1) then
            NaFrom=NAtMolFr(1,1)
            NaTo=NaFrom+iam(im)/KPoint(im)-1
          else
            NaFrom=NaFrom+iam(im-1)
            NaTo=NaFrom+iam(im)/KPoint(im)-1
          endif
        endif
        NAll=0
        NMerge=0
        do i=1,NAtActive
          if(.not.LAtActive(i)) cycle
          ia=IAtActive(i)
          if(ia.lt.NaFrom.or.ia.ge.NaTo.or.isf(ia).eq.0) cycle
          NAll=NAll+1
          isw=iswa(ia)
          j=ia+1
1100      j=KoincAtoms(ia,j,NaTo,dmez,dst)
          if(j.gt.0) then
            if(iswa(j).ne.isw.or.kswa(j).ne.KPhase) go to 1300
            if(isf(ia).eq.isf(j)) then
              do k=1,NAtActive
                if(.not.LAtActive(k)) cycle
                ja=IAtActive(k)
                if(j.eq.ja) then
                  ai(ia)=ai(ia)+ai(ja)
                  isf(ja)=0
                  NMerge=NMerge+1
                  go to 1300
                endif
              enddo
            endif
1300        j=j+1
            go to 1100
          else
            cycle
          endif
        enddo
      enddo
      call EM40CleanAtoms
      if(Klic.eq.0.and.NMerge.gt.0) then
        if(NMerge.eq.1) then
          TextInfo(1)='           One atom has'
        else
          write(Cislo,FormI15) NMerge
          call Zhusti(Cislo)
          TextInfo(1)='           '//Cislo(:idel(Cislo))//
     1                ' atoms have'
        endif
        TextInfo(1)(idel(TextInfo(1))+1:)=' been merged.'
        NInfo=1
        WaitTime=10000
        call FeInfoOut(-1.,-1.,'INFORMATION:','L')
      endif
9999  return
      end
      subroutine EM40DeleteAtoms
      use EditM40_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      id=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(id.ne.3) call FeYesNoAll(-1.,-1.,'Do you want to delete '//
     1                  'atom '//atom(ia)(1:idel(atom(ia)))//'?',2,id)
          if(id.eq.4) then
            exit
          else if(id.eq.2) then
            cycle
          else
            isf(ia)=0
            call CrlAtomNamesSave(Atom(ia),'#delete#',1)
          endif
        endif
      enddo
      call EM40CleanAtoms
      return
      end
      subroutine EM40CleanAtoms
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      j=0
      NAtMolPosun=0
      NAtMolDel=0
      NAtIndDel=0
      do i=1,NAtAll
        if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) then
          j=j+1
          cycle
        endif
        isw=iswa(i)
        ksw=kswa(i)
        if(isf(i).ne.0) then
          j=j+1
          if(i.ne.j) call AtSun(i,i,j)
        else
          if(i.le.NAtInd) then
            NAtIndLen(isw,ksw)=NAtIndLen(isw,ksw)-1
            NAtIndDel=NAtIndDel+1
          else
            im=(kmol(i)-1)/mxp+1
            iam(im)=iam(im)-KPoint(im)
            NAtMolLen(isw,ksw)=NAtMolLen(isw,ksw)-KPoint(im)
            NAtPosLen(isw,ksw)=NAtPosLen(isw,ksw)-mam(im)*KPoint(im)
            NAtMolDel=NAtMolDel+KPoint(im)
            NAtMolPosun=NAtMolPosun+mam(im)*KPoint(im)
          endif
        endif
      enddo
      if(NMolec.gt.0) then
        call AtSun(NAtMolFr(1,1)-NAtIndDel,NAtAll-NAtMolDel-NAtIndDel,
     1             NAtMolFr(1,1)-NAtMolPosun-NAtIndDel)
        call DelMol
      endif
      call EM40UpdateAtomLimits
      return
      end
      subroutine EM40MolAtomsToAtomic
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension qrat(3)
      logical MolBrat(mxm),EqIgCase
      call SetLogicalArrayTo(MolBrat,mxm,.false.)
      do 1020i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          do im=1,NMolec
            if(kswmol(im).ne.KPhase) cycle
            if(im.eq.1) then
              NaFrom=NAtMolFr(1,1)
              NaTo=NaFrom+iam(im)/KPoint(im)-1
            else
              NaFrom=NaFrom+iam(im-1)
              NaTo=NaFrom+iam(im)/KPoint(im)-1
            endif
            if(ia.ge.NaFrom.and.ia.le.NaTo) then
              MolBrat(im)=.true.
              go to 1020
            endif
          enddo
        endif
1020  continue
      ibp=NAtMolFr(1,1)
      iap=NAtPosFr(1,1)
      call trortho(0)
      i=0
      do im=1,NMolec
        if(.not.MolBrat(im)) go to 2900
        do id=0,iam(im)-1
          ia=iap+id
          ib=ibp+id
          do j=1,NAtActive
            if(LAtActive(j)) then
              if(ib.eq.IAtActive(j)) go to 1060
            endif
          enddo
          cycle
1060      do j=1,kpoint(im)
            isf(ib)=0
            do ji=1,mam(im)
              kmol(ia)=-kmol(ia)
              if(TypeModFun(ia).eq.1) TypeModFun(ia)=0
              call qbyx(x(1,ia),qrat,iswa(ia))
              kk=0
              do k=1,KModA(1,ia)
                if(k.gt.KModA(1,ia)-NDimI(KPhase).and.KFA(1,ia).ne.0)
     1            then
                  kk=kk+1
                  ax(k,ia)=ax(k,ia)+qrat(kk)-qcnt(kk,ia)
                else
                  fik=0.
                  do m=1,NDimI(KPhase)
                    fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
                  enddo
                  sinfik=sin(pi2*fik)
                  cosfik=cos(pi2*fik)
                  xp=ax(k,ia)
                  yp=ay(k,ia)
                  ax(k,ia)= xp*cosfik+yp*sinfik
                  ay(k,ia)=-xp*sinfik+yp*cosfik
                  sxp=sax(k,ia)**2
                  syp=say(k,ia)**2
                  sax(k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
                  say(k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
                endif
              enddo
              do k=1,KModA(2,ia)
                if(k.eq.KModA(2,ia).and.KFA(2,ia).ne.0) then
                  uy(1,k,ia)=uy(1,k,ia)+qrat(1)-qcnt(1,ia)
                else
                  fik=0.
                  do m=1,NDimI(KPhase)
                    fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
                  enddo
                  sinfik=sin(pi2*fik)
                  cosfik=cos(pi2*fik)
                  do l=1,3
                    xp=ux(l,k,ia)
                    yp=uy(l,k,ia)
                    ux(l,k,ia)= xp*cosfik+yp*sinfik
                    uy(l,k,ia)=-xp*sinfik+yp*cosfik
                    sxp=sux(l,k,ia)**2
                    syp=suy(l,k,ia)**2
                    sux(l,k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
                    suy(l,k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
                  enddo
                endif
              enddo
              do k=1,KModA(3,ia)
                fik=0.
                do m=1,NDimI(KPhase)
                  fik=fik+(qrat(m)-qcnt(m,ia))*float(kw(m,k,KPhase))
                enddo
                sinfik=sin(pi2*fik)
                cosfik=cos(pi2*fik)
                do l=1,6
                  xp=bx(l,k,ia)
                  yp=by(l,k,ia)
                  bx(l,k,ia)= xp*cosfik+yp*sinfik
                  by(l,k,ia)=-xp*sinfik+yp*cosfik
                  sxp=sbx(l,k,ia)**2
                  syp=sby(l,k,ia)**2
                  sbx(l,k,ia)=sqrt(sxp*cosfik**2+syp*sinfik**2)
                  sby(l,k,ia)=sqrt(sxp*sinfik**2+syp*cosfik**2)
                enddo
              enddo
              ia=ia+iam(im)/KPoint(im)
            enddo
            ib=ib+iam(im)/KPoint(im)
          enddo
        enddo
2900    iap=iap+iam(im)*mam(im)
        ibp=ibp+iam(im)
      enddo
      do i=NAtInd+1,NAtCalc
        if(kswa(i).ne.KPhase) cycle
        isw=iswa(i)
        kam=NAtIndTo(isw,KPhase)+1
        if(kmol(i).lt.0) then
          im=(-kmol(i)-1)/mxp+1
          call atsave(i,0)
          call atsun(kam,i-1,kam+1)
          call atsave(kam,1)
          NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
          NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)-1
          call EM40UpdateAtomLimits
          if(mam(im).eq.1) then
            idl=idel(Atom(kam))
            if(EqIgCase(Atom(kam)(idl:idl),'a'))
     1        Atom(kam)(idl:idl)=' '
          endif
          kmol(i)=0
        endif
      enddo
      i=NAtMolFr(1,1)
3200  if(i.gt.NAtAll) go to 4000
      ji=kmol(i)
      im=(ji-1)/mxp+1
      ksw=kswmol(im)
      isw=iswmol(im)
      if(isf(i).le.0) then
        call atsun(i+1,NAtAll,i)
        NAtMolLen(isw,ksw)=NAtMolLen(isw,ksw)-1
        iam(im)=iam(im)-1
        NAtAll=NAtAll-1
      else
        i=i+1
      endif
      go to 3200
4000  call EM40UpdateAtomLimits
      call delmol
      call trortho(1)
      n=NAtInd+NAtMol
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
      NAtActive=0
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        NAtActive=NAtActive+1
        IAtActive(NAtActive)=i
        NameAtActive(NAtActive)=Atom(i)
      enddo
      if(NMolecLenAll(KPhase).gt.0) then
        iak=NAtMolFrAll(KPhase)-1
        do im=NMolecFrAll(KPhase),NMolecToAll(KPhase)
          iap=iak+1
          iak=iap+iam(im)/KPoint(im)-1
          do ia=iap,iak
            NAtActive=NAtActive+1
            IAtActive(NAtActive)=ia
            NameAtActive(NAtActive)=Atom(ia)
          enddo
          iak=iak+(KPoint(im)-1)*(iak-iap+1)
        enddo
      endif
      call SetLogicalArrayTo(LAtActive,NAtActive,.true.)
      call EM40MergeAtomsRun(.1,ich)
9999  return
      end
      subroutine EM40ResetModMag
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 Veta,t256
      integer KModMx(5),EdwStateQuest
      logical UseKMod(5),CrwLogicQuest,Randomly
      real :: AmpMax(5)=(/.1,.01,.001,.1,.1/)
      KModMx=0
      do i=1,NAtActive
        if(.not.LAtActive(i)) cycle
        ia=IAtActive(i)
        do j=1,3
          KModMx(j)=max(KModMx(j),KModA(j,ia))
        enddo
        if(KModMx(4).le.0.and.MagPar(ia).ge.1) KModMx(4)=1
        KModMx(5)=max(KModMx(5),MagPar(ia))
      enddo
      ich=0
      id=NextQuestId()
      xqd=550.
      il=7
      Veta='Reset modulation/magnetic parameters'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=0
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='O%ccupational modulation'
      t256='=> maximal amplitude'
      tpome=tpom+FeTxLengthUnder(Veta)+35.
      xpome=tpome+FeTxLengthUnder(t256)+35.
      dpom=70.
      tpoml=xpome+dpom+5.
      do i=1,5
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        call FeQuestCrwOpen(CrwLastMade,.true.)
        call FeQuestEdwMake(id,tpome,il,xpome,il,t256,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,AmpMax(i),.false.,.false.)
        if(i.eq.1) then
          nCrwOcc=CrwLastMade
          nEdwOcc=EdwLastMade
          Veta='%Positional parameters'
        else if(i.eq.2) then
          nCrwX=CrwLastMade
          nEdwX=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'angstroems','L','N')
          nLblX=LblLastMade
          Veta='A%DP parameters'
        else if(i.eq.3) then
          nCrwADP=CrwLastMade
          nEdwADP=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'angstroems^2','L','N')
          nLblADP=LblLastMade
          Veta='%Magnetic parameters (k=0)'
        else if(i.eq.4) then
          nCrwMag0=CrwLastMade
          nEdwMag0=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'Bohr magnetons','L','N')
          nLblMag0=LblLastMade
          Veta='Ma%gnetic parameters (k<>0)'
        else if(i.eq.5) then
          nCrwMag=CrwLastMade
          nEdwMag=EdwLastMade
          call FeQuestLblMake(id,tpoml,il,'Bohr magnetons','L','N')
          nLblMag=LblLastMade
        endif
        if(KModMx(i).le.0) then
          UseKMod(i)=.false.
          call FeQuestCrwDisable(CrwLastMade)
          call FeQuestEdwDisable(EdwLastMade)
          call FeQuestLblDisable(LblLastMade)
        else
          UseKMod(i)=.true.
        endif
      enddo
      Veta='%Reset parameters to pmax (pmax=maximal ampitude)'
      il=il+1
      call FeQuestLinkaMake(id,il)
      xpom=5.
      tpom=xpom+CrwgXd+10.
      il=-10*il
      do i=1,2
        il=il-7
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,1)
        call FeQuestCrwOpen(CrwLastMade,i.eq.2)
        if(i.eq.1) then
          Veta='R%eset parameters randomly in the interval (-pmax,pmax)'
          nCrwMax=CrwLastMade
        else
          nCrwRandom=CrwLastMade
        endif
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwOcc) then
        UseKMod(1)=CrwLogicQuest(nCrwOcc)
        if(UseKMod(1)) then
          if(EdwStateQuest(nEdwOcc).ne.EdwOpened)
     1      call FeQuestRealEdwOpen(nEdwOcc,AmpMax(1),.false.,.false.)
        else
          if(EdwStateQuest(nEdwOcc).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwOcc,AmpMax(1))
            call FeQuestEdwDisable(nEdwOcc)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwX) then
        UseKMod(2)=CrwLogicQuest(nCrwX)
        if(UseKMod(2)) then
          if(EdwStateQuest(nEdwX).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwX,AmpMax(2),.false.,.false.)
            call FeQuestLblOn(nLblX)
          endif
        else
          if(EdwStateQuest(nEdwX).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwX,AmpMax(2))
            call FeQuestEdwDisable(nEdwX)
            call FeQuestLblDisable(nLblADP)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwADP) then
        UseKMod(3)=CrwLogicQuest(nCrwADP)
        if(UseKMod(3)) then
          if(EdwStateQuest(nEdwADP).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwADP,AmpMax(3),.false.,.false.)
            call FeQuestLblOn(nLblADP)
          endif
        else
          if(EdwStateQuest(nEdwADP).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwADP,AmpMax(3))
            call FeQuestEdwDisable(nEdwADP)
            call FeQuestLblDisable(nLblADP)
          endif
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMag0) then
        UseKMod(4)=CrwLogicQuest(nCrwMag0)
        if(UseKMod(4)) then
          if(EdwStateQuest(nEdwMag0).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwMag0,AmpMax(4),.false.,.false.)
            call FeQuestLblOn(nLblMag0)
          endif
        else
          if(EdwStateQuest(nEdwMag0).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwMag0,AmpMax(4))
            call FeQuestEdwDisable(nEdwMag0)
            call FeQuestLblDisable(nLblMag0)
          endif
        endif
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwMag) then
        UseKMod(5)=CrwLogicQuest(nCrwMag)
        if(UseKMod(5)) then
          if(EdwStateQuest(nEdwMag).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwMag,AmpMax(5),.false.,.false.)
            call FeQuestLblOn(nLblMag)
          endif
        else
          if(EdwStateQuest(nEdwMag).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwMag,AmpMax(5))
            call FeQuestEdwDisable(nEdwMag)
            call FeQuestLblDisable(nLblMag)
          endif
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        Randomly=CrwLogicQuest(nCrwRandom)
        nEdw=nEdwOcc
        do i=1,5
          if(EdwStateQuest(nEdw).eq.EdwOpened)
     1      call FeQuestRealFromEdw(nEdw,AmpMax(i))
          nEdw=nEdw+1
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(Randomly) call Random_Seed()
      do i=1,NAtActive
        if(.not.LAtActive(i)) cycle
        ia=IAtActive(i)
        do j=1,5
          if(.not.UseKMod(j).or.(j.le.3.and.KModA(j,ia).le.0).or.
     1       (j.eq.4.and.MagPar(ia).le.0)) cycle
          if(j.eq.1) then
            do k=1,KModA(j,ia)
              if(Randomly) then
                call Random_Number(pom)
                pom=2.*pom-1.
                ax(k,ia)=AmpMax(1)*pom
                call Random_Number(pom)
                pom=2.*pom-1.
                ay(k,ia)=AmpMax(1)*pom
              else
                ax(k,ia)=AmpMax(1)
                ay(k,ia)=AmpMax(1)
              endif
            enddo
          else if(j.eq.2) then
            do k=1,KModA(j,ia)
              do l=1,3
                ppp=1./CellPar(l,iswa(ia),kswa(ia))
                if(Randomly) then
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  ux(l,k,ia)=AmpMax(2)*pom*ppp
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  uy(l,k,ia)=AmpMax(2)*pom*ppp
                else
                  ux(l,k,ia)=AmpMax(2)*ppp
                  uy(l,k,ia)=AmpMax(2)*ppp
                endif
              enddo
            enddo
          else if(j.eq.3) then
            do k=1,KModA(j,ia)
              do l=1,6
                if(Randomly) then
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  bx(l,k,ia)=AmpMax(3)*pom
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  by(l,k,ia)=AmpMax(3)*pom
                else
                  bx(l,k,ia)=AmpMax(3)
                  by(l,k,ia)=AmpMax(3)
                endif
              enddo
            enddo
          else if(j.eq.4) then
            do l=1,3
              if(Randomly) then
                call Random_Number(pom)
                pom=2.*pom-1.
                sm0(l,ia)=pom*AmpMax(4)
              else
                sm0(l,ia)=AmpMax(4)
              endif
            enddo
          else if(j.eq.5) then
            do k=2,MagPar(ia)
              do l=1,3
                if(Randomly) then
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  smx(l,k-1,ia)=AmpMax(5)*pom
                  call Random_Number(pom)
                  pom=2.*pom-1.
                  smy(l,k-1,ia)=AmpMax(5)*pom
                else
                  smx(l,k-1,ia)=AmpMax(5)
                  smy(l,k-1,ia)=AmpMax(5)
                endif
              enddo
            enddo
          endif
        enddo
      enddo
9999  return
      end
      subroutine EM40SelAtomsCheck
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*256 EdwStringQuest
      integer RolMenuSelectedQuest
      if(nEdwAtoms.eq.0) go to 9999
      if(EdwStringQuest(nEdwAtoms).eq.' ') then
        call FeQuestButtonDisable(nButtInclAtoms)
        call FeQuestButtonDisable(nButtExclAtoms)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclAtoms)
        call FeQuestButtonOff(nButtExclAtoms)
      endif
      if(RolMenuSelectedQuest(nRolMenuTypes).eq.0) then
        call FeQuestButtonDisable(nButtInclTypes)
        call FeQuestButtonDisable(nButtExclTypes)
        call FeQuestActiveUpdate
      else
        call FeQuestButtonOff(nButtInclTypes)
        call FeQuestButtonOff(nButtExclTypes)
      endif
      if(nRolMenuMol.gt.0) then
        if(RolMenuSelectedQuest(nRolMenuMol).eq.0) then
          call FeQuestButtonDisable(nButtInclMol)
          call FeQuestButtonDisable(nButtExclMol)
          call FeQuestActiveUpdate
        else
          call FeQuestButtonOff(nButtInclMol)
          call FeQuestButtonOff(nButtExclMol)
        endif
      endif
9999  return
      end
      subroutine EM40NewAtH(ich)
      use EditM40_mod
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'editm40.cmn'
      common/KeepQuest/ nEdwList,nEdwCentr,nEdwHDist,nEdwNNeigh,
     1           nEdwNeighFirst,nEdwNeighLast,nEdwHFirst,nEdwHLast,
     2           nEdwAnchor,nEdwTorsAngle,nCrwUseAnchor,nButtSelect,
     3           AtomToBeFilled,nEdwNH,nEdwARiding,Recalculate,
     4           NButtLocate,NButtApply,BlowUpFactor,SaveKeepCommands,
     5           KeepTypeOld,KeepDistHOld,KeepNNeighOld,KeepNHOld,
     6           MapAlreadyUsed,iat,iap,iak,iako,iaLast,napp,
     7           TryAutomaticRun
      character*256 EdwStringQuest,t256
      character*80 Veta,AtFill(5)
      character*8 at
      character*2 nty
      logical SaveKeepCommands,CrwLogicQuest,EqIgCase,FeYesNo,
     1        MapAlreadyUsed,Recalculate,TryAutomaticRun,PridavaVodiky,
     2        TryAutomaticRunIn,FeYesNoHeader
      integer AtomToBeFilled,EdwStateQuest,RolMenuSelectedQuest
      external RefKeepUpdateQuest,FeVoid
      real KeepDistHOld
      save /KeepQuest/
      KeepDistHDefault=-1.
      SaveKeepCommands=.true.
      ich=0
      BlowUpFactor=1.2
      MapAlreadyUsed=.false.
      isfhp=isfh(KPhase)
      if(isfhp.le.0) then
        pom=100000.
        do i=1,NAtFormula(KPhase)
          if(ffbasic(1,i,KPhase).lt.pom) then
            pom=AtNum(i,KPhase)
            isfhp=i
          endif
        enddo
      endif
      PridavaVodiky=EqIgCase(AtType(isfhp,KPhase),'H')
1030  id=NextQuestId()
      il=3
      if(PridavaVodiky) il=il+1
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Adding of "hydrogen" '//
     1                   'atoms',0,LightGray,0,0)
      il=1
      Veta='"%Hydrogen" atomic type'
      xpom=5.
      dpom=40.
      tpom=xpom+dpom+10.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nRolMenuHType=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,AtType(1,KPhase),
     1                        NAtFormula(KPhase),isfhp)
      il=il+1
      tpom=xpom+20.
      Veta='%generate the "keep" commands for REFINE'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwSaveKeep=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,SaveKeepCommands)
      if(PridavaVodiky) then
        il=il+1
        Veta='%try automatic run for non-"N" and non-"O" atoms'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwAutomatic=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,SaveKeepCommands)
      else
        nCrwAutomatic=0
      endif
      il=il+1
      Veta='Default value for %ADP ext. factor'
      dpom=100.
      tpom=xpom+dpom+5.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFactor=EdwLastMade
      call FeQuestRealEdwOpen(nEdwFactor,BlowUpFactor,.false.,
     1                        .false.)
1100  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventRolMenu.and.CheckNumber.eq.nRolMenuHType)
     1  then
        isfhp=RolMenuSelectedQuest(nRolMenuHType)
        go to 1100
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1100
      endif
      if(ich.eq.0) then
        SaveKeepCommands=CrwLogicQuest(nCrwSaveKeep)
        if(nCrwAutomatic.le.0) then
          TryAutomaticRunIn=.false.
        else
          TryAutomaticRunIn=CrwLogicQuest(nCrwAutomatic)
        endif
        call FequestRealFromEdw(nEdwFactor,BlowUpFactor)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      call iom40(1,0,fln(:ifln)//'.m40')
      xdq=600.
      Veta='Lo%cate position in map'
      dpoma=FeTxLengthUnder(Veta)+10.
      id=NextQuestId()
      if(SaveKeepCommands) then
        IgnoreW=.true.
        call RefOpenCommands
        IgnoreW=.false.
      endif
      nEdwCentr=0
      napp=0
      iaw=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(.not.TryAutomaticRunIn.or.
     1       EqIgCase(AtType(isf(ia),KPhase),'N').or.
     2       EqIgCase(AtType(isf(ia),KPhase),'O')) iaw=iaw+1
          iaLast=ia
        endif
      enddo
      iaa=0
      if(TryAutomaticRunIn) then
        NRepeatFrom=0
        if(iaw.gt.0) then
          NRepeatTo=1
        else
          NRepeatTo=0
        endif
      else
        NRepeatFrom=1
        NRepeatTo=1
      endif
      if(NKeepMax.le.0) call ReallocKeepCommands(1,8)
      do IRepeat=NRepeatFrom,NRepeatTo
        TryAutomaticRun=TryAutomaticRunIn.and.IRepeat.eq.0
        do iat=1,NAtActive
          if(ISymmBasic.gt.0) call CrlRestoreSymmetry(ISymmBasic)
          call SpecAt
          if(.not.LAtActive(iat)) cycle
          ia=IAtActive(iat)
          if(EqIgCase(AtType(isf(ia),KPhase),'N').or.
     1       EqIgCase(AtType(isf(ia),KPhase),'O')) then
            if(TryAutomaticRun) cycle
          else
            if(TryAutomaticRunIn.and.IRepeat.ne.0) cycle
          endif
          KeepAngleH(1)=180.
          iaa=iaa+1
          isw=iswa(ia)
          if(kmol(ia).le.0) then
            im=0
            iap=1
            iak=NAtInd+NAtPos
            if(NMolec.gt.0) call CrlRestoreSymmetry(ISymmBasic)
          else
            im=(kmol(ia)-1)/mxp+1
            iap=NAtMolFr(1,1)
            do i=1,im-1
              iap=iap+iam(i)
            enddo
            iak=iap+iam(im)-1
            call CrlRestoreSymmetry(ISymmMolec(im))
          endif
          call EM40SetKeepForNewH(ia,iap,iak,isfhp)
          NNeigh=KeepNNeigh(1)
          UseAnchor=.false.
          if(TryAutomaticRun) then
            if(.not.EqIgCase(AtType(isf(ia),KPhase),'N').and.
     1         .not.EqIgCase(AtType(isf(ia),KPhase),'O')) then
              if(KeepNH(1).le.0.or.KeepAtNeigh(1,1).eq.' ') cycle
              NAtMax=KeepNH(1)+KeepNNeigh(1)
              call EM40NewAtomHMake(1,ia,im,isw,ich)
              NButtLocate=0
              NButtApply=1
              CheckNumber=1
              cycle
            endif
          endif
          il=8
          Veta='Adding "hydrogen" atoms for "'//
     1         atom(ia)(:idel(atom(ia)))//'"'
          call FeQuestCreate(id,-1.,-1.,xdq,il,Veta,0,LightGray,-1,-1)
          il=0
          xpom=5.
          tpom=xpom+10.+CrwgXd
          Veta='T%etrahedral'
          xpom1=tpom+FeTxLengthUnder(Veta)+66.
          nTypeHydro=3
          do i=1,nTypeHydro
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,1)
            if(i.eq.1) then
              Veta='T%rigonal'
              nCrwTetra=CrwLastMade
            else if(i.eq.2) then
              Veta='A%pical'
              nCrwTriangl=CrwLastMade
            else if(i.eq.3) then
              nCrwApical=CrwLastMade
            endif
          enddo
          il=1
          dpom=100.
          xpom=xpom1
          tpom=xpom+dpom+10.
          Veta='H di%st.'
          do i=1,2
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          1)
            if(i.eq.1) then
              nEdwHDist=EdwLastMade
              xpom=tpom+FeTxLengthUnder(Veta)+59.
              xpom=xpom1+dpoma+2.*(xdq*.5-xpom1-dpoma)
              Veta='%ADP ext. factor'
              tpom=xpom+dpom+10.
              xpom2=xpom
            else if(i.eq.2) then
              nEdwFactor=EdwLastMade
            endif
          enddo
          il=il+1
          Veta='%Neighbor(s)'
          xpom=xpom1
          dpom1=25.
          tpom=xpom+dpom1+20.
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom1,EdwYd,1)
          nEdwNNeigh=EdwLastMade
          ilh=il
          xpomh=xpom2+dpom*.5
          call FeQuestLblMake(id,xpomh,il,'Hydrogen(s)','C','N')
          xpom=xpom1
          ilp=il
          do j=1,2
            tpom=xpom+dpom+10.
            do i=1,5
              il=il+1
              write(Veta,'(i1,a2)') i,nty(i)
              call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                            EdwYd,1)
              if(j.eq.1) then
                if(i.eq.1) nEdwNeighFirst=EdwLastMade
              else
                if(i.eq.1) nEdwHFirst=EdwLastMade
              endif
            enddo
            if(j.eq.1) then
              nEdwNeighLast=EdwLastMade
              il=ilp
              xpom=xpom2
            else
              nEdwHLast=EdwLastMade
            endif
          enddo
          il=il-1
          Veta='%Use anchoring    =>'
          xpom=5.
          tpom=xpom+CrwXd+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwUseAnchor=CrwLastMade
          Veta='Anch%or'
          xpom=xpom1
          do i=1,2
            tpom=xpom+dpom+10.
            if(i.eq.1) then
              j=1
            else
              j=0
            endif
            call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          j)
            if(i.eq.1) then
              nEdwAnchor=EdwLastMade
              Veta='Tors.an%gle'
              xpom=xpom2
            else if(i.eq.2) then
              nEdwTorsAngle=EdwLastMade
            endif
          enddo
          il=il+1
          xpom=xpom1
          Veta='Lo%cate position in map'
          call FeQuestButtonMake(id,xpom,il,dpoma,ButYd,Veta)
          nButtLocate=ButtonLastMade
          xpom=xpom2
          Veta='Se%lect neighbors'
          call FeQuestButtonMake(id,xpom,il,dpoma,ButYd,Veta)
          nButtSelect=ButtonLastMade
          il=il+1
          Veta='Avoi%d'
          if(ia.ne.iaLast) Veta=Veta(:idel(Veta))//'->Go to next'
          dpomb=FeTxLengthUnder('%Quit')+20.
          dpom=dpoma
          xpom=xpom1
          do i=1,3
            call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
            if(i.eq.1) then
              Veta='%Quit'
              nButtAvoid=ButtonLastMade
            else if(i.eq.2) then
              Veta='Appl%y'
              if(ia.ne.iaLast) Veta=Veta(:idel(Veta))//'->Go to next'
              nButtQuit=ButtonLastMade
            else if(i.eq.3) then
              nButtApply=ButtonLastMade
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            if(i.eq.1) then
              dpom=dpomb
              xpom=xdq*.5-dpomb*.5
            else if(i.eq.2) then
              dpom=dpoma
              xpom=xpom2
            endif
          enddo
          call FeQuestMouseToButton(nButtApply)
2350      KeepTypeOld=0
2400      KeepTypeMain=KeepType(1)/10
          KeepTypeAdd=mod(KeepType(1)-1,10)+1
          KeepTypeMainOld=KeepTypeOld/10
          KeepTypeAddOld=mod(KeepTypeOld-1,10)+1
          if(KeepType(1).ne.KeepTypeOld) then
            nCrw=nCrwTetra
            do i=1,nTypeHydro
              call FeQuestCrwOpen(nCrw,i.eq.KeepTypeAdd)
              nCrw=nCrw+1
            enddo
          endif
          if(KeepType(1).eq.IdKeepHApical) then
            NAtMax=6
            KeepNH(1)=1
            UseAnchor=.false.
          else if(KeepType(1).eq.IdKeepHTetraHed) then
            NAtMax=4
            if(KeepTypeOld.eq.IdKeepHTriangl) then
!              NNeigh=NNeigh+1
            else if(KeepTypeOld.eq.IdKeepHApical) then
              NNeigh=min(NNeigh,NAtMax-1)
            endif
            KeepNH(1)=4-NNeigh
          else if(KeepType(1).eq.IdKeepHTriangl) then
            NAtMax=3
            if(KeepTypeOld.eq.IdKeepHTetraHed) then
              if(NNeigh.eq.1) then
                KeepNH(1)=KeepNH(1)-1
              else
                NNeigh=NNeigh-1
              endif
            else if(KeepTypeOld.eq.IdKeepHApical) then
              NNeigh=min(NNeigh,NAtMax-1)
            endif
            KeepNH(1)=3-NNeigh
          endif
          KeepNNeigh(1)=NNeigh
          if(UseAnchor) KeepNNeigh(1)=KeepNNeigh(1)+1
2500      if(KeepNH(1).ne.NAtMax-1) then
            call FeQuestCrwClose(nCrwUseAnchor)
            call FeQuestButtonClose(nButtLocate)
            UseAnchor=.false.
          else if(im.gt.0) then
            call FeQuestButtonDisable(nButtLocate)
          endif
          if(.not.UseAnchor) then
            call FeQuestEdwClose(nEdwAnchor)
            call FeQuestEdwClose(nEdwTorsAngle)
          endif
          call FeQuestIntEdwOpen(nEdwNNeigh,NNeigh,.false.)
          call FeQuestEudOpen(nEdwNNeigh,1,NAtMax-1,1,0.,0.,0.)
          call FeQuestRealEdwOpen(nEdwHDist,KeepDistH(1),.false.,
     1                            .false.)
          call FeQuestRealEdwOpen(nEdwFactor,BlowUpFactor,.false.,
     1                            .false.)
          do j=1,2
            if(j.eq.1) then
              nEdw=nEdwNeighFirst
              NMaxP=NNeigh
            else
              nEdw=nEdwHFirst
              NMaxP=KeepNH(1)
            endif
            do i=1,5
              if(i.le.NMaxP) then
                if(j.eq.1) then
                  Veta=KeepAtNeigh(1,i)
                else
                  Veta=KeepAtH(1,i)
                endif
                if(Veta.eq.' ') then
                  write(Cislo,FormI15) i
                  call Zhusti(Cislo)
                  Veta='H'//Cislo(:idel(Cislo))//
     1                          Atom(ia)(:idel(Atom(ia)))
                  KeepAtH(1,i)=Veta
                endif
                call FeQuestStringEdwOpen(nEdw,Veta)
              else
                call FeQuestEdwClose(nEdw)
              endif
              nEdw=nEdw+1
            enddo
          enddo
          if(KeepNH(1).eq.NAtMax-1) then
            UseAnchor=NAtMax.lt.KeepNNeigh(1)+KeepNH(1)
            call FeQuestCrwOpen(nCrwUseAnchor,UseAnchor)
            if(im.eq.0) then
              call FeQuestButtonOpen(nButtLocate,ButtonOff)
              if(.not.ExistM90) call FeQuestButtonDisable(nButtLocate)
            endif
          endif
          if(UseAnchor) then
            call FeQuestStringEdwOpen(nEdwAnchor,KeepAtAnchor(1))
            call FeQuestRealEdwOpen(nEdwTorsAngle,KeepAngleH(1),.false.,
     1                              .false.)
          endif
          call FeQuestButtonOpen(nButtSelect,ButtonOff)
          KeepTypeOld=KeepType(1)
          KeepNHOld=KeepNH(1)
          KeepNNeighOld=KeepNNeigh(1)
3000      MakeExternalCheck=1
          call FeQuestEventWithCheck(id,ich,RefKeepUpdateQuest,
     1                               FeVoid)
          if(CheckType.eq.EventCrw) then
            if(CheckNumber.eq.nCrwTetra) then
              KeepType(1)=IdKeepHTetraHed
            else if(CheckNumber.eq.nCrwTriangl) then
              KeepType(1)=IdKeepHTriangl
            else if(CheckNumber.eq.nCrwApical) then
              KeepType(1)=IdKeepHApical
            else if(CheckNumber.eq.nCrwUseAnchor) then
              UseAnchor=CrwLogicQuest(nCrwUseAnchor)
              if(UseAnchor) then
                KeepNNeigh(1)=NNeigh+1
              else
                KeepNNeigh(1)=NNeigh
              endif
              EventType=EventEdw
              EventNumber=nEdwAnchor
              go to 2500
            endif
            go to 2400
          else if(CheckType.eq.EventEdw) then
            if(CheckNumber.eq.nEdwNNeigh) then
              call FeQuestIntFromEdw(nEdwNNeigh,NNeigh)
              if(KeepType(1).eq.IdKeepHTetraHed.or.
     1           KeepType(1).eq.IdKeepHTriangl) then
                KeepNH(1)=NAtMax-NNeigh
                KeepNNeigh(1)=NNeigh
                if(UseAnchor.and.KeepNNeigh(1).eq.1)
     1            KeepNNeigh(1)=KeepNNeigh(1)+1
              else if(KeepType(1).eq.IdKeepHApical) then
                KeepNNeigh(1)=NNeigh
              endif
              if(KeepNH(1).ne.KeepNHOld.or.
     1           KeepNNeigh(1).ne.KeepNNeighOld) then
                EventType=EventEdw
                EventNumber=nEdwNNeigh
                go to 2500
              endif
            else if(CheckNumber.ge.nEdwNeighFirst.and.
     1              CheckNumber.le.nEdwNeighLast) then
              i=CheckNumber-nEdwNeighFirst+1
              KeepAtNeigh(1,i)=EdwStringQuest(CheckNumber)
              KeepNAtNeigh(1,i)=ktatmol(KeepAtNeigh(1,i))
            else if(CheckNumber.ge.nEdwHFirst.and.
     1              CheckNumber.le.nEdwHLast) then
              i=CheckNumber-nEdwHFirst+1
              KeepAtH(1,i)=EdwStringQuest(CheckNumber)
              KeepNAtH(1,i)=ktatmol(KeepAtH(1,i))
            else if(CheckNumber.eq.nEdwAnchor) then
              KeepAtAnchor(1)=EdwStringQuest(CheckNumber)
              KeepNAtAnchor(1)=ktatmol(KeepAtAnchor(1))
            else if(CheckNumber.eq.nEdwHDist) then
              call FequestRealFromEdw(nEdwHDist,KeepDistH(1))
              if(EqIgCase(AtType(isf(ia),KPhase),'C')) then
                KeepDistHDefault(1)=KeepDistH(1)
              else if(EqIgCase(AtType(isf(ia),KPhase),'N')) then
                KeepDistHDefault(2)=KeepDistH(1)
              else if(EqIgCase(AtType(isf(ia),KPhase),'O')) then
                KeepDistHDefault(3)=KeepDistH(1)
              endif
            else if(CheckNumber.eq.nEdwFactor) then
              call FequestRealFromEdw(nEdwFactor,BlowUpFactor)
            endif
            go to 3000
          else if(CheckType.eq.EventButton.and.
     1            CheckNumber.eq.nButtSelect) then
            EventType=EventEdw
            if(AtomToBeFilled.eq.SelectedAnchor) then
              EventNumber=nEdwAnchor
              Veta=EdwStringQuest(nEdwNeighFirst)
              if(Veta.ne.' ') then
                call SelNeighborAtoms('Select the anchor atom',3.,Veta,
     1                                AtFill,1,n,isw,ich)
                if(ich.ne.0) go to 3000
              else
                call FeChybne(-1.,-1.,'first you have to define the '//
     1                       'neighbor atom.',' ',SeriousError)
                EventNumber=nEdwNeighFirst
                go to 3000
              endif
              KeepAtAnchor(1)=AtFill(1)
            else
              nsp=NSymm(KPhase)
              if(AtomToBeFilled.eq.SelectedNeigh) then
                EventNumber=nEdwNeighFirst
                nmax=NNeigh
                Veta='Select the neighbor atoms'
              else if(AtomToBeFilled.eq.SelectedHydro) then
                EventNumber=nEdwHFirst
                nmax=KeepNH(1)
                Veta='Select the hydrogen atom'
                if(KeepNH(1).gt.1) Veta=Veta(:idel(Veta))//'s'
                NSymm(KPhase)=1
              endif
              call SelNeighborAtoms(Veta,3.,KeepAtCentr(1),AtFill,nmax,
     1                              n,isw,ich)
              if(ich.ne.0) go to 3000
              NSymm(KPhase)=nsp
              if(AtomToBeFilled.eq.SelectedNeigh) then
                do i=1,NNeigh
                  if(i.le.n) then
                    KeepAtNeigh(1,i)=AtFill(i)
                    KeepNAtNeigh(1,i)=ktatmol(KeepAtNeigh(1,i))
                  else
                    KeepAtNeigh(1,i)=' '
                  endif
                enddo
              else if(AtomToBeFilled.eq.SelectedHydro) then
                do i=1,KeepNH(1)
                  j=index(AtFill(i),'#')
                  if(j.gt.0) AtFill(i)(j:)=' '
                  if(i.le.n) then
                    KeepAtH(1,i)=AtFill(i)
                  else
                    KeepAtH(1,i)=' '
                  endif
                enddo
              endif
            endif
            go to 2500
          else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)
     1      then
            Veta=' '
            if(napp.gt.0) then
              write(Veta,'(i5)') napp
              call zhusti(Veta)
              Veta='Do you want to discard '//Veta(:idel(Veta))//
     1             ' new hydrogen atom'
              if(napp.gt.1) Veta=Veta(:idel(Veta))//'s?'
              if(FeYesNo(-1.,-1.,Veta,0)) then
                ich=1
              else
                ich=-1
              endif
            else
              Veta='Do you really want quit the procedure for this'
              if(iaa.lt.iaw) then
                write(Cislo,'(i5)') iaw-iaa+1
                call zhusti(Cislo)
                Veta=Veta(:idel(Veta))//' and remaining '//
     1               Cislo(:idel(Cislo))//' atom'
                if(iaw-iaa+1.gt.1) Veta=Veta(:idel(Veta))//'s?'
              else
                Veta=Veta(:idel(Veta))//' atom?'
              endif
              if(.not.FeYesNo(-1.,-1.,Veta,0)) then
                go to 3000
              else
                ich=1
              endif
            endif
            go to 3900
          else if(CheckType.eq.EventButton.and.
     1            CheckNumber.eq.nButtAvoid) then
            ich=0
            go to 3900
          else if(CheckType.eq.EventButton.and.
     1            (CheckNumber.eq.nButtApply.or.
     2             CheckNumber.eq.nButtLocate)) then
            if(KeepNH(1).le.0) then
              ich=0
              go to 3900
            endif
            nEdw=nEdwNeighFirst
            do i=1,NNeigh
              Veta=EdwStringQuest(nEdw)
              call UprAt(Veta)
              if(Veta.eq.' ') then
                Veta='the neighbor atom'
                go to 3820
              endif
              j=index(Veta,'#')-1
              if(j.lt.0) j=idel(Veta)
              at=Veta(:j)
              call AtCheck(at,ichp,j)
              if(ichp.eq.1) go to 3810
              j=ktat(Atom(iap),iak-iap+1,at)
              if(j.le.0) go to 3800
              KeepAtNeigh(1,i)=Veta
              KeepNAtNeigh(1,i)=ktatmol(KeepAtNeigh(1,i))
              nEdw=nEdw+1
            enddo
            nEdw=nEdwHFirst
            do i=1,KeepNH(1)
              Veta=EdwStringQuest(nEdw)
              call UprAt(Veta)
              if(Veta.eq.' ') then
                Veta='the hydrogen atom'
                go to 3820
              endif
              call AtCheck(Veta,ichp,j)
              if(ichp.eq.1) go to 3810
              j=index(Veta,'#')
              if(j.gt.0) Veta=Veta(:j-1)
              KeepAtH(1,i)=Veta
              nEdw=nEdw+1
            enddo
            if(UseAnchor) then
              Veta=EdwStringQuest(nEdwAnchor)
              call UprAt(Veta)
              if(Veta.eq.' ') then
                Veta='the anchor atom'
                go to 3820
              endif
              j=index(Veta,'#')-1
              if(j.lt.0) j=idel(Veta)
              at=Veta(:j)
              call AtCheck(at,ichp,j)
              if(ichp.eq.1) go to 3810
              j=ktat(atom(iap),iak-iap+1,at)
              if(j.le.0) go to 3800
              KeepAtAnchor(1)=Veta
            else
               KeepAtAnchor(1)='#unknown#'
            endif
            if(CheckNumber.eq.nButtLocate) then
              if(SaveKeepCommands) call RefRewriteCommands(1)
              call iom40Only(1,0,fln(:ifln)//'.m40')
            endif
            call FequestRealFromEdw(nEdwHDist,KeepDistH(1))
            if(EdwStateQuest(nEdwTorsAngle).eq.EdwOpened) then
              call FeQuestRealFromEdw(nEdwTorsAngle,KeepAngleH(1))
            endif
            call FeQuestRealFromEdw(nEdwFactor,BlowUpFactor)
            call EM40NewAtomHMake(0,ia,im,isw,ich)
            if(CheckNumber.eq.nButtLocate) then
              go to 3000
            else
              go to 3900
            endif
3800        Veta='atom "'//Veta(:idel(Veta))//'" not present.'
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            go to 3830
3810        call FeChybne(-1.,-1.,'the atom name "'//Veta(:idel(Veta))//
     1                    '" contains unacceptable symbol.',' ',
     2                    SeriousError)
            go to 3830
3820        NInfo=1
            call Velka(Veta(1:1))
            TextInfo(1)=Veta(:idel(Veta))//' has not been specified.'
            Veta='Do you want to leave this atom'
            if(ia.ne.iaLast) then
              Veta=Veta(:idel(Veta))//' and go to the next one?'
            else
              Veta=Veta(:idel(Veta))//'?'
            endif
            if(FeYesNoHeader(-1.,-1.,Veta,1)) then
              ich=0
              go to 3900
            endif
3830        EventType=EventEdw
            EventNumber=nEdw
            go to 3000
          else if(CheckType.eq.EventButton.and.
     1            CheckNumber.eq.nButtLocate) then
            dd=0
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 3000
          endif
3900      call FeQuestRemove(id)
          if(ich.ne.0) then
            if(ich.gt.0) then
              call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
              call CopyFile(fln(:ifln)//'.z50',fln(:ifln)//'.m50')
              call iom50(0,0,fln(:ifln)//'.m50')
              call iom40(0,0,fln(:ifln)//'.m40')
              go to 9999
            endif
            exit
          endif
        enddo
      enddo
      if(SaveKeepCommands) call RefRewriteCommands(1)
9999  call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      if(MapAlreadyUsed) then
        t256='"'//TmpDir(:idel(TmpDir))//fln(:ifln)//'*.*"'
        call DeleteAllFiles(t256)
        call FeTmpFilesClear(t256)
      endif
      call DeleteFile(fln(:ifln)//'.z40')
      call DeleteFile(fln(:ifln)//'.z50')
      if(NMolec.gt.0.and.ISymmBasic.gt.0) then
        call CrlRestoreSymmetry(ISymmBasic)
        call CrlCleanSymmetry
      endif
      return
      end
      subroutine EM40SetKeepForNewH(ia,iap,iak,isfhp)
      use Basic_mod
      use Atoms_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      dimension xc(3),xb(3,3),uxb(3,mxw),uyb(3,mxw),x40b(3),RMiib(9)
      character*2  AtTypeCentral,AtTypeNeigh,AtTypeNeighUsed
      logical EqIgCase
      do i=1,NKeepAtMax
        KeepAt(1,i)=' '
      enddo
      KeepAtCentr(1)=' '
      KeepAtAnchor(1)=' '
      do i=1,5
        KeepAtNeigh(1,i)=' '
        if(i.le.4) KeepAtH(1,i)=' '
      enddo
      KeepNNeigh(1)=3
      KeepNH(1)=1
      KeepType(1)=IdKeepHTetraHed
      AtTypeCentral=AtType(isf(ia),KPhase)
      if(EqIgCase(AtTypeCentral,'C')) then
        DNeighH=2.4
        DNeighL=2.
        if(KeepDistHDefault(1).lt.0.) then
          KeepDistH(1)=0.96
        else
          KeepDistH(1)=KeepDistHDefault(1)
        endif
      else if(EqIgCase(AtTypeCentral,'N')) then
        DNeighH=2.4
        DNeighL=2.
        if(KeepDistHDefault(2).lt.0.) then
          KeepDistH(1)=0.87
        else
          KeepDistH(1)=KeepDistHDefault(2)
        endif
      else if(EqIgCase(AtTypeCentral,'O')) then
        DNeighH=2.5
        DNeighL=2.
        if(KeepDistHDefault(3).lt.0.) then
          KeepDistH(1)=0.82
        else
          KeepDistH(1)=KeepDistHDefault(3)
        endif
      else
        KeepDistH(1)=1.
        go to 5000
      endif
      call DistFromAtom(Atom(ia),3.,iswa(ia))
      call CopyVek(x(1,ia),xc,3)
      nh=0
      do i=1,NDist
        k=ipord(i)
        iah=ktat(Atom(iap),iak-iap+1,ADist(k))+iap-1
        if(iah.le.0) cycle
        if(DDist(k).gt.1.1) cycle
        if(SymCodeJanaDist(k).ne.' ') cycle
        if(isf(iah).eq.isfhp.and.nh.lt.ubound(KeepAtH,2)) then
          nh=nh+1
          KeepAtH(1,nh)=ADist(k)
          j=idel(SymCodeJanaDist(k))
          if(j.gt.0) KeepAtH(1,nh)=KeepAtH(1,nh)(:idel(KeepAtH(1,nh)))//
     1                             '#'//SymCodeJanaDist(k)(:j)
        else
          go to 5000
        endif
      enddo
      fnn=0.
      nn=0
      DstMin=10.
      DstMax=0.
      DstAve=0.
      AtTypeNeighUsed=' '
      do i=1,NDist
        k=ipord(i)
        ian=ktat(Atom(iap),iak-iap+1,ADist(k))+iap-1
        if(ian.le.0) then
          cycle
        else
          isfi=isf(ian)
          AtTypeNeigh=AtType(isfi,KPhase)
        endif
        if(EqIgCase(AtTypeNeigh,'H')) cycle
        if((DDist(k).gt.DNeighL.and.AtNum(isfi,KPhase).le.18.).or.
     1     (DDist(k).gt.DNeighH.and.AtNum(isfi,KPhase).gt.18.)) cycle
        fnn=fnn+1.
        nn=nn+1
        yy=0.
        if(EqIgCase(AtTypeCentral,'C')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.53-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'N')) then
            yy=exp((1.442-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'B')) then
            yy=exp((1.7-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'O')) then
            yy=exp((1.390-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'F')) then
            yy=exp((1.32-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'S')) then
            yy=exp((1.770-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Cl')) then
            yy=exp((1.76-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Br')) then
            yy=exp((1.9-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Fe')) then
            yy=exp((1.689-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Co')) then
            yy=exp((1.634-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Cu')) then
            yy=exp((1.446-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Ru')) then
            yy=exp((2.2-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'P')) then
            yy=exp((1.89-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'Si')) then
            yy=exp((1.883-DDist(k))/.37)-1.
          else
            yy=-1.
          endif
          if(yy.lt.-.5) then
            fnn=fnn-1.
            nn=nn-1.
            cycle
          endif
        else if(EqIgCase(AtTypeCentral,'N')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.442-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'O')) then
            yy=exp((1.361-DDist(k))/.37)-1.
          endif
        else if(EqIgCase(AtTypeCentral,'O')) then
          if(EqIgCase(AtTypeNeigh,'C')) then
            yy=exp((1.390-DDist(k))/.37)-1.
          else if(EqIgCase(AtTypeNeigh,'N')) then
            yy=exp((1.361-DDist(k))/.37)-1.
          endif
        endif
        AtTypeNeighUsed=AtTypeNeigh
        fnn=fnn+yy
        if(nn.gt.5) cycle
        KeepAtNeigh(1,nn)=ADist(k)
        KeepNAtNeigh(1,nn)=ktatmol(KeepAtNeigh(1,nn))
        NAtNeigh=ktat(Atom(iap),iak-iap+1,KeepAtNeigh(1,nn))+iap-1
        j=idel(SymCodeJanaDist(k))
        if(j.gt.0) KeepAtNeigh(1,nn)=
     1               KeepAtNeigh(1,nn)(:idel(KeepAtNeigh(1,nn)))//
     2               '#'//SymCodeJanaDist(k)(:j)
        if(nn.le.3)
     1    call EM40GetXFromAtName(KeepAtNeigh(1,nn),ib,xb(1,nn),uxb,uyb,
     2                            x40b,RMiib,ich)
        DstMin=min(DstMin,DDist(k))
        DstMax=max(DstMax,DDist(k))
        DstAve=DstAve+DDist(k)
      enddo
      if(nn.gt.0) DstAve=DstAve/float(nn)
      KeepNNeigh(1)=nn
      if(anint(fnn).gt.4) then
        KeepType(1)=IdKeepHApical
        if(EqIgCase(AtTypeCentral,'C').or.
     1     EqIgCase(AtTypeCentral,'N').or.
     2     EqIgCase(AtTypeNeigh,'O')) then
          KeepNH(1)=0
        else
          KeepNH(1)=1
        endif
      else if(anint(fnn).eq.4) then
        KeepType(1)=IdKeepHApical
        KeepNH(1)=0
      else if(anint(fnn).eq.3..or.
     1        (EqIgCase(AtTypeCentral,'N').and.nn.eq.3)) then
        if(EqIgCase(AtTypeCentral,'C')) then
          if(nn.eq.2) then
            KeepType(1)=IdKeepHTriangl
            KeepNH(1)=1
          else
            KeepType(1)=IdKeepHTetraHed
            KeepNH(1)=1
          endif
        else if(EqIgCase(AtTypeCentral,'N')) then
          if(nn.eq.3) then
            pom=0.
            do i=1,2
              do j=i+1,3
                pom=pom+AngleFromThreePoints(xc,xb(1,i),xb(1,j))
              enddo
            enddo
            if(pom.lt.345.) then
              KeepType(1)=IdKeepHTetraHed
              KeepNH(1)=1
            else
              KeepType(1)=IdKeepHTriangl
              KeepNH(1)=0
            endif
          else
            KeepType(1)=IdKeepHTriangl
            KeepNH(1)=0
          endif
        else
          KeepType(1)=IdKeepHTriangl
          KeepNH(1)=0
        endif
      else if(fnn.gt.1.5) then
        if(EqIgCase(AtTypeCentral,'O')) then
          KeepType(1)=IdKeepHTriangl
          KeepNH(1)=0
        else
          if(nn.ge.2) then
            if(EqIgCase(AtTypeCentral,'C')) then
              KeepType(1)=IdKeepHTetraHed
              KeepNH(1)=2
            else
              if(nn.eq.2) then
                KeepType(1)=IdKeepHTriangl
                KeepNH(1)=1
              else
                KeepType(1)=IdKeepHApical
                KeepNH(1)=0
              endif
            endif
          else
            if(EqIgCase(AtTypeCentral,'C')) then
              KeepType(1)=IdKeepHTriangl
              KeepNH(1)=2
            else
              KeepType(1)=IdKeepHApical
              KeepNH(1)=1
            endif
          endif
        endif
      else if(anint(fnn).gt.0) then
        if(NAtNeigh.gt.0) then
          AtTypeNeigh=AtType(isf(NAtNeigh),KPhase)
          if((EqIgCase(AtTypeCentral,'O').and.
     1        EqIgCase(AtTypeNeigh,'Cl'))) then
            KeepType(1)=IdKeepHApical
            KeepNH(1)=0
            go to 3000
          endif
        else
          AtTypeNeigh=' '
        endif
        if(EqIgCase(AtTypeCentral,'O')) then
          if(DstMin.lt.1.2) then
            KeepType(1)=IdKeepHTriangl
            KeepNH(1)=0
          else if(DstMin.lt.1.42) then
            KeepType(1)=IdKeepHTriangl
            KeepNH(1)=0
          else
            KeepType(1)=IdKeepHApical
            KeepNH(1)=1
          endif
        else if(EqIgCase(AtTypeCentral,'C')) then
          KeepType(1)=IdKeepHTetraHed
          KeepNH(1)=3
        else if(EqIgCase(AtTypeCentral,'N')) then
          KeepType(1)=IdKeepHTriangl
          KeepNH(1)=2
        endif
        if(ian.le.0) go to 3000
        call DistFromAtom(KeepAtNeigh(1,nn),3.,iswa(ia))
        KeepAtAnchor(1)='#unknown#'
        do i=1,NDist
          k=ipord(i)
          ian=ktat(Atom(iap),iak-iap+1,ADist(k))+iap-1
          if(ian.le.0) cycle
          isfi=isf(ian)
          if((DDist(k).gt.DNeighL.and.AtNum(isfi,KPhase).le.16.).or.
     1       (DDist(k).gt.DNeighH.and.AtNum(isfi,KPhase).gt.16.)) cycle
          if(isfi.eq.isfhp.or.
     1       EqIgCase(ADist(k),Atom(ia))) cycle
          KeepAtAnchor(1)=ADist(k)
          j=idel(SymCodeJanaDist(k))
          if(j.gt.0) KeepAtAnchor(1)=
     1                 KeepAtAnchor(1)(:idel(KeepAtAnchor(1)))//
     2                 '#'//SymCodeJanaDist(k)(:j)
          exit
        enddo
      else if(nn.eq.0.or.anint(fnn).le.0.) then
        KeepType(1)=IdKeepHApical
        KeepNH(1)=0
      endif
3000  do i=nh+1,KeepNH(1)
        write(Cislo,FormI15) i
        call Zhusti(Cislo)
        KeepAtH(1,i)='H'//Cislo(:idel(Cislo))//Atom(ia)(:idel(Atom(ia)))
      enddo
5000  KeepAtCentr(1)=Atom(ia)
      KeepNAtCentr(1)=ia
      return
      end
      subroutine EM40SetDefaultKeepH(n)
      use Refine_mod
      KeepNAtCentr(n)=0
      KeepAtCentr(n)=' '
      KeepKiXCentr(n)=0
      KeepNNeigh(n)=0
      do i=1,5
        KeepNAtNeigh(n,i)=0
        KeepKiXNeigh(n,i)=0
        KeepAtNeigh(n,i)=' '
      enddo
      KeepKiXAnchor(n)=0
      KeepNAtAnchor(n)=0
      KeepAtAnchor(n)=' '
      do i=1,4
        KeepKiXH(n,i)=0
        KeepNAtH(n,i)=0
        KeepAtH(n,i)=' '
      enddo
      KeepAngleH(n)=180.
      KeepADPExtFac(n)=1.2
      KeepDistH(n)=1.0
      KeepN(n)=0
      do i=1,NKeepAtMax
        KeepNAt(n,i)=0
        KeepKiX(n,i)=0
        KeepAt(n,i)=' '
      enddo
      return
      end
      subroutine EM40NewAtomHMake(iaut,ia,im,isw,ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      common/KeepQuest/ nEdwList,nEdwCentr,nEdwHDist,nEdwNNeigh,
     1           nEdwNeighFirst,nEdwNeighLast,nEdwHFirst,nEdwHLast,
     2           nEdwAnchor,nEdwTorsAngle,nCrwUseAnchor,nButtSelect,
     3           AtomToBeFilled,nEdwNH,nEdwARiding,Recalculate,
     4           NButtLocate,NButtApply,BlowUpFactor,SaveKeepCommands,
     5           KeepTypeOld,KeepDistHOld,KeepNNeighOld,KeepNHOld,
     6           MapAlreadyUsed,iat,iap,iak,iako,iaLast,napp,
     7           TryAutomaticRun
      character*256 t256,p256,Command(1),CurrentDirO
      character*80 Veta,KeepAtNeighOld(:),KeepAtHOld(:)
      integer FeChdir,EdwStateQuest,KeepNAtHOld(:),KeepNAtNeighOld(:)
      logical Recalculate,SaveKeepCommands,EqIgCase,MapAlreadyUsed,
     1        TryAutomaticRun,RefineEnd
      real :: xp(1)=(/0./)
      allocatable KeepAtHOld,KeepAtNeighOld,KeepNAtHOld,KeepNAtNeighOld
      save /KeepQuest/
      call SpecPos(x(1,ia),xp,0,isw,.05,nocc)
      Recalculate=.false.
      iako=iak
      do i=1,KeepNH(1)
        kam=ktatmol(KeepAtH(1,i))
        if(kam.le.0) then
          if(im.le.0) then
            kam=NAtIndTo(isw,KPhase)+1
            if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
            call AtSun(kam,NAtAll,kam+1)
            NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
            do j=1,NAtActive
              if(IAtActive(j).ge.kam) IAtActive(j)=IAtActive(j)+1
            enddo
            call EM40UpdateAtomLimits
          else
            n=kpoint(im)*mam(im)
            IAtMol=NAtMolFr(1,1)
            if(NAtAll+n+kpoint(im).ge.MxAtAll)
     1        call ReallocateAtoms(100*(n+kpoint(im)))
            call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+n)
            NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+n
            iap=iap+n
            iak=iak+n
            iako=iako+n
            ia=ia+n
            iaLast=iaLast+n
            do j=1,NAtActive
              if(IAtActive(j).ge.IAtMol)
     1          IAtActive(j)=IAtActive(j)+n
            enddo
            do j=1,i-1
              KeepNAtH(1,j)=KeepNAtH(1,j)+n
            enddo
            NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+kpoint(im)
            call EM40UpdateAtomLimits
            kam=NAtMolFr(1,1)
            do j=1,im-1
              kam=kam+iam(j)
            enddo
            kam=kam+iam(im)/kpoint(im)
            call AtSun(kam,NAtAll-kpoint(im),kam+kpoint(im))
            iam(im)=iam(im)+kpoint(im)
          endif
          PrvniKiAtomu(kam)=PrvniKiAtomu(kam-1)+DelkaKiAtomu(kam-1)
          DelkaKiAtomu(kam)=0
        else
          Recalculate=Recalculate.or.ai(kam).gt.0.
        endif
        KeepNAtH(1,i)=kam
        call ShiftKiAt(ia,itf(ia),ifr(ia),lasmax(ia),KModA(1,ia),
     1                 MagPar(ia),.false.)
        call AtCopy(ia,kam)
        isf(kam)=isfhp
        atom(kam)=KeepAtH(1,i)
        call SetIntArrayTo(KiA(1,kam),DelkaKiAtomu(kam),0)
        if(im.gt.0) then
          NAtCalc=NAtInd
          if(NMolec.gt.0) call SetMol(0,0)
        endif
      enddo
      call EM40SetNewH(1)
      do i=1,KeepNH(1)
        iah=KeepNAtH(1,i)
        call SpecPos(x(1,iah),xp,0,isw,.1,nocc)
        if(CheckNumber.eq.nButtLocate) then
          ai(iah)=0.
        else
          ai(iah)=ai(ia)
        endif
        sai(iah)=0.
        call SetRealArrayTo(sbeta(1,iah),6,0.)
        if(itf(iah).ge.2) then
          call boueq(beta(1,iah),sbeta(1,iah),1,bizo,sbizo,isw)
          itf(iah)=1
        else
          bizo=beta(1,iah)
        endif
        call SetRealArrayTo(beta(1,iah),6,0.)
        beta(1,iah)=bizo*BlowUpFactor
        do j=3,7
          KFA(j,iah)=0
          KModA(j,iah)=0
        enddo
        call ShiftKiAt(iah,itf(iah),ifr(iah),lasmax(iah),
     1                 KModA(1,iah),MagPar(iah),.false.)
      enddo
      if(SaveKeepCommands.and.
     1   (CheckNumber.eq.nButtApply.or.TryAutomaticRun)) then
        do m=1,2
          if(iaut.eq.1) then
            call EM40KeepWriteCommandAut(Command(1),ich)
          else
            call EM40KeepWriteCommand(Command(1),ich)
          endif
          if(ich.ne.0) go to 9999
          ln=NextLogicNumber()
          call OpenFile(ln,fln(:ifln)//'_keep.tmp','formatted',
     1                  'unknown')
          j=0
3375      read(ln,FormA256,end=3380) t256
          call mala(t256)
          j=j+1
          k=0
          call kus(t256,k,Cislo)
          if(.not.EqIgCase(Cislo,'keep').and.
     1       .not.EqIgCase(Cislo,'!keep')) go to 3375
          call kus(t256,k,Cislo)
          if(m.eq.1) then
            ii=IdKeepHydro
          else
            ii=IdKeepADP
          endif
          if(.not.EqIgCase(Cislo,CKeepType(ii))) go to 3375
          call kus(t256,k,Cislo)
          call kus(t256,k,Cislo)
          call UprAt(Cislo)
          if(.not.EqIgCase(Cislo,KeepAtCentr(1))) go to 3375
          call CloseIfOpened(ln)
          call RewriteLinesOnFile(fln(:ifln)//'_keep.tmp',j,j,
     1                            Command,1)
          go to 3382
3380      call CloseIfOpened(ln)
          call AppendFile(fln(:ifln)//'_keep.tmp',Command,1)
3382      if(m.eq.1) then
            KeepDistHOld=KeepDistH(1)
            KeepADPExtFac(1)=BlowUpFactor
            KeepType(1)=IdKeepARiding
          else
            KeepType(1)=KeepTypeOld
            KeepDistH(1)=KeepDistHOld
          endif
        enddo
        napp=napp+KeepNH(1)
      else if(CheckNumber.eq.nButtLocate) then
        allocate(KeepAtNeighOld(KeepNNeighOld),
     1           KeepNAtNeighOld(KeepNNeighOld),KeepAtHOld(KeepNHOld),
     2           KeepNAtHOld(KeepNHOld))
        do i=1,KeepNNeighOld
          KeepAtNeighOld(i)=KeepAtNeigh(1,i)
          KeepNAtNeighOld(i)=KeepNAtNeigh(1,i)
        enddo
        do i=1,KeepNHOld
          KeepAtHOld(i)=KeepAtH(1,i)
          KeepNAtHOld(i)=KeepNAtH(1,i)
        enddo
        call EM40KeepWriteCommand(Command(1),ich)
        t256=fln(:ifln)//'.m40'
        j=idel(t256)-2
        p256=TmpDir(:idel(TmpDir))//fln(:ifln)//'.m40'
        k=idel(p256)-2
        do i=1,3
          call CopyFile(t256,p256)
          if(i.eq.1) then
            t256(j:)='m50'
            p256(k:)='m50'
          else if(i.eq.2) then
            t256(j:)='m90'
            p256(k:)='m90'
          endif
        enddo
        call iom40(1,0,fln(:ifln)//'.m40')
        p256=TmpDir(:idel(TmpDir))//fln(:ifln)//'*.*'
        call FeTmpFilesAdd(p256)
        CurrentDirO=CurrentDir
        i=FeChdir(TmpDir)
        call FeGetCurrentDir
        call iom40(1,0,fln(:ifln)//'.m40')
        IgnoreW=.true.
        Veta=KeepAtAnchor(1)
        call RefOpenCommands
        IgnoreW=.false.
        call DeleteFile(fln(:ifln)//'_keep.tmp')
        call RefRewriteCommands(1)
        call RefKeepReadCommand(Command(1),ich)
        KeepAtAnchor(1)=Veta
        if(.not.MapAlreadyUsed.or.Recalculate) then
          call EM40SetCommandsRefine
          call EM40SetCommandsFourier
          ShowInfoOnScreen=.false.
          call refine(0,RefineEnd)
          MapAlreadyUsed=.true.
        endif
        call EM40SetCommandsContour
        if(ErrFlag.ne.0) go to 3520
        call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.m45')
        call EM40RunContour(TorsAngle,ich)
3520    i=FeChdir(CurrentDirO)
        call FeGetCurrentDir
        if(EdwStateQuest(nEdwTorsAngle).eq.EdwOpened.and.ich.eq.0)
     1     call FeQuestRealEdwOpen(nEdwTorsAngle,TorsAngle,.false.,
     2                             .false.)
        if(SaveKeepCommands) then
          IgnoreW=.true.
          call RefOpenCommands
          IgnoreW=.false.
        endif
        iak=iako
        KeepAtCentr(1)=atom(ia)
        KeepNAtCentr(1)=ia
        KeepNH(1)=KeepNHOld
        KeepNNeigh(1)=KeepNNeighOld
        KeepAngleH(1)=TorsAngle
        KeepType(1)=KeepTypeOld
        do i=1,KeepNNeighOld
          KeepAtNeigh(1,i)=KeepAtNeighOld(i)
          KeepNAtNeigh(1,i)=KeepNAtNeighOld(i)
        enddo
        do i=1,KeepNHOld
          KeepAtH(1,i)=KeepAtHOld(i)
          KeepNAtH(1,i)=KeepNAtHOld(i)
          iah=KeepNAtH(1,i)
          ai(iah)=ai(ia)
        enddo
        if(KeepAtAnchor(1).eq.' ') then
          KeepAtAnchor(1)='#unknown#'
          call EM40SetNewH(1)
          KeepAtAnchor(1)='#unknown#'
        endif
        deallocate(KeepAtNeighOld,KeepNAtNeighOld,KeepAtHOld,
     1             KeepNAtHOld)
        call iom40(1,0,fln(:ifln)//'.m40')
      endif
9999  return
      end
      subroutine EM40SetCommandsRefine
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      call RefOpenCommands
      NacetlInt(nCmdncykl)=0
      NacetlInt(nCmdCallFour)=1
      NacetlInt(nCmdkim)=0
      NKeep=0
      call RefRewriteCommands(1)
      return
      end
      subroutine EM40SetCommandsFourier
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'fourier.cmn'
      call DefaultFourier
      call FouRewriteCommands(1)
      call FouOpenCommands
      ScopeType=1
      NacetlInt(nCmdUseWeight)=1
      NacetlInt(nCmdmapa)=6
      NacetlReal(nCmdptstep)=0.1
      NacetlReal(nCmdsnlmxf)=0.5
      NacetlInt(nCmdLPeaks)=0
      NacetlInt(nCmdPPeaks)=-333
      NacetlInt(nCmdNPeaks)=-333
      if(NDimI(KPhase).gt.0) then
        do i=4,NDim(KPhase)
          xrmn(i)=0.
          xrmx(i)=0.
          dd(i)=.1
        enddo
      endif
      call FouRewriteCommands(1)
      return
      end
      subroutine EM40SetCommandsContour
      use Atoms_mod
      use Contour_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'contour.cmn'
      character*256 t256
      integer ColorOrder
      dimension xb(3),xc(3),u(3),ug(3),v(3),vg(3),trp(9),trpi(9)
      equivalence (t80,t256)
      n=NAtCalc
      allocate(AtBrat(n))
      call ConPrelim
      DrawAtN=KeepNH(1)+1
      DrawAtName(1)=KeepAtCentr(1)
      DrawAtColor(1)=ColorOrder('Red')
      call SetRealArrayTo(DrawAtBondLim,4,1.2)
      call SetLogicalArrayTo(DrawAtSkip,4,.false.)
      call SetIntArrayTo(DrawAtColor(2),3,ColorOrder('Green'))
      do i=1,KeepNH(1)
        DrawAtName(i+1)=KeepAtH(1,i)
      enddo
      call SetRealArrayTo(XPlane(1,1),3,0.)
      do i=1,KeepNH(1)
        call AddVek(x(1,KeepNAtH(1,i)),XPlane(1,1),XPlane(1,1),3)
      enddo
      pom=1./float(KeepNH(1))
      do i=1,3
        XPlane(i,1)=XPlane(i,1)*pom
      enddo
      call CopyVek(x(1,KeepNAtCentr(1)),xb,3)
      call atsym(KeepAtNeigh(1,1),i,xc,v,vg,ISym,ich)
      if(ich.ne.0) then
        call FeChybne(-1.,-1.,'the neighbour atom "'//
     1                KeepAtNeigh(1,1)(:idel(KeepAtNeigh(1,1)))//
     2                '" isn''t present on M40.',' ',SeriousError)
        ErrFlag=1
        go to 9999
      endif
      isw=iswa(i)
      do i=1,3
        u(i)=xb(i)-xc(i)
      enddo
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      uu=scalmul(u,ug)
      pomm=2.
      do i=1,3
        call SetRealArrayTo(v,3,0.)
        v(i)=1.
        call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
        uv=scalmul(u,vg)
        vv=scalmul(v,vg)
        pom=abs(uv/sqrt(uu*vv))
        if(pom.lt.pomm) then
          pomm=pom
          call CopyVek(v,xc,3)
        endif
      enddo
      call RefMakeTrMat(u,xc,3,1,trp,trpi)
      if(KeepType(1).eq.IdKeepHTetraHed) then
        pom1=0.3333333
        pom2=0.9428090
      else
        pom1=0.5
        pom2=0.8660254
      endif
      DAngle=90.
      v(3)=pom1
      pom= pom2
      Angle=0.
      do i=2,3
        v(1)=pom*cos(Angle*ToRad)
        v(2)=pom*sin(Angle*ToRad)
        call CopyVek(xb,XPlane(1,i),3)
        call cultm(trpi,v,XPlane(1,i),3,3,1)
        Angle=Angle+DAngle
      enddo
      DeltaPlane=0.1
      ScopePlane(1)=3.
      ScopePlane(2)=3.
      ScopePlane(3)=0.
      ShiftPlane(1)=1.5
      ShiftPlane(2)=1.5
      ShiftPlane(3)=0.
9999  return
      end
      subroutine EM40RunContour(TorsAngle,ich)
      use Contour_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'contour.cmn'
      character*12 Labels(6)
      character*80 SaveFile,Veta
      dimension xdo(3),xp(3),xpo(3),xfract(3)
      integer FeGetSystemTime,TimeQuestEvent
      logical Drzi
      data Labels/'%Escape','%Apply','R%+','R%-','%Step','%Optimal'/
      ich=0
      TorsAngle=KeepAngleH(1)
      ContourQuest=NextQuestId()
      call FeQuestAbsCreate(ContourQuest,0.,0.,XMaxBasWin,YMaxBasWin,
     1                      ' ',0,0,-1,-1)
      CheckMouse=.true.
      QuestGetEdwActive(ContourQuest)=.false.
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      pom=YMaxGrWin
      dpom=ButYd+10.
      ypom=pom-dpom-2.
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      do i=1,6
        call FeQuestAbsButtonMake(ContourQuest,xpom,ypom,
     1                            wpom,ButYd,Labels(i))
        if(i.eq.1) then
          nButtEsc=ButtonLastMade
        else if(i.eq.2) then
          nButtApply=ButtonLastMade
          wpom=wpom/2.-2.5
        else if(i.eq.3) then
          nButtRotPlus=ButtonLastMade
          xpom=xpom+wpom+5.
          ypom=ypom+dpom
        else if(i.eq.4) then
          nButtRotMinus=ButtonLastMade
          wpom=80.
          xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
        else if(i.eq.5) then
          nButtDefStep=ButtonLastMade
        else if(i.eq.6) then
          nButtOptimal=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        if(i.eq.2) then
          ypom=ypom-6.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,
     1                           Gray,White)
          ypom=ypom-1.
        endif
        ypom=ypom-dpom
      enddo
      nButtBasTo=ButtonLastMade
      ypom=ypom+dpom-6.
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,White)
      TakeMouseMove=.true.
      call FeClearGrWin
      kpdf=1
      obecny=kpdf.gt.0
      DrawPDF=kpdf.gt.1
      isoucet=0
      smapy=.false.
      tmapy=.false.
      AtomsOn=.false.
      call NactiM81
      if(ErrFlag.ne.0) go to 9000
      call ConMakeGenSection(ich)
      if(ich.ne.0) go to 9000
      call ConCalcGeneral
      if(ErrFlag.ne.0) go to 9000
      do i=3,6
        nxdraw(i-2)=1
        nxfrom(i-2)=1
        nxto(i-2)=nx(i)
      enddo
      nxdraw(1)=0
      irec=-1
      irecold=-1
      reconfig=.true.
      if(xyzmap) call TrPor
      call SetTr
      i=ConTintType
      ConTintType=1
      call KresliMapu(0)
      ConTintType=i
      call FeHeaderInfo(' ')
      do i=1,2
        if(i.eq.1) then
          Veta='Density'
          xpom=XMinBasWin+50.
          ypom=YMaxBasWin-20.
          ypomt=YMaxBasWin-12.
          dpom=60.
          xpomt=xpom+dpom+20.
        else if(i.eq.2) then
          Veta='Torsion angle'
          xpom=xpom+600.
          xpomt=xpom-FeTxLength(Veta)-20.
        endif
        call FeQuestAbsLblMake(ContourQuest,xpomt,ypomt,Veta,'L','N')
        call FeWinfMake(i,0,xpom,ypom,dpom,1.2*PropFontHeightInPixels)
      enddo
      AngStep=.1
      do i=1,2
        if(i.eq.1) then
          write(Veta,100) EM40HDensity()
        else if(i.eq.2) then
          write(Veta,101) KeepAngleH(1)
        endif
        call Zhusti(Veta)
        call FeWInfWrite(i,Veta)
      enddo
      SaveFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SaveFile,i,1)
      call FeSaveImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SaveFile)
2000  call FeClearGrWin
      call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,SaveFile,
     1                 2)
      call ConDrawAtoms(zmap)
      do i=1,2
        if(i.eq.1) then
          write(Veta,100) EM40HDensity()
        else if(i.eq.2) then
          write(Veta,101) KeepAngleH(1)
        endif
        call Zhusti(Veta)
        call FeWInfWrite(i,Veta)
      enddo
2500  call FeQuestEvent(ContourQuest,ich)
      TimeQuestEvent=FeGetSystemTime()
      Drzi=.false.
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtESC.or.CheckNumber.eq.nButtApply) then
          call Del8
          call FeMakeGrWin(0.,0.,YBottomMargin,0.)
          call CloseIfOpened(m8)
          if(CheckNumber.eq.nButtESC) then
            ich=1
          else
            ich=0
            TorsAngle=KeepAngleH(1)
          endif
          call FeQuestRemove(ContourQuest)
        else if(CheckNumber.eq.nButtRotPlus.or.
     1          CheckNumber.eq.nButtRotMinus) then
2600      if(CheckNumber.eq.nButtRotPlus) then
            KeepAngleH(1)=KeepAngleH(1)+AngStep
          else
            KeepAngleH(1)=KeepAngleH(1)-AngStep
          endif
2605      if(KeepAngleH(1).gt.180.) then
            KeepAngleH(1)=KeepAngleH(1)-360.
            go to 2605
          endif
2610      if(KeepAngleH(1).le.-180.) then
            KeepAngleH(1)=KeepAngleH(1)+360.
            go to 2610
          endif
          call EM40SetNewH(1)
          call FeClearGrWin
          call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,YMaxGrWin,
     1                     SaveFile,2)
          call ConDrawAtoms(zmap)
          do i=1,2
            if(i.eq.1) then
              write(Veta,100) EM40HDensity()
            else if(i.eq.2) then
              write(Veta,101) KeepAngleH(1)
            endif
            call Zhusti(Veta)
            call FeWInfWrite(i,Veta)
          enddo
          call FeReleaseOutput
          call FeDeferOutput
2700      call FeEvent(1)
          i=CheckNumber+ButtonFr-1
          if(EventType.eq.0) then
            if(FeGetSystemTime()-TimeQuestEvent.lt.500) then
              go to 2700
            else
              Drzi=.true.
              go to 2600
            endif
          endif
          Drzi=.false.
          go to 2500
        else if(CheckNumber.eq.nButtDefStep) then
          idp=NextQuestId()
          xqd=160.
          il=1
          call FeQuestCreate(idp,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
          Veta='Angle step'
          tpom=5.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          dpom=60.
          call FeQuestEudMake(idp,tpom,1,xpom,1,Veta,'L',dpom,EdwYd,0)
          nEdwAngStep=EdwLastMade
          call FeQuestRealEdwOpen(EdwLastMade,AngStep,.false.,.false.)
          call FeQuestEudOpen(EdwLastMade,0,0,0,.1,180.,.1)
3000      call FeQuestEvent(idp,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 3000
          endif
          if(ich.eq.0) call FeQuestRealFromEdw(nEdwAngStep,AngStep)
          call FeQuestRemove(idp)
          call FeReleaseOutput
          go to 2000
        else if(CheckNumber.eq.nButtOptimal) then
          if(KeepType(1).eq.IdKeepHTetraHed) then
            n=1200
            KeepAngleH(1)=KeepAngleH(1)-60.
          else
            n=1800
            KeepAngleH(1)=KeepAngleH(1)-90.
          endif
          pom=-99999.
          do i=1,n
            KeepAngleH(1)=KeepAngleH(1)+.1
            call EM40SetNewH(1)
            Density=EM40HDensity()
            if(Density.gt.pom) then
              pom =Density
              poma=KeepAngleH(1)
            endif
          enddo
3205      if(poma.gt.180.) then
            poma=poma-360.
            go to 3205
          endif
3210      if(poma.le.-180.) then
            poma=poma+360.
            go to 3210
          endif
          KeepAngleH(1)=poma
          call EM40SetNewH(1)
          go to 2000
        endif
      else if(CheckType.eq.EventMouse) then
        call GetCoord(xdo,xpo,xfract)
        AngOld=atan2(xpo(2),xpo(1))/ToRad
        if(CheckNumber.eq.JeLeftDown) then
          TakeMouseMove=.true.
3250      call FeEvent(1)
          if(EventType.eq.EventMouse) then
            if(EventNumber.eq.JeMove) then
              call GetCoord(xdo,xp,xfract)
              if(xp(1).ne.xpo(1).or.xp(2).ne.xpo(2)) then
                Ang=atan2(xp(2),xp(1))/ToRad
                call CopyVek(xp,xpo,3)
                if(Ang.ne.AngOld) then
                  KeepAngleH(1)=KeepAngleH(1)+Ang-AngOld
3255              if(KeepAngleH(1).gt.180.) then
                    KeepAngleH(1)=KeepAngleH(1)-360.
                    go to 3255
                  endif
3260              if(KeepAngleH(1).le.-180.) then
                    KeepAngleH(1)=KeepAngleH(1)+360.
                    go to 3260
                  endif
                  AngOld=Ang
                  call EM40SetNewH(1)
                  call FeClearGrWin
                  call FeLoadImage(XMinGrWin,XMaxGrWin,YMinGrWin,
     1                             YMaxGrWin,SaveFile,2)
                  call ConDrawAtoms(zmap)
                  do i=1,2
                    if(i.eq.1) then
                      write(Veta,100) EM40HDensity()
                    else if(i.eq.2) then
                      write(Veta,101) KeepAngleH(1)
                    endif
                    call Zhusti(Veta)
                    call FeWInfWrite(i,Veta)
                  enddo
                  call FeReleaseOutput
                  call FeDeferOutput
                endif
                go to 3250
              endif
            else if(EventNumber.eq.JeLeftUp) then
              TakeMouseMove=.false.
              go to 2500
            endif
          else
            go to 3250
          endif
        endif
        TakeMouseMove=.false.
        go to 2500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2500
      endif
      go to 9999
9000  call FeQuestRemove(ContourQuest)
      ErrFlag=0
      ich=1
9999  if(allocated(AtBrat)) deallocate(AtBrat)
      call CloseIfOpened(m8)
      call CloseIfOpened(81)
      return
100   format(f8.3)
101   format(f8.1)
      end
      function EM40HDensity()
      use Atoms_mod
      use Contour_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'contour.cmn'
      dimension xp(3)
      EM40HDensity=0.
      do i=1,KeepNH(1)
        ia=KeepNAtH(1,i)
        call prevod(0,x(1,ia),xp)
        EM40HDensity=EM40HDensity+ExtMap(xp(1),xp(2),ActualMap,
     1                                   NActualMap)
      enddo
      return
      end
      subroutine EM40SetNewH(n)
      use Atoms_mod
      use EditM40_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'editm40.cmn'
      dimension u(3),ug(3),v(3),vg(3),dt(3),nd(3),nt(3),tt(3),xp(3),
     1          x4(3),
     2          xb(3),uxb(3,mxw),uyb(3,mxw),x40b(3),RMiib(9),xbm(3),
     3          xh(3,3),
     4          xc(3,0:5),uxc(3,mxw,0:5),uyc(3,mxw,0:5),x40c(3,0:5),
     5          xcm(3,0:5),
     6          RMiic(9,0:5),ic(0:5),
     7          rx((mxw+1)*(2*mxw+1)),px(2*mxw+1,3,3),dr(2*mxw+1),
     8          tt0(3),FPolA(2*mxw+1)
      logical   EqIgCase
      equivalence (xb,xc(1,0)),(uxb,uxc(1,1,0)),(uyb,uyc(1,1,0)),
     1            (x40b,x40c(1,0)),(RMiib,RMiic(1,0)),(ib,ic(0)),
     2            (xbm,xcm(1,0))
      KPhaseIn=KPhase
      call EM40GetXFromAtName(KeepAtCentr(n),ib,xb,uxb,uyb,x40b,RMiib,
     1                        ich)
      if(ich.ne.0) go to 9999
      ksw=kswa(ib)
      KPhase=kswa(ib)
      isw=iswa(ib)
      if(KeepType(n).eq.IdKeepHTetraHed) then
        NAtMax=4
      else if(KeepType(n).eq.IdKeepHTriangl) then
        NAtMax=3
      else if(KeepType(n).eq.IdKeepHApical) then
        NAtMax=6
      endif
      if((KeepType(n).eq.IdKeepHTetrahed.and.KeepNH(n).eq.3).or.
     1   (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.2)) then
        call EM40GetXFromAtName(KeepAtNeigh(n,1),ic(1),xc(1,1),
     1               uxc(1,1,1),uyc(1,1,1),x40c(1,1),RMiic(1,1),ich)
        if(ich.ne.0) go to 9999
        if(KeepNNeigh(n)+KeepNH(n).gt.NAtMax) then
          call EM40GetXFromAtName(KeepAtAnchor(n),ic(2),xc(1,2),
     1                  uxc(1,1,2),uyc(1,1,2),x40c(1,2),RMiic(1,2),ich)
          if(ich.ne.0) go to 9999
          natc=2
        else
          if(EqIgCase(KeepAtAnchor(n),'#unknown#')) then
            dpom=2.
1100        call DistFromAtom(KeepAtNeigh(n,1),dpom,isw)
            do i=1,NDist
              k=ipord(i)
              if(.not.EqIgCase(adist(k),KeepAtCentr(n))) then
                j=idel(SymCodeJanaDist(k))
                KeepAtAnchor(n)=adist(k)
                if(j.gt.0)
     1            KeepAtAnchor(n)=
     2              KeepAtAnchor(n)(:idel(KeepAtAnchor(n)))//'#'//
     3              SymCodeJanaDist(k)(:idel(SymCodeJanaDist(k)))
                call EM40GetXFromAtName(KeepAtAnchor(n),ic(2),xc(1,2),
     1                  uxc(1,1,2),uyc(1,1,2),x40c(1,2),RMiic(1,2),ich)
                if(ich.ne.0) go to 9999
                if(isf(ic(2)).eq.isfhp) cycle
                do j=1,3
                  u(j)=xc(j,1)-xb(j)
                enddo
                call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
                uu=scalmul(u,ug)
                do j=1,3
                  v(j)=xc(j,2)-xc(j,1)
                enddo
                call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
                vv=scalmul(v,vg)
                uv=scalmul(u,vg)
                pom=abs(uv/sqrt(uu*vv))
                if(pom.lt..01.or.abs(pom-1.).lt..01) go to 1120
                natc=2
                go to 1130
              endif
            enddo
            if(dpom.lt.3.) then
              dpom=dpom+.2
              go to 1100
            endif
            do i=1,3
              u(i)=xc(i,1)-xb(i)
            enddo
            call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
            uu=scalmul(u,ug)
1120        pomm=2.
            do i=1,3
              call SetRealArrayTo(v,3,0.)
              v(i)=1.
              call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
              uv=scalmul(u,vg)
              vv=scalmul(v,vg)
              pom=abs(uv/sqrt(uu*vv))
              if(pom.lt.pomm) then
                pomm=pom
                call AddVek(v,xc(1,1),xc(1,2),3)
                KeepAtAnchor(n)='#000#'
                KeepAtAnchor(n)(i+1:i+1)='1'
              endif
            enddo
            natc=2
          else
            if(KeepAtAnchor(n).eq.'#asitis') then
              KeepAngleH(n)=0.
              call EM40GetXFromAtName(KeepAtH(n,1),ic(3),xc(1,3),
     1                  uxc(1,1,3),uyc(1,1,3),x40c(1,3),RMiic(1,3),ich)
              if(ich.ne.0) go to 9999
              do i=1,3
                xc(i,2)=xc(i,3)-xb(i)+xc(i,1)
              enddo
              natc=2
              ic(2)=0
            else if(KeepAtAnchor(n)(1:1).eq.'#') then
              j=2
              do i=2,4
                if(KeepAtAnchor(n)(i:i).eq.'0') then
                  v(i-1)=0.
                else
                  v(i-1)=1.
                endif
              enddo
              call AddVek(v,xc(1,1),xc(1,2),3)
              natc=1
            else
              call EM40GetXFromAtName(KeepAtAnchor(n),ic(2),xc(1,2),
     1                uxc(1,1,2),uyc(1,1,2),x40c(1,2),RMiic(1,2),ich)
              if(ich.ne.0) go to 9999
              natc=2
            endif
          endif
        endif
1130    if(NDimI(KPhase).le.0) then
          call EM40AllButOneH(xb,xc(1,1),xh,KeepNH(n),KeepDistH(n),
     1                        KeepAngleH(n),isw)
          do j=1,KeepNH(n)
            call CopyVek(xh(1,j),x(1,KeepNAtH(n,j)),3)
          enddo
        endif
      else if((KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.1).or.
     1        (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.1).or.
     2        (KeepType(n).eq.IdKeepHApical)) then
        do j=1,KeepNNeigh(n)
          call EM40GetXFromAtName(KeepAtNeigh(n,j),ic(j),xc(1,j),
     1                  uxc(1,1,j),uyc(1,1,j),x40c(1,j),RMiic(1,j),ich)
          if(ich.ne.0) go to 9999
        enddo
        call EM40AddApicalH(xb,xc(1,1),KeepNNeigh(n),x(1,KeepNAtH(n,1)),
     1                      KeepDistH(n),isw)
        natc=KeepNNeigh(n)
      else if(KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.2) then
        do j=1,2
          call EM40GetXFromAtName(KeepAtNeigh(n,j),ic(j),xc(1,j),
     1                  uxc(1,1,j),uyc(1,1,j),x40c(1,j),RMiic(1,j),ich)
          if(ich.ne.0) go to 9999
        enddo
        call EM40TetraAdd2H(xb,xc(1,1),xh,KeepDistH(n),isw)
        do j=1,2
          call CopyVek(xh(1,j),x(1,KeepNAtH(n,j)),3)
        enddo
        natc=2
      endif
      if(NDimI(KPhase).le.0) go to 9000
      kmodmn=KModA(2,ic(0))
      kmodmx=KModA(2,ic(0))
      do i=1,natc
        if(ic(i).ne.0) then
          kmodmn=min(KModA(2,ic(i)),kmodmn)
          kmodmx=max(KModA(2,ic(i)),kmodmx)
        endif
      enddo
      do i=1,KeepNH(n)
        kmodmn=min(KModA(2,KeepNAtH(n,i)),kmodmn)
        kmodmx=max(KModA(2,KeepNAtH(n,i)),kmodmx)
      enddo
      ntmx=1
      do i=1,3
        if(i.le.NDimI(KPhase)) then
          if(kcommen(KPhase).le.0) then
            nt(i)=(6*kmodmx+3)*10
          else
            nt(i)=(ngc(KPhase)+1)*NCommQProduct(isw,KPhase)
            nt(i)=(6*kmodmx+3)*10
          endif
          dt(i)=1./float(nt(i))
        else
          nt(i)=1
          dt(i)=0
        endif
        ntmx=ntmx*nt(i)
      enddo
      nx=2*kmodmx+1
      if(KCommen(KPhase).gt.0) nx=min(nx,ntmx)
      call SetRealArrayTo(rx,(nx*(nx+1))/2,0.)
      call SetRealArrayTo(px,(2*mxw+1)*9,0.)
      if(KCommen(KPhase).gt.0) then
        call CopyVek(trez(1,1,KPhase),tt0,NDimI(KPhase))
      else
        call SetRealArrayto(tt0,NDimI(KPhase),0.)
      endif
      do 4000it=1,ntmx
        call RecUnpack(it,nd,nt,NDimI(KPhase))
        do i=1,NDimI(KPhase)
          tt(i)=(nd(i)-1)*dt(i)+tt0(i)
        enddo
        do j=0,natc
          if(KeepAtAnchor(n).eq.'#asitis'.and.j.eq.natc) then
            jj=j+1
          else
            jj=j
          endif
          ia=ic(jj)
          if(ia.le.0) cycle
          call CopyVek(xc(1,jj),xcm(1,jj),3)
          call CopyVek(x40c(1,jj),x4,NDimI(KPhase))
          call cultm(Rmiic(1,jj),tt,x4,NDimI(KPhase),NDimI(KPhase),1)
          if(KModA(1,ia).gt.0) then
            if(KFA(1,ia).eq.0) then
              occ=a0(ia)
              do kk=1,KModA(1,ia)
                arg=0.
                do i=1,NDimI(KPhase)
                  arg=arg+x4(i)*float(kw(i,kk,ksw))
                enddo
                arg=pi2*arg
                occ=occ+ax(kk,ia)*sin(arg)+ay(kk,ia)*cos(arg)
              enddo
            else
              kk=KModA(1,ia)-NDimI(KPhase)
              do k=1,NDimI(KPhase)
                kk=kk+1
                x4p=x4(k)-ax(kk,ia)
                ix4p=x4p
                if(x4p.lt.0.) ix4p=ix4p-1
                x4p=x4p-float(ix4p)
                if(x4p.gt..5) x4p=x4p-1.
                if(NDimI(KPhase).eq.1) then
                  delta=a0(ia)*.5
                else
                  delta=ay(kk,ia)
                endif
                if(x4p.ge.-delta.and.x4p.le.delta) then
                  occ=1.
                else
                  occ=0.
                  go to 4000
                endif
              enddo
            endif
            if(occ.lt..01) go to 4000
          endif
          if(jj.eq.0) dr(1)=1.
          k=1
          do kk=1,KModA(2,ia)
            if(kk.lt.KModA(2,ia).or.KFA(2,ia).eq.0) then
              if(TypeModFun(ia).le.1) then
                arg=0.
                do i=1,NDimI(KPhase)
                  arg=arg+x4(i)*float(kw(i,kk,ksw))
                enddo
                arg=pi2*arg
                sna=sin(arg)
                csa=cos(arg)
              else
                if(kk.eq.1) then
                  x4p=x4(k)-ax(kk,ia)
                  ix4p=x4p
                  if(x4p.lt.0.) ix4p=ix4p-1
                  x4p=x4p-float(ix4p)
                  if(x4p.gt..5) x4p=x4p-1.
                  pom=x4p/delta
                  call GetFPol(pom,FPolA,2*kmodmx+1,TypeModFun(ia))
                endif
                sna=FPolA(k+1)
                csa=FPolA(k+2)
              endif
              do i=1,3
                xcm(i,jj)=xcm(i,jj)+Uxc(i,kk,jj)*sna+Uyc(i,kk,jj)*csa
              enddo
              if(jj.eq.0) then
                k=k+1
                dr(k)=sna
                k=k+1
                dr(k)=csa
              endif
            else
              x4p=x4(1)-Uyc(1,kk,jj)
              ix4p=x4p
              if(x4p.lt.0.) ix4p=ix4p-1
              x4p=x4p-float(ix4p)
              if(x4p.gt..5) x4p=x4p-1.
              znak=2.*x4p/Uyc(2,kk,jj)
              do i=1,3
                xcm(i,jj)=xcm(i,jj)+znak*Uxc(i,kk,jj)
              enddo
            endif
          enddo
        enddo
        if((KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.3).or.
     1     (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.2)) then
          if(KeepNNeigh(n)+KeepNH(n).le.NAtMax) then
            if(EqIgCase(KeepAtAnchor(n),'#unknown#')) then
c              go to 9999
            else
              if(KeepAtAnchor(n).eq.'#asitis') then
                do i=1,3
                  xcm(i,2)=xcm(i,3)-xbm(i)+xcm(i,1)
                enddo
              else if(KeepAtAnchor(n)(1:1).eq.'#') then
              else
              endif
            endif
          endif
          call EM40AllButOneH(xbm,xcm(1,1),xh,KeepNH(n),KeepDistH(n),
     1                        KeepAngleH(n),isw)
        else if(KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.2) then
          call EM40TetraAdd2H(xbm,xcm(1,1),xh,KeepDistH(n),isw)
        else if((KeepType(n).eq.IdKeepHTetraHed.and.KeepNH(n).eq.1).or.
     1          (KeepType(n).eq.IdKeepHTriangl .and.KeepNH(n).eq.1).or.
     2          (KeepType(n).eq.IdKeepHApical)) then
          call EM40AddApicalH(xbm,xcm(1,1),KeepNNeigh(n),xh,
     1                        KeepDistH(n),isw)
        endif
        l=0
        do i=1,nx
          do k=1,KeepNH(n)
            do j=1,3
              px(i,j,k)=px(i,j,k)+xh(j,k)*dr(i)
            enddo
          enddo
          do j=1,i
            l=l+1
            rx(l)=rx(l)+dr(i)*dr(j)
          enddo
        enddo
4000  continue
      call smi(rx,nx,ising)
      if(ising.eq.0) then
        do k=1,KeepNH(n)
          nh=KeepNAtH(n,k)
          do j=1,3
            call nasob(rx,px(1,j,k),dr,nx)
            x(j,nh)=dr(1)
            l=1
            do kk=1,KModA(2,nh)
              l=l+1
              if(l.le.nx) then
                ux(j,kk,nh)=dr(l)
              else
                ux(j,kk,nh)=0.
              endif
              l=l+1
              if(l.le.nx) then
                uy(j,kk,nh)=dr(l)
              else
                uy(j,kk,nh)=0.
              endif
            enddo
          enddo
          call qbyx(x(1,nh),xp,isw)
          do kk=1,KModA(2,nh)
            if(kk.eq.KModA(2,nh).and.KFA(2,nh).ne.0) then
              uy(1,kk,nh)=uy(1,kk,nh)+xp(1)-qcnt(1,ib)
            else
              if(TypeModFun(ia).le.1) then
                fik=0.
                do m=1,NDimI(KPhase)
                  fik=fik+(xp(m)-qcnt(m,ib))*float(kw(m,kk,KPhase))
                enddo
                sinfik=sin(pi2*fik)
                cosfik=cos(pi2*fik)
                do l=1,3
                  xpom=ux(l,kk,nh)
                  ypom=uy(l,kk,nh)
                  ux(l,kk,nh)= xpom*cosfik+ypom*sinfik
                  uy(l,kk,nh)=-xpom*sinfik+ypom*cosfik
                enddo
              endif
            endif
          enddo
        enddo
      else
        go to 9000
      endif
9000  KPhase=KPhaseIn
9999  return
      end
      subroutine EM40AllButOneH(xb,xc,xh,n,DistH,AngleH,isw)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension xb(3),xc(3,*),xh(3,*),trp(3,3),trpi(9)
      dimension u(3),ug(3),v(3),vg(3),w(3),wg(3)
      do i=1,3
        u(i)=xb(i)-xc(i,1)
      enddo
      do i=1,3
        v(i)=xc(i,2)-xc(i,1)
      enddo
1150  call RefMakeTrMat(u,v,3,1,trp,trpi)
      if(n.eq.3) then
        pom1=0.3333333
        pom2=0.9428090
        DAngle=120.
      else
        pom1=0.5
        pom2=0.8660254
        DAngle=180.
      endif
      w(3)= pom1*DistH
      pom=  pom2*DistH
      Angle=AngleH
      do i=1,n
        w(1)=pom*cos(Angle*ToRad)
        w(2)=pom*sin(Angle*ToRad)
        call CopyVek(xb,xh(1,i),3)
        call cultm(trpi,w,xh(1,i),3,3,1)
        Angle=Angle+DAngle
      enddo
      go to 9999
      entry EM40AddApicalH(xb,xc,n,xh,DistH,isw)
      call SetRealArrayTo(u,3,0.)
      do j=1,n
        do i=1,3
          v(i)=xb(i)-xc(i,j)
        enddo
        call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
        call vecnor(v,vg)
        call AddVek(u,v,u,3)
      enddo
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      call vecnor(u,ug)
      do i=1,3
        xh(i,1)=xb(i)+u(i)*DistH
      enddo
      go to 9999
      entry EM40TetraAdd2H(xb,xc,xh,DistH,isw)
      do i=1,3
        u(i)=xc(i,1)-xb(i)
        v(i)=xc(i,2)-xb(i)
      enddo
      call multm(MetTens(1,isw,KPhase),u,ug,3,3,1)
      call vecnor(u,ug)
      call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
      call vecnor(v,vg)
      call VecMul(u,v,wg)
      call multm(MetTensI(1,isw,KPhase),wg,w,3,3,1)
      call vecnor(w,wg)
      do i=1,3
        v(i)=-v(i)-u(i)
      enddo
      call multm(MetTens(1,isw,KPhase),v,vg,3,3,1)
      call vecnor(v,vg)
      pom1=-1./(3.*scalmul(u,vg))
      if(abs(pom1).gt..99) pom1=.577350
      pom2=sqrt(1.-pom1**2)
      do j=1,2
        do i=1,3
          xh(i,j)=xb(i)+(v(i)*pom1+w(i)*pom2)*DistH
        enddo
        call RealVectorToOpposite(w,w,3)
      enddo
9999  return
      end
      subroutine EM40SplitAtoms
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Shift(3),trpom(36),upom(9),cpom(3,3),rpom(3,3)
      integer RolMenuSelectedQuest,ip(1)
      logical CrwLogicQuest,EqIgCase,FeYesNo,EqRV0
      logical, allocatable :: NUsed(:)
      character*256 EdwStringQuest
      character*80  Veta,t80
      character*8   At,AtPom
      data tpisq/19.7392088/
      call CopyFile(fln(:ifln)//'.m40',fln(:ifln)//'.z40')
      call CopyFile(fln(:ifln)//'.m50',fln(:ifln)//'.z50')
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      napp=0
      iaw=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          iaw=iaw+1
          iaLast=IAtActive(i)
        endif
      enddo
      NAtTest=1000
      if(allocated(NUsed)) deallocate(NUsed)
      allocate(NUsed(NAtTest))
      xqd=400.
      id=NextQuestId()
      iaa=0
      do iac=1,NAtActive
        if(.not.LAtActive(iac)) cycle
        call SetRealArrayTo(Shift,3,0.)
        ia=IAtActive(iac)
        iaa=iaa+1
        isw=iswa(ia)
        ksw=kswa(ia)
        isfp=isf(ia)
        Veta='Split atomic position for : '//Atom(ia)(:idel(Atom(ia)))
        il=7
        if(itf(ia).gt.1) il=il+3
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
        il=1
        tpom=5.
        Veta='%Name of the split atom:'
        xpom=tpom+FeTxLengthUnder(Veta)+70.
        dpom=80.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        Veta=Atom(ia)(:idel(Atom(ia)))//''''
        call FeQuestStringEdwOpen(EdwLastMade,Veta)
        nEdwAtName=EdwLastMade
        Veta='%Info'
        dpomb=FeTxLengthUnder(Veta)+30.
        xpomb=xpom+dpom+15.
        call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
        nButtInfo=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        il=il+1
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,'Atomic %type','L',
     1                          dpom,EdwYd,1)
        nRolMenuAtType=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                          NAtFormula(KPhase),isfp)
        il=il+1
        Veta='%Relative occupancy of the split atom:'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        call FeQuestRealEdwOpen(EdwLastMade,.5,.false.,.false.)
        nEdwOcc=EdwLastMade
        if(itf(ia).gt.1) then
          tpomc=5.
          il=il+1
          Veta='Shift of the split atom:'
          call FeQuestLblMake(id,tpomc,il,Veta,'L','N')
          xpomc=tpomc+50.
          tpomc=xpomc+CrwgXd+5.
          Veta='defined %manually'
          do j=1,2
            il=il+1
            call FeQuestCrwMake(id,tpomc,il,xpomc,il,Veta,'L',CrwXd,
     1                          CrwYd,1,1)
            if(j.eq.1) then
              nCrwShiftManual=CrwLastMade
              tpom=tpomc+FeTxLengthUnder(Veta)+5.
              call FeQuestEdwMake(id,tpom,il,xpom,il,'->','L',dpom,
     1                            EdwYd,1)
              nEdwShift=EdwLastMade
              Veta='derived from %ADP'
            else
              nCrwShiftFromADP=CrwLastMade
            endif
            call FeQuestCrwOpen(CrwLastMade,j.eq.2)
          enddo
          xpomc=xpomc-50.
          tpomc=tpomc-50.
          il=il+1
          Veta='Re%set ADP to isotropic'
          call FeQuestCrwMake(id,tpomc,il,xpomc,il,Veta,'L',CrwXd,
     1                        CrwYd,0,0)
          nCrwToIso=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,.true.)
        else
          il=il+1
          Veta='%Shift of the split atom:'
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                        EdwYd,1)
          nEdwShift=EdwLastMade
          call FeQuestRealAEdwOpen(nEdwShift,Shift,3,.false.,
     1                             .false.)
          nCrwShiftManual=0
          nCrwShiftFromADP=0
          nCrwToIso=0
          xpomc=5.
          tpomc=xpomc+CrwgXd+5.
        endif
        il=il+1
        Veta='%Generate restrict commands for REFINE'
        call FeQuestCrwMake(id,tpomc,il,xpomc,il,Veta,'L',CrwXd,
     1                      CrwYd,1,0)
        nCrwGenRest=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.true.)
        il=il+1
        tpomc=tpomc+15.
        xpomc=xpomc+15.
        Veta='R%estrict the split atoms to have identical positions'
        call FeQuestCrwMake(id,tpomc,il,xpomc,il,Veta,'L',CrwXd,
     1                      CrwYd,0,0)
        nCrwRestIdentical=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.true.)
        il=il+1
        Veta='Avoi%d'
        if(ia.ne.iaLast) Veta=Veta(:idel(Veta))//'->Go to next'
        dpoma=FeTxLengthUnder(Veta)+20.
        dpomb=FeTxLengthUnder('XXXX')+20.
        xpom=xqd*.5-dpoma-dpomb*.5-20.
        dpom=dpoma
        do i=1,3
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          if(i.eq.1) then
            Veta='%Quit'
            nButtAvoid=ButtonLastMade
          else if(i.eq.2) then
            Veta='Appl%y'
            if(ia.ne.iaLast) Veta=Veta(:idel(Veta))//'->Go to next'
            nButtQuit=ButtonLastMade
          else if(i.eq.3) then
            nButtApply=ButtonLastMade
          endif
          call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          xpom=xpom+dpom+20.
          if(i.eq.1) then
            dpom=dpomb
          else
            dpom=dpoma
          endif
        enddo
        call FeQuestMouseToButton(nButtApply)
1400    if(nCrwShiftManual.ne.0) then
          if(CrwLogicQuest(nCrwShiftManual)) then
            call FeQuestRealAEdwOpen(nEdwShift,Shift,3,.false.,
     1                               .false.)
            if(CrwLogicQuest(nCrwGenRest).and.EqRV0(Shift,3,.000001))
     1        then
              call FeQuestCrwOpen(nCrwRestIdentical,.true.)
            else
              call FeQuestCrwDisable(nCrwRestIdentical)
            endif
          else
            call FeQuestEdwDisable(nEdwShift)
            call FeQuestCrwDisable(nCrwRestIdentical)
          endif
        else
          if(CrwLogicQuest(nCrwGenRest).and.EqRV0(Shift,3,.000001)) then
            call FeQuestCrwOpen(nCrwRestIdentical,.true.)
          else
            call FeQuestCrwDisable(nCrwRestIdentical)
          endif
        endif
1500    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventCrw.and.
     1     (CheckNumber.eq.nCrwShiftManual.or.
     2      CheckNumber.eq.nCrwShiftFromADP)) then
          go to 1400
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInfo)
     1    then
          call FeFillTextInfo('em40newatomcomplete.txt',0)
          call FeInfoOut(-1.,-1.,'INFORMATION','L')
          go to 1500
        else if(CheckType.eq.EventRolMenu.and.
     1          CheckNumber.eq.nRolMenuAtType) then
          isfp=RolMenuSelectedQuest(nRolMenuAtType)
          go to 1500
        else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwGenRest)
     1    then
          isfp=RolMenuSelectedQuest(nRolMenuAtType)
          go to 1400
        else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwShift) then
          call FeQuestRealAFromEdw(nEdwShift,Shift)
          go to 1400
        else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwAtName)
     1    then
          AtPom=EdwStringQuest(nEdwAtName)
          if(AtPom.eq.' ') go to 1500
          call zhusti(AtPom)
          At=AtPom
          idl=idel(AtPom)
          if(AtPom(idl:idl).eq.'*') then
            idl=idl-1
            if(idl.le.0) go to 1650
            call SetLogicalArrayTo(NUsed,NAtTest,.false.)
            do j=1,2
              if(j.eq.1) then
                i1=1
                i2=NAtInd
              else
                i1=NAtMolFr(1,1)
                i2=NAtAll
              endif
              do i=i1,i2
                Veta=Atom(i)
                if(LocateSubstring(Veta,AtPom(:idl),.false.,.true.)
     1             .gt.0) then
                  k=idl
                  call StToInt(Veta,k,ip,1,.false.,ichp)
                  if(ichp.ne.0) cycle
                  NUsed(ip(1))=.true.
                endif
              enddo
            enddo
            do i=1,NAtTest
              if(.not.NUsed(i)) then
                write(Cislo,FormI15) i
                call Zhusti(Cislo)
                At=AtPom(:idl)//Cislo(:idel(Cislo))
                go to 1650
              endif
            enddo
          endif
1650      call uprat(At)
          call AtCheck(At,ichp,j)
          if(ichp.eq.1) then
            call FeChybne(-1.,YBottomMessage,'Unacceptable symbol in '//
     1                    'the names string, try again.',' ',
     2                    SeriousError)
            go to 1500
          endif
          call FeQuestStringEdwOpen(nEdwAtName,At)
          n=0
          do j=1,NAtFormula(KPhase)
            k=idel(AtType(j,KPhase))
            if(k.eq.0) cycle
            k=LocateSubstring(At,AtType(j,KPhase)(:k),.false.,.true.)
            if(k.ne.1) then
              cycle
            else
              n=j
              if(idel(AtType(j,KPhase)).eq.2) go to 1730
            endif
          enddo
          if(n.eq.0) go to 1500
1730      isfp=n
          call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                            NAtFormula(KPhase),isfp)
          go to 1500
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit)
     1    then
          Veta=' '
          if(napp.gt.0) then
            write(Veta,'(i5)') napp
            call zhusti(Veta)
            Veta='Do you want to discard '//Veta(:idel(Veta))//
     1           ' split atom'
            if(napp.gt.1) Veta=Veta(:idel(Veta))//'s?'
            if(FeYesNo(-1.,YBottomMessage,Veta,0)) then
              ich=1
            else
              ich=-1
            endif
          else
            Veta='Do you really want quit the procedure for this'
            if(iaa.lt.iaw) then
              write(Cislo,'(i5)') iaw-iaa+1
              call zhusti(Cislo)
              Veta=Veta(:idel(Veta))//' and remaining '//
     1             Cislo(:idel(Cislo))//' atom'
              if(iaw-iaa+1.gt.1) Veta=Veta(:idel(Veta))//'s?'
            else
              Veta=Veta(:idel(Veta))//' atom?'
            endif
            if(.not.FeYesNo(-1.,YBottomMessage,Veta,0)) then
              go to 1500
            else
              ich=1
            endif
          endif
          go to 2500
        else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAvoid)
     1    then
          ich=0
          go to 2500
        else if(CheckType.eq.EventButton.and.
     1          CheckNumber.eq.nButtApply) then
          napp=napp+1
          if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
          NAtCalc=NAtCalc+1
          NAtCalcBasic=NAtCalcBasic+1
          if(kmol(ia).gt.0) then
            im=(kmol(ia)-1)/mxp+1
            iam(im)=iam(im)+1
            NAtMolLen(isw,ksw)=NAtMolLen(isw,ksw)+1
            NAtPosLen(isw,ksw)=NAtPosLen(isw,ksw)+mam(im)
            call AtSun(NAtPosFr(1,1),NAtAll,NAtPosFr(1,1)+mam(im))
            ia=ia+mam(im)
            call AtSun(ia+1,NAtAll+mam(im),ia+2)
            do i=iac+1,NAtActive
              IAtActive(i)=IAtActive(i)+mam(im)
            enddo
          else
            NAtIndLen(isw,ksw)=NAtIndLen(isw,ksw)+1
            call AtSun(ia+1,NAtAll,ia+2)
          endif
          call EM40UpdateAtomLimits
          call AtCopy(ia,ia+1)
          isf(ia+1)=isfp
          Atom(ia+1)=At
          call FeQuestRealFromEdw(nEdwOcc,pom)
          ai(ia)=(1.-pom)*ai(ia)
          ai(ia+1)=pom*ai(ia+1)
          if(nCrwToIso.gt.0) then
            if(CrwLogic(nCrwShiftManual)) then
              call FeQuestRealAFromEdw(nEdwShift,Shift)
            else
              call srotb(TrToOrtho(1,isw,ksw),
     1                   TrToOrtho(1,isw,ksw),trpom)
              call MultM(trpom,beta(1,ia),upom,6,6,1)
              do j=1,6
                upom(j)=upom(j)/tpisq
              enddo
              do m=1,6
                call indext(m,j,k)
                cpom(j,k)=upom(m)
                if(j.ne.k) cpom(k,j)=upom(m)
              enddo
              call qln(cpom,rpom,upom,3)
              jmin=1
              jmax=1
              pmin=upom(1)
              pmax=upom(1)
              do j=2,3
                if(upom(j).gt.pmax) then
                  pmax=max(pmax,upom(j))
                  jmax=j
                endif
                if(upom(j).lt.pmin) then
                  pmin=min(pmin,upom(j))
                  jmin=j
                endif
              enddo
              pom=1.5*sqrt(pmax-pmin)
              do j=1,3
                upom(j)=pom*rpom(j,jmax)
              enddo
              call Multm(TrToOrthoI(1,isw,ksw),upom,Shift,3,3,1)
              do j=1,3
                Shift(j)=Shift(j)*CellPar(j,isw,ksw)
              enddo
c              call trmat(rpom,trpom,3,3)
c              call UnitMat(cpom,3)
c              do j=1,3
c                cpom(j,j)=upom(j)
c              enddo
c              call MultM(cpom,trpom,upom,3,3,3)
c              call MultM(rpom,upom,trpom,3,3,3)
c              if(lite(ksw).eq.0) call EM40SwitchBetaU(0,0)
            endif
            if(CrwLogicQuest(nCrwToIso)) then
              call ZMTF21(ia)
              call ZMTF21(ia+1)
            endif
          else
            call FeQuestRealAFromEdw(nEdwShift,Shift)
          endif
          if(CrwLogicQuest(nCrwGenRest)) then
            call iom40(1,0,fln(:ifln)//'.m40')
            IgnoreW=.true.
            call RefOpenCommands
            IgnoreW=.false.
            lni=NextLogicNumber()
            call OpenFile(lni,fln(:ifln)//'_restric.tmp','formatted',
     1                    'unknown')
            lno=NextLogicNumber()
            call OpenFile(lno,fln(:ifln)//'_restrin.tmp','formatted',
     1                    'unknown')
2000        read(lni,FormA,end=2200) Veta
            k=0
            call kus(Veta,k,t80)
            if(t80(1:1).eq.'!') go to 2100
            call kus(Veta,k,t80)
            if(.not.EqIgCase(t80,Atom(ia)).and.
     1         .not.EqIgCase(t80,Atom(ia+1))) go to 2100
            call kus(Veta,k,t80)
            call kus(Veta,k,t80)
            if(.not.EqIgCase(t80,Atom(ia)).and.
     1         .not.EqIgCase(t80,Atom(ia+1))) go to 2100
            if(k.lt.len(Veta)) go to 2100
            go to 2000
2100        write(lno,FormA) Veta(:idel(Veta))
            go to 2000
2200        if(CrwLogicQuest(nCrwRestIdentical)) then
              Cislo=' 1 '
            else
              Cislo=' 2 '
            endif
            Veta='restric '//Atom(ia)(:idel(Atom(ia)))//Cislo(1:3)//
     1           Atom(ia+1)(:idel(Atom(ia+1)))
            write(lno,FormA) Veta(:idel(Veta))
            close(lni)
            close(lno)
            call MoveFile(fln(:ifln)//'_restrin.tmp',
     1                    fln(:ifln)//'_restric.tmp')
            call RefRewriteCommands(1)
          endif
          do j=1,3
            x(j,ia+1)=x(j,ia)+ai(ia+1)*Shift(j)/CellPar(j,isw,ksw)
            x(j,ia  )=x(j,ia)-ai(ia)  *Shift(j)/CellPar(j,isw,ksw)
          enddo
          PrvniKiAtomu(ia+1)=PrvniKiAtomu(ia)+DelkaKiAtomu(ia)
          DelkaKiAtomu(ia+1)=0
          call ShiftKiAt(ia+1,itf(ia+1),ifr(ia+1),lasmax(ia+1),
     1                   KModA(1,ia+1),MagPar(ia+1),.false.)
          do j=iac+1,NAtActive
            IAtActive(j)=IAtActive(j)+1
          enddo
          iaLast=iaLast+1
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
2500    call FeQuestRemove(id)
        if(ich.ne.0) go to 3000
      enddo
      go to 3500
3000  if(ich.gt.0) then
        call CopyFile(fln(:ifln)//'.z40',fln(:ifln)//'.m40')
        call CopyFile(fln(:ifln)//'.z50',fln(:ifln)//'.m50')
        call iom50(0,0,fln(:ifln)//'.m50')
        call iom40(0,0,fln(:ifln)//'.m40')
      endif
3500  call DeleteFile(fln(:ifln)//'.z40')
      call DeleteFile(fln(:ifln)//'.z50')
      if(allocated(NUsed)) deallocate(NUsed)
      return
      end
      subroutine EM40AtomsDefine
      use Basic_mod
      use EditM40_mod
      use Atoms_mod
      use Molec_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension nRolMenuLocAtSystAx(2),nEdwLocAtSystSt(2),px(3),
     1          xx(6),ip(1)
      character*256 EdwStringQuest
      character*80  Veta,ErrSt,AtFill(5)
      character*27  Label(3)
      character*8   cSmbPGAt,iSmbPGAt
      character*2   Sipka,nty
      integer PrvKi,RolMenuSelectedQuest,CrwStateQuest,EdwStateQuest,
     1        RolMenuStateQuest
      logical CrwLogicQuest,EqIgCase,NUsed(:)
      allocatable NUsed
      save nEdwNo,nEdwAtomName,nRolMenuAtomType,nCrwIso,nCrwAniso,
     1     nCrwTLS,nCrwADP,nCrwMultNone,nCrwMultKappa,nCrwMultOrder,
     2     nEdwLocAtSystSt,nCrwRightHanded,nEdwADP,nEdwPointGroup,
     3     nButtPGInter,nButtPGSchoen,nEdwModPos,nRolMenuLocAtSystAx,
     4     nButtAtomList,nEdwModLast,NAtSelOld,isfo,itfo,MagParO,PrvKi,
     5     lasmaxo,nLblLocal,nLblUseCrenel,NMolSel,nLblPG,cSmbPGAt,
     6     nLblUseSawTooth,NAtMolSel,nLblTypeModFun,nButtNeigh
      data Sipka/'->'/
      data Label/'ADP parameter(s):','Multipole parameter(s):',
     1           'Modulation waves:'/
      entry EM40AtomsDefineMake(id)
      if(NKeepMax.le.0) call ReallocKeepCommands(1,8)
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      xqd=QuestXMax(id)-QuestXMin(id)
      NSel=0
      NAtSel=0
      NAtMolSel=0
      ctfa=0
      csfa=0
      clasmaxa=0
      call SetIntArrayTo(cmoda,7,-10)
      call SetIntArrayTo(ckfa,7,-10)
      CMagPar=-10
      KMagPar=-10
      CTypeModFun=-10
      CHarmLimit=-10.
      cSmbPGAt=' '
      iSmbPGAt=' '
      do i=1,NAtActive
        if(LAtActive(i)) then
          if(NAtSel.eq.0) NAtSel=i
          NSel=NSel+1
          ia=IAtActive(i)
          if(kmol(ia).gt.0) NAtMolSel=NAtMolSel+1
          if(ctfa.eq.0) then
            ctfa=itf(ia)
          else if(ctfa.gt.0) then
             if(itf(ia).ne.ctfa) ctfa=-1
          endif
          if(clasmaxa.eq.0) then
            clasmaxa=lasmax(ia)
          else if(clasmaxa.gt.0) then
             if(lasmax(ia).ne.clasmaxa) clasmaxa=-1
          endif
          if(ChargeDensities) then
            if(cSmbPGAt.eq.' ') then
              cSmbPGAt=SmbPGAt(ia)
              iSmbPGAt=SmbPGAt(ia)
            else if(.not.EqIgCase(cSmbPGAt,'ruzne')) then
              if(.not.EqIgCase(cSmbPGAt,SmbPGAt(i))) cSmbPGAt='ruzne'
            endif
          endif
          if(csfa.eq.0) then
            csfa=isf(ia)
          else if(csfa.gt.0) then
             if(isf(ia).ne.csfa) csfa=-1
          endif
          do j=1,7
            if(j.gt.2) then
              if(j.gt.itf(ia)+1) cycle
            endif
            if(cmoda(j).eq.-10) then
              cmoda(j)=KModA(j,ia)
            else if(cmoda(j).ge.0) then
              if(KModA(j,ia).ne.cmoda(j)) cmoda(j)=-1
            endif
          enddo
          do j=1,7
            if(cKFA(j).eq.-10) then
              cKFA(j)=KFA(j,ia)
            else if(cKFA(j).gt.0) then
              if(KFA(j,ia).ne.cKFA(j)) cKFA(j)=-1
            endif
          enddo
          if(CTypeModFun.eq.-10) then
            CTypeModFun=TypeModFun(ia)
          else if(CTypeModFun.ge.0) then
            if(CTypeModFun.ne.TypeModFun(ia)) CTypeModFun=-1
          endif
          if(NDimI(KPhase).gt.0) then
            if(TypeModFun(ia).eq.1) then
              if(CHarmLimit.lt.-9.) then
                CHarmLimit=OrthoEps(ia)
              else if(CHarmLimit.ge.0.) then
                if(CHarmLimit.ne.OrthoEps(ia)) CHarmLimit=-1.
              endif
            endif
          endif
          if(ia.lt.NAtMolFr(1,1)) then
            if(MagneticType(KPhase).gt.0) then
              if(CMagPar.le.-10) then
                CMagPar=MagPar(ia)
              else if(CMagPar.ne.-1) then
                if(CMagPar.ne.MagPar(ia)) CMagPar=-1
              endif
            endif
          endif
        endif
      enddo
      NAtSelOld=-1
      NAtSelAbs=IAtActive(NAtSel)
      il=1
      tpom=5.
      if(NSel.eq.1) then
        Veta='%#'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=40.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNo=EdwLastMade
        call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
        call FeQuestEudOpen(nEdwNo,1,NAtActive,1,0.,0.,0.)
        xpom=xpom+EdwYd+dpom+10.
        Veta='%List'
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAtomList=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='%Name'
        tpom=xpom+dpom+20.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=FeTxLengthUnder('XXXXXXXX')+10.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwAtomName=EdwLastMade
        tpom=xpom+dpom+20.
      else
        nEdwNo=0
        nButtAtomList=0
        nEdwAtomName=0
      endif
      Veta='%Type'
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=40.
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom+EdwYd,
     1                        EdwYd,1)
      nRolMenuAtomType=RolMenuLastMade
      il=il+1
      xpom=5.
      call FeQuestLblMake(id,xpom,il,Label(1),'L','B')
      if(NDim(KPhase).le.3) then
        if(ChargeDensities)
     1    call FeQuestLblMake(id,180.,il,Label(2),'L','B')
      else
        call FeQuestLblMake(id,180.,il,Label(3),'L','B')
      endif
      ilp=il
      xpom=5.
      tpom=xpom+20.
      Veta='%isotropic'
      ICrwGr=2
      do i=1,5
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      ICrwGr)
        if(i.eq.1) then
          nCrwIso=CrwLastMade
          Veta='%harmonic (anisotropic)'
        else if(i.eq.2) then
          nCrwAniso=CrwLastMade
          Veta='%anharmonic'
        else if(i.eq.3) then
          nCrwADP=CrwLastMade
          tpomp=tpom+FeTxLengthUnder(Veta)+5.
          xpomp=tpomp+FeTxLength(Sipka)+5.
          dpom=30.
          call FeQuestEudMake(id,tpomp,il,xpomp,il,Sipka,'L',dpom,EdwYd,
     1                        1)
          nEdwADP=EdwLastMade
          Veta='%Use TLS'
        else if(i.eq.4) then
          nCrwTLS=CrwLastMade
          ICrwGr=0
          if(NDimI(KPhase).gt.0) then
            il=9
          else
            il=il+1
          endif
          Veta='Use ma%gnetic'
        else if(i.eq.5) then
          nCrwMagnetic=CrwLastMade
        endif
      enddo
      nEdwMagnetic=0
      nCrwMultNone=0
      nCrwMultKappa=0
      nCrwMultOrder=0
      nEdwMult=0
      nLblLocal=0
      do i=1,2
        nRolMenuLocAtSystAx(i)=0
        nEdwLocAtSystSt(i)=0
      enddo
      nCrwRightHanded=0
      nEdwPointGroup=0
      nButtPGInter=0
      nButtPGSchoen=0
      nButtNeigh=0
      if(NDimI(KPhase).le.0) then
        if(ChargeDensities) then
          il=ilp
          xpom=180.
          tpom=xpom+20.
          Veta='n%one'
          do i=1,3
            il=il+1
            call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                          CrwgYd,1,3)
            if(i.eq.1) then
              Veta='%kappa'
              nCrwMultNone=CrwLastMade
            else if(i.eq.2) then
              Veta='o%rder'
              nCrwMultKappa=CrwLastMade
            else
              nCrwMultOrder=CrwLastMade
            endif
          enddo
          tpom=tpom+FeTxLength('XXXXX')+5.
          xpom=tpom+FeTxLength(Sipka)+5.
          dpom=30.
          call FeQuestEudMake(id,tpom,il,xpom,il,Sipka,'L',dpom,EdwYd,1)
          nEdwMult=EdwLastMade
          xlocal=xpom+110.
          il=ilp
          tpom=xlocal
          Veta='Local coordinate system:'
          call FeQuestLblMake(id,tpom+30.,il,Veta,'L','B')
          nLblLocal=LblLastMade
          call FeQuestLblDisable(LblLastMade)
          xpom=xlocal
          dpom=25.+EdwYd
          xp=xpom+dpom+5.
          xpp=xp+EdwYd+5.
          dpp=120.
          do i=1,2
            il=il+1
            call FeQuestRolMenuMake(id,tpom,ilp,xpom,il,' ','L',dpom,
     1                              EdwYd,1)
            nRolMenuLocAtSystAx(i)=RolMenuLastMade
            j=LocateInStringArray(SmbX,3,LocAtSystAx(NAtSelAbs)(i:i),
     1                            .true.)
            call FeQuestRolMenuOpen(RolMenuLastMade,SmbX,3,j)
            call FeQuestRolMenuDisable(RolMenuLastMade)
            call FeQuestEdwMake(id,tpom,il,xpp,il,' ','L',dpp,EdwYd,1)
            nEdwLocAtSystSt(i)=EdwLastMade
            call FeQuestStringEdwOpen(EdwLastMade,' ')
            call FeQuestEdwDisable(EdwLastMade)
          enddo
          il=il+1
          Veta='Select neigh%bor atoms'
          dpom=FeTxLengthUnder(Veta)+10.
          call FeQuestButtonMake(id,xpp,il,dpom,ButYd,Veta)
          nButtNeigh=ButtonLastMade
          il=il+1
          Veta='Ri%ght handed system'
          xpom=tpom+CrwXd+35.
          call FeQuestCrwMake(id,xpom,il,tpom+30.,il,Veta,'L',CrwXd,
     1                          CrwYd,1,0)
          nCrwRightHanded=CrwLastMade
!          call FeQuestCrwOpen(CrwLastMade,.true.)
          call FeQuestCrwDisable(CrwLastMade)
          il=il+1
          Veta='Point group:'
          xpom=tpom+60.
          dpom=60.
          call FeQuestLblMake(id,xpom+dpom*.5,il,Veta,'C','B')
          nLblPG=LblLastMade
          call FeQuestLblDisable(LblLastMade)
          il=il+1
          call FeQuestEdwMake(id,xpom,il,xpom,il,' ','C',dpom,EdwYd,1)
          nEdwPointGroup=EdwLastMade
          call FeQuestEdwDisable(EdwLastMade)
          Veta='Select Her%mann-Mauguin short symbol'
          dpom=FeTxLengthUnder(Veta)+10.
          do i=1,2
            il=il+1
            call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
            call FeQuestButtonDisable(ButtonLastMade)
            if(i.eq.1) then
              nButtPGInter=ButtonLastMade
              Veta='Select S%choenflies symbol'
            else
              nButtPGSchoen=ButtonLastMade
            endif
          enddo
        endif
        nEdwModFirst=0
        nEdwModLast=0
        nLblUseCrenel=0
        nLblUseSawTooth=0
        nCrwCrenel=0
        nCrwSawTooth=0
        nCrwZigZag=0
        nEdwMagnetic=0
        nLblTypeModFun=0
        nCrwHarm=0
        nCrwOrtho=0
        nCrwLegendre=0
        nCrwXHarm=0
        nEdwHarmLimit=0
      else
        il=ilp
        tpom=180.
        xpom=tpom+FeTxLength('ADP XXX')+EdwYd+5.
        dpom=30.
        xuse=xpom+dpom+EdwYd+20.
        xp=xuse+FeTxLength('XXXX')+15.
        tp=xp+CrwXd+5.
        xpp=tp+70.
        tpp=xpp+CrwXd+5.
        Veta='%Occupancy'
        do i=1,7
          il=il+1
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          if(i.le.2) then
            call FeQuestLblMake(id,xuse,il,'Use:','L','N')
            if(i.eq.1) then
              nLblUseCrenel=LblLastMade
            else
              nLblUseSawTooth=LblLastMade
            endif
          endif
          if(i.eq.1) then
            Veta='%Position'
            nEdwModOcc=EdwLastMade
            nEdwModFirst=EdwLastMade
            call FeQuestCrwMake(id,tp,il,xp,il,'%crenel','L',CrwXd,
     1                          CrwYd,1,0)
            nCrwCrenel=CrwLastMade
          else if(i.eq.2) then
            write(Veta,100) i,nty(i)
            nEdwModPos=EdwLastMade
            call FeQuestCrwMake(id,tp,il,xp,il,'%saw-tooth','L',
     1                          CrwXd,CrwYd,1,0)
            nCrwSawTooth=CrwLastMade
            call FeQuestCrwMake(id,tpp,il,xpp,il,'%zig-zag','L',
     1                          CrwXd,CrwYd,1,0)
            nCrwZigZag=CrwLastMade
            ilp=il+1
          else
            write(Veta,100) i,nty(i)
          endif
        enddo
        nEdwModLast=EdwLastMade
        if(MagneticType(KPhase).gt.0) then
          il=il+1
          Veta='%Magnetic'
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          nEdwMagnetic=EdwLastMade
        endif
        il=ilp
        call FeQuestLblMake(id,xuse,il,'Type of modulation functions:',
     1                      'L','B')
        nLblTypeModFun=LblLastMade
        xpom=xuse
        tpom=xuse+CrwgXd+5.
        ICrwGr=3
        Veta='harmonics (0,1)'
        do i=1,4
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGr)
          if(i.eq.1) then
            Veta='harmonics (0,1) orthogonalized to '//
     1           'crenel interval'
            nCrwHarm=CrwLastMade
          else if(i.eq.2) then
            Veta='Legendre polynomials in crenel interval'
            nCrwOrtho=CrwLastMade
          else if(i.eq.3) then
            Veta='x-harmonics in crenel interval'
            nCrwLegendre=CrwLastMade
          else if(i.eq.4) then
            nCrwXHarm=CrwLastMade
          endif
        enddo
        il=il+1
        tpom=xpom
        Veta='Selection limit for harmonics:'
        xpom=tpom+FeTxLength(Veta)+10.
        dpom=50.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwHarmLimit=EdwLastMade
      endif
      go to 2000
      entry EM40AtomsDefineUpdate
2000  if(NAtSel.ne.NAtSelOld) then
        itfo=-1
        isfo=-1
        MagParO=-1
        lasmaxo=-1
        if(kmol(NAtSelAbs).gt.0) then
          NMolSelPos=kmol(NAtSelAbs)
          NMolSel=(NMolSelPos-1)/mxp+1
          if(CrwStateQuest(NCrwTLS).eq.CrwClosed) then
            call FeQuestCrwClose(nCrwADP)
            call FeQuestCrwOpen(nCrwTLS,.false.)
          endif
          NAtSelPos=0
          do i=1,NMolSel
            NAtSelPos=NAtSelPos+iam(i)*mam(i)
          enddo
          if(NSel.le.1) NAtMolSel=1
        else
          NMolSelPos=0
          NMolSel=0
          if(CrwStateQuest(NCrwADP).eq.CrwClosed) then
            call FeQuestCrwClose(nCrwTLS)
            call FeQuestCrwOpen(nCrwADP,.false.)
          endif
          if(NSel.le.1) NAtMolSel=0
        endif
        if(NSel.le.1) then
          ctfa=itf(NAtSelAbs)
          clasmaxa=lasmax(NAtSelAbs)
          csfa=isf(NAtSelAbs)
          do j=1,7
           cmoda(j)=KModA(j,NAtSelAbs)
          enddo
          do j=1,7
            cKFA(j)=KFA(j,NAtSelAbs)
          enddo
          CTypeModFun=TypeModFun(NAtSelAbs)
          if(NDimI(KPhase).gt.0) then
            CHarmLimit=OrthoEps(NAtSelAbs)
          else
            CHarmLimit=-10.
          endif
          CMagPar=MagPar(NAtSelAbs)
          if(ChargeDensities) then
            cSmbPGAt=SmbPGAt(NAtSelAbs)
            iSmbPGAt=SmbPGAt(NAtSelAbs)
          endif
        endif
        call EM40GetAtom(NAtSelAbs,AtName,itfa,isfa,kmodan,kfan,lasmaxa,
     1                   MagParA,PrvKi)
        if(NSel.eq.1) then
          call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
          call FeQuestStringEdwOpen(nEdwAtomName,AtName)
        else
          do i=1,NAtActive
            if(LAtActive(i)) then
              ia=IAtActive(i)
              if((NMolSel.gt.0.and.kmol(ia).le.0).or.
     1           (NMolSel.le.0.and.kmol(ia).gt.0)) then
                NMolSel=-1
              endif
            endif
          enddo
          if(NMolSel.lt.0) then
            call FeQuestCrwClose(nCrwTLS)
            call FeQuestCrwClose(nCrwADP)
          endif
        endif
        if(NDimI(KPhase).gt.0) then
          call FeQuestLblOn(nLblUseCrenel)
          call FeQuestLblOn(nLblUseSawTooth)
          call FeQuestCrwOpen(nCrwCrenel,KFA(1,NAtSelAbs).ne.0)
          call FeQuestCrwOpen(nCrwSawTooth,KFA(2,NAtSelAbs).eq.1)
          call FeQuestCrwOpen(nCrwZigZag,KFA(2,NAtSelAbs).eq.2)
        endif
      endif
      if(itfa.ne.itfo) then
        if(itfo.lt.0) then
          nCrw=nCrwIso
          j=min(itfa,3)
          if(j.eq.0) j=4
          do i=1,4
            call FeQuestCrwOpen(nCrw,i.eq.j)
            if(NAtMolSel.gt.0.and.i.eq.3) then
              call FeQuestCrwDisable(nCrw)
            else if(NAtMolSel.lt.NSel.and.i.eq.4) then
              call FeQuestCrwDisable(nCrw)
            else
              if(ctfa.lt.0) then
                call FeQuestCrwLock(nCrw)
                if(itfa.gt.2) then
                  call FeQuestIntEdwOpen(nEdwADP,itfa,.false.)
                  call FeQuestEudOpen(nEdwADP,3,6,1,0.,0.,0.)
                  call FeQuestEdwLock(nEdwADP)
                endif
              endif
            endif
2100        nCrw=nCrw+1
          enddo
        else
          if(itfa.lt.0) then
            itfa=0
            if(ctfa.lt.0) ctfa=0
          endif
          if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,0)
          do i=1,NAtActive
            if(LAtActive(i).and.ctfa.ge.0) then
              ia=IAtActive(i)
              if(itfa.eq.1) then
                if(itf(ia).eq.0) call ZmTF02(ia)
                if(itf(ia).ne.1) call ZmTF21(ia)
              else if(itfa.eq.2) then
                if(itf(ia).eq.1) then
                  call ZmTF12(ia)
                else if(itf(ia).eq.0) then
                  call ZmTF02(ia)
                endif
              else if(itfa.gt.2) then
                if(itf(ia).eq.0) call ZmTF02(ia)
                call ZmAnhi(ia,itfa)
              else if(itfa.eq.0) then
                if(itf(ia).eq.1) call ZmTF12(ia)
                if(itf(ia).ne.0) call ZmTF20(ia)
              endif
            endif
          enddo
          if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,0)
        endif
        if(NDimI(KPhase).gt.0) then
          nEdw=nEdwModFirst
          j=itfa+1
          if(NMolSel.gt.0.and.j.eq.1) j=3
          do i=1,7
            if(EdwStateQuest(nEdw).ne.EdwLocked) then
              if(i.le.2.or.i.le.j) then
                call FeQuestIntEdwOpen(nEdw,kmodan(i),.false.)
                call FeQuestEudOpen(nEdw,0,mxw,1,0.,0.,0.)
              else
                call FeQuestEdwClose(nEdw)
                kmodan(i)=0
              endif
            endif
            nEdw=nEdw+1
          enddo
        endif
      endif
      itfo=itfa
      if(itfa.ge.3.and.NMolSel.eq.0) then
        if(ctfa.ge.0) then
          call FeQuestIntEdwOpen(nEdwADP,itfa,.false.)
          call FeQuestEudOpen(nEdwADP,3,6,1,0.,0.,0.)
        endif
      else
        call FeQuestEdwClose(nEdwADP)
      endif
      if(NDimI(KPhase).gt.0) then
        nEdw=nEdwModFirst
        do i=1,7
          if(cmoda(i).lt.0) then
            call FeQuestEdwLock(nEdw)
          else
c            call FeQuestIntEdwOpen(nEdw,kmodan(i),.false.)
          endif
          if(cKFA(i).lt.0) then
            if(i.eq.1) then
              call FeQuestCrwLock(nCrwCrenel)
            else if(i.eq.2) then
              call FeQuestCrwLock(nCrwSawTooth)
              call FeQuestCrwLock(nCrwZigZag)
            endif
          else
            if(i.eq.1) then
              call FeQuestCrwOpen(nCrwCrenel,cKFA(1).ne.0)
            else if(i.eq.2) then
              call FeQuestCrwOpen(nCrwSawTooth,cKFA(2).eq.1)
              call FeQuestCrwOpen(nCrwZigZag,cKFA(2).eq.2)
            endif
          endif
          nEdw=nEdw+1
        enddo
        nCrw=nCrwHarm
        if(CrwLogicQuest(NCrwCrenel)) then
          call FeQuestLblOn(nLblTypeModFun)
          do i=1,4
            call FeQuestCrwOpen(nCrw,i-1.eq.CTypeModFun)
            if(CTypeModFun.lt.0) then
              call FeQuestCrwLock(nCrw)
              call FeQuestEdwDisable(nEdwHarmLimit)
            endif
            if(i.eq.2) then
              if(CTypeModFun.eq.1) then
                if(CHarmLimit.lt.0.) then
                  if(EdwStateQuest(nEdwHarmLimit).ne.EdwOpened)
     1               call FeQuestRealEdwOpen(nEdwHarmLimit,
     2                                       OrthoEps(NAtSelAbs),
     3                                       .false.,.false.)
                  call FeQuestEdwLock(nEdwHarmLimit)
                else
                  call FeQuestRealEdwOpen(nEdwHarmLimit,
     1                                    CHarmLimit,
     2                                    .false.,.false.)
                endif
              else
                call FeQuestEdwDisable(nEdwHarmLimit)
              endif
            endif
            nCrw=nCrw+1
          enddo
        else
          call FeQuestLblDisable(nLblTypeModFun)
          do i=1,4
            call FeQuestCrwDisable(nCrw)
            nCrw=nCrw+1
          enddo
          call FeQuestEdwDisable(nEdwHarmLimit)
        endif
      endif
      if(NDimI(KPhase).le.0.and.ChargeDensities) then
        nCrw=nCrwMultNone
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.min(lasmaxa,3))
          if(clasmaxa.lt.0) call FeQuestCrwLock(nCrw)
          nCrw=nCrw+1
        enddo
        cSmbPGAt=' '
        iSmbPGAt=' '
        do i=1,NAtActive
          if(.not.LAtActive(i)) cycle
          ia=IAtActive(i)
          if(lasmaxa.ne.lasmaxo.and.clasmaxa.ge.0) then
            if(lasmaxo.gt.2.and.lasmaxa.le.2)
     1        PopV(ia)=PopV(ia)+PopAs(1,ia)
            call ZmMult(ia,lasmaxa-1)
          endif
          if(lasmaxo.gt.0.and.lasmaxo.le.2.and.lasmaxa.gt.2)
     1      call CrlSetLocAtSyst(ia)
          if(cSmbPGAt.eq.' ') then
            cSmbPGAt=SmbPGAt(ia)
            iSmbPGAt=SmbPGAt(ia)
          else if(.not.EqIgCase(cSmbPGAt,'ruzne')) then
            if(.not.EqIgCase(cSmbPGAt,SmbPGAt(i))) cSmbPGAt='ruzne'
          endif
        enddo
        if(lasmaxa.ge.3) then
          if(lasmaxo.ne.lasmaxa.or.
     1       (clasmaxa.ge.0.and.EdwStateQuest(nEdwMult).ne.EdwOpened))
     2      then
            call FeQuestIntEdwOpen(nEdwMult,lasmaxa-3,.false.)
            call FeQuestEudOpen(nEdwMult,0,7,1,0.,0.,0.)
            if(clasmaxa.lt.0) call FeQuestEdwLock(nEdwMult)
          endif
          if(NSel.eq.1) then
            do i=1,2
              n=nRolMenuLocAtSystAx(i)
              j=LocateInStringArray(SmbX,3,LocAtSystAx(NAtSelAbs)(i:i),
     1                              .true.)
              call FeQuestRolMenuOpen(n,SmbX,3,j)
              Veta=LocAtSystSt(i,NAtSelAbs)
              k=0
              call StToReal(Veta,k,xx,3,.false.,ich)
              if(ich.eq.0) then
                write(Veta,101) xx(1:3)
                call ZdrcniCisla(Veta,3)
              endif
              call FeQuestStringEdwOpen(nEdwLocAtSystSt(i),Veta)
            enddo
            call FeQuestLblOn(nLblLocal)
            call FeQuestCrwOpen(nCrwRightHanded,
     1                          .not.LocAtSense(NAtSelAbs).eq.'-')
            call FeQuestButtonOff(nButtNeigh)
            cSmbPGAt=SmbPGAt(NAtSelAbs)
            iSmbPGAt=SmbPGAt(NAtSelAbs)
          endif
          call FeQuestLblOn(nLblPG)
          if(EqIgCase(cSmbPGAt,'ruzne')) then
            call FeQuestStringEdwOpen(nEdwPointGroup,SmbPGAt(NAtSelAbs))
            call FeQuestEdwLock(nEdwPointGroup)
            call FeQuestButtonDisable(nButtPGInter,ButtonOff)
            call FeQuestButtonDisable(nButtPGSchoen,ButtonOff)
            call FeQuestButtonDisable(nButtNeigh)
          else
            call FeQuestStringEdwOpen(nEdwPointGroup,cSmbPGAt)
            call FeQuestButtonOpen(nButtPGInter,ButtonOff)
            call FeQuestButtonOpen(nButtPGSchoen,ButtonOff)
            call FeQuestButtonOff(nButtNeigh)
          endif
        else
          call FeQuestEdwDisable(nEdwMult)
          call FeQuestLblDisable(nLblLocal)
          call FeQuestLblDisable(nLblPG)
          do i=1,2
            if(RolMenuStateQuest(nRolMenuLocAtSystAx(i)).eq.
     1         RolMenuOpened) then
              j=RolMenuSelectedQuest(nRolMenuLocAtSystAx(i))
              LocAtSystAx(NAtSel)(i:i)=Smbx(j)
            endif
            call FeQuestRolMenuDisable(nRolMenuLocAtSystAx(i))
            if(EdwStateQuest(nEdwLocAtSystSt(i)).eq.EdwOpened)
     1        LocAtSystSt(i,NAtSel)=EdwStringQuest(nEdwLocAtSystSt(i))
            call FeQuestEdwDisable(nEdwLocAtSystSt(i))
            call FeQuestRolMenuDisable(nRolMenuLocAtSystAx(i))
            call FeQuestEdwDisable(nEdwLocAtSystSt(i))
          enddo
          call FeQuestCrwDisable(nCrwRightHanded)
          call FeQuestButtonDisable(nButtNeigh)
          call FeQuestButtonDisable(nButtNeigh)
          call FeQuestEdwDisable(nEdwPointGroup)
          call FeQuestButtonDisable(nButtPGInter)
          call FeQuestButtonDisable(nButtPGSchoen)
        endif
        lasmaxo=lasmaxa
      endif
      if(isfa.ne.isfo) then
        if(RolMenuStateQuest(nRolMenuAtomType).ne.RolMenuDisabled) then
          AtTypeName=AtTypeMenu(isfa)
          call FeQuestRolMenuOpen(nRolMenuAtomType,AtTypeMenu,
     1                            NAtFormula(KPhase),isfa)
        endif
        if(csfa.lt.0)
     1    call FeQuestRolMenuLock(nRolMenuAtomType)
      endif
      if(MagParA.ne.MagParO) then
        if(AtTypeMag(isfa,KPhase).eq.' ') then
          call FeQuestCrwClose(nCrwMagnetic)
          if(NDimI(KPhase).gt.0) call FeQuestEdwClose(nEdwMagnetic)
        else
          call FeQuestCrwOpen(nCrwMagnetic,MagParA.gt.0)
          if(CMagPar.lt.0) call FeQuestCrwLock(nCrwMagnetic)
          if(NDimI(KPhase).gt.0) then
            if(MagParA.gt.0) then
              if(MagParO.le.0) then
                call FeQuestIntEdwOpen(nEdwMagnetic,MagParA-1,.false.)
                call FeQuestEudOpen(nEdwMagnetic,0,mxw,1,0.,0.,0.)
                if(CMagPar.lt.0) call FeQuestEdwLock(nEdwMagnetic)
              endif
            else
              call FeQuestEdwClose(nEdwMagnetic)
            endif
          endif
        endif
        if(MagParA.ge.0) then
          do i=1,NAtActive
            if(LAtActive(i).and.CMagPar.ge.0) then
              ia=IAtActive(i)
              call ZmMag(ia,MagParA)
            endif
          enddo
        endif
      endif
      itfo=itfa
      isfo=isfa
      MagParO=MagParA
      NAtSelOld=NAtSel
      call EM40PutAtom(NAtSelAbs,NMolSel)
      go to 9999
      entry EM40AtomsDefineCheck
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNo) then
        call FeQuestIntFromEdw(nEdwNo,NAtSel)
        if(NAtSel.ne.NAtSelOld) then
          go to 3000
        else
          go to 9999
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAtomList)
     1  then
        call SelOneAtom('Select next atom',NameAtActive,NAtSel,
     1                  NAtActive,ich)
        if(NAtSel.ne.NAtSelOld.and.ich.eq.0) then
          go to 3000
        else
          NAtSel=NAtSelOld
          go to 9999
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwAtomName)
     1  then
        AtName=EdwStringQuest(nEdwAtomName)
        idl=idel(AtName)
        if(AtName(idl:idl).eq.'*') then
          NAtTest=1000
          allocate(NUsed(NAtTest))
          idl=idl-1
          if(idl.le.0) then
            i=1
            go to 2200
          endif
          call SetLogicalArrayTo(NUsed,NAtTest,.false.)
          do j=1,2
            if(j.eq.1) then
              i1=1
              i2=NAtInd
            else
              i1=NAtMolFr(1,1)
              i2=NAtAll
            endif
            do i=i1,i2
              Veta=Atom(i)
              if(LocateSubstring(Veta,AtName(:idl),.false.,.true.)
     1           .gt.0) then
                k=idl
                call StToInt(Veta,k,ip,1,.false.,ich)
                if(ich.ne.0) cycle
                NUsed(ip(1))=.true.
              endif
            enddo
          enddo
          do i=1,NAtTest
            if(.not.NUsed(i)) then
              write(Cislo,FormI15) i
              call Zhusti(Cislo)
              AtName=AtName(:idl)//Cislo(:idel(Cislo))
              exit
            endif
          enddo
          deallocate(NUsed)
        endif
        call UprAt(AtName)
        call AtCheck(AtName,i,j)
2200    if(i.ne.0) then
          if(i.eq.1) then
            Veta='Unacceptable symbol in the atom name'
          else if(i.eq.2) then
            if(j.ne.NAtSel) then
              Veta='The atom name already exists'
            else
              go to 2300
            endif
          else if(i.eq.3) then

          endif
          call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwAtomName
          go to 9999
        endif
2300    call FeQuestStringEdwOpen(nEdwAtomName,AtName)
      else if(CheckType.eq.EventRolMenuUnlock.and.
     1        CheckNumber.eq.nRolMenuAtomType) then
        csfa=isfa
        isfo=0
        go to 2000
      else if(CheckType.eq.EventEdwUnlock.and.
     1        CheckNumber.eq.nEdwADP) then
        ctfa=itfa
        itfo=-1
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuAtomType) then
        isfa=RolMenuSelectedQuest(nRolMenuAtomType)
        AtTypeName=AtTypeFull(isfa,KPhase)
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2         CheckNumber.ge.nCrwIso.and.
     3        (CheckNumber.le.nCrwADP.or.CheckNumber.le.nCrwTLS)) then
        if(CheckNumber.eq.nCrwIso) then
          itfa=1
        else if(CheckNumber.eq.nCrwAniso) then
          itfa=2
        else
          if(NMolSel.le.0) then
            itfa=3
          else
            itfa=-1
          endif
        endif
        if(CheckType.eq.EventCrwUnlock) then
          ctfa=itfa
          itfo=0
        endif
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2        CheckNumber.ge.nCrwMultNone.and.
     3        CheckNumber.le.nCrwMultOrder) then
        if(CheckNumber.eq.nCrwMultNone) then
          lasmaxa=1
        else if(CheckNumber.eq.nCrwMultKappa) then
          lasmaxa=2
        else if(CheckNumber.eq.nCrwMultOrder) then
          lasmaxa=3
          EventType=EventEdw
          EventNumber=nEdwMult
        endif
        if(CheckType.eq.EventCrwUnlock) then
          clasmaxa=lasmaxa
          lasmaxo=-1
        endif
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2         CheckNumber.ge.nCrwHarm.and.
     3         CheckNumber.le.nCrwXHarm) then
        CTypeModFun=CheckNumber-nCrwHarm
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            call EM40ChangeAtTypeMod(ia,CTypeModFun,OrthoEps(ia))
          endif
        enddo
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2         CheckNumber.eq.nCrwMagnetic) then
        if(CrwLogicQuest(nCrwMagnetic)) then
          MagParA=max(MagParO,1)
        else
          MagParA=0
        endif
        if(CheckType.eq.EventCrwUnlock) then
          CMagPar=MagParA
          MagParO=0
        endif
        go to 2000
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwADP) then
        call FeQuestIntFromEdw(nEdwADP,itfa)
        go to 2000
      else if((CheckType.eq.EventEdw.or.
     1         CheckType.eq.EventEdwUnlock).and.
     2         CheckNumber.ge.nEdwModFirst.and.
     3         CheckNumber.le.nEdwModLast.and.NDimI(KPhase).gt.0) then
        if(CheckType.eq.EventEdwUnlock) then
          j=CheckNumber-nEdwModFirst+1
          cmoda(j)=kmodan(j)
          call FeQuestIntEdwOpen(CheckNumber,kmodan(j),.false.)
        endif
        go to 2000
      else if((CheckType.eq.EventEdw.or.
     1         CheckType.eq.EventEdwUnlock).and.CheckNumber.eq.nEdwMult)
     2  then
        if(CheckType.eq.EventEdwUnlock) then
          clasmaxa=llasmaxa
        endif
        call FeQuestIntFromEdw(nEdwMult,i)
        lasmaxa=i+3
        go to 2000
      else if((CheckType.eq.EventEdw.or.
     1         CheckType.eq.EventEdwUnlock).and.
     2         CheckNumber.eq.nEdwMagnetic) then
        if(CheckType.eq.EventEdwUnlock) then
          CMagPar=MagParA
          MagParO=0
          call FeQuestCrwOpen(nCrwMagnetic,MagParA.gt.0)
        else
          call FeQuestIntFromEdw(nEdwMagnetic,i)
          MagParA=i+1
        endif
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2         CheckNumber.eq.nCrwCrenel) then
        if(CheckType.eq.EventCrwUnlock) then
          cmoda(1)=kmodan(1)
          n=cmoda(1)
          cKFA(1)=kfan(1)
        else
          call FeQuestIntFromEdw(nEdwModOcc,n)
          call FeQuestIntFromEdw(nEdwModPos,m)
          if(CrwLogicQuest(nCrwCrenel)) then
            mm=m
            if(CrwLogicQuest(nCrwSawTooth)) then
              call FeQuestCrwOff(nCrwSawTooth)
              cKFA(2)=0
              m=m-1
            endif
            if(CrwLogicQuest(nCrwZigZag)) then
              call FeQuestCrwOff(nCrwZigZag)
              cKFA(2)=0
              m=m-1
            endif
            if(mm.ne.m) call FeQuestIntEdwOpen(nEdwModPos,m,.false.)
            n=n+1
            cKFA(1)=1
          else
            n=n-1
            cKFA(1)=0
          endif
        endif
        call FeQuestIntEdwOpen(nEdwModOcc,n,.false.)
        call FeQuestEudOpen(nEdwModOcc,0,mxw,1,0.,0.,0.)
        EventType=EventEdw
        EventNumber=nEdwModOcc
        go to 2000
      else if((CheckType.eq.EventCrw.or.
     1         CheckType.eq.EventCrwUnlock).and.
     2        (CheckNumber.eq.nCrwSawTooth.or.
     3         CheckNumber.eq.nCrwZigZag)) then
        if(CheckType.eq.EventCrwUnlock) then
          cmoda(2)=kmodan(2)
          n=cmoda(2)
          cKFA(2)=kfan(2)
        else
          call FeQuestIntFromEdw(nEdwModPos,n)
          call FeQuestIntFromEdw(nEdwModOcc,m)
          mm=m
          if(CheckNumber.eq.nCrwSawTooth) then
            if(CrwLogicQuest(nCrwSawTooth)) then
              if(CrwLogicQuest(nCrwCrenel)) then
                call FeQuestCrwOff(nCrwCrenel)
                m=m-1
              endif
              if(CrwLogicQuest(nCrwZigZag)) then
                call FeQuestCrwOff(nCrwZigZag)
              else
                n=n+1
              endif
              cKFA(2)=1
            else
              n=n-1
              cKFA(2)=0
            endif
          else if(CheckNumber.eq.nCrwZigZag) then
            if(CrwLogicQuest(nCrwZigZag)) then
              if(CrwLogicQuest(nCrwCrenel)) then
                call FeQuestCrwOff(nCrwCrenel)
                m=m-1
              endif
              if(CrwLogicQuest(nCrwSawTooth)) then
                call FeQuestCrwOff(nCrwSawTooth)
              else
                n=n+1
              endif
              cKFA(2)=2
            else
              n=n-1
              cKFA(2)=0
            endif
          endif
          if(mm.ne.m) then
            call FeQuestIntEdwOpen(nEdwModOcc,m,.false.)
            cKFA(1)=0
          endif
        endif
        call FeQuestIntEdwOpen(nEdwModPos,n,.false.)
        call FeQuestEudOpen(nEdwModPos,0,mxw,1,0.,0.,0.)
        EventType=EventEdw
        EventNumber=nEdwModPos
        go to 2000
      else if((CheckType.eq.EventEdw.or.
     1         CheckType.eq.EventEdwUnlock).and.
     2        CheckNumber.eq.nEdwHarmLimit) then
        if(CheckType.eq.EventEdw) then
          call FeQuestRealFromEdw(nEdwHarmLimit,CHarmLimit)
        else
          CHarmLimit=OrthoEps(NAtSelAbs)
          call FeQuestRealEdwOpen(nEdwHarmLimit,CHarmLimit,.false.,
     1                            .false.)
        endif
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            call EM40ChangeAtTypeMod(ia,TypeModFun(ia),CHarmLimit)
          endif
        enddo
        go to 2000
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRightHanded)
     1  then
        if(CrwLogicQuest(nCrwRightHanded)) then
          LocAtSense(NAtSel)=' '
        else
          LocAtSense(NAtSel)='-'
        endif
        EventType=EventEdw
        EventNumber=nEdwLocAtSystSt(1)
        go to 9999
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.eq.nRolMenuLocAtSystAx(1).or.
     2         CheckNumber.eq.nRolMenuLocAtSystAx(2))) then
        if(CheckNumber.eq.nRolMenuLocAtSystAx(1)) then
          j=1
        else
          j=2
        endif
        i=RolMenuSelectedQuest(CheckNumber)
        LocAtSystAx(NAtSel)(j:j)=Smbx(i)
        go to 9999
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwLocAtSystSt(1).or.
     2         CheckNumber.eq.nEdwLocAtSystSt(2))) then
        if(CheckNumber.eq.nEdwLocAtSystSt(1)) then
          j=1
        else
          j=2
        endif
        i=nEdwLocAtSystSt(j)
        Veta=EdwStringQuest(i)
        if(Veta.eq.' ') go to 9999
        call CrlGetXFromAtString(Veta,0,px,ErrSt,ich)
        if(ich.gt.0) then
          call FeChybne(-1.,-1.,'in the definition of local '//
     1                  'coordinate system.',ErrSt,SeriousError)
          EventType=EventEdw
          EventNumber=i
        else
          LocAtSystSt(j,NAtSel)=Veta
          ich=0
        endif
        go to 9999
      else if((CheckType.eq.EventEdw.or.CheckType.eq.EventEdwUnlock)
     1         .and.CheckNumber.eq.nEdwPointGroup) then
        if(CheckType.eq.EventCrwUnlock) then
          cSmbPGAt=iSmbPGAt
          Veta=iSmbPGAt
        else
          Veta=EdwStringQuest(CheckNumber)
        endif
        do i=1,nSmbPG
          if(EqIgCase(Veta,SmbPGI(i))) then
            Veta=SmbPGI(i)
            go to 2460
          endif
        enddo
        do i=1,nSmbPG
          if(EqIgCase(Veta,SmbPGO(i))) then
            Veta=SmbPGO(i)
            go to 2460
          endif
        enddo
        call FeChybne(-1.,-1.,'wrong point group symbol.',' ',
     1                SeriousError)
        EventType=EventEdw
        EventNumber=CheckNumber
        go to 9999
2460    call FeQuestStringEdwOpen(nEdwPointGroup,Veta)
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            SmbPGAt(ia)=Veta
          endif
        enddo
        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtPGInter.or.
     2         CheckNumber.eq.nButtPGSchoen)) then
        ipg=LocateInStringArray(SmbPGI,nSmbPG,SmbPGAt(NAtSelAbs),.true.)
        if(ipg.le.0) ipg=LocateInStringArray(SmbPGO,nSmbPG,
     1                                       SmbPGAt(NAtSelAbs),.true.)
        Veta='Select point group'
        if(CheckNumber.eq.nButtPGInter) then
          call SelOneAtom(Veta,SmbPGI,ipg,nSmbPG,ich)
        else
          call SelOneAtom(Veta,SmbPGO,ipg,nSmbPG,ich)
        endif
        if(ich.eq.0) then
          if(CheckNumber.eq.nButtPGInter) then
            Veta=SmbPGI(ipg)
          else
            Veta=SmbPGI(ipg)
          endif
          call FeQuestStringEdwOpen(nEdwPointGroup,Veta)
          do i=1,NAtActive
            if(LAtActive(i)) then
              ia=IAtActive(i)
              SmbPGAt(ia)=Veta
            endif
          enddo
        endif
        go to 9999
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtNeigh)
     1  then
        Veta='Select atoms for local coordinate system'
        call SelNeighborAtoms(Veta,3.,Atom(NAtSelAbs),AtFill,5,n,
     1                        iswa(NAtSelAbs),ich)
        if(ich.eq.0) then
          if(n.gt.0) then
            LocAtSystSt(1,NAtSelAbs)=AtFill(1)
            call FeQuestStringEdwOpen(nEdwLocAtSystSt(1),AtFill(1))
            if(n.gt.1) then
              LocAtSystSt(2,NAtSelAbs)=AtFill(2)
              call FeQuestStringEdwOpen(nEdwLocAtSystSt(2),AtFill(2))
            endif
          endif
        endif
        go to 9999
      endif
3000  if(NAtSelOld.gt.0.and.nEdwNo.gt.0)
     1  call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
      NAtSelAbs=IAtActive(NAtSel)
      call SetLogicalArrayTo(LAtActive,NAtActive,.false.)
      LAtActive(NAtSel)=.true.
      ChargeDensities=.false.
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        if(lasmax(i).gt.0) then
          ChargeDensities=.true.
          go to 2000
        endif
      enddo
      go to 2000
9999  return
100   format('ADP %',i1,a2)
101   format(3f22.6)
      end
      subroutine EM40AtomsEdit
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension tpoma(4),xpoma(4),lmodan(8)
      character*256 EdwStringQuest
      character*80 Veta
      character*12 at,pn
      character*2  nty
      integer PrvKi,EdwStateQuest,CrwStateQuest,PrvKiMag
      logical lpom,Prvni,FeYesNoHeader,ExistFile
      save nEdwNo,nButtAtomList,NAtSelOld,nLblAtomName,nLblAtomType,
     1     nButtRefineAll,nButtFixAll,nButtReset,nEdwPrvPar,nCrwPrvPar,
     2     ltfo,PrvKi,npar,nButtEditADP,nButtSymmetry,
     3     nButtShowRest,nButtResetOcc,nButtEditModFirst,
     4     nButtEditModLast,nButtEditMult,lmodan,nEdwPrvParMag,
     5     nCrwPrvParMag
      entry EM40AtomsEditMake(id)
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      il=1
      tpom=5.
      if(NSel.eq.1) then
        Veta='%#'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=40.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNo=EdwLastMade
        call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
        call FeQuestEudOpen(nEdwNo,1,NAtActive,1,0.,0.,0.)
        xpom=xpom+EdwYd+dpom+10.
        Veta='%List'
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtAtomList=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        Veta='Name'
        tpom=xpom+dpom+20.
        xpom=tpom+FeTxLengthUnder(Veta)+10.+EdwMarginSize
        dpom=FeTxLengthUnder('XXXXXXXX')+5.
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        call FeQuestLblMake(id,xpom,il,' ','L','N')
        nLblAtomName=LblLastMade
        tpom=xpom+FeTxLengthUnder('XXXXXXXX')+30.-EdwMarginSize
        Veta='Type'
        xpom=tpom+FeTxLengthUnder(Veta)+10.+EdwMarginSize
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        call FeQuestLblMake(id,xpom,il,' ','L','N')
        nLblAtomType=LblLastMade
      else
        nEdwNo=0
        nButtAtomList=0
        nLblAtomName=0
        nLblAtomType=0
      endif
      il=il+1
      xqd=XdQuestEM40-2.*KartSidePruh
      pom=(xqd-4.*75.-10.)/4.
      pom=(xqd-80.-pom)/3.
      xpoma(4)=xqd-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      PrvKi=PrvniKiAtomu(NAtSelAbs)
      kip=PrvKi
      j=0
      do i=1,10
        call kdoco(kip,at,pn,1,pom,spom)
        if(i.gt.4.and.pn.eq.' ') pn='Uiso'
        j=j+1
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        kip=kip+1
        if(i.eq.1) then
          nEdwPrvPar=nEdw
          nCrwPrvPar=nCrw
        endif
        if(mod(i,4).eq.0) then
          il=il+1
          j=0
        endif
      enddo
      if(CMagPar.gt.-2) then
        if(CMagPar.ne.0) kip=PrvKiMag(NAtSelAbs)+PrvKi-1
        j=0
        il=il+1
        do i=1,3
          if(CMagPar.ne.0) then
            call kdoco(kip,at,pn,1,pom,spom)
            if(idel(pn).ne.3) pn='Mx0'
          else
            pn='Mx0'
          endif
          j=j+1
          call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
          if(CMagPar.ne.0) kip=kip+1
          if(i.eq.1) then
            nEdwPrvParMag=nEdw
            nCrwPrvParMag=nCrw
          endif
        enddo
      else
        nEdwPrvParMag=0
        nCrwPrvParMag=0
      endif
      il=il+1
      dpom=60.
      pom=20.
      xpom=(xqd-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          Veta='%Refine all'
        else if(i.eq.2) then
          Veta='Fi%x all'
        else if(i.eq.3) then
          Veta='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        if(i.ne.3.or.ctfa.ge.0) then
          j=ButtonOff
        else
          j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
      if(NSel.gt.1) then
        Veta='R%eset site occupancy'
      else
        Veta='Sho%w/reset site occupancy'
      endif
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=xqd*.5-1.5*dpom-15.
      do i=1,3
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        j=ButtonOff
        if(i.eq.1) then
          nButtResetOcc=ButtonLastMade
          Veta='Appl%y site symmetry'
        else if(i.eq.2) then
          nButtSymmetry=ButtonLastMade
          Veta='S%how symmetry restrictions'
        else if(i.eq.3) then
          nButtShowRest=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+20.
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=5.
      Veta='Edit special parameters:'
      call FeQuestLblMake(id,tpom,il,Veta,'L','B')
      nLblEditSpec=LblLastMade
      if(NDimI(KPhase).eq.0) then
        if(ChargeDensities) then
          iek=2
        else
          iek=1
        endif
      else
        if(NAtSelMol.eq.0) then
          iek=9
        else
          iek=5
        endif
        Veta='Edit modulation parameters:'
        call FeQuestLblMake(id,5.,il+1,Veta,'L','B')
      endif
      call FeBoldFont
      xpom0=tpom+FeTxLength(Veta)+10.
      call FeNormalFont
      xpom=xpom0
      do i=1,iek
        if(i.eq.1) then
          Veta='A%DP'
        else if(i.eq.2) then
          if(NDimI(KPhase).eq.0) then
            if(.not.ChargeDensities) cycle
            Veta='%Multipole(s)'
          else
            Veta='%Occupancy'
          endif
        else if(i.eq.3) then
          Veta='%Position'
        else if(i.eq.4) then
          Veta='Ma%gnetic'
        else
          j=i-3
          write(Veta,100) j,nty(j)
        endif
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtEditADP=ButtonLastMade
        else if(i.eq.2) then
          if(NDimI(KPhase).eq.0) then
            nButtEditMult=ButtonLastMade
            nButtEditModFirst=0
            nButtEditModLast=0
          else
            nButtEditMult=0
            nButtEditModFirst=ButtonLastMade
          endif
        endif
        if((i.eq.1.or.i.eq.4).and.NDimI(KPhase).gt.0) then
          il=il+1
          xpom=xpom0
        else
          xpom=xpom+dpom+pom
        endif
      enddo
      if(NDimI(KPhase).gt.0) nButtEditModLast=ButtonLastMade
      NAtSelOld=-1
      go to 2000
      entry EM40AtomsEditUpdate
      call EM40AtomNewBasic(nEdwPrvPar,nCrwPrvPar,npar,
     1                      nEdwPrvParMag,nCrwPrvParMag,
     2                      MagPar(NAtSelAbs))
      entry EM40AtomsEditRenew
2000  call EM40GetAtom(NAtSelAbs,AtName,itfa,isfa,kmodan,kfan,lasmaxa,
     1                 MagParA,PrvKi)
      if(ctfa.gt.0) call FeQuestButtonOpen(nButtReset,ButtonOff)
      AtTypeName=AtTypeMenu(isfa)
      ltfa=0
      llasmaxa=0
      lmodan=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          ltfa=max(ltfa,itf(ia))
          llasmaxa=max(llasmaxa,lasmax(ia))
          do j=1,7
            lmodan(j)=max(lmodan(j),KModA(j,ia))
          enddo
          lmodan(8)=max(lmodan(8),MagPar(ia)-1)
        endif
      enddo
      if(ltfa.eq.0) then
        npar=4
      else if(ltfa.eq.1) then
        npar=5
      else
        npar=10
      endif
      if(NAtSel.ne.NAtSelOld) ltfo=-1
      if(NSel.eq.1)
     1  call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
      if(ltfa.le.2) then
        call FeQuestButtonDisable(nButtEditADP)
      else
        call FeQuestButtonOff(nButtEditADP)
      endif
      if(NDimI(KPhase).eq.0) then
        if(ChargeDensities) then
          if(lasmaxa.le.1) then
            call FeQuestButtonDisable(nButtEditMult)
          else
            call FeQuestButtonOff(nButtEditMult)
          endif
        endif
      else
        nButt=nButtEditModFirst
        k=0
        do i=1,8
          if(i.eq.3) then
            k=8
          else
            k=k+1
          endif
          if(lmodan(k).gt.0) then
            call FeQuestButtonOff(nButt)
          else
            call FeQuestButtonDisable(nButt)
          endif
          if(i.eq.3) k=2
          nButt=nButt+1
        enddo
      endif
      ltfo=-1
      if(ltfa.ne.ltfo) then
        nEdw=nEdwPrvPar
        nCrw=nCrwPrvPar
        do j=1,10
          Prvni=.true.
          if(j.le.npar) then
            do i=1,NAtActive
              if(.not.LAtActive(i)) cycle
              ia=IAtActive(i)
              kip=PrvniKiAtomu(ia)+j-1
              call kdoco(kip,at,pn,1,pom,spom)
              if(Prvni) then
                call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                call FeQuestCrwOpen(nCrw,KiA(j,ia).ne.0)
                call FeQuestEdwLabelChange(nEdw,pn)
                pomc=pom
                kic=KiA(j,ia)
                Prvni=.false.
              else
                if(EdwStateQuest(nEdw).ne.EdwLocked.and.
     1             abs(pom-pomc).gt..000001)
     2            call FeQuestEdwLock(nEdw)
                if(CrwStateQuest(nCrw).ne.CrwLocked.and.
     1             KiA(j,ia).ne.kic)
     2            call FeQuestCrwLock(nCrw)
              endif
            enddo
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
      if(CMagPar.gt.-2) then
        nEdw=nEdwPrvParMag
        nCrw=nCrwPrvParMag
        do j=1,3
          Prvni=.true.
          if(MagPar(ia).gt.0) then
            do i=1,NAtActive
              if(.not.LAtActive(i)) cycle
              ia=IAtActive(i)
              kip=PrvKiMag(ia)+j-1
              call kdoco(kip+PrvniKiAtomu(ia)-1,at,pn,1,pom,spom)
              if(Prvni) then
                call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                call FeQuestCrwOpen(nCrw,KiA(kip,ia).ne.0)
                call FeQuestEdwLabelChange(nEdw,pn)
                pomc=pom
                kic=KiA(kip,ia)
                Prvni=.false.
              else
                if(EdwStateQuest(nEdw).ne.EdwLocked.and.
     1             abs(pom-pomc).gt..000001)
     2            call FeQuestEdwLock(nEdw)
                if(CrwStateQuest(nCrw).ne.CrwLocked.and.
     1             KiA(kip,ia).ne.kic)
     2            call FeQuestCrwLock(nCrw)
              endif
            enddo
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
      if(nLblAtomName.gt.0) then
        call FeQuestLblChange(nLblAtomName,AtName)
        call FeQuestLblChange(nLblAtomType,AtTypeName)
      endif
      NAtSelOld=NAtSel
      ltfo=ltfa
      call EM40AtomNewBasic(nEdwPrvPar,nCrwPrvPar,npar,
     1                      nEdwPrvParMag,nCrwPrvParMag,
     2                      MagPar(NAtSelAbs))
      go to 9999
      entry EM40AtomsEditCheck
      call EM40AtomNewBasic(nEdwPrvPar,nCrwPrvPar,npar,
     1                      nEdwPrvParMag,nCrwPrvParMag,
     2                      MagPar(NAtSelAbs))
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNo) then
        call FeQuestIntFromEdw(nEdwNo,NAtSel)
        NAtSelAbs=IAtActive(NAtSel)
        call SetLogicalArrayTo(LAtActive,NAtActive,.false.)
        LAtActive(NAtSel)=.true.
        if(NAtSel.ne.NAtSelOld) go to 5000
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwAtomName)
     1  then
        AtName=EdwStringQuest(nEdwAtomName)
        call UprAt(AtName)
        call AtCheck(AtName,i,j)
        if(i.ne.0) then
          if(i.eq.1) then
            Veta='Unacceptable symbol in the atom name.'
          else if(i.eq.2) then
            if(j.ne.NAtSel) then
              Veta='The atom name already exists.'
            else
              go to 2200
            endif
          endif
          call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwAtomName
          go to 9999
        endif
2200    call FeQuestStringEdwOpen(nEdwAtomName,AtName)
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAtomList)
     1  then
        call SelOneAtom('Select next atom',NameAtActive,NAtSel,
     1                  NAtActive,ich)
        NAtSelAbs=IAtActive(NAtSel)
        if(NAtSel.ne.NAtSelOld.and.ich.eq.0) then
          go to 5000
        else
          NAtSel=NAtSelOld
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrvPar
        do i=1,npar
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(1,NAtSel)
        pom=2.5
        isw=iswa(NAtSel)
        if(ltfa.ge.2) then
          do i=1,6
            beta(i,NAtSel)=pom*prcp(i,isw,KPhase)
          enddo
        else if(ltfa.eq.1) then
          beta(1,NAtSel)=pom
        endif
        if(lite(KPhase).eq.0) call EM40SwitchBetaU(0,NAtSel)
        nEdw=nEdwPrvPar+4
        do i=1,npar-4
          call FeQuestRealEdwOpen(nEdw,beta(i,NAtSel),.false.,.false.)
          nEdw=nEdw+1
        enddo
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditADP)
     1  then
        call EM40EditADP
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.ge.nButtEditModFirst.and.
     2        CheckNumber.le.nButtEditModLast) then
        n=CheckNumber-nButtEditModFirst+1
        if(n.eq.3) then
          n=8
        else if(n.gt.3) then
          n=n-1
        endif
        call EM40EditAtomModPar(n,lmodan(n))
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditMult)
     1  then
        call EM40EditMultipole
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtSymmetry.or.
     2         CheckNumber.eq.nButtShowRest)) then
        Veta=fln(:ifln)//'_pom.tmp'
        KPhaseIn=KPhase
        nvai=0
        neq=0
        neqs=0
        LstOpened=.true.
        uloha='Symmetry restrictions'
        call OpenFile(lst,Veta,'formatted','unknown')
        call NewPg(1)
        MaxUsedItfAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          MaxUsedItf(KPh)=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh) cycle
            MaxUsedItf(KPh)=max(MaxUsedItf(KPh),itf(i))
            if(NDimI(KPh).le.0) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
          MaxUsedItfAll=max(MaxUsedItfAll,MaxUsedItf(KPh))
        enddo
        if(Allocated(KWSym)) deallocate(KwSym)
        KPhase=kswa(ia)
        if(NDimI(KPhase).gt.0) then
          allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
          call SetSymmWaves
        endif
        call MagParToCell(0)
        ia=IAtActive(NAtSelOld)
        call atspec(ia,ia,NAtSelMolPos,NAtSelMol,NAtSelPos)
        do i=1,neq
          KiA(lnp(i)-PrvniKiAtomu(ia)+1,NAtSelOld)=0
        enddo
        call RefAppEq(0,0,0)
        call MagParToBohrMag(0)
        call CloseListing
        LstOpened=.false.
        if(.not.ExistFile(Veta)) then
          LstOpened=.true.
          uloha='Symmetry restrictions'
          call OpenFile(lst,Veta,'formatted','unknown')
          call NewPg(1)
          call newln(2)
          write(lst,FormA)
          write(lst,FormA) 'No symmetry restrictions -  the atom '//
     1                     'is located at a general position'
          call CloseListing
          LstOpened=.false.
        endif
        if(CheckNumber.eq.nButtShowRest) call FeListView(Veta,0)
        call DeleteFile(Veta)
        NAtSelOld=0
        KPhase=KPhaseIn
        go to 2000
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtResetOcc)
     1  then
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            xpoma(1)=0.
            k=0
            if(NDimI(KPhase).eq.1) then
              if(KFA(1,ia).ne.0) then
                xpoma(1)=ax(KModA(1,ia),ia)
                k=1
              else if(KFA(2,ia).ne.0) then
                xpoma(1)=uy(1,KModA(2,ia),ia)
                k=1
              endif
            endif
            call SpecPos(x(1,ia),xpoma,k,iswa(ia),.1,nocc)
            pom=1./float(nocc)
            if(NSel.eq.1) then
              NInfo=1
              write(Cislo,'(f9.6)') 1./float(nocc)
              TextInfo(1)='The full site occupacy is: '//
     1                    Cislo(:idel(Cislo))
              if(abs(ai(ia)-pom).lt..000001) then
                call FeInfoOut(-1.,-1.,'Information','C')
              else
                if(FeYesNoHeader(-1.,-1.,'Do you want to reset the '//
     1                           'actual occupancy?',0)) then
                  ai(ia)=pom
                  NAtSelOld=0
                  go to 2000
                endif
              endif
            else
              ai(ia)=pom
            endif
          endif
        enddo
        go to 2000
      endif
      go to 9999
5000  if(NAtSelOld.ne.0) then
        call FeQuestIntEdwOpen(nEdwNo,NAtSel,.false.)
      endif
      ChargeDensities=.false.
      do i=NAtIndFrAll(KPhase),NAtIndToAll(KPhase)
        if(lasmax(i).gt.0) then
          ChargeDensities=.true.
          go to 2000
        endif
      enddo
      go to 2000
9999  return
100   format('ADP %',i1,a2)
      end
      subroutine EM40AtomsUpdateListek(KartIdOld)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      if(KartId.eq.KartIdDefine) then
        call EM40AtomsDefineUpdate
      else if(KartId.eq.KartIdBasic) then
        call EM40AtomsEditRenew
      endif
      return
      end
      subroutine EM40PutAtom(iao,im)
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      integer CrwStateQuest,EdwStateQuest
      logical CrwLogicQuest
      integer TypeModFunN
      dimension kmodanp(7)
      if(NDimI(KPhase).le.0) then
        if(ChargeDensities) then
          if(lasmaxa.ge.3) then
            if(EdwStateQuest(nEdwMult).eq.EdwOpened) then
              call FeQuestIntFromEdw(nEdwMult,lasmaxa)
              lasmaxa=lasmaxa+3
            endif
          endif
        endif
      else
        nEdw=nEdwModFirst
        j=itfa+1
        if(im.gt.0.and.j.eq.1) j=3
        do i=1,7
          if(i.le.j) then
            if(EdwStateQuest(nEdw).eq.EdwOpened) then
              call FeQuestIntFromEdw(nEdw,kmodan(i))
              cmoda(i)=kmodan(i)
            else
              kmodan(i)=KModA(i,iao)
            endif
            nEdw=nEdw+1
          else
            kmodan(i)=0
          endif
        enddo
        TypeModFunN=0
        if(CrwStateQuest(nCrwCrenel).eq.CrwLocked) then
          kfsn=-1
        else
          kfsn=0
          if(CrwStateQuest(nCrwCrenel).ne.CrwClosed) then
            if(CrwLogicQuest(nCrwCrenel)) then
              kfsn=1
              nCrw=nCrwHarm
              do i=1,4
                if(CrwLogicQuest(nCrw)) then
                  TypeModFunN=i-1
                  exit
                endif
                nCrw=nCrw+1
              enddo
            endif
          endif
        endif
        if(CrwStateQuest(nCrwSawTooth).eq.CrwLocked) then
          kfxn=-1
        else
          kfxn=0
          if(CrwStateQuest(nCrwSawTooth).ne.CrwClosed) then
            if(CrwLogicQuest(nCrwSawTooth)) then
              kfxn=1
            else if(CrwLogicQuest(nCrwZigZag)) then
              kfxn=2
            endif
          endif
        endif
        if(nEdwMagnetic.gt.0) then
          if(CrwLogicQuest(nCrwMagnetic)) then
            call FeQuestIntFromEdw(nEdwMagnetic,i)
            MagParA=i+1
          else
            MagParA=0
          endif
        endif
      endif
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(NSel.eq.1) then
            call CrlAtomNamesSave(Atom(ia),AtName,-1)
            Atom(ia)=AtName
          endif
          if(ctfa.gt.0) then
            call ZmAnhi(ia,itfa)
            itf(ia)=itfa
          endif
          if(csfa.gt.0) isf(ia)=isfa
          if(NDimI(KPhase).le.0) then
            if(ChargeDensities) then
              if(clasmaxa.gt.0) call ZmMult(ia,lasmaxa-1)
            endif
          else
            do j=1,7
              k=itf(ia)+1
              if(im.gt.0.and.k.eq.1) k=3
              if(cmoda(j).ge.0.and.j.le.k) then
                kmodanp(j)=kmodan(j)
              else
                kmodanp(j)=-1
              endif
            enddo
            call AtModi(ia,kmodanp)
            if(cmoda(1).gt.0.and.kfsn.ge.0) then
              kfso=KFA(1,ia)
              KFA(1,ia)=min(kfso,kfsn)
              TypeModFun(ia)=TypeModFunN
              if(CHarmLimit.gt.0.) OrthoEps(ia)=CHarmLimit
            endif
            if(cmoda(2).gt.0.and.kfxn.ge.0) then
              kfxo=KFA(2,ia)
              KFA(2,ia)=min(kfxo,kfxn)
              if(KFA(2,ia).gt.0) TypeModFun(ia)=0
            endif
            kip=max(TRankCumul(itf(ia)),10)+1
            if(cmoda(1).gt.0.and.kfsn.ge.0) then
              KFA(1,ia)=kfsn
              k=KModA(1,ia)-NDimI(KPhase)
              if(k.ge.0) then
                if(kfsn.ne.kfso.and.kfsn.ne.0) then
                  if(NDimI(KPhase).eq.1) then
                    pom=0.
                  else
                    if(a0(ia).gt.0.) then
                      pom=a0(ia)**(1./float(NDimI(KPhase)))
                    else
                      pom=0.
                    endif
                  endif
                  do j=1,NDimI(KPhase)
                    k=k+1
                    ax(k,ia)=0.
                    ay(k,ia)=pom
                    KiA(kip,ia)=0
                    KiA(kip+(k-1)*2+1,ia)=0
                    KiA(kip+(k-1)*2+2,ia)=0
                  enddo
                  if(NSel.eq.1) call EM40EditAtomModPar(1,1)
                endif
              endif
              if(k.gt.0) kip=kip+2*k+1
            endif
            if(cmoda(2).gt.0.and.kfxn.gt.0) then
              KFA(2,ia)=kfxn
              k=KModA(2,ia)
              if(k.gt.0) then
                if(kfxn.ne.kfxo.and.kfxn.eq.1) then
                  call SetRealArrayTo(ux(1,k,ia),3,.001)
                  call SetRealArrayTo(uy(1,k,ia),3,0.)
                  uy(2,k,ia)=1.
                  call SetIntArrayTo(KiA(kip+(k-1)*6,ia),6,0)
                endif
              endif
            endif
          endif
        endif
      enddo
      return
      end
      subroutine EM40GetAtom(ia,AtName,itfa,isfa,kmodan,kfan,lasmaxa,
     1                       MagParA,PrvKi)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kmodan(*),kfan(*)
      character*8  AtName
      integer PrvKi
      AtName=Atom(ia)
      itfa=itf(ia)
      isfa=isf(ia)
      do i=1,7
        kmodan(i)=KModA(i,ia)
      enddo
      do i=1,7
        kfan(i)=KFA(i,ia)
      enddo
      lasmaxa=lasmax(ia)+1
      MagParA=MagPar(ia)
      PrvKi=1
      return
      end
      subroutine EM40AtomNewBasic(nEdwPrvPar,nCrwPrvPar,npar,
     1                            nEdwPrvParMag,nCrwPrvParMag,MagParP)
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kip(10),p(10)
      integer PrvKiMag,EdwStateQuest,CrwStateQuest
      call SetRealArrayTo(p,10,0.)
      call FeUpdateParamAndKeys(nEdwPrvPar,nCrwPrvPar,p,kip,npar)
      do j=1,npar
        if(kip(j).ge.110) cycle
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            if(kip(j)/100.eq.0) then
              if(j.eq.1) then
                ai(ia)=p(j)
              else if(j.le.4) then
                x(j-1,ia)=p(j)
              else
                beta(j-4,ia)=p(j)
              endif
            endif
            if(mod(kip(j),100).lt.10)
     1        KiA(j,ia)=mod(kip(j),10)
          endif
        enddo
      enddo
      j=0
      if(nEdwPrvParMag.le.0) go to 9999
      if(MagParP.gt.0) then
        call FeUpdateParamAndKeys(nEdwPrvParMag,nCrwPrvParMag,p,kip,3)
        nEdw=nEdwPrvParMag
        nCrw=nCrwPrvParMag
        do j=1,3
          if(kip(j).ge.110) cycle
          do i=1,NAtActive
            if(LAtActive(i)) then
              ia=IAtActive(i)
              k=PrvKiMag(ia)+j-1
              if(kip(j)/100.eq.0.and.EdwStateQuest(nEdw).eq.EdwOpened)
     1          sm0(j,ia)=p(j)
              if(mod(kip(j),100).lt.10.and.
     1          (CrwStateQuest(nCrw).eq.CrwOff.or.
     2           CrwStateQuest(nCrw).eq.CrwOn)) KiA(k,ia)=mod(kip(j),10)
            endif
          enddo
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      endif
9999  return
      end
      subroutine EM40EditAtomModPar(ModPar,lmod)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(4),xpoma(4),px(:,:),py(:,:),spx(:,:),spy(:,:),
     1          kip(:),pxp(:),kipp(:)
      character*80 Veta
      character*12 at,pn,pompn
      integer PrvKiMod,Order,PrvPar,PrvParOld,Harm,HarmOld,PrvKiMag,
     1        HarmP,HarmPOld
      logical :: lpom,Konec,InSig = .false.
      allocatable px,py,spx,spy,kip,pxp,kipp
      Konec=.false.
      if(ModPar.le.7) then
        Order=TRank(ModPar-1)
      else
        Order=3
      endif
      nki=2*lmod*Order
      if(ModPar.eq.1) nki=nki+1
      allocate(px(Order,lmod),py(Order,lmod),spx(Order,lmod),
     1         spy(Order,lmod),kip(nki),pxp(2*Order+1),kipp(2*Order+1))
      NAtEdit=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          NAtEdit=NAtEdit+1
          ia=IAtActive(i)
          l=lmod*Order
          if(ModPar.le.7) then
            if(KModA(ModPar,ia).eq.lmod) then
              if(ModPar.eq.1) then
                p0=a0(ia)
                sp0=sa0(ia)
                call CopyVek(ax(1,ia),px,l)
                call CopyVek(ay(1,ia),py,l)
                call CopyVek(sax(1,ia),spx,l)
                call CopyVek(say(1,ia),spy,l)
              else if(ModPar.eq.2) then
                call CopyVek(ux(1,1,ia),px,l)
                call CopyVek(uy(1,1,ia),py,l)
                call CopyVek(sux(1,1,ia),spx,l)
                call CopyVek(suy(1,1,ia),spy,l)
              else if(ModPar.eq.3) then
                if(lite(KPhase).eq.0) then
                  do k=1,lmod
                    do j=1,6
                       bx(j,k,ia)= bx(j,k,ia)/urcp(j,iswa(ia),KPhase)
                       by(j,k,ia)= by(j,k,ia)/urcp(j,iswa(ia),KPhase)
                      sbx(j,k,ia)=sbx(j,k,ia)/urcp(j,iswa(ia),KPhase)
                      sby(j,k,ia)=sby(j,k,ia)/urcp(j,iswa(ia),KPhase)
                    enddo
                  enddo
                endif
                call CopyVek(bx(1,1,ia),px,l)
                call CopyVek(by(1,1,ia),py,l)
                call CopyVek(sbx(1,1,ia),spx,l)
                call CopyVek(sby(1,1,ia),spy,l)
              else if(ModPar.eq.4) then
                call CopyVek(c3x(1,1,ia),px,l)
                call CopyVek(c3y(1,1,ia),py,l)
                call CopyVek(sc3x(1,1,ia),spx,l)
                call CopyVek(sc3y(1,1,ia),spy,l)
              else if(ModPar.eq.5) then
                call CopyVek(c4x(1,1,ia),px,l)
                call CopyVek(c4y(1,1,ia),py,l)
                call CopyVek(sc4x(1,1,ia),spx,l)
                call CopyVek(sc4y(1,1,ia),spy,l)
              else if(ModPar.eq.6) then
                call CopyVek(c5x(1,1,ia),px,l)
                call CopyVek(c5y(1,1,ia),py,l)
                call CopyVek(sc5x(1,1,ia),spx,l)
                call CopyVek(sc5y(1,1,ia),spy,l)
              else if(ModPar.eq.7) then
                call CopyVek(c6x(1,1,ia),px,l)
                call CopyVek(c6y(1,1,ia),py,l)
                call CopyVek(sc6x(1,1,ia),spx,l)
                call CopyVek(sc6y(1,1,ia),spy,l)
              endif
              PrvKiMod=max(TRankCumul(itf(ia)),10)+1
              do j=1,ModPar-1
                if(KModA(j,ia).gt.0) then
                    PrvKiMod=PrvKiMod+2*TRank(j-1)*KModA(j,ia)
                  if(j.eq.1) PrvKiMod=PrvKiMod+1
                endif
              enddo
            endif
          else
            call CopyVek(smx(1,1,ia),px,l)
            call CopyVek(smy(1,1,ia),py,l)
            call CopyVek(ssmx(1,1,ia),spx,l)
            call CopyVek(ssmy(1,1,ia),spy,l)
            PrvKiMod=PrvKiMag(ia)+3
          endif
          call CopyVekI(KiA(PrvKiMod,ia),kip,nki)
          go to 1200
        endif
      enddo
      call FeWinMessage('Co, co, co???',' ')
1200  if(NAtEdit.gt.1) InSig=.false.
      if(NAtActive.gt.0) then
        do 1250i=1,NAtActive
          if(LAtActive(i)) then
            iap=IAtActive(i)
            if(ModPar.le.7) then
              if(KModA(ModPar,iap).le.0) go to 1250
              l=max(TRankCumul(itf(iap)),10)+1
              do j=1,ModPar-1
                if(KModA(j,iap).gt.0) then
                    l=l+2*TRank(j-1)*KModA(j,iap)
                  if(j.eq.1) l=l+1
                endif
              enddo
              kmodp=KModA(ModPar,iap)
            else
              l=PrvKiMag(iap)+3
              kmodp=MagPar(iap)-1
            endif
            k=1
            if(ModPar.eq.1) then
              if(abs(p0-a0(iap)).gt..000001.and.kip(k).lt.100)
     1          kip(k)=kip(k)+100
              if(mod(kip(k),10).ne.KiA(l,iap).and.mod(kip(k),100).lt.10)
     1          kip(k)=kip(k)+10
              k=k+1
              l=l+1
            endif
            do jj=1,kmodp
              do ii=1,Order
                call kdoco(l+PrvniKiAtomu(iap)-1,at,pn,1,pom,spom)
                if(abs(px(ii,jj)-pom).gt..000001.and.kip(k).lt.100)
     1            kip(k)=kip(k)+100
                if(mod(kip(k),10).ne.KiA(l,iap).and.
     1             mod(kip(k),100).lt.10) kip(k)=kip(k)+10
                l=l+1
                k=k+1
              enddo
              do ii=1,Order
                call kdoco(l+PrvniKiAtomu(iap)-1,at,pn,1,pom,spom)
                if(abs(py(ii,jj)-pom).gt..000001.and.kip(k).lt.100)
     1            kip(k)=kip(k)+100
                if(mod(kip(k),10).ne.KiA(l,iap).and.
     1             mod(kip(k),100).lt.10) kip(k)=kip(k)+10
                l=l+1
                k=k+1
              enddo
            enddo
          endif
1250    continue
      endif
      if(ModPar.le.4.or.ModPar.eq.8) then
        nn=2
      else
        nn=1
      endif
      no=nn*Order
      if(ModPar.eq.1) then
        il=3
      else
        il=nn*((Order-1)/3+2)+1
      endif
      if(ModPar.eq.1.or.ModPar.eq.2) then
        if(KFA(2,ia).ne.0) then
          pompn='x-slope'
        else
          pompn='xcos33'
        endif
      else if(ModPar.eq.3) then
        pompn='U33cos33'
      else if(ModPar.eq.4) then
        pompn='C333cos33'
      else if(ModPar.eq.5) then
        pompn='D3333cos33'
      else if(ModPar.eq.6) then
        pompn='E33333cos33'
      else if(ModPar.eq.7) then
        pompn='F333333cos33'
      else
        pompn='Mxsin33'
      endif
      xqd=550.+3.*(FeTxLength(pompn)-FeTxLength('F111111cos11'))
      xqdp=xqd*.5
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      pom=FeTxLengthUnder(pompn)+5.
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Edit modulation amplitudes',
     2                   1,LightGray,0,OKForBasicFiles)
      il=1
      at='%Wave'
      xpom=xqdp
      tpom=xpom-FeTxLength(at)-5.
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,at,'L',dpom,EdwYd,1)
      nEdwHarm=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
      call FeQuestEudOpen(EdwLastMade,1,(2*lmod)/nn,1,0.,0.,0.)
      il=il+1
      j=1
      if(ModPar.eq.1) no=no+1
      do i=1,no
        if(i.eq.Order+1.and.ModPar.gt.1) then
          if(j.ne.1) il=il+1
          call FeQuestLinkaMake(id,il)
          il=il+1
          j=1
        endif
        call kdoco(PrvKiMod+PrvniKiAtomu(ia)+i-2,at,pn,1,pom,spom)
        if(ModPar.ne.1.or.i.ne.1) pn=pompn
        call FeQuestLblMake(id,tpoma(j),il,pn,'R','N')
        call FeQuestLblMake(id,xpoma(j)+6.,il,' ','L','N')
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        if(.not.InSig) then
          call FeQuestLblOff(LblLastMade-1)
          call FeQuestLblOff(LblLastMade)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kip(i),
     1                         .false.)
        endif
        if(i.eq.1) then
          nLblFirst=LblLastMade-1
          nEdwPrv=nEdw
          nCrwPrv=nCrw
          PrvPar=i
        else if(i.eq.no) then
          exit
        endif
        if(j.eq.3) then
          j=1
          il=il+1
        else
          j=j+1
        endif
      enddo
      iki=1
      Harm=1
      HarmOld=1
      PrvParOld=PrvPar
      il=il+1
      dpom=80.
      pom=15.
      xpom=(xqd-4.*dpom-2.5*pom)*.5
      Veta='%Refine all'
      do i=1,4
        j=ButtonOff
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
          Veta='%Fix all'
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
          Veta='Re%set'
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
          Veta='Show %p/sig(p)'
        else if(i.eq.4) then
          nButtInSig=ButtonLastMade
          if(NAtEdit.gt.1) j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
1400  k=0
      m=0
      idf=nn*(Harm-1)*Order+1-iki
      if(nn.eq.1) then
        HarmP=(Harm-1)/2+1
        HarmPOld=(HarmOld-1)/2+1
        lold=mod(HarmOld-1,2)+1
        l=mod(Harm-1,2)+1
      else
        HarmP=Harm
        HarmPOld=HarmOld
        l=1
        lold=1
      endif
      if(.not.InSig) call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,pxp,
     1                                         kipp,no)
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      nLbl=nLblFirst
      do i=iki,iki+no-1
        if(ModPar.eq.1) then
          if(i.eq.iki) then
            j=1
          else
            j=i
            k=k+1
          endif
        else
          j=i
          k=k+1
        endif
        if(k.gt.Order) then
          k=1
          l=2
          lold=2
        endif
        if(.not.InSig) then
          m=m+1
          kip(j)=kipp(m)
          if(k.eq.0) then
            if(kip(j).lt.100) p0=pxp(m)
          else
            if(lold.eq.1) then
              if(kip(j).lt.100) px(k,HarmPOld)=pxp(m)
            else
              if(kip(j).lt.100) py(k,HarmPOld)=pxp(m)
            endif
          endif
        endif
        if(ModPar.eq.1.and.i.eq.iki) then
          pom=p0
          poms=sp0
        else
          if(l.eq.1) then
            pom=px(k,HarmP)
            poms=spx(k,HarmP)
          else
            pom=py(k,HarmP)
            poms=spy(k,HarmP)
          endif
        endif
        if(InSig) then
          if(poms.gt.0.) then
            pom=abs(pom/poms)
          else
            pom=-1.
          endif
        endif
        if(ModPar.ne.1.or.i.ne.iki) j=j+idf
        call kdoco(PrvKiMod+PrvniKiAtomu(ia)+j-2,at,pn,1,pp,ss)
        if(InSig) then
          if(ModPar.ne.1.or.i.ne.iki) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
            call FeQuestLblChange(nLbl,pn)
            if(pom.lt.0.) then
              Cislo='fixed'
            else if(pom.lt.3.) then
              Cislo='< 3*su'
            else
              write(Cislo,'(f15.1)') pom
              call ZdrcniCisla(Cislo,1)
              Cislo=Cislo(:idel(Cislo))//'*su'
            endif
            call FeQuestLblChange(nLbl+1,Cislo)
          endif
        else
          call FeOpenParEdwCrw(nEdw,nCrw,pn,pom,kip(j),.false.)
        endif
        if(i.eq.iki+no-1.and.
     1     ((ModPar.eq.1.and.KFA(1,ia).ne.0.and.
     2       HarmP.eq.KModA(1,ia)).or.
     3      (ModPar.eq.2.and.KFA(2,ia).ne.0.and.
     4       HarmP.eq.KModA(2,ia)))) then
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
1410    nEdw=nEdw+1
        nCrw=nCrw+1
        nLbl=nLbl+2
      enddo
      if(Konec) go to 3000
      if(idf.ne.0) then
        iki=iki+idf
        HarmOld=Harm
      endif
      if(InSig) then
        j=ButtonDisabled
      else
        j=ButtonOff
      endif
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        Konec=.true.
        go to 1400
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwHarm) then
        call FeQuestIntFromEdw(nEdwHarm,Harm)
        if(Harm.ne.HarmOld) then
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,no
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        if(ModPar.eq.1) then
          pom=.01
        else if(ModPar.eq.2) then
          pom=.001
        else if(ModPar.eq.3) then
          pom=.0001
        else if(ModPar.eq.8) then
          pom=.1
        else
          pom=.00001
        endif
        do i=1,no
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInSig)
     1  then
        if(nn.eq.1) then
          l=mod(Harm-1,2)+1
        else
          l=1
        endif
        m=0
        k=0
        if(InSig) then
          nEdw=nEdwPrv
          nCrw=nCrwPrv
          do i=iki,iki+no-1
            m=m+1
            if(ModPar.eq.1.and.i.eq.iki) then
              go to 1700
            else
              k=k+1
              if(k.gt.Order) then
                k=1
                l=2
              endif
              if(l.eq.1) then
                pom=px(k,HarmP)
              else if(l.eq.2) then
                pom=py(k,HarmP)
              endif
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
              call FeQuestCrwOpen(nCrw,kipp(m).ne.0)
            endif
1700        nEdw=nEdw+1
            nCrw=nCrw+1
          enddo
        else
          call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,pxp,kipp,no)
          do i=iki,iki+no-1
            m=m+1
            if(ModPar.eq.1.and.i.eq.iki) then
              j=1
              l=0
            else
              j=i
              k=k+1
            endif
            if(k.gt.Order) then
              k=1
              l=2
            endif
            kip(j)=kipp(m)
            if(l.eq.1) then
              px(k,HarmP)=pxp(m)
            else if(l.eq.2) then
              py(k,HarmP)=pxp(m)
            endif
          enddo
        endif
        InSig=.not.InSig
        if(InSig) then
          Veta='%Edit mode'
        else
          Veta='Show %p/sig(p)'
        endif
        call FeQuestButtonLabelChange(nButtInSig,Veta)
        nButt=nButtRefineAll
        go to 1400
      else if(CheckType.eq.EventCrwUnlock.or.
     1        CheckType.eq.EventEdwUnlock) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
3000  if(ich.eq.0) then
        spom=0.
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            if(ModPar.le.7) then
              if(KModA(ModPar,ia).le.0) cycle
              PrvKiMod=max(TRankCumul(itf(ia)),10)+1
              do j=1,ModPar-1
                if(KModA(j,ia).gt.0) then
                    PrvKiMod=PrvKiMod+2*TRank(j-1)*KModA(j,ia)
                  if(j.eq.1) PrvKiMod=PrvKiMod+1
                endif
              enddo
              kmodp=KModA(ModPar,ia)
            else
              PrvKiMod=PrvKiMag(ia)+3
              kmodp=MagPar(ia)-1
            endif
            l=1
            if(ModPar.eq.1) then
              kk=PrvKiMod+l-1
              if(kip(l).lt.100) call kdoco(kk+PrvniKiAtomu(ia)-1,at,pn,
     1                                     -1,p0,spom)
              if(mod(kip(l),100).lt.10) KiA(kk,ia)=mod(kip(l),10)
              l=l+1
            endif
            do j=1,kmodp
              do m=1,Order
                if(ModPar.eq.3) then
                   px(m,j)= px(m,j)*urcp(m,iswa(ia),KPhase)
                  spx(m,j)=spx(m,j)*urcp(m,iswa(ia),KPhase)
                endif
                kk=PrvKiMod+l-1
                if(kip(l).lt.100) call kdoco(kk+PrvniKiAtomu(ia)-1,at,
     1                                       pn,-1,px(m,j),spx(m,j))
                if(mod(kip(l),100).lt.10) KiA(kk,ia)=mod(kip(l),10)
                l=l+1
                k=k+1
              enddo
              do m=1,Order
                if(ModPar.eq.3) then
                   py(m,j)= py(m,j)*urcp(m,iswa(ia),KPhase)
                  spy(m,j)=spy(m,j)*urcp(m,iswa(ia),KPhase)
                endif
                kk=PrvKiMod+l-1
                if(kip(l).lt.100) call kdoco(kk+PrvniKiAtomu(ia)-1,at,
     1                                       pn,-1,py(m,j),spy(m,j))
                if(mod(kip(l),100).lt.10) KiA(kk,ia)=mod(kip(l),10)
                l=l+1
                k=k+1
              enddo
            enddo
          endif
        enddo
      else
        if(lite(KPhase).eq.0.and.ModPar.eq.3) then
          do i=1,NAtActive
            if(LAtActive(i)) then
              ia=IAtActive(i)
              do k=1,lmod
                 do j=1,6
                    bx(j,k,ia)= bx(j,k,ia)*urcp(j,iswa(ia),KPhase)
                    by(j,k,ia)= by(j,k,ia)*urcp(j,iswa(ia),KPhase)
                   sbx(j,k,ia)=sbx(j,k,ia)*urcp(j,iswa(ia),KPhase)
                   sby(j,k,ia)=sby(j,k,ia)*urcp(j,iswa(ia),KPhase)
                 enddo
              enddo
            endif
          enddo
        endif
      endif
      call FeQuestRemove(id)
      deallocate(px,py,spx,spy,kip,pxp,kipp)
      return
      end
      subroutine EM40EditMolModPar(im,ModPar)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(4),xpoma(4)
      real, allocatable :: px(:,:),py(:,:),spx(:,:),spy(:,:),pxp(:)
      character*80 Veta
      character*12 at,pn,pompn
      integer PrvKiMod,Order,PrvPar,PrvParOld,Harm,HarmOld,HarmP,
     1        HarmPOld
      integer, allocatable :: kip(:),kipp(:)
      logical :: lpom,Konec,InSig = .false.
      Konec=.false.
      if(ModPar.eq.1) then
        Order=1
        lmod=KModM(1,im)
      else if(ModPar.le.3) then
        Order=3
        lmod=KModM(2,im)
      else if(ModPar.le.5) then
        Order=6
        lmod=KModM(3,im)
      else if(ModPar.le.6) then
        Order=9
        lmod=KModM(3,im)
      endif
      nki=2*lmod*Order
      if(ModPar.eq.1) nki=nki+1
      allocate(px(Order,lmod),py(Order,lmod),spx(Order,lmod),
     1         spy(Order,lmod),kip(nki),pxp(2*Order+1),kipp(2*Order+1))
      l=lmod*Order
      nki=2*l
      if(ModPar.eq.1) then
        nki=nki+1
        p0=a0m(im)
        sp0=sa0m(im)
        call CopyVek( axm(1,im), px,l)
        call CopyVek( aym(1,im), py,l)
        call CopyVek(saxm(1,im),spx,l)
        call CopyVek(saym(1,im),spy,l)
      else if(ModPar.eq.2) then
        call CopyVek( utx(1,1,im), px,l)
        call CopyVek( uty(1,1,im), py,l)
        call CopyVek(sutx(1,1,im),spx,l)
        call CopyVek(suty(1,1,im),spy,l)
      else if(ModPar.eq.3) then
        call CopyVek( urx(1,1,im), px,l)
        call CopyVek( ury(1,1,im), py,l)
        call CopyVek(surx(1,1,im),spx,l)
        call CopyVek(sury(1,1,im),spy,l)
      else if(ModPar.eq.4) then
        call CopyVek( ttx(1,1,im), px,l)
        call CopyVek( tty(1,1,im), py,l)
        call CopyVek(sttx(1,1,im),spx,l)
        call CopyVek(stty(1,1,im),spy,l)
      else if(ModPar.eq.5) then
        call CopyVek( tlx(1,1,im), px,l)
        call CopyVek( tly(1,1,im), py,l)
        call CopyVek(stlx(1,1,im),spx,l)
        call CopyVek(stly(1,1,im),spy,l)
      else if(ModPar.le.6) then
        call CopyVek( tsx(1,1,im), px,l)
        call CopyVek( tsy(1,1,im), py,l)
        call CopyVek(stsx(1,1,im),spx,l)
        call CopyVek(stsy(1,1,im),spy,l)
      endif
      InSig=.false.
      PrvKiMod=8
      if(ktls((im-1)/mxp+1).gt.0) PrvKiMod=PrvKiMod+21
      if(ModPar.gt.1.and.KModM(1,im).gt.0)
     1  PrvKiMod=PrvKiMod+1+2*KModM(1,im)
      if(ModPar.gt.2) PrvKiMod=PrvKiMod+6*KModM(2,im)
      if(ModPar.gt.3) PrvKiMod=PrvKiMod+6*KModM(2,im)
      if(ModPar.gt.4) PrvKiMod=PrvKiMod+12*KModM(3,im)
      if(ModPar.gt.5) PrvKiMod=PrvKiMod+12*KModM(3,im)
      call CopyVekI(KiMol(PrvKiMod,im),kip,nki)
!      if(ModPar.le.4.or.ModPar.eq.8) then
        nn=2
!      else
!        nn=1
!      endif
      no=nn*Order
      if(ModPar.eq.1) then
        il=3
      else
        il=nn*((Order-1)/3+2)+1
      endif
      if(ModPar.eq.1.or.ModPar.eq.2) then
        if(KFM(2,im).ne.0) then
          pompn='x-slope'
        else
          pompn='xcos33'
        endif
      else
        pompn='U33cos33'
      endif
      xqd=550.+3.*(FeTxLength(pompn)-FeTxLength('F111111cos11'))
      xqdp=xqd*.5
      pom=(xqd-3.*75.-10.)/3.
      pom=(xqd-80.-pom)/2.
      xpoma(3)=xqd-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      pom=FeTxLengthUnder(pompn)+5.
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Edit modulation amplitudes',
     2                   1,LightGray,0,OKForBasicFiles)
      il=1
      at='%Wave'
      xpom=xqdp
      tpom=xpom-FeTxLength(at)-5.
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,at,'L',dpom,EdwYd,1)
      nEdwHarm=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
      call FeQuestEudOpen(EdwLastMade,1,(2*lmod)/nn,1,0.,0.,0.)
      il=il+1
      j=1
      if(ModPar.eq.1) no=no+1
      do i=1,no
        if(i.eq.Order+1.and.ModPar.gt.1) then
          if(j.ne.1) il=il+1
          call FeQuestLinkaMake(id,il)
          il=il+1
          j=1
        endif
        call kdoco(PrvKiMod+PrvniKiMolekuly(im)+i-2,at,pn,1,pom,spom)
        if(ModPar.ne.1.or.i.ne.1) pn=pompn
        call FeQuestLblMake(id,tpoma(j),il,pn,'R','N')
        call FeQuestLblMake(id,xpoma(j)+6.,il,' ','L','N')
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        if(.not.InSig) then
          call FeQuestLblOff(LblLastMade-1)
          call FeQuestLblOff(LblLastMade)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kip(i),
     1                         .false.)
        endif
        if(i.eq.1) then
          nLblFirst=LblLastMade-1
          nEdwPrv=nEdw
          nCrwPrv=nCrw
          PrvPar=i
        else if(i.eq.no) then
          exit
        endif
        if(j.eq.3) then
          j=1
          il=il+1
        else
          j=j+1
        endif
      enddo
      iki=1
      Harm=1
      HarmOld=1
      PrvParOld=PrvPar
      il=il+1
      dpom=80.
      pom=15.
      xpom=(xqd-4.*dpom-2.5*pom)*.5
      Veta='%Refine all'
      do i=1,4
        j=ButtonOff
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
          Veta='%Fix all'
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
          Veta='Re%set'
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
          Veta='Show %p/sig(p)'
        else if(i.eq.4) then
          nButtInSig=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+pom
      enddo
      il=il+1
1400  k=0
      m=0
      idf=nn*(Harm-1)*Order+1-iki
      if(nn.eq.1) then
        HarmP=(Harm-1)/2+1
        HarmPOld=(HarmOld-1)/2+1
        lold=mod(HarmOld-1,2)+1
        l=mod(Harm-1,2)+1
      else
        HarmP=Harm
        HarmPOld=HarmOld
        l=1
        lold=1
      endif
      if(.not.InSig) call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,pxp,
     1                                         kipp,no)
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      nLbl=nLblFirst
      do i=iki,iki+no-1
        if(ModPar.eq.1) then
          if(i.eq.iki) then
            j=1
          else
            j=i
            k=k+1
          endif
        else
          j=i
          k=k+1
        endif
        if(k.gt.Order) then
          k=1
          l=2
          lold=2
        endif
        if(.not.InSig) then
          m=m+1
          kip(j)=kipp(m)
          if(k.eq.0) then
            if(kip(j).lt.100) p0=pxp(m)
          else
            if(lold.eq.1) then
              if(kip(j).lt.100) px(k,HarmPOld)=pxp(m)
            else
              if(kip(j).lt.100) py(k,HarmPOld)=pxp(m)
            endif
          endif
        endif
        if(ModPar.eq.1.and.i.eq.iki) then
          pom=p0
          poms=sp0
        else
          if(l.eq.1) then
            pom=px(k,HarmP)
            poms=spx(k,HarmP)
          else
            pom=py(k,HarmP)
            poms=spy(k,HarmP)
          endif
        endif
        if(InSig) then
          if(poms.gt.0.) then
            pom=abs(pom/poms)
          else
            pom=-1.
          endif
        endif
        if(ModPar.ne.1.or.i.ne.iki) j=j+idf
        call kdoco(PrvKiMod+PrvniKiMolekuly(im)+j-2,at,pn,1,pp,ss)
        if(InSig) then
          if(ModPar.ne.1.or.i.ne.iki) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
            call FeQuestLblChange(nLbl,pn)
            if(pom.lt.0.) then
              Cislo='fixed'
            else if(pom.lt.3.) then
              Cislo='< 3*su'
            else
              write(Cislo,'(f15.1)') pom
              call ZdrcniCisla(Cislo,1)
              Cislo=Cislo(:idel(Cislo))//'*su'
            endif
            call FeQuestLblChange(nLbl+1,Cislo)
          endif
        else
          call FeOpenParEdwCrw(nEdw,nCrw,pn,pom,kip(j),.false.)
        endif
        if(i.eq.iki+no-1.and.
     1     ((ModPar.eq.1.and.KFM(1,im).ne.0.and.
     2       HarmP.eq.KModM(1,im)).or.
     3      (ModPar.eq.2.and.KFM(2,im).ne.0.and.
     4       HarmP.eq.KModM(2,im)))) then
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
1410    nEdw=nEdw+1
        nCrw=nCrw+1
        nLbl=nLbl+2
      enddo
      if(Konec) go to 3000
      if(idf.ne.0) then
        iki=iki+idf
        HarmOld=Harm
      endif
      if(InSig) then
        j=ButtonDisabled
      else
        j=ButtonOff
      endif
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        Konec=.true.
        go to 1400
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwHarm) then
        call FeQuestIntFromEdw(nEdwHarm,Harm)
        if(Harm.ne.HarmOld) then
          go to 1400
        else
          go to 1500
        endif
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,no
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        if(ModPar.eq.1) then
          pom=.01
        else if(ModPar.eq.2) then
          pom=.001
        else if(ModPar.eq.3) then
          pom=.0001
        else
          pom=.00001
        endif
        do i=1,no
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInSig)
     1  then
        if(nn.eq.1) then
          l=mod(Harm-1,2)+1
        else
          l=1
        endif
        m=0
        k=0
        if(InSig) then
          nEdw=nEdwPrv
          nCrw=nCrwPrv
          do i=iki,iki+no-1
            m=m+1
            if(ModPar.eq.1.and.i.eq.iki) then
              go to 1700
            else
              k=k+1
              if(k.gt.Order) then
                k=1
                l=2
              endif
              if(l.eq.1) then
                pom=px(k,HarmP)
              else if(l.eq.2) then
                pom=py(k,HarmP)
              endif
              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
              call FeQuestCrwOpen(nCrw,kipp(m).ne.0)
            endif
1700        nEdw=nEdw+1
            nCrw=nCrw+1
          enddo
        else
          call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,pxp,kipp,no)
          do i=iki,iki+no-1
            m=m+1
            if(ModPar.eq.1.and.i.eq.iki) then
              j=1
              l=0
            else
              j=i
              k=k+1
            endif
            if(k.gt.Order) then
              k=1
              l=2
            endif
            kip(j)=kipp(m)
            if(l.eq.1) then
              px(k,HarmP)=pxp(m)
            else if(l.eq.2) then
              py(k,HarmP)=pxp(m)
            endif
          enddo
        endif
        InSig=.not.InSig
        if(InSig) then
          Veta='%Edit mode'
        else
          Veta='Show %p/sig(p)'
        endif
        call FeQuestButtonLabelChange(nButtInSig,Veta)
        nButt=nButtRefineAll
        go to 1400
      else if(CheckType.eq.EventCrwUnlock.or.
     1        CheckType.eq.EventEdwUnlock) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
3000  if(ich.eq.0) then
        l=1
        if(ModPar.eq.1) then
          kk=PrvKiMod+l-1
          call kdoco(kk+PrvniKiMolekuly(im)-1,at,pn,-1,p0,spom)
          KiMol(kk,im)=kip(l)
          l=l+1
        endif
        do j=1,lmod
          do m=1,Order
            kk=PrvKiMod+l-1
            call kdoco(kk+PrvniKiMolekuly(im)-1,at,pn,-1,px(m,j),
     1                 spx(m,j))
            KiMol(kk,im)=kip(l)
            l=l+1
            k=k+1
          enddo
        enddo
      endif
      call FeQuestRemove(id)
      deallocate(px,py,spx,spy,kip,pxp,kipp)
      return
      end
      subroutine EM40EditMultipole
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension tpoma(4),xpoma(4),PopAsP(68),PopAsPS(68),KiPopAsP(68)
      character*80 Veta
      character*12 at,pn
      integer Order,OrderOld,DelP
      logical :: lpom,InSigOld,InSig = .false.
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(lasmax(ia).eq.llasmaxa) then
            KiPocP=max(TRankCumul(itf(ia)),10)
            if(llasmaxa.lt.2) then
              DelP=3
            else
              DelP=(llasmaxa-1)**2+4
            endif
            KiP=KiPocP
            do j=1,DelP
              call kdoco(KiP+PrvniKiAtomu(ia),at,pn,1,PopAsP(j),
     1                   PopAsPS(j))
              KiP=KiP+1
              KiPopAsP(j)=KiA(KiP,ia)
            enddo
            exit
          endif
        endif
      enddo
      if(NAtActive.gt.0) then
        NAtEdit=0
        do i=1,NAtActive
          if(LAtActive(i)) then
            NAtEdit=NAtEdit+1
            iap=IAtActive(i)
            if(lasmax(iap).lt.3) then
              jmx=3
            else
              jmx=(lasmax(iap)-1)**2+4
            endif
            KiP=max(TRankCumul(itf(iap)),10)
            do j=1,jmx
              call kdoco(KiP+PrvniKiAtomu(iap),at,pn,1,pom,spom)
              KiP=KiP+1
              if(abs(PopAsP(j)-pom).gt..000001.and.KiPopAsP(j).lt.100)
     1          KiPopAsP(j)=KiPopAsP(j)+100
              if(mod(KiPopAsP(j),10).ne.KiA(KiP,iap).and.
     1           mod(KiPopAsP(j),100).lt.10) KiPopAsP(j)=KiPopAsP(j)+10
            enddo
          endif
        enddo
      endif
      id=NextQuestId()
      xqd=600.
      nlasmax=2*llasmaxa-3
      if(nlasmax.gt.0) then
        il=5+(nlasmax-1)/4
      else
        il=1
      endif
      pom=(xqd-4.*75.-10.)/4.
      pom=(xqd-80.-pom)/3.
      xpoma(4)=xqd-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Edit multipole parameters',
     2                   0,LightGray,0,OKForBasicFiles)
      if(llasmaxa.lt.2) then
        ib=3
      else
        ib=4
      endif
      il=1
      KiP=KiPocP
      do i=1,ib
        call kdoco(kip+PrvniKiAtomu(ia),at,pn,1,pom,spom)
        call FeMakeParEdwCrw(id,tpoma(i),xpoma(i),il,pn,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiPopAsP(i),
     1                      .false.)
        if(i.eq.1) then
          nEdwBasic=nEdw
          nCrwBasic=nCrw
        endif
        KiP=KiP+1
      enddo
      if(llasmaxa.ge.2) then
        il=il+1
        call FeQuestLinkaMake(id,il)
        il=il+1
        at='%Order'
        xpom=xqd*.5-7.5
        tpom=xpom-FeTxLength(at)-5.-EdwYd
        dpom=30.
        call FeQuestEudMake(id,tpom,il,xpom,il,at,'L',dpom,EdwYd,1)
        nEdwMultP=EdwLastMade
        call FeQuestIntEdwOpen(EdwLastMade,0,.false.)
        call FeQuestEudOpen(EdwLastMade,0,lasmaxa-3,1,0.,0.,0.)
        ilp=il+1
        do k=1,2
          il=ilp
          j=1
          call kdoco(KiP+PrvniKiAtomu(ia),at,pn,1,pom,spom)
          do i=1,nlasmax
            if(k.eq.1) then
              call FeQuestLblMake(id,tpoma(j),il,pn,'R','N')
              if(i.eq.1) nLblPop=LblLastMade
              call FeQuestLblOff(LblLastMade)
              call FeQuestLblMake(id,xpoma(j)+6.,il,' ','L','N')
              call FeQuestLblOff(LblLastMade)
            else
              call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
              if(i.eq.1) then
                nEdwPop=nEdw
                nCrwPop=nCrw
              endif
            endif
            if(j.eq.4) then
              j=1
              il=il+1
            else
              j=j+1
            endif
          enddo
        enddo
        Order=0
        OrderOld=-1
        NOrder=Order*2+1
        NOrderOld=OrderOld*2+1
        InSigOld=InSig
        il=il+1
        dpom=80.
        pom=15.
        xpom=(xqd-4.*dpom-2.5*pom)*.5
        Veta='%Refine all'
        do i=1,4
          j=ButtonOff
          call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
          if(i.eq.1) then
            nButtRefineAll=ButtonLastMade
            Veta='%Fix all'
          else if(i.eq.2) then
            nButtFixAll=ButtonLastMade
            Veta='Re%set'
          else if(i.eq.3) then
            nButtReset=ButtonLastMade
            Veta='Show %p/sig(p)'
          else if(i.eq.4) then
            nButtInSig=ButtonLastMade
            if(NAtEdit.gt.1) j=ButtonDisabled
          endif
          call FeQuestButtonOpen(ButtonLastMade,j)
          xpom=xpom+dpom+pom
        enddo
      else
        Order=1
        OrderOld=1
        InSigOld=.false.
        NOrder=0
        NOrderOld=0
        nEdwMultP=0
        nEdwPop=0
        nCrwPop=0
        nLblPop=0
        nButtRefineAll=0
        nButtFixAll=0
        nButtReset=0
        nButtInSig=0
      endif
1400  if(llasmaxa.lt.2) go to 1500
      if(OrderOld.ge.0) call FeQuestIntFromEdw(nEdwMultP,Order)
      NOrder=Order*2+1
      if(OrderOld.gt.1.and..not.InSigOld) then
        iskip=OrderOld**2+4
        call FeUpdateParamAndKeys(nEdwPop,nCrwPop,PopAsP(iskip+1),
     1                            KiPopAsP(iskip+1),NOrderOld)
      endif
      nEdw=nEdwPop
      nCrw=nCrwPop
      nLbl=nLblPop
      iskip=Order**2+4
      KiP=KiPocP+iskip
      k=iskip
      if(InSig) then
        do i=1,max(NOrder,NOrderOld)
          if(InSig.neqv.InSigOld) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          if(i.le.NOrder) then
            k=k+1
            call kdoco(kip+PrvniKiAtomu(ia),at,pn,1,pom,spom)
            call FeQuestLblChange(nLbl,pn)
            if(PopAsPS(k).gt.0.) then
              pom=abs(PopAsP(k)/PopAsPS(k))
            else
              pom=-1.
            endif
            if(pom.lt.0.) then
              Cislo='fixed'
            else if(pom.lt.3.) then
              Cislo='< 3*su'
            else
              write(Cislo,'(f15.1)') pom
              call ZdrcniCisla(Cislo,1)
              Cislo=Cislo(:idel(Cislo))//'*su'
            endif
            call FeQuestLblChange(nLbl+1,Cislo)
            KiP=KiP+1
          else
            call FeQuestLblOff(nLbl)
            call FeQuestLblOff(nLbl+1)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonDisabled
      else
        if(Order.ne.OrderOld.or.InSig.neqv.InSigOld) then
          do i=1,max(NOrder,NOrderOld)
            if(InSig.neqv.InSigOld) then
              call FeQuestLblOff(nLbl)
              call FeQuestLblOff(nLbl+1)
            endif
            if(i.le.NOrder) then
              k=k+1
              call kdoco(kip+PrvniKiAtomu(ia),at,pn,1,pom,spom)
              call FeQuestEdwLabelChange(nEdw,pn)
              call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',PopAsP(k),
     1                             KiPopAsP(k),.false.)
              KiP=KiP+1
            else
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            nLbl=nLbl+2
          enddo
        endif
        j=ButtonOff
      endif
      OrderOld=Order
      NOrderOld=NOrder
      InSigOld=InSig
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwMultP) then
        ich=-1
        go to 1600
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPop
        do i=1,NOrder
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPop
        if(Order.eq.0) then
          call FeQuestRealFromEdw(nEdw,pom)
          call FeQuestRealFromEdw(nEdwBasic+1,pomv)
          pom=pom+pomv
          call FeQuestRealEdwOpen(nEdwBasic+1,pom,.false.,.false.)
        endif
        do i=1,NOrder
          call FeQuestRealEdwOpen(nEdw,0.,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInSig)
     1  then
        InSig=.not.InSig
        if(InSig) then
          Veta='%Edit mode'
        else
          Veta='Show %p/sig(p)'
        endif
        call FeQuestButtonLabelChange(nButtInSig,Veta)
        go to 1400
      else if(CheckType.eq.EventEdwUnlock.or.
     1        CheckType.eq.EventCrwUnlock) then
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
1600  if(ich.le.0) then
        call FeUpdateParamAndKeys(nEdwBasic,nCrwBasic,PopAsP,KiPopAsP,
     1                            ib)
        if(nEdwMultP.gt.0) then
          call FeQuestIntFromEdw(nEdwMultP,Order)
          NOrder=Order*2+1
          iskip=OrderOld**2+4
          call FeUpdateParamAndKeys(nEdwPop,nCrwPop,PopAsP(iskip+1),
     1                              KiPopAsP(iskip+1),NOrderOld)
        endif
        do i=1,NAtActive
          if(LAtActive(i)) then
            ia=IAtActive(i)
            if(lasmax(ia).lt.2) then
              DelP=3
            else
              DelP=(lasmax(ia)-1)**2+4
            endif
            KiP=KiPocP
            do j=1,DelP
              if(KiPopAsP(j).lt.100)
     1          call kdoco(KiP+PrvniKiAtomu(ia),at,pn,-1,PopAsP(j),
     2                     PopAsPS(j))
              KiP=KiP+1
              if(mod(KiPopAsP(j),100).lt.10)
     1          KiA(KiP,ia)=mod(KiPopAsP(j),10)
            enddo
          endif
        enddo
        if(ich.lt.0) go to 1400
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine EM40EditADP
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension tpoma(4),xpoma(4),cp(:),cps(:),kip(:)
      character*80 Veta
      character*12 at,pn
      integer Order,OrderOld,PrvKiAnh
      logical :: lpom,InSigOld,InSig = .false.
      allocatable cp,cps,kip
      allocate(cp(74),cps(74),kip(74))
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          if(itf(ia).eq.ltfa) then
            nki=1
            do j=3,ltfa
              n=TRank(j)
              if(j.eq.3) then
                call CopyVek( c3(1,ia),cp (nki),n)
                call CopyVek(sc3(1,ia),cps(nki),n)
              else if(j.eq.4) then
                call CopyVek( c4(1,ia),cp (nki),n)
                call CopyVek(sc4(1,ia),cps(nki),n)
              else if(j.eq.5) then
                call CopyVek( c5(1,ia),cp (nki),n)
                call CopyVek(sc5(1,ia),cps(nki),n)
              else
                call CopyVek( c6(1,ia),cp (nki),n)
                call CopyVek(sc6(1,ia),cps(nki),n)
              endif
              nki=nki+n
            enddo
            nki=nki-1
            PrvKiAnh=11
            call CopyVekI(KiA(PrvKiAnh,ia),kip,nki)
            go to 1200
          endif
        endif
      enddo
1200  if(NAtActive.gt.0) then
        NAtEdit=0
        do i=1,NAtActive
          if(LAtActive(i)) then
            NAtEdit=NAtEdit+1
            iap=IAtActive(i)
            if(itf(iap).gt.2) then
              l=PrvKiAnh
              k=1
              do j=3,itf(iap)
                n=TRank(j)
                do ii=1,n
                  call kdoco(l+PrvniKiAtomu(iap)-1,at,pn,1,pom,spom)
                  if(abs(cp(k)-pom).gt..000001.and.kip(k).lt.100)
     1              kip(k)=kip(k)+100
                  if(mod(kip(k),10).ne.KiA(l,iap).and.
     1               mod(kip(k),100).lt.10) kip(k)=kip(k)+10
                  l=l+1
                  k=k+1
                enddo
              enddo
            endif
          endif
        enddo
      endif
      id=NextQuestId()
      xqd=600.
      NParMax=TRank(ltfa)
      il=3+(nParMax-1)/4
      pom=(xqd-4.*75.-10.)/4.
      pom=(xqd-80.-pom)/3.
      xpoma(4)=xqd-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      call FeQuestCreate(id,-1.,-1.,xqd,il,
     1                   'Edit ADP parameters',0,LightGray,0,
     2                   OKForBasicFiles)
      il=1
      at='%Order'
      xpom=xqd*.5-7.5
      tpom=xpom-FeTxLength(at)-5.-EdwYd
      dpom=30.
      call FeQuestEudMake(id,tpom,il,xpom,il,at,'L',dpom,EdwYd,1)
      nEdwOrder=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,3,.false.)
      call FeQuestEudOpen(EdwLastMade,3,ltfa,1,0.,0.,0.)
      ilp=il+1
      do k=1,2
        il=ilp
        j=1
        call kdoco(PrvKiAnh+PrvniKiAtomu(ia)-1,at,pn,1,pom,spom)
        do i=1,NParMax
          if(k.eq.1) then
            call FeQuestLblMake(id,tpoma(j),il,pn,'R','N')
            if(i.eq.1) nLblPrv=LblLastMade
            call FeQuestLblOff(LblLastMade)
            call FeQuestLblMake(id,xpoma(j)+6.,il,' ','L','N')
            call FeQuestLblOff(LblLastMade)
          else
            call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
            if(i.eq.1) then
              nEdwPrv=nEdw
              nCrwPrv=nCrw
            endif
          endif
          if(j.eq.4.and.i.ne.NParMax) then
            j=1
            il=il+1
          else
            j=j+1
          endif
        enddo
      enddo
      il=il+1
      dpom=80.
      pom=15.
      xpom=(xqd-4.*dpom-2.5*pom)*.5
      Veta='%Refine all'
      do i=1,4
        j=ButtonOff
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
          Veta='%Fix all'
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
          Veta='Re%set'
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
          Veta='Show %p/sig(p)'
        else if(i.eq.4) then
          nButtInSig=ButtonLastMade
          if(NAtEdit.gt.1) j=ButtonDisabled
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        xpom=xpom+dpom+pom
      enddo
      Order=3
      OrderOld=-1
      NOrder=10
      NOrderOld=10
      InSigOld=InSig
1400  if(OrderOld.ge.0) then
        call FeQuestIntFromEdw(nEdwOrder,Order)
        NOrder=TRank(Order)
        k=TRankCumul(OrderOld-1)-9
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,cp(k),kip(k),
     1                            NOrderOld)
      endif
      nEdw=nEdwPrv
      nCrw=nCrwPrv
      nLbl=nLblPrv
      k=TRankCumul(Order-1)-9
      l=PrvKiAnh+PrvniKiAtomu(ia)+k-2
      if(InSig) then
        do i=1,max(NOrder,NOrderOld)
          if(InSig.neqv.InSigOld) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          if(i.le.NOrder) then
            call kdoco(l,at,pn,1,pom,spom)
            call FeQuestLblChange(nLbl,pn)
            if(cps(k).gt.0.) then
              pom=abs(cp(k)/cps(k))
            else
              pom=-1.
            endif
            if(pom.lt.0.) then
              Cislo='fixed'
            else if(pom.lt.3.) then
              Cislo='< 3*su'
            else
              write(Cislo,'(f15.1)') pom
              call ZdrcniCisla(Cislo,1)
              Cislo=Cislo(:idel(Cislo))//'*su'
            endif
            call FeQuestLblChange(nLbl+1,Cislo)
          else
            call FeQuestLblOff(nLbl)
            call FeQuestLblOff(nLbl+1)
          endif
          k=k+1
          l=l+1
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonDisabled
      else
        do i=1,max(NOrder,NOrderOld)
          if(InSig.neqv.InSigOld) then
            call FeQuestLblOff(nLbl)
            call FeQuestLblOff(nLbl+1)
          endif
          if(i.le.NOrder) then
            call kdoco(l,at,pn,1,pom,spom)
            call FeOpenParEdwCrw(nEdw,nCrw,pn,cp(k),kip(k),.false.)
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          k=k+1
          l=l+1
          nEdw=nEdw+1
          nCrw=nCrw+1
          nLbl=nLbl+2
        enddo
        j=ButtonOff
      endif
      OrderOld=Order
      NOrderOld=NOrder
      InSigOld=InSig
      nButt=nButtRefineAll
      do i=1,3
        call FeQuestButtonOpen(nButt,j)
        nButt=nButt+1
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwOrder) then



        go to 1400
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        do i=1,NOrder
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        do i=1,NOrder
          call FeQuestRealEdwOpen(nEdw,0.,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInSig)
     1  then
        InSig=.not.InSig
        if(InSig) then
          Veta='%Edit mode'
        else
          Veta='Show %p/sig(p)'
        endif
        call FeQuestButtonLabelChange(nButtInSig,Veta)
        go to 1400
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwOrder,Order)
        NOrder=TRank(Order)
        k=TRankCumul(OrderOld-1)-9
        call FeUpdateParamAndKeys(nEdwPrv,nCrwPrv,cp(k),kip(k),
     1                            NOrderOld)
        do i=1,NAtActive
          if(.not.LAtActive(i)) cycle
          ia=IAtActive(i)
          if(itf(ia).gt.2) then
            k=PrvKiAnh
            do j=1,TRankCumul(itf(ia))-10
              if(kip(j).lt.100) call kdoco(k+PrvniKiAtomu(ia)-1,at,pn,
     1                                     -1,cp(j),cps(j))
              if(mod(kip(j),100).lt.10) KiA(k,ia)=mod(kip(j),10)
              k=k+1
            enddo
          endif
        enddo
      endif
      call FeQuestRemove(id)
      deallocate(cp,cps,kip)
      return
      end
      subroutine EM40SwitchBetaU(Klic,n)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension poma(6,3)
      if(Klic.eq.0) then
        pom=1./episq
        do isw=1,NComp(KPhase)
          do j=1,6
            poma(j,isw)=1./urcp(j,isw,KPhase)
          enddo
        enddo
      else
        pom=episq
        call CopyVek(urcp(1,1,KPhase),poma,6*NComp(KPhase))
      endif
      if(n.le.0) then
        ip=1
        ik=NAtAll
      else
        ip=NAtIndFrAll(KPhase)+n-1
        ik=ip
      endif
      do i=ip,ik
        if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
        if(kswa(i).ne.KPhase) cycle
        isw=iswa(i)
        if(itf(i).eq.1) then
          beta(1,i)=beta(1,i)*pom
        else
          do j=1,6
            beta(j,i)=beta(j,i)*poma(j,isw)
            do k=1,kmoda(3,i)
              bx(j,k,i)=bx(j,k,i)*poma(j,isw)
              by(j,k,i)=by(j,k,i)*poma(j,isw)
            enddo
          enddo
        endif
      enddo
      return
      end
      subroutine EM40SwitchMag(Key)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do i=1,NAtAll
        if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
        if(kswa(i).ne.KPhase.or.MagPar(i).le.0) cycle
        if(KUsePolar(i).ne.0) cycle
        isw=iswa(i)
        do k=1,3
          if(Key.eq.0) then
            pom=CellPar(k,isw,KPhase)
          else
            pom=1./CellPar(k,isw,KPhase)
          endif
          sm0(k,i)=sm0(k,i)*pom
          do j=1,MagPar(i)-1
            smx(k,j,i)=smx(k,j,i)*pom
            smy(k,j,i)=smy(k,j,i)*pom
          enddo
        enddo
      enddo
      return
      end
      subroutine ZmMult(ia,lasmaxn)
      use Basic_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      call ShiftKiAt(ia,itf(ia),ifr(ia),lasmaxn,KModA(1,ia),
     1               MagPar(ia),.false.)
      kip=max(TRankCumul(itf(ia)),10)+1
      n=nint(AtNum(isf(ia),KPhase))
      if(lasmax(ia).le.0.and.lasmaxn.gt.0) then
        popc(ia)=0.
        do i=1,28
          popc(ia)=popc(ia)+PopCore(i,isf(ia),KPhase)
        enddo
        spopc(ia)=0.
         popv(ia)=float(n)-popc(ia)
        spopv(ia)=0.
         kapa1(ia)=1.
        skapa1(ia)=0.
        KiA(kip  ,ia)=0
        KiA(kip+1,ia)=1
        KiA(kip+2,ia)=1
      endif
      if(lasmax(ia).le.1.and.lasmaxn.gt.1) then
         kapa2(ia)=1.
        skapa2(ia)=0.
        KiA(kip+3,ia)=0
        KiA(kip+4,ia)=0
      endif
      if(lasmaxn.gt.1.and.lasmax(ia).lt.lasmaxn) then
        if(lasmax(ia).gt.1) then
          j=(lasmax(ia)-1)**2+1
        else
          j=2
          popas(1,ia)=0.
        endif
        n=(lasmaxn-1)**2-j+1
        call SetRealArrayTo( popas(j,ia),n,0.)
        call SetRealArrayTo(spopas(j,ia),n,0.)
        kip=kip+3+j
        call SetIntArrayTo(KiA(kip,ia),n,1)
      endif
      lasmax(ia)=lasmaxn
9999  return
      end
      subroutine EM40Molecules(ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(4),xpoma(4),poma(1),kmodamn(3),Ktera(6),Rot(9),
     1          XCentr(3),trp(9),trpi(9),Rotp(9),rpgp(9,48),px(9),py(9),
     2          NAtMult(:)
      character*256 EdwStringQuest
      character*80 Veta
      character*12 men(6),menp(6)
      character*12 at,pn
      character*8 SmbPGMolOld,SmbPGMolIn(:)
      integer PrvKi,FeMenu,pocder,EdwStateQuest,TypeModFunNew
      logical Konec,lpom,CrwLogicQuest,eqrv,EqIgCase
      allocatable NAtMult,SmbPGMolIn
      equivalence (pom,poma)
      data men/'Occupancy','Translation','Rotation','Tensor T',
     1         'Tensor L','Tensor S'/
      if(allocated(SmbPGMolIn)) deallocate(SmbPGMolIn)
      allocate(SmbPGMolIn(NMolec))
      call CopyStringArray(SmbPGMol,SmbPGMolIn,NMolec)
      ich=0
      call CrlAtomNamesIni
      im=NMolecFrAll(KPhase)
      imo=0
      ip=1
      ipo=0
      imp=1
      impo=0
      Konec=.false.
      id=NextQuestId()
      xdq=550.
      xdqp=xdq*.5
      if(NDimI(KPhase).eq.0) then
        il=9
      else
        il=18
      endif
      call FeQuestCreate(id,-1.,-1.,xdq,il,'Molecule edit',0,
     1                   LightGray,0,OKForBasicFiles)
      il=1
      Veta='%Molecule #'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=40.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwMolNo=EdwLastMade
      call FeQuestIntEdwOpen(nEdwMolNo,im-NMolecFrAll(KPhase)+1,.false.)
      call FeQuestEudOpen(nEdwMolNo,1,NMolecLenAll(KPhase),1,0.,0.,0.)
      Veta='%List'
      dpom=FeTxLengthUnder(Veta)+20.
      tpom=xpom+dpom+2.*EdwMarginSize+EdwYd+20.
      call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
      nButtMolList=ButtonLastMade
      if(NMolecLenAll(KPhase).gt.1) then
        i=ButtonOff
      else
        i=ButtonDisabled
      endif
      call FeQuestButtonOpen(ButtonLastMade,i)
      Veta='%Name'
      tpom=tpom+dpom+30.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=FeTxLengthUnder('XXXXXXXX')+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwMolName=EdwLastMade
      Veta='%Position #'
      tpom=xpom+dpom+10.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=35.
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwPosNo=EdwLastMade
      call FeQuestIntEdwOpen(nEdwPosNo,ip,.false.)
      il=il+1
      call FeQuestLblMake(id,30.,il,'Reference point:','L','N')
      tpom=5.
      xpom=tpom+FeTxLength('XXXXXXXXXXXXXX')+5.
      ilp=il
      do i=1,3
        il=il+1
        if(i.eq.1) then
          Veta='%Explicitly'
        else if(i.eq.2) then
          Veta='%Gravity center'
        else
          Veta='Ge%om. center'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                      1,2)
        if(i.eq.1) then
          nCrwCenterFirst=CrwLastMade
          tp=xpom+CrwgXd+5.
          xp=tp+FeTxLength('XX')+5.
          dpom=200.
          call FeQuestEdwMake(id,tp,il,xp,il,' ','L',dpom,EdwYd,1)
          nEdwRef=EdwLastMade
        else if(i.eq.3) then
          nCrwCenterLast=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.1)
      enddo
      il=ilp
      Veta='Point group'
      tpom=.75*xdq
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=tpom-dpom*.5
      call FeQuestEdwMake(id,tpom,il,xpom,il+1,Veta,'C',dpom,EdwYd,1)
      nEdwPG=EdwLastMade
      il=il+2
      Veta='%Hermann-Mauguin short'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=tpom-dpom*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtPGInter=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      Veta='Schoen%flies'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=tpom-dpom*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtPGSchoen=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=ilp+4
      call FeQuestLinkaMake(id,il)
      il=il+1
      Veta='Imp%roper rotation'
      xpom=5.
      tpom=xpom+CrwXd+5.
      ilm=-10*il+2
      call FeQuestCrwMake(id,tpom,ilm,xpom,ilm,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwImprop=CrwLastMade
      tpom=tpom+FeTxLengthUnder(Veta)+100.
      Veta='%Define local coordinate system'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,tpom,ilm,dpom,ButYd,Veta)
      nButtDefLocSyst=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      tpom=tpom+dpom+20.
      Veta='Appl%y site symmetry'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,tpom,ilm,dpom,ButYd,Veta)
      nButtSymmetry=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      pom=(xdq-4.*75.-10.)/4.
      pom=(xdq-80.-pom)/3.
      xpoma(4)=xdq-80.-CrwXd
      do i=3,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,4
        tpoma(i)=xpoma(i)-5.
      enddo
      il=il+1
      j=0
      do i=1,7
        call kdoco(PrvniKiMolekuly(imp)+i-1,at,pn,1,pom,spom)
        k=idel(pn)
        j=j+1
        if(i.eq.5) j=j+1
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiMol(i,imp),
     1                       .false.)
        if(i.eq.1) then
          nEdwPrvPar=nEdw
          nCrwPrvPar=nCrw
        endif
        if(mod(i,4).eq.0) then
          il=il+1
          j=0
        endif
      enddo
      Veta='Edit TLS'
      dpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpoma(1),il,dpom,ButYd,Veta)
      nButtEditTLS=ButtonLastMade
      if(NDimI(KPhase).gt.0) then
        il=il+1
        tpom=5.
        Veta='Modulations:'
        call FeQuestLblMake(id,xdqp,il,Veta,'C','B')
        xpom=tpom+15.+FeTxLengthUnder('%TLS parameters')
        dpom=30.
        xuse=xpom+dpom+2.*EdwMarginSize+EdwYd+50.
        xp=xuse+30.
        tp=xp+CrwXd+5.
        xpp=tp+70.
        tpp=xpp+CrwXd+5.
        do i=1,3
          il=il+1
          if(i.eq.1) then
            Veta='Occ%upancy'
          else if(i.eq.2) then
            Veta='Po%sition'
          else if(i.eq.3) then
            Veta='%TLS parameters'
            ilp=il
          endif
          call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
          call FeQuestIntEdwOpen(EdwLastMade,KModM(i,imp),.false.)
          call FeQuestEudOpen(EdwLastMade,0,mxw,1,0.,0.,0.)
          if(i.eq.1.or.i.eq.2) call FeQuestLblMake(id,xuse,il,'Use:',
     1                                             'L','N')
          if(i.eq.1) then
            nEdwModOcc=EdwLastMade
            call FeQuestCrwMake(id,tp,il,xp,il,'%Crenel','L',CrwXd,
     1                          CrwYd,1,0)
            nCrwCrenel=CrwLastMade
          else if(i.eq.2) then
            nEdwModPos=EdwLastMade
            call FeQuestCrwMake(id,tp,il,xp,il,'Sa%w-tooth','L',
     1                          CrwXd,CrwYd,1,0)
            nCrwSawTooth=CrwLastMade
            call FeQuestCrwMake(id,tpp,il,xpp,il,'%zig-zag','L',
     1                          CrwXd,CrwYd,1,0)
            nCrwZigZag=CrwLastMade
          else if(i.eq.3) then
            nEdwModTLS=EdwLastMade
          endif
        enddo
        il=il+1
        xpom=10.
        Veta='Ed%it modulation parameters'
        dpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtEditMod=ButtonLastMade
        il=ilp
        call FeQuestLblMake(id,xuse,il,'Type of modulation functions:',
     1                      'L','B')
        nLblTypeModFun=LblLastMade
        xpom=xuse
        tpom=xuse+CrwgXd+5.
        ICrwGr=3
        Veta='harmonics (0,1)'
        do i=1,4
          il=il+1
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGr)
          if(i.eq.1) then
            Veta='harmonics (0,1) orthogonalized to '//
     1           'crenel interval'
            nCrwHarm=CrwLastMade
          else if(i.eq.2) then
            Veta='Legendre polynomials in crenel interval'
            nCrwOrtho=CrwLastMade
          else if(i.eq.3) then
            Veta='x-harmonics in crenel interval'
            nCrwLegendre=CrwLastMade
          else if(i.eq.4) then
            nCrwXHarm=CrwLastMade
          endif
        enddo
        il=il+1
        tpom=xpom
        Veta='Selection limit for harmonics:'
        xpom=tpom+FeTxLength(Veta)+10.
        dpom=50.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwHarmLimit=EdwLastMade
      else
        nEdwModOcc=0
        nEdwModPos=0
        nEdwModTLS=0
        nCrwCrenel=0
        nCrwSawTooth=0
        nCrwZigZag=0
        nButtEditMod=0
        nEdwHarmLimit=0
        nCrwHarm=0
        nCrwOrtho=0
        nCrwLegendre=0
        nCrwXHarm=0
      endif
      ipg=LocateInStringArray(SmbPGI,nSmbPG,SmbPGMol(im),.true.)
      if(ipg.le.0)
     1  ipg=LocateInStringArray(SmbPGO,nSmbPG,SmbPGMol(im),.true.)
      if(ipg.le.0) then
        SmbPGMol(im)='1'
        ipg=1
      endif
c      SmbPGMolOld='xyz'
1200  if(im.ne.imo) then
        call FeQuestIntEdwOpen(nEdwMolNo,im-NMolecFrAll(KPhase)+1,
     1                         .false.)
        call FeQuestEudOpen(nEdwPosNo,1,mam(im),1,0.,0.,0.)
        call FeQuestStringEdwOpen(nEdwMolName,MolName(im))
        do i=nCrwCenterFirst,nCrwCenterLast
          lpom=RefPoint(im).eq.i-nCrwCenterFirst
          call FeQuestCrwOpen(i,lpom)
          if(i.eq.nCrwCenterFirst) then
            if(lpom.or.ipg.gt.1) then
              Veta=StRefPoint(im)
              if(Veta.eq.' ') then
                write(Veta,100)(xm(j,im),j=1,3)
                call ZdrcniCisla(Veta,3)
              endif
              call FeQuestStringEdwOpen(nEdwRef,Veta)
            else
              call FeQuestEdwClose(nEdwRef)
            endif
          endif
        enddo
1230    SmbPGMolOld=SmbPGMol(im)
        KPointOld=KPoint(im)
        call FeQuestStringEdwOpen(nEdwPG,SmbPGMol(im))
        if(ktls(im).gt.0) then
          i=ButtonOff
        else
          i=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtEditTLS,i)
      endif
      imp=(im-1)*mxp+ip
      PrvKi=1
      if(imp.ne.impo) then
        nEdw=nEdwPrvPar
        nCrw=nCrwPrvPar
        do i=1,7
          call kdoco(PrvniKiMolekuly(imp)+i-1,at,pn,1,pom,spom)
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          call FeQuestCrwOpen(nCrw,KiMol(i,imp).ne.0)
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
        call FeQuestCrwOpen(nCrwImprop,RotSign(imp).lt.0)
        if(NDimI(KPhase).gt.0) then
          nEdw=nEdwModOcc
          do i=1,3
            kmodamn(i)=KModM(i,imp)
            call FeQuestIntEdwOpen(nEdw,kmodamn(i),.false.)
            nEdw=nEdw+1
          enddo
          call FeQuestCrwOpen(nCrwCrenel,KFM(1,imp).ne.0)
          call FeQuestCrwOpen(nCrwSawTooth,KFM(2,imp).eq.1)
          call FeQuestCrwOpen(nCrwZigZag,KFM(2,imp).eq.2)
        endif
      endif
      imo=im
      ipo=ip
      impo=imp
      isw=iswmol(im)
      if(NDimI(KPhase).gt.0) then
        if(KModM(1,impo).gt.0.or.KModM(2,impo).gt.0.or.
     1     KModM(3,impo).gt.0)
     2    then
          i=ButtonOff
        else
          i=ButtonDisabled
        endif
        call FeQuestButtonOpen(nButtEditMod,i)
        nCrw=nCrwHarm
        if(KFM(1,imp).gt.0) then
          call FeQuestLblOn(nLblTypeModFun)
        else
          call FeQuestLblDisable(nLblTypeModFun)
        endif
        do i=1,4
          call FeQuestCrwOpen(nCrw,i-1.eq.TypeModFunMol(imp))
          if(KFM(1,imp).le.0) call FeQuestCrwDisable(nCrw)
          if(i.eq.2) then
            if(TypeModFunMol(imp).eq.1.and.KFM(1,imp).gt.0) then
              call FeQuestRealEdwOpen(nEdwHarmLimit,OrthoEpsMol(imp),
     1                                .false.,.false.)
            else
              call FeQuestEdwDisable(nEdwHarmLimit)
            endif
          endif
          nCrw=nCrw+1
        enddo
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwMolNo) then
        call FeQuestIntFromEdw(nEdwMolNo,im)
        im=im+NMolecFrAll(KPhase)-1
        if(im.ne.imo) then
          ip=1
          impo=-impo
          call FeQuestIntEdwOpen(nEdwPosNo,ip,.false.)
          go to 2000
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwMolName)
     1  then
        Veta=EdwStringQuest(nEdwMolName)
        call UprAt(Veta)
        call AtCheck(Veta,i,j)
        if(i.ne.0) then
          if(i.eq.1) then
            Veta='Unacceptable symbol in the molecule name.'
          else if(i.eq.2) then
            if(j.ne.-im) then
              Veta='The name already exists.'
            else
              go to 1600
            endif
          endif
          call FeChybne(-1.,-1.,Veta,' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwMolName
          go to 1500
        else
          MolName(im)=Veta
        endif
1600    call FeQuestStringEdwOpen(nEdwMolName,MolName(im))
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwPosNo) then
        call FeQuestIntFromEdw(nEdwPosNo,ip)
        if(ip.ne.ipo) then
          go to 2000
        else
          go to 1500
        endif
      else if(CheckType.eq.EventEdw.and.(CheckNumber.eq.nEdwModOcc.or.
     1        CheckNumber.eq.nEdwModPos.or.CheckNumber.eq.nEdwModTLS))
     2  then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtMolList)
     1  then
        call SelOneAtom('Select next molecule',
     1                  MolName(NMolecFrAll(KPhase)),im,
     2                  NMolecLenAll(KPhase),ich)
        im=im+NMolecFrAll(KPhase)-1
        if(im.ne.imo.and.ich.eq.0) then
          ip=1
          impo=-impo
          go to 2000
        else
          im=imo
          go to 1500
        endif
      else if((CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwCenterFirst
     1                              .and.CheckNumber.le.nCrwCenterLast)
     2         .or.CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwRef)
     3  then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        call CrlSetRotMat(imp)
        ic=CheckType
        if(ic.eq.EventCrw) then
          i=CheckNumber-nCrwCenterFirst
          if(i.ne.RefPoint(im)) RefPoint(im)=i
        endif
        if(EdwStateQuest(nEdwRef).eq.EdwOpened) then
          Veta=EdwStringQuest(nEdwRef)
        else
          Veta=' '
        endif
        call CrlDefMolRefPoint(im,Veta,ich)
        if(ich.gt.0) then
          if(EdwStateQuest(nEdwRef).eq.EdwOpened) then
            EventType=EventEdw
            EventNumber=nEdwRef
            go to 1500
          endif
        else if(ich.eq.0) then
          StRefPoint(im)=Veta
        else if(ich.eq.-1) then
          StRefPoint(im)=' '
        endif
        imo=0
        impo=0
        go to 1200
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtDefLocSyst) then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        call CrlSetRotMat(imp)
        call EM40DefMolLocSyst(imp,ich)
        if(ich.eq.0) then
          call CopyMat(RotMol(1,imp),Rot,3)
          call CopyVek(TrToOrtho(1,isw,KPhase),TrMol(1,imp),9)
          call CopyVek(TrToOrthoI(1,isw,KPhase),TriMol(1,imp),9)
          do l=1,2
            if(l.eq.1) then
              call CopyVek(xm(1,im),XCentr,3)
            else
              call AddVek(XCentr,trans(1,imp),XCentr,3)
            endif
            if(LocMolSystType(imp).ge.l) then
              if(l.eq.1) then
                ii=im
              else
                ii=0
              endif
              Veta=molname(im)(:idel(molname(im)))
              write(Cislo,'(i2)') imp
              call zhusti(Cislo)
              Veta=Veta(:idel(Veta))//'#'//Cislo(:idel(Cislo))
              call CrlMakeTrMatToLocal(XCentr,Veta,LocMolSystAx(l,imp),
     1                    LocMolSystSt(1,l,imp),LocMolSystX(1,1,l,imp),
     2                    ' ',trp,trpi,ii,ich)
              if(ich.ne.0) go to 1800
              if(l.eq.1) then
                call CopyMat(trp,TrMol(1,imp),3)
                call CopyMat(trpi,TriMol(1,imp),3)
              else
                call CopyMat(trpi,TriMol(1,imp),3)
              endif
            endif
          enddo
          call MatInv(TrMol(1,imp),trpi,pom,3)
          call MatInv(TriMol(1,imp),trp,pom,3)
          call multm(rot,trpi,rotp,3,3,3)
          call multm(trp,rotp,rot,3,3,3)
          call EM40GetAngles(rot,irot(KPhase),euler(1,imp))
          do l=1,3
            euler(l,imp)=anint(euler(l,imp)*1000.)*.001
          enddo
          call CrlSetRotMat(imp)
          imo=0
          impo=0
          go to 1200
        else
          go to 1800
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPGInter)
     1  then
        call SelOneAtom('Select point group',SmbPGI,ipg,nSmbPG,ich)
        if(ich.eq.0) then
          SmbPGMol(im)=SmbPGI(ipg)
          call FeQuestStringEdwOpen(nEdwPG,SmbPGMol(im))
          go to 1900
        else
          go to 1800
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtPGSchoen)
     1  then
        call SelOneAtom('Select point group',SmbPGO,ipg,nSmbPG,ich)
        if(ich.eq.0) then
          SmbPGMol(im)=SmbPGO(ipg)
          call FeQuestStringEdwOpen(nEdwPG,SmbPGMol(im))
          go to 1900
        else
          go to 1800
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwPG) then
        Veta=EdwStringQuest(nEdwPG)
        ipg=LocateInStringArray(SmbPGI,nSmbPG,Veta,.true.)
        if(ipg.le.0) then
          ipg=LocateInStringArray(SmbPGO,nSmbPG,Veta,.true.)
          if(ipg.gt.0) Veta=SmbPGO(ipg)
        else
          Veta=SmbPGI(ipg)
        endif
        if(ipg.le.0) then
          call FeChybne(-1.,-1.,'the point group isn''t on the list',
     1                  ' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwPG
          ipg=1
          go to 1500
        else
          SmbPGMol(im)=Veta
          call FeQuestStringEdwOpen(nEdwPG,Veta)
          go to 1900
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwImprop) then
        if(CrwLogicQuest(nCrwImprop)) then
          RotSign(imp)=-1
        else
          RotSign(imp)= 1
        endif
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditTLS)
     1  then
        call EM40EditTLS(imp)
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSymmetry)
     1  then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        neq=0
        neqs=0
        call MolSpec(imo,imo)
        do i=1,neq
          KiMol(lnp(i),imp)=0
          lnp(i)=lnp(i)+pocder(ktatmol(lat(i)))
          do j=1,npa(i)
            pnp(j,i)=pnp(j,i)+pocder(ktatmol(pat(j,i)))
          enddo
        enddo
        call RefAppEq(0,0,0)
        imo=0
        impo=0
        go to 1200
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCrenel) then
        call FeQuestIntFromEdw(nEdwModOcc,n)
        call FeQuestIntFromEdw(nEdwModPos,m)
        if(CrwLogicQuest(nCrwCrenel)) then
          call FeQuestCrwOff(nCrwSawTooth)
          call FeQuestCrwOff(nCrwZigZag)
          if(KFM(2,imp).gt.0) then
            KFM(2,imp)=0
            m=m-1
            call FeQuestIntEdwOpen(nEdwModPos,m,.false.)
          endif
          n=n+1
          KFM(1,imp)=1
        else
          n=n-1
          KFM(1,imp)=0
        endif
        call FeQuestIntEdwOpen(nEdwModOcc,n,.false.)
        call FeQuestEudOpen(nEdwModOcc,0,mxw,1,0.,0.,0.)
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        EventType=EventEdw
        EventNumber=nEdwModOcc
        go to 1200
      else if(CheckType.eq.EventCrw.and.
     1       (CheckNumber.eq.nCrwSawTooth.or.CheckNumber.eq.nCrwZigZag))
     2  then
        call FeQuestIntFromEdw(nEdwModPos,n)
        call FeQuestIntFromEdw(nEdwModOcc,m)
        if(CheckNumber.eq.nCrwSawTooth) then
          call FeQuestCrwOff(nCrwZigZag)
          nCrw=nCrwSawTooth
          i=1
        else
          call FeQuestCrwOff(nCrwSawTooth)
          nCrw=nCrwZigZag
          i=2
        endif
        if(CrwLogicQuest(nCrw)) then
          if(CrwLogicQuest(nCrwCrenel)) then
            call FeQuestCrwOff(nCrwCrenel)
            if(KFM(1,imp).gt.0) then
              KFM(1,imp)=0
              m=m-1
              call FeQuestIntEdwOpen(nEdwModOcc,m,.false.)
            endif
          endif
          if(KFM(2,imp).le.0) n=n+1
          KFM(2,imp)=i
        else
          n=n-1
          KFM(2,imp)=0
        endif
        call FeQuestIntEdwOpen(nEdwModPos,n,.false.)
        call FeQuestEudOpen(nEdwModPos,0,mxw,1,0.,0.,0.)
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        EventType=EventEdw
        EventNumber=nEdwModPos
        go to 1200
      else if(CheckType.eq.EventCrw.and.
     1       (CheckNumber.ge.nCrwHarm.and.CheckNumber.le.nCrwXHarm))
     2  then
        TypeModFunNew=CheckNumber-nCrwHarm
        if(TypeModFunMol(imp).eq.1)
     1    call FeQuestRealFromEdw(nEdwHarmLimit,OrthoEpsMol(imp))
        call EM40ChangeMolTypeMod(imp,TypeModFunNew,OrthoEpsMol(imp))
        nEdw=nEdwPrvPar+1
        do i=1,3
          call FeQuestRealEdwOpen(nEdw,Euler(i,imp),.false.,.false.)
          nEdw=nEdw+1
        enddo
!        nEdw=nEdwPrvPar+4
        do i=1,3
          call FeQuestRealEdwOpen(nEdw,trans(i,imp),.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1200
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtEditMod)
     1  then
        call EM40MolNewBasic(imo,impo,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        j=0
        do i=1,6
          if(i.eq.1) then
            if(KModM(1,impo).le.0) cycle
          else if(i.eq.2.or.i.eq.3) then
            if(KModM(2,impo).le.0) cycle
          else
            if(KModM(3,impo).le.0) cycle
          endif
          j=j+1
          menp(j)=men(i)
          Ktera(j)=i
        enddo
        i=ButtonFr+nButtEditMod-1
        i=FeMenu(ButtonXMax(i),ButtonYMax(i),menp,1,j,1,1)
        if(i.gt.0) call EM40EditMolModPar(impo,Ktera(i))
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      go to 1890
1800  imo=0
      impo=0
      go to 1200
1890  Konec=.true.
      go to 2000
1900  if(.not.EqIgCase(SmbPGMol(im),SmbPGMolOld)) then
        call EM40DefPGSyst(im,ich)
        call CrlMakeTrMatToLocal(xm(1,im),molname(im),
     1                    LocPGSystAx(im),LocPGSystSt(1,im),' ',
     2                    LocPGSystX(1,1,im),
     3                    TrPG(1,im),TriPG(1,im),im,ich)
      endif
2000  if(Konec.and.ich.ne.0) go to 3000
      if(iabs(impo).gt.0) then
        call EM40MolNewBasic(imo,iabs(impo),PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                       kmodamn,nEdwModOcc,nCrwCrenel,nCrwSawTooth,
     2                       nCrwZigZag,nCrwHarm,nEdwHarmLimit)
        if(impo.lt.0) impo=0
      endif
      i=iabs(imo)
      if(i.gt.0.and..not.EqIgCase(SmbPGMol(i),SmbPGMolOld)) then
        KPointOld=KPoint(i)
        call GenPg(SmbPGMol(i),rpoint(1,1,i),npoint(i),ich)
        do l=1,NPoint(i)
          call multm(TriPG(1,i),rpoint(1,l,i),px,3,3,3)
          call multm(px,TrPG(1,i),rpoint(1,l,i),3,3,3)
        enddo
        ji=(i-1)*mxp+1
        call AddVek(xm(1,i),trans(1,ji),px,3)
        call SpecPg(px,rpgp,isw,n)
        call SetIntArrayTo(ipoint(1,i),npoint(i),1)
        nexp=npoint(i)
        do k=2,n
          call multm(rpgp(1,k),RotMol(1,ji),px,3,3,3)
          call multm(RotIMol(1,ji),px,rpgp(1,k),3,3,3)
          do l=1,NPoint(i)
            if(ipoint(l,i).le.0) cycle
            call multm(rpgp(1,k),rpoint(1,l,i),px,3,3,3)
            do m=2,npoint(i)
              if(ipoint(m,i).le.0) cycle
              if(eqrv(px,rpoint(1,m,i),9,.0001)) then
                ipoint(m,i)=0
                nexp=nexp-1
                exit
              endif
            enddo
          enddo
        enddo
        NAtCalcIn=NAtCalc
        n=iam(i)*(nexp-1)
        if(NAtAll+n.ge.MxAtAll) call ReallocateAtoms(n)
        NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+n
        if(i.lt.NMolec) then
          nap=NAtMolFr(1,1)
          do j=1,i
            nap=nap+iam(j)
          enddo
          call AtSun(nap,NAtAll,nap+n)
        else
          nap=NAtAll+1
        endif
c        iam(i)=iam(i)/KPointOld+n
c        iam(i)=iam(i)+n*KPoint(i)
        m=PrvniKiAtomu(nap-1)+DelkaKiAtomu(nap-1)+1
        do k=nap,nap+n-1
          call SetBasicKeysForAtom(k)
          PrvniKiAtomu(k)=m
          DelkaKiAtomu(k)=0
        enddo
        call EM40UpdateAtomLimits
        NAtCalc=NAtCalcIn
      else

      endif
      if(i.gt.0) then
        k=0
        do l=1,npoint(i)
          if(ipoint(l,i).gt.0) k=k+1
          call srotb(rpoint(1,l,i),rpoint(1,l,i),tpoint(1,l,i))
          call multm(rpoint(1,l,i),xm(1,i),spoint(1,l,i),3,3,1)
          do m=1,3
            spoint(m,l,i)=xm(m,i)-spoint(m,l,i)
          enddo
        enddo
        kpoint(i)=k
      endif
      SmbPGMolOld=SmbPGMol(im)
      if(.not.Konec) then
        imo=0
        impo=0
        go to 1200
      endif
3000  call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      do i=1,NMolec
        if(EqIgCase(SmbPGMol(i),SmbPGMolIn(i))) cycle
        nap=NAtMolFr(1,1)
        do j=1,i-1
          nap=nap+iam(j)
        enddo
        nak=nap+iam(i)-1
        Allocate(NAtMult(nak))
        call SetIntArrayTo(NAtMult,nak,1)
        do j=nap,nak-1
          do l=j+1,nak
            if(isf(j).ne.isf(l)) cycle
            do m=1,3
              xpoma(m)=x(m,j)-x(m,l)
            enddo
            call Multm(MetTens(1,1,KPhase),xpoma,tpoma,3,3,1)
            if(sqrt(scalmul(xpoma,tpoma)).lt..1) then
              ai(j)=ai(j)+ai(l)
              ai(l)=0.
              NAtMult(l)=0
            endif
          enddo
        enddo
        do j=nap,nak
          if(NAtMult(j).eq.0) cycle
          do k=2,NPoint(i)
            if(IPoint(k,i).le.0) cycle
            do m=1,3
              px(m)=x(m,j)-xm(m,i)
              py(m)=xm(m,i)
            enddo
            call cultm(rpoint(1,k,i),px,py,3,3,1)
            do l=j,nak
              do m=1,3
                xpoma(m)=py(m)-x(m,l)
              enddo
              call Multm(MetTens(1,1,KPhase),xpoma,tpoma,3,3,1)
              if(sqrt(scalmul(xpoma,tpoma)).lt..1.and.NAtMult(l).ne.0)
     1          then
                if(l.eq.j) then
                  NAtMult(j)=NAtMult(j)+1
                else
                  NAtMult(l)=0
                endif
                exit
              endif
            enddo
          enddo
        enddo
        do j=nap,nak
          if(NAtMult(j).le.0)
     1      call CrlAtomNamesSave(Atom(j),'#delete#',1)
        enddo
        n=nap-1
        nd=0
        do j=nap,nak
          if(NAtMult(j).le.0) then
            call AtSun(n+2,NAtAll,n+1)
            NAtAll=NAtAll-1
            NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)-1
            nd=nd+1
          else
            n=n+1
            ai(n)=ai(n)/float(NAtMult(j))
          endif
        enddo
        if(nd.gt.0) then
          m=(iam(i)-nd)*mam(i)*KPoint(i)-NAtPosLen(isw,KPhase)
          iam(i)=(iam(i)-nd)*KPoint(i)
          if(m.gt.0) call ReallocateAtoms(m)
          call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+m)
          NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+m
          call EM40UpdateAtomLimits
        endif
        deallocate(NAtMult)
        call CrlAtomNamesApply
      enddo
9999  if(allocated(SmbPGMolIn)) deallocate(SmbPGMolIn)
      return
100   format(3f10.6)
      end
      subroutine EM40MolNewBasic(im,imp,PrvKi,nEdwPrvPar,nCrwPrvPar,
     1                           kmodamn,nEdwModOcc,nCrwCrenel,
     2                           nCrwSawTooth,nCrwZigZag,nCrwHarm,
     3                           nEdwHarmLimit)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kmodamn(3)
      integer PrvKi,CrwStateQuest
      logical   CrwLogicQuest
      if(imp.gt.0) then
        nEdw=nEdwPrvPar
        nCrw=nCrwPrvPar
        kip=PrvKi
        call FeUpdateParamAndKeys(nEdw,nCrw,aimol(imp),KiMol(kip,imp),1)
        nEdw=nEdw+1
        nCrw=nCrw+1
        kip=kip+1
        call FeUpdateParamAndKeys(nEdw,nCrw,Euler(1,imp),KiMol(kip,imp),
     1                            3)
        nEdw=nEdw+3
        nCrw=nCrw+3
        kip=kip+3
        call FeUpdateParamAndKeys(nEdw,nCrw,trans(1,imp),KiMol(kip,imp),
     1                            3)
        if(NDimI(KPhase).gt.0) then
          nEdw=nEdwModOcc
          do i=1,3
            call FeQuestIntFromEdw(nEdw,kmodamn(i))
            nEdw=nEdw+1
          enddo
          kfso=KFM(1,imp)
          if(CrwStateQuest(nCrwCrenel).ne.CrwClosed) then
            if(CrwLogicQuest(nCrwCrenel)) then
              kfsn=1
            else
              kfsn=0
            endif
            KFM(1,imp)=min(kfso,kfsn)
          else
            kfsn=kfso
          endif
          kfxo=KFM(2,imp)
          if(CrwStateQuest(nCrwSawTooth).ne.CrwClosed) then
            if(CrwLogicQuest(nCrwSawTooth)) then
              kfxn=1
            else if(CrwLogicQuest(nCrwZigZag)) then
              kfxn=2
            else
              kfxn=0
            endif
            KFM(2,imp)=min(kfxo,kfxn)
          endif
          call MolModi(imp,im,kmodamn)
          nEdw=nEdwPrvPar
          call FeQuestRealEdwOpen(nEdwPrvPar,aimol(imp),.false.,.false.)
          KFM(1,imp)=kfsn
          KFM(2,imp)=kfxn
          if(ktls(im).gt.0) then
            kip=kip+24
          else
            kip=kip+3
          endif
          k=KModM(1,imp)-NDimI(KPhase)
          if(k.ge.0) then
            if(kfsn.ne.kfso.and.kfsn.ne.0) then
              if(NDimI(KPhase).eq.1) then
                pom=0.
              else
                if(a0m(imp).gt.0.) then
                  pom=a0m(imp)**(1./float(NDimI(KPhase)))
                else
                  pom=0.
                endif
              endif
              do i=1,NDimI(KPhase)
                k=k+1
                axm(k,imp)=0.
                aym(k,imp)=pom
                j=kip+(k-1)*2+1
                KiMol(kip,imp)=0
                KiMol(j  ,imp)=0
                KiMol(j+1,imp)=0
              enddo
            endif
          endif
          if(k.gt.0) kip=kip+2*k+1
          i=KModM(2,imp)
          if(i.gt.0) then
            if(kfxn.ne.kfxo.and.kfxn.eq.1) then
              j=kip+(i-1)*6
              call SetRealArrayTo(utx(1,i,imp),3,.001)
              call SetRealArrayTo(uty(1,i,imp),3,0.)
              uty(2,i,imp)=1.
              call SetIntArrayTo(KiMol(j,imp),6,0)
              j=j+i*6
              call SetRealArrayTo(urx(1,i,imp),3,0.)
              call SetRealArrayTo(ury(1,i,imp),3,0.)
              call SetIntArrayTo(KiMol(j,imp),6,0)
            endif
          endif
        endif
      endif
      return
      end
      subroutine EM40ChangeMolTypeMod(imp,TypeModFunNew,OrthoEpsNew)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (NPoints=1001)
      real xpp(6),qcntp(3),TransOld(3),RotVec(3),PomVec(3),RotMat(3,3),
     1     PomMat(3,3)
      real, allocatable :: xp(:),yp(:),OrthoMatNew(:),OrthoMatNewI(:),
     1                                 OrthoMatOld(:),OrthoMatOldI(:),
     2                     FOld(:,:),FNew(:,:),YOld(:),RMat(:),RSide(:),
     3                     RSol(:),Y1Old(:),Y1New(:)
      character*256 Veta
      integer TypeModFunNew
      integer, allocatable :: OrthoSelOld(:),OrthoSelNew(:)
      write(Cislo,'(2i5)') TypeModFunMol(imp),TypeModFunNew
      if(TypeModFunNew.eq.TypeModFunMol(imp).and.TypeModFunNew.ne.1)
     1   go to 9999
      if(KModM(2,imp).le.0.and.KModM(3,imp).le.0) go to 9999
      im=(imp-1)/mxp+1
      m=max(OrthoOrd,2*MaxUsedKwAll+1)
      allocate(xp(m),yp(m),OrthoSelNew(m),OrthoSelOld(m),
     1         OrthoMatNew(m**2),OrthoMatNewI(m**2),
     2         OrthoMatOld(m**2),OrthoMatOldI(m**2),
     3         FOld(m,NPoints),FNew(m,NPoints),YOld(NPoints),
     4         RMat(m*(m+1)),RSide(m),RSol(m),Y1Old(NPoints),
     5         Y1New(NPoints))
      X40=axm(1,imp)
      Delta=a0(imp)
      TransOld=Trans(1:3,imp)
      if(TypeModFunMol(imp).eq.1) then
        write(Veta,'(3f10.6)') OrthoX40Mol(imp),OrthoDeltaMol(imp),
     1                      OrthoEpsMol(imp)
        call UpdateOrtho(imp,OrthoX40Mol(imp),OrthoDeltaMol(imp),
     1                   OrthoEpsMol(imp),OrthoSelOld,OrthoOrd)
        call MatOr(X40,OrthoX40Mol(imp),OrthoSelOld,OrthoOrd,
     1             OrthoMatOld,OrthoMatOldI,imp)
      endif
      if(TypeModFunNew.eq.1) then
        if(OrthoOrd.le.0) OrthoOrd=m
        OrthoX40Mol(imp)=X40
        OrthoDeltaMol(imp)=Delta
        OrthoEpsMol(imp)=OrthoEpsNew
        call UpdateOrtho(imp,X40,Delta,OrthoEpsNew,OrthoSelNew,
     1                   OrthoOrd)
        call MatOr(X40,Delta,OrthoSelNew,OrthoOrd,OrthoMatNew,
     1             OrthoMatNewI,imp)
      endif
      if(KCommen(KPhase).gt.0) then
        call AddVek(xm(1,im),trans(1,imp),xpp,3)
        call qbyx(xpp,qcntp,iswmol(im))
        x4=trez(1,1,KPhase)+qcntp(1)
        n=2*NCommQProduct(iswmol(im),KPhase)
        dx4=1./float(n)
        NPointsUsed=n
      else
        NPointsUsed=NPoints
        x4=X40-Delta*.5
        dx4=Delta/float(NPointsUsed-1)
      endif
      nn=0
      x4p=x4
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        nn=nn+1
        if(TypeModFunMol(imp).eq.0) then
          do k=1,2*MaxUsedKwAll+1
            km=k/2
            if(k-1.eq.0) then
              FOld(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FOld(k,m)=sin(Pi2*km*x4)
            else
              FOld(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFunMol(imp).eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSelOld(k)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMatOld,yp,FOld(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FOld(1,m),2*MaxUsedKwAll+1,
     1                 TypeModFunMol(imp))
        endif
        if(TypeModFunNew.eq.0) then
          do k=1,2*MaxUsedKwAll+1
            km=k/2
            if(k-1.eq.0) then
              FNew(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FNew(k,m)=sin(Pi2*km*x4)
            else
              FNew(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFunNew.eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSelNew(k)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMatNew,yp,FNew(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FNew(1,m),2*MaxUsedKwAll+1,TypeModFunNew)
        endif
        x4=x4p+float(m)*dx4
      enddo
      if(TypeModFunMol(imp).eq.1) then
        nn=min(OrthoOrd,nn)
      else
        nn=min(2*MaxUsedKwAll+1,nn)
      endif
      RMat=0.
      if(KCommen(KPhase).gt.0) then
        x4=trez(1,1,KPhase)+qcntp(1)
      else
        x4=X40-Delta*.5
      endif
      x4p=x4
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        l=0
        do i=1,nn
          do j=1,i
            l=l+1
            RMat(l)=RMat(l)+FNew(i,m)*FNew(j,m)
          enddo
        enddo
        x4=x4p+float(m)*dx4
      enddo
      call smi(RMat,nn,ISing)
      do it=2,3
        kmod=KModM(it,imp)
        if(kmod.le.0) cycle
        if(it.eq.2) then
          irm=2
        else
          irm=3
        endif
        do ir=1,irm
          if(it.eq.2) then
            n=3
          else if(it.eq.3.and.ir.le.2) then
            n=6
          else
            n=9
          endif
          do i=1,n
            xp=0.
            do k=1,2*kmod+1
              km=k/2
              if(k.eq.1) then
!                if(it.eq.2.and.ir.eq.1) xp(k)=trans(i,imp)
              else if(mod(k,2).eq.0) then
                if(it.eq.2) then
                  if(ir.eq.1) then
                    xp(k)=utx(i,km,imp)
                  else
                    xp(k)=urx(i,km,imp)
                  endif
                else if(it.eq.3) then
                  if(ir.eq.1) then
                    xp(k)=ttx(i,km,imp)
                  else if(ir.eq.2) then
                    xp(k)=tlx(i,km,imp)
                  else
                    xp(k)=tsx(i,km,imp)
                  endif
                endif
              else
                if(it.eq.2) then
                  if(ir.eq.1) then
                    xp(k)=uty(i,km,imp)
                  else
                    xp(k)=ury(i,km,imp)
                  endif
                else if(it.eq.3) then
                  if(ir.eq.1) then
                    xp(k)=tty(i,km,imp)
                  else if(ir.eq.2) then
                    xp(k)=tly(i,km,imp)
                  else
                    xp(k)=tsy(i,km,imp)
                  endif
                endif
              endif
            enddo
            RSide=0.
            if(KCommen(KPhase).gt.0) then
              x4=trez(1,1,KPhase)+qcntp(1)
            else
              x4=X40-Delta*.5
            endif
            x4p=x4
            do m=1,NPointsUsed
              pom=x4-X40
              j=pom
              if(pom.lt.0.) j=j-1
              pom=pom-float(j)
              if(pom.gt..5) pom=pom-1.
              pom=2.*pom/Delta
              if(pom.lt.-1.01.or.pom.gt.1.01) then
                x4=x4p+float(m)*dx4
                cycle
              endif
              call MultM(FOld(1,m),xp,YOld(m),1,nn,1)
              if(it.eq.2.and.i.eq.1) Y1Old(m)=YOld(m)
              do j=1,nn
                RSide(j)=RSide(j)+YOld(m)*FNew(j,m)
              enddo
              x4=x4p+float(m)*dx4
            enddo
            call nasob(RMat,RSide,RSol,nn)
            do k=1,2*kmod+1
              km=k/2
              if(k.eq.1) then
                if(it.eq.2.and.ir.eq.1) then
                  trans(i,imp)=RSol(k)+TransOld(i)
                else if(it.eq.2.and.ir.eq.2) then
                  RotVec(i)=RSol(k)
                endif
              else if(mod(k,2).eq.0) then
                if(it.eq.2) then
                  if(ir.eq.1) then
                    utx(i,km,imp)=RSol(k)
                  else
                    urx(i,km,imp)=RSol(k)
                  endif
                else if(it.eq.3) then
                  if(ir.eq.1) then
                    ttx(i,km,imp)=RSol(k)
                  else if(ir.eq.2) then
                    tlx(i,km,imp)=RSol(k)
                  else
                    tsx(i,km,imp)=RSol(k)
                  endif
                endif
              else
                if(it.eq.2) then
                  if(ir.eq.1) then
                    uty(i,km,imp)=RSol(k)
                  else
                    ury(i,km,imp)=RSol(k)
                  endif
                else if(it.eq.3) then
                  if(ir.eq.1) then
                    tty(i,km,imp)=RSol(k)
                  else if(ir.eq.2) then
                    tly(i,km,imp)=RSol(k)
                  else
                    tsy(i,km,imp)=RSol(k)
                  endif
                endif
              endif
            enddo
          enddo
        enddo
      enddo
      PomMat=0.
      RotVec(1:3)=RotVec(1:3)*CellVol(iswmol(im),KPhase)
      PomMat(1,2)=-RotVec(3)
      PomMat(1,3)= RotVec(2)
      PomMat(2,1)= RotVec(3)
      PomMat(2,3)=-RotVec(1)
      PomMat(3,1)=-RotVec(2)
      PomMat(3,2)= RotVec(1)
      call Multm(MetTensI(1,iswmol(im),KPhase),PomMat,PomMat,3,3,3)
      PomMat(1,1)=1.+PomMat(1,1)
      PomMat(2,2)=1.+PomMat(2,2)
      PomMat(3,3)=1.+PomMat(3,3)
      call MultM(RotMol(1,imp),PomMat,RotMat,3,3,3)
      call MultM(TrMol(1,imp),RotMat,PomMat,3,3,3)
      call MultM(PomMat,TriMol(1,imp),RotMat,3,3,3)
      RotVec(1:3)=RotMat(1,1:3)
      call VecOrtNorm(RotVec,3)
      RotMat(1,1:3)=RotVec(1:3)
      PomVec(1:3)=RotMat(1,1:3)
      RotVec(1:3)=RotMat(2,1:3)
      pom=-VecOrtScal(RotVec,PomVec,3)
      do i=1,3
        RotVec(i)=pom*PomVec(i)+RotVec(i)
      enddo
      call VecOrtNorm(RotVec,3)
      RotMat(2,1:3)=RotVec(1:3)
      PomVec(1:3)=RotMat(1,1:3)
      RotVec(1:3)=RotMat(2,1:3)
      call VecMul(PomVec,RotVec,xpp)
      call VecOrtNorm(xpp,3)
      RotMat(3,1:3)=xpp(1:3)
      call MatInv(RotMat,PomMat,pom,3)
      call EM40GetAngles(RotMat,irot(KPhase),euler(1,imp))
      do i=1,3
        xpp(i)=trans(i,imp)-TransOld(i)
      enddo
      call qbyx(xpp,qcntp,iswmol(im))
      axm(1,imp)=X40+qcntp(1)
      TypeModFunMol(imp)=TypeModFunNew
      deallocate(xp,yp,OrthoSelNew,OrthoSelOld,OrthoMatNew,OrthoMatNewI,
     1           OrthoMatOld,OrthoMatOldI,FOld,FNew,YOld,RMat,RSide,
     2           RSol,Y1Old,Y1New)
9999  return
      end
      subroutine EM40EditTLS(imp)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension tpoma(3),xpoma(3)
      character*12 at,pn
      integer PrvKi
      logical lpom
      xdq=400.
      il=8
      pom=(xdq-3.*75.-10.)/3.
      pom=(xdq-80.-pom)/2.
      xpoma(3)=xdq-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xdq,il,'Edit TLS tensors',0,
     1                   LightGray,0,OKForBasicFiles)
      il=1
      j=1
      PrvKi=8
      kip=PrvKi
      do i=1,21
        call kdoco(kip+PrvniKiMolekuly(imp)-1,at,pn,1,pom,spom)
        call FeMakeParEdwCrw(id,tpoma(j),xpoma(j),il,pn,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,KiMol(kip,imp),
     1                       .false.)
        if(i.eq.1) then
          nEdwPrv=nEdw
          nCrwPrv=nCrw
        endif
        if(mod(j,3).eq.0) then
          j=1
          il=il+1
        else
          j=j+1
        endif
        kip=kip+1
      enddo
      dpom=60.
      pom=20.
      xpom=(xdq-3.*dpom-2.*pom)*.5
      do i=1,3
        if(i.eq.1) then
          at='%Refine all'
        else if(i.eq.2) then
          at='%Fix all'
        else if(i.eq.3) then
          at='Re%set'
        endif
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,at)
        if(i.eq.1) then
          nButtRefineAll=ButtonLastMade
        else if(i.eq.2) then
          nButtFixAll=ButtonLastMade
        else if(i.eq.3) then
          nButtReset=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        xpom=xpom+dpom+pom
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtRefineAll.or.
     2         CheckNumber.eq.nButtFixAll)) then
        if(CheckNumber.eq.nButtRefineAll) then
          lpom=.true.
        else
          lpom=.false.
        endif
        nCrw=nCrwPrv
        kip=PrvKi
        do i=1,21
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
          kip=kip+1
        enddo
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtReset)
     1  then
        nEdw=nEdwPrv
        pom=.001
        do i=1,21
          if(i.eq.4) pom=0.
          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
          nEdw=nEdw+1
        enddo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwPrv
        nCrw=nCrwPrv
        kip=PrvKi
        call FeUpdateParamAndKeys(nEdw,nCrw,tT(1,imp),KiMol(kip,imp),6)
        nEdw=nEdw+6
        nCrw=nCrw+6
        kip=kip+6
        call FeUpdateParamAndKeys(nEdw,nCrw,tL(1,imp),KiMol(kip,imp),6)
        nEdw=nEdw+6
        nCrw=nCrw+6
        kip=kip+6
        call FeUpdateParamAndKeys(nEdw,nCrw,tS(1,imp),KiMol(kip,imp),9)
      endif
      call FeQuestRemove(id)
      return
      end
      subroutine EM40DefPGSyst(imol,ich)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*80 Veta,ErrSt
      character*27 LocPGSystStO(2)
      character*2 LocPGSystAxO
      integer RolMenuSelectedQuest
      logical CrwLogicQuest
      dimension nRolMenuSystAx(2),nEdwSystSt(2),px(3)
      do j=1,2
        LocPGSystAxO(j:j)=LocPGSystAx(imol)(j:j)
        LocPGSystStO(j)=LocPGSystSt(j,imol)
      enddo
      id=NextQuestId()
      xdq=300.
      xdqp=xdq*.5
      il=2
      call FeQuestCreate(id,-1.,-1.,xdq,il,'Define PG coordinate'//
     1                   ' system',0,LightGray,0,OKForBasicFiles)
      il=1
      Veta='%Use local system'
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwLocSyst=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,UsePGSyst(imol))
      xpom=xpom+CrwXd+15.
      dpom=20.+EdwYd
      xpp=xpom+dpom+15.
      dpp=xdq-xpp-5.
      do j=1,2
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,' ','L',dpom,EdwYd,1)
        nRolMenuSystAx(j)=RolMenuLastMade
        call FeQuestEdwMake(id,tpom,il,xpp,il,' ','L',dpp,EdwYd,1)
        nEdwSystSt(j)=EdwLastMade
        il=il+1
      enddo
1400  if(UsePGSyst(imol)) then
        do i=1,2
          j=LocateInStringArray(SmbX,3,LocPGSystAx(imol)(i:i),.true.)
          call FeQuestRolMenuOpen(nRolMenuSystAx(i),SmbX,3,j)
          Veta=LocPGSystSt(i,imol)
          if(Veta(1:1).eq.' ') Veta=Veta(2:)
          k=0
          call StToReal(Veta,k,px,3,.false.,ich)
          if(ich.eq.0) then
            write(Veta,'(3f12.6)') px
            call ZdrcniCisla(Veta,3)
          endif
          call FeQuestStringEdwOpen(nEdwSystSt(i),Veta)
        enddo
      else
        do i=1,2
          call FeQuestRolMenuClose(nRolMenuSystAx(i))
          call FeQuestEdwClose(nEdwSystSt(i))
        enddo
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwLocSyst) then
        UsePGSyst(imol)=CrwLogicQuest(nCrwLocSyst)
        go to 1400
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.eq.nRolMenuSystAx(1).or.
     2         CheckNumber.eq.nRolMenuSystAx(2))) then
        if(CheckNumber.eq.nRolMenuSystAx(1)) then
          j=1
        else
          j=2
        endif
        k=RolMenuSelectedQuest(CheckNumber)
        LocPGSystAx(imol)(j:j)=Smbx(k)
        EventType=EventEdw
        EventNumber=nEdwSystSt(j)
        go to 1500
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwSystSt(1).or.
     2         CheckNumber.eq.nEdwSystSt(2))) then
        Veta=EdwStringQuest(CheckNumber)
        call CrlGetXFromAtString(Veta,imol,px,ErrSt,ich)
        if(ich.gt.0) then
          call FeChybne(-1.,-1.,'in the definition of local '//
     1                  'coordinate system.',ErrSt,SeriousError)
          EventType=EventEdw
          EventNumber=CheckNumber
        else
          ich=0
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(UsePGSyst(imol)) then
          do i=1,2
            Veta=EdwStringQuest(nEdwSystSt(i))
            if(Veta(1:1).ne.'-') Veta=' '//Veta(:idel(Veta))
            LocPGSystSt(i,imol)=Veta
          enddo
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        do j=1,2
          LocPGSystAx(imol)(j:j)=LocPGSystAxO(j:j)
          LocPGSystSt(j,imol)=LocPGSystStO(j)
        enddo
      endif
9999  return
      end
      subroutine EM40DefMolLocSyst(imp,ich)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*80 Veta,ErrSt
      character*27 LocMolSystStO(2,2)
      character*2 LocMolSystAxO(2)
      integer RolMenuSelectedQuest,CrwStateQuest
      logical CrwLogicQuest
      real LocMolSystXO(3,2,2)
      dimension nRolMenuModelSystAx (2),nEdwModelSystSt (2),
     1          nRolMenuActualSystAx(2),nEdwActualSystSt(2),px(3)
      LocMolSystTypeO=LocMolSystType(imp)
      do i=1,2
        do j=1,2
          LocMolSystAxO(i)(j:j)=LocMolSystAx(i,imp)(j:j)
          LocMolSystStO(j,i)=LocMolSystSt(j,i,imp)
          call CopyVek(LocMolSystX(1,j,i,imp),LocMolSystXO(1,j,i),3)
        enddo
      enddo
      id=NextQuestId()
      xdq=300.
      xdqp=xdq*.5
      il=6
      call FeQuestCreate(id,-1.,-1.,xdq,il,'Define local coordinate'//
     1                   ' system',0,LightGray,0,OKForBasicFiles)
      il=0
      do i=1,2
        il=il+1
        if(i.eq.1) then
          Veta='%Model  molecule'
        else
          Veta='%Actual molecule'
        endif
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        if(i.eq.1) then
          nCrwLocForModel=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,LocMolSystType(imp).gt.0)
        else
          nCrwLocForActual=CrwLastMade
        endif
        xpom=xpom+CrwXd+15.
        dpom=20.+EdwYd
        xpp=xpom+dpom+15.
        dpp=xdq-xpp-5.
        do j=1,2
          call FeQuestRolMenuMake(id,tpom,il,xpom,il,' ','L',dpom,EdwYd,
     1                            1)
          if(i.eq.1) then
            nRolMenuModelSystAx(j)=RolMenuLastMade
          else
            nRolMenuActualSystAx(j)=RolMenuLastMade
          endif
          call FeQuestEdwMake(id,tpom,il,xpp,il,' ','L',dpp,EdwYd,1)
          if(i.eq.1) then
            nEdwModelSystSt(j)=EdwLastMade
          else
            nEdwActualSystSt(j)=EdwLastMade
          endif
          il=il+1
        enddo
      enddo
1400  if(LocMolSystType(imp).gt.0) then
        if(CrwStateQuest(nCrwLocForActual).eq.CrwClosed) then
          call FeQuestCrwOpen(nCrwLocForActual,LocMolSystType(imp).gt.1)
          do i=1,2
            j=LocateInStringArray(SmbX,3,LocMolSystAx(1,imp)(i:i),
     1                            .true.)
            call FeQuestRolMenuOpen(nRolMenuModelSystAx(i),SmbX,3,j)
            Veta=LocMolSystSt(i,1,imp)
            if(Veta(1:1).eq.' ') Veta=Veta(2:)
            k=0
            call StToReal(Veta,k,px,3,.false.,ich)
            if(ich.eq.0) then
              write(Veta,'(3f12.6)') px
              call ZdrcniCisla(Veta,3)
            endif
            call FeQuestStringEdwOpen(nEdwModelSystSt(i),Veta)
          enddo
        endif
      else
        call FeQuestCrwClose(nCrwLocForActual)
        do i=1,2
          call FeQuestRolMenuClose(nRolMenuModelSystAx(i))
          call FeQuestEdwClose(nEdwModelSystSt(i))
        enddo
      endif
      if(LocMolSystType(imp).gt.1) then
        do i=1,2
          j=LocateInStringArray(SmbX,3,LocMolSystAx(2,imp)(i:i),.true.)
          call FeQuestRolMenuOpen(nRolMenuActualSystAx(i),SmbX,3,j)
          Veta=LocMolSystSt(i,2,imp)
          if(Veta(1:1).eq.' ') Veta=Veta(2:)
          k=0
          call StToReal(Veta,k,px,3,.false.,ich)
          if(ich.eq.0) then
            write(Veta,'(3f12.6)') px
            call ZdrcniCisla(Veta,3)
          endif
          call FeQuestStringEdwOpen(nEdwActualSystSt(i),Veta)
        enddo
      else
        do i=1,2
          call FeQuestRolMenuClose(nRolMenuActualSystAx(i))
          call FeQuestEdwClose(nEdwActualSystSt(i))
        enddo
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwLocForModel) then
        if(CrwLogicQuest(nCrwLocForModel)) then
          LocMolSystType(imp)=max(LocMolSystType(imp),1)
        else
          LocMolSystType(imp)=min(LocMolSystType(imp),0)
        endif
        go to 1400
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.eq.nCrwLocForActual) then
        if(CrwLogicQuest(nCrwLocForActual)) then
          LocMolSystType(imp)=2
        else
          LocMolSystType(imp)=1
        endif
        go to 1400
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.eq.nRolMenuModelSystAx(1).or.
     2         CheckNumber.eq.nRolMenuModelSystAx(2))) then
        if(CheckNumber.eq.nRolMenuModelSystAx(1)) then
          j=1
        else
          j=2
        endif
        k=RolMenuSelectedQuest(CheckNumber)
        LocMolSystAx(1,imp)(j:j)=Smbx(k)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        (CheckNumber.eq.nRolMenuActualSystAx(1).or.
     2         CheckNumber.eq.nRolMenuActualSystAx(2))) then
        if(CheckNumber.eq.nRolMenuActualSystAx(1)) then
          j=1
        else
          j=2
        endif
        k=RolMenuSelectedQuest(CheckNumber)
        LocMolSystAx(2,imp)(j:j)=Smbx(k)
        go to 1500
      else if(CheckType.eq.EventEdw.and.
     1        (CheckNumber.eq.nEdwModelSystSt(1).or.
     2         CheckNumber.eq.nEdwModelSystSt(2).or.
     1         CheckNumber.eq.nEdwActualSystSt(1).or.
     2         CheckNumber.eq.nEdwActualSystSt(2))) then
        if(CheckNumber.eq.nEdwModelSystSt(1).or.
     1     CheckNumber.eq.nEdwModelSystSt(2)) then
          imol=(imp-1)/mxp+1
        else
          imol=0
        endif
        Veta=EdwStringQuest(CheckNumber)
        call CrlGetXFromAtString(Veta,imol,px,ErrSt,ich)
        if(ich.gt.0) then
          call FeChybne(-1.,-1.,'in the definition of local '//
     1                  'coordinate system.',ErrSt,SeriousError)
          EventType=EventEdw
          EventNumber=CheckNumber
        else
          ich=0
        endif
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        if(LocMolSystType(imp).gt.0) then
          do i=1,2
            Veta=EdwStringQuest(nEdwModelSystSt(i))
            if(Veta(1:1).ne.'-') Veta=' '//Veta(:idel(Veta))
            LocMolSystSt(i,1,imp)=Veta
          enddo
        endif
        if(LocMolSystType(imp).gt.1) then
          do i=1,2
            Veta=EdwStringQuest(nEdwActualSystSt(i))
            if(Veta(1:1).ne.'-') Veta=' '//Veta(:idel(Veta))
            LocMolSystSt(i,2,imp)=Veta
          enddo
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        LocMolSystType(imp)=LocMolSystTypeO
        do i=1,2
          do j=1,2
            LocMolSystAx(i,imp)(j:j)=LocMolSystAxO(i)(j:j)
            LocMolSystSt(j,i,imp)=LocMolSystStO(j,i)
            call CopyVek(LocMolSystXO(1,j,i),LocMolSystX(1,j,i,imp),3)
          enddo
        enddo
      endif
9999  return
      end
      subroutine EM40RecalcOrtho(x40,delta,x,ux,uy,n,kmod,kmodo,kf,
     1                           iseln,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iseln(*),x(n),ux(n,*),uy(n,*),der(mxw21),
     1          am((mxw21*(mxw21+1))/2),yc(36),ps(mxw21,36),sol(mxw21)
      ich=0
      kmodp=kmod
      if(kf.ne.0) kmodp=kmodp-1
      nw=2*kmodp+1
      kmodop=kmodo
      if(kf.ne.0) kmodop=kmodop-1
      nwo=2*kmodop+1
      k=0
      do i=1,mxw21
        if(iseln(i).ne.0) then
          k=k+1
          if(k.ge.nwo) then
            nwn=i
            go to 1050
          endif
        endif
      enddo
      ich=1
      go to 9999
1050  kmodnp=nwn/2
      kmod=kmodnp
      if(kf.ne.0) then
        kmod=kmod+1
        call CopyVek(ux(1,kmodp+1),ux(1,kmod),n)
        call CopyVek(uy(1,kmodp+1),uy(1,kmod),n)
      endif
      call SetRealArrayTo(sol,mxw21,0.)
      call SetRealArrayTo(am,(nwo*(nwo+1))/2,0.)
      call SetRealArrayTo(ps,36*mxw21,0.)
      x4=x40-delta*.5
      dx4=.01*delta
      do i=1,100
        k=0
        do j=1,nwn
          if(j.gt.1) arg=pi2*float(kw(1,j/2,KPhase))*x4
          if(iseln(j).ne.0) then
            k=k+1
            if(j.eq.1) then
              der(k)=1.
            else if(mod(j,2).eq.0) then
              der(k)=sin(arg)
            else
              der(k)=cos(arg)
            endif
          endif
        enddo
        call CopyVek(x,yc,n)
        do j=1,kmodp
          arg=pi2*float(kw(1,j,KPhase))*x4
          sn=sin(arg)
          cs=cos(arg)
          do k=1,n
            yc(k)=yc(k)+ux(k,j)*sn+uy(k,j)*cs
          enddo
        enddo
        m=0
        do j=1,nwo
          derj=der(j)
          do k=1,j
            m=m+1
            am(m)=am(m)+derj*der(k)
          enddo
          do k=1,n
            ps(j,k)=ps(j,k)+derj*yc(k)
          enddo
        enddo
        x4=x4+dx4
      enddo
      ij=0
      do i=1,nwo
        ij=ij+i
        der(i)=1./sqrt(am(ij))
      enddo
      call znorm(am,der,nwo)
      call smi(am,nwo,ising)
      if(ising.gt.0) go to 9999
      call znorm(am,der,nwo)
      do l=1,n
        call nasob(am,ps(1,l),sol,nwo)
        k=0
        do j=1,nwn
          iw=j/2
          if(iseln(j).eq.0) then
            pom=0.
          else
            k=k+1
            pom=sol(k)
          endif
          if(j.eq.1) then
            x(l)=pom
          else if(mod(j,2).eq.0) then
            ux(l,iw)=pom
          else
            uy(l,iw)=pom
          endif
        enddo
      enddo
9999  return
      end
      subroutine EM40NewAt(ich)
      use Basic_mod
      use Editm40_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension xp(3),xo(3)
      character*80 Veta
      character*8  At
      integer SbwItemPointerQuest,UseTabsIn,RadiationUsed
      logical FeYesNo,CrwLogicQuest,PointAlreadyPresent
      external EM40NewAtCheck,FeVoid
      data biso,dmez,dmin/2*3.,.5/
      Klic=0
      go to 1050
      entry EM40NewAtFromFourier(ich)
      Klic=1
1050  if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      pom=FeTxLengthSpace('XX.XXXXXX ')
      xpom=2.*PropFontWidthInPixels
      do i=1,3
        call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
        xpom=xpom+pom
      enddo
      xpom=xpom+20.
      call FeTabsAdd(xpom,UseTabs,IdCharTab,'.')
      xpom=xpom+50.
      call FeTabsAdd(xpom,UseTabs,IdRightTab,' ')
      ich=0
      iz=0
      l48=.false.
      if(WizardMode) go to 1210
      call OpenFile(m40,fln(:ifln)//'.m40','formatted','old')
      if(ErrFlag.ne.0) go to 1150
1100  read(m40,FormA80,end=1110) Veta
      if(Veta(1:10).ne.'----------'.or.
     1  LocateSubstring(Veta,'Fourier maxima',.false.,.true.).le.0)
     2  go to 1100
      read(m40,102,end=1110) iswn,KPhasePeaks,RadiationUsed
      l48=.true.
      go to 1150
1110  rewind m40
1120  read(m40,FormA80,end=1150) Veta
      if(Veta(1:10).ne.'----------'.or.
     1  LocateSubstring(Veta,'Fourier minima',.false.,.true.).le.0)
     2  go to 1120
      read(m40,102,end=1150) iswn,KPhasePeaks,RadiationUsed
      l48=RadiationUsed.eq.NeutronRadiation
1150  if(l48) then
        if(Klic.eq.0) then
          il=6
        else
          il=4
        endif
      else
        il=1
        call CloseIfOpened(m40)
      endif
      if(NComp(KPhase).gt.1) il=il+1
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Inserting/replacing of '//
     1                   'atoms',0,LightGray,0,0)
      il=0
      if(l48.and.Klic.eq.0) then
        xpom=5.
        tpom=xpom+CrwgXd+10.
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                      'Peaks from the last Fourier calculation',
     2                      'L',CrwgXd,CrwgYd,1,1)
        nCrwFromFourier=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.true.)
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,'Coordinates from '//
     1                      '%keyboard','L',CrwgXd,CrwgYd,1,1)
        nCrwFromKeyboard=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.false.)
        il=il+1
        call FeQuestLinkaMake(id,il)
      else
        nCrwFromFourier=0
        nCrwFromKeyboard=0
      endif
      tpom=5.
      dpom=50.
      if(l48) then
        xpom=5.
        tpom=xpom+CrwgXd+3.
        il=il+1
        Veta='Ski%p peaks being too close to existing atoms'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwSkipShort=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.false.)
        tpom=5.
        il=il+1
        Veta='M%inimal distance'
        xpom=tpom+FeTxLengthUnder(Veta)+43.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwSkipShort=EdwLastMade
        call FeQuestLblMake(id,xpom+dpom+10.,il,'Angs.','L','N')
        nLblSkipShort=LblLastMade
        call FeQuestLblOff(LblLastMade)
      else
        nCrwSkipShort=0
        nEdwSkipShort=0
        nLblSkipShort=0
      endif
      il=il+1
      Veta='%Show distances up to'
      if(.not.l48) xpom=tpom+FeTxLengthUnder(Veta)+10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwMaxDistance=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,dmez,.false.,.false.)
      call FeQuestLblMake(id,xpom+dpom+10.,il,'Angs.','L','N')
      if(NComp(KPhase).gt.1) then
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,'%Composite part','L',
     1                      dpom,EdwYd,0)
        nEdwIsw=EdwLastMade
        if(.not.l48) call FeQuestIntEdwOpen(EdwLastMade,1,.false.)
      else
        nEdwIsw=0
        iswn=1
      endif
1200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwFromFourier.or.
     2    CheckNumber.eq.nCrwFromKeyboard)) then
        if(CheckNumber.eq.nCrwFromFourier) then
          if(nEdwIsw.gt.0) call FeQuestEdwClose(nEdwIsw)
          call FeQuestCrwOpen(nCrwSkipShort,.false.)
        else
          if(nEdwIsw.gt.0) call FeQuestIntEdwOpen(nEdwIsw,1,.false.)
          call FeQuestCrwClose(nCrwSkipShort)
          call FeQuestEdwClose(nEdwSkipShort)
          call FeQuestLblOff(nLblSkipShort)
        endif
        go to 1200
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwSkipShort)
     1  then
        if(CrwLogicQuest(CheckNumber)) then
          call FeQuestRealEdwOpen(nEdwSkipShort,dmin,.false.,.false.)
          call FeQuestLblOn(nLblSkipShort)
        else
          call FeQuestEdwClose(nEdwSkipShort)
          call FeQuestLblOff(nLblSkipShort)
        endif
        go to 1200
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1200
      endif
      if(ich.eq.0) then
        if(l48.and.Klic.eq.0) l48=CrwLogicQuest(nCrwFromFourier)
        call FeQuestRealFromEdw(nEdwMaxDistance,dmez)
        if(.not.l48.and.NComp(KPhase).gt.1)
     1    call FeQuestIntFromEdw(nEdwIsw,iswn)
        if(nCrwSkipShort.gt.0) then
          if(CrwLogicQuest(nCrwSkipShort)) then
            call FeQuestRealFromEdw(nEdwSkipShort,DSkip)
            dmin=DSkip
          else
            DSkip=-1.
          endif
        endif
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) then
        call CloseIfOpened(m40)
        go to 9999
      endif
1210  DMezN=dmez
      if(l48) then
        nan=0
        KMxMod=0
        if(RadiationUsed.eq.NeutronRadiation) then
          NPeakMax=2
        else
          NPeakMax=1
        endif
        do 1260NPeak=1,NPeakMax
          rewind m40
1220      read(m40,FormA80,end=1260) Veta
          if(Veta(1:10).ne.'----------') go to 1220
          if(NPeak.eq.1) then
            if(LocateSubstring(Veta,'Fourier maxima',.false.,.true.)
     1         .le.0) go to 1220
          else
            if(LocateSubstring(Veta,'Fourier minima',.false.,.true.)
     1         .le.0) go to 1220
          endif
          read(m40,FormA,end=1260)
1240      nan=nan+1
          read(m40,FormA80,end=1250) Veta
          if(Veta(1:10).eq.'----------') then
            if(NPeak.ne.NPeakMax) backspace m40
            go to 1250
          endif
          read(Veta,101) At,xp,k
          do i=1,k+1
            read(m40,FormA80,end=1250) Veta
          enddo
          if(k.gt.0) read(m40,100,end=1250)
          KMxMod=max(k,KMxMod)
          if(DSkip.gt.0.) then
            if(PointAlreadyPresent(xp,xo,x,NAtCalc,DSkip,.false.,dpom,i,
     1         i,iswn)) nan=nan-1
          endif
          go to 1240
1250      if(NPeak.ne.NPeakMax) nan=nan-1
1260    continue
        if(nan.gt.0) then
          allocate(atomn(nan),kmodxn(nan),xn(3,nan),Rho(nan))
          if(Klic.ne.0) then
            go to 1270
          else
            if(nan.gt.ubound(isfn,1)) go to 1270
          endif
          go to 1280
1270      if(allocated(isfn)) deallocate(isfn)
          allocate(isfn(nan))
1280      n=max(KMxMod,1)
          allocate(uxn(3,n,nan),uyn(3,n,nan))
        endif
        nan=nan-1
        i=0
        do 1360NPeak=1,NPeakMax
          rewind m40
1320      read(m40,FormA80,end=1360) Veta
          if(Veta(1:10).ne.'----------') go to 1320
          if(NPeak.eq.1) then
            if(LocateSubstring(Veta,'Fourier maxima',.false.,.true.)
     1         .le.0) go to 1320
          else
            if(LocateSubstring(Veta,'Fourier minima',.false.,.true.)
     1         .le.0) go to 1320
          endif
          read(m40,FormA,end=1360)
1340      i=i+1
          read(m40,FormA80,end=1350) Veta
          if(Veta(1:10).eq.'----------') then
            if(NPeak.ne.NPeakMax) backspace m40
            go to 1350
          endif
          read(Veta,'(a8,19x,3f9.6,12x,i3)')
     1      atomn(i),(xn(j,i),j=1,3),kmodxn(i)
          isfn(i)=0
          read(m40,'(2f9.6)',end=1350) pom,Rho(i)
          call zhusti(atomn(i))
          call uprat(atomn(i))
          do j=1,kmodxn(i)
            read(m40,'(6f9.6)',end=1350)
     1        (uxn(k,j,i),k=1,3),(uyn(k,j,i),k=1,3)
          enddo
          if(kmodxn(i).gt.0) read(m40,FormA,end=1350)
          if(DSkip.gt.0.) then
            if(PointAlreadyPresent(xn(1,i),xo,x,NAtCalc,DSkip,.false.,
     1                             dpom,j,j,iswn)) i=i-1
          endif
          go to 1340
1350      i=i-1
1360    continue
      else
        allocate(atomn(1),kmodxn(1),xn(3,1),uxn(3,1,1),uyn(3,1,1),
     1           Rho(1))
        kmodxn(1)=0
      endif
      call CloseIfOpened(m40)
      if(nan.le.0.and.l48) then
        call FeChybne(-1.,-1.,'there are no Fourier peak within '//
     1                'defined region.',' ',Warning)
        go to 9999
      endif
      isfp=1
      biso=3.
      NNewAt=0
      NModAt=0
      id=NextQuestId()
      if(WizardMode) then
        xqd=WizardLength
        il=WizardLines
        Veta='Define atomic positions:'
        call FeQuestTitleMake(id,Veta)
        iswn=1
      else
        xqd=650.
        il=17
        Veta=' '
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,-1)
      endif
      ild=11
      if(l48) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'_list.tmp','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        do i=1,nan
          write(ln,FormA) Atomn(i)(:idel(Atomn(i)))
        enddo
        call CloseIfOpened(ln)
        xpom=5.
        dpom=xqd*.5-50.-SbwPruhXd
        il=ild+1
        call FeQuestSbwMake(id,xpom,il,dpom,ild,3,CutTextFromRight,
     1                      SbwHorizontal)
        nSbwSel=SbwLastMade
        call FeQuestSbwMenuOpen(SbwLastMade,fln(:ifln)//'_list.tmp')
        il=ild+3
        Veta=' '
        call FeQuestLblMake(id,xpom,il,Veta,'L','N')
        nLblRho=LblLastMade
        il=1
        xpom=xpom+dpom*.5
        Veta='List of peaks'
        call FeQuestLblMake(id,xpom,il,Veta,'C','N')
        nEdwNewCoordinates=0
        nButtNewCoordinates=0
      else
        il=ild/2+1
        Veta='%Type new atom coordinates'
        xpom=5.
        dpom=xqd*.5-60.
        tpom=xpom+dpom*.5
        call FeQuestEdwMake(id,tpom,il,xpom,il+1,Veta,'C',
     1                      dpom,EdwYd,0)
        nEdwNewCoordinates=EdwLastMade
        call FeQuestRealAEdwOpen(EdwLastMade,xn,3,.true.,.false.)
        Veta='Use new coordinates ->'
        il=il+2
        pom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpom+(dpom-pom)*.5,il,pom,ButYd,Veta)
        nButtNewCoordinates=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        nSbwSel=0
        nLblRho=0
        nan=0
      endif
      xpom=xqd*.5-40.
      dpom=xqd-xpom-5.
      il=ild+1
      call FeQuestSbwMake(id,xpom,il,dpom,ild,1,CutTextFromRight,
     1                    SbwHorizontal)
      call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
      nSbwList=SbwLastMade
      call EM40NewAtCheckReset
      call EM40NewAtCheck
      il=1
      Veta='Equivalent coordinates'
      tpom=xpom+40.
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+FeTxLength(Veta)+50.*EnlargeFactor
      Veta='Distance'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      tpom=tpom+FeTxLength(Veta)+25.*EnlargeFactor
      Veta='Atom'
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      il=ild+3
      Veta='Include selected peak'
      pom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,xpom+(dpom-pom)*.5,il,pom,ButYd,Veta)
      nButtInclude=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ilp=-10*il-12
      Veta='No peaks included'
      call FeQuestLblMake(id,xpom+dpom*.5,ilp,Veta,'C','N')
      nLblIncuded=LblLastMade
      ilp=-10*il-18
      Veta='No atom modified'
      call FeQuestLblMake(id,xpom+dpom*.5,ilp,Veta,'C','N')
      nLblModified=LblLastMade
      il=il+2
      if(.not.WizardMode) then
        il=il+1
        Veta='%Finish'
        dpom=FeTxLengthUnder(Veta)+20.
        call FeQuestButtonMake(id,(xqd-dpom)*.5,il,dpom,ButYd,Veta)
        nButtFinish=ButtonLastMade
      else
        nButtFinish=0
      endif
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  if(l48) then
        call FeQuestEventWithCheck(id,ich,EM40NewAtCheck,FeVoid)
      else
        call FeQuestEvent(id,ich)
      endif
      if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtInclude)
     1   .or.(CheckType.eq.EventSbwDoubleClick.and.
     2        mod(CheckNumber,100).eq.nSbwList)) then
        if(l48) then
          j=SbwItemPointerQuest(nSbwSel)
        else
          j=1
        endif
        i=SbwItemPointerQuest(nSbwList)
        if(i.le.0) go to 1500
        i=MaxOrder(i)
        if(i.eq.0) then
          call EM40NewAtomComplete(xn(1,j),uxn(1,1,j),
     1                             uyn(1,1,j),kmodxn(j),iswn,ich)
        else
          call EM40NewAtomComplete(xdist(1,i),uxdist(1,1,i),
     1                             uydist(1,1,i),kmodxn(j),iswn,ich)
        endif
        if(ich.le.0) then
          if(ich.eq.0) then
            NNewAt=NNewAt+1
            if(NNewAt.eq.1) then
              Veta='One new atom has'
            else
              write(Cislo,102) NNewAt
              call Zhusti(Cislo)
              Veta=Cislo(:idel(Cislo))//' new atoms have'
            endif
            Veta=Veta(:idel(Veta)+1)//'been already included'
            call FeQuestLblChange(nLblIncuded,Veta)
          else if(ich.lt.0) then
            NModAt=NModAt+1
            if(NModAt.eq.1) then
              Veta='One atom has'
            else
              write(Cislo,102) NModAt
              call Zhusti(Cislo)
              Veta=Cislo(:idel(Cislo))//' atoms have'
            endif
            Veta=Veta(:idel(Veta)+1)//'been modified'
            call FeQuestLblChange(nLblModified,Veta)
          endif
          if(l48) then
            call FeQuestSbwItemSelDel(nSbwSel)
            do i=j+1,nan
              call CopyVek(xn(1,i),xn(1,i-1),3)
              do k=1,kmodxn(i)
                call CopyVek(uxn(1,k,i),uxn(1,k,i-1),3)
                call CopyVek(uyn(1,k,i),uyn(1,k,i-1),3)
              enddo
              kmodxn(i-1)=kmodxn(i)
              Atomn(i-1)=Atomn(i)
              Rho(i-1)=Rho(i)
            enddo
            nan=nan-1
          else
            nan=0
            call FeQuestRealAEdwOpen(nEdwNewCoordinates,xn,3,.true.,
     1                               .false.)
          endif
        endif
        call EM40NewAtCheckReset
        call EM40NewAtCheck
        go to 1500
      else if(CheckType.eq.EventSbwDoubleClick.and.
     1        mod(CheckNumber,100).eq.nSbwSel) then
        go to 1500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtNewCoordinates) then
        call FeQuestRealAFromEdw(nEdwNewCoordinates,xn)
        nan=1
        call EM40NewAtCheck
        go to 1500
      else if((CheckType.eq.EventButton.and.
     1         CheckNumber.eq.nButtFinish).or.
     2        (CheckType.eq.EventKey.and.
     3         CheckNumber.eq.JeEscape)) then
        if(NNewAt.le.0.and.NModAt.le.0) then
          Veta='Do you really want to leave the form?'
          if(.not.FeYesNo(-1.,YBottomMessage,Veta,0)) go to 1500
        else
          if(NNewAt.gt.0) then
            Veta='Do you want to include new atom'
            if(NNewAt.gt.1) Veta=Veta(:idel(Veta))//'s'
          endif
          if(NModAt.gt.0) then
            if(NNewAt.gt.0) then
              Veta=Veta(:idel(Veta))//' and modify the selected one'
              if(NModAt.gt.1) Veta=Veta(:idel(Veta))//'s'
            else
              Veta='Do you want to modify the selected atom'
              if(NModAt.gt.1) Veta=Veta(:idel(Veta))//'s'
            endif
          endif
          Veta=Veta(:idel(Veta))//'?'
          call FeYesNoCancel(-1.,YBottomMessage,Veta,1,i)
          if(i.eq.1) then
            ich=0
          else if(i.eq.2) then
            ich=1
          else
            go to 1500
          endif
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(.not.WizardMode) call FeQuestRemove(id)
      call EM40UpdateAtomLimits
9999  if(allocated(kmodxn)) deallocate(Atomn,kmodxn,xn,Rho)
      if(allocated(uxn)) deallocate(uxn,uyn)
      if(Klic.ne.0) then
        if(allocated(isfn)) deallocate(isfn)
      endif
      call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
100   format(f8.4)
101   format(a8,19x,3f9.6,12x,i3)
102   format(3i5)
      end
      subroutine EM40NewAtCheck
      use Atoms_mod
      use Editm40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension KModPom(7)
      character*80 t80,Veta
      integer SbwItemPointerQuest,SbwLnQuest
      data LastMax/-1/
      if(nSbwSel.gt.0) then
        nmx=SbwItemPointerQuest(nSbwSel)
        if(nmx.eq.LastMax) go to 9999
      else
        if(nan.eq.1) then
          nmx=1
        else
          nmx=0
        endif
      endif
      if(nmx.gt.0) then
        if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
        if(kmodxn(nmx).gt.KModAMax(2)) then
          call CopyVekI(KModAMax,KModPom,7)
          KModPom(2)=kmodxn(nmx)
          call ReallocateAtomParams(NAtAll,itfmax,IFrMax,KModPom,
     1                              MagParMax)
        endif
        call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+1)
        NAtCalc=NAtCalc+1
        NAtCalcBasic=NAtCalcBasic+1
        call SetBasicKeysForAtom(NAtCalc)
        call CopyVek(xn(1,nmx),x(1,NAtCalc),3)
        do k=1,kmodxn(nmx)
          call CopyVek(uxn(1,k,nmx),ux(1,k,NAtCalc),3)
          call CopyVek(uyn(1,k,nmx),uy(1,k,NAtCalc),3)
        enddo
        KModA(2,NAtCalc)=kmodxn(nmx)
        iswa(NAtCalc)=iswn
        kswa(NAtCalc)=KPhase
        atom(NAtCalc)='Itself'
        call SpecAt
        call DistForOneAtom(NAtCalc,DMezN,iswn,0)
        call AtSun(NAtMolFr(1,1)+1,NAtAll+1,NAtMolFr(1,1))
        NAtCalc=NAtCalc-1
        NAtCalcBasic=NAtCalcBasic-1
      endif
      call CloseIfOpened(SbwLnQuest(nSbwList))
      call DeleteFile(fln(:ifln)//'_newat.tmp')
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_newat.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      if(nmx.gt.0) then
        n=1
        MaxOrder(n)=0
        write(t80,101)(xn(m,nmx),m=1,3)
        k=0
        Veta=' '
1412    call kus(t80,k,Cislo)
        idl=idel(Veta)
        if(idl.le.0) then
          Veta=Tabulator//Cislo
        else
          Veta=Veta(:idl)//Tabulator//Cislo
        endif
        if(k.lt.len(t80)) go to 1412
        if(l48) then
          Veta=Veta(:idel(Veta))//' - as read in'
        else
          Veta=Veta(:idel(Veta))//' - as typed in'
        endif
        write(ln,FormA) Veta(:idel(Veta))
        dmin=999.
        do j=1,ndist
          k=ipord(j)
          if(adist(k).eq.'Itself') cycle
          if(dmin.gt.900.) dmin=ddist(k)
          n=n+1
          MaxOrder(n)=k
          write(t80,101)(xdist(m,k),m=1,3),ddist(k),adist(k)
          k=0
          Veta=' '
1414      call kus(t80,k,Cislo)
          idl=idel(Veta)
          if(idl.le.0) then
            Veta=Tabulator//Cislo
          else
            Veta=Veta(:idl)//Tabulator//Cislo
          endif
          if(k.lt.len(t80)) go to 1414
          write(ln,FormA) Veta(:idel(Veta))
        enddo
      endif
1450  call CloseIfOpened(ln)
      call FeQuestSbwMenuOpen(nSbwList,fln(:ifln)//'_newat.tmp')
      if(ndist.gt.1) then
        call FeQuestSbwItemOff(nSbwList,1)
        call FeQuestSbwItemOn(nSbwList,2)
      endif
      if(nLblRho.gt.0.and.nmx.gt.0) then
        write(Cislo,'(f15.3)') Rho(nmx)
        call Zhusti(Cislo)
        Veta='Peak : '//Atomn(nmx)(:idel(Atomn(nmx)))//'    Charge : '//
     1       Cislo(:idel(Cislo))
        call FeQuestLblChange(nLblRho,Veta)
        LastMax=nmx
      endif
      go to 9999
      entry EM40NewAtCheckReset
      LastMax=-1
      return
9999  return
101   format(3(f9.6,1x),f8.2,1x,a8)
      end
      subroutine EM40NewAtomComplete(xi,uxi,uyi,kmodxi,isw,ich)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*256 EdwStringQuest
      character*80 Veta
      character*12 menp(0:50)
      character*8  At,AtPom
      data menp(0)/'Atomic part'/,Biso/3./,isfp/1/,At/' '/
      dimension xi(*),uxi(3,*),uyi(3,*),xp(3),ip(1)
      integer RolMenuSelectedQuest
      logical Novy,FeYesNo,NUsed(:),WizardModeIn
      allocatable NUsed
      data AtPom/' '/
      WizardModeIn=WizardMode
      WizardMode=.false.
      NAtTest=1000
      allocate(NUsed(NAtTest))
      call EM40SetMolName
      i=idel(At)
      if(i.gt.0) then
        if(At(i:i).ne.'*') At=' '
      endif
      MolPart=0
      do i=1,MaxMolPos
        menp(i)=MolMenu(i)
      enddo
      xp(1)=0.
      call SpecPos(xi,xp,0,isw,.2,nocc)
      if(MaxMolPos.gt.0) then
        iw=6
      else
        iw=5
      endif
1100  id=NextQuestId()
      xqd=300.
      Veta='Complete information for the new atom'
      call FeQuestCreate(id,-1.,-1.,xqd,iw,Veta,1,LightGray,0,0)
      il=1
      tpom=5.
      xpom=150.
      dpom=xqd-xpom-60.
      Veta='%Name of the atom'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwAtName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,AtPom)
      Veta='%Info'
      dpomb=FeTxLengthUnder(Veta)+10.
      xpomb=xpom+dpom+15.
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      nButtInfo=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      if(lite(KPhase).eq.0) then
        Veta='%Uiso'
        Biso=Biso/episq
      else
        Veta='%Biso'
      endif
      il=il+1
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwUiso=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Biso,.false.,.false.)
      if(Lite(KPhase).eq.0) Biso=Biso*episq
      il=il+1
      Veta='Site symmetry order:'
      write(Cislo,'(i3)') nocc
      Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
      call FeQuestLblMake(id,tpom,il,Veta,'L','N')
      xpom=xpom+15.
      dpom=dpom-15.
      il=il+1
      Veta='Occupancy %reduction'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      Reduction=1.
      nEdwReduction=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Reduction,.false.,.false.)
      il=il+1
      call FeQuestRolMenuMake(id,tpom,il,xpom,il,'Atomic %type','L',
     1                        dpom,EdwYd,1)
      nRolMenuAtType=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,AtTypeMenu,
     1                        NAtFormula(KPhase),isfp)
      if(MaxMolPos.gt.0) then
        il=il+1
        Veta='%Part of structure'
        xpom=tpom+FeTxLengthUnder(Veta)+10.
        dpom=xqd-xpom-10.
        call FeQuestRolMenuMake(id,tpom,il,xpom,il,
     1                          Veta,'L',dpom,EdwYd,1)
        nRolMenuPart=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,menp,MaxMolPos+1,
     1                          MolPart+1)
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        if(AtPom.eq.' ') then
          call FeChybne(-1.,YBottomMessage,'Atom name cannot be an '//
     1                  'empty string.',' ',SeriousError)
          go to 1500
        endif
        QuestCheck(id)=0
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtInfo)
     1  then
        call FeFillTextInfo('em40newatomcomplete.txt',0)
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 1500
      else if(CheckType.eq.EventEdw) then
        if(CheckNumber.eq.nEdwAtName) then
          AtPom=EdwStringQuest(nEdwAtName)
          if(AtPom.eq.' ') go to 1500
          call zhusti(AtPom)
          At=AtPom
          idl=idel(AtPom)
          if(AtPom(idl:idl).eq.'*') then
            idl=idl-1
            if(idl.le.0) go to 1650
            call SetLogicalArrayTo(NUsed,NAtTest,.false.)
            do j=1,2
              if(j.eq.1) then
                i1=1
                i2=NAtInd
              else
                i1=NAtMolFr(1,1)
                i2=NAtAll
              endif
              do i=i1,i2
                Veta=Atom(i)
                if(LocateSubstring(Veta,AtPom(:idl),.false.,.true.)
     1             .gt.0) then
                  k=idl
                  call StToInt(Veta,k,ip,1,.false.,ichp)
                  if(ichp.ne.0) cycle
                  NUsed(ip(1))=.true.
                endif
              enddo
            enddo
            do i=1,NAtTest
              if(.not.NUsed(i)) then
                write(Cislo,FormI15) i
                call Zhusti(Cislo)
                At=AtPom(:idl)//Cislo(:idel(Cislo))
                go to 1650
              endif
            enddo
          endif
1650      call uprat(At)
          call AtCheck(At,ichp,j)
          if(ichp.eq.1) then
            call FeChybne(-1.,YBottomMessage,'Unacceptable symbol in '//
     1                    'the names string, try again.',' ',
     2                    SeriousError)
            go to 1500
          endif
          call FeQuestStringEdwOpen(nEdwAtName,At)
          n=0
          do j=1,NAtFormula(KPhase)
            k=idel(AtType(j,KPhase))
            if(k.eq.0) cycle
            k=LocateSubstring(At,AtType(j,KPhase)(:k),.false.,.true.)
            if(k.ne.1) then
              cycle
            else
              n=j
              if(idel(AtType(j,KPhase)).eq.2) go to 1730
            endif
          enddo
          if(n.eq.0) go to 1500
1730      isfp=n
          call FeQuestRolMenuOpen(nRolMenuAtType,AtTypeMenu,
     1                            NAtFormula(KPhase),isfp)
          go to 1500
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuAtType) then
        isfp=RolMenuSelectedQuest(nRolMenuAtType)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuPart) then
        MolPart=RolMenuSelectedQuest(nRolMenuPart)-1
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwUiso,Biso)
        if(Lite(KPhase).eq.0) Biso=Biso*episq
        call FeQuestRealFromEdw(nEdwReduction,reduction)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      if(MolPart.gt.0) then
        ji=-ktatmol(menp(MolPart))
        im=(ji-1)/mxp+1
      else
        ji=0
        im=0
      endif
      if(im.gt.0) then
        do j=1,3
          xi(j)=xi(j)-trans(j,ji)-xm(j,im)
        enddo
        call multm(RotiMol(1,ji),xi,xp,3,3,1)
        call AddVek(xp,xm(1,im),xi,3)
        do j=1,kmodxi
          call multm(RotiMol(1,ji),uxi(1,j),xp,3,3,1)
          call CopyVek(xp,uxi(1,j),3)
          call multm(RotiMol(1,ji),uyi(1,j),xp,3,3,1)
          call CopyVek(xp,uyi(1,j),3)
        enddo
      endif
      kam=ktatmol(at)
      if(kam.le.0) then
        if(im.eq.0) then
          if(NAtAll.ge.MxAtAll) call ReallocateAtoms(100)
          kam=NAtIndTo(isw,KPhase)+1
          call AtSun(kam,NAtAll,kam+1)
          NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
          call EM40UpdateAtomLimits
        else
          n=KPoint(im)*mam(im)
          if(NAtAll+n.ge.MxAtAll) call ReallocateAtoms(100*(n+1))
          call AtSun(NAtMolFr(1,1),NAtAll,NAtMolFr(1,1)+n)
          NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+n
          NAtMolLen(isw,KPhase)=NAtMolLen(isw,KPhase)+KPoint(im)
          call EM40UpdateAtomLimits
          kam=NAtMolFr(1,1)+1
          do i=1,im-1
            kam=kam+iam(i)/KPoint(i)
          enddo
          kam=kam+iam(im)/KPoint(im)-1
          call AtSun(kam,NAtAll-KPoint(im),kam+KPoint(im))
          iam(im)=iam(im)+KPoint(im)
        endif
        call SetBasicKeysForAtom(kam)
        if(kam.eq.1) then
          PrvniKiAtomu(kam)=ndoff
        else
          PrvniKiAtomu(kam)=PrvniKiAtomu(kam-1)+DelkaKiAtomu(kam-1)
        endif
        DelkaKiAtomu(kam)=0
        Novy=.true.
      else
        if(.not.FeYesNo(-1.,-1.,'Do you really want to '//
     1    'modify/replace the atom '//at(1:idel(at))//'?',1)) then
          go to 1100
        endif
        Novy=.false.
        ich=-1
      endif
      ai(kam)=Reduction/float(nocc)
      sai(kam)=0.
      call CopyVek(xi(1),x(1,kam),3)
      isf(kam)=isfp
      do j=1,kmodxi
        call CopyVek(uxi(1,j),ux(1,j,kam),3)
        call CopyVek(uyi(1,j),uy(1,j,kam),3)
        call SetRealArrayTo(sux(1,j,kam),3,0.)
        call SetRealArrayTo(suy(1,j,kam),3,0.)
         phf(kam)=0.
        sphf(kam)=0.
      enddo
      if(Novy) then
        Atom(kam)=at
        iswa(kam)=isw
        kswa(kam)=KPhase
        kmol(kam)=ji
        KModA(2,kam)=kmodxi
        KFA(2,kam)=0
        beta(1,kam)=Biso
        call SetRealArrayTo( beta(2,kam),5,0.)
        call SetRealArrayTo(sbeta(1,kam),6,0.)
      else
        KModA(2,kam)=kmodxi
        KFA(2,kam)=0
      endif
      call ShiftKiAt(kam,itf(kam),ifr(kam),lasmax(kam),KModA(1,kam),
     1               MagPar(kam),.false.)
      call SetIntArrayTo(KiA(1,kam),DelkaKiAtomu(kam),0)
      if(itf(kam).eq.2.and.kmodxi.gt.0) then
        do j=1,3
          xp(j)=sqrt(ux(j,1,kam)**2+uy(j,1,kam)**2)*pi
        enddo
        do j=1,6
          call indext(j,k,l)
          beta(j,kam)=beta(j,kam)-xp(k)*xp(l)
        enddo
      endif
      if(NMolec.gt.0) then
        NAtCalc=NAtInd
        call SetMol(0,0)
      endif
9999  deallocate(NUsed)
      deallocate(MolMenu)
      WizardMode=WizardModeIn
      return
      end
      subroutine EM40DefWaves(ich)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kwo(3,mxw),kw0(3),GammaIntP(9),GammaIntPI(9),kwp(3)
      character*80 Veta
      character*3 qc(3)
      character*2 nty
      integer GammaInt(:,:)
      logical Prvne,eqiv
      data qc/'*q1','*q2','*q3'/,kw0/3*0/
      allocatable GammaInt
      allocate(GammaInt(9,NSymm(KPhase)))
      Prvne=.true.
      call CopyVekI(kw(1,1,KPhase),kwo,3*mxw)
      xqd=120.+80.*NDimI(KPhase)
      n1=1
      n2=min(mxw,8)
      id=NextQuestId()
      ilm=n2-n1+3
      if(NDimI(KPhase).gt.1) ilm=ilm+1
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,'Modulation waves',0,
     1                   LightGray,0,0)
      if(mxw.gt.8) then
        il=ilm-1
        dpom=50.
        xpom=xqd-dpom-5.
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Next')
        nButtNext=ButtonLastMade
        il=1
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,'%Previous')
        nButtPrevious=ButtonLastMade
      endif
      if(NDimI(KPhase).gt.1) then
        il=ilm
        Veta='%Complete the set'
        dpom=FeTxLengthUnder(Veta)+10.
        xpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
        nButtComplete=ButtonLastMade
        call FeQuestButtonOpen(nButtComplete,ButtonOff)
        do is=1,NSymm(KPhase)
          do j=4,NDim(KPhase)
            do i=4,NDim(KPhase)
              GammaIntP(i-3+(j-4)*NDimI(KPhase))=
     1          rm6(i+(j-1)*NDim(KPhase),is,1,KPhase)
            enddo
          enddo
          call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
          do j=1,NDimI(KPhase)*NDimI(KPhase)
            GammaInt(j,is)=nint(GammaIntPI(j))
          enddo
        enddo
      endif
      il=1
      do i=1,8
        il=il+1
        call FeQuestLblMake(id,5.,il,' ','L','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblNFirst=LblLastMade
      enddo
      il=1
      do i=1,NDimI(KPhase)
        il=il+1
        xpom=60.+EdwMarginSize
        do j=1,NDimI(KPhase)
          call FeQuestLblMake(id,xpom,il,' ','L','N')
          call FeQuestLblOff(LblLastMade)
          if(i.eq.1.and.j.eq.1) nLblWFirst=LblLastMade
          xpom=xpom+80.
        enddo
      enddo
1500  n=n2-n1+1
      if(mxw.gt.8) then
        if(n2.lt.mxw) then
          call FeQuestButtonOff(nButtNext)
        else
          call FeQuestButtonDisable(nButtNext)
        endif
        if(n1.gt.1) then
          call FeQuestButtonOff(nButtPrevious)
        else
          call FeQuestButtonDisable(nButtPrevious)
        endif
      endif
      j=n1
      iw=0
      nLblN=nLblNFirst
      nLblW=nLblWFirst
      il=1
      do i=1,n
        il=il+1
        write(Veta,100) j,nty(j)
        call FeQuestLblChange(nLblN,Veta)
        xpom=60.
        dpom=40.
        do k=1,NDimI(KPhase)
          iw=iw+1
          if(Prvne) call FeQuestEdwMake(id,5.,i,xpom,il,' ','L',dpom,
     1                                  EdwYd,0)
          if(j.gt.NDimI(KPhase)) then
            call FeQuestIntEdwOpen(iw,kw(k,j,KPhase),.false.)
          else
            write(Veta,'(i5)') kw(k,j,KPhase)
            call Zhusti(Veta)
            call FeQuestLblChange(nLblW,Veta)
            nLblW=nLblW+1
          endif
          if(Prvne) then
            Veta=qc(k)(:min(NDimI(KPhase)+1,3))
            if(k.lt.NDimI(KPhase)) Veta=Veta(:idel(Veta))//'+'
            call FeQuestLblMake(id,xpom+dpom+5.,il,Veta,'L','N')
          endif
          xpom=xpom+80.
        enddo
        j=j+1
        nLblN=nLblN+1
      enddo
      Prvne=.false.
2000  call FeQuestEvent(id,ich)
      iw=0
      j=n1
      do i=1,n
        do k=1,NDimI(KPhase)
          iw=iw+1
          if(j.gt.NDimI(KPhase)) then
            call FeQuestIntFromEdw(iw,kw(k,j,KPhase))
            call FeQuestEdwClose(iw)
          endif
        enddo
        j=j+1
      enddo
      if(CheckType.eq.EventButton.and.(CheckNumber.eq.nButtPrevious.or.
     1                                 CheckNumber.eq.nButtNext)) then
        if(CheckNumber.eq.nButtNext) then
          n1=n1+8
          n2=min(n2+8,mxw)
        else
          n1=max(n1-8,1)
          n2=n2-8
        endif
        go to 1500
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtComplete)
     1  then
        do i=1,mxw
          if(eqiv(kw(1,i,KPhase),kw0,NDimI(KPhase))) go to 2210
        enddo
        i=mxw+1
2210    NwLast=i-1
        NwLastOld=NwLast
        i=0
2220    i=i+1
        if(i.gt.NwLast) go to 1500
        do 2300is=1,NSymm(KPhase)
          call multmi(kw(1,i,KPhase),GammaInt(1,is),kwp,1,NDimI(KPhase),
     1                NDimI(KPhase))
          do iz=1,2
            do j=1,NwLast
              if(eqiv(kwp,kw(1,j,KPhase),NDimI(KPhase))) go to 2300
            enddo
            call IntVectorToOpposite(kwp,kwp,NDimI(KPhase))
          enddo
          if(NwLast.lt.mxw) then
            NwLast=NwLast+1
            do i=1,NDimI(KPhase)
              if(kwp(i).lt.0) then
                call IntVectorToOpposite(kwp,kwp,NDimI(KPhase))
                go to 2260
              else if(kwp(i).gt.0) then
                go to 2260
              endif
            enddo
2260        call CopyVekI(kwp,kw(1,NwLast,KPhase),NDimI(KPhase))
          else
            write(Veta,FormI15) mxw
            call Zhusti(Veta)
            Veta='number of generated waves exceeds the limit of '//
     1           Veta(:idel(Veta))//' waves'
            call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            call SetIntArrayTo(kw(1,NwLastOld+1,KPhase),
     1                         3*(mxw-NwLastOld),0)
            go to 1500
          endif
2300    continue
        go to 2220
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) call CopyVekI(kwo,kw(1,1,KPhase),3*mxw)
      deallocate(GammaInt)
      return
100   format(i2,a2,' wave')
      end
      subroutine EM40RenameAccordingToAtomType(ich)
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*8 AtNew
      dimension natp(:)
      logical EqIgCase
      allocatable natp
      allocate(natp(NAtFormula(KPhase)))
      ich=0
      call SetIntArrayTo(natp,NAtFormula(KPhase),0)
      do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          jj=isf(ia)
          do j=1,jj
            if(EqIgCase(AtType(jj,KPhase),AtType(j,KPhase))) exit
          enddo
1100      natp(j)=natp(j)+1
          write(AtNew,'(a2,i4)') AtType(j,KPhase),natp(j)
          call zhusti(AtNew)
          do k=1,NAtActive
            if(.not.LAtActive(k)) then
              if(EqIgCase(AtNew,Atom(IAtActive(k)))) go to 1100
            endif
          enddo
          call CrlAtomNamesSave(Atom(ia),AtNew,1)
          Atom(ia)=AtNew
        endif
      enddo
      deallocate(natp)
      return
      end
      subroutine EM40RenameManually(ich)
      use EditM40_mod
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*256 EdwStringQuest
      character*8 AtNameNew(:)
      integer IAt(:)
      logical EqIgCase
      allocatable AtNameNew,IAt
      allocate(AtNameNew(NAtAll),IAt(NAtAll))
      ich=0
      na=0
      do i=1,NAtActive
        if(LAtActive(i)) then
          na=na+1
          AtNameNew(na)=Atom(IAtActive(i))
          IAt(na)=IAtActive(i)
        endif
      enddo
      id=NextQuestId()
      xqd=550.
      if(na.gt.45) then
        il=16
      else
        il=15
      endif
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Edit individual atom names',
     1                   0,LightGray,0,0)
      tpom=5.
      xpom=80.
      dpom=80.
      il=0
      do i=1,min(na,45)
        il=il+1
        call FeQuestLblMake(id,tpom,il,' ','L','N')
        call FeQuestEdwMake(id,xpom-5.,il,xpom,il,' ','R',dpom,EdwYd,
     1                      1)
        if(mod(i,15).eq.0) then
          call FeQuestSvisliceFromToMake(id,xpom+dpom+15.,1,15,0)
          il=0
          tpom=tpom+180.
          xpom=xpom+180.
        endif
        if(i.eq.1) then
          nLblFr=LblLastMade
          nEdwFr=EdwLastMade
        endif
      enddo
      if(na.gt.0) then
        il=-165
        tpom=15.
        dpom=60.
        Cislo='Previous'
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Cislo)
        nButtPrevious=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        tpom=xqd-dpom-15.
        Cislo='Next'
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Cislo)
        nButtNext=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      else
        nButtPrevious=0
        nButtNext=0
      endif
      NFrom=1
      NTo=min(na,45)
1400  nLbl=nLblFr
      nEdw=nEdwFr
      do i=NFrom,NTo
        write(Cislo,'(''#'',i5)') i
        call Zhusti(Cislo)
        call FeQuestLblChange(nLbl,Cislo)
        Cislo=Atom(IAt(i))
        Cislo=Cislo(:idel(Cislo))//'=>'
        call FeQuestEdwLabelChange(nEdw,Cislo)
        call FeQuestStringEdwOpen(nEdw,AtNameNew(i))
        nLbl=nLbl+1
        nEdw=nEdw+1
      enddo
      do i=NTo+1,NFrom+44
        call FeQuestLblOff(nLbl)
        call FeQuestEdwClose(nEdw)
        nLbl=nLbl+1
        nEdw=nEdw+1
      enddo
      if(nButtPrevious.gt.0) then
        if(NFrom.le.1) then
          call FeQuestButtonDisable(nButtPrevious)
        else
          call FeQuestButtonOff(nButtPrevious)
        endif
        if(NTo.ge.na) then
          call FeQuestButtonDisable(nButtNext)
        else
          call FeQuestButtonOff(nButtNext)
        endif
      endif
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtPrevious) then
          NFrom=max(NFrom-45,1)
          NTo=min(NFrom+44,na)
        else
          NFrom=max(NFrom+45,1)
          NTo=min(NFrom+44,na)
        endif
        go to 1400
      else if(CheckType.eq.EventEdw) then
        Cislo=EdwStringQuest(CheckNumber)
        call AtCheck(Cislo,ichp,i)
        if(ichp.eq.1) then
          call FeChybne(-1.,-1.,'unacceptable symbol in atom string '//
     1                  'try again.',' ',SeriousError)
          go to 1500
        endif
        AtNameNew(nFrom+CheckNumber-nEdwFr)=Cislo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        j=0
        do i=1,NAtActive
          if(LAtActive(i)) then
            j=j+1
            ia=IAtActive(i)
            call CrlAtomNamesSave(Atom(ia),AtNameNew(j),1)
            Atom(ia)=AtNameNew(j)
          endif
        enddo
        do i=1,NAtAll
          if(i.gt.NAtInd.and.i.lt.NAtMolFr(1,1)) cycle
          do j=i+1,NAtAll
            if(j.gt.NAtInd.and.j.lt.NAtMolFr(1,1)) cycle
            if(EqIgCase(Atom(i),Atom(j))) then
              call CrlCorrectAtomNames(ich)
              go to 9000
            endif
          enddo
        enddo
      endif
      call FeQuestRemove(id)
9000  if(allocated(AtNameNew)) deallocate(AtNameNew,IAt)
      return
      end
      subroutine EM40TransAtSelected(ich)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension GammaIntP(9),GammaIntPI(9),kwz(3),kwp(3)
      integer GammaInt(9,24),CoDelat
      logical eqiv,BratMol(mxpm),SwitchedToHarmIn,DeallocAt,Expand,
     1        DeallocTr
      equivalence (IdNumbers(1),JenAtomy),
     1            (IdNumbers(2),JenMolekuly),
     2            (IdNumbers(3),JakAtomyTakMolekuly)
      Expand=.false.
      go to 1100
      entry EM40ExpandAtSelected(ich)
      Expand=.true.
1100  DeallocAt=.false.
      call SetLogicalArrayTo(BratMol,mxp*mxm,.false.)
      isw=-1
      do i=1,NAtActive
        if(.not.LAtActive(i)) cycle
        ia=IAtActive(i)
        if(isw.lt.0) then
          isw=iswa(ia)
        else if(isw.ne.iswa(ia)) then
          isw=0
          go to 1130
        endif
      enddo
1130  CoDelat=JenAtomy
      go to 1200
      entry EM40TransMol(ich)
      Expand=.false.
      go to 1150
      entry EM40ExpandMol(ich)
      Expand=.true.
1150  CoDelat=JenMolekuly
      DeallocAt=.false.
      call EM40DefMolGroup(BratMol,ich)
      if(ich.ne.0) go to 9999
      isw=-1
      n=0
      do i=1,NMolec
        do j=1,mam(i)
          n=n+1
          if(BratMol(n)) then
            if(isw.lt.0) then
              isw=iswmol(i)
            else if(isw.ne.iswmol(i)) then
              isw=0
              go to 1200
            endif
          endif
        enddo
      enddo
1200  if(Expand) then
        NTrans=24
      else
        NTrans=1
      endif
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      DeallocTr=.true.
      call EM40ReadTr(Expand,isw,ich)
      if(ich.ne.0) go to 9999
      go to 2000
      entry EM40TransAtFromTo(ifrom,ito,ich)
      Expand=.false.
      go to 1300
      entry EM40ExpandAtFromTo(ifrom,ito,ich)
      Expand=.true.
1300  i1=ifrom
      i2=ito
      call SetLogicalArrayTo(BratMol,mxpm,.false.)
      CoDelat=JenAtomy
      go to 1500
      entry EM40TransAll(ich)
      Expand=.false.
      go to 1400
      entry EM40ExpandAll(ich)
      Expand=.true.
1400  i1=1
      i2=NAtInd
      call SetLogicalArrayTo(BratMol,mxpm,.false.)
      n=0
      do i=1,NMolec
        do j=1,mam(i)
          n=n+1
          BratMol(n)=kswmol(i).eq.KPhase
        enddo
      enddo
      CoDelat=JakAtomyTakMolekuly
1500  if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      n=max(i2-i1+1,1)
      allocate(IAtActive(n),LAtActive(n),NameAtActive(n))
      DeallocAt=.true.
      DeallocTr=.false.
      ich=0
      NAtActive=0
      do i=i1,i2
        if(kswa(i).eq.KPhase) then
          NAtActive=NAtActive+1
          IAtActive(NAtActive)=i
          LAtActive(NAtActive)=.true.
        endif
      enddo
2000  SwitchedToHarmIn=SwitchedToHarm
      if(.not.SwitchedToHarmIn) call TrOrtho(0)
      if(CoDelat.eq.JenMolekuly) go to 3000
      iswo=-1
      kmodxp=1
      do i=NAtActive,1,-1
        if(.not.LAtActive(i)) cycle
        ia=IAtActive(i)
        isw=iswa(ia)
        if(isw.ne.iswo) then
          do ntr=1,ntrans
            do j=4,NDim(KPhase)
              do k=4,NDim(KPhase)
                GammaIntP(k-3+(j-4)*NDimI(KPhase))=
     1                    TransMat(k+(j-1)*NDim(KPhase),ntr,isw)
              enddo
            enddo
            call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
            do j=1,NDimI(KPhase)*NDimI(KPhase)
              GammaInt(j,ntr)=nint(GammaIntPI(j))
            enddo
            iswo=isw
          enddo
          kmodxp=1
        endif
        kmodmx=MaxUsedKw(KPhase)
        do ntr=1,ntrans
          do 2200j=kmodxp,kmodmx
            call multmi(kw(1,j,KPhase),GammaInt(1,ntr),kwp,1,
     1                  NDimI(KPhase),NDimI(KPhase))
            do k=1,NDimI(KPhase)
              kwz(k)=-kwp(k)
            enddo
            do k=1,mxw
              if(eqiv(kw(1,k,KPhase),kwp,NDimI(KPhase))) then
                TransKwSym(j,ntr)= k
                go to 2200
              else if(eqiv(kw(1,k,KPhase),kwz,NDimI(KPhase))) then
                TransKwSym(j,ntr)=-k
                go to 2200
              endif
            enddo
            call FeChybne(-1.,-1.,'some of modulation waves are not'//
     1                    ' defined.','The transformation cannot be '//
     2                    'performed.',SeriousError)
            ErrFlag=1
            go to 5000
2200      continue
        enddo
        kmodxp=kmodmx+1
        if(Expand) then
          do ntr=ntrans,1,-1
            if(NAtAll.ge.MxAtAll) call ReallocateAtoms(1)
            call AtSun(ia+1,NAtAll,ia+2)
            DelkaKiAtomu(ia+1)=0
            PrvniKiAtomu(ia+1)=PrvniKiAtomu(ia)+DelkaKiAtomu(ia)
            NAtIndLen(isw,KPhase)=NAtIndLen(isw,KPhase)+1
            NAtIndLenAll(KPhase)=NAtIndLenAll(KPhase)+1
            NAtInd=NAtInd+1
            NAtAll=NAtAll+1
            NAtCalc=NAtCalc+1
            NAtCalcBasic=NAtCalcBasic+1
            call EM40TrAt(ia,ia+1,isw,ntr)
          enddo
        else
          call EM40TrAt(ia,ia,isw,1)
        endif
      enddo
      call EM40UpdateAtomLimits
3000  if(CoDelat.eq.JenAtomy.or.NMolec.le.0) go to 5000
      n=0
      iswo=-1
      kmodxp=1
      if(Expand) then
        call ReallocateMolecules(0,mxp*NTrans)
        call ReallocateAtoms(mxp*NTrans)
      endif
      NAtMolSh=0
      do i=1,NMolec
        if(kswmol(i).ne.KPhase) cycle
        mami=mam(i)
        jip=mami+mxp*(i-1)
        isw=iswmol(i)
        if(isw.ne.iswo) then
          do ntr=1,ntrans
            do j=4,NDim(KPhase)
              do k=4,NDim(KPhase)
                GammaIntP(k-3+(j-4)*NDimI(KPhase))=
     1            TransMat(k+(j-1)*NDim(KPhase),ntr,isw)
              enddo
            enddo
            call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
            do j=1,NDimI(KPhase)*NDimI(KPhase)
              GammaInt(j,ntr)=nint(GammaIntPI(j))
            enddo
            iswo=isw
          enddo
          kmodxp=1
        endif
        kmodmx=MaxUsedKw(KPhase)
        do ntr=1,ntrans
          do 3500j=kmodxp,kmodmx
            call multmi(kw(1,j,KPhase),GammaInt(1,ntr),kwp,1,
     1                  NDimI(KPhase),NDimI(KPhase))
            do k=1,NDimI(KPhase)
              kwz(k)=-kwp(k)
            enddo
            do k=1,mxw
              if(eqiv(kw(1,k,KPhase),kwp,NDimI(KPhase))) then
                TransKwSym(j,ntr)= k
                go to 3500
              else if(eqiv(kw(1,k,KPhase),kwz,NDimI(KPhase))) then
                TransKwSym(j,ntr)=-k
                go to 3500
              endif
            enddo
            call FeChybne(-1.,-1.,'some of modulation waves are not'//
     1                    ' defined.','The transformation cannot be '//
     2                    'performed.',SeriousError)
            ErrFlag=1
            go to 5000
3500      continue
        enddo
        kmodxp=kmodmx+1
        do ip=1,mami
          n=n+1
          if(.not.BratMol(n)) cycle
          ji=ip+mxp*(i-1)
          if(Expand) then
            do ntr=1,ntrans
              jip=jip+1
              DelkaKiMolekuly(jip)=0
              PrvniKiMolekuly(jip)=PrvniKiMolekuly(jip-1)+
     1                             DelkaKiMolekuly(jip-1)
              call EM40TrMol(ji,jip,isw,ntr,0)
              mam(i)=mam(i)+1
              NAtMolSH=NAtMolSH+iam(i)
              NAtPosLen(isw,KPhase)=NAtPosLen(isw,KPhase)+iam(i)
            enddo
          else
            call EM40TrMol(ji,ji,isw,1,0)
          endif
        enddo
      enddo
      if(NAtMolSh.gt.0) then
        call ReallocateAtoms(NAtMolSh)
        call AtSun(NAtMolFrAll(1),NAtAll,NAtMolFrAll(1)+NAtMolSh)
      endif
5000  if(.not.SwitchedToHarmIn) call TrOrtho(1)
      call EM40UpdateAtomLimits
9999  if(DeallocAt.and.allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(DeallocTr.and.allocated(TransMat)) call DeallocateTrans
      return
      end
      subroutine EM40ReadTr(Expand,isw,ich)
      use Basic_mod
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension trp(36)
      character*80 Veta
      character*21 Hlavicka
      character*2 nty
      integer EdwStateQuest,SbwItemPointerQuest
      logical Expand,CrwLogicQuest
      if(Expand) then
        NTrans=0
      else
        NTrans=1
      endif
      iswp=max(isw,1)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_symm.tmp','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do j=1,NSymm(KPhase)
        Veta=' '
        do i=1,NDim(KPhase)
          Veta=Veta(:idel(Veta))//' '//
     1      symmc(i,j,iswp,KPhase)(:idel(symmc(i,j,iswp,KPhase)))
        enddo
        if(MagneticType(KPhase).gt.0) then
          if(ZMag(j,1,KPhase).gt.0.) then
            Cislo=' m'
          else
            Cislo=' -m'
          endif
          Veta=Veta(:idel(Veta))//Cislo(:idel(Cislo))
        endif
        write(ln,FormA) Veta(:idel(Veta))
      enddo
      call CloseIfOpened(ln)
      if(Expand) then
        ilm=NDim(KPhase)+5
      else
        ilm=NDim(KPhase)+4
      endif
      if(MagneticType(KPhase).gt.0) ilm=ilm+1
      id=NextQuestId()
      xqd=280.
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,'Choice',1,LightGray,0,0)
      il=1
      Veta='Fill by a %symmetry operation'
      dpom=FeTxLengthUnder(Veta)+20.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      nButtSymmetry=ButtonLastMade
      if(Expand) then
        k=ButtonDisabled
      else
        k=ButtonOff
      endif
      call FeQuestButtonOpen(ButtonLastMade,k)
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,' ','C','B')
      nLblHlavicka=LblLastMade
      dpom=120.
      xpom=(xqd-dpom)*.5
      write(Veta,100) 1,nty(1)
      tpom=xpom-FeTxLengthUnder(Veta)-5.
      do i=1,NDim(KPhase)
        il=il+1
        write(Veta,100) i,nty(i)
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) nEdwMatrixFirst=EdwLastMade
      enddo
      Veta='Translation vector'
      il=il+1
      call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
      il=il+1
      call FeQuestEdwMake(id,0.,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwShift=EdwLastMade
      if(MagneticType(KPhase).gt.0) then
        il=il+1
        xpom=tpom
        tpom=xpom+CrwXd+5.
        Veta='Apply %time inversion'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                      0,0)
        nCrwTimeInversion=CrwLastMade
      endif
      if(Expand) then
        il=il+1
        Veta='%Next matrix'
        dpom=FeTxLengthUnder(Veta)+20.
        tpom=(xqd-dpom)*.5
        call FeQuestButtonMake(id,tpom,il,dpom,ButYd,Veta)
        nButtNext=ButtonLastMade
        call FeQuestButtonOpen(nButtNext,ButtonOff)
      else
        nButtNext=0
      endif
      if(Expand) then
        Hlavicka='Expansion matrix #1'
      else
        Hlavicka='Transformation matrix'
        call FeQuestLblChange(nLblHlavicka,Hlavicka)
      endif
1140  if(NTrans.le.0) go to 1200
      call UnitMat(TransMat(1,NTrans,iswp),NDim(KPhase))
      call SetRealArrayTo(TransVec(1,NTrans,iswp),NDim(KPhase),0.)
      if(MagneticType(KPhase).gt.0) TransZM(NTrans)=1.
1150  if(NTrans.gt.0) then
        nEdw=nEdwMatrixFirst
        do i=1,NDim(KPhase)
          do j=1,NDim(KPhase)
            trp(j)=TransMat(i+(j-1)*NDim(KPhase),NTrans,iswp)
          enddo
          call FeQuestRealAEdwOpen(nEdw,trp,NDim(KPhase),.false.,.true.)
          nEdw=nEdw+1
        enddo
        call FeQuestRealAEdwOpen(nEdw,TransVec(1,NTrans,iswp),
     1                           NDim(KPhase),.false.,.true.)
      endif
      if(MagneticType(KPhase).gt.0)
     1  call FeQuestCrwOpen(nCrwTimeInversion,TransZM(NTrans).lt.0.)
1200  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.
     1   (CheckNumberAbs.eq.ButtonOk.or.CheckNumber.eq.nButtNext)) then
        if(EdwStateQuest(nEdwMatrixFirst).eq.EdwOpened) then
          nEdw=nEdwMatrixFirst
          do j=1,NDim(KPhase)
            call FeQuestRealAFromEdw(nEdw,trp)
            do i=1,NDim(KPhase)
              TransMat(j+(i-1)*NDim(KPhase),NTrans,iswp)=trp(i)
            enddo
            nEdw=nEdw+1
          enddo
          call FeQuestRealAFromEdw(nEdwShift,TransVec(1,NTrans,iswp))
          call matinv(TransMat(1,NTrans,iswp),trp,pom,NDim(KPhase))
          if(abs(pom).lt..0001) then
            call FeChybne(-1.,-1.,'The transformation matrix is '//
     1                    'singular, try again.',' ',SeriousError)
            EventType=EventEdw
            EventNumber=nEdwMatrixFirst
            go to 1200
          endif
          if(abs(abs(pom)-1.).gt..0001.and.lite(KPhase).eq.0) then
            WaitTime=10000
            NInfo=2
            TextInfo(1)='The transformation is not orthogonal and '//
     1                  'therefore'
            TextInfo(2)='ADP parameters were changed from U''s to '//
     2                  'beta''s'
            call FeInfoOut(-1.,-1.,'INFORMATION','L')
            lite(KPhase)=1
          endif
          if(MagneticType(KPhase).gt.0) then
            if(CrwLogicQuest(nCrwTimeInversion)) then
              TransZM(NTrans)=-1.
            else
              TransZM(NTrans)= 1.
            endif
          endif
        endif
        if(CheckNumber.eq.nButtNext) then
          call FeQuestButtonOff(nButtSymmetry)
          NTrans=NTrans+1
          write(Cislo,'(i2)') NTrans
          call Zhusti(Cislo)
          Hlavicka(19:)=Cislo
          call FeQuestLblChange(nLblHlavicka,Hlavicka)
          go to 1140
        else
          QuestCheck(id)=0
          go to 1200
        endif
      else if(CheckType.eq.EventButton.and.CheckNumber.ge.nButtSymmetry)
     1  then
        xqd=300.
        idp=NextQuestId()
        il=max(NSymm(KPhase)+1,5)
        il=min(NSymm(KPhase)+1,15)
        call FeQuestCreate(idp,-1.,-1.,xqd,il,'Select symmetry '//
     1                     'operation',0,LightGray,0,0)
        xpom=15.
        dpom=xqd-30.-SbwPruhXd
        il=il-1
        call FeQuestSbwMake(idp,xpom,il,dpom,il,1,CutTextFromLeft,
     1                      SbwVertical)
        call FeQuestSbwSetDoubleClick(SbwLastMade,.true.)
        nSbw=SbwLastMade
        call FeQuestSbwMenuOpen(nSbw,fln(:ifln)//'_symm.tmp')
        il=il+1
        xpom=90.
        tpom=xpom+CrwXd+5.
        Veta='Apply %inversion centre'
        call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,
     1                      0,0)
        nCrwInver=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,.false.)
1300    call FeQuestEvent(idp,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 1300
        endif
        if(ich.eq.0) then
          i=SbwItemPointerQuest(nSbw)
          call CopyVek(s6(1,i,1,KPhase),TransVec(1,NTrans,iswp),
     1                 NDim(KPhase))
          call CopyMat(rm6(1,i,1,KPhase),TransMat(1,NTrans,iswp),
     1                 NDim(KPhase))
          if(CrwLogicQuest(nCrwInver))
     1      call RealMatrixToOpposite(TransMat(1,NTrans,iswp),
     2        TransMat(1,NTrans,iswp),NDim(KPhase))
          TransZM(NTrans)=ZMag(i,1,KPhase)
        endif
        call FeQuestRemove(idp)
        if(ich.ne.0) then
          go to 1200
        else
          go to 1150
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1200
      endif
      call FeQuestRemove(id)
      if(ich.ne.0.or.NTrans.le.0) go to 9999
      do it=1,NTrans
        do i=1,NComp(KPhase)
          call CopyMat(TransMat(1,it,iswp),TransMat(1,it,i),
     1                 NDim(KPhase))
          call CopyVek(TransVec(1,it,iswp),TransVec(1,it,i),
     1                 NDim(KPhase))
        enddo
      enddo
      go to 1600
      entry EM40SetTr(isw)
1600  iswp=isw
      if(iswp.eq.0) then
        do it=1,NTrans
          do i=2,NComp(KPhase)
            call multm(zv(1,i,KPhase),TransMat(1,it,1),trp,NDim(KPhase),
     1                 NDim(KPhase),NDim(KPhase))
            call multm(trp,zvi(1,i,KPhase),TransMat(1,it,i),
     1                 NDim(KPhase),NDim(KPhase),NDim(KPhase))
            call multm(zv(1,i,KPhase),TransVec(1,it,1),TransVec(1,it,i),
     1                 NDim(KPhase),NDim(KPhase),1)
          enddo
        enddo
      else
        do i=1,NComp(KPhase)
          if(i.ne.iswp) then
            do j=1,NTrans
              call UnitMat(TransMat(1,j,i),NDim(KPhase))
              call SetRealArrayTo(TransVec(1,j,i),NDim(KPhase),0.)
            enddo
          endif
        enddo
      endif
2000  do it=1,NTrans
        do i=1,NComp(KPhase)
          call MatBlock3(TransMat(1,it,i),TransX(1,it,i),NDim(KPhase))
          if(i.eq.1) then
            call matinv(TransX(1,it,1),trp,pom,3)
            if(pom.lt.0.) then
              SigNTransX(it)=-1.
            else
              SigNTransX(it)= 1.
            endif
          endif
          if(MagneticType(KPhase).ne.0) then
            call CopyMat(TransX(1,it,i),TransM(1,it,i),3)
            if(SignTransX(it)*TransZM(it).lt.0.)
     1        call RealMatrixToOpposite(TransM(1,it,i),TransM(1,it,i),3)
          endif
          call CopyVek(TransX(1,it,i),trp,9)
          call srotb(trp,trp,TransTemp(1,it,i))
          call srotss(trp,trp,TransTempS(1,it,i))
          call srotc(trp,3,TransC3(1,it,i))
          call srotc(trp,4,TransC4(1,it,i))
          call srotc(trp,5,TransC5(1,it,i))
          call srotc(trp,6,TransC6(1,it,i))
        enddo
      enddo
9999  return
100   format('%',i1,a2,' row')
      end
      subroutine EM40TrAt(ii,io,isw,ntr)
      use Atoms_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension xp(6),xpp(6),px(28),py(28),spx(28),spy(28),phi(3),
     1          xps(6,2),snm(:),csm(:),eps(:),pxa(:),pya(:),
     2          spxa(:),spya(:),xorg(3)
      allocatable snm,csm,eps,pxa,pya,spxa,spya
      n=MaxUsedKw(KPhase)
      allocate(snm(n),csm(n),eps(n),pxa(28*n),pya(28*n),spxa(28*n),
     1         spya(28*n))
      call CopyVek(x(1,ii),xorg,3)
      if(io.ne.ii) then
        call CopyBasicKeysForAtom(ii,io)
        i=min(5,idel(atom(ii)))
        if(ntr.le.1) atom(ii)=atom(ii)(1:i)//'_1'
        write(Cislo,'(''_'',i3)') ntr+1
        call Zhusti(Cislo)
        atom(io)=atom(io)(1:i)//Cislo(:idel(Cislo))
        if(NDimI(KPhase).gt.0) then
           phf(io)= phf(ii)
          sphf(io)=sphf(ii)
        endif
        call ShiftKiAt(io,itf(io),ifr(io),lasmax(io),KModA(1,io),
     1                 MagPar(io),.true.)
        call SetIntArrayTo(KiA(1,io),DelkaKiAtomu(io),0)
      endif
      if(MagPar(ii).gt.0) then
        if(KUsePolar(ii).gt.0) then
          do j=0,MagPar(ii)-1
            if(j.eq.0) then
              call ShpCoor2Fract( sm0(1,ii),px)
              call ShpCoor2Fract(ssm0(1,ii),px)
            else
              call ShpCoor2Fract( smx(1,j,ii),px)
              call ShpCoor2Fract(ssmx(1,j,ii),px)
              call ShpCoor2Fract( smy(1,j,ii),px)
              call ShpCoor2Fract(ssmy(1,j,ii),px)
            endif
          enddo
        endif
      endif
      call CopyVek(x(1,ii),xp,3)
      call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
      call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),NDim(KPhase),1)
      do j=1,NDim(KPhase)
        xp(j)=xpp(j)+TransVec(j,ntr,isw)
        if(j.lt.4) then
          x(j,io)=xp(j)
        else
          phi(j-3)=-xp(j)*pi2
        endif
      enddo
      if(itf(ii).le.1) then
        beta(1,io)=beta(1,ii)
        sbeta(1,io)=sbeta(1,ii)
      endif
      if(NDimI(KPhase).gt.0) then
        call qbyx(x(1,io),qcnt(1,io),isw)
        do k=1,MaxUsedKw(KPhase)
          eps(k)=isign(1,TransKwSym(k,ntr))
          l=iabs(TransKwSym(k,ntr))
          pom=0.
          do m=1,NDimI(KPhase)
            pom=pom+float(kw(m,l,KPhase))*phi(m)
          enddo
          snp=sin(pom)
          csp=cos(pom)
          if(abs(snp).lt..001) snp=0.
          if(abs(csp).lt..001) csp=0.
          if(abs(abs(snp)-1.).lt..001) snp=sign(1.,snp)
          if(abs(abs(csp)-1.).lt..001) csp=sign(1.,csp)
          snm(k)=snp
          csm(k)=csp
        enddo
      endif
      if(MagneticType(KPhase).gt.0) then
        nk=itf(ii)+1
      else
        nk=itf(ii)
      endif
      do n=0,nk
        if(n.gt.itf(ii)) then
          NRank=3
        else
          NRank=TRank(n)
        endif
        if(n.gt.itf(ii)) then
          if(MagPar(ii).gt.0) then
            do j=1,NRank
              px(j)=sm0(j,ii)
              spx(j)=ssm0(j,ii)
            enddo
            call multm (TransM(1,ntr,isw), px, sm0(1,io),NRank,NRank,1)
            call multmq(TransM(1,ntr,isw),spx,ssm0(1,io),NRank,NRank,1)
          endif
        else if(n.eq.0) then
          ai(io)=ai(ii)
          sai(io)=sai(ii)
          if(KModA(1,io).gt.0) then
            a0(io)=a0(ii)
            sa0(io)=sa0(ii)
          endif
        else if(n.eq.1) then
          do j=1,NRank
            spx(j)=sx(j,ii)
          enddo
          call multmq(TransX(1,ntr,isw),spx,sx(1,io),NRank,NRank,1)
        else if(n.eq.2) then
          do j=1,NRank
            px(j)= beta(j,ii)
            spx(j)=sbeta(j,ii)
          enddo
          call multm (TransTemp(1,ntr,isw), px, beta(1,io),NRank,NRank,
     1                1)
          call multmq(TransTemp(1,ntr,isw),spx,sbeta(1,io),NRank,NRank,
     1                1)
        else if(n.eq.3) then
          do j=1,NRank
            px(j)=c3(j,ii)
            spx(j)=sc3(j,ii)
          enddo
          call multm (TransC3(1,ntr,isw), px, c3(1,io),NRank,NRank,1)
          call multmq(TransC3(1,ntr,isw),spx,sc3(1,io),NRank,NRank,1)
        else if(n.eq.4) then
          do j=1,NRank
            px(j)=c4(j,ii)
            spx(j)=sc4(j,ii)
          enddo
          call multm (TransC4(1,ntr,isw), px, c4(1,io),NRank,NRank,1)
          call multmq(TransC4(1,ntr,isw),spx,sc4(1,io),NRank,NRank,1)
        else if(n.eq.5) then
          do j=1,NRank
            px(j)=c5(j,ii)
            spx(j)=sc5(j,ii)
          enddo
          call multm (TransC5(1,ntr,isw), px, c5(1,io),NRank,NRank,1)
          call multmq(TransC5(1,ntr,isw),spx,sc5(1,io),NRank,NRank,1)
        else
          do j=1,NRank
            px(j)=c6(j,ii)
            spx(j)=sc6(j,ii)
          enddo
          call multm (TransC6(1,ntr,isw), px, c6(1,io),NRank,NRank,1)
          call multmq(TransC6(1,ntr,isw),spx,sc6(1,io),NRank,NRank,1)
        endif
        if(n.gt.itf(ii)) then
          kfap=0
          kmodp=MagPar(ii)-1
        else
          kfap=KFA(n+1,ii)
          kmodp=KModA(n+1,ii)
        endif
        if(kmodp.le.0) cycle
        nn=kmodp*NRank
        if(kfap.ne.0) then
          if(n.eq.0) then
            NDimIp=NDimI(KPhase)
          else
            NDimIp=1
          endif
        else
          NDimIp=0
        endif
        if(n.gt.itf(ii)) then
          if(MagPar(ii).gt.0) then
            call CopyVek( smx(1,1,ii), pxa,nn)
            call CopyVek( smy(1,1,ii), pya,nn)
            call CopyVek(ssmx(1,1,ii),spxa,nn)
            call CopyVek(ssmy(1,1,ii),spya,nn)
          endif
        else if(n.eq.0) then
          call CopyVek( ax(1,ii), pxa,nn)
          call CopyVek( ay(1,ii), pya,nn)
          call CopyVek(sax(1,ii),spxa,nn)
          call CopyVek(say(1,ii),spya,nn)
        else if(n.eq.1) then
          call CopyVek( ux(1,1,ii), pxa,nn)
          call CopyVek( uy(1,1,ii), pya,nn)
          call CopyVek(sux(1,1,ii),spxa,nn)
          call CopyVek(suy(1,1,ii),spya,nn)
        else if(n.eq.2) then
          call CopyVek( bx(1,1,ii), pxa,nn)
          call CopyVek( by(1,1,ii), pya,nn)
          call CopyVek(sbx(1,1,ii),spxa,nn)
          call CopyVek(sby(1,1,ii),spya,nn)
        else if(n.eq.3) then
          call CopyVek( c3x(1,1,ii), pxa,nn)
          call CopyVek( c3y(1,1,ii), pya,nn)
          call CopyVek(sc3x(1,1,ii),spxa,nn)
          call CopyVek(sc3y(1,1,ii),spya,nn)
        else if(n.eq.4) then
          call CopyVek( c4x(1,1,ii), pxa,nn)
          call CopyVek( c4y(1,1,ii), pya,nn)
          call CopyVek(sc4x(1,1,ii),spxa,nn)
          call CopyVek(sc4y(1,1,ii),spya,nn)
        else if(n.eq.5) then
          call CopyVek( c5x(1,1,ii), pxa,nn)
          call CopyVek( c5y(1,1,ii), pya,nn)
          call CopyVek(sc5x(1,1,ii),spxa,nn)
          call CopyVek(sc5y(1,1,ii),spya,nn)
        else if(n.eq.6) then
          call CopyVek( c6x(1,1,ii), pxa,nn)
          call CopyVek( c6y(1,1,ii), pya,nn)
          call CopyVek(sc6x(1,1,ii),spxa,nn)
          call CopyVek(sc6y(1,1,ii),spya,nn)
        endif
        nn=1
        do k=1,kmodp
          if(TypeModFun(ii).le.1) then
            iw=iabs(TransKwSym(k,ntr))
            snp=snm(k)
            csp=csm(k)
          else
            iw=k
            snp=0.
            csp=1.
          endif
          epsp=eps(k)
          if(n.gt.itf(ii)) then
            if(MagPar(ii).gt.0) then
              call multm (TransM(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
              call multmq(TransM(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
              call multm (TransM(1,ntr,isw), pya(nn), py,NRank,NRank,1)
              call multmq(TransM(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
            endif
          else if(n.eq.0) then
             px(1)= pxa(nn)
             py(1)= pya(nn)
            spx(1)=spxa(nn)
            spy(1)=spya(nn)
          else if(n.eq.1) then
            call multm (TransX(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransX(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            if(kfap.ne.1.or.k.ne.kmodp.or.TypeModFun(ii).gt.1) then
              call multm (TransX(1,ntr,isw), pya(nn), py,NRank,NRank,1)
              call multmq(TransX(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
            endif
          else if(n.eq.2) then
            call multm (TransTemp(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransTemp(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            if(kfap.ne.1.or.k.ne.kmodp.or.TypeModFun(ii).gt.1) then
              call multm (TransTemp(1,ntr,isw), pya(nn), py,NRank,NRank,
     1                    1)
              call multmq(TransTemp(1,ntr,isw),spya(nn),spy,NRank,NRank,
     1                    1)
            endif
          else if(n.eq.3) then
            call multm (TransC3(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransC3(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            call multm (TransC3(1,ntr,isw), pya(nn), py,NRank,NRank,1)
            call multmq(TransC3(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
          else if(n.eq.4) then
            call multm (TransC4(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransC4(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            call multm (TransC4(1,ntr,isw), pya(nn), py,NRank,NRank,1)
            call multmq(TransC4(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
          else if(n.eq.5) then
            call multm (TransC5(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransC5(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            call multm (TransC5(1,ntr,isw), pya(nn), py,NRank,NRank,1)
            call multmq(TransC5(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
          else
            call multm (TransC6(1,ntr,isw), pxa(nn), px,NRank,NRank,1)
            call multmq(TransC6(1,ntr,isw),spxa(nn),spx,NRank,NRank,1)
            call multm (TransC6(1,ntr,isw), pya(nn), py,NRank,NRank,1)
            call multmq(TransC6(1,ntr,isw),spya(nn),spy,NRank,NRank,1)
          endif
          if(kfap.eq.0.or.k.le.kmodp-NDimIp) then
            do j=1,NRank
              ppx=epsp*csp*px(j)-snp*py(j)
              ppy=epsp*snp*px(j)+csp*py(j)
              sppx=abs(epsp*csp*spx(j)-snp*spy(j))
              sppy=abs(epsp*snp*spx(j)+csp*spy(j))
              if(n.gt.itf(ii)) then
                 smx(j,iw,io)=ppx
                 smy(j,iw,io)=ppy
                ssmx(j,iw,io)=sppx
                ssmy(j,iw,io)=sppy
              else if(n.eq.0) then
                ax(iw,io)=ppx
                ay(iw,io)=ppy
                sax(iw,io)=sppx
                say(iw,io)=sppy
              else if(n.eq.1) then
                ux(j,iw,io)=ppx
                uy(j,iw,io)=ppy
                sux(j,iw,io)=sppx
                suy(j,iw,io)=sppy
              else if(n.eq.2) then
                bx(j,iw,io)=ppx
                by(j,iw,io)=ppy
                sbx(j,iw,io)=sppx
                sby(j,iw,io)=sppy
              else if(n.eq.3) then
                c3x(j,iw,io)=ppx
                c3y(j,iw,io)=ppy
                sc3x(j,iw,io)=sppx
                sc3y(j,iw,io)=sppy
              else if(n.eq.4) then
                c4x(j,iw,io)=ppx
                c4y(j,iw,io)=ppy
                sc4x(j,iw,io)=sppx
                sc4y(j,iw,io)=sppy
              else if(n.eq.5) then
                c5x(j,iw,io)=ppx
                c5y(j,iw,io)=ppy
                sc5x(j,iw,io)=sppx
                sc5y(j,iw,io)=sppy
              else
                c6x(j,iw,io)=ppx
                c6y(j,iw,io)=ppy
                sc6x(j,iw,io)=sppx
                sc6y(j,iw,io)=sppy
              endif
            enddo
          else
            kk=k-kmodp+NDimI(KPhase)
            epsp=eps(kk)
            if(n.eq.0) then
              call CopyVek(xorg,xp,3)
              if(NDimI(KPhase).eq.1) then
                delta=a0(ii)*.5
              else
                delta=py(1)*.5
              endif
              call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
              do j=1,2
                if(j.eq.1) then
                  xp(kk+3)=px(1)-delta
                else
                  xp(kk+3)=px(1)+delta
                endif
                call multm(TransMat(1,ntr,isw),xp,xps(1,j),NDim(KPhase),
     1                     NDim(KPhase),1)
                call AddVek(xps(1,j),TransVec(1,ntr,isw),xps(1,j),
     1                      NDim(KPhase))
              enddo
              do j=4,NDim(KPhase)
                delta=abs(xps(j,2)-xps(j,1))
                if(delta.gt..00001) then
                  jj=j+kmodp-NDimI(KPhase)-3
                  go to 2270
                endif
              enddo
2270          pom=(xps(4,2)+xps(4,1))*.5
              j=pom
              if(pom.lt.0.) j=j-1
              ax(jj,io)=pom-float(j)
              sax(jj,io)=sax(k,ii)
              if(NDimI(KPhase).gt.1) then
                ay(jj,io)=delta
                say(jj,io)=say(kk,ii)
              else
                ay(jj,io)=0.
                say(jj,io)=0.
              endif
            else if(n.eq.1) then
              call multm (TransX(1,ntr,isw), ux(1,k,ii), px,3,3,1)
              call multmq(TransX(1,ntr,isw),sux(1,k,ii),spx,3,3,1)
              do j=1,3
                 ux(j,k,io)=epsp* px(j)
                sux(j,k,io)=abs(spx(j))
              enddo
              call CopyVek(x(1,ii),xp,3)
              xp(4)=uy(1,k,ii)
              call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),
     1                   NDim(KPhase),1)
              call AddVek(xpp,TransVec(1,ntr,isw),xp,NDim(KPhase))
              pom=xp(4)
              j=pom
              if(pom.lt.0.) j=j-1
              uy(1,k,io)=pom-float(j)
              uy(2,k,io)=uy(2,k,ii)
              suy(1,k,io)=suy(1,k,ii)
              suy(2,k,io)=suy(2,k,ii)
            else if(n.eq.2) then
              call multm (TransTemp(1,ntr,isw), bx(1,k,ii), px,6,6,1)
              call multmq(TransTemp(1,ntr,isw),sbx(1,k,ii),spx,6,6,1)
              do j=1,3
                bx(j,k,io)=epsp*px(j)
                sbx(j,k,io)=abs(spx(j))
              enddo
              pom=epsp*by(1,k,ii)+xp(4)
              j=pom
              if(pom.lt.0.) j=j-1
              by(1,k,io)=pom-float(j)
              by(2,k,io)=by(2,k,ii)
              sby(1,k,io)=sby(1,k,ii)
              sby(2,k,io)=sby(2,k,ii)
            endif
          endif
          nn=nn+NRank
        enddo
      enddo
      if(NDimI(KPhase).le.0) then
        if(ChargeDensities) then
          NPGAt(io)=NPGAt(ii)
          do j=1,NPGAt(ii)
            call CopyMat(RPGAt(1,j,ii),RPGAt(1,j,io),3)
          enddo
          SmbPGAt(io)=SmbPGAt(ii)
          LocAtSystSt(1,io)=LocAtSystSt(1,ii)
          LocAtSystSt(2,io)=LocAtSystSt(2,ii)
          LocAtSystAx(io)=LocAtSystAx(ii)
          LocAtSense(io)=LocAtSense(ii)
          call CopyVek(LocAtSystX(1,1,ii),LocAtSystX(1,1,io),6)
          call CopyMat(TrAt(1,ii),TrAt(1,io),3)
          call CopyMat(TriAt(1,ii),TriAt(1,io),3)
          call CopyMat(TroAt(1,ii),TroAt(1,io),3)
          call CopyMat(TroiAt(1,ii),TroiAt(1,io),3)
          kapa1(io)=kapa1(ii)
          skapa1(io)=skapa1(ii)
          kapa2(io)=kapa2(ii)
          skapa2(io)=skapa2(ii)
          popc(io)=popc(ii)
          spopc(io)=spopc(ii)
          popv(io)=popv(ii)
          spopv(io)=spopv(ii)
          if(lasmax(ii).gt.1) then
            call CopyVek( popas(1,ii), popas(1,io),lasmax(ii)**2)
            call CopyVek(spopas(1,ii),spopas(1,io),lasmax(ii)**2)
          endif
        endif
        go to 9999
      endif
      OrthoEps(io)=OrthoEps(ii)
      OrthoX40(io)=OrthoX40(ii)
      OrthoDelta(io)=OrthoDelta(ii)
      if(TypeModFun(ii).eq.1.and.kmol(ii).eq.0) then
        xp(4)=OrthoX40(ii)
        call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),NDim(KPhase),
     1             1)
        call AddVek(xpp,TransVec(1,ntr,isw),xp,NDim(KPhase))
        pom=xp(4)
        l=pom
        if(pom.lt.0.) l=l-1
        OrthoX40(io)=pom-float(l)
      endif
9999  if(MagPar(ii).gt.0) then
        KUsePolar(io)=KUsePolar(ii)
        if(KUsePolar(ii).gt.0) then
          do j=0,MagPar(ii)-1
            if(j.eq.0) then
              call Fract2ShpCoor( sm0(1,io))
              call Fract2ShpCoor(ssm0(1,io))
              call Fract2ShpCoor( sm0(1,ii))
              call Fract2ShpCoor(ssm0(1,ii))
            else
              call Fract2ShpCoor( smx(1,j,io))
              call Fract2ShpCoor(ssmx(1,j,io))
              call Fract2ShpCoor( smy(1,j,io))
              call Fract2ShpCoor(ssmy(1,j,io))
              call Fract2ShpCoor( smx(1,j,ii))
              call Fract2ShpCoor(ssmx(1,j,ii))
              call Fract2ShpCoor( smy(1,j,ii))
              call Fract2ShpCoor(ssmy(1,j,ii))
            endif
          enddo
        endif
      endif
      deallocate(snm,csm,eps,pxa,pya,spxa,spya)
      return
      end
      subroutine EM40TrMol(ii,io,isw,ntr,Klic)
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      character*8 MolNamePom
      dimension px(9),py(9),xp(6),xpp(6),phi(3),xps(6,2),
     1          snm(:),csm(:),eps(:),pxa(:),pya(:),spxa(:),
     2          spya(:)
      allocatable snm,csm,eps,pxa,pya,spxa,spya
      n=MaxUsedKw(KPhase)
      allocate(snm(n),csm(n),eps(n),pxa(9*n),pya(9*n),spxa(9*n),
     1         spya(9*n))
      im=(io-1)/mxp+1
      iak=NAtMolFr(1,1)-1
      do i=1,im
        iap=iak+1
        iak=iak+iam(i)/KPoint(i)
      enddo
      if(io.ne.ii) then
        aimol(io)=aimol(ii)
        saimol(io)=saimol(ii)
        LocMolSystType(io)=LocMolSystType(ii)
        TypeModFunMol(io)=TypeModFunMol(ii)
        do i=1,max(2,LocMolSystType(io))
          if(i.gt.LocMolSystType(io)) then
            LocMolSystAx(i,io)='zy'
          else
            LocMolSystAx(i,io)=LocMolSystAx(i,ii)
          endif
          do j=1,2
            LocMolSystSt(j,i,io)=LocMolSystSt(j,i,ii)
            call CopyVek(LocMolSystX(1,j,i,ii),LocMolSystX(1,j,i,io),3)
          enddo
        enddo
        call CopyVek(TrMol(1,ii),TrMol(1,io),9)
        call CopyVek(TriMol(1,ii),TriMol(1,io),9)
        AtTrans(io)=' '
        call CopyVekI(KFM(1,ii),KFM(1,io),3)
        call CopyVekI(KModM(1,ii),KModM(1,io),3)
        aimol(io)=aimol(ii)
        if(NDimI(KPhase).gt.0) then
          phfm(io)=phfm(ii)
          sphfm(io)=sphfm(ii)
        endif
        if(ktls(im).ne.0) then
          call CopyVek(tt(1,ii),tt(1,io),6)
          call CopyVek(tl(1,ii),tl(1,io),6)
          call CopyVek(ts(1,ii),ts(1,io),9)
        endif
      else
        call CopyVek(TrToOrtho(1,isw,KPhase),TrMol(1,io),9)
        call CopyVek(TrToOrthoI(1,isw,KPhase),TriMol(1,io),9)
      endif
      call ShiftKiMol(io,ktls(im),KModM(1,io),KModM(2,io),
     1                KModM(3,io),.false.)
      call CopyVekI(KiMol(1,ii),KiMol(1,io),DelkaKiMolekuly(io))
      RotSign(io)=nint(SignTransX(ntr))*RotSign(ii)
      call AddVek(xm(1,im),trans(1,ii),xp,3)
      call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
      call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),NDim(KPhase),1)
      do j=1,NDim(KPhase)
        xpp(j)=xpp(j)+TransVec(j,ntr,isw)
        if(j.ge.4) phi(j-3)=-xpp(j)*pi2
      enddo
      call CopyVek(STrans(1,ii),STrans(1,io),3)
      if(Klic.eq.0) then
        do i=1,3
          trans(i,io)=xpp(i)-xm(i,im)
        enddo
        call multm(TransX(1,ntr,isw),RotMol(1,ii),px,3,3,3)
        call CopyVek(px,RotMol(1,io),9)
      else
        call multm(TransX(1,ntr,isw),RotMol(1,ii),px,3,3,3)
        call MatInv(TransX(1,ntr,isw),py,pom,3)
        call multm(px,py,RotMol(1,io),3,3,3)
        call multm(TransX(1,ntr,isw),trans(1,ii),px,3,3,1)
        do j=1,3
          trans(j,io)=px(j)+TransVec(j,ntr,isw)
        enddo
      endif
      call MatInv(RotMol(1,io),RotIMol(1,io),pom,3)
      call multm(TrMol(1,io),RotMol(1,io),px,3,3,3)
      call multm(px,TriMol(1,io),py,3,3,3)
      call EM40GetAngles(py,irot(KPhase),euler(1,io))
      call CopyVek(SEuler(1,ii),SEuler(1,io),3)
      call CopyVek(DRotF(1,ii),DRotF(1,io),9)
      call CopyVek(DRotC(1,ii),DRotC(1,io),9)
      call CopyVek(DRotP(1,ii),DRotP(1,io),9)
      call CopyVek(RotB(1,ii),RotB(1,io),36)
      call CopyVek(DRotBF(1,ii),DRotBF(1,io),36)
      call CopyVek(DRotBC(1,ii),DRotBC(1,io),36)
      call CopyVek(DRotBP(1,ii),DRotBP(1,io),36)
      a0m(io)=a0m(ii)
      sa0m(io)=sa0m(ii)
      if(NDimI(KPhase).le.0) go to 9999
      write(Cislo,'(''#'',i5)') mod(ii-1,mxp)+1
      call Zhusti(Cislo)
      MolNamePom=molname(im)(:idel(molname(im)))//Cislo(:idel(Cislo))
      do k=1,MaxUsedKw(KPhase)
        eps(k)=isign(1,TransKwSym(k,ntr))
        l=iabs(TransKwSym(k,ntr))
        pom=0.
        if(TypeModFunMol(io).le.1) then
          do m=1,NDimI(KPhase)
            pom=pom+float(kw(m,l,KPhase))*phi(m)
          enddo
        endif
        snp=sin(pom)
        csp=cos(pom)
        if(abs(snp).lt..001) snp=0.
        if(abs(csp).lt..001) csp=0.
        if(abs(abs(snp)-1.).lt..001) snp=sign(1.,snp)
        if(abs(abs(csp)-1.).lt..001) csp=sign(1.,csp)
        snm(k)=snp
        csm(k)=csp
      enddo
      do n=0,2
        kmodp=KModM(n+1,ii)
        if(kmodp.le.0) cycle
        kfap=KFM(n+1,ii)
        if(n.eq.0) then
          mk=1
        else if(n.eq.1) then
          mk=2
        else if(n.eq.2) then
          mk=3
        endif
        NRank=TRank(n)
        if(kfap.ne.0) then
          if(n.eq.0) then
            NDimIp=NDimI(KPhase)
          else
            NDimIp=1
          endif
        endif
        do m=1,mk
          if(n.eq.2.and.m.eq.3) NRank=9
          nn=1
          do k=1,kmodp
            if(n.eq.0) then
              call CopyVek( axm(k,ii), pxa(nn),NRank)
              call CopyVek( aym(k,ii), pya(nn),NRank)
              call CopyVek(saxm(k,ii),spxa(nn),NRank)
              call CopyVek(saym(k,ii),spya(nn),NRank)
            else if(n.eq.1) then
              if(m.eq.1) then
                if(Klic.eq.0) then
                  call CopyVek( utx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( uty(1,k,ii), pya(nn),NRank)
                  call CopyVek(sutx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(suty(1,k,ii),spya(nn),NRank)
                else
                  call Multm (TransX(1,ntr,isw),utx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm (TransX(1,ntr,isw),uty(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransX(1,ntr,isw),sutx(1,k,ii),spxa(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransX(1,ntr,isw),suty(1,k,ii),spya(nn),
     1                        NRank,NRank,1)
                endif
              else
                if(Klic.eq.0) then
                  call CopyVek( urx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( ury(1,k,ii), pya(nn),NRank)
                  call CopyVek(surx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(sury(1,k,ii),spya(nn),NRank)
                else
                  call Multm (TransX(1,ntr,isw),urx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm (TransX(1,ntr,isw),ury(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransX(1,ntr,isw),surx(1,k,ii),spxa(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransX(1,ntr,isw),sury(1,k,ii),spya(nn),
     1                        NRank,NRank,1)
                endif
              endif
            else if(n.eq.2) then
              if(m.eq.1) then
                if(Klic.eq.0) then
                  call CopyVek( ttx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( tty(1,k,ii), pya(nn),NRank)
                  call CopyVek(sttx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(stty(1,k,ii),spya(nn),NRank)
                else
                  call Multm(TransTemp(1,ntr,isw),ttx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm(TransTemp(1,ntr,isw),tty(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransTemp(1,ntr,isw),sttx(1,k,ii),spxa(nn)
     1                       ,NRank,NRank,1)
                  call MultmQ(TransTemp(1,ntr,isw),stty(1,k,ii),spya(nn)
     1                       ,NRank,NRank,1)
                endif
              else if(m.eq.2) then
                if(Klic.eq.0) then
                  call CopyVek( tlx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( tly(1,k,ii), pya(nn),NRank)
                  call CopyVek(stlx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(stly(1,k,ii),spya(nn),NRank)
                else
                  call Multm(TransTemp(1,ntr,isw),tlx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm(TransTemp(1,ntr,isw),tly(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransTemp(1,ntr,isw),stlx(1,k,ii),
     1                        spxa(nn),NRank,NRank,1)
                  call MultmQ(TransTemp(1,ntr,isw),stly(1,k,ii),
     1                        spya(nn),NRank,NRank,1)
                endif
              else
                if(Klic.eq.0) then
                  call CopyVek( tsx(1,k,ii), pxa(nn),NRank)
                  call CopyVek( tsy(1,k,ii), pya(nn),NRank)
                  call CopyVek(stsx(1,k,ii),spxa(nn),NRank)
                  call CopyVek(stsy(1,k,ii),spya(nn),NRank)
                else
                  call Multm(TransTempS(1,ntr,isw),tsx(1,k,ii),pxa(nn),
     1                        NRank,NRank,1)
                  call Multm(TransTempS(1,ntr,isw),tsy(1,k,ii),pya(nn),
     1                        NRank,NRank,1)
                  call MultmQ(TransTempS(1,ntr,isw),stsx(1,k,ii),
     1                        spxa(nn),NRank,NRank,1)
                  call MultmQ(TransTempS(1,ntr,isw),stsy(1,k,ii),
     1                        spya(nn),NRank,NRank,1)
                endif
              endif
            endif
            nn=nn+nrank
          enddo
          nn=0
          do k=1,kmodp
            iw=iabs(TransKwSym(k,ntr))
            p11= eps(k)*csm(k)
            p21= eps(k)*snm(k)
            p12=-snm(k)
            p22= csm(k)
1100        if(kfap.eq.0.or.k.le.kmodp-NDimIp) then
              do j=1,nrank
                jj=nn+j
                ppx=p11*pxa(jj)+p12*pya(jj)
                ppy=p21*pxa(jj)+p22*pya(jj)
                sppx=abs(p11*spxa(jj)+p12*spya(jj))
                sppy=abs(p21*spxa(jj)+p22*spya(jj))
                if(n.eq.0) then
                  axm(iw,io)=ppx
                  aym(iw,io)=ppy
                  saxm(iw,io)=sppx
                  saym(iw,io)=sppy
                else if(n.eq.1) then
                  if(m.eq.1) then
                     utx(j,iw,io)= ppx
                     uty(j,iw,io)= ppy
                    sutx(j,iw,io)=sppx
                    suty(j,iw,io)=sppy
                  else
                     urx(j,iw,io)= ppx
                     ury(j,iw,io)= ppy
                    surx(j,iw,io)=sppx
                    sury(j,iw,io)=sppy
                  endif
                else if(n.eq.2) then
                  if(m.eq.1) then
                     ttx(j,iw,io)= ppx
                     tty(j,iw,io)= ppy
                    sttx(j,iw,io)=sppx
                    stty(j,iw,io)=sppy
                  else if(m.eq.2) then
                     tlx(j,iw,io)= ppx
                     tly(j,iw,io)= ppy
                    stlx(j,iw,io)=sppx
                    stly(j,iw,io)=sppy
                  else
                     tsx(j,iw,io)= ppx
                     tsy(j,iw,io)= ppy
                    stsx(j,iw,io)=sppx
                    stsy(j,iw,io)=sppy
                  endif
                endif
              enddo
            else
              kk=k-kmodp+NDimI(KPhase)
              epsp=eps(kk)
              if(n.eq.0) then
                if(NDimI(KPhase).eq.1) then
                  delta=a0m(ii)*.5
                else
                  delta=pya(nn+1)*.5
                endif
                call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
                do j=1,2
                  if(j.eq.1) then
                    xp(kk+3)=pxa(nn+1)-delta
                  else
                    xp(kk+3)=pxa(nn+1)+delta
                  endif
                  call multm(TransMat(1,ntr,isw),xp,xps(1,j),
     1                       NDim(KPhase),NDim(KPhase),1)
                  call AddVek(xps(1,j),TransVec(1,ntr,isw),xps(1,j),
     1                        NDim(KPhase))
                enddo
                do j=4,NDim(KPhase)
                  delta=abs(xps(j,2)-xps(j,1))
                  if(delta.gt..00001) then
                    jj=j+kmodp-NDimI(KPhase)-3
                    go to 2270
                  endif
                enddo
2270            pom=(xps(j,2)+xps(j,1))*.5
                j=pom
                if(pom.lt.0.) j=j-1
                axm(jj,io)=pom-float(j)
                saxm(jj,io)=spxa(nn+1)
                if(NDimI(KPhase).gt.1) then
                  aym(jj,io)=delta
                  saym(jj,io)=spxa(nn+1)
                else
                  aym(jj,io)=0.
                  saym(jj,io)=0.
                endif
              else if(n.eq.1) then
                if(m.eq.1) then
                  xp(4)=pya(nn+1)
                  call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),
     1                       NDim(KPhase),1)
                  call AddVek(xpp,TransVec(1,ntr,isw),xpp,NDim(KPhase))
                  pom=xpp(4)
                  j=pom
                  if(pom.lt.0.) j=j-1
                  uty(1,k,io)=pom-float(j)
                  uty(2,k,io)=pya(nn+2)
                  uty(3,k,io)=0.
                  call CopyVek(spya(nn+1),suty(1,k,io),3)
                  do j=1,3
                    jj=nn+j
                    utx(j,k,io)=epsp*pxa(jj)
                  enddo
                  call CopyVek(spxa(nn+1),sutx(1,k,io),3)
                else
                  call SetRealArrayTo( urx(1,k,io),3,0.)
                  call SetRealArrayTo( ury(1,k,io),3,0.)
                  call SetRealArrayTo(surx(1,k,io),3,0.)
                  call SetRealArrayTo(sury(1,k,io),3,0.)
                endif
              endif
            endif
            nn=nn+nrank
          enddo
        enddo
      enddo
      do i=iap,iak
        do n=0,2
          kmodp=KModA(n+1,i)
          if(kmodp.le.0) cycle
          kfap=KFA(n+1,i)
          nrank=TRank(n)
          if(kfap.ne.0) then
            if(n.eq.0) then
              NDimIp=NDimI(KPhase)
            else
              NDimIp=1
            endif
          endif
          nn=kmodp*nrank
          if(n.eq.0) then
            a0(i)=a0(ii)
            call CopyVek( ax(1,i), pxa,nn)
            call CopyVek( ay(1,i), pya,nn)
            call CopyVek(sax(1,i),spxa,nn)
            call CopyVek(say(1,i),spya,nn)
          else if(n.eq.1) then
            call CopyVek( ux(1,1,i), pxa,nn)
            call CopyVek( uy(1,1,i), pya,nn)
            call CopyVek(sux(1,1,i),spxa,nn)
            call CopyVek(suy(1,1,i),spya,nn)
          else if(n.eq.2) then
            call CopyVek( bx(1,1,i), pxa,nn)
            call CopyVek( by(1,1,i), pya,nn)
            call CopyVek(sbx(1,1,i),spxa,nn)
            call CopyVek(sby(1,1,i),spya,nn)
          endif
          nn=0
          do k=1,kmodp
            iw=iabs(TransKwSym(k,ntr))
            snp=snm(k)
            csp=csm(k)
            epsp=eps(k)
            if(kfap.eq.0.or.k.le.kmodp-NDimIp) then
              do j=1,nrank
                jj=nn+j
                ppx=epsp*csp*pxa(jj)-snp*pya(jj)
                ppy=epsp*snp*pxa(jj)+csp*pya(jj)
                sppx=abs(epsp*csp*spxa(jj)-snp*spya(jj))
                sppy=abs(epsp*snp*spxa(jj)+csp*spya(jj))
                if(n.eq.0) then
                  ax(iw,i)=ppx
                  ay(iw,i)=ppy
                  sax(iw,i)=sppx
                  say(iw,i)=sppy
                else if(n.eq.1) then
                  ux(j,iw,i)= ppx
                  uy(j,iw,i)= ppy
                  sux(j,iw,i)=sppx
                  suy(j,iw,i)=sppy
                else if(n.eq.2) then
                  bx(j,iw,i)= ppx
                  by(j,iw,i)= ppy
                  bx(j,iw,i)=sppx
                  by(j,iw,i)=sppy
                endif
              enddo
            else
              kk=k-kmodp+NDimI(KPhase)
              epsp=eps(kk)
              if(n.eq.0) then
                if(NDimI(KPhase).eq.1) then
                  delta=a0(i)*.5
                else
                  delta=pya(nn+1)*.5
                endif
                call SetRealArrayTo(xp(4),NDimI(KPhase),0.)
                do j=1,2
                  if(j.eq.1) then
                    xp(kk+3)=pxa(nn+1)-delta
                  else
                    xp(kk+3)=pxa(nn+1)+delta
                  endif
                  call multm(TransMat(1,ntr,isw),xp,xps(1,j),
     1                       NDim(KPhase),NDim(KPhase),1)
                  call AddVek(xps(1,j),TransVec(1,ntr,isw),xps(1,j),
     1                        NDim(KPhase))
                enddo
                do j=4,NDim(KPhase)
                  delta=abs(xps(j,2)-xps(j,1))
                  if(delta.gt..00001) then
                    jj=j+kmodp-NDimI(KPhase)-3
                    go to 3270
                  endif
                enddo
3270            pom=(xps(j,2)+xps(j,1))*.5
                j=pom
                if(pom.lt.0.) j=j-1
                ax(jj,i)=pom-float(j)
                sax(jj,i)=spxa(nn+1)
                if(NDimI(KPhase).gt.1) then
                  ay(jj,i)=delta
                  say(jj,i)=spxa(nn+1)
                else
                  ay(jj,i)=0.
                  say(jj,i)=0.
                endif
              else if(n.eq.1) then
                xp(4)=pya(nn+1)
                call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),
     1                     NDim(KPhase),1)
                call AddVek(xpp,TransVec(1,ntr,isw),xpp,NDim(KPhase))
                pom=xpp(4)
                j=pom
                if(pom.lt.0.) j=j-1
                uy(1,k,i)=pom-float(j)
                uy(2,k,i)=pya(nn+2)
                uy(3,k,i)=0.
                call CopyVek(spya(nn+1),suy(1,k,i),3)
                do j=1,3
                  jj=nn+j
                  utx(j,k,io)=epsp*pxa(jj)
                enddo
                call CopyVek(spya(nn+1),sux(1,k,i),3)
              endif
            endif
          enddo
          nn=nn+nrank
        enddo
      enddo
      if(TypeModFunMol(ii).eq.1) then
        OrthoEpsMol(io)=OrthoEpsMol(ii)
        OrthoDeltaMol(io)=OrthoDeltaMol(ii)
        xp(4)=OrthoX40Mol(ii)
        call multm(TransMat(1,ntr,isw),xp,xpp,NDim(KPhase),NDim(KPhase),
     1             1)
        call AddVek(xpp,TransVec(1,ntr,isw),xp,NDim(KPhase))
        pom=xp(4)
        l=pom
        if(pom.lt.0.) l=l-1
        OrthoX40Mol(io)=pom-float(l)
      endif
      if(ii.ne.io) TypeModFun(io)=TypeModFun(ii)
9999  deallocate(snm,csm,eps,pxa,pya,spxa,spya)
      return
      end
      subroutine EM40DefMolGroup(BratMol,ich)
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      logical BratMol(mxpm)
      dimension isfm(:)
      allocatable isfm
      allocate(isfm(mxpm))
      call SetLogicalArrayTo(BratMol,mxpm,.false.)
      call SetIntArrayTo(isfm,mxpm,0)
      if(NMolecLenAll(KPhase).gt.0) then
        call EM40SetMolName
        call SelAtoms('Select molecules',MolMenu,BratMol,isfm,MaxMolPos,
     1                ich)
      endif
      deallocate(MolMenu,isfm)
      return
      end
      subroutine EM40SetMolName
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      allocate(MolMenu(mxpm))
      MaxMolPos=0
      do i=NMolecFrAll(KPhase),NMolecToAll(KPhase)
        do j=1,mam(i)
          MaxMolPos=MaxMolPos+1
          if(mam(i).gt.1) then
            write(MolMenu(MaxMolPos),'(a8,''#'',i2)') molname(i),j
            call zhusti(MolMenu(MaxMolPos))
          else
            MolMenu(MaxMolPos)=molname(i)
          endif
        enddo
      enddo
      return
      end
      subroutine EM40MakeMotifs
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      use EditM40_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'editm40.cmn'
      dimension Motif(:),xp(6),xopt(3),kwp(3),kwz(3),GammaIntP(9),
     1          GammaIntPI(9),xpp(6)
      integer GammaInt(9)
      logical PointAlreadyPresent,eqiv
      data dmez/3./
      allocatable Motif
      iauto=0
      go to 2000
      entry EM40MakeMotifsAuto
      iauto=1
      if(allocated(IAtActive))
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      allocate(IAtActive(NAtInd),LAtActive(NAtInd),NameAtActive(NAtInd))
1500  NAtActive=0
      do i=1,NAtInd
        if(kswa(i).eq.KPhase) then
          NAtActive=NAtActive+1
          IAtActive(NAtActive)=i
          LAtActive(NAtActive)=.true.
        endif
      enddo
2000  NTrans=1
      if(allocated(TransMat)) call DeallocateTrans
      call AllocateTrans
      if(allocated(Motif)) deallocate(Motif)
      allocate(Motif(NAtInd))
      call SetIntArrayTo(Motif,NAtInd,0)
      MotifActual=0
3010  MotifActual=MotifActual+1
      nold=0
      n=0
      nuz=0
3050  do i=1,NAtActive
        if(LAtActive(i)) then
          ia=IAtActive(i)
          isfi=isf(ia)
          call SetRealArrayTo(xpp(4),3,0.)
          call CopyVek(x(1,ia),xpp,3)
          if(Motif(ia).le.0) then
            if(n.eq.0) then
              n=n+1
              Motif(ia)=MotifActual
              isw=iswa(ia)
            else
              if(iswa(ia).ne.isw) cycle
              dopt=1.5
              isymopt=1
              icentopt=1
              do j=1,NAtActive
                if(LAtActive(j)) then
                  ja=IAtActive(j)
                  if(iswa(ja).ne.isw) cycle
                  isfj=isf(ja)
                  if(Motif(ja).eq.MotifActual) then
                    if(PointAlreadyPresent(xpp,xp,x(1,ja),1,dmez,.false.
     1                                    ,dist,isym,icent,isw)) then
                      if(TypicalDist(isfi,isfj,KPhase).gt.0.) then
                        pom=dist/TypicalDist(isfi,isfj,KPhase)
                      else
                        pom=dist/(AtRadius(isfi,KPhase)+
     1                            AtRadius(isfj,KPhase))
                      endif
                      if(pom.lt.dopt.and.pom.gt.0.) then
                        dopt=pom
                        isymopt=isym
                        icentopt=icent
                        call CopyVek(xp,xopt,3)
                      endif
                    endif
                  endif
                endif
              enddo
              if(dopt.lt.1.1) then
                n=n+1
                if(isymopt.gt.1) then
                  call CopyVek(s6(1,isymopt,isw,KPhase),
     1                         TransVec(1,1,isw),NDim(KPhase))
                  call AddVek(vt6(1,icentopt,isw,KPhase),
     1                        TransVec(1,1,isw),TransVec(1,1,isw),
     2                        NDim(KPhase))
                  call CopyMat(rm6(1,isymopt,isw,KPhase),
     1                         TransMat(1,1,isw),NDim(KPhase))
                  call EM40SetTr(isw)
                  do j=4,NDim(KPhase)
                    do k=4,NDim(KPhase)
                      GammaIntP(k-3+(j-4)*NDimI(KPhase))=
     1                        TransMat(k+(j-1)*NDim(KPhase),1,isw)
                    enddo
                  enddo
                  call Matinv(GammaIntP,GammaIntPI,pom,NDimI(KPhase))
                  do j=1,NDimI(KPhase)*NDimI(KPhase)
                    GammaInt(j)=nint(GammaIntPI(j))
                  enddo
                  kmodmx=MaxUsedKw(KPhase)
c                  do k=1,7
c                    kmodmx=max(kmodmx,KModA(k,ia))
c                  enddo
                  do 3250j=1,kmodmx
                    call multmi(kw(1,j,KPhase),GammaInt,kwp,1,
     1                          NDimI(KPhase),NDimI(KPhase))
                    call IntVectorToOpposite(kwp,kwz,NDimI(KPhase))
                    do k=1,mxw
                      if(eqiv(kw(1,k,KPhase),kwp,NDimI(KPhase))) then
                        TransKwSym(j,1)= k
                        go to 3250
                      else if(eqiv(kw(1,k,KPhase),kwz,NDimI(KPhase)))
     1                  then
                        TransKwSym(j,1)=-k
                        go to 3250
                      endif
                    enddo
3250              continue
                  call EM40TrAt(ia,ia,isw,1)
                endif
                call CopyVek(xopt,x(1,ia),3)
                Motif(ia)=MotifActual
              endif
            endif
          endif
        endif
      enddo
      if(n.ne.nold) then
        nold=n
        go to 3050
      endif
      if(n.ne.0) go to 3010
      if(iauto.eq.1) call iom40(1,0,fln(:ifln)//'.m40')
      if(allocated(Motif)) deallocate(Motif)
      if(allocated(IAtActive).and.iauto.eq.1)
     1  deallocate(IAtActive,LAtActive,NameAtActive)
      if(allocated(TransMat)) call DeallocateTrans
      return
      end
      subroutine EM40DefMagPolar(ich)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension RMP(9)
      character*80  Veta
      character*12  Names(:)
      integer IPolar(:)
      logical CrwLogicQuest,lpom
      allocatable Names,IPolar
      ich=0
      nn=0
      do i=1,NAtInd
        if(MagPar(i).ne.0) nn=nn+1
      enddo
      allocate(Names(nn),IPolar(nn))
      nn=0
      do i=1,NAtInd
        if(MagPar(i).ne.0) then
          nn=nn+1
          Names(nn)=Atom(i)
        endif
      enddo
      id=NextQuestId()
      ilm=min(nn,10)+1
      Veta='Define atoms using polar coordinates for magnetic moments'
      call FeBoldFont
      xqd=FeTxLength(Veta)+10.
      call FeNormalFont
      call FeQuestCreate(id,-1.,-1.,xqd,ilm,Veta,0,LightGray,0,0)
      xpom=10.
      tpom=xpom+CrwXd+10.
      il=0
      do i=1,nn
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Names(i),'L',CrwXd,
     1                      CrwYd,0,0)
        iat=ktat(NamePolar,NPolar,Names(i))
        if(iat.gt.0) then
          IPolar(i)=1
        else
          IPolar(i)=0
        endif
        call FeQuestCrwOpen(CrwLastMade,iat.gt.0)
        if(i.eq.1) nCrwFirst=CrwLastMade
        if(mod(i,10).eq.0) then
          il=0
          xpom=xpom+60.
          tpom=tpom+60.
        endif
      enddo
      il=-ilm*10-3
      Veta='Select all'
      dpom=FeTxLength(Veta)+10.
      xpom=xqd*.5-dpom-5.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtAll=ButtonLastMade
      Veta='Refresh'
      xpom=xpom+dpom+10.
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,Veta)
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      nButtRefresh=ButtonLastMade
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        lpom=CheckNumber.eq.nButtAll
        nCrw=nCrwFirst
        do i=1,nn
          call FeQuestCrwOpen(nCrw,lpom)
          nCrw=nCrw+1
        enddo
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        NPolar=0
        nCrw=nCrwFirst
        do i=1,nn
          iat=ktat(Atom,NAtInd,Names(i))
          if(CrwLogicQuest(nCrw)) then
            NPolar=NPolar+1
            NamePolar(NPolar)=Names(i)
            if(IPolar(i).eq.0) then
              do j=1,MagPar(iat)
                if(j.eq.1) then
                  call Fract2ShpCoor(sm0(1,iat))
                else
                  call Fract2ShpCoor(smx(1,j-1,iat))
                  call Fract2ShpCoor(smy(1,j-1,iat))
                endif
              enddo
            endif
          else
            if(IPolar(i).ne.0) then
              do j=1,MagPar(iat)
                if(j.eq.1) then
                  call ShpCoor2Fract(sm0(1,iat),RMP)
                else
                  call ShpCoor2Fract(smx(1,j-1,iat),RMP)
                  call ShpCoor2Fract(smy(1,j-1,iat),RMP)
                endif
              enddo
            endif
          endif
          nCrw=nCrw+1
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
9999  Deallocate(Names,IPolar)
      return
      end
      subroutine EM40UpdateAtomLimits
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      NAtInd=0
      NAtPosPlus=NAtCalc-NAtCalcBasic
      do KPh=1,NPhase
        do i=1,NComp(KPh)
          if(KPh.eq.1.and.i.eq.1) then
            NAtIndFr(i,KPh)=1
          else
            NAtIndFr(i,KPh)=NAtInd+1
          endif
          NAtIndTo(i,KPh)=NAtIndFr(i,KPh)+NAtIndLen(i,KPh)-1
          NAtInd=NAtIndTo(i,KPh)
        enddo
      enddo
      NAtPos=0
      do KPh=1,NPhase
        do i=1,NComp(KPh)
          if(KPh.eq.1.and.i.eq.1) then
            NAtPosFr(i,KPh)=NAtInd+1
          else if(i.eq.1) then
            NAtPosFr(i,KPh)=NAtPosFr(i,KPh-1)+NAtPosLen(i,KPh-1)
          else
            NAtPosFr(i,KPh)=NAtPosFr(i-1,KPh)+NAtPosLen(i-1,KPh)
          endif
          NAtPosTo(i,KPh)=NAtPosFr(i,KPh)+NAtPosLen(i,KPh)-1
          NAtPos=NAtPos+NAtPosLen(i,KPh)
        enddo
      enddo
      NAtMol=0
      do KPh=1,NPhase
        do i=1,NComp(KPh)
          if(KPh.eq.1.and.i.eq.1) then
            NAtMolFr(i,KPh)=NAtInd+NAtPos+1
          else if(i.eq.1) then
            NAtMolFr(i,KPh)=NAtMolFr(i,KPh-1)+NAtMolLen(i,KPh-1)
          else
            NAtMolFr(i,KPh)=NAtMolFr(i-1,KPh)+NAtMolLen(i-1,KPh)
          endif
          NAtMolTo(i,KPh)=NAtMolFr(i,KPh)+NAtMolLen(i,KPh)-1
          NAtMol=NAtMol+NAtMolLen(i,KPh)
        enddo
      enddo
      do KPh=1,NPhase
        NAtIndFrAll(KPh)=NAtIndFr(1,KPh)
        NAtPosFrAll(KPh)=NAtPosFr(1,KPh)
        NAtMolFrAll(KPh)=NAtMolFr(1,KPh)
        NAtIndLenAll(KPh)=0
        NAtPosLenAll(KPh)=0
        NAtMolLenAll(KPh)=0
        do i=1,NComp(KPh)
          NAtIndLenAll(KPh)=NAtIndLenAll(KPh)+NAtIndLen(i,KPh)
          NAtPosLenAll(KPh)=NAtPosLenAll(KPh)+NAtPosLen(i,KPh)
          NAtMolLenAll(KPh)=NAtMolLenAll(KPh)+NAtMolLen(i,KPh)
        enddo
        NAtIndToAll(KPh)=NAtIndFr(1,KPh)+NAtIndLenAll(KPh)-1
        NAtPosToAll(KPh)=NAtPosFr(1,KPh)+NAtPosLenAll(KPh)-1
        NAtMolToAll(KPh)=NAtMolFr(1,KPh)+NAtMolLenAll(KPh)-1
      enddo
      NAtCalc=NAtInd+NAtPos+NAtPosPlus
      NAtCalcBasic=NAtInd+NAtPos
      NAtAll=NAtInd+NAtPos+NAtMol
      return
      end
      subroutine EM40UpdateMolecLimits
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      NMolec=0
      do KPh=1,NPhase
        do i=1,NComp(KPh)
          if(KPh.eq.1.and.i.eq.1) then
            NMolecFr(i,KPh)=1
          else
            NMolecFr(i,KPh)=NMolec+1
          endif
          NMolecTo(i,KPh)=NMolecFr(i,KPh)+NMolecLen(i,KPh)-1
          NMolec=NMolec+NMolecLen(i,KPh)
        enddo
      enddo
      do KPh=1,NPhase
        NMolecFrAll(KPh)=NMolecFr(1,KPh)
        NMolecLenAll(KPh)=0
        do i=1,NComp(KPh)
          NMolecLenAll(KPh)=NMolecLenAll(KPh)+NMolecLen(i,KPh)
        enddo
        NMolecToAll(KPh)=NMolecFr(1,KPh)+NMolecLenAll(KPh)-1
      enddo
      return
      end
      subroutine EM40AtomTypeSave
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      character*7 AtTypeOld(:),AtP,AtPOld
      integer Order(:)
      real AtNumOld(:)
      logical EqIgCase
      allocatable AtTypeOld,Order,AtNumOld
      save AtTypeOld,NAtFOld,AtNumOld,Order
      if(NAtFormula(KPhase).gt.0) then
        if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
        allocate(AtTypeMenu(NAtFormula(KPhase)))
        call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                         NAtFormula(KPhase))
      endif
      if(allocated(AtTypeOld)) deallocate(AtTypeOld,Order,
     1                                       AtNumOld)
      NAtFOld=NAtFormula(KPhase)
      allocate(AtTypeOld(NAtFOld),Order(NAtFOld),AtNumOld(NAtFOld))
      do i=1,NAtFOld
        AtTypeOld(i)=AtTypeMenu(i)
        AtNumOld(i)=AtNum(i,KPhase)
      enddo
      go to 9999
      entry  EM40AtomTypeModify
      if(allocated(AtTypeMenu)) deallocate(AtTypeMenu)
      allocate(AtTypeMenu(NAtFormula(KPhase)))
      call EM50SetAtTypeMenu(AtType(1,KPhase),AtTypeMenu,
     1                       NAtFormula(KPhase))
      call SetIntArrayTo(Order,NAtFOld,0)
      do i=1,NAtFOld
        do j=1,NAtFormula(KPhase)
          AtP=AtTypeMenu(j)
          AtPOld=AtTypeOld(i)
          ii=index(AtTypeOld(i),'#')
          jj=index(AtTypeMenu(j),'#')
          if(ii.le.0) then
            if(jj.gt.0) AtP=AtTypeMenu(j)(:jj-1)
          else
            if(jj.le.0) AtPOld=AtTypeOld(i)(:ii-1)
          endif
          if(EqIgCase(AtP,AtPOld)) then
            Order(i)=j
            exit
          endif
        enddo
      enddo
      do i=1,NAtFOld
        if(Order(i).ne.0) cycle
        pom=999999.
        k=0
        do j=1,NAtFormula(KPhase)
          ppp=abs(AtNumOld(i)-AtNum(j,KPhase))
          if(ppp.lt.pom) then
            k=j
            pom=ppp
          endif
        enddo
        Order(i)=k
      enddo
      do i=1,NAtAll
        if(i.gt.NAtInd.and.i.le.NAtCalcBasic) cycle
        if(kswa(i).ne.KPhase) cycle
        j=isf(i)
        isf(i)=Order(j)
      enddo
      if(allocated(AtTypeOld)) deallocate(AtTypeOld,Order,AtNumOld)
9999  return
      end
      subroutine EM40ChangeAtTypeMod(ia,TypeModFunNew,OrthoEpsNew)
      use Basic_mod
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (NPoints=1001)
      dimension xp(:),yp(:),OrthoMatNew(:),OrthoMatNewI(:),
     1          FOld(:,:),FNew(:,:),YOld(:),RMat(:),RSide(:),RSol(:),
     2          Y1Old(:),Y1New(:),kmodp(7),XPom(3),QPom(3)
      character*256 Veta
      integer OrthoSelNew(:),TypeModFunNew
      allocatable xp,yp,OrthoSelNew,OrthoMatNew,OrthoMatNewI,FOld,FNew,
     1            YOld,RMat,RSide,RSol,Y1Old,Y1New
      if(TypeModFunNew.eq.TypeModFun(ia).and.TypeModFunNew.ne.1)
     1   go to 9999
      MaxUsedKwP=0
      do j=1,7
        MaxUsedKwP=max(MaxUsedKwP,KModA(j,ia))
      enddo
      m=max(OrthoOrd,2*MaxUsedKwAll+1)
      allocate(xp(m),yp(m),OrthoSelNew(m),OrthoMatNew(m**2),
     1         OrthoMatNewI(m**2),FOld(m,NPoints),FNew(m,NPoints),
     2         YOld(NPoints),RMat(m*(m+1)),RSide(m),RSol(m),
     3         Y1Old(NPoints),Y1New(NPoints))
      X40=ax(1,ia)
      Delta=a0(ia)
      if(TypeModFunNew.eq.1) then
        if(OrthoOrd.le.0) OrthoOrd=m
        OrthoX40(ia)=X40
        OrthoDelta(ia)=Delta
        call UpdateOrtho(ia,X40,Delta,OrthoEpsNew,OrthoSelNew,
     1                   OrthoOrd)
        call MatOr(X40,Delta,OrthoSelNew,OrthoOrd,OrthoMatNew,
     1             OrthoMatNewI,ia)

      endif
      if(KCommen(KPhase).gt.0) then
        x4=trez(1,1,KPhase)+qcnt(1,ia)
        n=2*NCommQProduct(iswa(ia),KPhase)
        dx4=1./float(n)
        NPointsUsed=n
      else
        NPointsUsed=NPoints
        x4=X40-Delta*.5
        dx4=Delta/float(NPointsUsed-1)
      endif
      nn=0
      x4p=x4
      XPom(1:3)=x(1:3,ia)
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        nn=nn+1
        if(TypeModFun(ia).eq.0) then
          do k=1,2*MaxUsedKwAll+1
            km=k/2
            if(k-1.eq.0) then
              FOld(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FOld(k,m)=sin(Pi2*km*x4)
            else
              FOld(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFun(ia).eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSel(k,ia)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMat(1,ia),yp,FOld(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FOld(1,m),2*MaxUsedKwAll+1,TypeModFun(ia))
!          call GetFPol(pom,FOld(1,m),2*MaxUsedKwP+1,TypeModFun(ia))
        endif
        if(TypeModFunNew.eq.0) then
          do k=1,2*MaxUsedKwAll+1
!          do k=1,2*MaxUsedKwP+1
            km=k/2
            if(k-1.eq.0) then
              FNew(k,m)=1.
            else if(mod(k-1,2).eq.1) then
              FNew(k,m)=sin(Pi2*km*x4)
            else
              FNew(k,m)=cos(Pi2*km*x4)
            endif
          enddo
        else if(TypeModFunNew.eq.1) then
          do k=1,OrthoOrd
            kk=OrthoSelNew(k)
            km=(kk+1)/2
            if(kk.eq.0) then
              yp(k)=1.
            else if(mod(kk,2).eq.1) then
              yp(k)=sin(Pi2*km*x4)
            else
              yp(k)=cos(Pi2*km*x4)
            endif
          enddo
          call MultM(OrthoMatNew,yp,FNew(1,m),OrthoOrd,OrthoOrd,1)
        else
          call GetFPol(pom,FNew(1,m),2*MaxUsedKwAll+1,TypeModFunNew)
        endif
        x4=x4p+float(m)*dx4
      enddo
      if(TypeModFun(ia).eq.1) then
        nn=min(OrthoOrd,nn)
      else
        nn=min(2*MaxUsedKwP+1,nn)
      endif
      RMat=0.
      if(KCommen(KPhase).gt.0) then
        x4=trez(1,1,KPhase)+qcnt(1,ia)
      else
        x4=X40-Delta*.5
      endif
      x4p=x4
      do m=1,NPointsUsed
        pom=x4-X40
        j=pom
        if(pom.lt.0.) j=j-1
        pom=pom-float(j)
        if(pom.gt..5) pom=pom-1.
        pom=2.*pom/Delta
        if(pom.lt.-1.01.or.pom.gt.1.01) then
          x4=x4p+float(m)*dx4
          cycle
        endif
        l=0
        do i=1,nn
          do j=1,i
            l=l+1
            RMat(l)=RMat(l)+FNew(i,m)*FNew(j,m)
          enddo
        enddo
        x4=x4p+float(m)*dx4
      enddo
      call smi(RMat,nn,ISing)
      do it=2,itf(ia)+1
        kmod=KModA(it,ia)
        if(kmod.le.0) cycle
        n=TRank(it-1)
        do i=1,n
          xp=0.
          do k=1,2*kmod+1
            km=k/2
            if(k.eq.1) then
              if(it.eq.2) then
                xp(k)=x(i,ia)
              else if(it.eq.3) then
                xp(k)=beta(i,ia)
              else if(it.eq.4) then
                xp(k)=c3(i,ia)
              else if(it.eq.5) then
                xp(k)=c4(i,ia)
              else if(it.eq.6) then
                xp(k)=c5(i,ia)
              else if(it.eq.7) then
                xp(k)=c6(i,ia)
              endif
            else if(mod(k,2).eq.0) then
              if(it.eq.2) then
                xp(k)=ux(i,km,ia)
              else if(it.eq.3) then
                xp(k)=bx(i,km,ia)
              else if(it.eq.4) then
                xp(k)=c3x(i,km,ia)
              else if(it.eq.5) then
                xp(k)=c4x(i,km,ia)
              else if(it.eq.6) then
                xp(k)=c5x(i,km,ia)
              else if(it.eq.7) then
                xp(k)=c6x(i,km,ia)
              endif
            else
              if(it.eq.2) then
                xp(k)=uy(i,km,ia)
              else if(it.eq.3) then
                xp(k)=by(i,km,ia)
              else if(it.eq.4) then
                xp(k)=c3y(i,km,ia)
              else if(it.eq.5) then
                xp(k)=c4y(i,km,ia)
              else if(it.eq.6) then
                xp(k)=c5y(i,km,ia)
              else if(it.eq.7) then
                xp(k)=c6y(i,km,ia)
              endif
            endif
          enddo
          RSide=0.
          if(KCommen(KPhase).gt.0) then
            x4=trez(1,1,KPhase)+qcnt(1,ia)
          else
            x4=X40-Delta*.5
          endif
          x4p=x4
          do m=1,NPointsUsed
            pom=x4-X40
            j=pom
            if(pom.lt.0.) j=j-1
            pom=pom-float(j)
            if(pom.gt..5) pom=pom-1.
            pom=2.*pom/Delta
            if(pom.lt.-1.01.or.pom.gt.1.01) then
              x4=x4p+float(m)*dx4
              cycle
            endif
            call MultM(FOld(1,m),xp,YOld(m),1,nn,1)
            if(it.eq.2.and.i.eq.1) Y1Old(m)=YOld(m)
            do j=1,nn
              RSide(j)=RSide(j)+YOld(m)*FNew(j,m)
            enddo
            x4=x4p+float(m)*dx4
          enddo
          RSol=0.
          call nasob(RMat,RSide,RSol,nn)
          do k=1,2*kmod+1
            km=k/2
            if(k.eq.1) then
              if(it.eq.2) then
                x(i,ia)=RSol(k)
              else if(it.eq.3) then
                beta(i,ia)=RSol(k)
              else if(it.eq.4) then
                c3(i,ia)=RSol(k)
              else if(it.eq.5) then
                c4(i,ia)=RSol(k)
              else if(it.eq.6) then
                c5(i,ia)=RSol(k)
              else if(it.eq.7) then
                c6(i,ia)=RSol(k)
              endif
            else if(mod(k,2).eq.0) then
              if(it.eq.2) then
                ux(i,km,ia)=RSol(k)
              else if(it.eq.3) then
                bx(i,km,ia)=RSol(k)
              else if(it.eq.4) then
                c3x(i,km,ia)=RSol(k)
              else if(it.eq.5) then
                c4x(i,km,ia)=RSol(k)
              else if(it.eq.6) then
                c5x(i,km,ia)=RSol(k)
              else if(it.eq.7) then
                c6x(i,km,ia)=RSol(k)
              endif
            else
              if(it.eq.2) then
                uy(i,km,ia)=RSol(k)
              else if(it.eq.3) then
                by(i,km,ia)=RSol(k)
              else if(it.eq.4) then
                c3y(i,km,ia)=RSol(k)
              else if(it.eq.5) then
                c4y(i,km,ia)=RSol(k)
              else if(it.eq.6) then
                c5y(i,km,ia)=RSol(k)
              else if(it.eq.7) then
                c6y(i,km,ia)=RSol(k)
              endif
            endif
          enddo
        enddo
      enddo
      TypeModFun(ia)=TypeModFunNew
      if(TypeModFunNew.eq.1) then
        if(.not.allocated(OrthoSel))
     1    allocate(OrthoSel(OrthoOrd,NAtAll+1))
        m=OrthoOrd**2
        if(.not.allocated(OrthoMat))
     1     allocate(OrthoMat(m,NAtAll+1),OrthoMatI(m,NAtAll+1))
        call CopyVekI(OrthoSelNew,OrthoSel(1,ia),OrthoOrd)
        call CopyMat(OrthoMatNew ,OrthoMat (1,ia),OrthoOrd)
        call CopyMat(OrthoMatNewI,OrthoMatI(1,ia),OrthoOrd)
        OrthoX40(ia)=X40
        OrthoDelta(ia)=Delta
        OrthoEps(ia)=OrthoEpsNew
        call trortho(0)
!        x(1:3,ia)=XPom(1:3)
        MaxUsedKwAll=0
        do KPh=1,NPhase
          KPhase=KPh
          MaxUsedKw(KPh)=0
          kmodp=0
          do i=1,NAtCalc
            if(kswa(i).ne.KPh.or.NDim(KPh).le.3) cycle
            do j=1,7
              MaxUsedKw(KPh)=max(MaxUsedKw(KPh),KModA(j,i))
              kmodp(j)=max(kmodp(j),KModA(j,i))
            enddo
            MaxUsedKw(KPh)=max(MaxUsedKw(KPh),MagPar(i)-1)
          enddo
          MaxUsedKwAll=max(MaxUsedKwAll,MaxUsedKw(KPh))
        enddo
        call trortho(1)
        if(Allocated(KWSym)) deallocate(KwSym)
        allocate(KWSym(MaxUsedKwAll,MaxNSymm,MaxNComp,NPhase))
        call SetSymmWaves
      else
        do i=1,3
          XPom(i)=x(i,ia)-XPom(i)
        enddo
        call qbyx(XPom,QPom,iswa(ia))
        ax(1,ia)=ax(1,ia)+QPom(1)
        X40=ax(1,ia)
      endif
      call SetRealArrayTo(xp,3,0.)
      xp(1)=X40
      call SpecPos(x(1,ia),xp,1,iswa(ia),.2,nocc)
      Veta=fln(:ifln)//'_pom.tmp'
      KPhaseIn=KPhase
      nvai=0
      neq=0
      neqs=0
      LstOpened=.true.
      uloha='Symmetry restrictions'
      call OpenFile(lst,Veta,'formatted','unknown')
      call NewPg(1)
      call MagParToCell(0)
      if(kmol(ia).gt.0) then
        im=kmol(ia)
        ji=(im-1)/mxp+1
        nap=NAtPosFrAll(KPhase)
        do i=1,im-1
          nap=nap+iam(i)*mam(i)
        enddo
      else
        im=0
        ji=0
        nap=0
      endif
      call atspec(ia,ia,ji,im,nap)
      call RefAppEq(0,0,0)
      call MagParToBohrMag(0)
      call CloseListing
      LstOpened=.false.
      call DeleteFile(Veta)
      deallocate(xp,yp,OrthoSelNew,OrthoMatNew,OrthoMatNewI,FOld,FNew,
     1           YOld,RMat,RSide,RSol,Y1Old,Y1New)
9999  return
      end

