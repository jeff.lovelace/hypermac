      subroutine MEMExport(KeyText)
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'memexport.cmn'
      character*80 t80
      character*256 KeyText
      logical CrwLogicQuest
      real xqd
      logical ExistFile
      if(NPhase.gt.1) then
        KPhaseIn=KPhase
        il=NPhase
        id=NextQuestId()
        xqd=200.
        t80='Select the phase for MEM export:'
        call FeQuestCreate(id,-1.,-1.,xqd,il,t80,0,LightGray,0,0)
        xpom=5.
        tpom=xpom+CrwgXd+15.
        do il=1,NPhase
          call FeQuestCrwMake(id,tpom,il,xpom,il,PhaseName(il),'L',
     1                        CrwXd,CrwYd,0,1)
          if(il.eq.1) nCrwPhase=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,il.eq.KPhase)
        enddo
1500    call FeQuestEvent(id,ich)
        if(CheckType.ne.0) then
          call NebylOsetren
          go to 1500
        endif
        if(ich.eq.0) then
          do il=1,NPhase
            if(CrwLogicQuest(il)) then
              KPhase=il
              exit
            endif
          enddo
        endif
        call FeQuestRemove(id)
        if(ich.ne.0) go to 9999
      endif
      StringKey=KeyText
      if (StringKey.eq.' ') then
        call MEMPresets(0)
      else
        MEMName=StringKey
        StringKey=' '
      endif
      if (ExistFile(MEMName)) call NactiMEM
      xqd=500.
      linecnt=max(12,10+NDim(KPhase)/3)
      call FeKartCreate(-1.,-1.,xqd,linecnt,'Create MEM file',0,0)
      call FeCreateListek('General',1)
      KartIdGeneral=KartLastId
      call MEMGeneralMake(KartIdGeneral)
      call MEMGeneralOpen
      call FeCreateListek('MEM',1)
      KartIdMEM=KartLastId
      call MEMMEMMake(KartIdMEM)
      call MEMMEMOpen
      call FeCreateListek('Charge flip',1)
      KartIdCF=KartLastId
      call MEMCFMake(KartIdCF)
      call MEMCFOpen
      call FeCreateListek('Prior',1)
      KartIdPrior=KartLastId
      call MEMPriorMake(KartIdPrior)
      call MEMPriorOpen
      call FeCreateListek('EDMA',1)
      KartIdEDMA=KartLastId
      call MEMEDMAMake(KartIdEDMA)
      call MEMEDMAOpen
      call FeCompleteKart(1)
2500  ErrFlag=0
3000  call FeQuestEvent(KartId,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOk) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        if(KartId.eq.KartIdGeneral) then
          call MEMGeneralUpdate
        else if(KartId.eq.KartIdMEM) then
          call MEMMEMUpdate
        else if(KartId.eq.KartIdCF) then
          call MEMCFUpdate
        else if(KartId.eq.KartIdPrior) then
          call MEMPriorUpdate
        else if(KartId.eq.KartIdEDMA) then
          call MEMEDMAUpdate
        endif
        call MEMGeneralComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdGeneral-1)
          goto 3000
        endif
        call MEMMEMComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdMEM-1)
          goto 3000
        endif
        call MEMCFComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdCF-1)
          goto 3000
        endif
        call MEMPriorComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdPrior-1)
          goto 3000
        endif
        call MEMEDMAComplete(okcheck)
        if (okcheck.ne.0) then
          call FePrepniListek(KartIdEDMA-1)
          goto 3000
        endif
        call WriteMEMFile
      else if(CheckType.eq.EventKartSw) then
        if(KartId.eq.KartIdGeneral) then
          call MEMGeneralUpdate
        else if(KartId.eq.KartIdMEM) then
          call MEMMEMUpdate
        else if(KartId.eq.KartIdCF) then
          call MEMCFUpdate
        else if(KartId.eq.KartIdPrior) then
          call MEMPriorUpdate
        else if(KartId.eq.KartIdEDMA) then
          call MEMEDMAUpdate
        endif
        go to 3000
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdGeneral) then
          call MEMGeneralCheck(ich)
        else if(KartId.eq.KartIdMEM) then
          call MEMMEMCheck
        else if(KartId.eq.KartIdCF) then
          call MEMCFCheck
        else if(KartId.eq.KartIdPrior) then
          call MEMPriorCheck
        else if(KartId.eq.KartIdEDMA) then
          call MEMEDMACheck(KartIdEdma)
        endif
        if (ich.eq.0) go to 3000
      end if
      call FeDestroyKart
      KeyText=StringKey
      if(allocated(AtBer)) deallocate(AtBer)
      if(allocated(AtBerEDMA)) deallocate(AtBerEDMA)
      if(NPhase.gt.1) KPhase=KPhaseIn
9999  return
      end
      subroutine MEMPresets(klic)
      use Basic_mod
      use Atoms_mod
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      parameter (eps=0.00001)
      real  ddiv,mindif,sh,minnel,maxnel
      integer n2max,n3max,n2b,n3b,cv,os,symfac(6),mxsymfac(6),klic
      MEMTitle=fln(1:ifln)
      if (klic.eq.1) then
        MEMName=fln(1:ifln)//'.inflip'
        MEMOutFile=fln(1:ifln)//'.m81 '//fln(1:ifln)//'.m80'
      else
        MEMName=fln(1:ifln)//'.BayMEM'
        MEMOutFile=fln(1:ifln)//'.m81'
      endif
      MEMOutPrior=fln(1:ifln)//'_prior.m81'
      k=0
      call kus(MEMOutFile,k,MEMInEDMA)
      MEMOutEDMA=fln(:ifln)
c!    Find the factors in the pixel division given by the symmetry
      do 400 cv=1,NLattVec(KPhase)
        do 500 os=1,NSymmN(KPhase)
          do 700 i=1,NDim(KPhase)
            sh=vt6(i,cv,1,KPhase)+s6(i,os,1,KPhase)
            symfac(i)=0
            mxsymfac(i)=0
800         symfac(i)=symfac(i)+1
            dummy=sh*float(symfac(i))
            if (abs(dummy-nint(dummy)).ge.eps) goto 800
            if (symfac(i).gt.mxsymfac(i)) mxsymfac(i)=symfac(i)
700       continue
500     continue
400   continue
      do 3100 i=1,3
        if (klic.lt.2) then
          ddiv=CellPar(i,1,KPhase)/.2
        elseif(klic.eq.2) then
          ddiv=CellPar(i,1,KPhase)/.08
        endif
        mindif=ddiv+2.
        n2max=int(log(ddiv)/log(2.))+1
        n3max=int(log(ddiv)/log(3.))+1
        do 3110 j2=0,n2max
          do 3120 j3=0,n3max
            dummy=float(2**j2*3**j3*mxsymfac(i))
            if (abs(dummy-ddiv).lt.mindif) then
              mindif=abs(dummy-ddiv)
              n2b=j2
              n3b=j3
            endif
3120      continue
3110    continue
        MEMDivision(i)=2**n2b*3**n3b*mxsymfac(i)
3100  continue
      do 3200 i=4,NDim(KPhase)
        MEMDivision(i)=16
3200  continue
      IncludeRefs=.true.
      RefsTypeCode=Refsobs
      if (klic.eq.0.or.klic.eq.2)then
        PerformCode=PerformMEM
        RefsSourceCode=Refsm80
        IncludeMEM=.true.
        IncludeCF=.false.
        IncludeEDMA=.false.
      elseif(klic.eq.1)then
        PerformCode=PerformCF
        IncludeRefs=.true.
        RefsSourceCode=Refsm90
        IncludeMEM=.false.
        IncludeCF=.true.
        IncludeEDMA=.true.
      endif
      if (klic.ne.2) then
        IncludePrior=.false.
      else
        IncludePrior=.true.
      endif

      MEMAlgCode=1
      MEMConOrder=2
      MEMConWeight=0


c!    Charge Flipping
      MEMCFDelta=1.
      MEMCFRelDelta=.false.
      AutoDelta=.true.
      MEMCFWeak=0.
      Biso=0.
      AutoRS=.true.
      MEMCFRS=1000
      CFSymCode=AverageSym
      MEMAim=1.
      MEMLambda=0.
      MEMRate=1.
      if(allocated(AtBer)) deallocate(AtBer)
      allocate(AtBer(NAtInd))
      call SetLogicalArrayTo(AtBer,NAtInd,.true.)
      if(klic.eq.2)then
        PriorCode=FromFile
      else
        PriorCode=Flat
      endif
      TwoChannel=.false.
      PriorFile=fln(1:ifln)//'_prior.m81'
      PriorFmt='jana'
      PriorSF=.false.
      PDCSinThMin=0.0
      PDCSinThMax=1.5
      PDCSigma=0.01
      PDCMaxSat=0
c!    for EDMA
      EDmaxima=all
      EDm40file=fln(1:ifln)//'.m40'
      EDtolerance=0.15
      EDposition=absol
      EDcharge=.true.
      if(klic.eq.1) then
        EDwrm40=.true.
        if(NAtFormula(KPhase).gt.0) then
          minnel=100
          maxnel=0
          do 3500i=1,NAtFormula(KPhase)
            minnel=min(minnel,AtNum(i,KPhase))
            maxnel=max(maxnel,AtNum(i,KPhase))
3500      continue
          EDchlimlist=0.3*minnel/maxnel
        else
          EDchlimlist=0.03
        endif
        AbsChlim=.false.
      else
        EDwrm40=.false.
        EDchlimlist=0.0
        AbsChlim=.true.
      endif
      EDchlimint=0.25
      EDplim=0.3
c      EDplim=1.5
      EDscale=fract
      EDprojection=.true.
      EDaddb=0.2
      EDfullcell=.false.
      do 4000 i=1,NDimI(KPhase)
        tstart(i)=0.0
        tstep(i)=0.1
        tend(i)=1.0
4000  continue
      if (klic.gt.0) then
        call FeDeferOutput
        call FePrepniListek(KartIdMEM-1)
        call MEMMEMOpen
        call FePrepniListek(KartIdCF-1)
        call MEMCFOpen
        call FePrepniListek(KartIdPrior-1)
        call MEMPriorOpen
        call FePrepniListek(KartIdEDMA-1)
        call MEMEDMAOpen
        call FePrepniListek(KartIdGeneral-1)
        call MEMGeneralOpen
        call FeReleaseOutput
      endif
9999  continue
      return
      end
      subroutine NactiMEM
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      parameter (nmemkwd=33)
      integer tit,voxel,perform,
     1           outputfile,fbegin,twoch,
     2           algorithm,conorder,conweight,
     3           initialdensity,initialfile,prsf,
     4           delta,weakratio,randomseed,
     5           searchsymmetry,outputprior,inputfile,
     6           outputbase,maxima,tolerance,plimit,
     7           chlimit,chlimlist,scale,coc,
     8           projection,writem40,tlist,addborder,
     9           position,fullcell,ibiso
      parameter (tit=1,voxel=2,perform=3,
     1           outputfile=4,fbegin=5,twoch=6,
     2           algorithm=7,conorder=8,conweight=9,
     3           initialdensity=10,initialfile=11,prsf=12,
     4           delta=13,weakratio=14,randomseed=15,
     5           searchsymmetry=16,outputprior=17,inputfile=18,
     6           outputbase=19,maxima=20,tolerance=21,plimit=22,
     7           chlimit=23,chlimlist=24,scale=25,coc=26,
     8           projection=27,writem40=28,tlist=29,addborder=30,
     9           position=31,fullcell=32,ibiso=33)
      integer bmf,NextLogicNumber,k
      real rpom(1)
      character*132 buffer,memkwdval(nmemkwd)
      character*18 memkwd(nmemkwd)
      integer memkwdln(nmemkwd)
      data memkwd /'title','voxel','perform','outputfile','fbegin',
     1             '2channel','algorithm','conorder','conweight',
     2             'initialdensity','initialfile','priorsf','delta',
     3             'weakratio','randomseed','searchsymmetry',
     4             'outputprior','inputfile','outputbase','maxima',
     5             'tolerance','plimit','chlimit','chlimlist','scale',
     6             'centerofcharge','projection','writem40','tlist',
     7             'addborder','position','fullcell','Biso'/
      data memkwdln/5,5,7,10,6,8,9,8,9,14,11,7,5,9,10,14,11,9,10,6,9,
     1              6,7,9,5,14,10,8,5,9,8,8,4/
      IncludeMEM=.false.
      IncludeCF=.false.
      IncludePrior=.false.
      do 900i=1,nmemkwd
        memkwdval(i)='not found'
900   continue
      bmf=NextLogicNumber()
      call OpenFile(bmf,MEMName,'formatted','unknown')
1000  read(bmf,FormA,end=8000) buffer
      do 2000 i=1,nmemkwd
      if(buffer(:memkwdln(i)).eq.memkwd(i)(:memkwdln(i))) then
        memkwdval(i)(1:)=buffer(memkwdln(i)+2:)
        if (idel(memkwdval(i)).eq.0) then
          if (i.eq.tlist) then
            do 1050 j=1,NDimI(KPhase)
              read(bmf,*) tstart(j),tend(j),tstep(j)
1050        continue
          endif
          goto 1000
        endif
1100    if (memkwdval(i)(1:1).eq.' ') then
          memkwdval(i)(1:)=memkwdval(i)(2:)
          goto 1100
        else
          goto 2000
        endif
      endif
2000  continue
      goto 1000
8000  continue
      close(bmf)
      if(memkwdval(tit).ne.'not found') then
        read(memkwdval(tit),FormA) MEMTitle
      endif
      if(memkwdval(voxel).ne.'not found') then
        read(memkwdval(voxel),*) (MEMDivision(i),i=1,NDim(KPhase))
      endif
      if(memkwdval(perform).ne.'not found') then
        if(memkwdval(perform)(:idel(memkwdval(perform))).eq.'CF')
     1    PerformCode=1
        if(memkwdval(perform)(:idel(memkwdval(perform))).eq.'MEM')
     1    PerformCode=2
        if(memkwdval(perform)(:idel(memkwdval(perform))).eq.'CF+MEM')
     1    PerformCode=3
      endif
      if(memkwdval(outputfile).ne.'not found') then
        read(memkwdval(outputfile),FormA) MEMOutFile
      endif
      if(memkwdval(outputprior).ne.'not found') then
        IncludePrior=.true.
        read(memkwdval(outputprior),FormA) MEMOutPrior
      endif
      if(memkwdval(fbegin).ne.'not found') then
        IncludeRefs=.true.
      endif
      if(memkwdval(twoch).ne.'not found') then
        IncludeMEM=.true.
        if(memkwdval(twoch)(:idel(memkwdval(twoch))).eq.'yes')
     1    TwoChannel=.true.
        else
          TwoChannel=.false.
      endif
      if(memkwdval(algorithm).ne.'not found') then
        IncludeMEM=.true.
        if (memkwdval(algorithm)(:3).eq.'S-S')then
          MEMAlgCode=SaSa
          memkwdval(algorithm)(1:)=memkwdval(algorithm)(5:)
          if (idel(memkwdval(algorithm)).ne.0) then
            if (memkwdval(algorithm)(1:4).eq.'AUTO') then
              MEMLambda=0.0
              read(memkwdval(algorithm)(6:),*) MEMAim
            else
              read(memkwdval(algorithm),*) MEMLambda,MEMAim
            endif
          endif
        elseif(memkwdval(algorithm)(:6).eq.'MEMSys')then
          MEMAlgCode=MEMSys
          if (idel(memkwdval(algorithm)(7:)).ne.0)
     1      read(memkwdval(algorithm)(7:),*) i,i,MEMAim,MEMRate,dm
        endif
      endif
      if(memkwdval(conorder).ne.'not found') then
        IncludeMEM=.true.
        read(memkwdval(conorder),*) MEMConOrder
      endif
      if(memkwdval(conweight).ne.'not found') then
        IncludeMEM=.true.
        read(memkwdval(conweight)(2:),*) MEMConWeight
      endif
      if(memkwdval(initialdensity).ne.'not found') then
        IncludeMEM=.true.
        read(memkwdval(initialdensity),FormA) PriorFmt
        if (PriorFmt(:idel(PriorFmt)).eq.'flat')then
          PriorCode=Flat
          PriorFmt='jana'
        else
          PriorCode=FromFile
        endif
      endif
      if(memkwdval(initialfile).ne.'not found') then
        IncludeMEM=.true.
        read(memkwdval(initialfile),FormA) PriorFile
      endif
      if(memkwdval(prsf).ne.'not found') then
        IncludeMEM=.true.
        PriorSF=.true.
        if (NDim(KPhase).gt.3) then
          read(memkwdval(prsf),*)
     1         PDCSinThMin,PDCSinThMax,PDCSigma,PDCMaxSat
        else
          read(memkwdval(prsf),*)
     1         PDCSinThMin,PDCSinThMax,PDCSigma
        endif
      endif
      if(memkwdval(delta).ne.'not found') then
        memkwdval(delta)=memkwdval(delta)//'   '
        if (memkwdval(delta)(1:4).eq.'AUTO') then
          AutoDelta=.true.
        else
          AutoDelta=.false.
          read(memkwdval(delta),*) MEMCFDelta,Buffer
          if(Buffer.eq.'absolute') then
            MEMCFRelDelta=.false.
          elseif (Buffer.eq.'fraction') then
            MEMCFRelDelta=.true.
          endif
        endif
        IncludeCF=.true.
      endif
      if(memkwdval(weakratio).ne.'not found') then
        IncludeCF=.true.
        read(memkwdval(weakratio),*) MEMCFWeak
      endif
      if(memkwdval(weakratio).ne.'not found') then
        IncludeCF=.true.
        read(memkwdval(weakratio),*) Biso
      endif
      if(memkwdval(randomseed).ne.'not found') then
        IncludeCF=.true.
        if (memkwdval(randomseed)(1:4).eq.'AUTO') then
          AutoRS=.true.
        else
          AutoRS=.false.
          read(memkwdval(randomseed),*) MEMCFRS
        endif
      endif
      if(memkwdval(searchsymmetry).ne.'not found') then
        IncludeCF=.true.
        if(memkwdval(searchsymmetry)(1:5).eq.'shift') then
          CFSymCode=RecoverSym
        elseif(memkwdval(searchsymmetry)(1:7).eq.'average') then
          CFSymCode=AverageSym
        else
          CFSymCode=NoSym
        endif
      endif
c! EDMA
      if(memkwdval(inputfile).ne.'not found') then
        IncludeEDMA=.true.
        read(memkwdval(inputfile),FormA) MEMInEDMA
      endif
      if(memkwdval(outputbase).ne.'not found') then
        IncludeEDMA=.true.
        read(memkwdval(outputbase),FormA) MEMOutEDMA
      endif
      if(memkwdval(maxima).ne.'not found') then
        IncludeEDMA=.true.
        if(memkwdval(maxima).eq.'atoms') then
          EDmaxima=atms
        elseif(memkwdval(maxima).eq.'all') then
          EDmaxima=all
        endif
      endif
      if(memkwdval(scale).ne.'not found') then
        if(memkwdval(scale).eq.'angstrom') then
          EDscale=angst
        elseif(memkwdval(scale).eq.'fractional') then
          EDscale=fract
        endif
      endif
      if(memkwdval(writem40).ne.'not found') then
        IncludeEDMA=.true.
        EDm40file=memkwdval(writem40)(:idel(memkwdval(writem40)))
      endif
      if(memkwdval(tolerance).ne.'not found') then
        read(memkwdval(tolerance),*) EDtolerance
      endif
      if(memkwdval(position).ne.'not found') then
        if(memkwdval(position).eq.'absolute') then
          EDposition=absol
        elseif(memkwdval(position).eq.'relative') then
          EDposition=relat
        endif
      endif
      if(memkwdval(coc).ne.'not found') then
        if(memkwdval(coc).eq.'yes') then
          EDcharge=.true.
        else
          EDcharge=.false.
        endif
      endif
      if(memkwdval(fullcell).ne.'not found') then
        if(memkwdval(fullcell).eq.'yes') then
          EDfullcell=.true.
        else
          EDfullcell=.false.
        endif
      endif
      if(memkwdval(plimit).ne.'not found') then
        read(memkwdval(plimit),*) EDplim
      endif
      if(memkwdval(chlimit).ne.'not found') then
        read(memkwdval(chlimit),*) EDchlimint
      endif
      if(memkwdval(chlimlist).ne.'not found') then
        IncludeEDMA=.true.
        k=0
        call StToReal(memkwdval(chlimlist),k,rpom,1,.false.,ich)
        EDchlimlist=rpom(1)
        if (k.lt.len(memkwdval(chlimlist)))then
          call Kus(memkwdval(chlimlist),k,Buffer)
          if(Buffer.eq.'absolute') then
            AbsChlim=.true.
          elseif (Buffer.eq.'relative') then
            AbsChlim=.false.
          endif
        endif
      endif
      if(memkwdval(addborder).ne.'not found') then
        read(memkwdval(addborder),*) EDaddb
      endif
      if(memkwdval(projection).ne.'not found') then
        if(memkwdval(projection).eq.'yes') then
          EDprojection=.true.
        else
          EDprojection=.false.
        endif
      endif
      if(memkwdval(coc).ne.'not found') then
        if(memkwdval(coc).eq.'yes') then
          EDcharge=.true.
        else
          EDcharge=.false.
        endif
      endif
9999  continue
      return
      end
      subroutine MEMGeneral
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical   CrwLogicQuest,ExistFile,FeYesNo,MEMCheckDivision
      integer vyskoc
      save nEdwTitle,nEdwName,nEdwDivision,nEdwOutFile,
     1     nCrwPerformCF,nCrwPerformMEM,nCrwPerformCFMEM,
     2     nCrwIncludeRefs,nCrwRefsm80,nCrwRefsm90,nCrwRefsobs,
     3     nCrwRefscalc,nButtStructSol,nButtMEMwithPR
      character*256 EdwStringQuest,t256
      real xqd

      entry MEMGeneralMake(id)
      xqd=QuestXMax(id)-QuestXMin(id)
      il=1
      call FeQuestEdwMake(id,5.,il,80.,il,'%Title','L',xqd-85.,EdwYd,0)
      nEdwTitle=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,80.,il,'%File name','L',xqd-85.,
     1                    EdwYd,1)
      nEdwName=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,80.,il,'%Pixel Division','L',
     1                    xqd-85.,EdwYd,0)
      nEdwDivision=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,80.,il,'%Output file','L',
     1                    xqd-85.,EdwYd,0)
      nEdwOutFile=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,5.,il,'Perform:','L','B')
      call FeQuestLblOn(LblLastMade)
      il=il+1
      call FeQuestCrwMake(id,5.,il,120.,il,'Charge flipping:','L',
     1                    CrwgXd,CrwgYd,0,1)
      nCrwPerformCF=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,120.,il,'MEM:','L',CrwgXd,
     1                    CrwgYd,0,1)
      nCrwPerformMEM=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,120.,il,'Charge flipping + MEM:','L',
     1                    CrwgXd,CrwgYd,0,1)
      nCrwPerformCFMEM=CrwLastMade

      il=il-3
      call FeQuestCrwMake(id,xqd*.5,il,xqd*.5+150.,il,
     1                    'Include structure factors:','L',CrwXd,CrwYd,
     2                    1,0)
      nCrwIncludeRefs=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,xqd*.5,il,xqd*.5+100.,il,'Phased (m80)',
     1                    'L',CrwgXd,CrwgYd,1,5)
      nCrwRefsm80=CrwLastMade
      call FeQuestCrwMake(id,xqd*.5+155.,il,xqd*.5+180.,il,'Obs','L',
     1                    CrwgXd,CrwgYd,0,6)
      nCrwRefsobs=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,xqd*.5,il,xqd*.5+100.,il,'Unphased (m90)',
     1                     'L',CrwgXd,CrwgYd,1,5)
      nCrwRefsm90=CrwLastMade
      call FeQuestCrwMake(id,xqd*.5+155.,il,xqd*.5+180.,il,'Calc'
     1                    ,'L',CrwgXd,CrwgYd,0,6)
      nCrwRefscalc=CrwLastMade
      il=il+2
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestLblMake(id,5.,il,'Presets:','L','B')
      call FeQuestLblOn(LblLastMade)
      call FeQuestButtonMake(id,80.,il,100.,ButYd,'%Structure solution')
      nButtStructSol=ButtonLastMade
      call FeQuestButtonMake(id,200.,il,100.,ButYd,'%MEM with prior')
      nButtMEMwithPR=ButtonLastMade
      goto 9999

c!    MEMGeneralOpen
      entry MEMGeneralOpen
      call FeQuestStringEdwOpen(nEdwTitle,MEMTitle(1:idel(MEMTitle)))
      call FeQuestStringEdwOpen(nEdwName,MEMName(1:idel(MEMName)))
      call FeQuestIntAEdwOpen(nEdwDivision,MEMDivision,NDim(KPhase),
     1                          .false.)
      call FeQuestStringEdwOpen(nEdwOutFile,MEMOutFile)
      call FeQuestCrwOpen(nCrwPerformCF,PerformCode.eq.PerformCF)
      call FeQuestCrwOpen(nCrwPerformMEM,PerformCode.eq.PerformMEM)
      call FeQuestCrwOpen(nCrwIncludeRefs,IncludeRefs)
      call FeQuestCrwOpen(nCrwPerformCFMEM,PerformCode.eq.PerformCFMEM)
      if (Includerefs) then
        call FeQuestCrwOpen(nCrwRefsm80,RefsSourceCode.eq.Refsm80)
        call FeQuestCrwOpen(nCrwRefsm90,RefsSourceCode.eq.Refsm90)
        if (RefsSourceCode.eq.Refsm80) then
          call FeQuestCrwOpen(nCrwRefsobs,RefsTypeCode.eq.Refsobs)
          call FeQuestCrwOpen(nCrwRefscalc,RefsTypeCode.eq.Refscalc)
        else
          call FeQuestCrwClose(nCrwRefsobs)
          call FeQuestCrwClose(nCrwRefscalc)
        endif
      else
        call FeQuestCrwClose(nCrwRefsm80)
        call FeQuestCrwClose(nCrwRefsm90)
        call FeQuestCrwClose(nCrwRefsobs)
        call FeQuestCrwClose(nCrwRefscalc)
      endif
      call FeQuestButtonOpen(nButtStructSol,ButtonOff)
      if(isPowder) call FeQuestButtonDisable(nButtStructSol)
      call FeQuestButtonOpen(nButtMEMwithPR,ButtonOff)
      goto 9999

c!    MEMGeneralUpdate
      entry MEMGeneralUpdate
      MEMTitle=EdwStringQuest(nEdwTitle)
      MEMName=EdwStringQuest(nEdwName)
      MEMOutFile=EdwStringQuest(nEdwOutFile)
      IncludeRefs=CrwLogicQuest(nCrwIncludeRefs)
      if(EdwStringQuest(nEdwDivision).ne.' ') then
        call FeQuestIntAFromEdw(nEdwDivision,MEMDivision)
      else
        MEMDivision(1)=-111112
      endif
      do 1000 i=nCrwPerformCF,nCrwPerformCF+2
        if (CrwLogicQuest(i)) PerformCode=i-nCrwPerformCF+1
1000  continue
      IncludeRefs=CrwLogic(nCrwIncludeRefs)
      if (CrwLogicQuest(nCrwRefsm80)) then
        if (CrwLogicQuest(nCrwRefsObs))then
          RefsTypeCode=RefsObs
        else
          RefsTypeCode=RefsCalc
        endif
        RefsSourceCode=Refsm80
      else
        RefsSourceCode=Refsm90
      endif
      goto 9999
c!    MEMGeneralCheck
      entry MEMGeneralCheck(vyskoc)
      vyskoc=0
      if (CheckType.eq.EventButton.and.CheckNumber.eq.nButtStructSol)
     1    then
        call FeQuestButtonOn(nButtStructSol)
        call MEMPresets(1)
        call FeQuestButtonOff(nButtStructSol)
      elseif(CheckType.eq.EventButton.and.
     1       EventNumber.eq.nButtMEMwithPR) then
        call FeQuestButtonOn(nButtMEMwithPR)
        call MEMPresets(2)
        call FeQuestButtonOff(nButtMEMwithPR)
      endif
      if (CheckType.eq.EventCrw.and.Checknumber.eq.nCrwIncludeRefs)then
        if(CrwLogic(nCrwIncludeRefs))then
          call FeQuestCrwOpen(nCrwRefsm80,RefsSourceCode.lt.Refsm90)
          call FeQuestCrwOpen(nCrwRefsm90,RefsSourceCode.eq.Refsm90)
          if (CrwLogic(nCrwRefsm80)) then
            call FeQuestCrwOpen(nCrwRefsobs,RefsTypeCode.eq.Refsobs)
            call FeQuestCrwOpen(nCrwRefscalc,RefsTypeCode.eq.Refscalc)
          endif
        else
          if (CrwLogicQuest(nCrwRefsm80))then
            if (CrwLogicQuest(nCrwRefsObs))then
              RefsTypeCode=RefsObs
            else
              RefsTypeCode=RefsCalc
            endif
            RefsSourceCode=Refsm80
          else
            RefsSourceCode=Refsm90
          endif
          call FeQuestCrwClose(nCrwRefsm80)
          call FeQuestCrwClose(nCrwRefsm90)
          call FeQuestCrwClose(nCrwRefsobs)
          call FeQuestCrwClose(nCrwRefscalc)
        endif
      endif
      if (CheckType.eq.EventCrw.and.(Checknumber.eq.nCrwRefsm80.or.
     1    CheckNumber.eq.nCrwRefsm90)) then
        if (CrwLogicQuest(nCrwRefsm80)) then
          call FeQuestCrwOpen(nCrwRefsObs,RefsTypeCode.eq.RefsObs)
          call FeQuestCrwOpen(nCrwRefsCalc,RefsTypeCode.eq.RefsCalc)
          RefsSourceCode=Refsm80
        else
          if (CrwLogicQuest(nCrwRefsObs))then
            RefsTypeCode=RefsObs
          else
            RefsTypeCode=RefsCalc
          endif
          call FeQuestCrwClose(nCrwRefsObs)
          call FeQuestCrwClose(nCrwRefsCalc)
          RefsSourceCode=Refsm90
        endif
      endif
      if (CheckType.eq.EventEdw.and.Checknumber.eq.nEdwName)then
        t256=EdwStringQuest(nEdwName)
        if (t256.ne.MEMName) then
          MEMName=t256
          if (ExistFile(MEMName)) then
            if(FeYesNo(-1.,-1.,'The MEM input file exists. '//
     1             'Read the file?',1)) then
              StringKey=MEMName
              vyskoc=1
            endif
          endif
        endif
      endif
      goto 9999
c!    MEMGeneralComplete
      entry MEMGeneralComplete(okcheck)
      okcheck=0
      if (idel(MEMName).eq.0) then
        call FeChybne(-1.,-1.,'The filename must not be empty!',
     1                        ' ',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwName
        okcheck=1
        goto 9999
      endif
      if (idel(MEMOutFile).eq.0) then
        call FeChybne(-1.,-1.,'The name of the output file',
     1                        'must not be empty!',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwOutFile
        goto 9999
      endif
      if (MEMDivision(1).le.-111112) then
        call FeChybne(-1.,-1.,'Define the division',
     1                        'of the grid.',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwDivision
        goto 9999
      endif
      do 9900 i=1,NDim(KPhase)
        if (MEMDivision(i).le.0) then
          call FeChybne(-1.,-1.,'The number of pixels',
     1                          'must be positive.',SeriousError)
          okcheck=1
          EventType=EventEdw
          EventNumber=nEdwDivision
          goto 9999
        endif
9900  continue
      if (MEMCheckDivision(MEMDivision).eqv..false.) then
        call FeChybne(-1.,-1.,'The pixel division is inconsistent',
     1                        'with the symmetry.',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwDivision
        goto 9999
      endif
9999  continue
      return
      end
      subroutine MEMMEM
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical   CrwLogicQuest
      integer EdwStateQuest
      character*256 EdwStringQuest
      save nCrwIncMEM,nCrwSasa,nCrwMEMSys,nEdwConOrder,
     1     nEdwConWeight,nEdwAim,nEdwLambda,nEdwRate,nCrwFlat,
     2     nCrwFromFile,nCrw2ch,nEdwPriorFile,nEdwPriorFmt,
     3     nCrwPriorSF,nEdwSinThMin,nEdwSinThMax,nEdwMaxSat,
     4     nEdwPDCSigma
      real xqd
c!    MEMMEMMake
      entry MEMMEMMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      il=1
      call FeQuestCrwMake(id,5.,il,140.,il,'Include MEM','L',CrwXd,
     1                    CrwYd,0,0)
      nCrwIncMEM=CrwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Algorithm:','L','N')
      call FeQuestLblOn(LblLastMade)
      call FeQuestCrwMake(id,60.,il,140.,il,'Sakata-Sato','L',CrwgXd,
     1                    CrwgYd,1,3)
      nCrwSaSa=CrwLastMade
      call FeQuestEdwMake(id,xqd*.5+10.,il,xqd*.5+120.,il,'%Aim','L',
     1                    30.,EdwYd,0)
      nEdwAim=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,60.,il,140.,il,'MEMSys','L',CrwgXd,CrwgYd,
     1                      1,3)
      nCrwMEMSys=CrwLastMade
      call FeQuestEdwMake(id,xqd*.5+10.,il,xqd*.5+120.,il,'%Lambda','L',
     1                    50.,EdwYd,0)
      nEdwLambda=EdwLastMade
      call FeQuestEdwMake(id,xqd*.5+10.,il,xqd*.5+120.,il,'%Rate','L',
     1                    50.,EdwYd,0)
      nEdwRate=EdwLastMade
      il=il+1
      call FeQuestEudMake(id,5.,il,140.,il,'%Constraint order','L',
     1                    50.,EdwYd,0)
      nEdwConOrder=EdwLastMade
      call FeQuestEudMake(id,xqd*.5+10.,il,xqd*.5+120.,il,
     1                    '%Static weighting','L',50.,EdwYd,0)
      nEdwConWeight=EdwLastMade
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      call FeQuestCrwMake(id,5.,il,110.,il,'Flat prior','L',CrwgXd,
     1                    CrwgYd,1,4)
      nCrwFlat=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,110.,il,'Non-uniform prior','L',
     1                    CrwgXd,CrwgYd,1,4)
      nCrwFromFile=CrwLastMade
c!    only if non-flat prior
      il=il+1
      call FeQuestCrwMake(id,5.,il,110.,il,'Two channel','L',CrwXd,
     1                    CrwYd,0,0)
      nCrw2ch=CrwLastMade
      il=il-2
      call FeQuestEdwMake(id,170.,il,250.,il,'Prior file','L',70.,EdwYd,
     1                    0)
      nEdwPriorFile=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,170.,il,250.,il,'Prior format','L',70.,
     1                    EdwYd,0)
      nEdwPriorFmt=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,170.,il,250.,il,'PDC','L',CrwXd,CrwYd,1,0)
      nCrwPriorSF=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,170.,il,250.,il,
     1                    'Sin(th)/l from','L',50.,EdwYd,0)
      nEdwSinThMin=EdwLastMade
      call FeQuestEdwMake(id,310.,il,330.,il,'to','L',50.,EdwYd,0)
      nEdwSinThMax=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,170.,il,250.,il,'Sigma','L',50.,EdwYd,0)
      nEdwPDCSigma=EdwLastMade
      if(NDim(KPhase).gt.3) then
        il=il+1
        call FeQuestEudMake(id,170.,il,250.,il,'Max. sat. index','L',
     1                      50.,EdwYd,0)
        nEdwMaxSat=EdwLastMade
      endif
      goto 9999

c!    MEMMEMOpen
      entry MEMMEMOpen
      call FeQuestCrwOpen(nCrwIncMEM,IncludeMEM)
      call FeQuestCrwOpen(nCrwSaSa,MEMAlgCode.eq.SaSa)
      call FeQuestRealEdwOpen(nEdwAim,MEMAim,.false.,.false.)
      call FeQuestCrwOpen(nCrwMEMSys,MEMAlgCode.eq.MEMSys)
      if (MEMAlgCode.eq.SaSa)then
        call FeQuestEdwClose(nEdwRate)
        call FeQuestRealEdwOpen(nEdwLambda,MEMLambda,.false.,.false.)
      elseif (MEMAlgCode.eq.MEMSys) then
        call FeQuestEdwClose(nEdwLambda)
        call FeQuestRealEdwOpen(nEdwRate,MEMRate,.false.,.false.)
      endif
      call FeQuestIntEdwOpen(nEdwConOrder,MEMConOrder,.false.)
      call FeQuestEudOpen(nEdwConOrder,2,8,2,0.,0.,0.)
      call FeQuestIntEdwOpen(nEdwConWeight,MEMConWeight,.false.)
      call FeQuestEudOpen(nEdwConWeight,0,8,1,0.,0.,0.)
      call FeQuestCrwOpen(nCrwFlat,PriorCode.eq.Flat)
      call FeQuestCrwOpen(nCrwFromFile,PriorCode.eq.FromFile)
      if(PriorCode.eq.FromFile) then
        call FeQuestCrwOpen(nCrw2ch,Twochannel)
        call FeQuestStringEdwOpen(nEdwPriorFile,PriorFile)
        call FeQuestStringEdwOpen(nEdwPriorFmt,PriorFmt)
        call FeQuestCrwOpen(nCrwPriorSF,PriorSF)
        if (PriorSF) then
          call FeQuestRealEdwOpen(nEdwSinThMin,PDCSinThMin,.false.,
     1         .false.)
          call FeQuestRealEdwOpen(nEdwSinThMax,PDCSinThMax,.false.,
     1         .false.)
          call FeQuestRealEdwOpen(nEdwPDCSigma,PDCSigma,.false.,
     1         .false.)
          if(NDim(KPhase).gt.3)then
            call FeQuestIntEdwOpen(nEdwMaxSat,PDCMaxSat,.false.)
            call FeQuestEudOpen(nEdwMaxSat,0,20,1,0.,0.,0.)
          endif
        else
          call FeQuestEdwClose(nEdwSinThMin)
          call FeQuestEdwClose(nEdwSinThMax)
          call FeQuestEdwClose(nEdwPDCSigma)
          if(NDim(KPhase).gt.3)then
            call FeQuestEdwClose(nEdwMaxSat)
          endif
        endif
      else
        call FeQuestCrwClose(nCrw2ch)
        call FeQuestEdwClose(nEdwPriorFile)
        call FeQuestEdwClose(nEdwPriorFmt)
        call FeQuestCrwClose(nCrwPriorSF)
        call FeQuestEdwClose(nEdwSinThMin)
        call FeQuestEdwClose(nEdwSinThMax)
        call FeQuestEdwClose(nEdwPDCSigma)
        if(NDim(KPhase).gt.3)then
          call FeQuestEdwClose(nEdwMaxSat)
        endif
      endif
      goto 9999

c!    MEMMEMUpdate
      entry MEMMEMUpdate
      if(CrwLogicQuest(nCrwIncMEM)) then
        IncludeMEM=.true.
      else
        IncludeMEM=.false.
      endif
      call FeQuestIntFromEdw(nEdwConOrder,MEMConOrder)
      call FeQuestIntFromEdw(nEdwConWeight,MEMConWeight)
      do 1000 i=nCrwSaSa,nCrwSaSa+1
        if (CrwLogicQuest(i)) MEMAlgCode=i-nCrwSaSa+1
1000  continue
      call FeQuestRealFromEdw(nEdwAim,MEMAim)
      if(EdwStateQuest(nEdwLambda).eq.EdwOpened) then
        call FeQuestRealFromEdw(nEdwLambda,MEMLambda)
      endif
      if(EdwStateQuest(nEdwRate).eq.EdwOpened) then
        call FeQuestRealFromEdw(nEdwRate,MEMRate)
      endif
      do 1100 i=nCrwFlat,nCrwFlat+1
        if (CrwLogicQuest(i)) PriorCode=i-nCrwFlat+1
1100  continue
      if (PriorCode.eq.FromFile)then
        TwoChannel=CrwLogicQuest(nCrw2ch)
        PriorFile=EdwStringQuest(nEdwPriorFile)
        PriorFmt=EdwStringQuest(nEdwPriorFmt)
        PriorSF=CrwLogicQuest(nCrwPriorSF)
        if(PriorSF)then
          call FeQuestRealFromEdw(nEdwSinThMin,PDCSinThMin)
          call FeQuestRealFromEdw(nEdwSinThMax,PDCSinThMax)
          call FeQuestRealFromEdw(nEdwPDCSigma,PDCSigma)
          if(NDim(KPhase).gt.3)then
            call FeQuestIntFromEdw(nEdwMaxSat,PDCMaxSat)
          endif
        endif
      endif
      goto 9999
c!    MEMMEMCheck
      entry MEMMEMCheck
      if(CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwSaSa.or.
     1   CheckNumber.eq.nCrwMEMSys)) then
        if(CrwLogicQuest(nCrwSaSa)) then
          if(EdwStateQuest(nEdwRate).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwRate,MEMRate)
            call FeQuestEdwClose(nEdwRate)
          endif
          if(EdwStateQuest(nEdwLambda).ne.EdwOpened) then
            call FeQuestrealEdwOpen(nEdwLambda,MEMLambda,.false.,
     1           .false.)
          endif
          EventType=EventEdw
          EventNumber=nEdwLambda
        else
          if(EdwStateQuest(nEdwLambda).eq.EdwOpened) then
            call FeQuestRealFromEdw(nEdwLambda,MEMLambda)
            call FeQuestEdwClose(nEdwLambda)
          endif
          if(EdwStateQuest(nEdwRate).ne.EdwOpened) then
            call FeQuestrealEdwOpen(nEdwRate,MEMRate,.false.,.false.)
          endif
          EventType=EventEdw
          EventNumber=nEdwRate
        endif
      elseif(CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwFlat.or.
     1   CheckNumber.eq.nCrwFromFile)) then
        if(CrwLogicQuest(nCrwFlat)) then
          TwoChannel=CrwLogicQuest(nCrw2ch)
          call FeQuestCrwClose(nCrw2ch)
          PriorFile=EdwStringQuest(nEdwPriorFile)
          call FeQuestEdwClose(nEdwPriorFile)
          PriorFmt=EdwStringQuest(nEdwPriorFmt)
          call FeQuestEdwClose(nEdwPriorFmt)
          PriorSF=CrwLogicQuest(nCrwPriorSF)
          call FeQuestCrwClose(nCrwPriorSF)
          if(PriorSF)then
            call FeQuestRealFromEdw(nEdwSinThMin,PDCSinThMin)
            call FeQuestEdwClose(nEdwSinThMin)
            call FeQuestRealFromEdw(nEdwSinThMax,PDCSinThMax)
            call FeQuestEdwClose(nEdwSinThMax)
            call FeQuestRealFromEdw(nEdwPDCSigma,PDCSigma)
            call FeQuestEdwClose(nEdwPDCSigma)
            if(NDim(KPhase).gt.3)then
              call FeQuestIntFromEdw(nEdwMaxSat,PDCMaxSat)
              call FeQuestEdwClose(nEdwMaxSat)
            endif
          endif
        elseif (CrwLogicQuest(nCrwFromFile)) then
          if(EdwStateQuest(nEdwPriorFile).eq.EdwClosed) then
          call FeQuestCrwOpen(nCrw2ch,Twochannel)
          call FeQuestStringEdwOpen(nEdwPriorFile,PriorFile)
          call FeQuestStringEdwOpen(nEdwPriorFmt,PriorFmt)
          call FeQuestCrwOpen(nCrwPriorSF,PriorSF)
          if(CrwLogicQuest(nCrwPriorSF))then
            call FeQuestRealEdwOpen(nEdwSinThMin,PDCSinThMin,.false.,
     1          .false.)
            call FeQuestRealEdwOpen(nEdwSinThMax,PDCSinThMax,.false.,
     1          .false.)
            call FeQuestRealEdwOpen(nEdwPDCSigma,PDCSigma,.false.,
     1          .false.)
            if(NDim(KPhase).gt.3)then
              call FeQuestIntEdwOpen(nEdwMaxSat,PDCMaxSat,.false.)
              call FeQuestEudOpen(nEdwMaxSat,0,20,1,0.,0.,0.)
            endif
          endif
          EventType=EventEdw
          EventNumber=nEdwPriorFile
          endif
        endif
      elseif(CheckType.eq.EventCrw.and.
     1       CheckNumber.eq.nCrwPriorSF) then
        if (CrwLogicQuest(nCrwPriorSF)) then
          if(EdwStateQuest(nEdwSinThMin).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwSinThMin,PDCSinThMin,.false.,
     1           .false.)
            call FeQuestRealEdwOpen(nEdwSinThMax,PDCSinThMax,.false.,
     1           .false.)
            call FeQuestRealEdwOpen(nEdwPDCSigma,PDCSigma,.false.,
     1           .false.)
            if(NDim(KPhase).gt.3)then
              call FeQuestIntEdwOpen(nEdwMaxSat,PDCMaxSat,.false.)
              call FeQuestEudOpen(nEdwMaxSat,0,20,1,0.,0.,0.)
            endif
          endif
          EventType=EventEdw
          EventNumber=nEdwSinThMin
        else
          call FeQuestRealFromEdw(nEdwSinThMin,PDCSinThMin)
          call FeQuestEdwClose(nEdwSinThMin)
          call FeQuestRealFromEdw(nEdwSinThMax,PDCSinThMax)
          call FeQuestEdwClose(nEdwSinThMax)
          call FeQuestRealFromEdw(nEdwPDCSigma,PDCSigma)
          call FeQuestEdwClose(nEdwPDCSigma)
          if(NDim(KPhase).gt.3)then
            call FeQuestIntFromEdw(nEdwMaxSat,PDCMaxSat)
            call FeQuestEdwClose(nEdwMaxSat)
          endif
        endif
      else
        call FeInfoOut(-1.,-1.,'Kontrolni bod Questu nebyl osetren','L')
      endif
      goto 9999
c!    MEMMEMComplete
      entry MEMMEMComplete(okcheck)
      okcheck=0
      if (.not.IncludeMEM) goto 9999
      if (MEMAim.le.0.) then
        call FeChybne(-1.,-1.,'Aim must be a positive number.',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwAim
        goto 9999
      endif
      if (MEMRate.le.0.) then
        call FeChybne(-1.,-1.,'Rate must be a positive number.',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwRate
        goto 9999
      endif
      if (PriorCode.eq.FromFile)then
        if (PriorFile.eq.' ')then
        call FeChybne(-1.,-1.,'The name of the file with prior',
     1                        'must not be empty!',SeriousError)
          okcheck=1
          EventType=EventEdw
          EventNumber=nEdwPriorFile
          goto 9999
        endif
        if (PriorFmt(:idel(PriorFmt)).ne.'jana'.and.
     1      PriorFmt(:idel(PriorFmt)).ne.'BMascii'.and.
     2      PriorFmt(:idel(PriorFmt)).ne.'BMbinary')then
        call FeChybne(-1.,-1.,'The format of the prior must be',
     1                'jana, BMascii or BMbinary!',SeriousError)
          okcheck=1
          EventType=EventEdw
          EventNumber=nEdwPriorFmt
          goto 9999
        endif

        if (PriorSF.and.PDCSigma.le.0.) then
          call FeChybne(-1.,-1.,'Aim must be a positive number.',
     1                  ' ',SeriousError)
          okcheck=1
          EventType=EventEdw
          EventNumber=nEdwPDCSigma
          goto 9999
        endif
      endif
9999  continue
      return
      end
      subroutine MEMCF
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical CrwLogicQuest
      integer EdwStateQuest
      character*256 EdwStringQuest
      save nEdwDelta,nEdwWeak,nCrwIncCF,nCrwAutoRS,nEdwAutoRS,
     1     nCrwSymRecover,nCrwRelDelta,nCrwAbsDelta,nCrwAutoDelta,
     2     nCrwDefineDelta,nEdwBiso,nCrwSymNo,nCrwSymAverage
      real xqd
c!    MEMCFMake
      entry MEMCFMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      il=1
      call FeQuestCrwMake(id,5.,il,130.,il,'Include CF','L',CrwXd,CrwYd,
     1                      0,0)
      nCrwIncCF=CrwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Delta','L','N')
      call FeQuestLblOn(LblLastMade)
      call FeQuestCrwMake(id,40.,il,130.,il,'automatic:','L',CrwgXd,
     1     CrwgYd,0,12)
      nCrwAutoDelta=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,40.,il,130.,il,'defined:','L',CrwgXd,
     1     CrwgYd,0,12)
      nCrwDefineDelta=CrwLastMade
      call FeQuestEdwMake(id,23.,il,150.,il,' ','L',50.,EdwYd,0)
      nEdwDelta=EdwLastMade
      call FeQuestCrwMake(id,220.,il,270.,il,'absolute','L',CrwgXd,
     1                       CrwgYd,0,10)
      nCrwAbsDelta=CrwLastMade
      call FeQuestCrwMake(id,290.,il,340.,il,'fraction','L',CrwgXd,
     1                       CrwgYd,0,10)
      nCrwRelDelta=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,130.,il,'%Weak reflections','L',50.,
     1                    EdwYd,0)
      nEdwWeak=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,130.,il,'%Biso','L',50.,EdwYd,0)
      nEdwBiso=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,130.,il,'Automatic random seed','L',
     1                    CrwXd,CrwYd,1,0)
      nCrwAutoRS=CrwLastMade
      call FeQuestEdwMake(id,170.,il,250.,il,'Random seed:','L',50.,
     1                    EdwYd,0)
      nEdwAutoRS=EdwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Symmetry:','L','N')
      call FeQuestLblOn(LblLastMade)
      il=il+1
      call FeQuestCrwMake(id,5.,il,130.,il,'Do not search','L',CrwgXd,
     1                    CrwgYd,0,2)
      nCrwSymNo=CrwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,130.,il,'Shift origin','L',CrwgXd,
     1                    CrwgYd,0,2)
      nCrwSymRecover=CrwLastMade

      il=il+1
      call FeQuestCrwMake(id,5.,il,130.,il,'Average over symmetry',
     1                    'L',CrwgXd,CrwgYd,0,2)
      nCrwSymAverage=CrwLastMade

c!    MEMCFOpen
      entry MEMCFOpen
      call FeQuestCrwOpen(nCrwIncCF,IncludeCF)
      call FeQuestCrwOpen(nCrwAutodelta,AutoDelta.eqv..true.)
      call FeQuestCrwOpen(nCrwDefineDelta,AutoDelta.eqv..false.)
      call FeQuestRealEdwOpen(nEdwDelta,MEMCFdelta,.false.,.false.)
      call FeQuestCrwOpen(nCrwAbsDelta,.not.MEMCFRelDelta)
      call FeQuestCrwOpen(nCrwRelDelta,MEMCFRelDelta)
      call FeQuestRealEdwOpen(nEdwWeak,MEMCFWeak,.false.,.false.)
      call FeQuestRealEdwOpen(nEdwBiso,Biso,.false.,.false.)
      call FeQuestCrwOpen(nCrwAutoRS,AutoRS)
      if(.not.AutoRS) then
        call FeQuestIntEdwOpen(nEdwAutoRS,MEMCFRS,.false.)
      else
        call FeQuestEdwClose(nEdwAutoRS)
      endif
      call FeQuestCrwOpen(nCrwSymNo,CFSymCode.eq.NoSym)
      call FeQuestCrwOpen(nCrwSymRecover,CFSymCode.eq.RecoverSym)
      call FeQuestCrwOpen(nCrwSymAverage,CFSymCode.eq.AverageSym)
      goto 9999

c!    MEMCFUpdate
      entry MEMCFUpdate
      if(CrwLogicQuest(nCrwIncCF)) then
        IncludeCF=.true.
      else
        IncludeCF=.false.
      endif
      if (CrwLogicQuest(nCrwAutoDelta)) then
        AutoDelta=.true.
      else
        AutoDelta=.false.
      endif
      if(EdwStringQuest(nEdwDelta).ne.' ') then
        call FeQuestRealFromEdw(nEdwDelta,MEMCFDelta)
      else
        MEMCFDelta=-1.0e30
      endif
      if (CrwLogicQuest(nCrwAbsDelta)) then
        MEMCFRelDelta=.false.
      else
        MEMCFRelDelta=.true.
      endif
      if(EdwStringQuest(nEdwWeak).ne.' ') then
        call FeQuestRealFromEdw(nEdwWeak,MEMCFWeak)
      else
        MEMCFWeak=-1.0e30
      endif
      if(EdwStringQuest(nEdwBiso).ne.' ') then
        call FeQuestRealFromEdw(nEdwBiso,Biso)
      else
        Biso=-1.0e30
      endif
      if (CrwLogicQuest(nCrwAutoRS)) then
        AutoRS=.true.
      else
        AutoRS=.false.
        call FeQuestIntFromEdw(nEdwAutoRS,MEMCFRS)
      endif
      if (CrwLogicQuest(nCrwSymRecover)) then
        CFSymCode=RecoverSym
      elseif (CrwLogicQuest(nCrwSymAverage)) then
        CFSymCode=AverageSym
      else
        CFSymCode=NoSym
      endif
      goto 9999
c!    MEMCFCheck
      entry MEMCFCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAutoRS) then
        if(.not.CrwLogicQuest(nCrwAutoRS)) then
          if(EdwStateQuest(nEdwAutoRS).ne.EdwOpened) then
            call FeQuestIntEdwOpen(nEdwAutoRS,MEMCFRS,.false.)
          endif
          EventType=EventEdw
          EventNumber=nEdwAutoRS
        else
          call FeQuestIntFromEdw(nEdwAutoRS,MEMCFRS)
          call FeQuestEdwClose(nEdwAutoRS)
        endif
      endif
      goto 9999
c!    MEMCFComplete
      entry MEMCFComplete(okcheck)
      okcheck=0
      if (.not.IncludeCF) goto 9999
      if (MEMCFDelta.le.-1.0e29) then
        call FeChybne(-1.,-1.,'Fill the value of delta',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwdelta
        goto 9999
      endif
      if (MEMCFDelta.le.0.0) then
        call FeChybne(-1.,-1.,'Delta must be a positive number',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwDelta
        goto 9999
      endif
      if (MEMCFWeak.le.-1.0e29) then
        call FeChybne(-1.,-1.,'Fill the proportion',
     1                        'of the weak reflections',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwWeak
        goto 9999
      endif
      if (MEMCFWeak.lt.0.0.or.MEMCFWeak.ge.1.) then
        call FeChybne(-1.,-1.,'Proportion of the weak reflections',
     1                        'must be a number between 0 and 1.',
     2                        SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwWeak
        goto 9999
      endif
      if (Biso.le.-1.0e29) then
        call FeChybne(-1.,-1.,'Please give Biso',' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwBiso
        goto 9999
      endif
      if (.not.AutoRS.and.MEMCFRS.lt.0) then
        call FeChybne(-1.,-1.,'Random seed must be a positive integer',
     1                        ' ',SeriousError)
        okcheck=1
        EventType=EventEdw
        EventNumber=nEdwAutoRS
        goto 9999
      endif
9999  continue
      return
      end
      subroutine MEMPrior
      use Atoms_mod
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical   CrwLogicQuest
      character*256 EdwStringQuest
      save nCrwIncPrior,nEdwOutPrior,nButtSelAt
c!    MEMPriorMake
      entry MEMPriorMake(id)
      xqd=(QuestXMax(id)-QuestXMin(id))
      il=1
      call FeQuestCrwMake(id,5.,il,75.,il,'Include Prior','L',
     1     CrwXd,CrwYd,0,0)
      nCrwIncPrior=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,75.,il,'%Output file','L',xqd-80.,
     1                    EdwYd,1)
      nEdwOutPrior=EdwLastMade
      il=il+1
      call FeQuestButtonMake(id,5.,il,70.,ButYd,'%Select atoms')
      nButtSelAt=ButtonLastMade
      goto 9999

c!    MEMPriorOpen
      entry MEMPriorOpen
      call FeQuestCrwOpen(nCrwIncPrior,IncludePrior)
      call FeQuestStringEdwOpen(nEdwOutPrior,
     1     MEMOutPrior(1:idel(MEMOutPrior)))
      call FeQuestButtonOpen(nButtSelAt,ButtonOff)
      goto 9999

c!    MEMPRiorUpdate
      entry MEMPriorUpdate
      if(CrwLogicQuest(nCrwIncPrior)) then
        IncludePrior=.true.
      else
        IncludePrior=.false.
      endif
      MEMOutPrior=EdwStringQuest(nEdwOutPrior)
      goto 9999
c!    MEMPriorCheck
      entry MEMPriorCheck
      if (CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelAt) then
        if(allocated(AtBer)) deallocate(AtBer)
        allocate(AtBer(NAtInd))
        call SetLogicalArrayTo(AtBer,NAtInd,.true.)
        call iom40(0,0,fln(:ifln)//'.m40')
        call SelAtoms('Select atoms from atomic part',Atom(1),
     1                AtBer(1),isf(1),NAtInd,ich)
      endif
      goto 9999
      entry MEMPriorComplete(okcheck)
      okcheck=0
      if (.not.IncludePrior) goto 9999
      if (idel(MEMOutPrior).eq.0) then
        call FeChybne(-1.,-1.,'The filename must not be empty!',
     1                        ' ',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwOutPrior
        okcheck=1
        goto 9999
      endif
9999  continue
      return
      end
      subroutine MEMEDMA
      use Atoms_mod
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'memexport.cmn'
      logical CrwLogicQuest
      character*256 EdwStringQuest
      integer nEdwtstart(3),nEdwtend(3),nEdwtstep(3)
      save nCrwIncEDMA,nEdwInEDMA,nEdwOutEDMA,nButtSelAt,nCrwMaxAll,
     1     nCrwMaxAtoms,nCrwPosRel,nCrwPosAbs,nEdwTol,nCrwWrm40,
     2     nEdwwrm40,nCrwScAngst,nCrwScFract,nCrwproj,nEdwaddb,
     3     nEdwtstart,nEdwtend,nEdwtstep,nEdwChlimlist,nEdwChlimint,
     4     nEdwPlim,nCrwCharge,nLblPos,nLblt,nCrwfullcell,nCrwAbsChlim,
     5     nCrwRelChlim,nEdwtotatoms
c!    MEMEDMAMake
      entry MEMEDMAMake(id)
      xqd=QuestXMax(id)-QuestXMin(id)
      il=1
      call FeQuestCrwMake(id,5.,il,100.,il,'Include EDMA','L',
     1     CrwXd,CrwYd,0,0)
      nCrwIncEDMA=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,100.,il,'%Density file','L',xqd-105.,
     1                    EdwYd,0)
      nEdwInEDMA=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,100.,il,'%Output filebase','L',
     1                    xqd-105.,EdwYd,0)
      nEdwOutEDMA=EdwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Maxima:','L','N')
      call FeQuestCrwMake(id,60.,il,90.,il,'atoms','L',CrwgXd,
     1                    CrwgYd,1,7)
      nCrwMaxAtoms=CrwLastMade
      call FeQuestCrwMake(id,120.,il,135.,il,'all','L',CrwgXd,
     1                    CrwgYd,1,7)
      nCrwMaxAll=CrwLastMade
      il=il+1
      call FeQuestButtonMake(id,50.,il,90.,ButYd,'%Select atoms')
      nButtSelAt=ButtonLastMade
      il=il-1
      call FeQuestEdwMake(id,170.,il,230.,il,'%Tolerance','L',50.,
     1                    EdwYd,0)
      nEdwTol=EdwLastMade
      il=il+1
      ilpos=il
      call FeQuestLblMake(id,170.,il,'Position:','L','N')
      nLblPos=LblLastMade
      call FeQuestCrwMake(id,230.,il,280.,il,'absolute','L',CrwgXd,
     1                    CrwgYd,0,8)
      nCrwPosAbs=CrwLastMade
      call FeQuestCrwMake(id,310.,il,360.,il,'relative','L',CrwgXd,
     1                    CrwgYd,0,8)
      nCrwPosRel=CrwLastMade
      il=il-1
      call FeQuestCrwMake(id,170.,il,230.,il,'Create m40','L',
     1     CrwXd,CrwYd,1,0)
      nCrwWrm40=CrwLastMade
      call FeQuestEdwMake(id,250.,il,260.,il,'','L',xqd-265.,
     1                    EdwYd,0)
      nEdwwrm40=EdwLastMade
      il=il+1
      call FeQuestEdwMake(id,170.,il,260.,il,'Number of atoms','L',
     1                    xqd-265.,EdwYd,0)
      nEdwtotatoms=EdwLastMade
      call FeQuestCrwMake(id,5.,il,120.,il,'List maxima in full cell',
     1                    'L',CrwXd,CrwYd,0,0)
      nCrwFullCell=CrwLastMade
      il=il+1
      call FeQuestLblMake(id,5.,il,'Scale:','L','N')
      call FeQuestCrwMake(id,50.,il,80.,il,'fract.','L',CrwgXd,
     1                    CrwgYd,0,9)
      nCrwScFract=CrwLastMade
      call FeQuestCrwMake(id,100.,il,120.,il,'A','L',CrwgXd,
     1                    CrwgYd,0,9)
      nCrwScAngst=CrwLastMade
      call FeQuestEdwMake(id,150.,il,190.,il,'Density limit','L',50.,
     1                    EdwYd,0)
      nEdwPlim=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,5.,il,100.,il,'Integrate charge','L',CrwXd,
     1                    CrwYd,1,0)
      nCrwCharge=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,100.,il,'Charge limit','L',50.,EdwYd,
     1                    0)
      nEdwChlimlist=EdwLastMade
      call FeQuestCrwMake(id,180.,il,225.,il,'absolute','L',CrwgXd,
     1                       CrwgYd,0,12)
      nCrwAbsChlim=CrwLastMade
      call FeQuestCrwMake(id,250.,il,290.,il,'relative','L',CrwgXd,
     1                       CrwgYd,0,12)
      nCrwRelChlim=CrwLastMade
      il=il+1
      call FeQuestEdwMake(id,5.,il,100.,il,'Fract. for c.o.c.','L',50.,
     1                    EdwYd,0)
      nEdwChlimint=EdwLastMade
      if (NDim(KPhase).eq.3) goto 9999
      il=il+1
      call FeQuestCrwMake(id,5.,il,80.,il,'Projection','L',CrwXd,CrwYd,
     1                    1,0)
      nCrwProj=CrwLastMade
      call FeQuestLblMake(id,150.,il,'t-sections:','L','N')
      call FeQuestLblOff(LblLastMade)
      nLblt=LblLastMade
      do 1000 i=1,NDimI(KPhase)
        call FeQuestEdwMake(id,210.,il,240.,il,'start','L',
     1       40.,EdwYd,0)
        nEdwtstart(i)=EdwLastMade
        call FeQuestEdwMake(id,290.,il,320.,il,'end','L',
     1       40.,EdwYd,0)
        nEdwtend(i)=EdwLastMade
        call FeQuestEdwMake(id,370.,il,400.,il,'step','L',
     1       40.,EdwYd,0)
        nEdwtstep(i)=EdwLastMade
        il=il+1
1000  continue
      call FeQuestEdwMake(id,5.,il,80.,il,'Add border','L',50.,EdwYd,0)
      nEdwaddb=EdwLastMade
      goto 9999
c!   MEMEDMAOpen
      entry MEMEDMAOpen
      call FeQuestCrwOpen(nCrwIncEDMA,IncludeEDMA)
      call FeQuestStringEdwOpen(nEdwInEDMA,
     1     MEMInEDMA(:idel(MEMInEDMA)))
      call FeQuestStringEdwOpen(nEdwOutEDMA,
     1     MEMOutEDMA(:idel(MEMOutEDMA)))
      call FeQuestCrwOpen(nCrwMaxAtoms,EDmaxima.eq.atms)
      call FeQuestCrwOpen(nCrwMaxAll,EDmaxima.eq.all)
      if(EDmaxima.eq.atms) then
        call FeQuestCrwClose(nCrwWrm40)
        call FeQuestCrwClose(nCrwFullCell)
        call FeQuestEdwClose(nEdwwrm40)
        call FeQuestLblOn(nLblPos)
        call FeQuestEdwClose(nEdwtotatoms)
        call FeQuestButtonOpen(nButtSelAt,ButtonOff)
        call FeQuestRealEdwOpen(nEdwTol,EDtolerance,.false.,.false.)
        call FeQuestCrwOpen(nCrwPosAbs,EDposition.eq.absol)
        call FeQuestCrwOpen(nCrwPosRel,EDposition.eq.relat)
      elseif(EDmaxima.eq.all) then
        call FeQuestButtonClose(nButtSelAt)
        call FeQuestEdwClose(nEdwTol)
        call FeQuestCrwClose(nCrwPosAbs)
        call FeQuestCrwClose(nCrwPosRel)
        call FeQuestLblOff(nLblPos)
        call FeQuestCrwOpen(nCrwWrm40,EDwrm40)
        call FeQuestCrwOpen(nCrwfullcell,EDfullcell)
        if (EDwrm40) then
          call FeQuestStringEdwOpen(nEdwwrm40,EDm40file)
          call FeQuestIntEdwOpen(nEdwtotatoms,totatoms,.false.)
        endif
      endif
      call FeQuestCrwOpen(nCrwScFract,EDscale.eq.fract)
      call FeQuestCrwOpen(nCrwScAngst,EDscale.eq.angst)
      call FeQuestCrwOpen(nCrwCharge,EDcharge)
      call FeQuestRealEdwOpen(nEdwplim,EDplim,.false.,.false.)
      if(EDcharge) then
        call FeQuestRealEdwOpen(nEdwchlimlist,
     1                          EDchlimlist,.false.,.false.)
        call FeQuestCrwOpen(nCrwAbsChlim,AbsChlim)
        call FeQuestCrwOpen(nCrwRelChlim,.not.AbsChlim)
        call FeQuestRealEdwOpen(nEdwchlimint,
     1                          EDchlimint,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwchlimlist)
        call FeQuestCrwClose(nCrwAbsChlim)
        call FeQuestCrwClose(nCrwRelChlim)
        call FeQuestEdwClose(nEdwchlimint)
      endif
      if (NDim(KPhase).eq.3) goto 9999
      call FeQuestCrwOpen(nCrwProj,EDprojection)
      do 1100 i=1,NDimI(KPhase)
        if(.not.EDprojection) then
          call FeQuestRealEdwOpen(nEdwtstart(i),
     1                        tstart(i),.false.,.false.)
          call FeQuestRealEdwOpen(nEdwtend(i),
     1                        tend(i),.false.,.false.)
          call FeQuestRealEdwOpen(nEdwtstep(i),
     1                        tstep(i),.false.,.false.)
        else
          call FeQuestEdwClose(nEdwtstart(i))
          call FeQuestEdwClose(nEdwtend(i))
          call FeQuestEdwClose(nEdwtstep(i))
        endif
1100  continue
      if (.not.EDprojection) then
        call FeQuestRealEdwOpen(nEdwaddb,EDaddb,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwaddb)
      end if
      goto 9999

c!    MEMEDMAUpdate
      entry MEMEDMAUpdate
      if(CrwLogicQuest(nCrwIncEDMA)) then
        IncludeEDMA=.true.
      else
        IncludeEDMA=.false.
      endif
      MEMInEDMA=EdwStringQuest(nEdwInEDMA)
      MEMOutEDMA=EdwStringQuest(nEdwOutEDMA)
      if(CrwLogicQuest(nCrwMaxAll)) then
        EDmaxima=all
        EDfullcell=CrwLogicQuest(nCrwfullcell)
        if (CrwLogicQuest(nCrwWrm40)) then
          EDwrm40=.true.
          EDm40file=EdwStringQuest(nEdwWrm40)
        endif
      elseif(CrwLogicQuest(nCrwMaxAtoms)) then
        EDmaxima=atms
        call FeQuestRealFromEdw(nEdwTol,EDtolerance)
        if(CrwLogicQuest(nCrwPosAbs)) then
          EDposition=absol
        elseif(CrwLogicQuest(nCrwPosrel))then
          EDposition=relat
        endif
      endif
      call FeQuestRealFromEdw(nEdwPlim,EDplim)
      if(CrwLogicQuest(nCrwCharge)) then
        EDcharge=.true.
        call FeQuestRealFromEdw(nEdwChlimlist,EDchlimlist)
        call FeQuestRealFromEdw(nEdwChlimint,EDchlimint)
        AbsChlim=CrwLogicQuest(nCrwAbsChlim)
      else
        EDcharge=.false.
      endif
      if (CrwLogicQuest(nCrwScFract)) then
        EDscale=fract
      else
        EDscale=angst
      endif
      if(NDim(KPhase).gt.3) then
        if (CrwLogicQuest(nCrwproj)) then
          EDprojection=.true.
        else
          EDprojection=.false.
          call FeQuestRealFromEdw(nEdwAddb,EDaddb)
          do 1900 i=1,NDimI(KPhase)
            call FeQuestRealFromEdw(nEdwtstart(i),tstart(i))
            call FeQuestRealFromEdw(nEdwtend(i),tend(i))
            call FeQuestRealFromEdw(nEdwtstep(i),tstep(i))
1900      continue
        endif
      endif
      goto 9999
c!    MEMEDMACheck
      entry MEMEDMACheck(id)
      if (CheckType.eq.EventButton.and.CheckNumber.eq.nButtSelAt) then
        call FeQuestButtonOn(nButtSelAt)
        call iom40(0,0,fln(:ifln)//'.m40')
        if(allocated(AtBerEDMA)) deallocate(AtBerEDMA)
        allocate(AtBerEDMA(NAtInd))
        call SetLogicalArrayTo(AtBerEDMA,NAtInd,.true.)
        call SelAtoms('Select atoms from atomic part',Atom(1),
     1                AtBerEDMA(1),isf(1),NAtInd,ich)
        call FeQuestButtonOff(nButtSelAt)
      endif
      if (CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwMaxAtoms.or.
     1    CheckNumber.eq.nCrwMaxAll)) then
        if(CrwLogicQuest(nCrwMaxAtoms)) then
          EDmaxima=atms
          if (CrwLogicQuest(nCrwWrm40)) then
            EDwrm40=.true.
            EDm40file=EdwStringQuest(nEdwWrm40)
            CALL FeQuestIntFromEdw(nEdwtotatoms,TotAtoms)
          else
            EDwrm40=.false.
          endif
          call FeQuestCrwClose(nCrwWrm40)
          call FeQuestEdwClose(nEdwWrm40)
          call FeQuestEdwClose(nEdwtotatoms)
          EDfullcell=CrwLogicQuest(nCrwFullcell)
          call FeQuestCrwClose(nCrwfullcell)
          call FeQuestButtonOpen(nButtSelAt,ButtonOff)
          call FeQuestRealEdwOpen(nEdwTol,EDtolerance,.false.,.false.)
          call FeQuestLblOn(nLblPos)
          call FeQuestCrwOpen(nCrwPosAbs,EDposition.eq.absol)
          call FeQuestCrwOpen(nCrwPosRel,EDposition.eq.relat)
        elseif(CrwLogicQuest(nCrwMaxAll)) then
          EDmaxima=all
          call FeQuestRealFromEdw(nEdwTol,EDtolerance)
          if(CrwLogicQuest(nCrwPosAbs)) then
            EDposition=absol
          elseif(CrwLogicQuest(nCrwPosrel))then
            EDposition=relat
          endif
          call FeQuestButtonClose(nButtSelAt)
          call FeQuestEdwClose(nEdwTol)
          call FeQuestLblOff(nLblPos)
          call FeQuestCrwClose(nCrwPosAbs)
          call FeQuestCrwClose(nCrwPosRel)
          call FeQuestCrwOpen(nCrwWrm40,EDwrm40)
          call FeQuestCrwOpen(nCrwfullcell,EDfullcell)
          if (EDwrm40) then
            call FeQuestStringEdwOpen(nEdwwrm40,EDm40file)
            call FeQuestIntEdwOpen(nEdwtotatoms,totatoms,.false.)
          endif
        endif
      endif
      if (CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwWrm40) then
        if(CrwLogicQuest(nCrwWrm40)) then
          call FeQuestStringEdwOpen(nEdwwrm40,EDm40file)
          call FeQuestIntEdwOpen(nEdwtotatoms,Totatoms,.false.)
        else
          EDm40file=EdwStringQuest(nEdwWrm40)
          call FeQuestEdwClose(nEdwwrm40)
          CALL FeQuestIntFromEdw(nEdwTotatoms,TotAtoms)
          call FeQuestEdwClose(nEdwTotAtoms)
        endif
      endif
      if (CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwProj) then
        if(CrwLogicQuest(nCrwproj)) then
          call FeQuestLblOff(nLblt)
          do 2000 i=1,NDimI(KPhase)
            call FeQuestRealFromEdw(nEdwtstart(i),tstart(i))
            call FeQuestEdwClose(nEdwtstart(i))
            call FeQuestRealFromEdw(nEdwtend(i),tend(i))
            call FeQuestEdwClose(nEdwtend(i))
            call FeQuestRealFromEdw(nEdwtstep(i),tstep(i))
            call FeQuestEdwClose(nEdwtstep(i))
2000      continue
          call FeQuestRealFromEdw(nEdwaddb,EDaddb)
          call FeQuestEdwClose(nEdwaddb)
        else
          call FeQuestLblOn(nLblt)
          do 2010 i=1,NDimI(KPhase)
            call FeQuestRealEdwOpen(nEdwtstart(i),tstart(i),.false.,
     1           .false.)
            call FeQuestRealEdwOpen(nEdwtend(i),tend(i),.false.,
     1           .false.)
            call FeQuestRealEdwOpen(nEdwtstep(i),tstep(i),.false.,
     1           .false.)
2010      continue
          call FeQuestRealEdwOpen(nEdwaddb,EDaddb,.false.,.false.)
        endif
      endif
      if (CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCharge) then
        if(CrwLogicQuest(nCrwcharge)) then
          call FeQuestRealEdwOpen(nEdwchlimlist,
     1                        EDchlimlist,.false.,.false.)
          call FeQuestCrwOpen(nCrwAbsChlim,AbsChlim)
          call FeQuestCrwOpen(nCrwRelChlim,.not.AbsChlim)
          call FeQuestRealEdwOpen(nEdwchlimint,
     1                        EDchlimint,.false.,.false.)
        else
          call FeQuestRealFromEdw(nEdwchlimlist,EDchlimlist)
          call FeQuestEdwClose(nEdwchlimlist)
          AbsChlim=CrwLogicQuest(nCrwAbsChlim)
          call FeQuestCrwClose(nCrwAbsChlim)
          call FeQuestCrwClose(nCrwRelChlim)
          call FeQuestRealFromEdw(nEdwchlimint,EDchlimint)
          call FeQuestEdwClose(nEdwchlimint)
        endif
      endif
      goto 9999
c!    MEMEDMAComplete
      entry MEMEDMAComplete(okcheck)
      okcheck=0
      if (.not.IncludeEDMA) goto 9999
      if (idel(MEMInEDMA).eq.0) then
        call FeChybne(-1.,-1.,'The name of the density file',
     1                        'must not be empty!',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwInEDMA
        okcheck=1
        goto 9999
      endif
      if (idel(MEMOutEDMA).eq.0) then
        call FeChybne(-1.,-1.,'The name of the output filebase',
     1                        'must not be empty!',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwOutEDMA
        okcheck=1
        goto 9999
      endif
      if (EDwrm40.and.(idel(EDm40file).eq.0)) then
        call FeChybne(-1.,-1.,'The name of the m40 file',
     1                        'must not be empty!',SeriousError)
        EventType=EventEdw
        EventNumber=nEdwWrm40
        okcheck=1
        goto 9999
      endif
9999  continue
      return
      end
      subroutine WriteMEMFile
      use Basic_mod
      use Atoms_mod
      use Chargeden_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'memexport.cmn'
      character*16 VoxFmt
      character*30 symmcp(:,:)
      character*80 t80
      integer CrlCentroSymm
      allocatable symmcp
      if(allocated(symmcp)) deallocate(symmcp)
      allocate(symmcp(NDim(KPhase),NSymmN(KPhase)))
      VoxFmt='(''voxel '', i4)'
      write(VoxFmt(11:11),'(i1)') NDim(KPhase)
      open(MEMFileId,file=MEMName)
c!    General Information
      write(MEMFileId,'(2a)') 'title ',
     1      MEMTitle(1:idel(MEMTitle))
      if(PerformCode.eq.PerformMEM)then
        t80='perform MEM'
      elseif(PerformCode.eq.PerformCFMEM)then
        t80='perform CF+MEM'
      else if(SuperflipAlgorithm.eq.1) then
        t80='perform CF'
      else if(SuperflipAlgorithm.eq.2) then
        t80='perform LDE'
      else if(SuperflipAlgorithm.eq.3) then
        t80='perform AAR'
      endif
      write(MEMFileId,FormA) t80(1:idel(t80))
      if(SuperflipStartModel.eq.2)
     1  write(MEMFileId,FormA) 'modelfile superposition'
      write(MEMFileId,'(''outputfile '',a)')
     1        MEMOutfile(1:idel(MEMOutFile))
      write(MEMFileId,'(''outputformat jana'')')
      write(MEMFileId,'(a,i3)') 'dimension',NDim(KPhase)
      if(PerformCode.ne.PerformCF)
     1  write(MEMFileId,VoxFmt) (MEMDivision(i),i=1,NDim(KPhase))
      write(MEMFileId,'(a,3f10.4,3f8.2)')'cell ',
     1                (CellPar(i,1,KPhase),i=1,6)
      if (NDim(KPhase).gt.3) then
        write(MEMFileId,'(''qvectors'')')
        do 1000 i=1,NDimI(KPhase)
          write(MEMFileId,'(3f14.9)') (qu(j,i,1,KPhase),j=1,3)
1000    continue
        write(MEMFileId,'(''endqvectors'')')
      endif
      write(MEMFileId,'(2a)') 'spacegroup ',Grupa(KPhase)
        if(CrlCentroSymm().gt.0) then
          write(MEMFileId,'(''centro yes'')')
        else
          write(MEMFileId,'(''centro no'')')
        endif
      write(MEMFileId,'(''centers'')')
        do 1200j=1,NLattVec(KPhase)
          write(MEMFileId,'(6f10.6)')
     1      (vt6(k,j,1,KPhase),k=1,NDim(KPhase))
1200    continue
      write(MEMFileId,'(''endcenters'')')
      write(MEMFileId,'(''symmetry'')')
        n=0
        do 1400j=1,NSymmN(KPhase)
          call codesymm(rm6(1,j,1,KPhase),s6(1,j,1,KPhase),
     1                  symmcp(1,j),1)
          do 1300k=1,NDim(KPhase)
            n=max(n,idel(symmcp(k,j))+1)
1300      continue
1400    continue
        if(n.lt.5) n=5
        do 1600j=1,NSymmN(KPhase)
          t80=' '
          k=1
          do 1500l=1,NDim(KPhase)
            t80(k+n-idel(symmcp(l,j)):)=symmcp(l,j)
            k=k+n
1500      continue
          write(MEMFileId,FormA)t80(1:Idel(t80))
1600    continue
      write(MEMFileId,'(''endsymmetry'')')
c!    MEM-specific
      if (includeMEM) then
        write(MEMFileId,*)
        write(MEMFileId,'(''# MEM-specific keywords'')')
        if (MEMAlgCode.eq.SaSa) then
          if (MEMlambda.eq.0.0.and.MEMAim.eq.1.0)then
            write(MEMFileId,'(''algorithm S-S'')')
          elseif(MEMLambda.eq.0.0)then
            write(MEMFileId,'(''algorithm S-S AUTO '',f6.3)') MEMAim
          else
            write(MEMFileId,'(''algorithm S-S '',2f6.3)')
     1            MEMLambda,MEMAim
          endif
        elseif (MEMAlgCode.eq.MEMSys)then
          if (MEMRate.eq.1.0.and.MEMAim.eq.1.0)then
            write(MEMFileId,'(''algorithm MEMSys'')')
          else
           write(MEMFileId,
     1     '(''algorithm MEMSys 4 1'',2f7.3,'' 0.05'')') MEMAim,MEMRate
          endif
        endif
        if (MEMConOrder.ne.2) write(MEMFileId,'(''conorder '',i1)')
     1                              MEMConOrder
        if (MEMConWeight.ne.0) write(MEMFileId,'(''conweight H'',i1)')
     1                              MEMConWeight
        if (PriorCode.eq.Flat) then
          write(MEMFileId,'(''initialdensity flat'')')
        elseif (PriorCode.eq.FromFile) then
          write(MEMFileId,'(''initialdensity '',a)')
     1          PriorFmt(1:idel(PriorFmt))
          write(MEMFileId,'(''initialfile '',a)')
     1          PriorFile(1:idel(PriorFile))
          if (TwoChannel) write(MEMFileId,'(''2channel yes'')')
          if (PriorSF) then
            if(NDim(KPhase).gt.3)then
              write(MEMFileId,'(''priorsf '',3f6.3,i2)')
     1              PDCSinThMin,PDCSinThMax,PDCSigma,PDCMaxSat
            else
              write(MEMFileId,'(''priorsf '',3f7.3)')
     1              PDCSinThMin,PDCSinThMax,PDCSigma
            endif
          endif
        endif
        write(MEMFileId,'(''# End of MEM-specific keywords'')')
      endif
c!    Charge flipping
      if (includeCF) then
        if(NAtFormula(KPhase).gt.0) then
          t80='composition'
          do i=1,NAtFormula(KPhase)
            write(Cislo,FormI15)
     1        nint(AtMult(i,KPhase)*float(NUnits(KPhase)))
            call Zhusti(Cislo)
            t80=t80(:idel(t80))//' '//
     1          AtType(i,KPhase)(:idel(AtType(i,KPhase)))//
     2          Cislo(:idel(Cislo))
          enddo
          write(MEMFileId,FormA) t80(:idel(t80))
        endif
        write(MEMFileId,*)
        write(MEMFileId,'(''# Keywords for charge flipping'')')
        if(SuperflipLocalNorm)
     1    write(MEMFileId,FormA) 'normalize local'
        if(SuperflipKeepTerminal)
     1    write(MEMFileId,FormA) 'terminal yes keep'
        if(SuperflipRepeatMode.eq.-1) then
          write(MEMFileId,FormA) 'repeatmode nosuccess'
        else if(SuperflipRepeatMode.eq.1) then
          write(Cislo,FormI15) SuperflipRepeatMax
          call Zhusti(Cislo)
          write(MEMFileId,FormA) 'repeatmode '//Cislo(:idel(Cislo))
        endif
        if(SuperflipRepeatMode.ne.0)
     1    write(MEMFileId,FormA) 'bestdensities 1 symmetry'
        write(MEMFileId,FormA) 'polish yes'
        write(Cislo,FormI15) SuperflipMaxCycles
        call zhusti(Cislo)
        write(MEMFileId,'(''maxcycles '',a)') Cislo(:idel(Cislo))
        if(AutoDelta) then
          if(SuperflipDelta.le.0.) then
            write(MEMFileId,'(''delta AUTO'')')
          else
            write(Cislo,'(f8.4)') SuperFlipDelta
            call ZdrcniCisla(Cislo,1)
            write(MEMFileId,'(''delta '',a,'' sigma'')')
     1        Cislo(:idel(Cislo))
          endif
        else
          if (MEMCFRelDelta) then
            t80=' fraction'
          else
            t80=' absolute'
          endif
          write(MEMFileId,'(''delta '',f6.3,a)') MEMCFDelta,
     1         t80(:idel(t80))
        endif
        write(MEMFileId,'(''weakratio '',f5.3)') MEMCFWeak
        write(MEMFileId,'(''Biso '',f7.3)') Biso
        if(AutoRS) then
          write(MEMFileID,'(''randomseed AUTO'')')
        else
          write(t80,'(''randomseed '',i15)') MEMCFRS
          call ZdrcniCisla(t80,2)
          write(MEMFileID,FormA) t80(:idel(t80))
        endif
        if (CFSymCode.eq.NoSym)then
          t80='searchsymmetry no'
        elseif (CFSymCode.eq.RecoverSym)then
          t80='searchsymmetry shift'
        elseif (CFSymCode.eq.AverageSym)then
          t80='searchsymmetry average'
        endif
        write(MEMFileId,FormA) t80(1:idel(t80))
        t80='derivesymmetry yes'
        write(MEMFileId,FormA) t80(1:idel(t80))
        if(Radiation(KDatBlock).eq.ElectronRadiation)
     1    write(MEMFileId,FormA) 'missing float 0.5'
        write(MEMFileId,'(''# End of keywords for charge flipping'')')
      endif
c!    Prior
      if (IncludePrior) then
        call iom40(0,0,fln(:ifln)//'.m40')
        write(MEMFileId,*)
        write(MEMFileId,'(''# Prior-specific keywords'')')
        write(MEMFileId,'(''outputprior '',a)')
     1        MEMOutPrior(:idel(MEMOutPrior))
        write(MEMFileId,'(''prioratoms'')')
        do 3000i=1,NAtInd
          if (Atber(i))
     1      write(MEMFileId,'(a8,a2,2X,4f10.6,2x,6f10.6)') atom(i),
     2                    AtType(isf(i),KPhase),ai(i),(x(j,i),j=1,3),
     3                    (beta(j,i)/urcp(j,1,KPhase),j=1,6)
3000    continue
        write(MEMFileId,'(''endprioratoms'')')
        write(MEMFileId,'(''# End of Prior-specific keywords'')')
      endif
c!    EDMA
      if (IncludeEDMA) then
        write(MEMFileId,*)
        write(MEMFileId,'(''# EDMA-specific keywords'')')
        write(MEMFileId,'(''inputfile '',a)')
     1        MEMInEDMA(:idel(MEMInEDMA))
        write(MEMFileId,'(''outputbase '',a)')
     1        MEMOutEDMA(:idel(MEMOutEDMA))
        if (EDwrm40) then
          write(MEMFileId,'(''m40forjana yes'')')
          write(MEMFileId,'(''writem40 '',a)')
     1               EDm40file(1:idel(EDm40file))
        endif
        if (EDmaxima.eq.atms) then
          write(MEMFileId,'(''maxima atoms'')')
          write(MEMFileId,'(''edmaatoms'')')
          do 3100i=1,NAtInd
            if (AtberEDMA(i))
     1        write(MEMFileId,'(a8,2X,3f10.6)') atom(i),(x(j,i),j=1,3)
3100      continue
          write(MEMFileId,'(''endedmaatoms'')')
          if (EDposition.eq.absol) then
            write(MEMFileId,'(''position absolute'')')
          elseif (EDposition.eq.relat) then
            write(MEMFileId,'(''position relative'')')
          endif
          write(MEMFileId,'(''tolerance '',f6.3)') EDtolerance
        elseif (EDmaxima.eq.all) then
          write(MEMFileId,'(''maxima all'')')
        endif
        if (EDfullcell) then
          write(MEMFileId,'(''fullcell yes'')')
        else
          write(MEMFileId,'(''fullcell no'')')
        endif
        if (EDscale.eq.fract) then
          write(MEMFileId,'(''scale fractional'')')
        elseif (EDmaxima.eq.angst) then
          write(MEMFileId,'(''scale angstrom'')')
        endif
c        write(MEMFileId,'(''plimit '',f9.4)') EDplim
        write(MEMFileId,'(''plimit '',f9.4,'' sigma'')') EDplim
        if(EDAtoms.gt.0) then
          write(Cislo,FormI15) EDAtoms
          call Zhusti(Cislo)
        else
          Cislo='composition'
        endif
        t80='numberofatoms '//Cislo(:idel(Cislo))
        write(MEMFileId,FormA) t80(:idel(t80))
        if (EDcharge) then
          write(MEMFileId,'(''centerofcharge yes'')')
          write(MEMFileId,'(''chlimit '',f9.4)') EDchlimint
          if (AbsChlim) then
            write(MEMFileId,'(''chlimlist '',f9.4,'' absolute'')')
     1                        EDchlimlist
          else
            write(MEMFileId,'(''chlimlist '',f9.4,'' relative'')')
     1                        EDchlimlist
          endif
        elseif (EDmaxima.eq.all) then
          write(MEMFileId,'(''centerofcharge no'')')
        endif
        if (NDim(KPhase).gt.3) then
          if (EDprojection) then
            write(MEMFileId,'(''projection yes'')')
          else
            write(MEMFileId,'(''addborder '',f7.4)') EDaddb
            write(MEMFileId,'(''tlist'')')
            do3200 i=1,NDimI(KPhase)
              write(MEMFileId,'(3f6.2)') tstart(i),tend(i),tstep(i)
3200        continue
            write(MEMFileId,'(''endtlist'')')
          endif
        endif

        write(MEMFileId,'(''# End of EDMA-specific keywords'')')
      endif


c!    Reflection list
      if (IncludeRefs) then
        write(MEMFileId,*)
        call ExportRefl2MEM
      endif
      close(MEMFileId)
9999  continue
      if(allocated(symmcp)) deallocate(symmcp)
      return
      end
      subroutine ExportRefl2MEM
      use Atoms_mod
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'memexport.cmn'
      integer ihkl(6),ph
      real Fo,Fc,A00,B00,sig,dm,Iobs,sigIobs
      character*256 t256
      character*80 t80
      character*22 formatm80,formatBM,formatm90
      logical ExistFile,EqIgCase
      data formatm80/'( i4,13e12.5)'/
      data formatBM/'( i4,3f14.7)'/
      data formatm90/'( i4,2f9.1,3i4)'/
      write(formatm80(2:2),'(i1)') NDim(KPhase)+1
      write(formatm90(2:2),'(i1)') NDim(KPhase)
      write(formatBM(2:2),'(i1)') NDim(KPhase)
      if (RefsSourceCode.eq.Refsm80)then
        t80=fln(:ifln)//'.m80'
        if (ExistFile(t80)) then
          call OpenFile(80,t80,'formatted','unknown')
        else
          t80='The file '//t80(1:idel(t80))//' doesn''t exist.'
          call FeChybne(-1.,-1.,t80,
     2         'Run Refine to create the file.',SeriousError)
          goto 9999
        endif
        F000=0.
        do i=1,NAtCalc
          if(kswa(i).eq.KPhase) then
            j=isf(i)
            if(j.gt.NAtFormula(KPhase)) cycle
            pom=ai(i)*CellVol(1,KPhase)/CellVol(iswa(i),KPhase)
            if(NDimI(KPhase).gt.0) then
              pom=pom*a0(i)
              if(KFA(2,i).eq.1.and.KModA(2,i).ne.0)
     1          pom=pom*uy(2,KModA(2,i),i)
            endif
            if(Radiation(KDatBlock).eq.NeutronRadiation) then
              fpom=ffn(j,KPhase)
            else
              fpom=AtNum(j,KPhase)
            endif
            F000=F000+fpom*pom
          endif
        enddo
        F000=F000*float(NSymm(KPhase))*Centr(KPhase)
        write(MEMFileId,'(''electrons '',f12.4)') F000
        write(MEMFileId,'(''fbegin'')')
1000    read(80,formatm80,end=3000)(ihkl(i),i=1,NDim(KPhase)),Ph,Fo,dm,
     1                             Fc,A00,B00,(dm,i=1,6),sig,dm
        if(ph.ne.KPhase) go to 1000
        write(MEMFileId,formatBM)(0,i=1,NDim(KPhase)),Fo,0.0,0.0005*Fo
1500    read(80,formatm80,end=3000)(ihkl(i),i=1,NDim(KPhase)),ph,Fo,dm,
     1                              Fc,A00,B00,(dm,i=1,6),sig,dm
        if (ihkl(1).eq.999) goto 3000
        if(ph.ne.KPhase) go to 1500
        if (RefsTypeCode.eq.RefsObs)then
          if (abs(Fc).gt.0.00001)then
            A00=A00*Fo/Fc
            B00=B00*Fo/Fc
          else
            A00=0.0
            B00=0.0
          endif
        endif
        write(MEMFileId,formatBM) (ihkl(i),i=1,NDim(KPhase)),A00,B00,sig
2000    goto 1500
3000    continue
        write(MEMFileId,'(''endf'')')
        close(80)
      elseif(RefsSourceCode.eq.Refsm90)then
        if(isPowder) then
          t80=fln(:ifln)//'.rfl'
        else
          t80=fln(:ifln)//'.m90'
        endif
        if(ExistFile(t80)) then
          if(isPowder) then
            open(91,file=t80)
          else
            call OpenDatBlockM90(91,1,t80)
          endif
        else
          t80='The file '//t80(:idel(t80))//' doesn''t exist.'
          call FeChybne(-1.,-1.,t80,' ',SeriousError)
          goto 9999
        endif
        write(MEMFileId,'(''electrons '',f12.4)') 0.
        if(isPowder) then
          write(MEMFileId,'(''dataitemwidths 4 15 15'')')
          write(MEMFileId,'(''dataformat intensity fwhm'')')
        else
          write(MEMFileId,'(''dataitemwidths 4 14 14'')')
        endif
        write(MEMFileId,'(''fbegin'')')
        if(isPowder) then
          write(FormRfl(2:2),'(i1)') NDim(KPhase)
3500      read(91,FormRfl,end=5000)(ihkl(i),i=1,NDim(KPhase)),Iobs,pom
          write(MEMFileId,FormRfl)(ihkl(i),i=1,NDim(KPhase)),Iobs,pom
          go to 3500
        else
          write(MEMFileId,formatBM)(0,i=1,NDim(KPhase)),0.0,0.0,0.1
4000      read(91,FormA256,end=5000) t256
          if(t256.eq.' ') go to 4000
          k=0
          call kus(t256,k,Cislo)
          if(EqIgCase(Cislo,'data')) go to 5000
          read(t256,format91)(ihkl(i),i=1,NDim(KPhase)),Iobs,sigIobs,i,
     1                        i,itw
          if(ihkl(1).eq.999) goto 5000
          if(itw.gt.1.or.itw.le.0) go to 4000
          if(Iobs.gt.0.) then
            Fo=sqrt(Iobs)
          else
            Fo=0.
          endif
          if(Iobs.le..01*SigIobs) then
            Sig=sqrt(SigIobs)/(.2)
          else
            Sig=.5/Fo*SigIobs
          endif
          Sig=sqrt(Sig**2+(blkoef*Fo)**2)
          if (ihkl(1).lt.999)
     1      write(MEMFileId,formatBM)(ihkl(i),i=1,NDim(KPhase)),Fo,0.,
     2                               sig
          goto 4000
        endif
5000    write(MEMFileId,'(''endf'')')
        close(91)
      endif
9999  continue
      return
      end
      function MEMCheckDivision(Pix)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      parameter (eps=0.00001)
      integer Pix(6),cv,os,i,ii
      logical   MEMCheckDivision
      real RPix(6),dummy
      MEMCheckDivision=.true.
      do 100 i=1,NDim(KPhase)
        RPix(i)=float(Pix(i))
100   continue
      do 500 cv=1,NLattVec(KPhase)
        do 600 i=1,NDim(KPhase)
          dummy=RPix(i)*vt6(i,cv,1,KPhase)
          if (abs(dummy-int(dummy)).ge.eps) then
            MEMCheckDivision=.false.
            return
          endif
600     continue
500   continue
      do 1100 os=1,NSymmN(KPhase)
        do 1200 i=1,NDim(KPhase)
          do 1300 ii=1,NDim(KPhase)
            dummy=RPix(ii)*rm6(ii+(i-1)*NDim(KPhase),os,1,KPhase)/
     1            RPix(i)
            if (abs(dummy-int(dummy)).ge.eps) then
              MEMCheckDivision=.false.
              return
            endif
1300      continue
          dummy=RPix(i)*s6(i,os,1,KPhase)
          if (abs(dummy-int(dummy)).ge.eps) then
            MEMCheckDivision=.false.
            return
          endif
1200    continue
1100  continue
      return
      end
