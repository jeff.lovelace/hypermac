      subroutine PwdPrf(KtF)
      use Powder_mod
      use Refine_mod
      use RefPowder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      logical Mpom,odEdw,drzi,skoc,StartAgain,menilFile,odSkipToBragg,
     1        FileDiff,Prekresli,FeYesNo,lpom,PwdCheckTOFInD,EqRV,
     2        CrwLogicQuest
      integer Color,TimeQuestEvent,tnew,tline,FeGetSystemTime,CoDal,
     1        FeMenu,TimeDrzi,BraggCenterOld,RolMenuSelectedQuest
      dimension xtr(3),ytr(3),ihh(6),xsel(5),ysel(5),hh(3),
     1          XManBackgOld(:),YManBackgOld(:)
      character*20 :: LabPrf(19) =
     1  (/'%Quit       ','%Print      ','S%ave       ','X-          ',
     2    'X+          ','X %exactly  ','Y-          ','Y+          ',
     3    'P-          ','P+          ','%Info       ','%Fit Y      ',
     4    'Sh%rink     ','Defa%ult    ','P%nts OFF   ','%Zero ON    ',
     5    'Details ON  ','Draw in d''%s','%Options    '/)
      character*30 Men(4)
      character*32 TitlePrf,TitleM92
      character*80 ch80,Veta
      character*256 EdwStringQuest
      external PwdPrfNic
      allocatable XManBackgOld,YManBackgOld
      data Men/'%Escape','%Make zoom','%Add to exluded regions',
     1         '%Remove from excluded regions'/
      data TitlePrf/'Powder profile based on prf file'/,
     1     TitleM92/'Powder profile based on m92 file'/
      DrawInD=.false.
      NLabIn=3
      BraggCenter=0.
      call iom40(0,0,fln(:ifln)//'.m40')
      if(ErrFlag.eq.-1) then
        call CrlCorrectAtomNames(ich)
        if(ich.ne.0) then
          ErrFlag=1
          go to 9999
        endif
      else if(ErrFlag.ne.0) then
        go to 9999
      endif
      StartAgain=.false.
1100  lpom=DrawInD
      DrawInD=.false.
      if(ExistM90) then
        call PwdM92Nacti
      else
        PrfPointsFormat='(f10.3,3e15.6,f10.3,i5,99e15.6)'
      endif
      KDatBlockIn=KDatBlock
      KDatBlockNew=0
      ktFile=ktf
      call PwdPrfNacti(ktFile)
      if(ErrFlag.ne.0) go to 9999
      allocate(CalcPArr(npnts),CalcPPArr(npnts),BkgArr(npnts),
     1         DifArr(npnts))
      call PwdSetTOF(KDatBlock)
      n=100
      allocate(PeakXPos(n),PeakInt(n),PeakFWHM(n),PeakBckg(3,n),
     1         PeakEta(n),PeakXPosI(n),PeakPor(n))
      DrawInD=lpom
      if(DrawInD) then
        call PwdX2D(XPwd,NPnts)
        call PwdX2D(XManBackg(1,KDatBlock),NManBackg(KDatBlock))
        if(.not.isTOF) then
          j=NPnts
          do i=1,NPnts/2
            pom=XPwd(j)
            XPwd(j)=XPwd(i)
            XPwd(i)=pom
            pom=YoPwd(j)
            YoPwd(j)=YoPwd(i)
            YoPwd(i)=pom
            pom=YcPwd(j)
            YcPwd(j)=YcPwd(i)
            YcPwd(i)=pom
            k=YfPwd(j)
            YfPwd(j)=YfPwd(i)
            YfPwd(i)=k
            j=j-1
          enddo
        endif
        do i=1,NBragg
          if(isTOF) then
            BraggArr(i)=PwdTOF2D(BraggArr(i))
          else
            if(KRefLam(KDatBlock).eq.1) then
              BraggArr(i)=PwdTwoTh2D(BraggArr(i),LamPwd(1,KDatBlock))
            else
              BraggArr(i)=PwdTwoTh2D(BraggArr(i),LamAve(KDatBlock))
            endif
          endif
        enddo
        if(.not.isTOF) then
          j=NBragg
          do i=1,NBragg/2
            pom=BraggArr(j)
            BraggArr(j)=BraggArr(i)
            BraggArr(i)=pom
            pom=MultArr(j)
            MultArr(j)=MultArr(i)
            MultArr(i)=pom
            pom=ShiftArr(j)
            ShiftArr(j)=ShiftArr(i)
            ShiftArr(i)=pom
            pom=ypeakArr(j)
            ypeakArr(j)=ypeakArr(i)
            ypeakArr(i)=pom
            ipom=KlicArr(j)
            KlicArr(j)=KlicArr(i)
            KlicArr(i)=ipom
            do k=1,NDim(KPhase)
              ipom=indArr(k,j)
              indArr(k,j)=indArr(k,i)
              indArr(k,i)=ipom
            enddo
            j=j-1
          enddo
        endif
      endif
      KPhase=KPhaseBasic
      call CopyFile(fln(:ifln)//'.m41',PreviousM41)
      call CopyFile(fln(:ifln)//'.m50',PreviousM50)
      call CopyFile(fln(:ifln)//'.m90',fln(:ifln)//'.z90')
      NManBackgOld=NManBackg(KDatBlock)
      if(NManBackgOld.gt.0) then
        allocate(XManBackgOld(NManBackgOld),YManBackgOld(NManBackgOld))
        call CopyVek(XManBackg(1,KDatBlock),XManBackgOld,NManBackgOld)
        call CopyVek(YManBackg(1,KDatBlock),YManBackgOld,NManBackgOld)
      endif
      CalcDer=.false.
      NlamPwd=0
      DegMin=-1.
      DegMax=-1.
      IntMax=-1.
      menilFile=.false.
      smallKrok=4.
      HardCopy=0
      KresliZero=.false.
      KresliDetails=.false.
      KresliObsPoints=.true.
      KresliManBackg=.false.
      whichLine=3
      call PwdPrfOptions(ich,.true.,menilFile,ktFile,istt)
      call FeFlush
1500  id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,-1,
     1                      -1)
      CheckMouse=.true.
      call FeMakeGrWin(5.,120.,60.,30.)
      call FeMakeAcWin(5.,0.,0.,10.)
      call FeBottomInfo('#prazdno#')
      if(NDatBlock.gt.1) then
        dpom=0.
        do i=1,NDatBlock
          if(DataType(i).eq.1) then
            MenuDatBlockUse(i)=0
          else
            MenuDatBlockUse(i)=1
          endif
          dpom=max(dpom,FeTxLength(MenuDatBlock(i)))
        enddo
        dpom=dpom+EdwYd+2.*EdwMarginSize
        tpom=300.
        ypom=10.
        Veta='S%witch datblock'
        xpom=tpom+FeTxLength(Veta)+2.
        call FeQuestAbsRolMenuMake(id,tpom,ypom+6.,xpom,ypom,Veta,'L',
     1                             dpom,EdwYd,1)
        nRolMenuDatBlock=RolMenuLastMade
        call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,MenuDatBlock,
     1    MenuDatBlockUse,NDatBlock,KDatBlock)
      endif
      call UnitMat(F2O,3)
      call FeSetTransXo2X(1.,40.,22.,0.,.false.)
      YMaxPlW=YMaxAcWin-6.
      XMaxPlW=XMaxAcWin-3.
      YMaxPlC=YMaxPlW-2.
      XMaxPlC=XMaxPlW-2.
      xpom=12.
      ButtWidth1=XMaxBasWin-XMaxAcWin-2.*xpom
      xpom=XMaxAcWin+xpom
      xleft=xpom
      xright=xpom+ButtWidth1
      ButtWidth2=ButtWidth1/2.-6.
      ButtWidth=ButtWidth1
      il=2
      do i=1,19
        call FeQuestButtonMake(id,xpom,il,ButtWidth,ButYd,
     1                            LabPrf(i))
        k=ButtonOff
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtPrint=ButtonLastMade
        else if(i.eq.3) then
          nButtSave=ButtonLastMade
          ButtWidth=ButtWidth2
        else if(i.eq.4) then
          nButtXMinus=ButtonLastMade
          xpom=xright-ButtWidth2
          il=il-1
        else if(i.eq.5) then
          nButtXPlus=ButtonLastMade
          ButtWidth=ButtWidth1
          xpom=xleft
        else if(i.eq.6) then
          nButtXExactly=ButtonLastMade
          ButtWidth=ButtWidth2
        else if(i.eq.7) then
          nButtYMinus=ButtonLastMade
          xpom=xright-ButtWidth2
          il=il-1
        else if(i.eq.8) then
          nButtYPlus=ButtonLastMade
          xpom=xleft
        else if(i.eq.9) then
          nButtPminus=ButtonLastMade
          if(NBragg.le.0) k=ButtonDisabled
          xpom=xright-ButtWidth2
          il=il-1
        else if(i.eq.10) then
          if(NBragg.le.0) k=ButtonDisabled
          nButtPPlus=ButtonLastMade
          ButtWidth=ButtWidth1
          xpom=xleft
        else if(i.eq.11) then
          nButtInfo=ButtonLastMade
        else if(i.eq.12) then
          nButtFitY=ButtonLastMade
        else if(i.eq.13) then
          nButtFit=ButtonLastMade
        else if(i.eq.14) then
          nButtDefault=ButtonLastMade
        else if(i.eq.15) then
          nButtPoints=ButtonLastMade
        else if(i.eq.16) then
          nButtZero=ButtonLastMade
        else if(i.eq.17) then
          nButtDetails=ButtonLastMade
        else if(i.eq.18) then
          nButtDrawInD=ButtonLastMade
          if(isTOF.and.TOFInD) then
            call FeQuestButtonClose(ButtonLastMade)
            il=il-1
            cycle
          else
            if(DrawInD) then
              if(isTOF) then
                Veta=LabPrf(18)(:idel(LabPrf(18))-4)//'t''s'
              else
                Veta=LabPrf(18)(:idel(LabPrf(18))-4)//'2th''s'
              endif
              call FeQuestButtonLabelChange(ButtonLastMade,Veta)
            endif
          endif
        else if(i.eq.19) then
          nButtOpt=ButtonLastMade
        endif
        il=il+1
        call FeQuestButtonOpen(ButtonLastMade,k)
      enddo
      if(DrawInD) then
        Veta='Go to d'
      else
        if(isTOF) then
          Veta='Go to t'
        else
          Veta='Go to 2th'
        endif
      endif
      if(NBragg.gt.0) Veta=Veta(:idel(Veta))//'/hkl'
      il=il+1
      tpom=(xleft+xright)*.5
      call FeQuestEdwMake(id,tpom,-10*il+8,xleft,il,Veta,'C',
     1                    xright-xleft,EdwYd,1)
      nEdwGoTo=EdwLastMade
      call FeQuestStringEdwOpen(nEdwGoTo,' ')
      il=il+2
      Veta='Manual background:'
      call FeQuestLblMake(id,tpom,il,Veta,'C','B')
      Veta='Create new'
      do i=1,2
        il=il+1
        call FeQuestButtonMake(id,xpom,il,ButtWidth,ButYd,Veta)
        if(NManBackg(KDatBlock).le.0) then
          k=ButtonDisabled
        else
          k=ButtonOff
        endif
        if(i.eq.1) then
          nButtBackgNew=ButtonLastMade
          Veta='Back%groud ON'
          k=ButtonOff
        else if(i.eq.2) then
          nButtBackgOn=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,k)
      enddo
      DegMin=XPwd(1)
      DegMax=XPwd(Npnts)
      pom=(YMaxBasWin+YMaxGrWin)*.5
      call FeQuestAbsLblMake(id,XCenGrWin,pom,' ','C','N')
      call FeQuestLblOff(LblLastMade)
      nLblTitle=LblLastMade
      call PwdPrfKresli(-1,1)
      if(ErrFlag.ne.0) go to 9999
      delkaKroku=XLenAcWin/(nen-nst)
      npulo=0
      xpom=XMinGrWin+2.
      ypom=YMinGrWin
      call FeQuestAbsSbwMake(id,xpom,ypom,XLenGrWin-4.,
     1                       YLenGrWin,1,CutTextNone,SbwHorizontal)
      nSbw=SbwLastMade
      call FeQuestSbwPureOpen(SbwLastMade,nst,nen,Npnts,0,0,0)
      xcenpl=XCenAcWin
      ycenpl=YCenAcWin
      AskIfQuit=.true.
2000  call FeQuestEvent(id,ich)
      if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtQuit).or.
     1   (CheckType.eq.EventGlobal.and.CheckNumber.eq.GlobalClose))
     2  then
        StartAgain=.false.
        go to 6000
      endif
      TimeQuestEvent=FeGetSystemTime()
      odEdw=.false.
      odSkipToBragg=.false.
      HardCopy=0
      ErrFlag=0
      drzi=.false.
      iButOff=0
      if(CheckType.eq.0) go to 6000
      if(CheckType.eq.EventMouse) then
        if(CheckNumber.eq.JeLeftDown) then
          if(YPos.gt.y1bragg.and.
     1       YPos.lt.y2bragg+PruhBragg*float(NPhase-1)) then
            k=ifix((YPos-y1Bragg)/PruhBragg)+1
            call PwdPrfBraggInfo(k)
          else
            call FeKillInfoWin
            if(KresliManBackg) then
              XPom=PwdPrfX2Th(XPos)
              call locate(XManBackg(1,KDatBlock),NManBackg(KDatBlock),
     1                    XPom,i)
              XPosMin=PwdPrfTh2X(XManBackg(i,KDatBlock))
              YPosMin=PwdPrfInt2Y(YManBackg(i,KDatBlock)*
     1                            Sc(2,KDatBlock))
              D1=sqrt((XPos-XPosMin)**2+(YPos-YPosMin)**2)
              if(i.lt.NManBackg(KDatBlock)) then
                XPosMax=PwdPrfTh2X(XManBackg(i+1,KDatBlock))
                YPosMax=PwdPrfInt2Y(YManBackg(i+1,KDatBlock)*
     1                              Sc(2,KDatBlock))
                D2=sqrt((XPos-XPosMax)**2+(YPos-YPosMax)**2)
              else
                XPosMax=XPosMin
                YPosMax=YPosMin
                D2=999999.
              endif
              D=sqrt((XPosMax-XPosMin)**2+(YPosMax-YPosMin)**2)
              if(D1.gt.10..and.D2.gt.10.) then
                if(D1+D2.gt.D+5.) go to 2170
                call ReallocManBackg(NManBackg(KDatBlock)+1)
                do j=NManBackg(KDatBlock),i+1,-1
                  XManBackg(j+1,KDatBlock)=XManBackg(j,KDatBlock)
                  YManBackg(j+1,KDatBlock)=YManBackg(j,KDatBlock)
                enddo
                NManBackg(KDatBlock)=NManBackg(KDatBlock)+1
                XManBackg(i+1,KDatBlock)=PwdPrfX2Th(XPos)
                YManBackg(i+1,KDatBlock)=PwdPrfY2Int(YPos)/
     1                                   Sc(2,KDatBlock)
                i=i+1
                XPosMin=XPos
                YPosMin=YPos
              else
                if(D2.lt.D1) then
                  XPosMin=XPosMax
                  YPosMin=YPosMax
                  D1=D2
                  i=i+1
                endif
              endif
              if(XManBackg(i,KDatBlock).ge.DegMin.and.
     1           XManBackg(i,KDatBlock).le.DegMax) then
                if(i.le.1) then
                  xsel1=PwdPrfTh2X(XManBackg(i,KDatBlock))
                  ysel1=PwdPrfInt2Y(YManBackg(i,KDatBlock)*
     1                              Sc(2,KDatBlock))
                else
                  xsel1=PwdPrfTh2X(XManBackg(i-1,KDatBlock))
                  ysel1=PwdPrfInt2Y(YManBackg(i-1,KDatBlock)*
     1                              Sc(2,KDatBlock))
                endif
                if(i.ge.NManBackg(KDatBlock)) then
                  xsel3=PwdPrfTh2X(XManBackg(i,KDatBlock))
                  ysel3=PwdPrfInt2Y(YManBackg(i,KDatBlock)*
     1                              Sc(2,KDatBlock))
                else
                  xsel3=PwdPrfTh2X(XManBackg(i+1,KDatBlock))
                  ysel3=PwdPrfInt2Y(YManBackg(i+1,KDatBlock)*
     1                              Sc(2,KDatBlock))
                endif
                if(xsel1.lt.XMinPlC) then
                  call PwdPrfPrusX(xsel1,ysel1,
     1                             XPosMin,YPosMin,
     2                             xsel(1),ysel(1),XMinPlC)
                else
                  xsel(1)=xsel1
                  ysel(1)=ysel1
                endif
                if(xsel3.gt.XMaxPlC) then
                  call PwdPrfPrusX(xsel3,ysel3,
     1                             XPosMin,YPosMin,
     2                             xsel(3),ysel(3),XMaxPlC)
                else
                  xsel(3)=xsel3
                  ysel(3)=ysel3
                endif
                call FeReleaseOutput
                call FeMoveMouseTo(XPosMin,YPosMin)
                call FePlotMode('E')
                TakeMouseMove=.true.
                XPosOld=XPosMin
                YPosOld=YPosMin
                call FeCircle(XPosOld,YPosOld,4.,Green)
                EventType=0
                EventNumber=0
2100            call FeEvent(1)
                if(EventType.eq.0) go to 2100
                if(EventType.eq.EventMouse) then
                  if(EventNumber.eq.JeMove) then
                    if((XPos.le.xsel(1).or.XPos.ge.xsel(3)).and.
     1                 (i.ne.1.and.i.ne.NManBackg(KDatBlock))) then
                      do j=i+1,NManBackg(KDatBlock)
                        XManBackg(j-1,KDatBlock)=XManBackg(j,KDatBlock)
                        YManBackg(j-1,KDatBlock)=YManBackg(j,KDatBlock)
                      enddo
                      NManBackg(KDatBlock)=NManBackg(KDatBlock)-1
                      call FePlotMode('N')
                      Prekresli=.true.
                      EventType=0
                      EventNumber=0
                      go to 4800
                    endif
                    call FeCircle(XPosOld,YPosOld,4.,Green)
                    xsel(2)=XPosOld
                    ysel(2)=YPosOld
                    call FePolyline(3,xsel,ysel,Green)
                    call FeCircle(XPos,YPos,4.,Green)
                    xsel(2)=XPos
                    ysel(2)=YPos
                    if(xsel1.lt.XMinPlC) then
                      call PwdPrfPrusX(xsel1,ysel1,
     1                                 xsel(2),ysel(2),
     2                                 xsel(1),ysel(1),XMinPlC)
                    else
                      xsel(1)=xsel1
                      ysel(1)=ysel1
                    endif
                    if(xsel3.gt.XMaxPlC) then
                      call PwdPrfPrusX(xsel3,ysel3,
     1                                 xsel(2),ysel(2),
     2                                 xsel(3),ysel(3),XMaxPlC)
                    else
                      xsel(3)=xsel3
                      ysel(3)=ysel3
                    endif
                    call FePolyline(3,xsel,ysel,Green)
                    XPosOld=XPos
                    YPosOld=YPos
                    go to 2100
                  else if(EventNumber.eq.JeLeftUp) then
                    call FePlotMode('N')
                    XManBackg(i,KDatBlock)=PwdPrfX2Th(XPos)
                    YManBackg(i,KDatBlock)=PwdPrfY2Int(YPos)/
     1                                     Sc(2,KDatBlock)
                    Prekresli=.true.
                    EventType=0
                    EventNumber=0
                    go to 4800
                  endif
                endif
              endif
            endif
2170        if(XPos.ge.XMinAcWin.and.XPos.le.XMaxAcWin.and.
     1         YPos.ge.yminplC.and.YPos.le.ymaxplC) then
              xsel(1)=XPos
              ysel(1)=yminplC
              xsel(2)=XPos
              ysel(2)=YPos
              xsel(3)=XPos
              ysel(3)=YPos
              xsel(4)=XPos
              ysel(4)=yminplC
              xsel(5)=xsel(1)
              ysel(5)=ysel(1)
              call FePlotMode('E')
              ip=2
              call FePolyLine(2,xsel,ysel,Green)
              TakeMouseMove=.true.
              Prekresli=.false.
              if(DeferredOutput) call FeReleaseOutput
2200          call FeEvent(1)
              if(EventType.eq.0) go to 2200
2250          if(EventType.eq.EventMouse) then
                if(EventNumber.eq.JeMove) then
                  if(YPos.ge.yminAcWin.and.YPos.le.ymaxAcWin.and.
     1               XPos.ge.xminAcWin.and.XPos.le.xmaxAcWin) then
                    call FePolyLine(ip,xsel,ysel,Green)
                    ysel(2)=YPos
                    xsel(3)=XPos
                    ysel(3)=YPos
                    xsel(4)=XPos
                    if(xsel(2).ne.xsel(3)) then
                      ip=5
                    else
                      ip=2
                    endif
                    call FePolyLine(ip,xsel,ysel,Green)
                  endif
                  go to 2200
                else if(EventNumber.eq.JeLeftUp) then
                  TakeMouseMove=.false.
                  DegMinP=PwdPrfX2Th(min(xsel(1),xsel(3)))
                  DegMaxP=PwdPrfX2Th(max(xsel(1),xsel(3)))
                  call locate(XPwd(nst),nen-nst+1,DegMinP,i)
                  call locate(XPwd(nst),nen-nst+1,DegMaxP,j)
                  if(j.le.i) then
                    go to 2270
                  else
                    go to 2200
                  endif
                else if(EventNumber.eq.JeLeftDown) then
                  DegMinP=PwdPrfX2Th(min(xsel(1),xsel(3)))
                  DegMaxP=PwdPrfX2Th(max(xsel(1),xsel(3)))
                  call locate(XPwd(nst),nen-nst+1,DegMinP,i)
                  call locate(XPwd(nst),nen-nst+1,DegMaxP,j)
                  if(i+10.ge.j) then
                    go to 2270
                  else
                    DegMin=DegMinP
                    DegMax=DegMaxP
                  endif
                  IntMax=PwdPrfY2Int(ysel(3))
                  Prekresli=.true.
                  call FeDeferOutput
                else if(EventNumber.eq.JeRightDown) then
                  call FePlotMode('N')
                  CoDal=FeMenu(-1.,-1.,men,1,4,1,0)
                  call FePlotMode('E')
                  if(CoDal.eq.3.or.CoDal.eq.4) then
                    PomMin=PwdPrfX2Th(min(xsel(1),xsel(3)))
                    PomMax=PwdPrfX2Th(max(xsel(1),xsel(3)))
                    if(DrawInD) then
                      if(isTOF) then
                        PomMaxP=PwdD2TOF(PomMax)
                        PomMinP=PwdD2TOF(PomMin)
                        DegMinP=PwdD2TOF(DegMin)
                        DegMaxP=PwdD2TOF(DegMax)
                      else
                        if(KRefLam(KDatBlock).eq.1) then
                          PomMaxP=PwdD2TwoTh(PomMin,LamPwd(1,KDatBlock))
                          PomMinP=PwdD2TwoTh(PomMax,LamPwd(1,KDatBlock))
                          DegMaxP=PwdD2TwoTh(DegMin,LamPwd(1,KDatBlock))
                          DegMinP=PwdD2TwoTh(DegMax,LamPwd(1,KDatBlock))
                        else
                          PomMaxP=PwdD2TwoTh(PomMin,LamAve(KDatBlock))
                          PomMinP=PwdD2TwoTh(PomMax,LamAve(KDatBlock))
                          DegMaxP=PwdD2TwoTh(DegMin,LamAve(KDatBlock))
                          DegMinP=PwdD2TwoTh(DegMax,LamAve(KDatBlock))
                        endif
                      endif
                    else
                      DegMinP=DegMin
                      DegMaxP=DegMax
                      PomMinP=PomMin
                      PomMaxP=PomMax
                    endif
                  endif
                  if(CoDal.eq.2) then
                    EventType=EventMouse
                    EventNumber=JeLeftDown
                    go to 2250
                  else if(CoDal.eq.3) then
                    NSkipPwd(KDatBlock)=NSkipPwd(KDatBlock)+1
                    n=NSkipPwd(KDatBlock)
                    SkipPwdFr(n,KDatBlock)=max(PomMinP,DegMinP)
                    SkipPwdTo(n,KDatBlock)=min(PomMaxP,DegMaxP)
                    call PwdOrderSkipRegions
                    call iom40(1,0,fln(:ifln)//'.m40')
                    Prekresli=.true.
                  else if(CoDal.eq.4) then
                    call PwdRemoveSkipRegion(max(PomMinP,DegMinP),
     1                                       min(PomMaxP,DegMaxP))
                    call iom40(1,0,fln(:ifln)//'.m40')
                    Prekresli=.true.
                  endif
                  go to 2270
                else
                  go to 2200
                endif
              else if(EventType.eq.EventASCII) then
                if(EventNumber.ne.JeEscape) go to 2200
              endif
2270          call FePolyLine(ip,xsel,ysel,Green)
              call FePlotMode('N')
              if(Prekresli) then
                EventType=0
                EventNumber=0
                go to 4800
              endif
            endif
          endif
          EventType=0
          EventNumber=0
          go to 2000
        endif
      endif
2300  if(CheckType.eq.EventButton) then
        call FeKillInfoWin
        if(CheckNumber.eq.nButtXPlus) then
          delkaKroku=delkaKroku*expandX
          drzi=.true.
        else if(CheckNumber.eq.nButtXMinus) then
          delkaKroku=delkaKroku/shrinkX
          drzi=.true.
        else if(CheckNumber.eq.nButtYPlus) then
          IntMax=IntMax/expandY
          drzi=.true.
        else if(CheckNumber.eq.nButtYMinus) then
          IntMax=IntMax*shrinkY
          drzi=.true.
        else
          drzi=.false.
          call FeReleaseOutput
        endif
        if(drzi) go to 4700
        iButOff=CheckNumber
        if(CheckNumber.eq.nButtDrawInD) then
          StartAgain=.true.
          DrawInD=.not.DrawInD
          go to 6000
        else if(CheckNumber.eq.nButtSave.or.
     1          CheckNumber.eq.nButtPrint) then
          if(CheckNumber.eq.nButtSave) then
            ipom=6
            call FeSavePicture('plot',ipom,1)
            if(HardCopy.eq.0) go to 2000
          else
            call FePrintPicture(ich)
            if(ich.ne.0) go to 2000
          endif
          call FeHardCopy(HardCopy,'open')
          if(InvertWhiteBlack.or.
     1       (HardCopy.ne.HardCopyBMP.and.
     2        HardCopy.ne.HardCopyPCX)) then
            if(InvertWhiteBlack) then
              do i=1,8
                if(ColorPole(i).eq.White) then
                  ColorPole(i)=Black
                else if(ColorPole(i).eq.Black) then
                  ColorPole(i)=White
                endif
              enddo
            endif
            call FeFillRectangle(XMinGrWin,XMaxGrWin,YMinGrWin,
     1                           YMaxGrWin,4,0,0,Black)
            call PwdPrfKresli(0,0)
            if(InvertWhiteBlack) then
              do i=1,8
                if(ColorPole(i).eq.White) then
                  ColorPole(i)=Black
                else if(ColorPole(i).eq.Black) then
                  ColorPole(i)=White
                endif
              enddo
            endif
          endif
          call FeHardCopy(HardCopy,'close')
          if(CheckNumber.eq.nButtPrint) then
            call FePrintFile(PSPrinter,HCFileName,ich)
            call DeleteFile(HCFileName)
            call FeTmpFilesClear(HCFileName)
          endif
          HardCopy=0
          go to 4800
        else if(CheckNumber.eq.nButtPPlus.or.CheckNumber.eq.nButtPMinus)
     1    then
          if(CheckNumber.eq.nButtPPlus) then
            izn= 1
          else
            izn=-1
          endif
          BraggCenterOld=BraggCenter
          if(BraggCenter.le.0) then
            pul=.5*(DegMax+DegMin)
            call locate(BraggArr,NBragg,pul,mst)
            if(izn.lt.0) then
              mst=min(mst+1,NBragg)
            else
              mst=max(mst-1,1)
            endif
          else
            mst=BraggCenter
          endif
          m=mst
2400      m=m+izn
          if(m.lt.1.or.m.gt.NBragg) go to 2000
          if(abs(BraggArr(m)-BraggArr(mst)).lt..001) go to 2400
          BraggCenter=m
          pul=.5*(DegMax-DegMin)
          if(BraggArr(m)-pul.le.XPwd(1)) then
            DegMin=XPwd(1)
            DegMax=min(XPwd(Npnts),DegMin+2.*pul)
          else if(BraggArr(m)+pul.ge.XPwd(Npnts)) then
            DegMax=XPwd(Npnts)
            DegMin=max(XPwd(1),DegMax-2.*pul)
          else
            DegMin=BraggArr(m)-pul
            DegMax=BraggArr(m)+pul
          endif
          th=BraggArr(m)
          odSkipToBragg=.true.
          go to 4800
        else if(CheckNumber.eq.nButtDetails) then
          if(kType.eq.0.or.NBragg.le.0.or.KtFile.ne.0) then
            call FeMsgOut(-1.,-1.,'The component function cannot be '//
     1        'calculated for this plot')
            go to 4900
          endif
          KresliDetails=.not.KresliDetails
          if(KresliDetails) then
            Veta=LabPrf(17)(:idel(LabPrf(17))-1)//'FF'
          else
            Veta=LabPrf(17)
          endif
          call FeQuestButtonLabelChange(nButtDetails,Veta)
          go to  4800
        else if(CheckNumber.eq.nButtInfo) then
          if(OpSystem.le.0) then
            call FePlotMode('E')
            Color=Yellow
          else
            Color=BlackXOR
          endif
          call FeReleaseOutput
          ZarazkaL=PwdPrfTh2X(XPwd(nst))
          ZarazkaR=PwdPrfTh2X(XPwd(nen))
          xtr(1)=xcenpl
          xtr(2)=xtr(1)
          ytr(1)=YminPlW
          ytr(2)=YmaxPlW
          call FePolyLine(2,xtr,ytr,Color)
          call FeMoveMouseTo(xcenpl,ycenpl)
          itxt=0
          Mpom=TakeMouseMove
          TakeMouseMove=.true.
          tline=-1
2500      call FeEvent(1)
          tnew=FeGetSystemTime()
          skoc=.false.
          if(itxt.eq.0.and.tline.ge.0.and.tnew-tline.ge.1000) then
            skoc=.true.
            go to 2540
          endif
          if(EventType.eq.0) go to 2500
2540      if(skoc.or.abs(XPos-xtr(1)).gt..5) then
            if(skoc) go to 2550
            if(XPos.gt.ZarazkaR) then
              xkam=ZarazkaR
            else if(XPos.lt.ZarazkaL) then
              xkam=ZarazkaL
            else
              xkam=XPos
            endif
            if(xtr(1).ne.XPos) then
              call FePolyLine(2,xtr,ytr,Color)
              xtr(1)=xkam
              xtr(2)=xkam
              tline=FeGetSystemTime()
              th=PwdPrfX2Th(xkam)
              if(itxt.eq.1) then
                call PwdPrfInfoObnova
                itxt=0
              endif
              call FePolyLine(2,xtr,ytr,Color)
              go to 2600
            endif
2550        call locate(XPwd(nst),nen-nst+1,th,j)
            skoc=.false.
            tline=tnew
            if(j.ne.0) then
              j=j+nst-1
              pom=XPwd(j+1)-XPwd(j)
              wright=(th-XPwd(j))/pom
              wleft=(XPwd(j+1)-th)/pom
              HDif=wleft*DifArr(j)+wright*DifArr(j+1)
              HObs=wleft*abs(YoPwd(j))+wright*abs(YoPwd(j+1))
              HCalc=wleft*YcPwd(j)+wright*YcPwd(j+1)
              HTh=wleft*XPwd(j)+wright*XPwd(j+1)
              if(KresliBragg) then
                call locate(BraggArr,NBragg,th,m)
                m=min(m,NBragg)
                m=max(m,1)
              else
                go to 2570
              endif
              HBragg=0.
              do i=1,6
                ihh(i)=0
              enddo
              if(m.ne.0) then
                plim=abs(PwdPrfX2Th(6.)-PwdPrfX2Th(0.))
                if(abs(th-BraggArr(m)).le.abs(th-BraggArr(m+1)))
     1            then
                  if(abs(BraggArr(m)-th).le.plim)
     1              HBragg=BraggArr(m)+ShiftArr(m)
                  j=m
                else
                  if(abs(BraggArr(m+1)-th).le.plim)
     1              HBragg=BraggArr(m+1)+ShiftArr(m+1)
                  j=m+1
                endif
              endif
2570          call PwdPrfInfo(HTh,HCalc,HObs,HDif,HBragg,xkam)
              itxt=1
            endif
          endif
2600      if(EventType.eq.EventKey.and.
     1       (EventNumber.eq.JeRight.or.EventNumber.eq.JeLeft)) then
            if(EventNumber.eq.JeRight) then
              XPos=XPos+1.1
            else
              XPos=XPos-1.1
            endif
            call FeMoveMouseTo(XPos,YPos)
            go to 2500
          endif
          if((EventType.ne.EventMouse.or.EventNumber.ne.JeLeftDown).and.
     1       (EventType.ne.EventKey.or.EventNumber.ne.JeEscape))
     2        go to 2500
          call FePolyLine(2,xtr,ytr,Color)
          if(itxt.eq.1) call PwdPrfInfoObnova
          if(OpSystem.le.0) call FePlotMode('N')
          TakeMouseMove=Mpom
          go to 4900
        else if(CheckNumber.eq.nButtFit) then
          DegMin=XPwd(1)
          DegMax=XPwd(Npnts)
          IntMax=0.
          go to 4800
        else if(CheckNumber.eq.nButtXExactly) then
          idp=NextQuestId()
          xqdp=170.
          il=2
          call FeQuestCreate(idp,-1.,-1.,xqdp,il,' ',0,LightGray,0,0)
          il=1
          tpom=5.
          ch80='Nex X(m%in):'
          dpom=80.
          xpom=tpom+FeTxLengthUnder(ch80)+10.
          call FeQuestEdwMake(idp,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,DegMin,.false.,.false.)
          nEdwXMin=EdwLastMade
          il=il+1
          ch80='Nex X(m%ax):'
          call FeQuestEdwMake(idp,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,DegMax,.false.,.false.)
          nEdwXMax=EdwLastMade
2610      call FeQuestEvent(idp,ichp)
          if(CheckType.eq.EventGlobal) then
            go to 2610
          else if(CheckType.ne.0) then
            call NebylOsetren
            go to 2610
          endif
          if(ichp.eq.0) then
            call FeQuestRealFromEdw(nEdwXMin,DegMinNew)
            call FeQuestRealFromEdw(nEdwXMax,DegMaxNew)
            if(DegMaxNew.gt.DegMinNew) then
              DegMin=max(DegMinNew,XPwd(1    ))
              DegMax=min(DegMaxNew,XPwd(Npnts))
              IntMax=0.
            else
              ichp=1
            endif
          endif
          call FeQuestRemove(idp)
          if(ichp.eq.0) go to 4800
        else if(CheckNumber.eq.nButtDefault) then
          call locate(XPwd,Npnts,.5*(DegMax+DegMin),ipul)
          idp=nint(XLenAcWin/stepIni)
          nst=ipul-idp/2
          nen=nst+idp
          if(nen.gt.Npnts) then
            nst=Npnts-idp
            nen=Npnts
          else if(nst.lt.1) then
            nst=1
            nen=idp
          endif
          DegMin=XPwd(nst)
          DegMax=XPwd(nen)
          IntMax=0.
          go to 4800
        else if(CheckNumber.eq.nButtPoints)then
          KresliObsPoints=.not.KresliObsPoints
          if(KresliObsPoints)then
            Veta=LabPrf(15)
          else
            Veta=LabPrf(15)(:idel(LabPrf(15))-2)//'N'
          endif
          call FeQuestButtonLabelChange(nButtPoints,Veta)
          go to 4800
        else if(CheckNumber.eq.nButtZero) then
          KresliZero=.not.KresliZero
          if(KresliZero) then
            Veta=LabPrf(16)(:idel(LabPrf(16))-1)//'FF'
          else
            Veta=LabPrf(16)
          endif
          call FeQuestButtonLabelChange(nButtZero,Veta)
          go to 4800
        else if(CheckNumber.eq.nButtFitY) then
          CalcMax=-1.
          ObsMax=-1.
          do i=1,Npnts
            if(XPwd(i).lt.DegMin) cycle
            if(XPwd(i).gt.DegMax) exit
            CalcMax=max(CalcMax,YcPwd(i))
            if(KresliObsPoints.or.KresliObsLine)
     1        ObsMax=max(ObsMax,abs(YoPwd(i)))
          enddo
          IntMax=max(CalcMax,ObsMax)
          if(KresliObsPoints) then
            pom=4.*IntMax/(ymaxplW-yminplW)
            if(CalcMax-ObsMax.lt.pom)
     1        IntMax=IntMax+float(nint(pom))
          endif
          go to  4800
        else if(CheckNumber.eq.nButtOpt) then
          call PwdPrfOptions(ich,.false.,menilFile,ktFile,istt)
          if(menilFile) then
            call FeQuestRemove(id)
            go to 1500
          endif
          if(ich.eq.0) then
            CheckNumber=nButtOpt
            go to 4800
          else
            go to 2000
          endif
        else if(CheckNumber.eq.nButtBackgNew) then
          call PwdSetManualBackg(ichp)
          if(ichp.eq.0) then
            if(DrawInD) then
              call PwdD2X(XPwd,NPnts)
              call PwdD2X(XManBackg(1,KDatBlock),NManBackg(KDatBlock))
            endif
            call PwdUpdateReflFiles
            Veta='Back%groud OFF'
            call FeQuestButtonLabelChange(nButtBackgOn,Veta)
            call FeQuestButtonOff(nButtBackgOn)
            KresliManBackg=.true.
            if(DrawInD) then
              call PwdX2D(XPwd,NPnts)
              call PwdX2D(XManBackg(1,KDatBlock),NManBackg(KDatBlock))
            endif
            go to 4800
          endif
          go to 4900
        else if(CheckNumber.eq.nButtBackgOn) then
          KresliManBackg=.not.KresliManBackg
          if(KresliManBackg) then
            Veta='Back%groud OFF'
          else
            Veta='Back%groud ON'
          endif
          call FeQuestButtonLabelChange(nButtBackgOn,Veta)
!          if(DrawInD) call PwdX2D(XManBackg(1,KDatBlock),
!     1                            NManBackg(KDatBlock))
          go to 4800
        else
          go to 4900
        endif
      endif
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwGoTo) then
        ch80=EdwStringQuest(nEdwGoTo)
        call FeQuestStringEdwOpen(nEdwGoTo,' ')
        idp=idel(ch80)
        if(idp.le.0) go to 2000
        k=0
        call StToInt(ch80,k,ihh,NDim(KPhase),.false.,ich)
        if(ich.eq.0) then
          call FromIndSinthl(ihh,hh,sinthl,sinthlq,1,0)
          if(DrawInD) then
            th=.5/sinthl
          else
            if(isTOF) then
              th=PwdD2TOF(.5/sinthl)
            else
              if(KRefLam(KDatBlock).eq.1) then
                pom=min(1.,sinthl*LamPwd(1,KDatBlock))
              else
                pom=min(1.,sinthl*LamAvePwd(KDatBlock))
              endif
              th=2.*asin(pom)/torad
            endif
          endif
          zerosh=0.
          call locate(BraggArr,NBragg,th,m)
          if(m.gt.0) then
            zerosh=ShiftArr(m)
            if(m.lt.NBragg) then
              if(abs(th-BraggArr(m)).gt.abs(th-BraggArr(m+1)))
     1          zerosh=ShiftArr(m+1)
            endif
          endif
          th=th-zerosh
        else if(ich.eq.1.or.ich.eq.3) then
          k=0
          call StToReal(ch80,k,th,1,.false.,ich)
        endif
        if(ich.ne.0) then
          call FeChybne(-1.,-1.,
     1      'string doesn''t contain proper number of items',
     2      'or it contains illegal character.',SeriousError)
          go to 2000
        endif
        if(th.lt.XPwd(1).or.th.gt.XPwd(Npnts)) then
          write(ch80,'(''Angle '',f9.3,'' is outside the limits <'',
     1      f9.3,'','',f9.3,''>'')')th,XPwd(1),XPwd(Npnts)
          call FeDelTwoSpaces(ch80)
          call FeMsgOut(-1.,-1.,ch80)
          go to 2000
        endif
        call locate(XPwd(1),Npnts,th,ipul)
        if(ipul.eq.0) go to 2000
        odEdw=.true.
        pul=.5*(DegMax-DegMin)
        if(XPwd(ipul)-pul.le.XPwd(1)) then
          DegMin=XPwd(1)
          DegMax=min(XPwd(Npnts),DegMin+2.*pul)
        else if(XPwd(ipul)+pul.ge.XPwd(Npnts)) then
          DegMax=XPwd(Npnts)
          DegMin=max(XPwd(1),DegMax-2.*pul)
        else
          DegMin=XPwd(ipul)-pul
          DegMax=XPwd(ipul)+pul
        endif
        go to 4800
      endif
      if(CheckType.eq.EventRolMenu) then
        KDatBlockNew=RolMenuSelectedQuest(NRolMenuDatBlock)
        if(KDatBlockNew.ne.KDatBlock) then
          StartAgain=.true.
          go to 6000
        endif
      endif
      if(CheckType.eq.EventKey) then
        if(CheckNumber.eq.JeEscape) call FeKillInfoWin
      endif
      go to 2000
4700  if(CheckNumber.eq.nButtXPlus.or.CheckNumber.eq.nButtXMinus)
     1  then
        npul=XLenAcWin/(2.*DelkaKroku)
        if(npul.le.0) npul=1
        if(CheckNumber.eq.nButtXMinus.and.npul.le.npulo) npul=npulo+1
        ipul=nst+(nen-nst)/2
        nst=max(1,ipul-npul)
        nen=min(Npnts,ipul+npul)
        DegMin=XPwd(nst)
        DegMax=XPwd(nen)
        DelkaKroku=XLenAcWin/(2.*float(nen-nst+1))
        npulo=npul
      endif
4800  if(CheckType.ne.EventButton.or.
     1   (CheckNumber.ne.nButtPPlus.and.CheckNumber.ne.nButtPMinus))
     2  then
        BraggCenter=0
      endif
      call FeKillInfoWin
      call PwdPrfKresli(0,1)
      if(.not.KresliDetails)
     1  call FeQuestButtonLabelChange(nButtDetails,LabPrf(17))
      if(ZPrf) then
        call FeQuestLblChange(nLblTitle,TitlePrf)
      else
        call FeQuestLblChange(nLblTitle,TitleM92)
      endif
      call FeQuestSbwPureOpen(SbwLastMade,nst,nen,Npnts,0,0,0)
      if(drzi) then
        call FeReleaseOutput
        call FeDeferOutput
        if(nen-nst.lt.4.or.nen-nst.gt.Npnts-2) then
          drzi=.false.
          call FeReleaseOutput
        endif
      endif
      delkaKroku=XLenAcWin/float(nen-nst)
4900  if(odEdw.or.odSkipToBragg) then
        strana=8.
4910    xc=PwdPrfTh2X(th)
        yc=YMinAcWin+10.
        vyska=anint(strana*sin(60.*torad))
        xtr(1)=xc
        ytr(1)=yc-vyska*0.5
        xtr(2)=xc+strana*0.5
        ytr(2)=yc+vyska*0.5
        xtr(3)=xc-strana*0.5
        ytr(3)=ytr(2)
        if(xtr(2).le.XMaxAcWin.and.xtr(3).ge.XMinAcWin) then
          phi=180.
          call RotatePoints(xtr,ytr,3,xc,yc,phi)
          call FePolygon(xtr,ytr,3,4,0,0,Red)
        else
          if(BraggCenter.ne.BraggCenterOld) then
            BraggCenter=BraggCenterOld
            go to 4910
          endif
        endif
      else
        if(CheckNumber.eq.nButtXPlus.or.CheckNumber.eq.nButtXMinus.or.
     1     CheckNumber.eq.nButtYPlus.or.CheckNumber.eq.nButtYMinus)
     2    then
          TakeMouseMove=.true.
          TimeDrzi=FeGetSystemTime()
5000      if(EventTypeLost.ne.0) then
            EventType=EventTypeLost
            EventNumber=EventNumberLost
            EventTypeLost=0
            EventNumberLost=0
          else
            call FeEvent(1)
          endif
          TakeMouseMove=.false.
          if(EventType.eq.EventMouse.and.EventNumber.eq.JeLeftUp) then
            drzi=.false.
            call FeReleaseOutput
          else if(FeGetSystemTime()-TimeQuestEvent.lt.500) then
            go to 5000
          else
            if(FeGetSystemTime()-TimeDrzi.lt.20) go to 5000
          endif
        endif
      endif
      if(drzi) then
        go to 2300
      else
        if(iButOff.gt.0) call FeQuestButtonOff(iButOff)
        go to 2000
      endif
6000  call FeKillInfoWin
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      call FeQuestRemove(id)
      TakeMouseMove=.false.
      AskIfQuit=.false.
      Veta=' '
      if(NManBackg(KDatBlock).ne.NManBackgOld) then
        if(NManBackgOld.eq.0) then
          Veta='A manual background has been created:'
        else if(NManBackg(KDatBlock).eq.0) then
          Veta='The manual background has been deleted:'
        else
          Veta='The number of points for the manual background has '//
     1         'been modified:'
        endif
      else if(allocated(XManBackgOld)) then
        if(.not.EqRV(XManBackgOld,XManBackg(1,KDatBlock),
     1               NManBackgOld,.001).or.
     2     .not.EqRV(YManBackgOld,YManBackg(1,KDatBlock),
     3               NManBackgOld,.1))
     4    Veta='Points defining the manual background have been '//
     5         'modified:'
      endif
      if(Veta.ne.' ') then
        ich=0
        id=NextQuestId()
        xqd=400.
        il=5
        call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,-1,0)
        il=1
        xpom=5.
        tpom=xpom+CrwgXd+10.
        Veta='%Accept the new manual background'
        do i=1,2
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.1) then
            nCrwAccept=CrwLastMade
            Veta='%Discard the changes'
          else if(i.eq.2) then
            nCrwDiscard=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          il=il+4
        enddo
        xpom=xpom+30.
        tpom=tpom+30.
        il=2
        Veta='%Supress using of polynomial terms'
        do i=1,3
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        0,2)
          if(i.eq.1) then
            nCrwSupress=CrwLastMade
            Veta='%Reset polynomial coefficients to zeros'
          else if(i.eq.2) then
            nCrwReset=CrwLastMade
            Veta='%Keep polynomial coefficients unchanged'
          else
            nCrwKeep=CrwLastMade
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1)
          if(NBackg(KDatBlock).le.0) call FeQuestCrwDisable(CrwLastMade)
          il=il+1
        enddo
6500    call FeQuestEvent(id,ich)
        if((CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAccept).or.
     1     (CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDiscard)) then
          if(CrwLogicQuest(nCrwAccept).and.NBackg(KDatBlock).gt.0) then
            call FeQuestCrwOpen(nCrwReset,.true.)
            call FeQuestCrwOpen(nCrwSupress,.false.)
            call FeQuestCrwOpen(nCrwKeep,.false.)
          else
            call FeQuestCrwDisable(nCrwReset)
            call FeQuestCrwDisable(nCrwSupress)
            call FeQuestCrwDisable(nCrwKeep)
          endif
          go to 6500
        else if(CheckType.eq.EventGlobal) then
          go to 6500
        else if(CheckType.ne.0) then
          call NebylOsetren
          go to 6500
        endif
        if(CrwLogicQuest(nCrwAccept)) then
          if(DrawInD) then
            call PwdD2X(XPwd,NPnts)
            call PwdD2X(XManBackg(1,KDatBlock),NManBackg(KDatBlock))
          endif
          call PwdUpdateReflFiles
          KManBackg(KDatBlock)=1
          if(CrwLogicQuest(nCrwSupress)) then
            NBackg(KDatBlock)=0
          else if(CrwLogicQuest(nCrwReset)) then
            call SetRealArrayTo(BackgPwd(1,KDatBlock),NBackg(KDatBlock),
     1                          0.)
          endif
          Sc(2,KDatBlock)=1.
          call iom40(1,0,fln(:ifln)//'.m40')
          call iom41(1,0,fln(:ifln)//'.m41')
        else
          call CopyFile(PreviousM40,fln(:ifln)//'.m40')
          call CopyFile(PreviousM41,fln(:ifln)//'.m41')
          call MoveFile(fln(:ifln)//'.z90',fln(:ifln)//'.m90')
        endif
        call FeQuestRemove(id)
      else
        if(FileDiff(fln(:ifln)//'.m41',PreviousM41)) then
          Veta='The set of excluded regions has been modified.'//
     1         ' Do want to accept the changes?'
          if(.not.FeYesNo(-1.,-1.,Veta,0))
     1      call CopyFile(PreviousM41,fln(:ifln)//'.m41')
        endif
      endif
9999  if(KDatBlockNew.ne.0) KDatBlock=KDatBlockNew
      close(75)
      if(allocated(CalcPArr))
     1  deallocate(CalcPArr,CalcPPArr,BkgArr,DifArr)
      if(allocated(BraggArr))
     1  deallocate(BraggArr,ShiftArr,MultArr,ypeakArr,KlicArr,indArr)
      if(allocated(PeakXPos))
     1  deallocate(PeakXPos,PeakInt,PeakFWHM,PeakBckg,PeakEta,PeakXPosI,
     2             PeakPor)
      if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YcPwd,YbPwd,YsPwd,
     1                                YfPwd,YiPwd,YcPwdInd)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf)
      if(allocated(XManBackgOld)) deallocate(XManBackgOld,YManBackgOld)
      if(StartAgain) go to 1100
      KDatBlock=KDatBlockIn
      KPhase=KPhaseBasic
      DrawInD=.false.
      return
      end
      subroutine PwdX2D(X,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension X(n)
      do i=1,n
        if(isTOF) then
          X(i)=PwdTOF2D(X(i))
        else
          if(KRefLam(KDatBlock).eq.1) then
            X(i)=PwdTwoTh2D(X(i),LamPwd(1,KDatBlock))
          else
            X(i)=PwdTwoTh2D(X(i),LamAve(KDatBlock))
          endif
        endif
      enddo
      return
      end
      subroutine PwdD2X(X,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension X(n)
      do i=1,n
        if(isTOF) then
          X(i)=PwdD2TOF(X(i))
        else
          if(KRefLam(KDatBlock).eq.1) then
            X(i)=PwdD2TwoTh(X(i),LamPwd(1,KDatBlock))
          else
            X(i)=PwdD2TwoTh(X(i),LamAve(KDatBlock))
          endif
        endif
      enddo
      return
      end
      subroutine PwdPrfKresli(Klic,Psani)
      use Powder_mod
      use RefPowder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension XPwdP(:),XPwdG(:),XPwdA(:),seg1x(100),seg2x(100),
     1          seg1y(100),kkFlag(3),seg2y(100),hh(3)
      integer Color,whLine,SegCol(100),Exponent10,Psani,
     1        CrlSubSystemNumber
      logical EqualStep,nahradZacatek,nahradKonec,KrObs,FeYesNoLong,
     1        KrDif,CrlItIsSatellite
      logical :: DetailyAno=.true.
      character*10 ch10
      character*20 ch20,FormatInt
      character*80 Veta
      real IntNaDil
      save EqualStep,YpMin,YpMax,YObsMax,YDifMin,YDifMax,
     1     YCalcMax,KrObs,n1,n2,n1b,n2b
      allocatable XPwdP,XPwdA,XPwdG
      allocate(XPwdP(npnts),XPwdG(NManBackg(KDatBlock)),
     1         XPwdA(max(npnts,NManBackg(KDatBlock))))
      if(Psani.eq.1) DetailyAno=.true.
      call FeDeferOutput
      if(.not.ZPrf) then
        KrDif=.false.
      else
        KrDif=KresliDifLine
      endif
      WLabXL=0.
      xpom=FeTxLength('XXXXX')+5.
      if(KresliBragg) then
        do i=1,NPhase
          if(BraggLabel.eq.0) then
            Cislo=PhaseName(i)
          else
            Cislo='('//char(ichar('a')+i-1)//')'
          endif
          WLabXL=max(WLabXL,FeTxLength(Cislo)-xpom)
        enddo
      endif
      if(KrDif) then
        if(SqrtScale) then
          WLabXL=max(WLabXL,FeTxLength('sqrt(Iobs)-'))
        else
          WLabXL=max(WLabXL,FeTxLength('delta(I)'))
        endif
      endif
      if(SqrtScale) then
        WLabXL=max(WLabXL,FeTxLength('sqrt(Icalc)'))
      else
        WLabXL=max(WLabXL,FeTxLength('I'))
      endif
      WLabXL=WLabXL+5.
      if(KresliLab) then
        WLabX=WLabXL+xpom
        WLabY=30.
      endif
      YMinPlW=YMinAcWin+WLabY
      XMinPlW=XMinAcWin+WLabX
      XMaxTx=XMinAcWin+WLabXL
      YMinPlC=YMinPlW+6.
      XMinPlC=XMinPlW+2.
      if(klic.eq.-1) then
        EqualStep=.true.
        YpMin=YoPwd(1)
        YpMax=0.
        YObsMax=0.
        YCalcMax=0.
        DelMin=999999.
        YdMax1=0.
        YdMax2=0.
        do i=1,Npnts
          XPwdi=XPwd(i)
          if(i.lt.Npnts) then
            pom=XPwd(i+1)-XPwdi
            if(pom.lt.-.0001) then
              if(Psani.gt.0) then
                Veta='See point #'
                write(Cislo,FormI15) i
                call Zhusti(Cislo)
                Veta='See point #'//Cislo(:idel(Cislo))//':'
                write(Veta(idel(Veta)+1:),Format92) XPwd(i),YoPwd(i),
     1                                    YsPwd(i)
                call FeChybne(-1.,-1.,'Profile steps are not '//
     1                        'ascending.',Veta,SeriousError)
              endif
              go to 9000
            endif
            EqualStep=EqualStep.and.abs(pom-DegStepPwd).lt..0001
          endif
          YpMax=max(YcPwd(i),YoPwd(i),YpMax)
          YpMin=min(YoPwd(i),YpMin)
          YObsMax=max(abs(YoPwd(i)),YObsMax)
          YCalcMax=max(abs(YcPwd(i)),YCalcMax)
          if(YfPwd(i).eq.1) then
            YpMin=min(YcPwd(i),YpMin)
            if(SqrtScale) then
              DifArr(i)=sqrt(YoPwd(i))-sqrt(YcPwd(i))
            else
              DifArr(i)=YoPwd(i)-YcPwd(i)
            endif
            YdMax1=max(YdMax1,abs(DifArr(i)))
            YdMax2=max(YdMax2,abs(YoPwd(i)-YcPwd(i)))
          else
            DifArr(i)=0.
          endif
        enddo
        YpMax=max(YpMax,YObsMax)
        if(SqrtScale) then
          do i=1,Npnts
            DifArr(i)=YdMax2/YdMax1*DifArr(i)
          enddo
        endif
        if(YOriginShift.gt.0.) then
          YpMin=YpMin*YOriginShift
        else
          YpMin=0.
        endif
      endif
      if(EqualStep) then
        if(DegMin.gt.0.) then
          n1=nint((max(DegMin,XPwd(1))-XPwd(1))/DegStepPwd)+1
        else
          n1=1
        endif
        if(DegMax.gt.0.) then
          n2=nint((min(DegMax,XPwd(Npnts))-XPwd(1))/
     1      DegStepPwd)+1
        else
          n2=Npnts
        endif
      else
        if(DegMax.lt.0.) DegMax=XPwd(Npnts)
        if(DegMin.lt.0.) DegMin=XPwd(1)
        call locate(XPwd,Npnts,DegMin,n1)
        call locate(XPwd,Npnts,DegMax,n2)
      endif
      n1=max(1,n1)
      n2=min(n2,Npnts)
      if(n2.eq.0) n2=Npnts
      if(n2.le.n1) then
        ErrFlag=1
        go to 9000
      endif
      DegMin=XPwd(n1)
      DegMax=XPwd(n2)
      if(KresliManBackg) then
        call locate(XManBackg(1,KDatBlock),NManBackg(KDatBlock),
     1              DegMin,n1g)
        call locate(XManBackg(1,KDatBlock),NManBackg(KDatBlock),
     1              DegMax,n2g)
        if(XManBackg(n1g,KDatBlock).lt.DegMin) n1g=n1g+1
        n1g=max(1,n1g)
        if(XManBackg(n2g,KDatBlock).gt.DegMax) n2g=n2g-1
        n2g=min(n2g,NManBackg(KDatBlock))
      endif
      nst=n1
      nen=n2
      if(NBragg.gt.0) then
        call locate(BraggArr,NBragg,XPwd(n1),n1b)
        call locate(BraggArr,NBragg,XPwd(n2),n2b)
        n1b=max(n1b,1)
        n2b=min(n2b,NBragg)
        n2b=max(n2b,0)
900     if(n1b.lt.NBragg.and.BraggArr(n1b).lt.XPwd(n1)) then
          n1b=n1b+1
          go to 900
        endif
1000    if(n2b.gt.0.and.BraggArr(n2b).gt.XPwd(n2)) then
          n2b=n2b-1
          go to 1000
        endif
        if(n1b.eq.0) n2b=-1
      else
        n1b=1
        n2b=0
      endif
      if(IntMax.le.0.) then
        if(KresliCalcLine.and.(KresliObsLine.or.KresliObsPoints)) then
           IntMax=YpMax
        else
          if(KresliCalcLine) then
            IntMax=YCalcMax
          else
            IntMax=YObsMax
          endif
        endif
      else
        if(IntMax.gt.YpMax) IntMax=YpMax
        if(IntMax.lt.YpMin+1.) IntMax=YpMin+1.
      endif
      Celkem=IntMax-YpMin
      if(Celkem.le.0.) then
        IntMax=100.
        YpMin=0.
        Celkem=100.
      endif
      if(KrDif) then
        YDifMin=0.
        YDifMax=0.
        do i=n1,n2
          YDifMin=min(YDifMin,DifArr(i))
          YDifMax=max(YDifMax,DifArr(i))
        enddo
        YDifMin=abs(min(YDifMin,0.))
        YDifMax=max(0.,YDifMax)
        Celkem=Celkem+YDifMin+YDifMax
      endif
      Vyska=YMaxPlC-YMinPlC
      VyskaMezera1=30.
      VyskaMezera2=8.
      VyskaMezera3=5.
      if(NPhase.le.3) then
        VyskaBragg=16.
      else if(NPhase.le.8) then
        VyskaBragg=12.
      else
        VyskaBragg=8.
      endif
      PruhBragg=VyskaBragg+3.
      Vyska=Vyska-VyskaMezera1
      if(KrDif) then
        Vyska=Vyska-VyskaMezera3
        if(KresliBragg) Vyska=Vyska-VyskaMezera2
        if(Celkem.gt.0.) then
          VyskaDif=Vyska*(YDifMin+YDifMax)/Celkem
        else
          VyskaDif=10.
        endif
      endif
      if(KresliBragg) then
        y1bragg=YMinPlW+VyskaMezera1
        if(KrDif) y1bragg=y1bragg+VyskaDif+VyskaMezera2
        y2bragg=y1bragg+VyskaBragg
      endif
      YMinPlC=YMinPlC+VyskaMezera1
      if(KrDif) YMinPlC=YMinPlC+VyskaDif
      if(KresliBragg) YMinPlC=YMinPlC+PruhBragg*float(NPhase)
      if(KrDif.or.KresliBragg) YMinPlC=YMinPlC+VyskaMezera3
      if(KrDif.and.KresliBragg) YMinPlC=YMinPlC+VyskaMezera2
      if(YMinPlC.le.YMinPlW.or.YMinPlC.ge.YMaxPlC) go to 9000
      if(SqrtScale) then
        shy=sqrt(YpMin)
      else
        shy=YpMin
      endif
      skydif=(YMaxPlC-YMinPlC)/(IntMax-YpMin)
      if(SqrtScale) then
        sky=(YMaxPlC-YMinPlC)/(sqrt(IntMax)-sqrt(YpMin))
      else
        sky=(YMaxPlC-YMinPlC)/(IntMax-YpMin)
      endif
      shx=XPwd(n1)
      skx=(XMaxPlC-XMinPlC)/(XPwd(n2)-XPwd(n1))
      do i=max(1,n1-1),n2
        XPwdP(i)=PwdPrfTh2X(XPwd(i))
      enddo
      if(KresliManBackg) then
        do i=max(1,n1g-1),min(n2g+1,NManBackg(KDatBlock))
          XPwdG(i)=PwdPrfTh2X(XManBackg(i,KDatBlock))
        enddo
      endif
      if(HardCopy.eq.0) then
        if(Klic.ne.-2) then
          call FeClearGrWin
        else
          call FeFillRectangle(XMinPlW,XMaxPlW,YMinPlW,YMaxPlW,4,0,0,
     1                         Black)
        endif
      endif
      call FeFillRectangle(XMinPlW,XMaxPlW,YMinPlW,YMaxPlW,0,0,0,White)
2000  do i=max(1,n1-1),n2
        CalcPArr(i)=PwdPrfInt2Y(YcPwd(i))
        if(KrDif) DifArr(i)=(DifArr(i)+YDifMin)*skydif+YMinPlW+
     1                       VyskaMezera1
      enddo
      if(KresliObsPoints.and.(klic.ne.-2.or.
     1   (.not.KresliCalcLine.and..not.KresliObsLine))) then
        KrObs=.true.
        dzn=min(DegStepPwd*skx-2.,2.)
        dznh=dzn/sqrt(2.)
      else
        KrObs=.false.
      endif
      if(klic.eq.-2) go to 2340
      ZeroCalc=PwdPrfInt2Y(YpMin)
!      ZeroCalc=PwdPrfInt2Y(0.)
      if(KrDif) ZeroDif=YDifMin*skydif+YMinPlW+VyskaMezera1
      if(KresliZero) then
        Color=White
      else
        Color=Black
      endif
      call FeLineType(DashedLine)
      seg1x(1)=XMinPlC
      seg1x(2)=XMaxPlC
      seg1y(1)=ZeroCalc
      seg1y(2)=ZeroCalc
      call FePolyLine(2,seg1x,seg1y,Color)
      if(KrDif) then
        seg1y(1)=ZeroDif
        seg1y(2)=ZeroDif
        call FePolyLine(2,seg1x,seg1y,Color)
      endif
      call FeLineType(NormalLine)
      if(KresliLab) then
        if(SqrtScale) then
          DiffI=sqrt(IntMax)-sqrt(YpMin)
        else
          DiffI=IntMax-YpMin
        endif
        Step=10.**(Exponent10(DiffI)-1)
        Rad=Step*10.
        nn=anint(DiffI/Step)
        if(nn.lt.100) then
          Step=Step*.5
          Rad=Step*10.
          nn=nn*2
        endif
        if(Rad.gt..1) then
          FormatInt='(f20.1)'
        else if(Rad.gt..01) then
          FormatInt='(f20.2)'
        else if(Rad.gt..001) then
          FormatInt='(f20.3)'
        else
          FormatInt='(e20.5)'
        endif
        YpPom=0.
        pisy =-1000.
        pisyd= 1000.
        do i=1,nn
          pomy=PwdPrfInt2Y(YpPom)
          nseg=nseg+1
          seg1y(nseg)=pomy
          seg2y(nseg)=pomy
          SegCol(nseg)=White
          if(mod(i,10).eq.1) then
            seg1x(nseg)=XMinPlW-5.
            seg2x(nseg)=XMinPlW
            if(pomy-pisy.gt.2.*PropFontWidthInPixels) then
              if(Rad.lt.1.) then
                write(ch20,FormatInt) YpPom
              else
                write(ch20,'(i20)') nint(YpPom)
              endif
              call FeOutSt(0,XMinPlW-9.,pomy,ch20,'R',White)
              pisy=pomy
              pisyd=pisy
            endif
            go to 2150
          else if(mod(i,10).eq.6) then
            YIntLab=YpPom
          endif
          seg1x(nseg)=XMinPlW-2.
          seg2x(nseg)=XMinPlW
2150      if(nseg.ge.100) then
            call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
            nseg=0
          endif
          YpPom=YpPom+Step
        enddo
2210    if(nseg.gt.0) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        if(SqrtScale) then
          ch20='sqrt(IRel)'
        else
          if(isTOF) then
            ch20='TOF'
          else
            ch20='Intensity'
          endif
        endif
        call FeOutSt(0,XMinPlW-9.,PwdPrfInt2Y(YIntLab),ch20,'R',
     1               White)
        if(KrDif) then
          dify=abs(PwdPrfInt2Y(Step)-PwdPrfInt2Y(0.))
          seg1x(1)=XMinPlW-5.
          seg1x(2)=XMinPlW
          seg1x(3)=XMinPlW-2.
          seg1x(4)=XMinPlW
          i1=nint((ZeroDif-YMinPlW-VyskaMezera1)/dify)
          i2=nint(((YDifMax+YDifMin)*skydif+YMinPlW+VyskaMezera1-
     1            ZeroDif)/dify)
          pisy=-1000.
          do i=-i1-1,i2+1
            pomy=ZeroDif+i*dify
            if(pomy.le.YMinPlW.or.pomy.ge.y1bragg) cycle
            seg1y(1)=pomy
            seg1y(2)=pomy
            if(mod(i,10).eq.0) then
              call FePolyLine(2,seg1x,seg1y,White)
              if(pomy-pisy.gt.2.*PropFontWidthInPixels) then
                pom=float(i)*Step
                if(Rad.lt.1.) then
                  write(ch20,FormatInt) pom
                else
                  write(ch20,'(i20)') nint(pom)
                endif
                call FeOutSt(0,XMinPlW-9.,pomy,ch20,'R',White)
                pisy=pomy
              endif
            else
              call FePolyLine(2,seg1x(3),seg1y,White)
            endif
          enddo
          call FeOutSt(0,XMinPlW-9.,ZeroDif,'0.0','R',White)
        endif
        DegNaDil=(XMaxPlW-XMinPlW)*0.2*(DegMax-DegMin)/
     1    (XMaxPlW-XMinPlW-1.)
        if(DegNaDil.gt.10.) then
          step=10.
          idiv=10
        else if(DegNaDil.gt.5.) then
          step=5.
          idiv=5
        else if(DegNaDil.gt.2.) then
          step=2.
          idiv=10
        else if(DegNaDil.gt.1.) then
          step=1.
          idiv=10
        else if(DegNaDil.gt.0.5) then
          step=0.5
          idiv=5
        else if(DegNaDil.gt.0.2) then
          step=0.2
          idiv=10
        else if(DegNaDil.gt.0.1) then
          step=0.1
          idiv=10
        else if(DegNaDil.gt.0.05) then
          step=0.05
          idiv=5
        else
          step=0.01
          idiv=10
        endif
        ch10='(f6.1)'
        write(ch10(5:5),'(i1)') max(-Exponent10(DegNaDil),1)
        idrobne=1
        if(PwdPrfTh2X(DegMin+step/idiv)-PwdPrfTh2X(DegMin).lt.2.) then
          if(idiv.eq.20) then
            idiv=10
            if(PwdPrfTh2X(DegMin+step/idiv)-
     1         PwdPrfTh2X(DegMin).lt.2.) idrobne=0
          else
            idrobne=0
          endif
        endif
        if(idrobne.eq.0) idiv=1.
        deg=DegMin*float(idiv)/step
        deg=float(nint(deg)+1)*(step/float(idiv))
        pisx=-1000.
        nseg=0
        pomx1=0.
        pomx2=0.
2300    pomx=PwdPrfTh2X(deg)
        nseg=nseg+1
        seg1x(nseg)=pomx
        seg2x(nseg)=pomx
        SegCol(nseg)=White
        pom=deg/step
        if(abs(pom-float(nint(pom))).le.0.01) then
          deg=step*float(nint(pom))
          write(ch20,ch10) deg
          call zhusti(ch20)
          pom=FeTxLength(ch20)*.5
          if(pomx-pom.ge.pisx+distLabel) then
            pisx=pomx+pom
            pomx1=pomx2
            pomx2=pomx
            if(pisx.lt.XMaxPlW)
     1        call FeOutSt(0,pomx,
     2          YMinPlW-2.-1.-PropFontHeightInPixels,ch20,'C',White)
          endif
          seg1y(nseg)=YMinPlW-5.
          seg2y(nseg)=YMinPlW
        else
          if(idrobne.eq.1) then
            seg1y(nseg)=YMinPlW-2.
            seg2y(nseg)=YMinPlW
          endif
        endif
        if(nseg.ge.100) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        deg=deg+step/float(idiv)
        if(deg.le.DegMax) go to 2300
        if(nseg.gt.0) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        if(pomx1.gt.0..or.pomx2.gt.0.) then
          pomx=(pomx1+pomx2)*.5
          if(DrawInD) then
            ch20='d'
          else
            if(isTOF) then
              ch20='t'
            else if(isED) then
              ch20='E'
            else
              ch20='2th'
            endif
          endif
          call FeOutSt(0,pomx,YMinPlW-2.-1.-PropFontHeightInPixels,ch20,
     1                 'C',White)
        endif
      endif
      if(KresliTitle.and.plotTitle.ne.' ') then
        if(CenteredTitle) then
          call FeOutSt(0,XMinPlW+0.5*(XMaxPlW-XMinPlW),YMaxPlW-8.,
     1                 plotTitle,'C',White)
        else
          pom=FeTxLengthUnder(plotTitle)
          call FeOutSt(0,XMaxPlW-pom-2.,YMaxPlW-8.,plotTitle,'L',White)
        endif
      endif
      if(KresliBragg) then
        xpom=XMinPlW-5.
        do i=1,NPhase
          if(BraggLabel.eq.0) then
            Cislo=PhaseName(i)
          else
            Cislo='('//char(ichar('a')+i-1)//')'
          endif
          ypom=y2bragg+PruhBragg*(float(i-1)-.5)+1.
          call FeOutSt(0,xpom,ypom,Cislo,'R',White)
        enddo
      endif
2340  ist=0
      nseg=0
      if(KresliDetails.and.DetailyAno.and.klic.ne.-2) then
        NRefPwd=1
        NLamPwd=1
        iist=0
        iien=0
        if(DrawInD) then
          if(isTOF) then
            DegMinP=PwdTOF2D(DegMin)
            DegMaxP=PwdTOF2D(DegMax)
          else
            if(KRefLam(KDatBlock).eq.1) then
              DegMinP=PwdD2TwoTh(DegMax,LamPwd(1,KDatBlock))
              DegMaxP=PwdD2TwoTh(DegMin,LamPwd(1,KDatBlock))
            else
              DegMinP=PwdD2TwoTh(DegMax,LamAve(KDatBlock))
              DegMaxP=PwdD2TwoTh(DegMin,LamAve(KDatBlock))
            endif
          endif
        else
          DegMinP=DegMin
          DegMaxP=DegMax
        endif
        if(.not.allocated(ihPwdArr)) then
          n=1
          m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
          l=NAlfa(KDatBlock)
          allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),MultPwdArr(n),
     1             iqPwdArr(n),KPhPwdArr(n),ypeaka(l,n),peaka(l,n),
     2             pcota(l,n),rdega(2,l,n),shifta(l,n),fnorma(l,n),
     3             tntsima(l,n),ntsima(l,n),sqsga(l,n),fwhma(l,n),
     4             sigpa(l,n),etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     5             dfwdga(l,n),dfwdla(l,n),coef(m,l,n),coefp(m,l,n),
     6             coefq(2,l,n),Prof0(n),cotg2tha(l,n),cotgtha(l,n),
     7             cos2tha(l,n),cos2thqa(l,n),Alpha12a(l,n),
     8             Beta12a(l,n),IBroadHKLa(n))
          if(.not.isTOF.and..not.isED.and.
     1       KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     2      allocate(AxDivProfA(-25:25,l,n),DerAxDivProfA(-25:25,7,l,n),
     3               FDSProf(-25:25,l,n))
        endif
        iin=0
        do i=1,NBragg
          if(DrawInD) then
            if(isTOF) then
              pom=PwdTOF2D(BraggArr(i))
            else
              if(KRefLam(KDatBlock).eq.1) then
                pom=PwdD2TwoTh(BraggArr(i),LamPwd(1,KDatBlock))
              else
                pom=PwdD2TwoTh(BraggArr(i),LamAve(KDatBlock))
              endif
            endif
          else
            pom=BraggArr(i)
          endif
          th=(pom+ShiftArr(i))*ToRad*0.5
          KPh=KlicArr(i)/100+1
          KPhase=KPh
          call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
          call SetProfFun(indArr(1,i),hh,th)
          if(rdeg(1)/torad.le.DegMaxP.and.rdeg(2)/torad.ge.DegMinP) then
            if(iist.eq.0) iist=i
            iien=i
            iin=iin+1
          endif
        enddo
2360    if(iist.gt.0) then
          iist=iist-1
          iien=iien+1
          iin=iin+2
        else
          iist=1
          iien=1
          iin=1
        endif
        if(iin.gt.100) then
          if(Psani.eq.1) then
            TextInfo(1)='You are about to plot component functions for'
            write(Cislo,'(i4)') iin
            call Zhusti(Cislo)
            TextInfo(2)=Cislo(:idel(Cislo))//
     1                  ' Bragg peaks. Do you want to continue?'
            NInfo=2
            DetailyAno=FeYesNoLong(-1.,-1.,'Warning',0)
          endif
          if(.not.DetailyAno) then
            iist=1
            iien=1
            KresliDetails=.false.
          endif
        endif
      else
        iist=1
        iien=1
      endif
      call SetIntArrayTo(kkFlag,3,0)
      kkFlag(1)=1
      if(KresliObsLine.and.(Klic.ne.-2.or..not.KresliCalcLine))
     1  kkFlag(2)=1
      if(KresliManBackg) kkFlag(3)=1
      if(KresliCalcLine) then
        whLine=whichLine
      else
        whLine=2
      endif
      if(KrObs.or.KresliObsLine.or.KresliCalcLine) then
        k=0
        do j=n1,n2
2430      if(YfPwd(j).eq.0) then
            if(k.eq.0) then
              k=k+1
              seg1x(k)=XPwdP(j)
              seg1y(k)=YMinPlC
            endif
            k=k+1
            seg1x(k)=XPwdP(j)
            if(KrObs.or.KresliObsLine) then
              seg1y(k)=PwdPrfInt2Y(abs(YoPwd(j)))
            else
              seg1y(k)=PwdPrfInt2Y(abs(YcPwd(j)))
            endif
            seg1y(k)=min(seg1y(k),YMaxPlC)
            if(k.lt.98) cycle
          endif
          if(k.gt.0) then
            k=k+1
            seg1x(k)=seg1x(k-1)
            seg1y(k)=YMinPlC
            k=k+1
            seg1x(k)=seg1x(1)
            seg1y(k)=YMinPlC
            call FePolygon(seg1x,seg1y,k,4,0,0,ColorSkip)
            k=0
            if(YfPwd(j).eq.0) go to 2430
          endif
        enddo
        if(k.gt.1) then
          k=k+1
          seg1x(k)=seg1x(k-1)
          seg1y(k)=YMinPlC
          k=k+1
          seg1x(k)=seg1x(1)
          seg1y(k)=YMinPlC
          call FePolygon(seg1x,seg1y,k,4,0,0,ColorSkip)
        endif
      endif
      do ii=iist,iien
        if(ii.gt.iist) then
          call PwdPrfSum(n1,n2,ii,ii.eq.iist+1,ii.eq.iien)
          do i=n1,n2
            CalcPArr(i)=PwdPrfInt2Y(CalcPArr(i))
          enddo
        else
          Color=ColorCalc
        endif
        if(ii.eq.iien.and.iien.gt.1) then
          if(whLine.eq.3) then
            do i=n1,n2
              if(abs(PwdPrfInt2Y(YcPwd(i))-CalcPArr(i)).gt.1.)
     1          go to 2920
            enddo
            cycle
          else if(whLine.eq.2) then
            Color=ColorCalc
          else
            call FeLineType(NormalLine)
            cycle
          endif
        endif
2920    if(ii.eq.iist) then
          call FeLineType(NormalLine)
          kken=3
        else
          call FeLineType(DenseDashedLine)
          kken=1
        endif
        do kk=1,kken
          if(kkFlag(kk).eq.0) cycle
          if(kk.eq.1) then
            if(ii.eq.iist.and.iien.gt.iist.and.whLine.eq.2) cycle
            do i=n1,n2
              XPwdA(i)=XPwdP(i)
            enddo
            n1p=n1
            n2p=n2
          else if(kk.eq.2) then
            Color=ColorObs
            do i=n1,n2
              CalcPArr(i)=PwdPrfInt2Y(abs(YoPwd(i)))
              XPwdA(i)=XPwdP(i)
            enddo
            n1p=n1
            n2p=n2
          else if(kk.eq.3) then
            Color=Magenta
            do i=max(1,n1g-1),min(n2g+1,NManBackg(KDatBlock))
              CalcPArr(i)=PwdPrfInt2Y(abs(YManBackg(i,KDatBlock))*
     1                                Sc(2,KDatBlock))
              XPwdA(i)=XPwdG(i)
            enddo
            n1p=n1g
            n2p=n2g
          endif
          iskip=0
          do i=n1p,n2p
            if(ii.eq.iist.and.kk.eq.1.and..not.KresliCalcLine)
     1        go to 2990
            if(CalcPArr(i).le.YMaxPlC.and.ist.eq.0) ist=i
            if(CalcPArr(i).gt.YMaxPlC.or.i.eq.n2p) then
              if(ist.eq.0) go to 2990
              if(i-ist.ge.1) then
                nahradZacatek=.false.
                nahradKonec=.false.
                if(ist.gt.n1p) then
                  if(CalcPArr(ist-1).gt.YMaxPlC) then
                    nahradZacatek=.true.
                    call PwdPrfPrusY(XPwdA(ist),CalcPArr(ist),
     1                XPwdA(ist-1),CalcPArr(ist-1),pomx1,pomy1,
     2                YMaxPlC)
                    ist=ist-1
                  endif
                endif
                if(CalcPArr(i).gt.YMaxPlC) then
                  nahradKonec=.true.
                  call PwdPrfPrusY(XPwdA(i-1),CalcPArr(i-1),
     1              XPwdA(i),CalcPArr(i),pomx,pomy,YMaxPlC)
                endif
                call FePolyLine(i-ist+1,XPwdA(ist),CalcPArr(ist),
     1            Color)
                if(nahradKonec) then
                  XPwdA(i)=pomx
                  CalcPArr(i)=pomy
                endif
                if(nahradZacatek) then
                  XPwdA(ist)=pomx1
                  CalcPArr(ist)=pomy1
                endif
                ist=0
              endif
            endif
2990        if(kk.le.2) then
              if(KrObs.and.ii.eq.iist) then
                pomx=XPwdA(i)
                pomy=PwdPrfInt2Y(abs(YoPwd(i)))
                if(pomy+dzn.le.YMaxPlC) then
                  nseg=nseg+1
                  seg1x(nseg)=pomx-dznh
                  seg2x(nseg)=pomx+dznh
                  seg1y(nseg)=pomy-dznh
                  seg2y(nseg)=pomy+dznh
                  SegCol(nseg)=ColorObs
                  nseg=nseg+1
                  seg1x(nseg)=pomx-dznh
                  seg2x(nseg)=pomx+dznh
                  seg1y(nseg)=pomy+dznh
                  seg2y(nseg)=pomy-dznh
                  SegCol(nseg)=ColorObs
                endif
                if(nseg.ge.50.or.(i.eq.n2p.and.nseg.gt.0)) then
                  call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,
     1                                SegCol)
                  nseg=0
                endif
              endif
            else
              pomx=XPwdA(i)
              pomy=CalcPArr(i)
              call FeCircle(pomx,pomy,4.,Magenta)
            endif
          enddo
          if(kk.eq.3.and.n2p.gt.0.and.n1p.lt.NManBackg(KDatBlock)) then
            if(n1p.gt.n2p) then
              call PwdPrfPrusX(XPwdA(n2p),CalcPArr(n2p),
     1                         XPwdA(n1p),CalcPArr(n1p),
     2                         seg1x(1),seg1y(1),XMinPlC)
              call PwdPrfPrusX(XPwdA(n2p),CalcPArr(n2p),
     1                         XPwdA(n1p),CalcPArr(n1p),
     2                         seg1x(2),seg1y(2),XMaxPlC)
              call FePolyLine(2,seg1x,seg1y,Magenta)
            else
              if(n1p.gt.1) then
                call PwdPrfPrusX(XPwdA(n1p-1),CalcPArr(n1p-1),
     1                           XPwdA(n1p  ),CalcPArr(n1p  ),
     2                           seg1x(1),seg1y(1),XMinPlC)
                seg1x(2)=XPwdA(n1p)
                seg1y(2)=CalcPArr(n1p)
                call FePolyLine(2,seg1x,seg1y,Magenta)
              endif
              if(n2p.lt.NManBackg(KDatBlock)) then
                seg1x(1)=XPwdA(n2p)
                seg1y(1)=CalcPArr(n2p)
                call PwdPrfPrusX(XPwdA(n2p)  ,CalcPArr(n2p)  ,
     1                           XPwdA(n2p+1),CalcPArr(n2p+1),
     2                           seg1x(2),seg1y(2),XMaxPlC)
                call FePolyLine(2,seg1x,seg1y,Magenta)
              endif
            endif
          endif
        enddo
        Color=ColorCalc
      enddo
      call FeLineType(NormalLine)
      if(KrDif) then
        call FePolyLine(n2-n1+1,XPwdP(n1),DifArr(n1),ColorDif)
        do i=max(1,n1-1),n2
          if(KrDif) DifArr(i)=(DifArr(i)-YMinPlW-VyskaMezera1)/skydif-
     1                        YDifMin
        enddo
      endif
      if(KresliBragg.and.klic.ne.-2) then
        do i=n1b,n2b
          seg1x(1)=PwdPrfTh2X(BraggArr(i))
          seg1x(2)=seg1x(1)
          KPh=KlicArr(i)/100+1
          KPhase=KPh
          k=mod(KlicArr(i),100)
          if(CrlItIsSatellite(indArr(1,i))) then
            Color=ColorSat
          else
            j=CrlSubSystemNumber(indArr(1,i))
            if(j.le.0) then
              Color=ColorMain12
            else if(j.eq.1) then
              Color=ColorMain1
            else
              Color=ColorMain2
            endif
          endif
          Zdvih=PruhBragg*float(KPh-1)
          if(k.eq.1) then
            seg1y(1)=y2bragg+Zdvih-VyskaBragg*.5
          else
            seg1y(1)=y1bragg+Zdvih
          endif
          seg1y(2)=y2bragg+Zdvih
          call FePolyLine(2,seg1x,seg1y,Color)
        enddo
        ybragg=y1bragg+0.5*(y2bragg-y1bragg)
      else
        ybragg=0.
      endif
      go to 9999
9000  ErrFlag=0
9999  if(allocated(XPwdP)) deallocate(XPwdP,XPwdG,XPwdA)
      return
      end
      subroutine PwdPrfKresliOld(Klic,Psani)
      use Powder_mod
      use RefPowder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension XPwdP(:),XPwdG(:),XPwdA(:),seg1x(100),seg2x(100),
     1          seg1y(100),kkFlag(3),seg2y(100),hh(3)
      integer Color,whLine,SegCol(100),Exponent10,Psani,
     1        CrlSubSystemNumber
      logical EqualStep,nahradZacatek,nahradKonec,KrObs,FeYesNoLong,
     1        KrDif,CrlItIsSatellite
      logical :: DetailyAno=.true.
      character*10 ch10
      character*20 ch20
      character*80 Veta
      save EqualStep,YpMin,YpMax,YObsMax,YDifMin,YDifMax,
     1     YCalcMax,KrObs,n1,n2,n1b,n2b
      allocatable XPwdP,XPwdA,XPwdG
      allocate(XPwdP(npnts),XPwdG(NManBackg(KDatBlock)),
     1         XPwdA(max(npnts,NManBackg(KDatBlock))))
      if(Psani.eq.1) DetailyAno=.true.
      call FeDeferOutput
      if(.not.ZPrf) then
        KrDif=.false.
      else
        KrDif=KresliDifLine
      endif
      WLabXL=0.
      xpom=FeTxLength('XXXXX')+5.
      if(KresliBragg) then
        do i=1,NPhase
          if(BraggLabel.eq.0) then
            Cislo=PhaseName(i)
          else
            Cislo='('//char(ichar('a')+i-1)//')'
          endif
          WLabXL=max(WLabXL,FeTxLength(Cislo)-xpom)
        enddo
      endif
      if(KrDif) then
        if(SqrtScale) then
          WLabXL=max(WLabXL,FeTxLength('sqrt(Iobs)-'))
        else
          WLabXL=max(WLabXL,FeTxLength('delta(IRel)'))
        endif
      endif
      if(SqrtScale) then
        WLabXL=max(WLabXL,FeTxLength('sqrt(Icalc)'))
      else
        WLabXL=max(WLabXL,FeTxLength('IRel'))
      endif
      WLabXL=WLabXL+5.
      if(KresliLab) then
        WLabX=WLabXL+xpom
        WLabY=30.
      endif
      YMinPlW=YMinAcWin+WLabY
      XMinPlW=XMinAcWin+WLabX
      XMaxTx=XMinAcWin+WLabXL
      YMinPlC=YMinPlW+6.
      XMinPlC=XMinPlW+2.
      if(klic.eq.-1) then
        EqualStep=.true.
        YpMin=YoPwd(1)
        YpMax=0.
        YObsMax=0.
        YCalcMax=0.
        DelMin=999999.
        YdMax1=0.
        YdMax2=0.
        do i=1,Npnts
          XPwdi=XPwd(i)
          if(i.lt.Npnts) then
            pom=XPwd(i+1)-XPwdi
            if(pom.lt.-.0001) then
              if(Psani.gt.0) then
                Veta='See point #'
                write(Cislo,FormI15) i
                call Zhusti(Cislo)
                Veta='See point #'//Cislo(:idel(Cislo))//':'
                write(Veta(idel(Veta)+1:),Format92) XPwd(i),YoPwd(i),
     1                                    YsPwd(i)
                call FeChybne(-1.,-1.,'Profile steps are not '//
     1                        'ascending.',Veta,SeriousError)
              endif
              go to 9000
            endif
            EqualStep=EqualStep.and.abs(pom-DegStepPwd).lt..0001
          endif
          YpMax=max(YcPwd(i),YoPwd(i),YpMax)
          YpMin=min(YoPwd(i),YpMin)
          YObsMax=max(abs(YoPwd(i)),YObsMax)
          YCalcMax=max(abs(YcPwd(i)),YCalcMax)
          if(YfPwd(i).eq.1) then
            YpMin=min(YcPwd(i),YpMin)
            if(SqrtScale) then
              DifArr(i)=sqrt(YoPwd(i))-sqrt(YcPwd(i))
            else
              DifArr(i)=YoPwd(i)-YcPwd(i)
            endif
            YdMax1=max(YdMax1,abs(DifArr(i)))
            YdMax2=max(YdMax2,abs(YoPwd(i)-YcPwd(i)))
          else
            DifArr(i)=0.
          endif
        enddo
        YpMax=max(YpMax,YObsMax)
        if(SqrtScale) then
          do i=1,Npnts
            DifArr(i)=YdMax2/YdMax1*DifArr(i)
          enddo
        endif
        if(YOriginShift.gt.0.) then
          YpMin=YpMin*YOriginShift
        else
          YpMin=0.
        endif
      endif
      if(EqualStep) then
        if(DegMin.gt.0.) then
          n1=nint((max(DegMin,XPwd(1))-XPwd(1))/DegStepPwd)+1
        else
          n1=1
        endif
        if(DegMax.gt.0.) then
          n2=nint((min(DegMax,XPwd(Npnts))-XPwd(1))/
     1      DegStepPwd)+1
        else
          n2=Npnts
        endif
      else
        if(DegMax.lt.0.) DegMax=XPwd(Npnts)
        if(DegMin.lt.0.) DegMin=XPwd(1)
        call locate(XPwd,Npnts,DegMin,n1)
        call locate(XPwd,Npnts,DegMax,n2)
      endif
      n1=max(1,n1)
      n2=min(n2,Npnts)
      if(n2.eq.0) n2=Npnts
      if(n2.le.n1) then
        ErrFlag=1
        go to 9000
      endif
      DegMin=XPwd(n1)
      DegMax=XPwd(n2)
      if(KresliManBackg) then
        call locate(XManBackg(1,KDatBlock),NManBackg(KDatBlock),
     1              DegMin,n1g)
        call locate(XManBackg(1,KDatBlock),NManBackg(KDatBlock),
     1              DegMax,n2g)
        if(XManBackg(n1g,KDatBlock).lt.DegMin) n1g=n1g+1
        n1g=max(1,n1g)
        if(XManBackg(n2g,KDatBlock).gt.DegMax) n2g=n2g-1
        n2g=min(n2g,NManBackg(KDatBlock))
      endif
      nst=n1
      nen=n2
      if(NBragg.gt.0) then
        call locate(BraggArr,NBragg,XPwd(n1),n1b)
        call locate(BraggArr,NBragg,XPwd(n2),n2b)
        n1b=max(n1b,1)
        n2b=min(n2b,NBragg)
        n2b=max(n2b,0)
900     if(n1b.lt.NBragg.and.BraggArr(n1b).lt.XPwd(n1)) then
          n1b=n1b+1
          go to 900
        endif
1000    if(n2b.gt.0.and.BraggArr(n2b).gt.XPwd(n2)) then
          n2b=n2b-1
          go to 1000
        endif
        if(n1b.eq.0) n2b=-1
      else
        n1b=1
        n2b=0
      endif
      if(IntMax.le.0.) then
        if(KresliCalcLine.and.(KresliObsLine.or.KresliObsPoints)) then
           IntMax=YpMax
        else
          if(KresliCalcLine) then
            IntMax=YCalcMax
          else
            IntMax=YObsMax
          endif
        endif
!        if(YObsMax.gt.100..and.YObsMax/YCalcMax.lt..01.and.
!     1    (KresliObsLine.or.KresliObsPoints)) IntMax=YObsMax
!        if(YCalcMax.gt.100..and.YCalcMax/YObsMax.lt..01.and.
!     1    KresliCalcLine) IntMax=YCalcMax
      else
        if(IntMax.gt.YpMax) IntMax=YpMax
        if(IntMax.lt.YpMin+1.) IntMax=YpMin+1.
      endif
      Celkem=IntMax-YpMin
      if(Celkem.le.0.) then
        IntMax=100.
        YpMin=0.
        Celkem=100.
      endif
      if(KrDif) then
        YDifMin=0.
        YDifMax=0.
        do i=n1,n2
          YDifMin=min(YDifMin,DifArr(i))
          YDifMax=max(YDifMax,DifArr(i))
        enddo
        YDifMin=abs(min(YDifMin,0.))
        YDifMax=max(0.,YDifMax)
        Celkem=Celkem+YDifMin+YDifMax
      endif
      Vyska=YMaxPlC-YMinPlC
      VyskaMezera1=30.
      VyskaMezera2=8.
      VyskaMezera3=5.
      if(NPhase.le.3) then
        VyskaBragg=16.
      else if(NPhase.le.8) then
        VyskaBragg=12.
      else
        VyskaBragg=8.
      endif
      PruhBragg=VyskaBragg+3.
      Vyska=Vyska-VyskaMezera1
      if(KrDif) then
        Vyska=Vyska-VyskaMezera3
        if(KresliBragg) Vyska=Vyska-VyskaMezera2
        if(Celkem.gt.0.) then
          VyskaDif=Vyska*(YDifMin+YDifMax)/Celkem
        else
          VyskaDif=10.
        endif
      endif
      if(KresliBragg) then
        y1bragg=YMinPlW+VyskaMezera1
        if(KrDif) y1bragg=y1bragg+VyskaDif+VyskaMezera2
        y2bragg=y1bragg+VyskaBragg
      endif
      YMinPlC=YMinPlC+VyskaMezera1
      if(KrDif) YMinPlC=YMinPlC+VyskaDif
      if(KresliBragg) YMinPlC=YMinPlC+PruhBragg*float(NPhase)
      if(KrDif.or.KresliBragg) YMinPlC=YMinPlC+VyskaMezera3
      if(KrDif.and.KresliBragg) YMinPlC=YMinPlC+VyskaMezera2
      if(YMinPlC.le.YMinPlW.or.YMinPlC.ge.YMaxPlC) go to 9000
      if(SqrtScale) then
        shy=sqrt(YpMin)
      else
        shy=YpMin
      endif
      skydif=(YMaxPlC-YMinPlC)/(IntMax-YpMin)
      if(SqrtScale) then
        sky=(YMaxPlC-YMinPlC)/(sqrt(IntMax)-sqrt(YpMin))
      else
        sky=(YMaxPlC-YMinPlC)/(IntMax-YpMin)
      endif
      shx=XPwd(n1)
      skx=(XMaxPlC-XMinPlC)/(XPwd(n2)-XPwd(n1))
      do i=max(1,n1-1),n2
        XPwdP(i)=PwdPrfTh2X(XPwd(i))
      enddo
      if(KresliManBackg) then
        do i=max(1,n1g-1),min(n2g+1,NManBackg(KDatBlock))
          XPwdG(i)=PwdPrfTh2X(XManBackg(i,KDatBlock))
        enddo
      endif
      if(HardCopy.eq.0) then
        if(Klic.ne.-2) then
          call FeClearGrWin
        else
          call FeFillRectangle(XMinPlW,XMaxPlW,YMinPlW,YMaxPlW,4,0,0,
     1                         Black)
        endif
      endif
      call FeFillRectangle(XMinPlW,XMaxPlW,YMinPlW,YMaxPlW,0,0,0,White)
2000  do i=max(1,n1-1),n2
        CalcPArr(i)=PwdPrfInt2Y(YcPwd(i))
        if(KrDif) DifArr(i)=(DifArr(i)+YDifMin)*skydif+YMinPlW+
     1                       VyskaMezera1
      enddo
      if(KresliObsPoints.and.(klic.ne.-2.or.
     1   (.not.KresliCalcLine.and..not.KresliObsLine))) then
        KrObs=.true.
        dzn=min(DegStepPwd*skx-2.,2.)
        dznh=dzn/sqrt(2.)
      else
        KrObs=.false.
      endif
      if(klic.eq.-2) go to 2340
      ZeroCalc=PwdPrfInt2Y(YpMin)
!      ZeroCalc=PwdPrfInt2Y(0.)
      if(KrDif) ZeroDif=YDifMin*skydif+YMinPlW+VyskaMezera1
      if(KresliZero) then
        Color=White
      else
        Color=Black
      endif
      call FeLineType(DashedLine)
      seg1x(1)=XMinPlC
      seg1x(2)=XMaxPlC
      seg1y(1)=ZeroCalc
      seg1y(2)=ZeroCalc
      call FePolyLine(2,seg1x,seg1y,Color)
      if(KrDif) then
        seg1y(1)=ZeroDif
        seg1y(2)=ZeroDif
        call FePolyLine(2,seg1x,seg1y,Color)
      endif
      call FeLineType(NormalLine)
      if(SqrtScale) then
        ch20='sqrt(IRel)'
      else
        ch20='Irel'
      endif
      call FeOutSt(0,XMaxTx,YMaxPlW-20.,ch20,'R',White)
      if(KresliLab) then
        dify=(PwdPrfInt2Y(YpMax)-PwdPrfInt2Y(0.))/100.
        if(dify.lt.2.) then
          idrobne=0
        else
          idrobne=1
        endif
        pomy=PwdPrfInt2Y(0.)
        pisy =-1000.
        pisyd= 1000.
        nseg=0
        do i=0,100
          if(pomy.lt.YMinPlC) go to 2150
          if(pomy.gt.YMaxPlW) go to 2210
          if(idrobne.eq.0.and.mod(i,10).ne.0) go to 2150
          nseg=nseg+1
          seg1y(nseg)=pomy
          seg2y(nseg)=pomy
          SegCol(nseg)=White
          if(mod(i,10).eq.0) then
            seg1x(nseg)=XMinPlW-5.
            seg2x(nseg)=XMinPlW
            if(pomy-pisy.gt.2.*PropFontWidthInPixels) then
              write(ch20,'(f3.1)') float(i)/100.
c              call FeOutSt(0,XMinAcWin+5.,pomy,ch20,'L',White)
              call FeOutSt(0,XMinPlW-9.,pomy,ch20,'R',White)
              pisy=pomy
              pisyd=pisy
            endif
          else
            if(pomy-pisyd.gt.4.*PropFontWidthInPixels) then
              write(ch20,'(f4.2)') float(mod(i,10))/100.
              call FeOutSt(0,XMinPlW-9.,pomy,ch20(index(ch20,'.'):),
     1                     'R',White)
            endif
            pisyd=pomy
            seg1x(nseg)=XMinPlW-2.
            seg2x(nseg)=XMinPlW
          endif
          if(nseg.ge.100) then
            call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
            nseg=0
          endif
2150      pomy=pomy+dify
        enddo
        pomy=YMaxPlW-10.
        if(KresliMaxCounts) then
          write(ch20,'(i10,1x,''COUNTS(o)'')') nint(YObsMax)
          call FeDelTwoSpaces(ch20)
          pomx=XMinPlW+10.
          call FeOutSt(0,pomx,pomy,ch20,'L',White)
          write(ch20,'(i10,1x,''COUNTS(c)'')') nint(YCalcMax)
          call FeDelTwoSpaces(ch20)
          pomy=pomy-PropFontWidthInPixels/0.6-5.
          call FeOutSt(0,pomx,pomy,ch20,'L',White)
        endif
2210    if(nseg.gt.0) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        if(KrDif) then
          seg1x(1)=XMinPlW-5.
          seg1x(2)=XMinPlW
          seg1x(3)=XMinPlW-2.
          seg1x(4)=XMinPlW
          i1=nint((ZeroDif-YMinPlW-VyskaMezera1)/dify)
          i2=nint(((YDifMax+YDifMin)*skydif+YMinPlW+VyskaMezera1-
     1            ZeroDif)/dify)
          pisy=-1000.
          do i=-i1-1,i2+1
            pomy=ZeroDif+i*dify
            if(pomy.le.YMinPlW.or.pomy.ge.y1bragg) cycle
            seg1y(1)=pomy
            seg1y(2)=pomy
            if(mod(i,10).eq.0) then
              call FePolyLine(2,seg1x,seg1y,White)
              if(pomy-pisy.gt.2.*PropFontWidthInPixels) then
                write(ch20,'(f4.1)') float(i)/100.
                call zhusti(ch20)
                call FeOutSt(0,XMinPlW-9.,pomy,ch20,'R',White)
                pisy=pomy
                if(i.eq.0) then
                  if(SqrtScale) then
                    Veta='sqrt(Iobs)-'
                    pomx=XMaxTx-FeTxLength(Veta)
                    call FeOutSt(0,pomx,pomy+8.,Veta,'L',White)
                    call FeOutSt(0,pomx,pomy-8.,'sqrt(Icalc)','L',
     1                           White)
                  else
                    call FeOutSt(0,XMaxTx,pomy,'delta(Irel)','R',White)
                  endif
                endif
              endif
            else
              if(idrobne.eq.1) call FePolyLine(2,seg1x(3),seg1y,White)
            endif
          enddo
          call FeOutSt(0,XMinPlW-9.,ZeroDif,'0.0','R',White)
        endif
        DegNaDil=(XMaxPlW-XMinPlW)*0.2*(DegMax-DegMin)/
     1    (XMaxPlW-XMinPlW-1.)
        if(DegNaDil.gt.10.) then
          step=10.
          idiv=10
        else if(DegNaDil.gt.5.) then
          step=5.
          idiv=5
        else if(DegNaDil.gt.2.) then
          step=2.
          idiv=10
        else if(DegNaDil.gt.1.) then
          step=1.
          idiv=10
        else if(DegNaDil.gt.0.5) then
          step=0.5
          idiv=5
        else if(DegNaDil.gt.0.2) then
          step=0.2
          idiv=10
        else if(DegNaDil.gt.0.1) then
          step=0.1
          idiv=10
        else if(DegNaDil.gt.0.05) then
          step=0.05
          idiv=5
        else
          step=0.01
          idiv=10
        endif
        ch10='(f6.1)'
        write(ch10(5:5),'(i1)') max(-Exponent10(DegNaDil),1)
        idrobne=1
        if(PwdPrfTh2X(DegMin+step/idiv)-PwdPrfTh2X(DegMin).lt.2.) then
          if(idiv.eq.20) then
            idiv=10
            if(PwdPrfTh2X(DegMin+step/idiv)-
     1         PwdPrfTh2X(DegMin).lt.2.) idrobne=0
          else
            idrobne=0
          endif
        endif
        if(idrobne.eq.0) idiv=1.
        deg=DegMin*float(idiv)/step
        deg=float(nint(deg)+1)*(step/float(idiv))
        pisx=-1000.
        nseg=0
        pomx1=0.
        pomx2=0.
2300    pomx=PwdPrfTh2X(deg)
        nseg=nseg+1
        seg1x(nseg)=pomx
        seg2x(nseg)=pomx
        SegCol(nseg)=White
        pom=deg/step
        if(abs(pom-float(nint(pom))).le.0.01) then
          deg=step*float(nint(pom))
          write(ch20,ch10) deg
          call zhusti(ch20)
          pom=FeTxLength(ch20)*.5
          if(pomx-pom.ge.pisx+distLabel) then
            pisx=pomx+pom
            pomx1=pomx2
            pomx2=pomx
            if(pisx.lt.XMaxPlW)
     1        call FeOutSt(0,pomx,
     2          YMinPlW-2.-1.-PropFontHeightInPixels,ch20,'C',White)
          endif
          seg1y(nseg)=YMinPlW-5.
          seg2y(nseg)=YMinPlW
        else
          if(idrobne.eq.1) then
            seg1y(nseg)=YMinPlW-2.
            seg2y(nseg)=YMinPlW
          endif
        endif
        if(nseg.ge.100) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        deg=deg+step/float(idiv)
        if(deg.le.DegMax) go to 2300
        if(nseg.gt.0) then
          call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,SegCol)
          nseg=0
        endif
        if(pomx1.gt.0..or.pomx2.gt.0.) then
          pomx=(pomx1+pomx2)*.5
          if(DrawInD) then
            ch20='d'
          else
            if(isTOF) then
              ch20='t'
            else if(isED) then
              ch20='E'
            else
              ch20='2th'
            endif
          endif
          call FeOutSt(0,pomx,YMinPlW-2.-1.-PropFontHeightInPixels,ch20,
     1                 'C',White)
        endif
      endif
      if(KresliTitle.and.plotTitle.ne.' ') then
        if(CenteredTitle) then
          call FeOutSt(0,XMinPlW+0.5*(XMaxPlW-XMinPlW),YMaxPlW-8.,
     1                 plotTitle,'C',White)
        else
          pom=FeTxLengthUnder(plotTitle)
          call FeOutSt(0,XMaxPlW-pom-2.,YMaxPlW-8.,plotTitle,'L',White)
        endif
      endif
      if(KresliBragg) then
        xpom=XMinPlW-5.
        do i=1,NPhase
          if(BraggLabel.eq.0) then
            Cislo=PhaseName(i)
          else
            Cislo='('//char(ichar('a')+i-1)//')'
          endif
          ypom=y2bragg+PruhBragg*(float(i-1)-.5)+1.
          call FeOutSt(0,xpom,ypom,Cislo,'R',White)
        enddo
      endif
2340  ist=0
      nseg=0
      if(KresliDetails.and.DetailyAno.and.klic.ne.-2) then
        NRefPwd=1
        NLamPwd=1
        iist=0
        iien=0
        if(DrawInD) then
          if(isTOF) then
            DegMinP=PwdTOF2D(DegMin)
            DegMaxP=PwdTOF2D(DegMax)
          else
            if(KRefLam(KDatBlock).eq.1) then
              DegMinP=PwdD2TwoTh(DegMax,LamPwd(1,KDatBlock))
              DegMaxP=PwdD2TwoTh(DegMin,LamPwd(1,KDatBlock))
            else
              DegMinP=PwdD2TwoTh(DegMax,LamAve(KDatBlock))
              DegMaxP=PwdD2TwoTh(DegMin,LamAve(KDatBlock))
            endif
          endif
        else
          DegMinP=DegMin
          DegMaxP=DegMax
        endif
        if(.not.allocated(ihPwdArr)) then
          n=1
          m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
          l=NAlfa(KDatBlock)
          allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),MultPwdArr(n),
     1             iqPwdArr(n),KPhPwdArr(n),ypeaka(l,n),peaka(l,n),
     2             pcota(l,n),rdega(2,l,n),shifta(l,n),fnorma(l,n),
     3             tntsima(l,n),ntsima(l,n),sqsga(l,n),fwhma(l,n),
     4             sigpa(l,n),etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     5             dfwdga(l,n),dfwdla(l,n),coef(m,l,n),coefp(m,l,n),
     6             coefq(2,l,n),Prof0(n),cotg2tha(l,n),cotgtha(l,n),
     7             cos2tha(l,n),cos2thqa(l,n),Alpha12a(l,n),
     8             Beta12a(l,n),IBroadHKLa(n))
          if(.not.isTOF.and..not.isED.and.
     1       KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     2      allocate(AxDivProfA(-25:25,l,n),DerAxDivProfA(-25:25,7,l,n),
     3               FDSProf(-25:25,l,n))
        endif
        iin=0
        do i=1,NBragg
          if(DrawInD) then
            if(isTOF) then
              pom=PwdTOF2D(BraggArr(i))
            else
              if(KRefLam(KDatBlock).eq.1) then
                pom=PwdD2TwoTh(BraggArr(i),LamPwd(1,KDatBlock))
              else
                pom=PwdD2TwoTh(BraggArr(i),LamAve(KDatBlock))
              endif
            endif
          else
            pom=BraggArr(i)
          endif
          th=(pom+ShiftArr(i))*ToRad*0.5
          KPh=KlicArr(i)/100+1
          KPhase=KPh
          call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
          call SetProfFun(indArr(1,i),hh,th)
          if(rdeg(1)/torad.le.DegMaxP.and.rdeg(2)/torad.ge.DegMinP) then
            if(iist.eq.0) iist=i
            iien=i
            iin=iin+1
          endif
        enddo
2360    if(iist.gt.0) then
          iist=iist-1
          iien=iien+1
          iin=iin+2
        else
          iist=1
          iien=1
          iin=1
        endif
        if(iin.gt.100) then
          if(Psani.eq.1) then
            TextInfo(1)='You are about to plot component functions for'
            write(Cislo,'(i4)') iin
            call Zhusti(Cislo)
            TextInfo(2)=Cislo(:idel(Cislo))//
     1                  ' Bragg peaks. Do you want to continue?'
            NInfo=2
            DetailyAno=FeYesNoLong(-1.,-1.,'Warning',0)
          endif
          if(.not.DetailyAno) then
            iist=1
            iien=1
            KresliDetails=.false.
          endif
        endif
      else
        iist=1
        iien=1
      endif
      call SetIntArrayTo(kkFlag,3,0)
      kkFlag(1)=1
      if(KresliObsLine.and.(Klic.ne.-2.or..not.KresliCalcLine))
     1  kkFlag(2)=1
      if(KresliManBackg) kkFlag(3)=1
      if(KresliCalcLine) then
        whLine=whichLine
      else
        whLine=2
      endif
      if(KrObs.or.KresliObsLine.or.KresliCalcLine) then
        k=0
        do j=n1,n2
2430      if(YfPwd(j).eq.0) then
            if(k.eq.0) then
              k=k+1
              seg1x(k)=XPwdP(j)
              seg1y(k)=YMinPlC
            endif
            k=k+1
            seg1x(k)=XPwdP(j)
            if(KrObs.or.KresliObsLine) then
              seg1y(k)=PwdPrfInt2Y(abs(YoPwd(j)))
            else
              seg1y(k)=PwdPrfInt2Y(abs(YcPwd(j)))
            endif
            seg1y(k)=min(seg1y(k),YMaxPlC)
            if(k.lt.98) cycle
          endif
          if(k.gt.0) then
            k=k+1
            seg1x(k)=seg1x(k-1)
            seg1y(k)=YMinPlC
            k=k+1
            seg1x(k)=seg1x(1)
            seg1y(k)=YMinPlC
            call FePolygon(seg1x,seg1y,k,4,0,0,ColorSkip)
            k=0
            if(YfPwd(j).eq.0) go to 2430
          endif
        enddo
        if(k.gt.1) then
          k=k+1
          seg1x(k)=seg1x(k-1)
          seg1y(k)=YMinPlC
          k=k+1
          seg1x(k)=seg1x(1)
          seg1y(k)=YMinPlC
          call FePolygon(seg1x,seg1y,k,4,0,0,ColorSkip)
        endif
      endif
      do ii=iist,iien
        if(ii.gt.iist) then
          call PwdPrfSum(n1,n2,ii,ii.eq.iist+1,ii.eq.iien)
          do i=n1,n2
            CalcPArr(i)=PwdPrfInt2Y(CalcPArr(i))
          enddo
        else
          Color=ColorCalc
        endif
        if(ii.eq.iien.and.iien.gt.1) then
          if(whLine.eq.3) then
            do i=n1,n2
              if(abs(PwdPrfInt2Y(YcPwd(i))-CalcPArr(i)).gt.1.)
     1          go to 2920
            enddo
            cycle
          else if(whLine.eq.2) then
            Color=ColorCalc
          else
            call FeLineType(NormalLine)
            cycle
          endif
        endif
2920    if(ii.eq.iist) then
          call FeLineType(NormalLine)
          kken=3
        else
          call FeLineType(DenseDashedLine)
          kken=1
        endif
        do kk=1,kken
          if(kkFlag(kk).eq.0) cycle
          if(kk.eq.1) then
            if(ii.eq.iist.and.iien.gt.iist.and.whLine.eq.2) cycle
            do i=n1,n2
              XPwdA(i)=XPwdP(i)
            enddo
            n1p=n1
            n2p=n2
          else if(kk.eq.2) then
            Color=ColorObs
            do i=n1,n2
              CalcPArr(i)=PwdPrfInt2Y(abs(YoPwd(i)))
              XPwdA(i)=XPwdP(i)
            enddo
            n1p=n1
            n2p=n2
          else if(kk.eq.3) then
            Color=Magenta
            do i=max(1,n1g-1),min(n2g+1,NManBackg(KDatBlock))
              CalcPArr(i)=PwdPrfInt2Y(abs(YManBackg(i,KDatBlock))*
     1                                Sc(2,KDatBlock))
              XPwdA(i)=XPwdG(i)
            enddo
            n1p=n1g
            n2p=n2g
          endif
          iskip=0
          do i=n1p,n2p
            if(ii.eq.iist.and.kk.eq.1.and..not.KresliCalcLine)
     1        go to 2990
            if(CalcPArr(i).le.YMaxPlC.and.ist.eq.0) ist=i
            if(CalcPArr(i).gt.YMaxPlC.or.i.eq.n2p) then
              if(ist.eq.0) go to 2990
              if(i-ist.ge.1) then
                nahradZacatek=.false.
                nahradKonec=.false.
                if(ist.gt.n1p) then
                  if(CalcPArr(ist-1).gt.YMaxPlC) then
                    nahradZacatek=.true.
                    call PwdPrfPrusY(XPwdA(ist),CalcPArr(ist),
     1                XPwdA(ist-1),CalcPArr(ist-1),pomx1,pomy1,
     2                YMaxPlC)
                    ist=ist-1
                  endif
                endif
                if(CalcPArr(i).gt.YMaxPlC) then
                  nahradKonec=.true.
                  call PwdPrfPrusY(XPwdA(i-1),CalcPArr(i-1),
     1              XPwdA(i),CalcPArr(i),pomx,pomy,YMaxPlC)
                endif
                call FePolyLine(i-ist+1,XPwdA(ist),CalcPArr(ist),
     1            Color)
                if(nahradKonec) then
                  XPwdA(i)=pomx
                  CalcPArr(i)=pomy
                endif
                if(nahradZacatek) then
                  XPwdA(ist)=pomx1
                  CalcPArr(ist)=pomy1
                endif
                ist=0
              endif
            endif
2990        if(kk.le.2) then
              if(KrObs.and.ii.eq.iist) then
                pomx=XPwdA(i)
                pomy=PwdPrfInt2Y(abs(YoPwd(i)))
                if(pomy+dzn.le.YMaxPlC) then
                  nseg=nseg+1
                  seg1x(nseg)=pomx-dznh
                  seg2x(nseg)=pomx+dznh
                  seg1y(nseg)=pomy-dznh
                  seg2y(nseg)=pomy+dznh
                  SegCol(nseg)=ColorObs
                  nseg=nseg+1
                  seg1x(nseg)=pomx-dznh
                  seg2x(nseg)=pomx+dznh
                  seg1y(nseg)=pomy+dznh
                  seg2y(nseg)=pomy-dznh
                  SegCol(nseg)=ColorObs
                endif
                if(nseg.ge.50.or.(i.eq.n2p.and.nseg.gt.0)) then
                  call FeDrawSegments(nseg,seg1x,seg1y,seg2x,seg2y,
     1                                SegCol)
                  nseg=0
                endif
              endif
            else
              pomx=XPwdA(i)
              pomy=CalcPArr(i)
              call FeCircle(pomx,pomy,4.,Magenta)
            endif
          enddo
          if(kk.eq.3.and.n2p.gt.0.and.n1p.lt.NManBackg(KDatBlock)) then
            if(n1p.gt.n2p) then
              call PwdPrfPrusX(XPwdA(n2p),CalcPArr(n2p),
     1                         XPwdA(n1p),CalcPArr(n1p),
     2                         seg1x(1),seg1y(1),XMinPlC)
              call PwdPrfPrusX(XPwdA(n2p),CalcPArr(n2p),
     1                         XPwdA(n1p),CalcPArr(n1p),
     2                         seg1x(2),seg1y(2),XMaxPlC)
              call FePolyLine(2,seg1x,seg1y,Magenta)
            else
              if(n1p.gt.1) then
                call PwdPrfPrusX(XPwdA(n1p-1),CalcPArr(n1p-1),
     1                           XPwdA(n1p  ),CalcPArr(n1p  ),
     2                           seg1x(1),seg1y(1),XMinPlC)
                seg1x(2)=XPwdA(n1p)
                seg1y(2)=CalcPArr(n1p)
                call FePolyLine(2,seg1x,seg1y,Magenta)
              endif
              if(n2p.lt.NManBackg(KDatBlock)) then
                seg1x(1)=XPwdA(n2p)
                seg1y(1)=CalcPArr(n2p)
                call PwdPrfPrusX(XPwdA(n2p)  ,CalcPArr(n2p)  ,
     1                           XPwdA(n2p+1),CalcPArr(n2p+1),
     2                           seg1x(2),seg1y(2),XMaxPlC)
                call FePolyLine(2,seg1x,seg1y,Magenta)
              endif
            endif
          endif
        enddo
        Color=ColorCalc
      enddo
      call FeLineType(NormalLine)
      if(KrDif) then
        call FePolyLine(n2-n1+1,XPwdP(n1),DifArr(n1),ColorDif)
        do i=max(1,n1-1),n2
          if(KrDif) DifArr(i)=(DifArr(i)-YMinPlW-VyskaMezera1)/skydif-
     1                        YDifMin
        enddo
      endif
      if(KresliBragg.and.klic.ne.-2) then
        do i=n1b,n2b
          seg1x(1)=PwdPrfTh2X(BraggArr(i))
          seg1x(2)=seg1x(1)
          KPh=KlicArr(i)/100+1
          KPhase=KPh
          k=mod(KlicArr(i),100)
          if(CrlItIsSatellite(indArr(1,i))) then
            Color=ColorSat
          else
            j=CrlSubSystemNumber(indArr(1,i))
            if(j.le.0) then
              Color=ColorMain12
            else if(j.eq.1) then
              Color=ColorMain1
            else
              Color=ColorMain2
            endif
          endif
          Zdvih=PruhBragg*float(KPh-1)
          if(k.eq.1) then
            seg1y(1)=y2bragg+Zdvih-VyskaBragg*.5
          else
            seg1y(1)=y1bragg+Zdvih
          endif
          seg1y(2)=y2bragg+Zdvih
          call FePolyLine(2,seg1x,seg1y,Color)
        enddo
        ybragg=y1bragg+0.5*(y2bragg-y1bragg)
      else
        ybragg=0.
      endif
      go to 9999
9000  ErrFlag=0
9999  if(allocated(XPwdP)) deallocate(XPwdP,XPwdG,XPwdA)
      return
      end
      subroutine PwdUpdateReflFiles
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      do i=1,NRefBlock
        if(RefDatCorrespond(i).eq.KDatBlock) then
          KRefBlock=i
          go to 1100
        endif
      enddo
      go to 9999
1100  write(Cislo,'(''.l'',i2)') KRefBlock
      if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
      RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
      call DeleteFile(RefBlockFileName)
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      call PwdPutRecordToM95(95)
      ModifiedRefBlock(KRefBlock)=.true.
      call CloseIfOpened(95)
      call CompleteM95(0)
      call EM9CreateM90Powder(ich)
      call CompleteM90
9999  return
      end
      subroutine PwdPrfInfo(th,hcalc,hobs,hdif,hbragg,xkam)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      logical   obnova
      real xlow(5),xhigh(5),ylow(5),yhigh(5)
      character*1 zn
      character*80 ch80(5)
      character*50 jmena(5)
      save jmena,xlow,xhigh,ylow,yhigh
      fhp=PropFontWidthInPixels*.3
      ymx=max(PwdPrfInt2Y(hcalc),PwdPrfInt2Y(hobs))+12.
      if(ymx.gt.ymaxplW-1.) ymx=ymaxplW-1.
      obnova=.false.
      go to 200
      entry PwdPrfInfoObnova
      obnova=.true.
200   if(OpSystem.le.0) call FePlotMode('N')
      do 600k=1,3
        if(k.gt.1.and.obnova) go to 600
        do 500i=1,5
          if(obnova) then
            if(xlow(i).eq.-999.) go to 500
            call FeLoadImage(xlow(i),xhigh(i),ylow(i),yhigh(i),
     1                       jmena(i),0)
            go to 500
          else
            if((hbragg.eq.0.and.i.eq.5).or.
     1        (.not.KresliDifLine.and.i.eq.4).or.
     2        (kType.eq.0.and.i.eq.2)) then
              xlow(i)=-999.
              go to 500
            endif
          endif
          if(k.eq.1) then
            if(i.eq.1) write(ch80(i),'(f10.3)') th
            if(i.eq.2) write(ch80(i),'(f10.1,''(c)'')') hcalc
            if(i.eq.3) write(ch80(i),'(f10.1,''(o)'')') hobs
            if(i.eq.4) write(ch80(i),'(f10.1,''(o-c)'')') hdif
            if(i.eq.5) write(ch80(i),'(f20.3)') hbragg
            call zhusti(ch80(i))
            if(OpSystem.le.0) then
              jmena(i)='jinf'
              call CreateTmpFile(jmena(i),j,1)
            else
              write(zn,'(i1)') i
              jmena(i)='jinf'//zn
            endif
            xl=FeTxLength(ch80(i))
            if(i.eq.1) yp=yminplW+3.
            if(i.eq.2) yp=ymx-36.
            if(i.eq.3) yp=ymx-21.
            if(i.eq.4) yp=ZeroDif
            if(i.eq.5) yp=ybragg
            xlow(i)=xkam+2.
            xhigh(i)=xkam+xl+4.
            ylow(i)=yp-fhp-9.
            yhigh(i)=yp+fhp+9.
            if(xhigh(i).gt.XMaxBasWin) then
              pom=xhigh(i)-xlow(i)+21.
              xlow(i)=xlow(i)-pom
              xhigh(i)=xhigh(i)-pom
            endif
            call FeSaveImage(xlow(i),xhigh(i),ylow(i),yhigh(i),jmena(i))
          else if(k.eq.2) then
            call FeFillRectangle(xlow(i),xhigh(i),ylow(i),yhigh(i),
     1                           4,0,0,Black)
          else
            xp=xlow(i)+1.
            yp=ylow(i)+.5*(yhigh(i)-ylow(i))
            call FeOutSt(0,xp,yp,ch80(i),'L',Yellow)
          endif
500     continue
600   continue
1000  if(OpSystem.le.0) call FePlotMode('E')
      return
      end
      subroutine PwdPrfSum(n1,n2,nb,zacni,skonci)
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'refine.cmn'
      logical zacni,skonci
      dimension derp(36),hh(3)
      if(zacni) then
        call PwdSetBackground
        do i=n1,n2
          if(DrawInD) then
            if(isTOF) then
              XPwdi=PwdTOF2D(XPwd(i))
            else
              if(KRefLam(KDatBlock).eq.1) then
                XPwdi=PwdD2TwoTh(XPwd(i),LamPwd(1,KDatBlock))
              else
                XPwdi=PwdD2TwoTh(XPwd(i),LamAve(KDatBlock))
              endif
            endif
          else
            XPwdi=XPwd(i)
          endif
          call PwdGetBackground(XPwdi,CalcPPArr(i),derp,DerSc)
          BkgArr(i)=CalcPPArr(i)
        enddo
      else if(skonci) then
        call CopyVek(CalcPPArr(n1),CalcPArr(n1),n2-n1+1)
        go to 3000
      endif
      if(DrawInD) then
        if(isTOF) then
          pom=PwdTOF2D(BraggArr(nb))
        else
          if(KRefLam(KDatBlock).eq.1) then
            pom=PwdD2TwoTh(BraggArr(nb),LamPwd(1,KDatBlock))
          else
            pom=PwdD2TwoTh(BraggArr(nb),LamAve(KDatBlock))
          endif
        endif
      else
        pom=BraggArr(nb)
      endif
      th=(pom+ShiftArr(nb))*torad*0.5
      KPh=KlicArr(nb)/100+1
      KPhase=KPh
      call FromIndSinthl(indArr(1,nb),hh,sinthl,sinthlq,1,0)
      call SetProfFun(indArr(1,nb),hh,th)
      do i=n1,n2
        if(DrawInD) then
          if(isTOF) then
            XPwdi=PwdTOF2D(XPwd(i))
          else
            if(KRefLam(KDatBlock).eq.1) then
              XPwdi=PwdD2TwoTh(XPwd(i),LamPwd(1,KDatBlock))
            else
              XPwdi=PwdD2TwoTh(XPwd(i),LamAve(KDatBlock))
            endif
          endif
        else
          XPwdi=XPwd(i)
        endif
        CalcPArr(i)=BkgArr(i)
        if(isTOF) then
          pom=XPwdi
          if(KUseTOFAbs(KDatBlock).gt.0) then
            Absorption=PwdTOFAbsorption(XPwdi,TOFAbsPwd(KDatBlock),
     1                                  DAbsorption)
          else
             Absorption=1.
          endif
        else
          pom=XPwdi*ToRad
          Absorption=1./PwdAbsorption(pom)
          call PwdRougness(pom,Roughness,derp)
          Absorption=Absorption*Roughness
        endif
        if(pom.ge.rdeg(1).and.pom.le.rdeg(2)) then
          prispevek=ypeakArr(nb)*prfl(pom-peak)*Absorption
          CalcPArr(i)=CalcPArr(i)+prispevek
          CalcPPArr(i)=CalcPPArr(i)+prispevek
        endif
      enddo
3000  return
      end
      subroutine PwdPrfBraggInfo(KPhClick)
      use Powder_mod
      use RefPowder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension px(2),py(2),hh(3)
      character*6 ch6
      character*8 ch8
      integer Color,UseTabsIn,CrlSubSystemNumber
      logical CrlItIsSatellite
      if(.not.KresliBragg.or.NBragg.le.0) go to 9999
      th=PwdPrfX2Th(xpos)
      plim=abs(PwdPrfX2Th(6.)-PwdPrfX2Th(0.))
      NInfo=1
      ipocet=0
      UseTabsIn=UseTabs
      UseTabs=NextTabs()
      do 1200i=1,NBragg
        if(abs(BraggArr(i)-th).gt.plim) then
          if(BraggArr(i).gt.th) go to 1300
          go to 1200
        endif
        KPhase=KlicArr(i)/100+1
        if(KPhase.ne.KPhClick) go to 1200
        k=mod(KlicArr(i),100)
        ipocet=ipocet+1
        if(ipocet.eq.1) then
          pom=15.
          TextInfo(1)=' '
          do j=1,NDim(KPhase)
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//
     1                  Tabulator//indices(j)
            call FeTabsAdd(pom,UseTabs,IdLeftTab,' ')
            pom=pom+15.
          enddo
          pom=pom+25.
          do j=1,3
            if(j.eq.1) then
              if(DrawInD) then
                Cislo='d'
              else
                Cislo='2th'
              endif
            else if(j.eq.2) then
              Cislo='F**2'
            else if(j.eq.3) then
              Cislo='FWHM'
            endif
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//Tabulator//
     1                  Cislo(:idel(Cislo))
            call FeTabsAdd(pom,UseTabs,IdCenterTab,' ')
            pom=pom+60.
          enddo
          pom=pom-30.
          if(kAlpha2.gt.0) then
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//Tabulator//
     1                  'Doublet'
            call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
            pom=pom+50.
          endif
          if(NPhase.gt.1) then
            TextInfo(1)=TextInfo(1)(:idel(TextInfo(1)))//Tabulator//
     1                  'Phase'
            call FeTabsAdd(pom,UseTabs,IdRightTab,' ')
          endif
        endif
        Zdvih=PruhBragg*float(KPhase-1)
        if(k.eq.1) then
          py(1)=y2bragg+Zdvih-VyskaBragg*.5
        else
          py(1)=y1bragg+Zdvih
        endif
        py(2)=y2bragg+Zdvih
        px(1)=PwdPrfTh2X(BraggArr(i))
        px(2)=px(1)
        if(px(1).lt.XminPlC.or.px(1).gt.XmaxPlC) go to 9999
        call FePolyLine(2,px,py,Cyan)
        if(NInfo.eq.19) go to 1200
        NInfo=NInfo+1
        pos=BraggArr(i)+ShiftArr(i)
        posp=.5*torad*pos
        if(kType.ne.0) then
          call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
          NRefPwd=1
          NLamPwd=1
          if(.not.allocated(ihPwdArr)) then
            n=1
            m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
            l=NAlfa(KDatBlock)
            allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),MultPwdArr(n),
     1               iqPwdArr(n),KPhPwdArr(n),ypeaka(l,n),peaka(l,n),
     2               pcota(l,n),rdega(2,l,n),shifta(l,n),fnorma(l,n),
     3               tntsima(l,n),ntsima(l,n),sqsga(l,n),fwhma(l,n),
     4               sigpa(l,n),etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     5               dfwdga(l,n),dfwdla(l,n),coef(m,l,n),coefp(m,l,n),
     6               coefq(2,l,n),Prof0(n),cotg2tha(l,n),cotgtha(l,n),
     7               cos2tha(l,n),cos2thqa(l,n),Alpha12a(l,n),
     8               Beta12a(l,n),IBroadHKLa(n))
            if(.not.isTOF.and..not.isED.and.
     1         KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     2        allocate(AxDivProfA(-25:25,l,n),
     3                 DerAxDivProfA(-25:25,7,l,n),FDSProf(-25:25,l,n))
          endif
          call SetProfFun(indArr(1,i),hh,posp)
        else
          fwhm=0.
        endif
        KPhase=KlicArr(i)/100+1
        k=mod(KlicArr(i),100)
        if(k.eq.2) then
          ch6=' '
        else if(k.eq.0) then
          ch6='alpha1'
        else
          ch6='alpha2'
        endif
        if(NPhase.gt.1) then
          ch8=PhaseName(KPhase)
        else
          ch8=' '
        endif
        TextInfo(NInfo)=' '
        do j=1,NDim(KPhase)
          write(Cislo,FormI15) indArr(j,i)
          call Zhusti(Cislo)
          TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     1                    Tabulator//Cislo(:idel(Cislo))
        enddo
        do j=1,3
          if(j.eq.1) then
            pom=pos
          else if(j.eq.2) then
            pom=ypeakArr(i)
          else if(j.eq.3) then
            pom=fwhm/torad
          endif
          write(Cislo,'(f15.4)') pom
          call Zhusti(Cislo)
          TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     1                    Tabulator//Cislo(:idel(Cislo))
        enddo
        if(ch6.ne.' ')
     1    TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     2                    Tabulator//ch6(:idel(ch6))
        if(ch8.ne.' ')
     1    TextInfo(NInfo)=TextInfo(NInfo)(:idel(TextInfo(NInfo)))//
     2                    Tabulator//ch8(:idel(ch8))
1200  continue
1300  if(NInfo.eq.19.and.ipocet.gt.18) then
        write(TextInfo(NInfo),'(''... and another '',i5,
     1    '' reflections'')') ipocet-17
        call FeDelTwoSpaces(TextInfo(NInfo))
      endif
      call FeKillInfoWin
      if(NInfo.gt.1) then
        pom=y2bragg+2.+PruhBragg*float(KPhClick-1)
        call FeInfoWin(-1.,pom)
      else
        go to 9999
      endif
      do 2000i=1,NBragg
        if(abs(BraggArr(i)-th).gt.plim) then
          if(BraggArr(i).gt.th) go to 9999
          go to 2000
        endif
        KPhase=KlicArr(i)/100+1
        k=mod(KlicArr(i),100)
        Zdvih=PruhBragg*float(KPhase-1)
        if(k.eq.1) then
          py(1)=y2bragg+Zdvih-VyskaBragg*.5
        else
          py(1)=y1bragg+Zdvih
        endif
        py(2)=y2bragg+Zdvih
        px(1)=PwdPrfTh2X(BraggArr(i))
        px(2)=px(1)
        if(CrlItIsSatellite(indArr(1,i))) then
          Color=ColorSat
        else
          j=CrlSubSystemNumber(indArr(1,i))
          if(j.le.0) then
            Color=Red
          else if(j.eq.1) then
            Color=White
          else
            Color=Yellow
          endif
        endif
        call FePolyLine(2,px,py,Color)
2000  continue
9999  call FeTabsReset(UseTabs)
      UseTabs=UseTabsIn
      return
100   format(i1)
      end
      subroutine PwdPrfNacti(ktFile)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer ihh(6)
      integer, allocatable :: intpole(:),intorder(:)
      logical ExistFile,EqIgCase
      character*80 ch80
      character*120 radka
      if(ktFile.eq.0) then
        ZPrf=ExistFile(fln(:idel(fln))//'.prf')
      else
        ZPrf=.false.
      endif
      ln=0
1000  if(ZPrf) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:idel(fln))//'.prf','formatted','unknown')
        if(ErrFlag.ne.0) go to 9900
        KdeJe=0
1010    if(NDatBlock.gt.1) then
1020      read(ln,FormA,end=1030,err=9000) ch80
          k=0
          call Kus(ch80,k,Cislo)
          if(EqIgCase(Cislo,DatBlockName(KDatBlock))) then
            call Kus(ch80,k,Cislo)
            if(EqIgCase(Cislo,'begin')) then
              if(KdeJe.eq.0) then
                go to 1050
              else
                go to 1085
              endif
            endif
          endif
          go to 1020
1030      call CloseIfOpened(ln)
          ZPrf=.false.
          go to 1000
        endif
        if(KdeJe.eq.1) go to 1085
1050    read(ln,'(2i5)',end=9000,err=9000) kType,kAlpha2
        NBragg=1
        Npnts=0
1060    read(ln,FormA,end=9000,err=9000) radka
        if(NBragg.le.1) then
          call mala(Radka)
          i=index(Radka,'e')
          if(i.lt.63+(maxNDim-3)*4) then
            call FeChybne(-1.,-1.,'you are trying to use incorrect or'//
     1        ' Jana2000 "prf" file.','Please re-run the refinement '//
     2        'or le Bail decomposition.',SeriousError)
            go to 9900
          endif
        endif
        read(Radka,PrfFormat,err=9000)(ihh(i),i=1,maxNDim)
        if(ihh(1).gt.900) go to 1070
        NBragg=NBragg+1+kAlpha2
        go to 1060
1070    NBragg=NBragg+kAlpha2
        if(allocated(BraggArr))
     1    deallocate(BraggArr,ShiftArr,MultArr,ypeakArr,KlicArr,indArr)
        if(allocated(intpole)) deallocate(intpole,intorder)
        allocate(BraggArr(NBragg),ShiftArr(NBragg),MultArr(NBragg),
     1           ypeakArr(NBragg),KlicArr(NBragg),indArr(6,NBragg),
     2           intpole(NBragg),intorder(NBragg))
        nn=0
1080    read(ln,FormA,end=9000,err=9000) radka
        k=0
        call kus(radka,k,Cislo)
        call Posun(Cislo,1)
        read(Cislo,'(f15.0)') pom
        if(pom.gt.990.) go to 1082
        if(nn.eq.0.and.
     1     LocateSubstring(Radka,'e',.false.,.true.).le.0)
     2    PrfPointsFormat='(f10.3,3f10.1,f10.3,i5,99f10.1)'
        read(Radka,PrfPointsFormat,err=9000) pom
        nn=nn+1
        if(pom.lt.900.) go to 1080
1082    if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
        if(allocated(YcPwd)) deallocate(YcPwd,YbPwd,YfPwd,YcPwdInd)
        allocate(XPwd(nn),YoPwd(nn),YcPwd(nn),YbPwd(nn),YsPwd(nn),
     1           YfPwd(nn),YiPwd(nn),YcPwdInd(nn,NPhase))
        rewind ln
        KdeJe=1
        go to 1010
1085    read(ln,FormA,end=9000,err=9000) radka
        NBragg=1
1100    read(ln,FormA,end=9000,err=9000) radka
        read(Radka,PrfFormat,err=9000)(ihh(i),i=1,maxNDim),
     1        MultArr(NBragg),KPh,(BraggArr(i),ShiftArr(i),pom,
     2        ypeakArr(i),i=NBragg,NBragg+kAlpha2)
        if(ihh(1).gt.900) go to 1300
        do i=NBragg,NBragg+kAlpha2
          intpole(i)=nint(BraggArr(i)*10000.)
          call CopyVekI(ihh,indArr(1,i),maxNDim)
          BraggArr(i)=BraggArr(i)-ShiftArr(i)
        enddo
        k=(KPh-1)*100
        if(kAlpha2.gt.0) then
          KlicArr(NBragg)=k
          KlicArr(NBragg+1)=k+1
          MultArr(NBragg+1)=MultArr(NBragg)
        else
          KlicArr(NBragg)=k+2
        endif
        NBragg=NBragg+1+kAlpha2
        go to 1100
1300    NBragg=NBragg-1
        call indexx(NBragg,intpole,intorder)
        do 1500i=1,NBragg
          if(i.eq.intorder(i)) go to 1500
          io=intorder(i)
          pom=BraggArr(i)
          BraggArr(i)=BraggArr(io)
          BraggArr(io)=pom
          pom=MultArr(i)
          MultArr(i)=MultArr(io)
          MultArr(io)=pom
          pom=ShiftArr(i)
          ShiftArr(i)=ShiftArr(io)
          ShiftArr(io)=pom
          pom=ypeakArr(i)
          ypeakArr(i)=ypeakArr(io)
          ypeakArr(io)=pom
          ipom=KlicArr(i)
          KlicArr(i)=KlicArr(io)
          KlicArr(io)=ipom
          do j=1,NDim(KPhase)
            ipom=indArr(j,i)
            indArr(j,i)=indArr(j,io)
            indArr(j,io)=ipom
          enddo
          do j=i+1,NBragg
            if(intorder(j).eq.i) intorder(j)=io
          enddo
1500    continue
2000    read(ln,'(a)',end=8000) radka
        k=0
        call kus(radka,k,Cislo)
        call Posun(Cislo,1)
        read(Cislo,'(f15.0)') pom
        if(pom.gt.990.) go to 8000
        read(radka,PrfPointsFormat,err=9000,end=9000) XPwdP,YoPwdP,
     1    YcPwdP,YsPwdP,pom,i,YiPwdP
        Npnts=Npnts+1
        XPwd(Npnts)=XPwdP
        YoPwd(Npnts)=YoPwdP
        YsPwd(Npnts)=YsPwdP
        YcPwd(Npnts)=YcPwdP
        YiPwd(Npnts)=YiPwdP
        if(kType.eq.0) YcPwd(Npnts)=abs(YoPwd(Npnts))
        YcPwd(Npnts)=max(0.,YcPwd(Npnts))
        YfPwd(NPnts)=1
        go to 2000
      else
        kType=0
        NBragg=0
        call PwdM92Nacti
        if(ErrFlag.ne.0) go to 9999
        do i=1,Npnts
          YcPwd(i)=abs(YoPwd(i))
        enddo
        go to 8100
      endif
8000  call PwdMakeSkipFlags
8100  if(Npnts.le.0) then
        call FeChybne(-1.,-1.,
     1    'The file doesn''t contain profile data.',' ',SeriousError)
        ErrFlag=0
      endif
      go to 9990
9000  call FeReadError(ln)
9900  ErrFlag=1
9990  if(ln.gt.0) call CloseIfOpened(ln)
9999  if(allocated(intpole)) deallocate(intpole,intorder)
      return
      end
      subroutine locate(xx,n,x,j)
      include 'fepc.cmn'
      real xx(n)
      jl=0
      ju=n+1
100   if(ju-jl.gt.1) then
        jm=(ju+jl)/2
        if(xx(n).ge.xx(1).eqv.x.ge.xx(jm)) then
          jl=jm
        else
          ju=jm
        endif
        go to 100
      endif
      if(x.le.xx(1)) then
        j=1
      else if(x.ge.xx(n)) then
        j=n
      else
        j=jl
      endif
      return
      end
      subroutine PwdPrfOptions(ich,JenomNastav,menilFile,ktFile,istt)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension nCrwKresliLecos(7),nEdwA(5)
      integer EdwStateQuest
      logical JenomNastav,nechT,CrwLogicQuest,ExistFile,menilFile,lpom
      logical :: UzTadyByl = .false.
      character*80 ch80
      character*256 EdwStringQuest
      integer RolMenuSelectedQuest
      menilFile=.false.
      nechT=.false.
      nButtDefaults=0
      if(JenomNastav) then
        ZPrf=ExistFile(fln(:ifln)//'.prf')
        go to 2100
      endif
      xqd=500.
      il=12
      if(maxNDimI.gt.0) il=il+1
      if(maxNComp.gt.1) il=il+2
      if(NPhase.gt.1) il=il+1
      id=NextQuestId()
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Options',0,LightGray,0,0)
      il=1
      offset=5.
      ch80='Input file:'
      call FeQuestLblMake(id,offset,il,ch80,'L','N')
      xpom=offset+FeTxLengthUnder(ch80)+10.
      tpom=xpom+CrwYd+5.
      ch80=fln(:idel(fln))//'.m90'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,1,1)
      nCrwm90=CrwLastMade
      xpom=tpom+FeTxLengthUnder(ch80)+10.
      tpom=xpom+CrwYd+5.
      ch80=fln(:idel(fln))//'.prf'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,1,1)
      nCrwPrf=CrwLastMade
      if(.not.ExistFile(fln(:ifln)//'.prf')) then
        call FeQuestLblDisable(LblLastMade)
        call FeQuestCrwDisable(nCrwm90)
        call FeQuestCrwDisable(nCrwPrf)
      endif
      il=il+1
      xpom=offset
      tpom=xpom+CrwYd+5.
      ch80='Show title'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,1,0)
      nCrwShowTitle=CrwLastMade
      xpom=tpom+FeTxLengthUnder(ch80)+10.
      tpom=xpom+CrwYd+5.
      ch80='Centered'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwgXd,CrwgYd,0,2)
      nCrwCent=CrwLastMade
      xpom=tpom+FeTxLengthUnder(ch80)+10.
      tpom=xpom+CrwYd+5.
      ch80='Right aligned'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwgXd,CrwgYd,0,2)
      nCrwRight=CrwLastMade
      il=il+1
      tpom=offset
      ch80='Title:'
      xpom=tpom+FeTxLengthUnder(ch80)+10.
      dpom=250.
      call FeQuestEdwMake(id,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,0)
      nEdwTitle=EdwLastMade
      call FeQuestStringEdwOpen(nEdwTitle,plotTitle)
      xpom=offset
      tpom=xpom+CrwYd+5.
      ilp=il
      ch80='Plot labels'
      ichk=0
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,
     1                      ichk,0)
        if(i.eq.1) then
          nCrwLab=CrwLastMade
          ch80='Plot max. count'
        else if(i.eq.2) then
          nCrwCounts=CrwLastMade
          ch80='Plot Bragg positions'
          il=il+2
          ichk=1
          dpom=FeTxLength(ch80)
        else if(i.eq.3) then
          nCrwBragg=CrwLastMade
        endif
      enddo
      il=ilp
      xpom=tpom+dpom+25.
      tpom=xpom+CrwYd+5.
      ch80='Plot %observed line with color:'
      xpomp=tpom+FeTxLengthUnder(ch80)+60.
      dpom=xqd-xpomp-5.
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,
     1                      1,0)
        call FeQuestRolMenuMake(id,tpom,il,xpomp,il,' ','L',dpom,EdwYd,
     1                          1)
        if(i.eq.1) then
          nCrwObsLine=CrwLastMade
          nRolMenuColorObs=RolMenuLastMade
          ch80='Plot %calculated line with color:'
        else if(i.eq.2) then
          nCrwCalcLine=CrwLastMade
          nRolMenuColorCalc=RolMenuLastMade
          ch80='Plot %difference curve with color:'
        else if(i.eq.3) then
          nCrwDifLine=CrwLastMade
          nRolMenuColorDif=RolMenuLastMade
        endif
      enddo
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      tpom=xpom
      if(maxNDimI.le.0) then
        ch80='%Reflections with color:'
      else
        if(maxNComp.le.1) then
          ch80='%Main reflections with color:'
        else
          ch80='Main reflections of subsystem #%1 with color:'
        endif
      endif
      call FeQuestRolMenuMake(id,tpom,il,xpomp,il,ch80,'L',dpom,EdwYd,1)
      nRolMenuColorMain1=RolMenuLastMade
      if(maxNComp.gt.1) then
        il=il+1
        ch80='Main reflection of subsystem #2 with color:'
        call FeQuestRolMenuMake(id,tpom,il,xpomp,il,ch80,'L',dpom,EdwYd,
     1                          1)
        nRolMenuColorMain2=RolMenuLastMade
        il=il+1
        ch80='Co%mmon reflections with color:'
        call FeQuestRolMenuMake(id,tpom,il,xpomp,il,ch80,'L',dpom,EdwYd,
     1                          1)
        nRolMenuColorMain12=RolMenuLastMade
      else
        nRolMenuColorMain2=0
        nRolMenuColorMain12=0
      endif
      if(maxNDimI.gt.0) then
        il=il+1
        ch80='Satellites with color:'
        call FeQuestRolMenuMake(id,tpom,il,xpomp,il,ch80,'L',dpom,EdwYd,
     1                          1)
        nRolMenuColorSat=RolMenuLastMade
      else
        nRolMenuColorSat=0
      endif
      if(NPhase.gt.1) then
        il=il+1
        ch80='Bragg labels as phase names'
        xpom=offset
        tpom=xpom+CrwYd+5.
        do i=1,2
          call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,0,
     1                        3)
          if(i.eq.1) then
            nCrwUsePhaseNames=CrwLastMade
            ch80='Bragg labels in form (a), (b), ...'
            xpom=tpom+FeTxLengthUnder(ch80)+10.
            tpom=xpom+CrwYd+5.
          endif
          call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.BraggLabel.eq.0)
        enddo
      else
        nCrwUsePhaseNames=0
      endif
      il=il+1
      call FeQuestLinkaMake(id,il)
      il=il+1
      ch80='%Use linear scale for intensity'
      xpom=offset
      tpom=xpom+CrwYd+5.
      do i=1,2
        call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,0,4)
        if(i.eq.1) then
          nCrwUseLinearScale=CrwLastMade
          ch80='Use square root scale for intensity'
          xpom=tpom+FeTxLengthUnder(ch80)+10.
          tpom=xpom+CrwYd+5.
        endif
      enddo
      il=il+1
      xpom=offset
      tpom=xpom+CrwYd+5.
      ch80='Shift ori%gin of the y axis'
      call FeQuestCrwMake(id,tpom,il,xpom,il,ch80,'L',CrwYd,CrwXd,1,0)
      nCrwYOriginShift=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,YOriginShift.gt.0.)
      tpom=tpom+FeTxLengthUnder(ch80)+4.
      ch80='to =>'
      call FeQuestLblMake(id,tpom,il,ch80,'L','N')
      nLblTo=LblLastMade
      xpom=tpom+FeTxLengthUnder(ch80)+3.
      dpom=50.
      tpom=xpom+dpom+5.
      ch80=' * min(I(obs)),I(calc)) - factor from the interval <0,1>'
      call FeQuestEdwMake(id,tpom,il,xpom,il,ch80,'L',dpom,EdwYd,1)
      nEdwYOriginShift=EdwLastMade
      il=il+1
      ch80='%Set defaults'
      dpom=FeTxLength(ch80)+50.
      xpom=(xqd-dpom)*.5
      call FeQuestButtonMake(id,xpom,il,dpom,ButYd,ch80)
      nButtDefaults=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call FeQuestCrwOpen(nCrwShowTitle,KresliTitle)
      call FeQuestCrwOpen(nCrwCent,CenteredTitle)
      call FeQuestCrwOpen(nCrwRight,.not.CenteredTitle)
1000  nCrw=nCrwLab
      do i=1,4
        if(i.eq.1) then
          lpom=KresliLab
        else if(i.eq.2) then
          lpom=KresliMaxCounts
        else if(i.eq.3) then
          lpom=KresliBragg
        else if(i.eq.4) then
          lpom=KresliDifLine
        endif
        call FeQuestCrwOpen(nCrw,lpom)
        nCrw=nCrw+1
      enddo
      nCrw=nCrwObsLine
      nRolMenu=nRolMenuColorObs
      lpom=KresliObsLine
      j=ColorObs
      do i=1,3
        call FeQuestCrwOpen(nCrw,lpom)
        k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenu,ColorNames,PocetBarev,k)
        if(i.eq.1) then
          lpom=KresliCalcLine
          j=ColorCalc
        else if(i.eq.2) then
          lpom=KresliDifLine
          j=ColorDif
        endif
        nCrw=nCrw+1
        nRolMenu=nRolMenu+1
      enddo
      k=max(LocateInIntArray(ColorMain1,ColorNumbers,PocetBarev),1)
      call FeQuestRolMenuOpen(nRolMenuColorMain1,ColorNames,PocetBarev,
     1                        k)
      if(nRolMenuColorMain2.gt.0) then
        k=max(LocateInIntArray(ColorMain2,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenuColorMain2,ColorNames,
     1                          PocetBarev,k)
        k=max(LocateInIntArray(ColorMain12,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenuColorMain12,ColorNames,
     1                          PocetBarev,k)
      endif
      if(nRolMenuColorSat.gt.0) then
        k=max(LocateInIntArray(ColorSat,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenuColorSat,ColorNames,PocetBarev,
     1                          k)
      endif
      if(kType.eq.0) then
        call FeQuestCrwDisable(nCrwDifLine)
        call FeQuestRolMenuDisable(nRolMenuColorDif)
        call FeQuestCrwDisable(nCrwCalcLine)
        call FeQuestRolMenuDisable(nRolMenuColorCalc)
      endif
      if(NBragg.le.0) call FeQuestCrwDisable(nCrwBragg)
      call FeReleaseOutPut
      if(ExistFile(fln(:ifln)//'.prf')) then
        call FeQuestCrwOpen(nCrwm90,.not.ZPrf)
        call FeQuestCrwOpen(nCrwPrf,ZPrf)
      endif
      call FeReleaseOutPut
      nCrw=nCrwUseLinearScale
      do i=1,2
        call FeQuestCrwOpen(nCrw,i.eq.2.eqv.SqrtScale)
        nCrw=nCrw+1
      enddo
1500  nCrw=nCrwObsLine
      nRolMenu=nRolMenuColorObs
      do i=1,3
        if(CrwLogicQuest(nCrw)) then
          if(i.eq.1) then
            j=ColorObs
          else if(i.eq.2) then
            j=ColorCalc
          else if(i.eq.3) then
            j=ColorDif
          endif
          k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
          call FeQuestRolMenuOpen(nRolMenu,ColorNames,PocetBarev,k)
        else
          j=ColorNumbers(RolMenuSelectedQuest(nRolMenu))
          if(i.eq.1) then
            ColorObs=j
          else if(i.eq.2) then
            ColorCalc=j
          else
            ColorDif=j
          endif
          call FeQuestRolMenuDisable(nRolMenu)
        endif
        nCrw=nCrw+1
        nRolMenu=nRolMenu+1
      enddo
      if(KresliTitle) then
        if(EdwStateQuest(nEdwTitle).ne.EdwOpened) then
          call FeQuestStringEdwOpen(nEdwTitle,plotTitle)
          call FeQuestCrwOpen(nCrwCent,CenteredTitle)
          call FeQuestCrwOpen(nCrwRight,.not.CenteredTitle)
        endif
      else
        if(EdwStateQuest(nEdwTitle).ne.EdwDisabled) then
          CenteredTitle=CrwLogicQuest(nCrwCent)
          call FeQuestEdwDisable(nEdwTitle)
          call FeQuestCrwDisable(nCrwCent)
          call FeQuestCrwDisable(nCrwRight)
        endif
      endif
      if(KresliBragg) then
        j=ColorMain1
        k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
        call FeQuestRolMenuOpen(nRolMenuColorMain1,ColorNames,
     1                          PocetBarev,k)
        if(nRolMenuColorMain2.gt.0) then
          j=ColorMain2
          k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
          call FeQuestRolMenuOpen(nRolMenuColorMain2,ColorNames,
     1                            PocetBarev,k)
          j=ColorMain12
          k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
          call FeQuestRolMenuOpen(nRolMenuColorMain12,ColorNames,
     1                            PocetBarev,k)
        endif
        if(nRolMenuColorSat.gt.0) then
          j=ColorSat
          k=max(LocateInIntArray(j,ColorNumbers,PocetBarev),1)
          call FeQuestRolMenuOpen(nRolMenuColorSat,ColorNames,
     1                            PocetBarev,k)
        endif
        if(NPhase.gt.1) then
          nCrw=nCrwUsePhaseNames
          do i=1,2
            call FeQuestCrwOpen(nCrw,i.eq.1.eqv.BraggLabel.eq.0)
            nCrw=nCrw+1
          enddo
        endif
      else
        j=ColorNumbers(RolMenuSelectedQuest(nRolMenuColorMain1))
        call FeQuestRolMenuDisable(nRolMenuColorMain1)
        if(nRolMenuColorMain2.gt.0) then
          j=ColorNumbers(RolMenuSelectedQuest(nRolMenuColorMain1))
          call FeQuestRolMenuDisable(nRolMenuColorMain1)
          j=ColorNumbers(RolMenuSelectedQuest(nRolMenuColorMain12))
          call FeQuestRolMenuDisable(nRolMenuColorMain12)
        endif
        if(nRolMenuColorSat.gt.0) then
          j=ColorNumbers(RolMenuSelectedQuest(nRolMenuColorSat))
          call FeQuestRolMenuDisable(nRolMenuColorSat)
        endif
        if(NPhase.gt.1) then
          nCrw=nCrwUsePhaseNames
          do i=1,2
            call FeQuestCrwDisable(nCrw)
            nCrw=nCrw+1
          enddo
        endif
      endif
      if(CrwLogicQuest(nCrwYOriginShift)) then
        call FeQuestLblOn(nLblTo)
        if(YOriginShift.le.0.) YOriginShift=1.
        call FeQuestRealEdwOpen(nEdwYOriginShift,YOriginShift,.false.,
     1                          .false.)
      else
        call FeQuestLblOff(nLblTo)
        if(EdwStateQuest(nEdwYOriginShift).eq.EdwOpened) then
          call FeQuestRealFromEdw(nEdwYOriginShift,YOriginShift)
          call FeQuestEdwClose(nEdwYOriginShift)
        endif
      endif
2000  call FeQuestEvent(id,ich)
      nechT=.false.
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwm90.or.CheckNumber.eq.nCrwPrf)) then
        ZPrf=CrwLogicQuest(nCrwPrf)
        if(CrwLogicQuest(nCrwm90))then
          ktFileNew=1
        else
          ktFileNew=0
        endif
        if(ktFileNew.eq.ktFile) go to 2000
        ktFile=ktFileNew
        menilFile=.true.
        call PwdPrfNacti(ktFile)
        CheckType=EventButton
        CheckNumber=nButtDefaults
      endif
2100  if((CheckType.eq.EventButton.and.CheckNumber.eq.nButtDefaults)
     1   .or.JenomNastav) then
        KresliObsLine=kType.eq.0.or.kType.eq.2
        KresliCalcLine=kType.ne.0
        KresliBragg=NBragg.gt.0
        KresliDifLine=kType.ne.0
        if(.not.UzTadyByl.or..not.JenomNastav) then
          ColorObs=White
          ColorCalc=White
          ColorDif=White
          ColorCalc=Yellow
          ColorMain1=White
          ColorMain2=Yellow
          ColorMain12=Red
          ColorSat=Green
          KresliLab=.true.
          KresliMaxCounts=.true.
          KresliTitle=.false.
          CenteredTitle=.false.
        endif
        if(.not.nechT) plotTitle=StructureName
        shrinkX=1.1
        shrinkY=1.1
        expandX=1.1
        expandY=1.1
        stepIni=1.
        distLabel=6.
        SqrtScale=.false.
        YOriginShift=0.
        if(JenomNastav) then
          go to 9900
        else
          go to 1000
        endif
      endif
      if(CheckType.eq.EventCrw.and.
     1   (CheckNumber.eq.nCrwObsLine.or.CheckNumber.eq.nCrwCalcLine.or.
     2    CheckNumber.eq.nCrwDifLine)) then
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwShowTitle)
     1  then
        KresliTitle=CrwLogicQuest(nCrwShowTitle)
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwBragg) then
        KresliBragg=CrwLogicQuest(nCrwBragg)
        go to 1500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwYOriginShift)
     1  then
        go to 1500
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwYOriginShift)
     1  then
        call FeQuestRealFromEdw(nEdwYOriginShift,YOriginShift)
        YOriginShift=min(YOriginShift,1.)
        YOriginShift=max(YOriginShift,0.)
        call FeQuestRealEdwOpen(nEdwYOriginShift,YOriginShift,.false.,
     1                          .false.)
        go to 1500
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorObs) then
        ColorObs=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorDif) then
        ColorDif=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorCalc) then
        ColorCalc=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorMain1) then
        ColorMain1=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorMain2) then
        ColorMain2=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorMain12) then
        ColorMain12=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuColorSat) then
        ColorSat=ColorNumbers(RolMenuSelectedQuest(CheckNumber))
        go to 2000
      else if(CheckType.eq.EventGlobal) then
        go to 2000
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      if(ich.eq.0) then
        if(.not.CrwLogicQuest(nCrwYOriginShift)) YOriginShift=0.
        SqrtScale=.not.CrwLogicQuest(nCrwUseLinearScale)
        KresliTitle=CrwLogicQuest(nCrwShowTitle)
        if(KresliTitle) then
          plotTitle=EdwStringQuest(nEdwTitle)
          CenteredTitle=CrwLogicQuest(nCrwCent)
        endif
        nCrw=nCrwLab
        do i=1,6
          lpom=CrwLogicQuest(nCrw)
          if(i.eq.1) then
            KresliLab=lpom
          else if(i.eq.2) then
            KresliMaxCounts=lpom
          else if(i.eq.3) then
            KresliBragg=lpom
          else if(i.eq.4) then
            KresliObsLine=lpom
          else if(i.eq.5) then
            KresliCalcLine=lpom
          else if(i.eq.6) then
            KresliDifLine=lpom
          endif
          nCrw=nCrw+1
        enddo
        if(ExistFile(fln(:ifln)//'.prf'))
     1    ZPrf=CrwLogicQuest(nCrwPrf)
        if(nCrwUsePhaseNames.gt.0) then
          if(CrwLogicQuest(nCrwUsePhaseNames)) then
            BraggLabel=0
          else
            BraggLabel=1
          endif
        endif
      endif
9000  call FeQuestRemove(id)
9900  ColorSkip=Gray
      if(ColorObs.eq.ColorSkip.or.ColorCalc.eq.ColorSkip) then
        ColorSkip=Yellow
        if(ColorObs.eq.ColorSkip.or.ColorCalc.eq.ColorSkip)
     1    ColorSkip=Blue
      endif
      UzTadyByl=.true.
9999  return
      end
      subroutine PwdPrfPrusX(x1,y1,x2,y2,PrusX,PrusY,x)
      include 'fepc.cmn'
      s=(y2-y1)/(x2-x1)
      PrusX=x
      PrusY=y1+s*(x-x1)
      return
      end
      subroutine PwdPrfPrusY(x1,y1,x2,y2,pomx,pomy,y)
      include 'fepc.cmn'
      s=(x2-x1)/(y2-y1)
      pomx=x2
      pomy=y2
      x2=x1+s*(y-y1)
      y2=y
      return
      end
      function PwdPrfX2Th(x)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      PwdPrfX2Th=(x-XminPlC)/skx+shx
      end
      function PwdPrfTh2X(th)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      PwdPrfTh2X=XminPlC+(th-shx)*skx
      end
      function PwdPrfInt2Y(int)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      real int
      if(SqrtScale) then
        pom=sqrt(int)-shy
      else
        pom=int-shy
      endif
      PwdPrfInt2Y=YminPlC+pom*sky
      return
      end
      function PwdPrfY2Int(y)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      if(SqrtScale) then
        PwdPrfY2Int=((y-YminPlC)/sky+shy)**2
      else
        PwdPrfY2Int=(y-YminPlC)/sky+shy
      endif
      return
      end
      subroutine PwdSetManualBackg(ich)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 Veta,t80
      integer, allocatable :: FP(:)
      logical :: Skip = (.true.),FeYesNo,CrwLogicQuest
      real :: SigLev = (1.0)
      real, allocatable :: XP(:),YP(:),YS(:)
      AskIfQuit=.false.
      NManBackgOld=NManBackg(KDatBlock)
      if(NManBackg(KDatBlock).gt.0) then
        Veta='Manual background already exist, do you want to rewrite'//
     1       ' it?'
        if(.not.FeYesNo(-1.,-1.,Veta,0)) then
          ich=0
          go to 9900
        endif
      else
        NManBackg(KDatBlock)=30
      endif
1100  id=NextQuestId()
      xqd=350.
      il=3
      Veta='Options for generating background profile'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      dpom=60.
      tpom=5.
      Veta='%Number of manual background points:'
      xpom=tpom+FeTxLengthUnder(Veta)+50.
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwNumberPoints=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,NManBackg(KDatBlock),.false.)
      il=il+1
      Veta='%Significance intensity level for peaks regions:'
      call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwSigLevel=EdwLastMade
      spom=xpom+dpom+5.
      call FeQuestLblMake(id,spom,il,'*sig(I)','L','N')
      call FeQuestRealEdwOpen(EdwLastMade,SigLev,.false.,.false.)
      il=il+1
      Veta='%Avoid from considerations skipped regions:'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwSkip=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,Skip)
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        call FeQuestIntFromEdw(nEdwNumberPoints,NManBackg(KDatBlock))
        call FeQuestRealFromEdw(nEdwSigLevel,SigLev)
        Skip=CrwLogicQuest(nCrwSkip)
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9900
      n=0
      do i=1,NPnts
        if(YfPwd(i).ne.0) n=n+1
      enddo
      if(n.lt.5*NManBackg(KDatBlock)) then
        write(Cislo,FormI15) n
        call Zhusti(Cislo)
        write(Veta,FormI15) NManBackg(KDatBlock)
        call Zhusti(Veta)
        Veta='the number of '//Cislo(:idel(Cislo))//' profile points '//
     1       ' too small to determine'
        write(Cislo,FormI15) NManBackg(KDatBlock)
        call Zhusti(Cislo)
        t80=Cislo(:idel(Cislo))//
     1      ' points  manual background, try again.'
        call FeChybne(-1.,-1.,Veta,t80,SeriousError)
        go to 1100
      endif
      if(.not.allocated(XManBackg)) then
        m=NManBackg(KDatBlock)
        allocate(XManBackg(m,NDatBlock),YManBackg(m,NDatBlock))
      else
        call ReallocManBackg(NManBackg(KDatBlock))
      endif
      fm=float(n)/float(NManBackg(KDatBlock)-1)
      do i=1,NPnts
        if(YfPwd(i).eq.1.or..not.Skip) then
          XManBackg(1,KDatBlock)=XPwd(i)
          exit
        endif
      enddo
      k=0
      j=1
      do i=1,NPnts
        if(YfPwd(i).eq.0.and.Skip) cycle
        k=k+1
        if(k.ge.nint(fm*float(j))) then
          j=j+1
          if(j.gt.NManBackg(KDatBlock)-1) cycle
          XManBackg(j,KDatBlock)=XPwd(i)
        endif
      enddo
      do i=NPnts,1,-1
        if(YfPwd(i).eq.1.or..not.Skip) then
          XManBackg(NManBackg(KDatBlock),KDatBlock)=XPwd(i)
          exit
        endif
      enddo
      m=2*nint(fm)+5
      if(allocated(XP)) deallocate(XP,YP,YS,FP)
      allocate(XP(m),YP(m),YS(m),FP(m))
      do i=1,NManBackg(KDatBlock)
        XPMin=XManBackg(max(i-1,1),KDatBlock)
        XPMax=XManBackg(min(i+1,NManBackg(KDatBlock)),KDatBlock)
        m=0
        do j=1,NPnts
          if(YfPwd(j).eq.0.and.Skip) cycle
          if(XPwd(j).gt.XPMax) exit
          if(XPwd(j).ge.XPMin) then
            m=m+1
            XP(m)=XPwd(j)-XManBackg(i,KDatBlock)
            YP(m)=YoPwd(j)
            YS(m)=YsPwd(j)
            if(YS(m).le.0.) YS(m)=sqrt(abs(YP(m)))
            FP(m)=1
          endif
        enddo
        Kolikrat=0
2000    Kolikrat=Kolikrat+1
        n=0
        SumX=0.
        SumY=0.
        SumXY=0.
        SumX2=0.
        do j=1,m
          if(FP(j).eq.0) cycle
          n=n+1
          SumX=SumX+XP(j)
          SumY=SumY+YP(j)
          SumXY=SumXY+XP(j)*YP(j)
          SumX2=SumX2+XP(j)**2
        enddo
        det=0.
        fn=n
        ave=SumX/fn
        nn=0
        do j=1,m
          if(FP(j).eq.0) cycle
          nn=nn+1
          det=det+(XP(j)-ave)**2
        enddo
        det=det*fn
        det=fn*SumX2-SumX**2
        AA=(fn*SumXY-SumX*SumY)/det
        BB=(SumX2*SumY-SumX*SumXY)/det
        SumY=0.
        NOut=0
        NAll=0
        do j=1,m
          if(FP(j).eq.0) cycle
          NAll=NAll+1
          Y=AA*XP(j)+BB
          if(YP(j)-Y.gt.SigLev*YS(j)) then
            FP(j)=0
            NOut=NOut+1
          endif
        enddo
        if(NOut.gt.0.and.NAll-NOut.gt.3) go to 2000
        YManBackg(i,KDatBlock)=BB
      enddo
      Sc(2,KDatBlock)=1.
      go to 9999
9900  NManBackg(KDatBlock)=NManBackgOld
9999  AskIfQuit=.true.
      if(allocated(XP)) deallocate(XP,YP,YS,FP)
      return
      end
      subroutine PwdBasic(ich)
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension ip(12),xp(12),nn(7)
      character*256 Radka
      character*80  Veta,t80
      integer :: NLast = 0, nprcf(MxDatBlock)
      integer, allocatable :: Nex(:),Res(:),Npr(:),Nscp(:),Nba(:)
      logical EqIgCase,TOFParNacteny,TOFParNebude
      real LamIn
      equivalence (ip,xp)
      ich=0
      ln=0
      NTimeMaps=0
      NPointsTimeMapsMax=0
      TOFParNebude=.false.
      ISISGSS=.false.
      LamIn=-333.
      NMax=0
      BankPtyp=0
      BankPrcf=0.
      if(DifCode(KRefBlock).ne.IdDataGSAS.and.
     1   DifCode(KRefBlock).ne.IdTOFGSAS.and.
     2   DifCode(KRefBlock).ne.IdDataUXD.and.
     3   DifCode(KRefBlock).ne.IdEDInel.and.
     4   DifCode(KRefBlock).ne.IdDataPSI.and.
     5   DifCode(KRefBlock).ne.IdDataXRDML.and.
     6   DifCode(KRefBlock).ne.IdDataPwdRigaku.and.
     7   DifCode(KRefBlock).ne.IdDataPwdStoe.and.
     8   DifCode(KRefBlock).ne.IdDataG41.and.
     9   DifCode(KRefBlock).ne.IdDataRiet7) then
1010    read(71,FormA,err=9000,end=9000) Radka
        k=0
        kk=0
1020    call Kus(Radka,k,t80)
        if(t80(1:1).eq.'!') then
          if(kk.eq.0) then
            go to 1010
          else
            backspace 71
            go to 1030
          endif
        endif
        call Posun(t80,1)
        read(t80,*,err=1010) pom
        kk=k
        if(k.lt.len(t80)) go to 1020
        backspace 71
      endif
1030  if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1   DifCode(KRefBlock).eq.IdTOFGSAS.or.
     2   DifCode(KRefBlock).eq.IdData11BM) then
        TOFParNacteny=.false.
1100    read(71,FormA,err=9000,end=1200) Radka
        if(Radka.eq.' ') go to 1100
        if(Radka(1:1).eq.'#') then
          if(DifCode(KRefBlock).eq.IdData11BM) then
            k=1
            call kus(Radka,k,Veta)
            if(EqIgCase(Veta,'Wavelength:')) then
              call StToReal(Radka,k,xp,1,.false.,ich)
              if(ich.eq.0) LamIn=xp(1)
            endif
          endif
          go to 1100
        endif
        call mala(Radka)
        i=index(Radka,'instrument parameter file:')
        if(i.gt.0) then
          if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1       DifCode(KRefBlock).eq.IdData11BM) go to 1100
          i=index(Radka,':')
          Radka=Radka(i+1:)
          k=0
          call kus(Radka,k,InsParFileRefBlock(KRefBlock))
          i=idel(InsParFileRefBlock(KRefBlock))
1120      if(InsParFileRefBlock(KRefBlock)(i:i).ne.'/'.and.
     1       InsParFileRefBlock(KRefBlock)(i:i).ne.ObrLom) then
            i=i-1
            if(i.ne.0) go to 1120
          endif
          InsParFileRefBlock(KRefBlock)=
     1      InsParFileRefBlock(KRefBlock)(i+1:)
        else if(index(Radka,'time_map').eq.1) then
          if(DifCode(KRefBlock).eq.IdDataGSAS) go to 1100
          k=0
          call Kus(Radka,k,Veta)
          call StToInt(Radka,k,ip,3,.false.,ich)
          if(ich.ne.0) go to 9000
          NTimeMaps=max(NTimeMaps,ip(1))
          NPointsTimeMapsMax=max(NPointsTimeMapsMax,ip(2))
        endif
        go to 1100
1200    if(DifCode(KRefBlock).eq.IdTOFGSAS) then
          call PwdBasicDefInsFile(1,ich)
          if(ich.ne.0) go to 1300
          ln=NextLogicNumber()
          call OpenFile(ln,InsParFileRefBlock(KRefBlock),'formatted',
     1                  'old')
          if(ErrFlag.ne.0) go to 1200
1210      read(ln,FormA,err=9200,end=1220) Radka
          call mala(Radka)
          i=index(Radka,' icons')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            k=i+5
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9200
            BankDifc(n)=xp(1)
            BankDifa(n)=xp(2)
            BankZero(n)=xp(3)
            BankJason(n)=0
            BankCE(n)=0.
            BankZeroE(n)=0.
            BankTCross(n)=0.
            BankWCross(n)=0.
            call SetRealArrayTo(BankSigma(1,n),3,-3333.)
            call SetRealArrayTo(BankGamma(1,n),3,-3333.)
            call SetRealArrayTo(BankAlfbe(1,n),4,-3333.)
            go to 1210
          endif
          i=index(Radka,'bnkpar')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            k=i+6
            call StToReal(Radka,k,xp,2,.false.,ich)
            if(ich.ne.0) go to 9200
            BankTTh(n)=xp(2)
            go to 1210
          endif
          i=index(Radka,'i ityp')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            k=i+6
            call StToInt(Radka,k,ip,1,.false.,ich)
            BankITyp(n)=ip(1)
            NCoff=1
          endif
          i=index(Radka,'icoff')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            k=i+6
            call StToReal(Radka,k,xp,4,.false.,ich)
            if(ich.ne.0) go to 9200
            call CopyVek(xp,BankICoff(NCoff,n),4)
            NCoff=NCoff+4
            go to 1210
          endif
          do j=1,10
            write(Cislo,'(''prcf'',i2)') j
            i=index(Radka,Cislo(:idel(Cislo)))
            if(i.eq.7) then
              read(Radka(i-2:),102) n
              NMax=max(NMax,n)
              k=i+6
              m=min(4,nprcf(n)-4*(j-1))
              call StToReal(Radka,k,BankPrcf(4*(j-1)+1,n),m,.false.,ich)
              go to 1210
            endif
          enddo
          i=index(Radka,'prcf')
          if(i.eq.7) then
            read(Radka(i-2:),102) n
            NMax=max(NMax,n)
            read(Radka(i+6:),'(2i5)') BankPtyp(n),nprcf(n)
            read(Radka(i+16:),*) pom
            go to 1210
          endif
          go to 1210
1220      call CloseIfOpened(ln)
          do i=1,NMax
            if(BankPtyp(i).eq.3) then
              BankAlfBe(1,i)=0.
              BankAlfBe(2,i)=BankPrcf(2,i)
              BankAlfBe(3,i)=BankPrcf(1,i)
              BankAlfBe(4,i)=BankPrcf(3,i)
              BankSigma(1,i)=BankPrcf(4,i)
              BankSigma(2,i)=BankPrcf(5,i)
              BankSigma(3,i)=BankPrcf(6,i)
              BankGamma(1,i)=BankPrcf(7,i)
              BankGamma(2,i)=BankPrcf(8,i)
              BankGamma(3,i)=BankPrcf(9,i)
            endif
          enddo
        endif
1300    if(allocated(TMapTOF)) deallocate(TMapTOF,ClckWdtTOF,
     1                                    NPointsTimeMaps)
        if(NTimeMaps.gt.0) then
          NPointsTimeMapsMax=(NPointsTimeMapsMax-1)/3
          allocate(TMapTOF(3,NPointsTimeMapsMax+1,NTimeMaps),
     1             ClckWdtTOF(NTimeMaps),NPointsTimeMaps(NTimeMaps))
          rewind 71
1320      read(71,FormA,err=9000,end=1400) Radka
          if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1320
          call mala(Radka)
          if(index(Radka,'time_map').eq.1) then
            k=0
            call Kus(Radka,k,Veta)
            call StToInt(Radka,k,ip,3,.false.,ich)
            if(ich.ne.0) go to 9000
            l=ip(1)
            n=(ip(2)-1)/3+1
            NPointsTimeMaps(l)=n
            m=ip(3)
            call Kus(Radka,k,Veta)
            call StToReal(Radka,k,xp,1,.false.,ich)
            if(ich.ne.0) go to 9000
            ClckWdtTOF(l)=nint(xp(1))
            read(71,'(10i8)',err=9000,end=9000)
     1        ((TMapTOF(i,j,l),i=1,3),j=1,n-1),TMapTOF(2,n,l)
          endif
          go to 1320
        endif
1400    rewind 71
        NBanks=0
1420    read(71,FormA,err=9000,end=1430) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1420
        if(index(Radka,'bank').eq.1) NBanks=NBanks+1
        go to 1420
1430    rewind 71
1440    read(71,FormA,err=9000,end=9000) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.Radka(1:1).eq.'#'.or.
     1     index(Radka,'bank').ne.1) go to 1440
        backspace(71)
        if(DifCode(KRefBlock).eq.IdTOFGSAS) then
          RadiationRefBlock(KRefBlock)=NeutronRadiation
        else if(DifCode(KRefBlock).eq.IdData11BM) then
          RadiationRefBlock(KRefBlock)=XRayRadiation
          PolarizationRefBlock(KRefBlock)=PolarizedLinear
        endif
        if(DifCode(KRefBlock).eq.IdData11BM.and.LamIn.gt.0.) then
          LamAveRefBlock(KRefBlock)=LamIn
          LamA1RefBlock(KRefBlock)=LamIn
          LamA2RefBlock(KRefBlock)=LamIn
        endif
      else if(DifCode(KRefBlock).eq.IdTOFISIST.or.
     1        DifCode(KRefBlock).eq.IdTOFFree.or.
     2        DifCode(KRefBlock).eq.IdTOFISISFullProf) then
1500    if(KRefBlock.le.1) then
          InsParFileRefBlock(KRefBlock)=' '
        else
          InsParFileRefBlock(KRefBlock)=InsParFileRefBlock(KRefBlock-1)
        endif
        call PwdBasicDefInsFile(2,ich)
        if(ich.eq.0) then
          ln=NextLogicNumber()
          call OpenFile(ln,InsParFileRefBlock(KRefBlock),'formatted',
     1                  'old')
          if(ErrFlag.ne.0) go to 1500
          call GetExtentionOfFileName(InsParFileRefBlock(KRefBlock),
     1                                Veta)
          if(EqIgCase(Veta,'pcr')) then
            do i=1,2
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            k=0
            call Kus(Radka,k,Cislo)
            NPatt=1
            if(EqIgCase(Cislo,'npatt')) then
              call StToInt(Radka,k,ip,1,.false.,ich)
              if(ich.ne.0) go to 9999
              NPatt=ip(1)
              do i=1,2
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
            endif
            allocate(Npr(NPatt),Res(NPatt),Nex(NPatt),Nscp(NPatt),
     1               Nba(NPatt))
            k=0
            call StToInt(Radka,k,ip,6,.false.,ich)
            if(ich.ne.0) go to 9000
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              k=0
              call StToInt(Radka,k,ip,12,.false.,ich)
              if(ich.ne.0) go to 9000
              if(ip(1).ne.-1) then
                Veta='data are not TOF type'
                go to 1595
              endif
              if(ip(11).ne.1) then
                Veta='data are in TOF microseconds'
                go to 1595
              endif
              Npr(i)=ip(2)
              if(Npr(i).eq.9) then
                BankJason(i)=0
              else if(Npr(i).eq.10) then
                BankJason(i)=1
              else
                Veta='data are not TOF type'
                go to 1595
              endif
              Nba(i)=ip(3)
              Nex(i)=ip(4)
              Nscp(i)=ip(5)
              Res(i)=ip(9)
            enddo
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            do i=1,NPatt
              if(Res(i).gt.0) then
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              endif
            enddo
            call GetNextPCR(ln,Radka,iend,ierr)
            if(iend.ne.0) go to 1590
            if(ierr.ne.0) go to 9000
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            call GetNextPCR(ln,Radka,iend,ierr)
            if(iend.ne.0) go to 1590
            if(ierr.ne.0) go to 9000
            do i=1,NPatt
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            do i=1,NPatt
              do j=1,Nex(i)
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
            enddo
            do i=1,NPatt
              do j=1,Nscp(i)
                call FeWinMessage('For Nsc<>0 not implemented',' ')
              enddo
            enddo
            call GetNextPCR(ln,Radka,iend,ierr)
            if(iend.ne.0) go to 1590
            if(ierr.ne.0) go to 9000
            do i=1,NPatt
              call SetRealArrayTo(BankSigma(1,i),3,-3333.)
              call SetRealArrayTo(BankGamma(1,i),3,-3333.)
              call SetRealArrayTo(BankAlfbe(1,i),4,-3333.)
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
              k=0
              call StToReal(Radka,k,xp,7,.false.,ich)
              if(ich.ne.0) go to 9999
              BankZero(i)=xp(1)
              BankDifc(i)=xp(3)
              BankDifa(i)=xp(5)
              BankTTh(i)=xp(7)
              if(BankJason(i).gt.0) then
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
                k=0
                call StToReal(Radka,k,xp,5,.false.,ich)
                if(ich.ne.0) go to 9999
                BankZeroE(i)=BankZero(i)
                BankCE(i)=BankDifc(i)
                BankZero(i)=xp(1)
                BankDifc(i)=xp(2)
                BankDifa(i)=xp(3)
                BankTCross(i)=xp(4)
                BankWCross(i)=xp(5)
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
                if(Nba(i).eq.-3) then
                  n=4
                else if(Nba(i).eq.-2) then
                  n=1
                else if(Nba(i).eq.-1) then
                  n=6
                else if(Nba(i).eq.0) then
                  n=2
                else if(Nba(i).eq.1) then
                  n=2
                else
                  n=0
                  cycle
                endif
                do j=1,n
                  call GetNextPCR(ln,Radka,iend,ierr)
                  if(iend.ne.0) go to 1590
                  if(ierr.ne.0) go to 9000
                enddo
              endif
            enddo
1520        read(ln,FormA,end=1590,err=9000) Radka
            if(LocateSubstring(Radka,'Profile Parameters',.false.,
     1                         .true.).le.0) go to 1520
            do i=1,NPatt
              do j=1,3
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
              k=0
              call StToReal(Radka,k,BankSigma(1,i),3,.false.,ich)
              if(ich.ne.0) go to 9999
              do j=1,2
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
              k=0
              call StToReal(Radka,k,BankGamma(1,i),3,.false.,ich)
              if(ich.ne.0) go to 9999
              do j=1,2
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
              do j=1,2
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              enddo
              k=0
              call StToReal(Radka,k,xp,6,.false.,ich)
              do j=1,4
                BankAlfbe(j,i)=xp(j+2)
              enddo
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
              if(BankJason(i).gt.0) then
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
                k=0
                call StToReal(Radka,k,BankAlfbt(1,i),4,.false.,ich)
                call GetNextPCR(ln,Radka,iend,ierr)
                if(iend.ne.0) go to 1590
                if(ierr.ne.0) go to 9000
              endif
              call GetNextPCR(ln,Radka,iend,ierr)
              if(iend.ne.0) go to 1590
              if(ierr.ne.0) go to 9000
            enddo
            call CloseIfOpened(ln)
            deallocate(Npr,Res,Nex,Nscp,Nba)
            n=NPatt
            go to 1640
1590        Veta='end of file reached before expected'
1595        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
            deallocate(Npr,Res,Nex,Nscp,Nba)
            go to 9999
           else
            call SetRealArrayTo(BankSigma(1,1),3,-3333.)
            call SetRealArrayTo(BankGamma(1,1),3,-3333.)
            call SetRealArrayTo(BankAlfbe(1,1),4,-3333.)
          endif
          nn=0
1610      read(ln,FormA,end=1620,err=9000) Radka
          call Mala(Radka)
          k=0
          call Kus(Radka,k,Cislo)
          if(EqIgCase(Cislo,'d2tof')) then
            nn(1)=nn(1)+1
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0.and.ich.le.2) go to 9200
            do i=1,3
              if(i.le.ich-2.or.ich.eq.0) then
                pom=xp(i)
              else
                pom=0.
              endif
              if(i.eq.1) then
                BankDifc(nn(1))=pom
              else if(i.eq.2) then
                BankDifa(nn(1))=pom
              else
                BankZero(nn(1))=pom
              endif
            enddo
          else if(EqIgCase(Cislo,'zd2tof')) then
            nn(1)=nn(1)+1
            call StToReal(Radka,k,xp,2,.false.,ich)
            if(ich.ne.0) go to 9200
            BankZero(nn(1))=xp(1)
            BankDifc(nn(1))=xp(2)
          else if(EqIgCase(Cislo,'d2tot').or.
     1            EqIgCase(Cislo,'zd2tot')) then
            nn(2)=nn(2)+1
            if(EqIgCase(Cislo,'d2tot')) then
              n=4
            else
              n=5
            endif
            call StToReal(Radka,k,xp,n,.false.,ich)
            BankJason(nn(2))=1
            if(n.eq.5) then
              BankZeroE(nn(2))=xp(1)
            else
              BankZeroE(nn(2))=0.
            endif
            i=n-3
            BankCE(nn(2))=xp(i)
            BankDifa(nn(2))=xp(i+1)
            BankTCross(nn(2))=xp(i+2)
            BankWCross(nn(2))=xp(i+3)
          else if(EqIgCase(Cislo,'zd2tot')) then
            nn(2)=nn(2)+1
            call StToReal(Radka,k,xp,6,.false.,ich)
            BankJason(nn(2))=1
            BankJason(nn(2))=1
            BankCE(nn(2))=xp(1)
            BankDifa(nn(2))=xp(2)
            BankTCross(nn(2))=xp(3)
            BankWCross(nn(2))=xp(4)
            BankZeroE(nn(2))=0.
          else if(EqIgCase(Cislo,'twoth')) then
            nn(3)=nn(3)+1
            call StToReal(Radka,k,xp,1,.false.,ich)
            if(ich.ne.0) go to 9200
            BankTTh(nn(3))=xp(1)
          else if(EqIgCase(Cislo,'sigma')) then
            nn(4)=nn(4)+1
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9200
            pom=xp(3)
            xp(3)=xp(1)
            xp(1)=pom
            call CopyVek(xp,BankSigma(1,nn(4)),3)
          else if(EqIgCase(Cislo,'gamma')) then
            nn(5)=nn(5)+1
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) go to 9200
            pom=xp(3)
            xp(3)=xp(1)
            xp(1)=pom
            call CopyVek(xp,BankGamma(1,nn(5)),3)
          else if(EqIgCase(Cislo,'alfbe')) then
            nn(6)=nn(6)+1
            call StToReal(Radka,k,xp,4,.false.,ich)
            if(ich.ne.0) go to 9200
            call CopyVek(xp,BankAlfbe(1,nn(6)),4)
          else if(EqIgCase(Cislo,'alfbt')) then
            nn(7)=nn(7)+1
            call StToReal(Radka,k,xp,4,.false.,ich)
            if(ich.ne.0) go to 9200
            call CopyVek(xp,BankAlfbt(1,nn(7)),4)
          endif
          go to 1610
1620      n=nn(1)
          if(DifCode(KRefBlock).eq.IdTOFISIST.and.nn(1).le.0) then
            rewind ln
1630        read(ln,FormA,err=9200,end=1640) Radka
            call mala(Radka)
            i=index(Radka,' icons')
            if(i.eq.7) then
              read(Radka(i-2:),102) n
              k=i+5
              call StToReal(Radka,k,xp,3,.false.,ich)
              if(ich.ne.0) go to 1640
              BankDifc(n)=xp(1)
              BankDifa(n)=xp(2)
              BankZero(n)=xp(3)
              BankJason(n)=0
              BankCE(n)=0.
              BankZeroE(n)=0.
              BankTCross(n)=0.
              BankWCross(n)=0.
              call SetRealArrayTo(BankSigma(1,n),3,-3333.)
              call SetRealArrayTo(BankGamma(1,n),3,-3333.)
              call SetRealArrayTo(BankAlfbe(1,n),4,-3333.)
              go to 1630
            endif
            i=index(Radka,'bnkpar')
            if(i.eq.7) then
              read(Radka(i-2:),102) n
              k=i+6
              call StToReal(Radka,k,xp,2,.false.,ich)
              if(ich.ne.0) go to 1640
              BankTTh(n)=xp(2)
              go to 1630
            endif
            go to 1630
          else
            call CloseIfOpened(ln)
            if(n.gt.0) then
              do i=1,n
                if(BankJason(i).gt.0) then
                  pom=BankCE(i)
                  BankCE(i)=BankDifc(i)
                  BankDifc(i)=pom
                  pom=BankZeroE(i)
                  BankZeroE(i)=BankZero(i)
                  BankZero(i)=pom
                endif
              enddo
            endif
          endif
        else
          n=1
          ich=0
          BankJason(n)=0
          BankDifc(n)=0.
          BankDifa(n)=0.
          BankZero(n)=0.
          BankCE(n)=0.
          BankZeroE(n)=0.
          BankTTh(n)=0.
          BankTCross(n)=0.
          BankWCross(n)=0.
          call SetRealArrayTo(BankSigma(1,n),3,-3333.)
          call SetRealArrayTo(BankGamma(1,n),3,-3333.)
          call SetRealArrayTo(BankAlfbe(1,n),4,-3333.)
        endif
1640    if(DifCode(KRefBlock).eq.IdTOFISIST) then
          NBanks=0
          rewind 71
1650      read(71,FormA,err=9000,end=1660) Radka
          call mala(Radka)
          if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1650
          if(index(Radka,'bank').eq.1) NBanks=NBanks+1
          go to 1650
1660      if(NBanks.gt.0) then
            rewind 71
1670        read(71,FormA,err=9000,end=9000) Radka
            call mala(Radka)
            if(Radka.eq.' '.or.Radka(1:1).eq.'#'.or.
     1         index(Radka,'bank').ne.1) go to 1670
            backspace(71)
            ISISGSS=.true.
          else
            rewind 71
1680        read(71,FormA,err=9000,end=1690) Radka
            call mala(Radka)
            if(Radka.eq.' ') go to 1680
            k=0
            call Kus(Radka,k,Cislo)
            if(Cislo.eq.'#s') NBanks=NBanks+1
            go to 1680
1690        rewind 71
            if(NBanks.le.0) go to 9000
          endif
        else
          if(n.gt.1) then
            WizardMode=.false.
            ich=0
            id=NextQuestId()
            xqd=250.
            il=6
            call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,-1,0)
            il=-7
            tpom=5.
            Veta='Please select TOF parameters which correspond'
            call FeQuestLblMake(id,tpom,il,Veta,'L','N')
            Veta='to the data block just imported:'
            il=il-6
            call FeQuestLblMake(id,tpom,il,Veta,'L','N')
            il=il-10
            Veta=' '
            dpom=50.
            call FeQuestEudMake(id,tpom,il,tpom,il,' ','L',dpom,EdwYd,1)
            nEdwN=EdwLastMade
            nsel=NLast+1
            if(nsel.gt.n) nsel=1
            call FeQuestIntEdwOpen(EdwLastMade,nsel,.false.)
            call FeQuestEudOpen(EdwLastMade,1,n,1,dpom,dpom,dpom)
            tpom=tpom+dpom+120.
            Veta='Detector angle:'
            if(BankJason(1).gt.0) then
              n=8
            else
              n=4
            endif
            do i=1,n
              call FeQuestLblMake(id,tpom,il,Veta,'R','N')
              call FeQuestLblMake(id,tpom+5.,il,' ','L','N')
              if(i.eq.1) then
                nLblFirst=LblLastMade
              else if(i.eq.n) then
                exit
              endif
              if(BankJason(1).le.0) then
                Veta=lTOF1Pwd(i)
              else
                Veta=lTOF2Pwd(i)
              endif
              Veta=Veta(:idel(Veta))//':'
              il=il-6
            enddo
1755        nLbl=nLblFirst
            do i=1,n
              if(i.eq.1) then
                pom=BankTTh(nsel)
              else if(i.eq.2) then
                pom=BankZero(nsel)
              else if(i.eq.3) then
                pom=BankDifc(nsel)
              else if(i.eq.4) then
                pom=BankDifa(nsel)
              else if(i.eq.5) then
                pom=BankZeroE(nsel)
              else if(i.eq.6) then
                pom=BankCE(nsel)
              else if(i.eq.7) then
                pom=BankWCross(nsel)
              else if(i.eq.8) then
                pom=BankTCross(nsel)
              endif
              write(Veta,101) pom
              call ZdrcniCisla(Veta,1)
              call FeQuestLblChange(nLbl,Veta)
              nLbl=nLbl+2
            enddo
1760        call FeQuestEvent(id,ich)
            if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwN) then
              call FeQuestIntFromEdw(nEdwN,nsel)
              go to 1755
            else if(CheckType.ne.0) then
              call NebylOsetren
              go to 1760
            endif
            call FeQuestRemove(id)
            if(nsel.ne.1) then
              BankTTh (1)=BankTTh (nsel)
              BankDifc(1)=BankDifc(nsel)
              BankDifa(1)=BankDifa(nsel)
              BankZero(1)=BankZero(nsel)
              if(BankJason(1).gt.0) then
                BankZeroE(1)=BankZeroE(nsel)
                BankCE(1)=BankCE(nsel)
                BankWCross(1)=BankWCross(nsel)
                BankTCross(1)=BankTCross(nsel)
              endif
              call CopyVek(BankSigma(1,nsel),BankSigma(1,1),3)
              call CopyVek(BankGamma(1,nsel),BankGamma(1,1),3)
              call CopyVek(BankAlfbe(1,nsel),BankAlfbe(1,1),4)
              call CopyVek(BankAlfbt(1,nsel),BankAlfbt(1,1),4)
            endif
            WizardMode=.true.
            NLast=nsel
          endif
          NBanks=1
        endif
        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else if(DifCode(KRefBlock).eq.IdTOFISISD) then
        NBanks=0
        rewind 71
1800    read(71,FormA,err=9000,end=1820) Radka
        call Mala(Radka)
        if(Radka.eq.' ') go to 1800
        k=0
        call Kus(Radka,k,Cislo)
        if(Cislo.eq.'#s') NBanks=NBanks+1
        go to 1800
1820    rewind 71
        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else if(DifCode(KRefBlock).eq.IdTOFVEGA) then
        NBanks=1
        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else if(DifCode(KRefBlock).eq.IdEDInel) then
1950    read(71,FormA,err=9000,end=1960) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1950
        k=0
        n=0
        call kus(Radka,k,Veta)
        if(EqIgCase(Veta,'CAL_OFFSET:')) then
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9000
          EDOffsetRefBlock(KRefBlock)=xp(1)*1000.
          if(mod(n,10).eq.0) n=n+1
        else if(EqIgCase(Veta,'CAL_SLOPE:')) then
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9000
          EDSlopeRefBlock(KRefBlock)=xp(1)*1000.
         if(mod(n/10,10).eq.0) n=n+10
        else if(EqIgCase(Veta,'TWO_THETA:')) then
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9000
          EDTThRefBlock(KRefBlock)=xp(1)
          EDEnergy2DRefBlock(KRefBlock)=
     1      .5*TokEV/sin(EDTThRefBlock(KRefBlock)*.5*ToRad)
          if(mod(n/100,100).eq.0) n=n+100
        else if(EqIgCase(Veta,'DATA:')) then
          go to 1960
        endif
        go to 1950
1960    RadiationRefBlock(KRefBlock)=XRayRadiation
!      else if(DifCode(KRefBlock).eq.IdTOFFree) then
!        NBanks=1
!        RadiationRefBlock(KRefBlock)=NeutronRadiation
      else if(DifCode(KRefBlock).eq.IdDataFreeOnlyI) then
2100    read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,2,.false.,ich)
        if(ich.ne.0) go to 2100
        deg0=xp(1)
        degs=xp(2)
        PolarizationRefBlock(KRefBLock)=PolarizedMonoPar
      else if(DifCode(KRefBlock).eq.IdDataD1AD2B) then
        NImpPwd=2
        read(71,'(16x,f8.3)',err=9000) degs
        read(71,'(f8.3)',err=9000) deg0
        read(71,'(2f8.0)',err=9000) RMoni,RMoniOld
        if(RMoniOld.lt.1.) then
          CNormD1AD2B=1.
        else
          CNormD1AD2B=RMoni/RMoniOld
        endif
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        PolarizationRefBlock(KRefBlock)=PolarizedLinear
      else if(DifCode(KRefBlock).eq.IdDataD1BD20) then
        NImpPwd=2
        read(71,FormA,err=9000)
        read(71,FormA,err=9000)
        read(71,FormA,err=9000)
        read(71,'(f13.0,f9.0,1X,10(f8.3,1X))') pom,pom,deg0,pom,pom,
     1                                         pom,pom,pom,degs
        read(71,FormA,err=9000)
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        PolarizationRefBlock(KRefBlock)=PolarizedLinear
      else if(DifCode(KRefBlock).eq.IdDataSaclay.or.
     1        DifCode(KRefBlock).eq.IdDataD1AD2BOld.or.
     2        DifCode(KRefBlock).eq.IdDataRiet7) then
        read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,3,.false.,ich)
        if(ich.ne.0) go to 9000
        degs=xp(2)
        deg0=xp(1)
        if(DifCode(KRefBlock).eq.IdDataRiet7) then
          read(71,FormA,err=9000,end=9000) Radka
          k=0
          call kus(Radka,k,t80)
          kp=k
2140      if(Radka(kp:kp).eq.' ') then
            kp=kp-1
            go to 2140
          endif
          call kus(Radka,k,t80)
2150      if(Radka(k:k).eq.' ') then
            k=k-1
            go to 2150
          endif
          backspace 71
          if(k.eq.8) then
            NImpPwd=2
          else if(k.eq.16) then
            if(kp.eq.8.and.index(Radka,'.').le.0) then
              NImpPwd=1
            else
              NImpPwd=-2
            endif
          endif
        else
          NImpPwd=2
        endif
        if(DifCode(KRefBlock).eq.IdDataSaclay.or.
     1     DifCode(KRefBlock).eq.IdDataD1AD2BOld) then
          RadiationRefBlock(KRefBlock)=NeutronRadiation
          PolarizationRefBlock(KRefBLock)=PolarizedLinear
        else
          if(NImpPwd.eq.2) then
            RadiationRefBlock(KRefBlock)=NeutronRadiation
            PolarizationRefBlock(KRefBLock)=PolarizedLinear
          else if(NImpPwd.eq.-2) then
            PolarizationRefBlock(KRefBLock)=PolarizedLinear
          endif
        endif
      else if(DifCode(KRefBlock).eq.IdDataPSI) then
        read(71,FormA,err=9000,end=9000) Radka
        read(71,FormA,err=9000,end=9000) Radka
        k=LocateSubstring(Radka,'lambda=',.false.,.true.)
        if(k.gt.0) then
          k=k+7
          i=0
2170      if(Radka(k:k).eq.' ') then
            k=k+1
            go to 2170
          else if(index(Cifry,Radka(k:k)).gt.0) then
            i=i+1
            Cislo(i:i)=Radka(k:k)
            k=k+1
            go to 2170
          endif
          read(Cislo,'(f15.0)',err=2175) pom
          LamAveRefBlock(KRefBlock)=pom
          LamA1RefBlock(KRefBlock)=pom
          LamA2RefBlock(KRefBlock)=pom
        endif
2175    NImpPwd=1
        read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,3,.false.,ich)
        if(ich.ne.0) then
          k=0
          call StToReal(Radka,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9000
          xp(3)=360.
        endif
        deg0=xp(1)
        degs=xp(2)
        NPntsMax=nint((xp(3)-deg0)/degs)+1
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        PolarizationRefBlock(KRefBLock)=PolarizedLinear
      else if(DifCode(KRefBlock).eq.IdDataCPI) then
        NImpPwd=1
        read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9000
        deg0=xp(1)
        read(71,FormA,err=9000,end=9000) Radka
        read(71,FormA,err=9000,end=9000) Radka
        k=0
        call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9000
        degs=xp(1)
2200    read(71,FormA,err=9000,end=9000) Radka
        if(.not.EqIgCase(Radka,'scandata')) go to 2200
        PolarizationRefBlock(KRefBLock)=PolarizedMonoPar
      else if(DifCode(KRefBlock).eq.IdDataUXD) then
        n=0
2210    read(71,FormA,err=9000,end=9000) Radka
        call velka(Radka)
        if(index(Radka,'_FILEVERSION').ne.0) then
          j=0
        else if(index(Radka,'_STEPSIZE').ne.0) then
          j=1
          go to 2220
        else if(index(Radka,'_2THETACOUNTS').ne.0.or.
     1          index(Radka,'_2THETACPS').ne.0) then
          UXDVersion=-UXDVersion
          go to 2210
        else if(index(Radka,'_2THETA').ne.0) then
          j=10
          go to 2220
        else
          if(Radka(1:1).eq.';'.or.Radka(1:1).eq.'_') then
            go to 2210
          else
            if(mod(n,10).lt.1.or.n/10.lt.0) then
              if(n.eq.0) then
                Radka='neither scan step nor starting 2th were found.'
              else if(n/10.lt.0) then
                Radka='starting 2th wasn''t found.'
              else
                Radka='scan step wasn''t found.'
              endif
              call FeChybne(-1.,-1.,Radka,' ',SeriousError)
              call FeTxOutEnd
              ErrFlag=1
              go to 9900
            endif
            backspace 71
            go to 9900
          endif
        endif
2220    i=index(Radka,'=')
        if(i.gt.0) then
          Cislo=Radka(i+1:)
          if(j.eq.0) then
            i=idel(Cislo)+1
            Cislo(i:i)='.'
          endif
          read(Cislo,100,err=2210) pom
          if(j.eq.0) then
            UXDVersion=nint(pom)
          else if(j.eq.1) then
            degs=pom
          else if(j.eq.10) then
            deg0=pom
          endif
          n=n+j
        endif
        go to 2210
      else if(DifCode(KRefBlock).eq.IdDataXRDML) then
        call  PwdBasicXRDML(ich)
        deg0=0.
        if(ich.ne.0) go to 9999
      else if(DifCode(KRefBlock).eq.IdDataPwdRigaku) then
        call  PwdBasicRigaku(ich)
        deg0=0.
        if(ich.ne.0) go to 9999
      else if(DifCode(KRefBlock).eq.IdDataPwdHuber) then
        call  PwdBasicHuber(ich)
        if(ich.ne.0) go to 9999
      else if(DifCode(KRefBlock).eq.IdDataPwdStoe) then
        call  PwdBasicStoe(ich)
        if(ich.ne.0) go to 9999
      else if(DifCode(KRefBlock).eq.IdDataG41) then
        call  PwdBasicG41(ich)
        if(ich.ne.0) go to 9999
      else
        deg0=0.
      endif
      go to 9900
9000  call FeReadError(71)
      ich=1
      go to 9999
9200  call FeReadError(ln)
      call CloseIfOpened(ln)
9900  deg=deg0
9999  return
100   format(f15.0)
101   format(f15.4)
102   format(i2)
      end
      subroutine GetNextPCR(ln,Radka,iend,ierr)
      include 'fepc.cmn'
      include 'basic.cmn'
      character*(*) Radka
      iend=0
      ierr=0
1100  read(ln,FormA,end=2000,err=3000) Radka
      if(Radka(1:1).eq.'!') go to 1100
      go to 9999
2000  iend=1
      go to 4000
3000  ierr=1
4000  Radka=' '
9999  return
      end
      subroutine PwdBasicXRDML(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension xp(1)
      character*256 Veta
      character*80 t80
      character*1  t1
      integer CR,Stav
      real, allocatable :: Int(:),IntOld(:)
      logical EqIgCase,SkipScan
      data CR,LF/13,10/
      if(allocated(Int)) deallocate(Int)
      ich=0
      Nacetl=0
      Stav=0
      NPointsAll=0
      XStart=-9999.
      XEnd  =-9999.
      call CloseIfOpened(71)
      call FeFileBufferOpen(SourceFileRefBlock(KRefBlock),OpSystem)
1100  n=1
      i=0
      Veta=' '
1200  call FeFileBufferGetOneCharacter(t1,ich)
      if(ich.ne.0) then
        ich=0
        go to 5000
      endif
1250  if(t1.eq.'<') then
        i=1
        n=1
        Veta=t1
        if(Stav/10.eq.11) then
          k=0
          call StToReal(Cislo,k,xp,1,.false.,ich)
          if(ich.eq.0) then
            if(Stav.eq.111) then
              LamA1RefBlock(NRefBlock)=xp(1)
              Nacetl=Nacetl+1
            else if(Stav.eq.112) then
              LamA2RefBlock(NRefBlock)=xp(1)
              Nacetl=Nacetl+1
            else if(Stav.eq.113) then
              LamRatRefBlock(NRefBlock)=max(xp(1),0.)
              Nacetl=Nacetl+1
            endif
          endif
          Stav=11
        else if(Stav/10.eq.1211) then
          k=0
          call StToReal(Cislo,k,xp,1,.false.,ich)
          if(ich.eq.0) then
            if(Stav.eq.12111) then
              XStartInd=xp(1)
            else if(Stav.eq.12112) then
              XEndInd=xp(1)
            endif
          endif
          Stav=1211
        endif
      else if(t1.eq.'>'.and.i.eq.1) then
        if(n.lt.len(Veta)) n=n+1
        Veta(n:n)=t1
        if(Stav.eq.0) then
          if(LocateSubstring(Veta,'xrdMeasurement ',.false.,.true.)
     1     .eq.2) then
            Stav=1
          endif
        else if(Stav.eq.1) then
          if(LocateSubstring(Veta,'usedWavelength',.false.,.true.).eq.2)
     1      then
            Stav=11
          else if(LocateSubstring(Veta,'scan',.false.,.true.).eq.2)
     1      then
            k=LocateSubstring(Veta,'status=',.false.,.true.)
            t80=Veta(k+idel('status='):)
            k1=index(t80,'"')+1
            k2=index(t80(k1+1:),'"')
            k2=k2+k1-1
            SkipScan=EqIgCase(t80(k1:k2),'Aborted')
            Stav=12
          else if(LocateSubstring(Veta,'/xrdMeasurement',.false.,.true.)
     1            .eq.2) then
              Stav=Stav/10
              go to 1100
          else
            go to 1100
          endif
        else if(Stav.eq.11) then
          if(LocateSubstring(Veta,'kAlpha1',.false.,.true.).eq.2) then
            Stav=111
            Cislo=' '
          else if(LocateSubstring(Veta,'kAlpha2',.false.,.true.).eq.2)
     1      then
            Stav=112
            Cislo=' '
          else if(LocateSubstring(Veta,'ratioKAlpha2KAlpha1',
     1                            .false.,.true.).eq.2) then
            Stav=113
            Cislo=' '
          else if(LocateSubstring(Veta,'/usedWavelength',.false.,.true.)
     1            .eq.2) then
            Stav=Stav/10
            write(Cislo,FormI15) Stav
          endif
          go to 1100
        else if(Stav.eq.12) then
          if(LocateSubstring(Veta,'dataPoints',.false.,.true.).eq.2)
     1      then
            Stav=121
          else if(LocateSubstring(Veta,'/scan',.false.,.true.).eq.2)
     1      then
            Stav=Stav/10
          endif
          go to 1100
        else if(Stav.eq.121) then
          if(LocateSubstring(Veta,'positions axis="2Theta" unit="deg"',
     1       .false.,.true.).eq.2) then
            Stav=1211
          else if(LocateSubstring(Veta,'intensities',.false.,.true.)
     1      .eq.2) then
            if(.not.allocated(Int)) then
              NPointsMax=100
              allocate(Int(NPointsMax))
              Int=0.
            endif
            NPoints=0
2000        Cislo=' '
2100        call FeFileBufferGetOneCharacter(t1,ich)
            if(t1.eq.' '.or.t1.eq.'<') then
              if(idel(Cislo).le.0) then
                go to 2000
              else
                if(index(Cislo,'.').le.0) Cislo=Cislo(:idel(Cislo))//'.'
                k=0
                call StToReal(Cislo,k,xp,1,.false.,ich)
                if(ich.eq.0.and..not.SkipScan) then
                  if(NPoints.ge.NPointsMax) then
                    allocate(IntOld(NPoints))
                    call CopyVek(Int,IntOld,NPoints)
                    deallocate(Int)
                    NPointsMax=NPointsMax+100
                    allocate(Int(NPointsMax))
                    Int=0.
                    call CopyVek(IntOld,Int,NPoints)
                    deallocate(IntOld)
                  endif
                  NPoints=NPoints+1
                  Int(NPoints)=Int(NPoints)+xp(1)
                endif
              endif
              if(t1.eq.'<') then
                if(.not.SkipScan) then
                  NPointsAll=max(NPoints,NPointsAll)
                  XStart=max(XStart,XStartInd)
                  XEnd=max(XEnd,XEndInd)
                endif
                go to 1250
              else
                go to 2000
              endif
            else
              Cislo=Cislo(:idel(Cislo))//t1
              go to 2100
            endif
          else if(LocateSubstring(Veta,'/dataPoints',.false.,.true.)
     1            .eq.2) then
            Stav=Stav/10
          endif
          go to 1100
        else if(Stav.eq.1211) then
          if(LocateSubstring(Veta,'startPosition',.false.,.true.).eq.2)
     1      then
            Stav=12111
            Cislo=' '
          else if(LocateSubstring(Veta,'endPosition',.false.,.true.)
     1            .eq.2) then
            Stav=12112
            Cislo=' '
          else if(LocateSubstring(Veta,'/positions',.false.,.true.)
     1            .eq.2) then
            Stav=Stav/10
          endif
          go to 1100
        endif
      else if(Stav/10.eq.11.or.Stav/10.eq.1211) then
        idl=idel(Cislo)
        if(idl.ge.len(Cislo)) idl=idl-1
        Cislo=Cislo(:idel(Cislo))//t1
      else if(ichar(t1).eq.CR.or.ichar(t1).eq.LF.or.i.eq.0) then
        go to 1200
      else
        if(n.lt.len(Veta)) n=n+1
        Veta(n:n)=t1
      endif
      go to 1200
5000  if(Nacetl.eq.3) then
        if(LamRatRefBlock(NRefBlock).le.0) then
          LamA2RefBlock(NRefBlock)=LamA1RefBlock(NRefBlock)
          LamAveRefBlock(NRefBlock)=LamA1RefBlock(NRefBlock)
          KLamRefBlock(NRefBlock)=
     1      LocateInArray(LamA1RefBlock(NRefBlock),LamA1D,7,.0001)
          NAlfaRefBlock(NRefBlock)=1
        else
          LamAveRefBlock(NRefBlock)=(LamA1RefBlock(NRefBlock)+
     1                               LamRatRefBlock(NRefBlock)*
     2                               LamA1RefBlock(NRefBlock))/
     3                              (1.+LamRatRefBlock(NRefBlock))
          KLamRefBlock(NRefBlock)=
     1      LocateInArray(LamAveRefBlock(NRefBlock),LamAveD,7,.0001)
          NAlfaRefBlock(NRefBlock)=2
        endif
      else
        ich=1
        go to 9999
      endif
      if(NPointsAll.gt.0) then
        Veta=SourceFileRefBlock(KRefBlock)
     1       (:idel(SourceFileRefBlock(KRefBlock)))//'_xy'
        call OpenFile(71,Veta,'formatted','unknown')
        Step=anint(100000.*(XEnd-XStart)/float(NPointsAll-1))/100000.
        do i=1,NPointsAll
          write(71,'(2f15.6)') XStart+float(i-1)*Step,Int(i)
        enddo
        rewind 71
      else
        ich=1
      endif
9999  if(allocated(Int)) deallocate(Int)
      return
      end
      subroutine PwdBasicRigaku(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*256 Veta
      character*80  t80
      logical EqIgCase
      integer, allocatable :: ipor(:)
      integer Flags
      real, allocatable :: XPom(:),XPomOld(:),Int(:),IntOld(:),SInt(:),
     1                     SIntOld(:)
      real xp(3),ISum
      ich=0
      ib=0
      NPoints=0
      if(allocated(XPom)) deallocate(XPom,Int,SInt)
      NPointsMax=1000
      allocate(XPom(NPointsMax),Int(NPointsMax),SInt(NPointsMax))
      XPom=0.
      Int=0.
1100  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1150
      k=0
      call Kus(Veta,k,t80)
      if(EqIgCase(Veta,'*RAS_DATA_START')) then
        RigakuPwdType=RigakuPwdTypeRas
      else if(EqIgCase(Veta,'*RAS_HEADER_START')) then
        RigakuPwdType=RigakuPwdTypeRas
        go to 1150
      else if(EqIgCase(t80,'*TYPE')) then
        call Kus(Veta,k,t80)
        if(.not.EqIgCase(t80,'=')) go to 1100
        call Kus(Veta,k,t80)
        if(.not.EqIgCase(t80,'Raw')) go to 1100
        RigakuPwdType=RigakuPwdTypeRaw
      else
        go to 1100
      endif
      nl=0
1150  Speed=1.
      Flags=0
1200  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1200
      k=0
      call Kus(Veta,k,t80)
      if(RigakuPwdType.eq.RigakuPwdTypeRas) then
        if(EqIgCase(t80,'*RAS_DATA_END')) then
          go to 1500
        else if(EqIgCase(t80,'*RAS_HEADER_END')) then
          ib=ib+1
          go to 1300
        else if(EqIgCase(t80,'*MEAS_SCAN_START')) then
          i=1
        else if(EqIgCase(t80,'*MEAS_SCAN_STEP')) then
          i=2
        else if(EqIgCase(t80,'*HW_XG_WAVE_LENGTH_ALPHA1')) then
          i=3
        else if(EqIgCase(t80,'*HW_XG_WAVE_LENGTH_ALPHA2')) then
          i=4
        else if(EqIgCase(t80,'*MEAS_SCAN_SPEED')) then
          i=5
        else if(EqIgCase(t80,'*RAS_TEMPERATURE_START')) then
1220      read(71,FormA,end=9000) Veta
          k=0
          call kus(Veta,k,t80)
          if(.not.EqIgCase(t80,'*RAS_TEMPERATURE_END')) go to 1220
          go to 1200
        else if(Veta(1:1).eq.'*') then
          go to 1200
        else
          go to 9900
        endif
        call Kus(Veta,k,t80)
        do j=1,idel(t80)
          if(t80(j:j).eq.'"') t80(j:j)=' '
        enddo
      else if(RigakuPwdType.eq.RigakuPwdTypeRaw) then
        if(EqIgCase(t80,'*EOF')) then
          go to 1500
        else if(EqIgCase(t80,'*BEGIN')) then
          go to 1200
        else if(EqIgCase(t80,'*START')) then
          i=1
        else if(EqIgCase(t80,'*STEP')) then
          i=2
        else if(EqIgCase(t80,'*WAVE_LENGTH1')) then
          i=3
        else if(EqIgCase(t80,'*WAVE_LENGTH2')) then
          i=4
        else if(EqIgCase(t80,'*SPEED')) then
          i=5
        else if(Veta(1:1).eq.'*') then
          go to 1200
        else
          if(Flags.eq.11111.or.(ib.gt.0.and.Flags.eq.10011)) then
            backspace 71
            if(RigakuPwdType.eq.RigakuPwdTypeRaw)
     1        Speed=60.*degs/Speed
            nn=0
            ib=ib+1
            go to 1400
          else
            go to 9900
          endif
        endif
        call Kus(Veta,k,t80)
        if(.not.EqIgCase(t80,'=')) go to 1200
        call Kus(Veta,k,t80)
      endif
      read(t80,*) pom
      if(i.eq.1) then
        deg0=pom
        Flags=Flags+1
      else if(i.eq.2) then
        degs=pom
        if(ib.eq.0) degs0=pom
        Flags=Flags+10
      else if(i.eq.3) then
        LamA1RefBlock(NRefBlock)=pom
        nl=nl+1
        Flags=Flags+100
      else if(i.eq.4) then
        LamA2RefBlock(NRefBlock)=pom
        nl=nl+10
        Flags=Flags+1000
      else if(i.eq.5) then
        Speed=pom
        Flags=Flags+10000
      endif
      go to 1200
1300  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1300
      k=0
      call Kus(Veta,k,t80)
      if(.not.EqIgCase(t80,'*RAS_INT_START')) go to 1300
1400  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1400
      k=0
      call Kus(Veta,k,t80)
      if(RigakuPwdType.eq.RigakuPwdTypeRas) then
        if(EqIgCase(t80,'*RAS_INT_END')) go to 1150
      else
        if(EqIgCase(t80,'*END')) go to 1150
        do j=1,idel(Veta)
          if(Veta(j:j).eq.',') Veta(j:j)=' '
        enddo
      endif
      k=0
1450  if(RigakuPwdType.eq.RigakuPwdTypeRas) then
        call StToReal(Veta,k,xp,3,.false.,ich)
      else
        call StToReal(Veta,k,xp(2),1,.false.,ich)
        nn=nn+1
        xp(1)=deg0+float(nn-1)*degs
        xp(3)=1.
      endif
      if(NPoints.ge.NPointsMax) then
        if(allocated(XPomOld)) deallocate(XPomOld,IntOld,SIntOld)
        allocate(XPomOld(NPointsMax),IntOld(NPointsMax),
     1           SIntOld(NPointsMax))
        XPomOld=XPom
        IntOld=Int
        SIntOld=SInt
        deallocate(XPom,Int,SInt)
        NPointsMax=2*NPointsMax
        allocate(XPom(NPointsMax),Int(NPointsMax),SInt(NPointsMax))
        XPom=0.
        Int=0.
        XPom(1:NPoints)=XPomOld(1:NPoints)
        Int(1:NPoints)=IntOld(1:NPoints)
        SInt(1:NPoints)=SIntOld(1:NPoints)
        if(allocated(XPomOld)) deallocate(XPomOld,IntOld,SIntOld)
      endif
      NPoints=NPoints+1
      XPom(NPoints)=xp(1)
!      XPom(NPoints)=xp(1)+degs*.5
      pom=Speed*degs0/degs
      Int(NPoints)=xp(2)*pom
      SInt(NPoints)=max(sqrt(xp(2))*pom,1.)
      if(abs(xp(3)-1.).gt..001) then
        write(Cislo,'(f10.5)') xp(3)
        call FeWinMessage('Uz je to tady',Cislo)
      endif
      if(RigakuPwdType.eq.RigakuPwdTypeRas) then
        go to 1400
      else
        if(k.lt.len(Veta)) then
          go to 1450
        else
          go to 1400
        endif
      endif
1500  call CloseIfOpened(71)
      if(NPoints.gt.0) then
        Veta=SourceFileRefBlock(KRefBlock)
     1       (:idel(SourceFileRefBlock(KRefBlock)))//'_xy'
        call OpenFile(71,Veta,'formatted','unknown')
        allocate(ipor(NPoints))
        call RIndexx(NPoints,XPom,ipor)
        do ii=1,NPoints
          i=ipor(ii)
          if(Int(i).lt.0.) cycle
          XSum=XPom(i)
          wt=1./SInt(i)**2
          SSum=(wt*SInt(i))**2
          ISum=wt*Int(i)
          WSum=wt
          do jj=ii+1,NPoints
            j=ipor(jj)
            if(XPom(j)-XSum.gt..001) exit
            if(Int(j).lt.0.) cycle
            if(abs(XPom(j)-XSum).lt..000001) then
              wt=1./SInt(j)**2
              ISum=ISum+wt*Int(j)
              SSum=SSum+(wt*SInt(j))**2
              WSum=WSum+wt
              Int(j)=-999.
            endif
          enddo
          Int(i)=ISum/WSum
          SInt(i)=sqrt(SSum)/WSum
        enddo
        do ii=1,NPoints
          i=ipor(ii)
          if(Int(i).ge.0.) write(71,'(3f15.6)') XPom(i),Int(i),SInt(i)
        enddo
        deallocate(ipor)
        rewind 71
        go to 9900
      else
        ich=1
      endif
9000  call FeChybne(-1.,-1.,'EOF reached without detecting relevant '//
     1              'data',' ',SeriousError)
      ich=1
      go to 9999
9900  if(nl.ge.10) then
        NAlfaRefBlock(NRefBlock)=2
      else
        NAlfaRefBlock(NRefBlock)=1
      endif
9999  return
      end
      subroutine PwdBasicStoe(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*512 Veta
      character*2   Ch2
      character*4   Ch4
      character*1 VetaP(512)
      integer FeOpenBinaryFile,FeReadBinaryFile,I4
      integer*2 I2
      equivalence (Ch2,I2),(Ch4,I4,R4),(Veta,VetaP)
      ich=0
      call CloseIfOpened(71)
      LnStoe=FeOpenBinaryFile(SourceFileRefBlock(KRefBlock)
     1                 (:idel(SourceFileRefBlock(KRefBlock))))
      if(LnStoe.le.0) go to 9999
      i=FeReadBinaryFile(LnStoe,VetaP,512)
      if(i.le.0) go to 9000
      do i=1,304
        if(ichar(Veta(i:i)).eq.0) Veta(i:i)=' '
      enddo
      RadiationRefBlock(KRefBlock)=XRayRadiation
      PolarizationRefBlock(KRefBLock)=PolarizedMonoPar
      NAlfaRefBlock(KRefBlock)=2
      k=305
      do i=1,9
        ch2=Veta(k:k+1)
        k=k+2
        if(i.eq.3) then
        else if(i.eq.4) then
          KLamRefBlock(KRefBlock)=8-i2
        endif
      enddo
      do i=1,3
        ch4=Veta(k:k+3)
        if(i.eq.1) then
          LamA1RefBlock(KRefBlock)=R4
        else if(i.eq.2) then
          LamA2RefBlock(KRefBlock)=R4
          LamRatRefBlock(KRefBlock)=.5
          LamAveRefBlock(KRefBlock)=(LamA1RefBlock(KRefBlock)+
     1                               LamRatRefBlock(KRefBlock)*
     2                               LamA1RefBlock(KRefBlock))/
     3                               (1.+LamRatRefBlock(KRefBlock))
        else if(i.eq.3) then
          pom=LamAveRefBlock(KRefBlock)*.5*R4
          if(abs(pom).lt..9999) AngleMon(KDatBlock)=asin(pom)/ToRad
        endif
        k=k+4
      enddo
      do i=1,4
        ch2=Veta(k:k+1)
        k=k+2
      enddo
      do i=1,4
        ch4=Veta(k:k+3)
        k=k+4
      enddo
      do i=1,13
        ch2=Veta(k:k+1)
        k=k+2
      enddo
      do i=1,9
        ch4=Veta(k:k+3)
        k=k+4
      enddo
      n=0
1100  n=n+1
      if(n.gt.10) stop
      i=FeReadBinaryFile(LnStoe,VetaP,512)
      do i=1,32
        if(ichar(Veta(i:i)).eq.0) Veta(i:i)=' '
      enddo
      if(Veta(3:3).ne.'-'.or.Veta(7:7).ne.'-'.or.
     1   Veta(13:13).ne.':') go to 1100
      k=33
      do i=1,4
        ch2=Veta(k:k+1)
        k=k+2
        if(i.eq.4) then
          if(i2.le.0) then
            StoeDataType=4
          else
            StoeDataType=2
          endif
        endif
      enddo
      do i=1,18
        ch4=Veta(k:k+3)
        if(i.eq.2) then
          Deg0=R4
        else if(i.eq.4) then
          DegF=R4
        else if(i.eq.6) then
          DegS=R4
        endif
        k=k+4
      enddo
      NPntsMax=nint((DegF-Deg0)/DegS)+1
      go to 9999
9000  ich=1
9999  return
      end
      subroutine PwdBasicHuber(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*256 Veta
      character*80  t80
      real xp(2)
      integer ip(1)
      logical EqIgCase
      ich=0
      NImpPwd=1
      n=0
1100  read(71,FormA,end=9000) Veta
      if(Veta.eq.' ') go to 1100
      n=n+1
      if(n.gt.20) then
        HuberPwdType=HuberPwdTypeMdi
        go to 1500
      endif
      if(LocateSubstring(Veta,'/*<beginc>',.false.,.true.).gt.0) then
1200    if(LocateSubstring(Veta,'<endc>*/',.false.,.true.).gt.0) then
          HuberPwdType=HuberPwdTypeGDF
          go to 1300
        else
          read(71,FormA,end=9000) Veta
          go to 1200
        endif
      else
        go to 1100
      endif
1300  read(71,*,end=9000) Deg0
      read(71,*,end=9000) DegF
      read(71,*,end=9000) DegS
      NPntsMax=nint((DegF-Deg0)/DegS)+1
      do i=1,3
        read(71,FormA,end=9000) Veta
      enddo
      go to 9999
      entry PwdBasicHuberMDI(ich)
1500  rewind 71
      do i=1,2
        read(71,FormA,end=9000) Veta
      enddo
      k=0
      call StToReal(Veta,k,xp,3,.false.,ich)
      if(ich.ne.0) go to 9100
      Deg0=xp(1)
      DegS=xp(2)
      call Kus(Veta,k,Cislo)
      call StToReal(Veta,k,xp,3,.false.,ich)
      if(ich.ne.0) go to 9100
      LamA1RefBlock(KRefBlock)=xp(1)
      DegF=xp(2)
      NPntsMax=nint((DegF-Deg0)/DegS)+1
      go to 9999
9000  call FeChybne(-1.,-1.,'EOF reached without detecting relevant '//
     1              'data',' ',SeriousError)
      go to 9900
9100  call FeReadError(71)
9900  call CloseIfOpened(71)
      ich=1
9999  return
      end
      subroutine PwdBasicG41(ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      character*256 Veta
      real xp(6)
      ich=0
      do i=1,4
        read(71,FormA,err=9100,end=9100) Veta
      enddo
      k=0
      call StToReal(Veta,k,xp,6,.false.,ich)
      if(ich.ne.0) go to 9100
      NPntsMax=nint(xp(1))
      TempRefBlock(KRefBlock)=xp(2)
      IVariG41=nint(xp(4))
      if(xp(5).gt.1..and.xp(6).gt.1.) then
        CNormG41=xp(5)/xp(6)
      else
        CNormG41=1.
      endif
      read(71,FormA,err=9100,end=9100) Veta
      k=0
      call StToReal(Veta,k,xp,3,.false.,ich)
      deg0=xp(1)
      degs=xp(2)
      RadiationRefBlock(KRefBlock)=NeutronRadiation
      PolarizationRefBlock(KRefBlock)=PolarizedLinear
      LamAveRefBlock(KRefBlock)=2.423
      LamA1RefBlock(KRefBlock)=2.423
      LamA2RefBlock(KRefBlock)=2.423
      go to 9999
9000  call FeChybne(-1.,-1.,'EOF reached without detecting relevant '//
     1              'data',' ',SeriousError)
      go to 9900
9100  call FeReadError(71)
9900  call CloseIfOpened(71)
      ich=1
9999  return
      end
      subroutine PwdBasicDefInsFile(Klic,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      character*256 EdwStringQuest,Veta
      logical ExistFile
      WizardMode=.false.
      id=NextQuestId()
      xqd=400.
      if(Klic.eq.1) then
        il=6
      else
        il=3
      endif
      Veta='Define instrument parameter file:'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,1,LightGray,0,0)
      il=1
      Veta='%Browse'
      dpomb=FeTxLengthUnder(Veta)+10.
      xpomb=xqd-dpomb-5.
      call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
      nButtBrowsePar=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=5.
      dpom=xpomb-xpom-10.
      call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
      nEdwFilePar=EdwLastMade
      if(InsParFileRefBlock(KRefBlock).eq.' ') then
        do i=KRefBlock-1,1,-1
          if(InsParFileRefBlock(i).ne.' ') then
            InsParFileRefBlock(KRefBlock)=InsParFileRefBlock(i)
            exit
          endif
        enddo
      endif
      call FeQuestStringEdwOpen(EdwLastMade,
     1                          InsParFileRefBlock(KRefBlock))
      if(Klic.eq.1) then
        call FeFillTextInfo('pwdbasictof1.txt',0)
      else
        call FeFillTextInfo('pwdbasictof2.txt',0)
      endif
      tpom=5.
      ilp=il
      il=-10*(il+1)+2
      do i=1,NInfo
        call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
        il=il-6
      enddo
      if(Klic.eq.1) then
        il=ilp+3
        Veta='Define incident spectral file:'
        call FeQuestLblMake(id,xqd*.5,il,Veta,'C','B')
        il=il+1
        Veta='Bro%wse'
        dpomb=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
        nButtBrowseSpec=ButtonLastMade
        call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
        call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
        nEdwFileSpec=EdwLastMade
        if(IncSpectFileRefBlock(KRefBlock).eq.' ') then
          do i=KRefBlock-1,1,-1
            if(IncSpectFileRefBlock(i).ne.' ') then
              IncSpectFileRefBlock(KRefBlock)=IncSpectFileRefBlock(i)
              exit
            endif
          enddo
        endif
        call FeQuestStringEdwOpen(EdwLastMade,
     1                            IncSpectFileRefBlock(KRefBlock))
        call FeFillTextInfo('pwdbasictof3.txt',0)
        il=-10*(il+1)+2
        do i=1,NInfo
          call FeQuestLblMake(id,tpom,il,TextInfo(i),'L','N')
          il=il-6
        enddo
      else
        nButtBrowseSpec=0
        nEdwFileSpec=0
      endif
1150  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK)
     1  then
        InsParFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFilePar)
        if(nEdwFileSpec.gt.0)
     1    IncSpectFileRefBlock(KRefBlock)=EdwStringQuest(nEdwFileSpec)
        if(.not.ExistFile(InsParFileRefBlock(KRefBlock)).and.
     1     InsParFileRefBlock(KRefBlock).ne.' ') then
          call FeChybne(-1.,-1.,'the file "'//
     1      InsParFileRefBlock(KRefBlock)
     2        (:idel(InsParFileRefBlock(KRefBlock)))//
     3      '" does not exist.',' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwFilePar
          go to 1150
        endif
        if(.not.ExistFile(IncSpectFileRefBlock(KRefBlock)).and.
     1     IncSpectFileRefBlock(KRefBlock).ne.' ') then
          call FeChybne(-1.,-1.,'the file "'//
     1      IncSpectFileRefBlock(KRefBlock)
     2        (:idel(IncSpectFileRefBlock(KRefBlock)))//
     3      '" does not exist.',' ',SeriousError)
          EventType=EventEdw
          EventNumber=nEdwFilePar
          go to 1150
        endif
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtBrowsePar) then
        Veta=EdwStringQuest(nEdwFilePar)
        call FeFileManager('Browse for instrument parameter file',
     1                      Veta,'*.irf *.prm',0,.true.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwFilePar,Veta)
        go to 1150
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtBrowseSpec) then
        Veta=EdwStringQuest(nEdwFileSpec)
        call FeFileManager('Browse for instrument parameter file',
     1                      Veta,'*.gsa *.gda',0,.true.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwFileSpec,Veta)
        go to 1150
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1150
      endif
      call FeQuestRemove(id)
      WizardMode=.true.
      if(ich.ne.0) then
        InsParFileRefBlock(KRefBlock)=' '
        IncSpectFileRefBlock(KRefBlock)=' '
      endif
      return
      end
      subroutine PwdSetBank(ich)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension xp(4),ip(4)
      character*256 Radka
      character*80  Veta
      logical EqIgCase
      equivalence (ip,xp)
      ich=0
      LRALF=.false.
      LSLOG=.false.
      if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1   DifCode(KRefBlock).eq.IdData11BM.or.
     2   DifCode(KRefBlock).eq.IdTOFGSAS.or.
     3   (DifCode(KRefBlock).eq.IdTOFISIST.and.ISISGSS)) then
        if(DifCode(KRefBlock).eq.IdDataGSAS) then
          if(NBankActual.le.1) then
            PolarizationRefBlock(KRefBlock)=PolarizedMonoPar
          else
            PolarizationRefBlock(KRefBlock)=
     1        PolarizationRefBlock(KRefBlock-1)
          endif
        else if(DifCode(KRefBlock).eq.IdData11BM) then
          RadiationRefBlock(KRefBlock)=XRayRadiation
          PolarizationRefBlock(KRefBlock)=PolarizedLinear
        else
          RadiationRefBlock(KRefBlock)=NeutronRadiation
        endif
        rewind 71
        n=0
1100    read(71,FormA,err=9100,end=9100) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.Radka(1:1).eq.'#'.or.
     1     index(Radka,'bank').ne.1) go to 1100
        n=n+1
        if(n.lt.NBankActual) go to 1100
        if(IncSpectFileOpened) then
          rewind 72
          n=0
1150      read(72,FormA,err=9100,end=9100) Veta
          call mala(Veta)
          if(Veta.eq.' '.or.Veta(1:1).eq.'#'.or.
     1       index(Veta,'bank').ne.1) go to 1150
          n=n+1
          if(n.lt.NBankActual) go to 1150
          k=5
          do i=1,6
            call kus(Veta,k,TypeBankInc)
          enddo
          if(TypeBankInc.eq.'std'.or.TypeBankInc.eq.' ') then
            FormatBankInc='(10(i2,f6.0))'
          else if(TypeBankInc.eq.'esd') then
            FormatBankInc='(10f8.0)'
          else if(TypeBankInc.eq.'alt') then
            FormatBankInc='(4(f8.0,f7.0,f5.0))'
          endif
        endif
        k=5
        call StToInt(Radka,k,ip,3,.false.,ich)
        if(ich.ne.0) go to 9100
        if(DifCode(KRefBlock).eq.IdTOFGSAS.or.
     1     DifCode(KRefBlock).eq.IdTOFISIST) then
          j=ip(1)
          if(TOFDifcRefBlock(KRefBlock).lt.-3000.) then
            TOFDifcRefBlock(KRefBlock)=BankDifc(j)
            TOFJasonRefBlock(KRefBlock)=BankJason(j)
          endif
          if(TOFDifaRefBlock(KRefBlock).lt.-3000.)
     1       TOFDifaRefBlock(KRefBlock)=BankDifa(j)
          if(TOFZeroRefBlock(KRefBlock).lt.-3000.)
     1      TOFZeroRefBlock(KRefBlock)=BankZero(j)
          if(TOFTThRefBlock(KRefBlock).lt.-3000.)
     1      TOFTThRefBlock(KRefBlock)=BankTTh(j)
          if(TOFCERefBlock(KRefBlock).lt.-3000.)
     1      TOFCERefBlock(KRefBlock)=BankCE(j)
          if(TOFZeroERefBlock(KRefBlock).lt.-3000.)
     1      TOFZeroERefBlock(KRefBlock)=BankZeroE(j)
          if(TOFTCrossRefBlock(KRefBlock).lt.-3000.)
     1      TOFTCrossRefBlock(KRefBlock)=BankTCross(j)
          if(TOFWCrossRefBlock(KRefBlock).lt.-3000.)
     1      TOFWCrossRefBlock(KRefBlock)=BankWCross(j)
          if(TOFSigmaRefBlock(1,KRefBlock).lt.-3000.)
     1      call CopyVek(BankSigma(1,j),TOFSigmaRefBlock(1,KRefBlock),3)
          if(TOFGammaRefBlock(1,KRefBlock).lt.-3000.)
     1      call CopyVek(BankGamma(1,j),TOFGammaRefBlock(1,KRefBlock),3)
          if(TOFAlfbeRefBlock(1,KRefBlock).lt.-3000.)
     1      call CopyVek(BankAlfBe(1,j),TOFAlfBeRefBlock(1,KRefBlock),4)
          if(TOFAlfbtRefBlock(1,KRefBlock).lt.-3000.)
     1      call CopyVek(BankAlfBt(1,j),TOFAlfBtRefBlock(1,KRefBlock),4)
        endif
        call kus(Radka,k,Veta)
        if(EqIgCase(Veta,'time_map')) then
          call StToInt(Radka,k,ip,1,.false.,ich)
          NTimeMap=ip(1)
        else if(EqIgCase(Veta,'const').or.EqIgCase(Veta,'cons')) then
          kp=k
          call StToReal(Radka,k,xp,4,.false.,ich)
          if(ich.ne.0) then
            k=kp
            call StToReal(Radka,k,xp,3,.false.,ich)
            if(ich.ne.0) then
              k=kp
              call StToReal(Radka,k,xp,2,.false.,ich)
              if(ich.ne.0) go to 9999
              JenDve=.true.
            endif
          endif
          deg0=xp(1)*.01
          degs=xp(2)*.01
        else if(EqIgCase(Veta,'ralf')) then
          call StToReal(Radka,k,TOFRALF,4,.false.,ich)
          do i=1,4
            TOFRALF(i)=TOFRALF(i)*32.
          enddo
          LRALF=.true.
        else if(EqIgCase(Veta,'slog')) then
          LSLOG=.true.
          call StToReal(Radka,k,SLogPwd,4,.false.,ich)
c         parametry zatim nepouzity
        else
          call Velka(Veta)
          call FeChybne(-1.,-1.,'BINTYP='//Veta(:idel(Veta))//
     1                  ' not yet implemented.',' ',SeriousError)
          go to 9900
        endif
        call kus(Radka,k,TypeBank)
        if(TypeBank.eq.'std'.or.TypeBank.eq.' ') then
          FormatBank='(10(i2,f6.0))'
        else if(TypeBank.eq.'esd') then
          FormatBank='(10f8.0)'
        else if(TypeBank.eq.'alt') then
          FormatBank='(4(f8.0,f7.0,f5.0))'
        else if(TypeBank.eq.'fxy') then
          LRALF=.false.
          FormatBank='*'
          JenDve=.true.
        else if(TypeBank.eq.'fxye') then
          LRALF=.false.
          FormatBank='*'
          JenDve=.false.
        else
          call FeChybne(-1.,-1.,'TYPE='//TypeBank(:idel(TypeBank))//
     1                  ' not yet implemented.',' ',SeriousError)
          go to 9900
        endif
      else if(DifCode(KRefBlock).eq.IdTOFISISD.or.
     1        (DifCode(KRefBlock).eq.IdTOFISIST.and..not.ISISGSS)) then
        rewind 71
        n=0
1200    read(71,FormA,err=9100,end=9100) Radka
        call Mala(Radka)
        if(Radka.eq.' ') go to 1200
        k=0
        call Kus(Radka,k,Cislo)
        if(Cislo.eq.'#s') then
          n=n+1
          if(n.ge.NBankActual) go to 1220
        endif
        go to 1200
1220    read(71,FormA,err=9100,end=9100) Radka
        if(Radka.eq.' '.or.Radka(1:1).eq.'#') go to 1220
        backspace 71
        RadiationRefBlock(KRefBlock)=NeutronRadiation
        if(DifCode(KRefBlock).eq.IdTOFISISD) then
          TOFJasonRefBlock(KRefBlock)=0
          TOFDifcRefBlock(KRefBlock)=1.
          TOFDifaRefBlock(KRefBlock)=0.
          TOFZeroRefBlock(KRefBlock)=0.
          TOFTThRefBlock(KRefBlock)=90.
          TOFCERefBlock(KRefBlock)=0.
          TOFZeroERefBlock(KRefBlock)=0.
          TOFTCrossRefBlock(KRefBlock)=0.
          TOFWCrossRefBlock(KRefBlock)=0.
        else
          TOFJasonRefBlock(KRefBlock)=BankJason(n)
          TOFDifcRefBlock(KRefBlock)=BankDifc(n)
          TOFDifaRefBlock(KRefBlock)=BankDifa(n)
          TOFZeroRefBlock(KRefBlock)=BankZero(n)
          TOFTThRefBlock(KRefBlock)=BankTTh(n)
          TOFCERefBlock(KRefBlock)=BankCE(n)
          TOFZeroERefBlock(KRefBlock)=BankZeroE(n)
          TOFTCrossRefBlock(KRefBlock)=BankTCross(n)
          TOFWCrossRefBlock(KRefBlock)=BankWCross(n)
        endif
      else if(DifCode(KRefBlock).eq.IdTOFVEGA) then
        rewind 71
1300    read(71,FormA,err=9100,end=9100) Radka
        call Mala(Radka)
        if(Radka.eq.' ') go to 1300
        k=0
        call Kus(Radka,k,Veta)
        if(EqIgCase(Veta,'x')) then
          call Kus(Radka,k,Veta)
          if(index(Veta,'d=').gt.0) then
            i=index(Veta,'/')
            if(i.eq.idel(Veta)) then
              call Kus(Radka,k,Veta)
            else
              Veta=Veta(i+1:)
            endif
            k=0
            call StToReal(Veta,k,TOFDifcRefBlock(KRefBlock),1,.false.,
     1                    ich)
            if(ich.ne.0) go to 9100
            TOFDifaRefBlock(KRefBlock)=0.
            TOFZeroRefBlock(KRefBlock)=0.
            TOFTThRefBlock(KRefBlock)=-3333.
            go to 1320
          endif
        endif
        go to 1300
1320    rewind 71
1330    read(71,FormA,err=9100,end=9100) Radka
        call Mala(Radka)
        if(Radka.eq.' ') go to 1330
        if(index(Radka,'waves').ne.0.and.index(Radka,'tof').ne.0) then
1340      read(71,FormA,err=9100,end=9100) Radka
          if(EqIgCase(Radka,'begin')) go to 9999
          go to 1340
        endif
        go to 1330
      else if(DifCode(KRefBlock).eq.IdTOFISISFullProf.or.
     1        DifCode(KRefBlock).eq.IdTOFFree) then
        if(TOFDifcRefBlock(KRefBlock).lt.-3000.) then
          TOFDifcRefBlock(KRefBlock)=BankDifc(1)
          TOFJasonRefBlock(KRefBlock)=BankJason(1)
        endif
        if(TOFDifaRefBlock(KRefBlock).lt.-3000.)
     1     TOFDifaRefBlock(KRefBlock)=BankDifa(1)
        if(TOFZeroRefBlock(KRefBlock).lt.-3000.)
     1    TOFZeroRefBlock(KRefBlock)=BankZero(1)
        if(TOFTThRefBlock(KRefBlock).lt.-3000.)
     1    TOFTThRefBlock(KRefBlock)=BankTTh(1)
        if(TOFCERefBlock(KRefBlock).lt.-3000.)
     1    TOFCERefBlock(KRefBlock)=BankCE(1)
        if(TOFZeroERefBlock(KRefBlock).lt.-3000.)
     1    TOFZeroERefBlock(KRefBlock)=BankZeroE(1)
        if(TOFTCrossRefBlock(KRefBlock).lt.-3000.)
     1    TOFTCrossRefBlock(KRefBlock)=BankTCross(1)
        if(TOFWCrossRefBlock(KRefBlock).lt.-3000.)
     1    TOFWCrossRefBlock(KRefBlock)=BankWCross(1)
        if(TOFSigmaRefBlock(1,KRefBlock).lt.-3000.)
     1    call CopyVek(BankSigma(1,1),TOFSigmaRefBlock(1,KRefBlock),3)
        if(TOFGammaRefBlock(1,KRefBlock).lt.-3000.)
     1    call CopyVek(BankGamma(1,1),TOFGammaRefBlock(1,KRefBlock),3)
        if(TOFAlfbeRefBlock(1,KRefBlock).lt.-3000.)
     1    call CopyVek(BankAlfBe(1,1),TOFAlfBeRefBlock(1,KRefBlock),4)
        if(TOFAlfbtRefBlock(1,KRefBlock).lt.-3000.)
     1    call CopyVek(BankAlfBt(1,1),TOFAlfBtRefBlock(1,KRefBlock),4)
      endif
      go to 9999
9100  call FeReadError(71)
9900  ich=1
9999  return
      end
      subroutine PwdImport(ln,Konec,nalloc,ich)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      include 'editm9.cmn'
      dimension ip(4),xp(4)
      integer FeReadBinaryFile,StoeDataLength
      character*512 Radka
      character*1 RadkaP(512)
      character*2   Ch2
      character*4   Ch4
      integer*4 I4
      integer*2 I2
      logical EqIgCase
      equivalence (Ch2,I2),(Ch4,I4,R4)
      equivalence (ip,xp),(Radka,RadkaP)
      xp(3)=0.
      Konec=0
      ich=0
1100  if(DifCode(KRefBlock).eq.IdDataPwdStoe) then
        StoeDataLength=FeReadBinaryFile(LnStoe,RadkaP,512)
        if(StoeDataLength.le.0) go to 5000
      else
        read(ln,FormA,err=9100,end=5000) Radka
        call mala(Radka)
        if(Radka.eq.' '.or.
     1     (Radka(1:1).eq.'#'.and.
     2      DifCode(KRefBlock).ne.IdTOFISISD.and.
     3      (DifCode(KRefBlock).ne.IdTOFISIST.or.ISISGSS)))
     4    go to 1100
      endif
      if(DifCode(KRefBlock).eq.IdDataD1AD2B) then
        k=0
        call kus(Radka,k,Cislo)
        if(Cislo(1:1).eq.'-') go to 1100
      endif
      if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1   DifCode(KRefBlock).eq.IdData11BM.or.
     2   DifCode(KRefBlock).eq.IdTOFGSAS) then
        if(index(Radka,'bank').eq.1.or.
     1     index(Radka,'time_map').eq.1) go to 5000
        i=0
1120    i=i+1
        if(Radka(i:i).eq.' ') go to 1120
        if(index(Cifry(1:10),Radka(i:i)).le.0) go to 1100
        if(TypeBank.eq.'esd'.or.TypeBank.eq.'std'.or.
     1     TypeBank.eq.'alt'.or.TypeBank.eq.'fxye'.or.
     2     TypeBank.eq.' ') then
          if(LRALF) then
            n=idel(Radka)/20
          else if(TypeBank.eq.'fxye') then
            n=1
          else
            n=idel(Radka)/8
            if(TypeBank.eq.'esd') n=n/2
          endif
          if(NPnts+n.gt.nalloc) call PwdImportReallocate(nalloc)
          if(.not.IncSpectFileOpened)
     1      call SetRealArrayTo(YiPwd(NPnts+1),n,0.)
          if(LRALF) then
            if(ln.eq.71) then
              read(Radka,FormatBank)(XPwd(i),YoPwd(i),YsPwd(i),
     1                               i=NPnts+1,NPnts+n)
            else
              read(Radka,FormatBank)(XPwd(i),YiPwd(i),YsPwd(i),
     1                               i=NPnts+1,NPnts+n)
              call SetRealArrayTo(YoPwd(NPnts+1),n,0.)
            endif
            do i=NPnts+1,NPnts+n
              XPwd(i)=XPwd(i)/32000.
            enddo
          else
            if(TypeBank.eq.'esd') then
              if(ln.eq.71) then
                read(Radka,FormatBank)(YoPwd(i),YsPwd(i),
     1                                 i=NPnts+1,NPnts+n)
              else
                read(Radka,FormatBank)(YiPwd(i),YsPwd(i),
     1                                 i=NPnts+1,NPnts+n)
                call SetRealArrayTo(YoPwd(NPnts+1),n,0.)
              endif
              call SetRealArrayTo(XPwd(NPnts+1),n,0.)
            else if(TypeBank.eq.'std'.or.TypeBank.eq.' ') then
              if(ln.eq.71) then
                read(Radka,FormatBank)(ii,YoPwd(i),
     1                                 i=NPnts+1,NPnts+n)
              else
                read(Radka,FormatBank)(ii,YiPwd(i),
     1                                 i=NPnts+1,NPnts+n)
                call SetRealArrayTo(YoPwd(NPnts+1),n,0.)
              endif
              call SetRealArrayTo(XPwd(NPnts+1),n,0.)
              call SetRealArrayTo(YsPwd(NPnts+1),n,0.)
            else if(TypeBank.eq.'fxye') then
              k=0
              call StToReal(Radka,k,xp,3,.false.,ich)
              if(ln.eq.71) then
                XPwd(NPnts+n)=xp(1)*.001
                YoPwd(NPnts+n)=xp(2)
                YsPwd(NPnts+n)=xp(3)
              else
                XPwd(NPnts+n)=xp(1)*.001
                YiPwd(NPnts+n)=xp(2)
                YsPwd(NPnts+n)=xp(3)
              endif
            endif
          endif
          if(DifCode(KRefBlock).eq.IdDataGSAS.or.
     1       DifCode(KRefBlock).eq.IdData11BM) then
            do i=NPnts+1,NPnts+n
              XPwd(i)=deg0+float(i-1)*degs
            enddo
          else
            if(.not.LRALF.and.allocated(NPointsTimeMaps).and.
     1         ln.eq.71) then
              jp=1
              do i=NPnts+1,NPnts+n
                do j=jp,NPointsTimeMaps(NTimeMap)-1
                  if(i.lt.TMapTOF(1,j,NTimeMap)) go to 1230
                enddo
                j=NPointsTimeMaps(NTimeMap)
1230            jp=j-1
                XPwd(i)=float(TMapTOF(2,jp,NTimeMap)+
     1            (i-TMapTOF(1,jp,NTimeMap))*TMapTOF(3,jp,NTimeMap))*
     2            float(ClckWdtTOF(NTimeMap))/1000000.
!                pom=10000./(PwdIncIntFun(XPwd(i),KRefBlock)*
!     1                      float(TMapTOF(3,jp,NTimeMap)))
!                if(YiPwd(i).gt.0.) then
!                  pom=100./(YiPwd(i)*float(TMapTOF(3,jp,NTimeMap)))
!                else
!                  pom=0.
!                endif
                pom=1./TMapTOF(3,jp,NTimeMap)
                YoPwd(i)=pom*YoPwd(i)
                YsPwd(i)=pom*YsPwd(i)
                YiPwd(i)=pom*YiPwd(i)
              enddo
            endif
          endif
          NPnts=NPnts+n
        endif
        go to 9999
      else if(DifCode(KRefBlock).eq.IdDataMAC.or.
     1   DifCode(KRefBlock).eq.IdDataAPS.or.
     2   DifCode(KRefBlock).eq.IdDataFree.or.
     3   DifCode(KRefBlock).eq.IdDataXRDML.or.
     4   DifCode(KRefBlock).eq.IdDataPwdRigaku.or.
     5   DifCode(KRefBlock).eq.IdDataM92.or.
     6   DifCode(KRefBlock).eq.IdTOFISISD.or.
     7   DifCode(KRefBlock).eq.IdTOFISIST.or.
     8   DifCode(KRefBlock).eq.IdTOFISISFullProf.or.
     9   DifCode(KRefBlock).eq.IdTOFFree) then
        if(DifCode(KRefBlock).eq.IdDataM92) then
          k=0
          call StToReal(Radka,k,xp,1,.false.,ich)
          if(ich.ne.0) go to 9100
          if(xp(1).gt.900.) go to 5000
          call StToReal(Radka,k,xp(2),2,.false.,ich)
          if(ich.ne.0) go to 9100
        else if(DifCode(KRefBlock).eq.IdTOFISISD.or.
     1          (DifCode(KRefBlock).eq.IdTOFISIST.and..not.ISISGSS))
     2    then
          if(Radka(1:1).eq.'#') go to 5000
          k=0
          call StToReal(Radka,k,xp,3,.false.,ich)
          if(ich.ne.0) go to 9100
        else if(DifCode(KRefBlock).eq.IdTOFISIST) then
          if(index(Radka,'bank').eq.1) go to 5000
          k=0
          if(JenDve) then
            n=2
            xp(3)=0.
          else
            n=3
          endif
          call StToReal(Radka,k,xp,n,.false.,ich)
          if(ich.ne.0) go to 9100
          xp(1)=xp(1)*.001
        else if(DifCode(KRefBlock).eq.IdTOFFree.or.
     1          DifCode(KRefBlock).eq.IdTOFISISFullProf) then
          if(EqIgCase(Radka,'END')) go to 5000
          k=0
          call StToReal(Radka,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9100
          if(k.lt.len(Radka)) call StToReal(Radka,k,xp(3),1,.false.,ich)
          if(ich.ne.0) go to 9100
          xp(1)=xp(1)*.001
        else
          k=0
          call GetRealLengthPoint(Radka,k,i,j)
          iorderA=max(iorderA,i)
          ipointA=max(ipointA,j)
          call GetRealLengthPoint(Radka,k,i,j)
          iorderI=max(iorderI,i)
          ipointI=max(ipointI,j)
          k=0
          call StToReal(Radka,k,xp,2,.false.,ich)
          if(ich.ne.0) go to 9100
          if(k.lt.len(Radka)) then
            kk=k
            call GetRealLengthPoint(Radka,k,i,j)
            iorderI=max(iorderI,i)
            ipointI=max(ipointI,j)
            k=kk
            call StToReal(Radka,k,xp(3),1,.false.,ich)
          endif
          if(ich.ne.0) go to 9100
        endif
        if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
        Npnts=Npnts+1
        XPwd(Npnts)=xp(1)
        if(DifCode(KRefBlock).eq.IdDataAPS) XPwd(Npnts)=XPwd(Npnts)*.01
        YoPwd(Npnts)=xp(2)
        YiPwd(Npnts)=0.
        YsPwd(Npnts)=xp(3)
      else if(DifCode(KRefBlock).eq.IdTOFVEGA) then
        if(index(Radka,'end').eq.1) go to 5000
        k=0
        call StToReal(Radka,k,xp,4,.false.,ich)
        if(ich.ne.0) go to 9100
        if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
        Npnts=Npnts+1
        XPwd(Npnts)=xp(1)*.001
        pom=10000./xp(4)
        YoPwd(Npnts)=xp(2)*pom
        YiPwd(Npnts)=0.
        YsPwd(Npnts)=sqrt(xp(2)/xp(3))*pom
      else if(DifCode(KRefBlock).eq.IdDataFreeOnlyI.or.
     1        DifCode(KRefBlock).eq.IdDataD1AD2B.or.
     2        DifCode(KRefBlock).eq.IdDataD1AD2BOld.or.
     3        DifCode(KRefBlock).eq.IdDataD1BD20.or.
     4        DifCode(KRefBlock).eq.IdDataSaclay.or.
     5        DifCode(KRefBlock).eq.IdDataPSI.or.
     6        DifCode(KRefBlock).eq.IdDataCPI.or.
     7        DifCode(KRefBlock).eq.IdDataRiet7.or.
     8        DifCode(KRefBlock).eq.IdDataUXD.or.
     9        DifCode(KRefBlock).eq.IdDataPwdHuber.or.
     a        DifCode(KRefBlock).eq.IdDataG41) then
        k=0
        if(DifCode(KRefBlock).eq.IdDataUXD.and.UXDVersion.lt.0)
     1    call Kus(Radka,k,Cislo)
2200    if(DifCode(KRefBlock).eq.IdDataD1AD2B.or.
     1     DifCode(KRefBlock).eq.IdDataD1AD2BOld) then
          read(Radka(k+1:),'(i2,f6.0)',err=9100) i,xp(2)
          if(DifCode(KRefBlock).eq.IdDataD1AD2B.and.
     1       CNormD1AD2B.ne.1.) xp(2)=CNormD1AD2B*xp(2)
          xp(3)=max(i,1)
          k=k+8
        else if(DifCode(KRefBlock).eq.IdDataD1BD20) then
          read(Radka(k+1:),'(i2,f8.0)',err=9100) i,xp(2)
          xp(3)=max(i,1)
          k=k+10
        else if(DifCode(KRefBlock).eq.IdDataSaclay) then
          call StToReal(Radka,k,xp(2),NImpPwd,.false.,ich)
        else if(DifCode(KRefBlock).eq.IdDataRiet7) then
          if(NImpPwd.eq.1) then
            read(Radka(k+1:),'(f8.0)',err=9100) xp(2)
            k=k+8
          else if(NImpPwd.eq.2) then
            read(Radka(k+1:),'(f2.0,f6.0)',err=9100)(xp(4-i),i=1,2)
            k=k+8
          else if(NImpPwd.eq.-2) then
            read(Radka(k+1:),'(f6.0,f10.0)',err=9100)(xp(4-i),i=1,2)
            k=k+16
          endif
        else if(DifCode(KRefBlock).eq.IdDataG41) then
          read(Radka(k+1:),'(f8.1)',err=9100) xp(2)
          if(IVariG41.le.0) xp(3)=sqrt(CNormG41*xp(2))
          k=k+8
        else if(DifCode(KRefBlock).eq.IdDataCPI.or.
     1          DifCode(KRefBlock).eq.IdDataUXD.or.
     2          DifCode(KRefBlock).eq.IdDataFreeOnlyI.or.
     3          DifCode(KRefBlock).eq.IdDataPwdHuber) then
          call StToReal(Radka,k,xp(2),NImpPwd,.false.,ich)
        else if(MaxLength.ne.8) then
          call StToReal(Radka,k,xp(2),NImpPwd,.false.,ich)
          if(ich.ne.0) go to 9100
        else
          read(Radka(k+1:),'(5f8.0)')(xp(i),i=2,NImpPwd+1)
          k=k+MaxLength*NImpPwd
          if(Radka(k+1:).eq.' ') k=256
        endif
        xp(1)=deg
        if(NImpPwd.gt.1.or.NImpPwd.eq.-2) then
          if(DifCode(KRefBlock).eq.IdDataD1AD2B.or.
     1       DifCode(KRefBlock).eq.IdDataD1AD2BOld.or.
     2       DifCode(KRefBlock).eq.IdDataSaclay) then
            xp(3)=sqrt(xp(2)/xp(3))
          else
            xp(3)=0.
          endif
        endif
        if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
        Npnts=Npnts+1
        XPwd(Npnts)=xp(1)
        if(DifCode(KRefBlock).eq.IdDataPSI.or.
     1     (DifCode(KRefBlock).eq.IdDataG41.and.IVariG41.gt.0)) then
          if(PwdInputInt) then
            YoPwd(Npnts)=xp(2)
            YiPwd(Npnts)=0.
            YsPwd(Npnts)=0.
          else
            YsPwd(Npnts)=xp(2)
          endif
          if(Npnts.ge.NPntsMax) then
            if(PwdInputInt) then
              PwdInputInt=.false.
              NPnts=0
            else
              go to 5000
            endif
          endif
        else
          YoPwd(Npnts)=xp(2)
          YiPwd(Npnts)=0.
          YsPwd(Npnts)=xp(3)
        endif
        deg=deg0+float(Npnts)*degs
        if(k.lt.idel(Radka)) go to 2200
      else if(DifCode(KRefBlock).eq.IdEDInel) then
        k=0
3100    call StToReal(Radka,k,xp,1,.false.,ich)
        if(ich.ne.0) go to 9100
        NInel=NInel+1
        if(xp(1).gt.0..or.Npnts.gt.0) then
          if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
          Npnts=Npnts+1
          XPwd(Npnts)=(EDOffsetRefBlock(KRefBlock)+
     1                 EDSlopeRefBlock(KRefBlock)*NInel)*.001
          YoPwd(Npnts)=xp(1)
          YiPwd(Npnts)=0.
          YsPwd(Npnts)=0.
        endif
        if(k.lt.len(Radka)) go to 3100
      else if(DifCode(KRefBlock).eq.IdDataPwdStoe) then
        k=1
        do j=1,StoeDataLength/StoeDataType
          if(Npnts.ge.NPntsMax) go to 5000
          if(Npnts.ge.nalloc) call PwdImportReallocate(nalloc)
          Npnts=Npnts+1
          if(StoeDataType.eq.2) then
            ch2=Radka(k:k+1)
            YoPwd(Npnts)=i2
          else
            ch4=Radka(k:k+3)
            YoPwd(Npnts)=i4
          endif
          XPwd(Npnts)=deg0+(Npnts-1)*degs
          YiPwd(Npnts)=0.
          YsPwd(Npnts)=0.
          k=k+StoeDataType
        enddo
      endif
      go to 9999
5000  Konec=1
      go to 9999
9100  call FeReadError(ln)
      ich=1
9999  return
      end
      function PwdIncIntFun(xx,n)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension pa(12)
      data nnn/0/
      if(BankITyp(n).le.2) then
        PwdIncIntFun=BankICoff(1,n)
        m=2
        xxp=1.
        do i=1,5
          xxp=xxp*xx
          if(BankITyp(n).eq.2.and.i.eq.1) then
            PwdIncIntFun=PwdIncIntFun+BankICoff(m,n)/xx**5*
     1                                exp(-BankICoff(m+1,n)/xx**2)
          else
            PwdIncIntFun=PwdIncIntFun+BankICoff(m,n)*
     1                                exp(-BankICoff(m+1,n)*xxp)
          endif
          m=m+2
        enddo
      else if(BankITyp(n).eq.3.or.BankITyp(n).eq.5) then
        if(BankITyp(n).eq.3) then
          xxp=2./xx-1.
        else
          xxp=xx/10.
        endif
        PwdIncIntFun=Chebev(BankICoff(m,n),pa,xxp,12)
      else if(BankITyp(n).eq.4) then
        xxp=2./xx-1.
        if(xxp.lt.-1.) xxp=-1.
        if(xxp.gt. 1.) xxp= 1.
        PwdIncIntFun=BankICoff(1,n)+BankICoff(2,n)/xx**5*
     1                              exp(-BankICoff(3,n)/xx**2)+
     2                              Chebev(BankICoff(4,n),pa,xxp,9)
      else
        PwdIncIntFun=1.
      endif
      return
      end
      subroutine PwdMakeFormat92
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer Exponent10
      ipointA=0
      iorderA=0
      ipointI=0
      iorderI=0
      do i=Npnts,1,-1
        Deg=XPwd(i)
        iorderA=max(iorderA,Exponent10(Deg))
        j=ipointA
3030    if(iorderA+j.gt.6) go to 3040
        pom=Deg*10.**j
        if(abs(pom-anint(pom)).gt..05) then
          j=j+1
          go to 3030
        endif
3040    ipointA=max(ipointA,j)
        Fobs=YoPwd(i)
        if(Fobs.le.0.) cycle
        iorderI=max(Exponent10(Fobs),iorderI)
        j=ipointI
3050    if(iorderI+j.gt.6) go to 3060
        pom=Fobs*10.**j
        if(abs(pom-anint(pom)).gt..05) then
          j=j+1
          go to 3050
        endif
3060    ipointI=max(ipointI,j)
        Ys=YsPwd(i)
        if(Ys.le.0.) go to 3100
        iorderI=max(Exponent10(Ys),iorderI)
        j=ipointI
        if(iorderI+j.gt.6) go to 3080
        pom=Ys*10.**j
        if(abs(pom-anint(pom)).gt..05) then
          j=j+1
          go to 3060
        endif
3080    ipointI=max(ipointI,j)
3100    Ys=YiPwd(i)
        if(Ys.le.0.) cycle
        iorderI=max(Exponent10(Ys),iorderI)
        j=ipointI
3120    if(iorderI+j.gt.6) go to 3180
        pom=Ys*10.**j
        if(abs(pom-anint(pom)).gt..05) then
          j=j+1
          go to 3120
        endif
3180    ipointI=max(ipointI,j)
      enddo
!      if(iorderA+ipointA.gt.6) ipointA=6-iorderA
!      if(iorderI+ipointI.gt.6) ipointI=6-iorderI
5000   write(Format92,'(''(f'',i2,''.'',i1,'',3f'',i2,''.'',i1,'')'')')
     1  ipointA+iorderA+3,ipointA,ipointI+iorderI+3,ipointI
      return
      end
      subroutine PwdPutRecordToM95(ln)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      call PwdIncSpectNorm(1)
      do i=1,Npnts
        if(UseIncSpect(KDatBlock).eq.1) then
          write(ln,Format92) XPwd(i),YoPwd(i),YsPwd(i),YiPwd(i)
        else
          write(ln,Format92) XPwd(i),YoPwd(i),YsPwd(i)
        endif
      enddo
      NLines95(KRefBlock)=Npnts
      NRef95(KRefBlock)=Npnts
      KDatB=RefDatCorrespond(KRefBlock)
      if(KDatB.gt.0) then
        if(NManBackg(KDatB).gt.0) then
          write(ln,FormA) 'manual_background'
          do i=1,NManBackg(KDatB)
            write(ln,Format92) XManBackg(i,KDatB),
     1                         YManBackg(i,KDatB)
          enddo
        endif
      endif
      call PwdIncSpectNorm(0)
      return
      end
      subroutine PwdIncSpectNorm(Klic)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      if(UseIncSpect(KDatBlock).ne.1.or.
     1   (Klic.eq.0.eqv.IncSpectNorm)) go to 9999
      do i=1,Npnts
        if(Klic.eq.0) then
          if(YiPwd(i).gt.0.) then
            pom=1./YiPwd(i)
          else
            pom=0.
          endif
        else
          pom=YiPwd(i)
        endif
        YoPwd(i)=YoPwd(i)*pom
        YsPwd(i)=YsPwd(i)*pom
      enddo
      IncSpectNorm=Klic.eq.0
9999  return
      end
      subroutine PwdImportManBackg
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      character*80 Veta
      character*12 men(2)
      character*4  mene(2)
      data men/'%GSAS','Fr%ee format'/
      data mene/'.gbg','.dat'/
      character*256 EdwStringQuest
      character*80 FileNameIn
      character*5  t5
      dimension xp(2)
      integer FileInType
      logical NameFromBrowser,ExistFile
      lni=0
      ntypein=2
      NameFromBrowser=.false.
      FileNameIn=' '
      id=NextQuestId()
      xqd=300.
      call FeQuestCreate(id,-1.,-1.,xqd,ntypein+1,'Specify input '//
     1                   'background file',1,LightGray,0,0)
      FileInType=1
      tpom=5.
      xpom=70.
      do i=1,ntypein
        call FeQuestCrwMake(id,tpom,i+1,xpom,i+1,men(i),'L',CrwgXd,
     1                      CrwgYd,1,1)
        if(i.eq.1) ncrwTypeFr=CrwLastMade
        if(i.eq.ntypein) ncrwTypeTo=CrwLastMade
        call FeQuestCrwOpen(i,i.eq.FileInType)
      enddo
      Veta='%Browse'
      dpom=FeTxLengthUnder(Veta)+10.
      xpom=xqd-dpom-5.
      call FeQuestButtonMake(id,xpom,1,dpom,ButYd,Veta)
      nButtonBrowse=ButtonLastMade
      FileNameIn=fln(:ifln)//mene(1)
      Veta='File %name'
      dpom=xpom-20.
      xpom=tpom+FeTxLengthUnder(Veta)+10
      dpom=dpom-xpom
      call FeQuestEdwMake(id,tpom,1,xpom,1,Veta,'L',dpom,EdwYd,0)
      nEdwFileIn=EdwLastMade
      call FeQuestStringEdwOpen(nEdwFileIn,FileNameIn)
      call FeQuestButtonOpen(nButtonBrowse,ButtonOff)
1015  if(.not.NameFromBrowser) FileNameIn=fln(:ifln)//mene(FileInType)
      call FeQuestStringEdwOpen(nEdwFileIn,FileNameIn)
1020  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumberAbs.eq.ButtonOK) then
        FileNameIn=EdwStringQuest(nEdwFileIn)
        if(.not.ExistFile(FileNameIn)) then
          call FeChybne(-1.,-1.,'the file "'//
     1                  FileNameIn(:idel(FileNameIn))//'" doesn''t'//
     2                  ' exist','try again.',SeriousError)
          go to 1020
        endif
        QuestCheck(id)=0
        go to 1020
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.ncrwTypeFr.and.
     1                                  CheckNumber.le.ncrwTypeTo) then
        FileInType=CheckNumber
        go to 1015
      else if(CheckType.eq.EventButton.and.CheckNumber.ge.nButtonBrowse)
     1  then
        t5='*'//mene(FileInType)
        call FeFileManager('Select reflection file',FileNameIn,t5,0,
     1                     .true.,ich)
        if(ich.le.0) then
          NameFromBrowser=FileNameIn.ne.' '
          go to 1015
        else
          FileNameIn=EdwStringQuest(nEdwFileIn)
          go to 1020
        endif
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1020
      endif
      if(ich.eq.0) FileNameIn=EdwStringQuest(nEdwFileIn)
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call iom95(0,fln(:ifln)//'.m95')
      call PwdM92Nacti
      lni=NextLogicNumber()
      call OpenFile(lni,FileNameIn,'formatted','old')
      if(ErrFlag.ne.0) go to 8000
      if(.not.allocated(XManBackg))
     1   allocate(XManBackg(100,NDatBlock),YManBackg(100,NDatBlock))
      NManBackg(KDatBlock)=0
1000  read(lni,FormA,err=9100,end=2000) Veta
      if(Veta.eq.' ') go to 1000
      k=0
      if(FileInType.eq.1) then
        call kus(Veta,k,Cislo)
      else
        call DeleteFirstSpaces(Veta)
        if(index(Cifry,Veta(1:1)).le.0) go to 1000
      endif
      call StToReal(Veta,k,xp,2,.false.,ich)
      n=NManBackg(KDatBlock)+1
      call ReallocManBackg(n)
      NManBackg(KDatBlock)=n
      XManBackg(NManBackg(KDatBlock),KDatBlock)=xp(1)
      YManBackg(NManBackg(KDatBlock),KDatBlock)=xp(2)
      go to 1000
2000  write(Cislo,'(''.l'',i2)') KRefBlock
      if(Cislo(3:3).eq.' ') Cislo(3:3)='0'
      RefBlockFileName=fln(:ifln)//Cislo(:idel(Cislo))
      call DeleteFile(RefBlockFileName)
      call OpenFile(95,RefBlockFileName,'formatted','unknown')
      call PwdPutRecordToM95(95)
      ModifiedRefBlock(KRefBlock)=.true.
      call CloseIfOpened(95)
      call CompleteM95(0)
      call EM9CreateM90Powder(ich)
      call CompleteM90
      go to 9999
8000  ErrFlag=0
      go to 9999
9100  call FeReadError(lni)
9999  call CloseIfOpened(lni)
      return
103   format(i6)
      end
      function PwdManualBackground(tth)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      if(NManBackg(KDatBlock).le.1) then
        PwdManualBackground=0.
        go to 9999
      endif
      do ik=2,NManBackg(KDatBlock)
        if(tth.lt.XManBackg(ik,KDatBlock)) go to 2000
      enddo
      ik=NManBackg(KDatBlock)
2000  ip=ik-1
      PwdManualBackground=YManBackg(ip,KDatBlock)+
     1  (YManBackg(ik,KDatBlock)-YManBackg(ip,KDatBlock))/
     2  (XManBackg(ik,KDatBlock)-XManBackg(ip,KDatBlock))*
     3  (tth-XManBackg(ip,KDatBlock))
9999  return
      end
      subroutine PwdGetAllBackground
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension derp(36)
      call PwdSetBackground
      do i=1,npnts
        call PwdGetBackground(XPwd(i),YbPwd(i),derp,DerSc)
      enddo
      return
      end
      subroutine PwdBasicCIF(ln,Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      dimension ifc(3),ra(8)
      character*256 Radka,Veta
      integer Possibilities(3)
      logical EqIgCase,AlreadyAllocated
      save ifc,NData,mm,AlreadyAllocated
      data Possibilities/114,130,147/
      if(allocated(CifKey)) then
        AlreadyAllocated=.true.
      else
        AlreadyAllocated=.false.
        allocate(CifKey(400,40),CifKeyFlag(400,40))
        call NactiCifKeys(CifKey,CifKeyFlag,0)
        if(ErrFlag.ne.0) go to 9999
      endif
      if(ln.eq.0) then
        mm=0
      else
        rewind ln
      endif
1000  if(ln.eq.0) then
        mm=mm+1
        if(mm.gt.nCIFUsed) go to 9000
        Radka=CIFArray(mm)
      else
        read(ln,FormA,end=9000) Radka
      endif
      k=0
      call kus(Radka,k,Veta)
      call mala(Veta)
      if(Veta.eq.'loop_') then
        NData=0
        call SetIntArrayTo(ifc,3,0)
1100    if(ln.eq.0) then
          mm=mm+1
          if(mm.gt.nCIFUsed) go to 9000
          Radka=CIFArray(mm)
        else
          read(ln,FormA,end=9000) Radka
        endif
        k=0
        call kus(Radka,k,Veta)
        if(Veta.eq.' ') go to 1100
        i=1
1110    if(Veta(i:i).eq.' ') then
          i=i+1
          go to 1110
        endif
        Veta=Veta(i:)
        call mala(Veta)
        if(Veta(1:1).ne.'_') go to 1200
        NData=NData+1
        do i=1,3
          if(EqIgCase(Veta,CifKey(Possibilities(i),20))) ifc(i)=NData
        enddo
        go to 1100
1200    n=0
        do i=1,3
          if(ifc(i).ne.0) n=n+1
        enddo
        if(n.le.0) then
          go to 1000
        else if(n.lt.3) then
          go to 9100
        endif
        if(NData.gt.0) then
          if(ln.eq.0) then
            mm=mm-1
          else
            backspace ln
          endif
          go to 9999
        endif
      endif
      go to 1000
      entry PwdReadCIF(ln,Klic,ik)
      ik=0
2000  if(ln.eq.0) then
        mm=mm+1
        if(mm.gt.nCIFUsed) go to 3000
        Radka=CIFArray(mm)
      else
        read(ln,FormA,end=3000) Radka
      endif
      if(Radka.eq.' ') go to 2000
      k=0
      call kus(Radka,k,Veta)
      if(Veta(1:1).eq.'_'.or.index(Veta,'data_').eq.1.or.
     1                       index(Veta,'loop_').eq.1) then
        ik=1
      else if(Veta(1:1).eq.'#') then
        go to 2000
      else
        k=0
        call StToReal(Radka,k,ra,NData,.false.,ich)
        if(ich.ne.0) go to 9900
        ih(1)=nint(ra(ifc(1))*1000000.)
        ri=ra(ifc(2))
        rs=ra(ifc(3))
      endif
      go to 9999
3000  ik=1
      go to 9999
9000  Veta='before the end of CIF file is reached.'
      go to 9900
9100  Veta='or some of items h, k, l, I, sig(I) is missing.'
9900  if(Klic.eq.0) then
        Radka='CIF file does not contain any reflection block'
        call FeChybne(-1.,-1.,Radka,Veta,SeriousError)
      endif
      ErrFlag=1
9999  if(allocated(CifKey).and..not.AlreadyAllocated)
     1   deallocate(CifKey,CifKeyFlag)
      return
      end
      subroutine PwdM92Nacti
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension xp(4),ipoint(4),length(4)
      character*80 Veta
      logical   MamCislo,EqIgCase
      equivalence (xp,ipoint)
      ln=NextLogicNumber()
      call OpenDatBlockM90(ln,KDatBlock,fln(:ifln)//'.m90')
      nn=0
      n=4
1000  read(ln,FormA,err=9000,end=1200) Veta
      if(Veta.eq.' ') go to 1000
      if(EqIgCase(Veta,'manual_background')) then
        n=2
        go to 1200
      endif
      k=0
      call kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'data')) go to 1200
      nn=nn+1
      if(nn.eq.1) then
        k=0
        MamCislo=.false.
        call SetIntArrayTo(ipoint,4,-1)
        ip=0
        do i=1,idel(Veta)+1
          if(index(Cifry(1:11),Veta(i:i)).gt.0) then
            if(.not.MamCislo) then
              MamCislo=.true.
              k=k+1
              if(k.gt.4) go to 1105
            endif
            if(Veta(i:i).eq.'.') ipoint(k)=i-ip
          else if(Veta(i:i).eq.' '.or.Veta(i:i).eq.'-') then
            if(MamCislo) then
              length(k)=i-1-ip
              if(ipoint(k).lt.0) then
                ipoint(k)=0
              else
                ipoint(k)=length(k)-ipoint(k)
              endif
              ip=i-1
              MamCislo=.false.
            endif
          endif
        enddo
        if(ipoint(4).lt.0) then
          ipoint(4)=ipoint(3)
          length(4)=length(3)
        endif
1105    format92='(f__.__,f__.__,f__.__,f__.__)'
        FormatSkipFrTo(KDatBlock)='(f__.__,1x,f__.__)'
        PrfPointsFormat='(f__.__,3e15.6,f__.__,i5,99e15.6)'
        k=3
        do i=1,4
          write(format92(k:k+1),100) length(i)
          k=k+3
          write(format92(k:k+1),100) ipoint(i)
          k=k+4
        enddo
        OrderSkipFrTo(KDatBlock)=ipoint(1)
        call zhusti(format92)
        write(FormatSkipFrTo(KDatBlock)(3:4),100) length(1)
        write(FormatSkipFrTo(KDatBlock)(6:7),100) ipoint(1)
        write(FormatSkipFrTo(KDatBlock)(13:14),100) length(1)
        write(FormatSkipFrTo(KDatBlock)(16:17),100) ipoint(1)
        call zhusti(FormatSkipFrTo(KDatBlock))
        write(PrfPointsFormat(3:4),100) length(1)
        write(PrfPointsFormat(6:7),100) ipoint(1)
        write(PrfPointsFormat(17:18),100) length(1)
        write(PrfPointsFormat(20:21),100) ipoint(1)
        call zhusti(PrfPointsFormat)
      endif
      go to 1000
1200  call CloseIfOpened(ln)
      call OpenDatBlockM90(ln,KDatBlock,fln(:ifln)//'.m90')
      if(allocated(YoPwd)) deallocate(XPwd,YoPwd,YsPwd,YiPwd)
      if(allocated(YcPwd)) deallocate(YcPwd,YbPwd,YfPwd,YcPwdInd)
      allocate(XPwd(nn),YoPwd(nn),YcPwd(nn),YbPwd(nn),YsPwd(nn),
     1         YfPwd(nn),YiPwd(nn),YcPwdInd(nn,NPhase))
      NPnts=0
      NManBackg(KDatBlock)=0
      if(UseIncSpect(KDatBlock).eq.1) then
        n=4
      else
        n=3
      endif
      if(allocated(XManBackg)) deallocate(XManBackg,YManBackg)
1500  read(ln,FormA,err=9000,end=2000) Veta
      k=0
      call kus(Veta,k,Cislo)
      if(EqIgCase(Cislo,'data')) go to 2000
      if(EqIgCase(Veta,'manual_background')) then
        n=2
        if(.not.allocated(XManBackg))
     1     allocate(XManBackg(100,NDatBlock),
     2              YManBackg(100,NDatBlock))
        go to 1500
      endif
      k=0
      if(Veta.eq.' ') go to 1500
      k=0
      call StToReal(Veta,k,xp,n,.false.,ich)
      if(xp(1).le.0.) go to 1500
      if(ich.ne.0) go to 9000
      if(n.ge.3) then
        Npnts=Npnts+1
        XPwd(Npnts)=xp(1)
        YoPwd(Npnts)=xp(2)
        if(xp(3).le.0.) then
          YsPwd(Npnts)=sqrt(abs(xp(2)))
        else
          YsPwd(Npnts)=xp(3)
        endif
        if(n.eq.4) then
          YiPwd(Npnts)=xp(4)
        else
          YiPwd(Npnts)=0.
        endif
        go to 1500
      else
        nn=NManBackg(KDatBlock)+1
        call ReallocManBackg(nn)
        NManBackg(KDatBlock)=nn
        XManBackg(NManBackg(KDatBlock),KDatBlock)=xp(1)
        YManBackg(NManBackg(KDatBlock),KDatBlock)=xp(2)
      endif
      go to 1500
2000  call PwdMakeSkipFlags
      DegStepPwd=(XPwd(NPnts)-XPwd(1))/float(Npnts-1)
      RadStepPwd=DegStepPwd*ToRad
      IncSpectNorm=.false.
      call PwdIncSpectNorm(0)
      go to 9999
9000  call FeReadError(ln)
9900  ErrFlag=1
9999  call CloseIfOpened(ln)
      return
100   format(i2)
      end
      subroutine PwdSetFundamentalParameters
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      RadPrim(KDatBlock)=173.
      RadSec(KDatBlock)=173.
      AsymPwd(1,KDatBlock)=.2
      if(PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS) then
        AsymPwd(2,KDatBlock)=10.
        KUseDS(KDatBlock)=2
      else
        AsymPwd(2,KDatBlock)=1.
        KUseDS(KDatBlock)=1
      endif
      AsymPwd(3,KDatBlock)=12.
      AsymPwd(4,KDatBlock)=15.
      AsymPwd(5,KDatBlock)=12.
      AsymPwd(6,KDatBlock)=5.1
      AsymPwd(7,KDatBlock)=5.1
      KUseRSW(KDatBlock)=1
      KUsePSoll(KDatBlock)=1
      KUseSSoll(KDatBlock)=1
      return
      end
      subroutine PwdSkipFrTo
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/SkipFrToQuest/ nEdwFirst,ThFrTo(2),Prazdno
      dimension xp(2)
      character*80 Veta,FileName
      character*10 Label
      integer WhatHappened
      logical Prazdno,Enabled,EqIgCase
      external PwdSkipFrToRead,PwdSkipFrToWrite,FeVoid
      save /SkipFrToQuest/
      FileName=fln(:ifln)//'_skipfrto.tmp'
      ln=NextLogicNumber()
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      do i=1,NSkipPwd(KDatBlock)
        if(SkipPwdFr(i,KDatBlock).gt.-180.) then
          write(Veta,FormatSkipFrTo(KDatBlock))
     1      SkipPwdFr(i,KDatBlock),SkipPwdTo(i,KDatBlock)
          Label='skipfrto'
        else
          write(Veta,FormatSkipFrTo(KDatBlock))
     1      -SkipPwdFr(i,KDatBlock)-200.,-SkipPwdTo(i,KDatBlock)-200.
          Label='!skipfrto'
        endif
        k=0
        call WriteLabeledRecord(ln,Label,Veta,k)
      enddo
      close(ln)
      xqd=400.
      il=1
      call RepeatCommandsProlog(id,FileName,xqd,il,ilp,0)
      il=ilp+1
      tpom=5.
      Veta='fro%m'
      xpom=FeTxLengthUnder(Veta)+5.
      do i=1,2
        call FeQuestEdwMake(id,tpom,il,tpom+xpom,il,Veta,'L',60.,EdwYd,
     1                      0)
        tpom=tpom+100.
        if(i.eq.1) then
          nEdwFirst=EdwLastMade
          nEdwRepeatCheck=EdwLastMade
          Veta='%to'
        endif
      enddo
      call FeQuestButtonLabelChange(nButtRepeatEdit,'%Edit')
1300  call SetRealArrayTo(ThFrTo,2,0.)
      Prazdno=.true.
1350  nEdw=nEdwFirst
      do i=1,2
        call FeQuestRealEdwOpen(nEdw,ThFrTo(i),Prazdno,.false.)
        nEdw=nEdw+1
      enddo
2000  MakeExternalCheck=0
      call RepeatCommandsEvent(id,ich,WhatHappened,
     1  PwdSkipFrToRead,PwdSkipFrToWrite,FeVoid,FeVoid)
      if(WhatHappened.eq.RepeatCommandWrittenOut) then
        go to 1300
      else if(WhatHappened.eq.RepeatCommandReadIn) then
        go to 1350
      endif
      if(CheckType.eq.EventCrw) then
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 2000
      endif
      call RepeatCommandsEpilog(id,ich)
      if(ich.ge.10) then
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 2000
      endif
      if(ich.eq.0) then
        n=0
        call OpenFile(ln,FileName,'formatted','unknown')
3000    read(ln,FormA,end=3100) Veta
        k=0
        call kus(Veta,k,Label)
        if(EqIgCase(Label,'skipfrto')) then
          n=n+1
          Enabled=.true.
        else if(EqIgCase(Label,'!skipfrto')) then
          n=n+1
          Enabled=.false.
        else
          go to 3000
        endif
        call StToReal(Veta,k,xp,2,.false.,ich)
        if(ich.eq.0) then
          SkipPwdFr(n,KDatBlock)=xp(1)
          SkipPwdTo(n,KDatBlock)=xp(2)
          if(.not.Enabled) then
            SkipPwdFr(n,KDatBlock)=-SkipPwdFr(n,KDatBlock)-200.
            SkipPwdTo(n,KDatBlock)=-SkipPwdTo(n,KDatBlock)-200.
          endif
        else
          n=n-1
        endif
        go to 3000
3100    NSkipPwd(KDatBlock)=n
        call CloseIfOpened(ln)
        call DeleteFile(FileName)
      endif
9999  return
      end
      subroutine PwdSkipFrToRead(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      common/SkipFrToQuest/ nEdwFirst,ThFrTo(2),Prazdno
      character*80 t80
      character*(*) Command
      logical   Prazdno
      save /SkipFrToQuest/
      ich=0
      if(Command.eq.' '.or.Command(1:1).eq.'#'.or.Command(1:1).eq.'*')
     1  then
        go to 9999
      endif
      lenc=Len(Command)
      k=0
      call kus(Command,k,t80)
      call mala(t80)
      if(t80.ne.'skipfrto'.and.t80.ne.'!skipfrto') go to 8010
      do i=1,2
        if(k.ge.lenc) go to 8000
        call kus(Command,k,t80)
        kk=0
        call StToReal(t80,kk,ThFrTo(i),1,.false.,ich)
        if(ich.ne.0) go to 8040
      enddo
      Prazdno=.false.
      go to 9999
8000  t80='the command is too short'
      go to 8100
8010  t80='incorrect command "'//t80(:idel(t80))//'"'
      go to 8100
8040  t80='incorrect real "'//t80(:idel(t80))//'"'
      go to 8100
8100  ich=1
      call FeChybne(-1.,-1.,t80,'Edit or delete the relevant command.',
     1              SeriousError)
9999  return
      end
      subroutine PwdSkipFrToWrite(Command,ich)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/SkipFrToQuest/ nEdwFirst,ThFrTo(2),Prazdno
      character*(*) Command
      character*256 EdwStringQuest
      logical   Prazdno
      save /SkipFrToQuest/
      ich=0
      Command='skipfrto'
      nEdw=nEdwFirst
      do i=1,2
        if(EdwStringQuest(nedw).eq.' ') go to 9100
        call FeQuestRealFromEdw(nEdw,pom)
        write(Cislo,FormatSkipFrTo(KDatBlock)) pom
        call ZdrcniCisla(Cislo,1)
        Command=Command(:idel(Command)+1)//Cislo(:idel(Cislo))
        nEdw=nEdw+1
      enddo
      go to 9999
9100  ich=1
9999  return
      end
      subroutine PwdPrfNic
      include 'fepc.cmn'
      return
      end
      function PwdAbsorption(tth)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      include 'powder.cmn'
      dimension AbsVal(19,81),aa(7,19),yy(2)
      data ((absval(i,j),i=1,19),j=1,49)/
     1 1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     2 1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,
     3 1.18,1.18,1.18,1.18,1.18,1.18,1.18,1.18,1.18,1.18,
     4 1.18,1.18,1.18,1.18,1.18,1.18,1.18,1.18,1.18,
     5 1.40,1.40,1.40,1.40,1.40,1.40,1.39,1.39,1.39,1.39,
     6 1.39,1.38,1.38,1.38,1.38,1.38,1.37,1.37,1.37,
     7 1.65,1.65,1.65,1.65,1.65,1.64,1.64,1.63,1.63,1.62,
     8 1.62,1.61,1.61,1.60,1.59,1.59,1.59,1.59,1.59,
     9 1.95,1.95,1.95,1.94,1.94,1.93,1.92,1.91,1.90,1.89,
     a 1.87,1.86,1.85,1.84,1.83,1.82,1.82,1.81,1.81,
     1 2.29,2.29,2.29,2.28,2.27,2.26,2.24,2.22,2.20,2.18,
     2 2.16,2.13,2.12,2.10,2.08,2.07,2.06,2.05,2.05,
     3 2.69,2.69,2.69,2.67,2.65,2.63,2.60,2.57,2.53,2.50,
     4 2.47,2.43,2.40,2.37,2.35,2.33,2.31,2.30,2.30,
     5 3.16,3.16,3.15,3.13,3.09,3.05,3.01,2.96,2.91,2.85,
     6 2.80,2.75,2.71,2.66,2.63,2.60,2.58,2.56,2.56,
     7 3.70,3.70,3.68,3.65,3.60,3.54,3.47,3.39,3.32,3.24,
     8 3.16,3.09,3.03,2.97,2.92,2.88,2.85,2.84,2.83,
     9 4.33,4.33,4.30,4.24,4.17,4.08,3.98,3.87,3.76,3.65,
     a 3.55,3.46,3.37,3.29,3.23,3.18,3.14,3.11,3.11,
     1 5.06,5.05,5.01,4.92,4.81,4.68,4.54,4.39,4.24,4.10,
     2 3.97,3.84,3.73,3.63,3.55,3.48,3.43,3.40,3.39,
     3 5.90,5.88,5.81,5.69,5.54,5.36,5.16,4.96,4.76,4.58,
     4 4.40,4.24,4.10,3.97,3.87,3.79,3.73,3.69,3.68,
     5 6.86,6.84,6.74,6.57,6.35,6.10,5.84,5.57,5.32,5.08,
     6 4.86,4.66,4.49,4.33,4.20,4.11,4.03,3.98,3.97,
     7 7.96,7.93,7.79,7.55,7.25,6.92,6.58,6.23,5.91,5.61,
     8 5.34,5.09,4.88,4.70,4.54,4.43,4.34,4.28,4.27,
     9 9.23,9.18,8.97,8.65,8.25,7.82,7.37,6.94,6.53,6.16,
     a 5.83,5.54,5.29,5.07,4.89,4.75,4.65,4.58,4.57,
     1 10.7,10.6,10.3,9.88,9.35,8.79,8.22,7.68,7.19,6.74,
     2 6.35,6.00,5.71,5.45,5.24,5.08,4.96,4.89,4.87,
     3 12.3,12.2,11.8,11.2,10.6,9.84,9.13,8.47,7.87,7.34,
     4 6.87,6.47,6.13,5.84,5.60,5.42,5.28,5.19,5.17,
     5 14.2,14.0,13.5,12.7,11.9,11.0,10.1,9.30,8.58,7.96,
     6 7.42,6.95,6.57,6.23,5.96,5.75,5.60,5.50,5.48,
     7 16.3,16.0,15.4,14.4,13.3,12.2,11.1,10.2,9.32,8.59,
     8 7.97,7.45,7.01,6.63,6.33,6.09,5.92,5.81,5.78,
     9 18.6,18.3,17.5,16.2,14.8,13.5,12.2,11.1,10.1,9.25,
     a 8.54,7.94,7.45,7.03,6.69,6.44,6.24,6.12,6.09,
     1 21.3,20.9,19.8,18.2,16.5,14.8,13.3,12.0,10.9,9.91,
     2 9.12,8.45,7.91,7.44,7.06,6.78,6.56,6.43,6.40,
     3 24.2,23.7,22.3,20.3,18.2,16.2,14.5,12.9,11.7,10.6,
     4 9.71,8.97,8.36,7.84,7.44,7.13,6.89,6.75,6.71,
     5 27.5,26.9,25.1,22.6,20.1,17.7,15.7,13.9,12.5,11.3,
     6 10.3,9.49,8.82,8.26,7.81,7.47,7.22,7.06,7.02,
     7 31.2,30.4,28.1,25.1,22.0,19.3,16.9,14.9,13.3,12.0,
     8 10.9,10.0,9.29,8.67,8.19,7.82,7.54,7.38,7.33,
     9 35.3,34.2,31.4,27.7,24.1,20.9,18.2,16.0,14.2,12.7,
     a 11.5,10.5,9.76,9.09,8.57,8.17,7.87,7.69,7.64,
     1 39.8,38.5,34.9,30.5,26.2,22.5,19.5,17.0,15.0,13.4,
     2 12.1,11.1,10.2,9.51,8.95,8.53,8.20,8.01,7.96,
     3 44.7,43.1,38.7,33.4,28.5,24.2,20.8,18.1,15.9,14.2,
     4 12.8,11.6,10.7,9.93,9.33,8.88,8.53,8.33,8.27,
     5 50.1,48.1,42.8,36.5,30.8,26.0,22.2,19.2,16.8,14.9,
     6 13.4,12.2,11.2,10.4,9.72,9.23,8.87,8.64,8.58,
     7 56.1,53.5,47.2,39.7,33.2,27.8,23.6,20.3,17.7,15.6,
     8 14.0,12.7,11.7,10.8,10.1,9.59,9.20,8.96,8.90,
     9 62.5,59.4,51.8,43.1,35.7,29.6,25.0,21.4,18.6,16.4,
     a 14.6,13.2,12.1,11.2,10.5,9.95,9.53,9.28,9.21,
     1 69.5,65.8,56.7,46.6,38.2,31.5,26.4,22.5,19.5,17.1,
     2 15.3,13.8,12.6,11.6,10.9,10.3,9.86,9.60,9.53,
     3 77.2,72.6,61.8,50.3,40.8,33.4,27.9,23.7,20.4,17.9,
     4 15.9,14.3,13.1,12.1,11.3,10.7,10.2,9.92,9.84,
     5 85.4,79.9,67.3,54.0,43.5,35.4,29.3,24.8,21.4,18.7,
     6 16.6,14.9,13.6,12.5,11.7,11.0,10.5,10.2,10.2,
     7 94.2,87.6,72.9,57.9,46.2,37.3,30.8,26.0,22.3,19.4,
     8 17.2,15.5,14.1,12.9,12.0,11.4,10.9,10.6,10.5,
     9 104.,95.9,78.9,61.9,48.9,39.3,32.3,27.1,23.2,20.2,
     a 17.9,16.0,14.6,13.4,12.4,11.7,11.2,10.9,10.8,
     1 114.,105.,85.0,65.9,51.7,41.3,33.8,28.3,24.2,21.0,
     2 18.5,16.6,15.1,13.8,12.8,12.1,11.5,11.2,11.1,
     3 125.,114.,91.4,70.1,54.6,43.3,35.5,29.5,25.1,21.7,
     4 19.2,17.1,15.6,14.2,13.2,12.5,11.9,11.5,11.4,
     5 136.,124.,98.0,74.4,57.4,45.4,36.9,30.7,26.1,22.5,
     6 19.8,17.7,16.1,14.7,13.6,12.8,12.2,11.8,11.7,
     7 149.,134.,105.,78.7,60.4,47.5,38.4,31.9,27.0,23.3,
     8 20.5,18.3,16.6,15.1,14.0,13.2,12.6,12.2,12.1,
     9 162.,145.,112.,83.1,63.3,49.5,39.9,33.1,28.0,24.1,
     a 21.2,18.8,17.1,15.6,14.4,13.6,12.9,12.5,12.4,
     1 175.,156.,119.,87.6,66.3,51.6,41.5,34.3,28.9,24.9,
     2 21.8,19.4,17.6,16.0,14.8,13.9,13.2,12.8,12.7,
     3 190.,168.,127.,92.1,69.3,53.8,43.1,35.5,29.9,25.7,
     4 22.5,20.0,18.1,16.4,15.2,14.3,13.6,13.1,13.0,
     5 206.,180.,134.,96.7,72.3,55.9,44.6,36.7,30.9,26.5,
     6 23.2,20.6,18.6,16.9,15.6,14.6,13.9,13.5,13.3,
     7 222.,193.,142.,101.,75.4,58.0,46.2,37.9,31.8,27.3,
     8 23.8,21.1,19.1,17.3,16.0,15.0,14.3,13.8,13.7,
     9 239.,206.,150.,106.,78.5,60.2,47.8,39.1,32.8,28.1,
     a 24.5,21.7,19.6,17.8,16.4,15.4,14.6,14.1,14.0,
     1 257.,220.,158.,111.,81.6,62.3,49.4,40.4,33.8,28.9,
     2 25.2,22.3,20.1,18.2,16.8,15.7,14.9,14.4,14.3,
     3 275.,234.,166.,116.,84.7,64.5,51.0,41.6,34.8,29.7,
     4 25.8,22.9,20.6,18.7,17.2,16.1,15.3,14.8,14.6,
     5 295.,249.,175.,121.,87.8,66.7,52.6,42.8,35.8,30.5,
     6 26.5,23.4,21.1,19.1,17.6,16.5,15.6,15.1,14.9,
     7 316.,264.,183.,125.,91.0,68.9,54.2,44.1,36.7,31.3,
     8 27.2,24.0,21.6,19.6,18.0,16.9,16.0,15.4,15.3/
      data ((absval(i,j),i=1,19),j=50,81)/
     9 337.,280.,192.,130.,94.2,71.1,55.8,45.3,37.7,32.1,
     a 27.9,24.6,22.1,20.0,18.4,17.2,16.3,15.7,15.6,
     1 359.,296.,200.,135.,97.4,73.3,57.5,46.6,38.7,32.9,
     2 28.6,25.2,22.6,20.5,18.8,17.6,16.7,16.1,15.9,
     3 383.,313.,209.,140.,101.,75.5,59.1,47.8,39.7,33.7,
     4 29.2,25.8,23.1,20.9,19.2,18.0,17.0,16.4,16.2,
     5 407.,330.,218.,145.,104.,77.8,60.7,49.1,40.7,34.5,
     6 29.9,26.3,23.6,21.4,19.6,18.3,17.3,16.7,16.6,
     7 432.,348.,228.,150.,107.,80.0,62.4,50.3,41.7,35.3,
     8 30.6,26.9,24.1,21.8,20.0,18.7,17.7,17.1,16.9,
     9 458.,366.,237.,156.,110.,82.3,64.0,51.6,42.7,36.1,
     a 31.3,27.5,24.6,22.3,20.4,19.1,18.0,17.4,17.2,
     1 485.,384.,246.,161.,114.,84.5,65.7,52.9,43.7,37.0,
     2 32.0,28.1,25.2,22.7,20.9,19.5,18.4,17.7,17.5,
     3 513.,403.,255.,166.,117.,86.8,67.3,54.1,44.7,37.8,
     4 32.7,28.7,25.7,23.2,21.3,19.8,18.7,18.0,17.9,
     5 542.,422.,265.,171.,120.,89.1,69.0,55.4,45.7,38.6,
     6 33.4,29.3,26.2,23.6,21.7,20.2,19.1,18.4,18.2,
     7 573.,442.,275.,176.,124.,91.4,70.7,56.7,46.8,39.4,
     8 34.0,29.9,26.7,24.1,22.1,20.6,19.4,18.7,18.5,
     9 604.,462.,284.,182.,127.,93.6,72.4,58.0,47.8,40.3,
     a 34.7,30.5,27.2,24.5,22.5,21.0,19.8,19.0,18.8,
     1 636.,483.,294.,187.,130.,95.9,74.0,59.2,48.8,41.1,
     2 35.4,31.0,27.7,25.0,22.9,21.3,20.1,19.4,19.2,
     3 670.,504.,304.,192.,134.,98.3,75.7,60.5,49.8,41.9,
     4 36.1,31.6,28.3,25.4,23.3,21.7,20.5,19.7,19.5,
     5 704.,525.,314.,198.,137.,101.,77.4,61.8,50.8,42.7,
     6 36.8,32.2,28.8,25.9,23.7,22.1,20.8,20.0,19.8,
     7 740.,547.,324.,203.,140.,103.,79.1,63.1,51.9,43.6,
     8 37.5,32.8,29.3,26.4,24.1,22.5,21.2,20.4,20.25,
     9 777.,569.,334.,209.,144.,105.,80.8,64.4,52.5,44.4,
     a 38.2,33.3,29.8,26.8,24.6,22.8,21.5,20.7,20.5,
     1 814.,591.,345.,214.,147.,108.,82.5,65.7,53.9,45.2,
     2 38.9,34.0,30.3,27.3,25.0,23.2,21.9,21.0,20.8,
     3 853.,614.,355.,219.,151.,110.,84.2,67.0,54.9,46.1,
     4 39.6,34.6,30.9,27.7,25.4,23.6,22.2,21.4,21.1,
     5 894.,638.,365.,225.,154.,112.,85.9,68.3,56.0,46.9,
     6 40.3,35.2,31.4,28.2,25.8,24.0,22.6,21.7,21.5,
     7 935.,661.,376.,231.,158.,115.,87.6,69.6,57.0,47.8,
     8 41.0,35.8,31.9,28.7,26.2,24.4,23.0,22.1,21.8,
     9 978.,685.,386.,236.,161.,117.,89.4,70.9,58.0,48.6,
     a 41.7,36.4,32.4,29.1,26.6,24.8,23.3,22.4,22.1,
     11022.,710.,397.,242.,165.,119.,91.1,72.2,59.1,49.4,
     2 42.4,37.0,33.0,29.6,27.0,25.1,23.7,22.7,22.5,
     31067.,735.,408.,247.,168.,122.,92.8,73.6,60.1,50.3,
     4 43.1,37.6,33.5,30.1,27.5,25.5,24.0,23.1,22.8,
     51113.,760.,418.,253.,172.,124.,94.6,74.9,61.2,51.1,
     6 43.8,38.2,34.0,30.5,27.9,25.9,24.4,23.4,23.1,
     71161.,785.,429.,259.,175.,127.,96.3,76.2,62.2,52.0,
     8 44.6,38.8,34.6,31.0,28.3,26.3,24.7,23.8,23.5,
     91210.,811.,440.,264.,179.,129.,98.1,77.5,63.3,52.8,
     a 45.3,39.5,35.1,31.5,28.7,26.7,25.1,24.1,23.8,
     11260.,837.,451.,270.,182.,131.,99.8,78.9,64.3,53.7,
     2 46.0,40.1,35.6,31.9,29.2,27.1,25.5,24.4,24.1,
     31311.,864.,462.,276.,186.,134.,102.,80.2,65.4,54.5,
     4 46.7,40.7,36.1,32.4,29.6,27.4,25.8,24.8,24.5,
     51364.,891.,473.,281.,190.,136.,103.,81.5,66.4,55.4,
     6 47.4,41.3,36.7,32.9,30.0,27.8,26.2,25.1,24.8,
     71418.,918.,484.,287.,193.,139.,105.,82.9,67.5,56.2,
     8 48.1,41.9,37.2,33.3,30.4,28.2,26.5,25.5,25.2,
     91474.,945.,495.,293.,197.,141.,107.,84.2,68.5,57.1,
     a 48.9,42.5,37.8,33.8,30.8,28.6,26.9,25.8,25.5,
     11530.,974.,507.,299.,200.,144.,109.,85.5,69.6,58.0,
     2 49.5,43.1,38.3,34.3,31.3,29.0,27.3,26.1,25.8/
      data aa
     1  /0.000000,0.000000, 0.318300, 0.000000, 0.955000,0.000000,7.1,
     2   0.001615,0.031400, 0.064800, 0.835000, 0.140000,0.000000,0.0,
     3   0.006420,0.053000, 0.179000, 0.057000, 0.000000,0.000000,0.0,
     4   0.014420,0.086000, 0.061000, 0.330000, 0.000000,0.000000,0.0,
     5   0.025400,0.105000, 0.080000, 0.130000, 0.000000,0.000000,0.0,
     6   0.039400,0.123000, 0.073000,-0.160000, 0.000000,0.000000,0.0,
     7   0.056000,0.139000, 0.000000, 0.000000, 0.000000,0.000000,0.0,
     8   0.075200,0.150000,-0.022000, 0.000000, 0.000000,0.000000,0.0,
     9   0.096600,0.159000,-0.100000, 0.000000, 0.000000,0.000000,0.0,
     a   0.119900,0.164000,-0.160000, 0.000000, 0.000000,0.000000,0.0,
     1   0.144800,0.160000,-0.200000, 0.000000, 0.000000,0.000000,0.0,
     2   0.174100,0.130000,-0.300000, 0.000000, 0.000000,0.000000,0.0,
     3   0.198400,0.110000,-0.210000, 0.000000, 0.000000,0.000000,0.0,
     4   0.226000,0.100000,-0.330000, 0.000000, 0.000000,0.000000,0.0,
     5   0.250000,0.105000,-0.480000, 0.000000, 0.000000,0.000000,0.0,
     6   0.274000,0.080000,-0.520000, 0.000000, 0.000000,0.000000,0.0,
     7   0.295000,0.064000,-0.640000, 0.000000, 0.000000,0.000000,0.0,
     8   0.310700,0.030000,-0.500000, 0.000000, 0.000000,0.000000,0.0,
     9   0.318300,0.000000,-0.530000, 0.000000, 0.000000,0.000000,0.0/
      if(Kabsor(KDatBlock).le.IdPwdAbsorbNone.or.
     1   MirPwd(KDatBlock).le.0.) then
        PwdAbsorption=1.
      else if(Kabsor(KDatBlock).eq.IdPwdAbsorbCylinder) then
        th=.5*tth/torad
        d1=.1*tth/torad+1.
        j=d1
        d1=d1-float(j)
        d2=MirPwd(KDatBlock)*10.+1.
        i=d2
        d2=d2-float(i)
        if(i.lt.81) then
          PwdAbsorption=absval(j,i)+d1*(absval(j+1,i)-absval(j,i))+
     1                              d2*(absval(j,i+1)-absval(j,i))+
     2                           d1*d2*(absval(j+1,i+1)+absval(j,i)-
     3                                  absval(j+1,i)-absval(j,i+1))
        else
          xm=1.
          xd=1./MirPwd(KDatBlock)
          call SetRealArrayTo(yy,2,0.)
          do l=1,7
            xm=xm*xd
            m=0
            do k=j,j+1
              m=m+1
              yy(m)=yy(m)+aa(l,k)*xm
            enddo
          enddo
          do k=1,2
            if(yy(k).gt.0.) then
              yy(k)=1./yy(k)
            else
              yy(k)=999999.
            endif
          enddo
          PwdAbsorption=yy(1)+d1*(yy(2)-yy(1))
        endif
      else if(Kabsor(KDatBlock).eq.IdPwdAbsorbTransmission) then
        sec=1./cos(tth*.5)
        PwdAbsorption=1./ExpJana(-MirPwd(KDatBlock)*sec,ich)
      else if(Kabsor(KDatBlock).eq.IdPwdAbsorbReflection) then
        cosec=1./sin(tth*.5)
        PwdAbsorption=cosec/
     1                (1.-ExpJana(-2.*MirPwd(KDatBlock)*cosec,ich))
      endif
      return
      end
      function PwdTOFAbsorption(XPwdi,AbsTOFPwd,DerAbsTOFPwd)
      pom=PwdTOF2D(XPwdi)
      PwdTOFAbsorption=expJana(-AbsTOFPwd*pom,ich)
      if(ich.eq.0) then
        DerAbsTOFPwd=-PwdTOFAbsorption*pom
      else
        PwdTOFAbsorption=1.
        DerAbsTOFPwd=0.
      endif
      return
      end
      subroutine PwdRougness(th2,Roughness,DerRoughness)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension DerRoughness(2)
      real Numer
      if(KRough(KDatBlock).eq.IdPwdRoughNone) then
        RoughNess=1.
        call SetRealArrayTo(DerRoughness,2,0.)
      else
        rsnth=1./sin(th2*.5)
        rsnthq=rsnth**2
      endif
      if(KRough(KDatBlock).eq.IdPwdRoughPitchke) then
        Numer=(1.-RoughPwd(1,KDatBlock)*
     1        (rsnth-RoughPwd(2,KDatBlock)*rsnthq))
        Denom=(1.-RoughPwd(1,KDatBlock)+
     1         RoughPwd(1,KDatBlock)*RoughPwd(2,KDatBlock))
        RoughNess=Numer/Denom
        DenomQ=Denom**2
        DNumer=-rsnth+RoughPwd(2,KDatBlock)*rsnthq
        DDenom=-1.+RoughPwd(2,KDatBlock)
        DerRoughNess(1)=(DNumer*Denom-Numer*DDenom)/DenomQ
        DNumer=RoughPwd(1,KDatBlock)*rsnthq
        DDenom=RoughPwd(1,KDatBlock)
        DerRoughNess(2)=(DNumer*Denom-Numer*DDenom)/DenomQ
      else if(KRough(KDatBlock).eq.IdPwdRoughSuortti) then
        expn=exp(-RoughPwd(2,KDatBlock)*rsnth)
        expd=exp(-RoughPwd(2,KDatBlock))
        Numer=(RoughPwd(1,KDatBlock)+(1.-RoughPwd(1,KDatBlock))*expn)
        Denom=(RoughPwd(1,KDatBlock)+(1.-RoughPwd(1,KDatBlock))*expd)
        RoughNess=Numer/Denom
        DenomQ=Denom**2
        DNumer=1.-expn
        DDenom=1.-expd
        DerRoughNess(1)=(DNumer*Denom-Numer*DDenom)/DenomQ
        DNumer=(-1.+RoughPwd(1,KDatBlock))*expn*rsnth
        DDenom=(-1.+RoughPwd(1,KDatBlock))*expd
        DerRoughNess(2)=(DNumer*Denom-Numer*DDenom)/DenomQ
      endif
      return
      end
      subroutine PwdWriteRefLst(lstp,ihr,Yoqp,Ycqp,Ysqp,Ykp,Ywp,Yss,
     1                          abas,bbas,afp,bfp,
     2                          affreep,bffreep,afstp,bfstp,affreestp,
     3                          bffreestp,nout,KPh,sinthlp,extkorp,
     4                          extkorpm)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*1 nspec,kspec,t1
      dimension ihr(*)
      if(CalcDer.and.lstp.le.0) go to 9999
      Fcalc=sqrt(Ycqp)
      if(Ywp.gt.0.and.Ykp.gt.1.) then
        Fobs=Yoqp/Ywp
        pom1=Ysqp/Ywp-Fobs**2
        yss=sqrt(Yss)/Ywp
      else
        Fobs=0.
        pom1=0.
        yss=Fobs+1.
      endif
      if(pom1.gt.0.) then
        ys=sqrt(pom1)
      else
        ys=Fobs+1.
      endif
      if(SigMethod.eq.1) then
        ys=yss
      else if(SigMethod.eq.3) then
        ys=max(ys,yss)
      else if(SigMethod.eq.4) then
        ys=min(ys,yss)
      endif
      if(Fobs.gt..01) then
        Fobs=sqrt(Fobs)
        sigyo=max(.5*ys/Fobs,.0001)
        sigyo=sqrt(sigyo**2+(blkoef*Fobs)**2)
      else
        Fobs  =0.
        if(Fcalc.gt..0001) then
          sigyo=Fcalc
        else
          sigyo=1.
        endif
      endif
      dyp=Fobs-Fcalc
      wdy=min(dyp/sigyo,9999.)
      wdy=max(wdy,-9999.)
      if(abs(wdy).gt.vyh) then
        kspec='#'
      else
        kspec=' '
      endif
      if(Fobs.lt.2.*slevel*sigyo) then
        nspec='*'
      else
        nspec=' '
      endif
      if(.not.CalcDer) then
        if(lstp.gt.0) then
          ycf=sqrt(afp**2+bfp**2)
        else
          ycf=Fcalc
        endif
        if(NAtCalc.le.0.or.DoLeBail.ne.0) then
          yof=ycf
        else
          if(Fcalc.ne.0.) then
            yof=Fobs/Fcalc*ycf
          else
            yof=Fobs
          endif
        endif
        write(80,format80)(ihr(m),m=1,maxNDim),KPh,yof,yof,ycf,afp,bfp,
     1                     affreep,bffreep,afstp,bfstp,affreestp,
     2                     bffreestp,sigyo,sigyo
        if(NSpec.eq.'*') then
          t1='<'
        else
          t1='o'
        endif
        write(83,format83p)(ihr(m),m=1,maxNDim),ycf**2,yof**2,
     1                      2.*yof*sigyo,t1,.5/sinthlp,KPh,
     2                      sqrt(abas**2+bbas**2),abas,bbas
      endif
      if(lstp.le.0) go to 9999
      nout=nout+1
      if(ExistMagnetic) then
        write(lstp,format3p)(ihr(m),m=1,maxNDim),Fobs,Fcalc,abas,bbas,
     1    dyp,sigyo,1./sigyo**2,wdy,nout,nspec,kspec,sinthlp,extkorp,
     2    extkorpm,KPh,KPh,KPh
      else
        write(lstp,format3p)(ihr(m),m=1,maxNDim),Fobs,Fcalc,abas,bbas,
     1    dyp,sigyo,1./sigyo**2,wdy,nout,nspec,kspec,sinthlp,extkorp,
     2    KPh,KPh,KPh
      endif
9999  return
      end
      subroutine PwdRemoveSkipRegion(DegFr,DegTo)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      n=NSkipPwd(KDatBlock)
      do 1000i=1,NSkipPwd(KDatBlock)
        if(SkipPwdFr(i,KDatBlock).gt.DegTo) then
          go to 2000
        else if(SkipPwdTo(i,KDatBlock).lt.DegFr) then
          go to 1000
        else if(SkipPwdFr(i,KDatBlock).ge.DegFr) then
          if(SkipPwdTo(i,KDatBlock).gt.DegTo) then
            SkipPwdFr(i,KDatBlock)=DegTo
            go to 2000
          else
            SkipPwdFr(i,KDatBlock)=-999.
          endif
        else if(SkipPwdFr(i,KDatBlock).lt.DegFr) then
          if(SkipPwdTo(i,KDatBlock).gt.DegTo) then
            n=n+1
            SkipPwdFr(n,KDatBlock)=DegTo
            SkipPwdTo(n,KDatBlock)=SkipPwdTo(i,KDatBlock)
            SkipPwdTo(i,KDatBlock)=DegFr
            go to 2000
          else
            SkipPwdTo(i,KDatBlock)=DegFr
          endif
        endif
1000  continue
2000  NSkipPwd(KDatBlock)=n
      call PwdOrderSkipRegions
      return
      end
      subroutine PwdOrderSkipRegions
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension ifr(MxSkipPwd),ipor(MxSkipPwd),SkipPwdFrNew(MxSkipPwd),
     1          SkipPwdToNew(MxSkipPwd)
      call PwdSetTOF(KDatBlock)
      Order=min(10**OrderSkipFrTo(KDatBlock),10000)
      n=0
      do i=1,NSkipPwd(KDatBlock)
        if(SkipPwdFr(i,KDatBlock).lt.-900.) cycle
        n=n+1
        SkipPwdFr(n,KDatBlock)=SkipPwdFr(i,KDatBlock)
        SkipPwdTo(n,KDatBlock)=SkipPwdTo(i,KDatBlock)
      enddo
      if(NSkipPwd(KDatBlock).le.1) go to 1500
      do i=1,NSkipPwd(KDatBlock)
        SkipPwdFrNew(i)=SkipPwdFr(i,KDatBlock)
        SkipPwdToNew(i)=SkipPwdTo(i,KDatBlock)
        ifr(i)=Order*SkipPwdFr(i,KDatBlock)
      enddo
      call indexx(NSkipPwd(KDatBlock),ifr,ipor)
      n=0
      do i=1,NSkipPwd(KDatBlock)
        j=ipor(i)
        pomFr=SkipPwdFrNew(j)
        pomTo=SkipPwdToNew(j)
        if(n.gt.0) then
          if(SkipPwdTo(n,KDatBlock).ge.pomFr) then
            pomTo=max(pomTo,SkipPwdTo(n,KDatBlock))
            go to 1150
          endif
        endif
        n=n+1
        SkipPwdFr(n,KDatBlock)=pomFr
1150    SkipPwdTo(n,KDatBlock)=pomTo
      enddo
      if(DrawInD) then
        if(isTOF) then
          XPwd3=PwdD2TOF(XPwd(3))
          XPwd1=PwdD2TOF(XPwd(1))
        else
          if(KRefLam(KDatBlock).eq.1) then
            XPwd3=PwdD2TwoTh(XPwd(NPnts-2),LamPwd(1,KDatBlock))
            XPwd1=PwdD2TwoTh(XPwd(NPnts),LamPwd(1,KDatBlock))
          else
            XPwd3=PwdD2TwoTh(XPwd(NPnts-2),LamAve(KDatBlock))
            XPwd1=PwdD2TwoTh(XPwd(NPnts),LamAve(KDatBlock))
          endif
        endif
      else
        XPwd3=XPwd(3)
        XPwd1=XPwd(1)
      endif
      if(SkipPwdFr(1,KDatBlock).lt.XPwd3)
     1  SkipPwdFr(1,KDatBlock)=XPwd1*.999
      if(DrawInD) then
        if(isTOF) then
          XPwd3=PwdD2TOF(XPwd(NPnts-2))
          XPwd1=PwdD2TOF(XPwd(NPnts))
        else
          if(KRefLam(KDatBlock).eq.1) then
            XPwd3=PwdD2TwoTh(XPwd(3),LamPwd(1,KDatBlock))
            XPwd1=PwdD2TwoTh(XPwd(1),LamPwd(1,KDatBlock))
          else
            XPwd3=PwdD2TwoTh(XPwd(3),LamAve(KDatBlock))
            XPwd1=PwdD2TwoTh(XPwd(1),LamAve(KDatBlock))
          endif
        endif
      else
        XPwd3=XPwd(NPnts-2)
        XPwd1=XPwd(NPnts)
      endif
      if(SkipPwdTo(n,KDatBlock).gt.XPwd3)
     1  SkipPwdTo(n,KDatBlock)=XPwd1*1.001
1500  NSkipPwd(KDatBlock)=n
      do 2200j=1,Npnts
        if(DrawInD) then
          if(isTOF) then
            XPwdj=PwdD2TOF(XPwd(j))
          else
            if(KRefLam(KDatBlock).eq.1) then
              XPwdj=PwdD2TwoTh(XPwd(j),LamPwd(1,KDatBlock))
            else
              XPwdj=PwdD2TwoTh(XPwd(j),LamAve(KDatBlock))
            endif
          endif
        else
          XPwdj=XPwd(j)
        endif
        do i=1,NskipPwd(KDatBlock)
          if(XPwdj.ge.SkipPwdFr(i,KDatBlock).and.
     1       XPwdj.le.SkipPwdTo(i,KDatBlock)) then
            YfPwd(j)=0
            go to 2200
          endif
        enddo
        if(UseCutoffPwd) then
          if(DrawInD) then
            dpom=XPwd(j)
          else
            if(isTOF) then
              if(TOFInD) then
                dpom=XPwd(j)
              else
                dpom=PwdTOF2D(XPwd(j))
              endif
            else
              dpom=PwdTwoTh2D(XPwd(j),LamA1(KDatBlock))
            endif
          endif
          if(dpom.lt.CutoffMinPwd.or.dpom.gt.CutoffMaxPwd) then
            YfPwd(j)=0
            go to 2200
          endif
        endif
        YfPwd(j)=1
2200  continue
      return
      end
      subroutine PwdSimulation
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      include 'main.cmn'
      character*256 :: FileName = ' ',Veta,EdwStringQuest
      integer FeChdir
      logical StructureExists,FeYesNo,RefineEnd
      id=NextQuestId()
      xqd=400.
      il=1
      call FeQuestCreate(id,-1.,-1.,xqd,il,'Simulated structure',0,
     1                   LightGray,0,0)
      il=1
      tpom=5.
      Veta='%Name of the simulated structure:'
      xpomp=tpom+FeTxLengthUnder(Veta)+5.
      dpomp=70.
      dpom=xqd-xpomp-dpomp-15.
      call FeQuestEdwMake(id,tpom,il,xpomp,il,Veta,'L',dpom,EdwYd,0)
      nEdwName=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileName)
      xpomb=xpomp+dpom+10.
      Veta='%Browse'
      call FeQuestButtonMake(id,xpomb,il,dpomp,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        FileName=EdwStringQuest(nEdwName)
        call FeFileManager('Define name of the simulated structure',
     1                     FileName,' ',1,.false.,ich)
        if(ich.eq.0) call FeQuestStringEdwOpen(nEdwName,FileName)
        call FeQuestButtonOff(nButtBrowse)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        FileName=EdwStringQuest(nEdwName)
        if(FileName.eq.' ') then
          Veta='the name of simulated structure hasn''t been '//
     1         'defined, try again.'
          call FeChybne(-1.,YBottomMessage,Veta,' ',SeriousError)
        else if(StructureExists(FileName)) then
          call ExtractFileName(FileName,Veta)
          if(FeYesNo(-1.,YBottomMessage,'The structure "'//
     1               Veta(:idel(Veta))//
     2               '" already exists, rewrite it?',0)) go to 1800
        else
          go to 1800
        endif
        EventType=EventEdw
        EventNumber=nEdwName
        call FeQuestButtonOff(ButtonOK-ButtonFr+1)
        go to 1500
      endif
1800  call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      call DeletePomFiles
      call ExtractDirectory(FileName,Veta)
      i=FeChdir(Veta)
      call FeGetCurrentDir
      call ExtractFileName(FileName,fln)
      ifln=idel(fln)
      call FeBottomInfo(' ')
      call DeletePomFiles
      isPowder=.true.
      ExistPowder=.true.
      if(ExistM90) then
        LamAve(1)=LamAve(KDatBlock)
        LamAvePwd(1)=LamAve(KDatBlock)
        LamA1(1)=LamA1(KDatBlock)
        LamA2(1)=LamA2(KDatBlock)
        LamPwd(1,1)=LamA1(KDatBlock)
        LamPwd(2,1)=LamA2(KDatBlock)
        NAlfa(1)=NAlfa(KDatBlock)
        LamRat(1)=LamRat(KDatBlock)
        LpFactor(1)=LpFactor(KDatBlock)
        AngleMon(1)=AngleMon(KDatBlock)
        AlphaGMon(1)=AlphaGMon(KDatBlock)
        BetaGMon(1)=BetaGMon(KDatBlock)
        FractPerfMon(1)=FractPerfMon(KDatBlock)
        Radiation(1)=Radiation(KDatBlock)
      else
        LamAve(1)=LamAveD(5)
        LamAvePwd(1)=LamAveD(5)
        LamA1(1)=LamA1D(5)
        LamA2(1)=LamA2D(5)
        LamPwd(1,1)=LamA1D(5)
        LamPwd(2,1)=LamA2D(5)
        NAlfa(1)=1
        LamRat(1)=LamRatD(5)
        LpFactor(1)=PolarizedMonoPar
        AngleMon(1)=CrlMonAngle(MonCell(1,3),MonH(1,3),LamAve(1))
        FractPerfMon(1)=.5
        Radiation(1)=XRayRadiation
      endif
      call PwdSetTOF(KDatBlock)
      LorentzToCrySize(1)=10.*LamAve(1)/ToRad
      GaussToCrySize(1)=sqrt8ln2*LorentzToCrySize(1)
      ExistM90=.false.
      call DeleteFile(fln(:ifln)//'.m90')
      ExistM95=.false.
      call DeleteFile(fln(:ifln)//'.m95')
      NDatBlock=1
      KDatBlock=1
      DataType(1)=2
      call iom50(1,0,fln(:ifln)//'.m50')
      call SetBasicM41(0)
      call iom40(1,0,fln(:ifln)//'.m40')
      call RefOpenCommands
      NacetlInt(nCmdisimul)=1
      call RefRewriteCommands(1)
      call iom50(1,0,fln(:ifln)//'.m50')
      call FeFillTextInfo('pwdsimul.txt',0)
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
      call EM40EditParameters(IdParamPowder,0)
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      call Refine(0,RefineEnd)
      if(ErrFlag.ne.0) go to 9999
      call FeDeferOutput
9999  return
      end
      subroutine PwdResetPowderPar
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*8 MenuPhase(MxPhases+1)
      integer RolMenuSelectedQuest
      if(NPhase.le.1) go to 9999
      do i=1,NPhase
        MenuPhase(i)=PhaseName(i)
      enddo
      MenuPhase(NPhase+1)='Default'
      dpom=20.
      do i=1,NPhase+1
        dpom=max(dpom,FeTxLengthUnder(MenuPhase(i)))
      enddo
      dpom=dpom+EdwYd+10.
      xpom=5.
      pom=xpom+dpom
      id=NextQuestId()
      xqd=2.*pom+50.
      il=2
      call FeQuestCreate(id,-1.,-1.,xqd,il,' ',0,LightGray,0,0)
      call FeQuestRolMenuMake(id,xpom,il-1,xpom,il,'Source','L',dpom,
     1                         EdwYd,0)
      nRolMenuSource=RolMenuLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,MenuPhase,NPhase+1,1)
      bpom=15.
      xpom=(xqd-bpom)*.5
      call FeQuestButtonMake(id,xpom,il,bpom,ButYd,'=>')
      nButtAction=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      xpom=xqd-dpom-5.
      call FeQuestRolMenuMake(id,xpom,il-1,xpom,il,'Target','L',dpom,
     1                        EdwYd,0)
      nRolMenuTarget=EdwLastMade
      call FeQuestRolMenuOpen(RolMenuLastMade,MenuPhase,NPhase,2)
1500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtAction)
     1  then
        is=RolMenuSelectedQuest(nRolMenuSource)
        it=RolMenuSelectedQuest(nRolMenuTarget)
        if(is.ne.it) call PwdCopyPowderPar(is,it)
        go to 1500
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      call FeQuestRemove(id)
      if(ich.eq.0) call iom40(1,0,fln(:ifln)//'.m40')
      call iom40(0,0,fln(:ifln)//'.m40')
9999  return
      end
      subroutine PwdCopyPowderPar(is,it)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      KPhase=it
      if(is.le.0) then
        KStrain(it,KDatBlock)=0
        KAsym(KDatBlock)=0
        NAsym(KDatBlock)=0
        KPref(it,KDatBlock)=0
        KProfPwd(it,KDatBlock)=1
        PCutOff(it,KDatBlock)=8.
        SPref(it,KDatBlock)=2.
        call CopyVek(CellPar(1,1,it),CellPwd(1,it),6)
        call CopyVek(qu(1,1,1,it),QuPwd(1,1,it),3*NDimI(KPhase))
        call SetRealArrayTo(GaussPwd(1,it,KDatBlock),4,0.)
        if(DataType(KDatBlock).eq.-2) then
          GaussPwd(2,it,KDatBlock)=1.
        else
          GaussPwd(3,it,KDatBlock)=5.
        endif
      else
        call CopyVek(GaussPwd(1,is,KDatBlock),
     1               GaussPwd(1,it,KDatBlock),4)
        call CopyVek(LorentzPwd(1,is,KDatBlock),
     1               LorentzPwd(1,it,KDatBlock),5)
        PCutOff(it,KDatBlock)=PCutOff(is,KDatBlock)
      endif
      return
      end
      subroutine PwdSetBackground
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension derp(*)
      save pom1,pom2,XPwdi1,XPwdi2
      do i=1,npnts
        if(YfPwd(i).eq.1) exit
      enddo
      i1=i
      do i=npnts,1,-1
        if(YfPwd(i).eq.1) exit
      enddo
      i2=i
      call PwdSetTOF(KDatBlock)
      if(DrawInD) then
        if(isTOF) then
          XPwdi1=PwdTOF2D(XPwd(i1))
          XPwdi2=PwdTOF2D(XPwd(i2))
        else
          if(KRefLam(KDatBlock).eq.1) then
            XPwdi1=PwdD2TwoTh(XPwd(i2),LamPwd(1,KDatBlock))
            XPwdi2=PwdD2TwoTh(XPwd(i1),LamPwd(1,KDatBlock))
          else
            XPwdi1=PwdD2TwoTh(XPwd(i2),LamAve(KDatBlock))
            XPwdi2=PwdD2TwoTh(XPwd(i1),LamAve(KDatBlock))
          endif
        endif
      else
        XPwdi1=XPwd(i1)
        XPwdi2=XPwd(i2)
      endif
      if(KBackg(KDatBlock).le.2) then
        pom1=2./(XPwdi2-XPwdi1)
        pom2=-.5*(XPwdi2+XPwdi1)
      else if(KBackg(KDatBlock).eq.3) then
        pom1=pi/(XPwdi2-XPwdi1)
        pom2=-XPwdi1
      endif
      go to 9999
      entry PwdGetBackground(XPwdi,Bkg,Derp,DerSc)
      if(KBackg(KDatBlock).ne.4) then
        bgi=pom1*(XPwdi+pom2)
      else
        if(isTOF) then
          bgi=XPwdi/XPwdi2*Pi
        else
          bgi=XPwdi*ToRad
        endif
      endif
      if(KUseInvX(KDatBlock).eq.1) then
        pom=1./(XPwdi*ToRad)
        Bkg=BackgPwd(1,KDatBlock)*pom
        derp(1)=pom
        ip=2
      else
        Bkg=0.
        ip=1
      endif
      if(KBackg(KDatBlock).eq.1) then
        Bkg=Bkg+FLegen(BackgPwd(ip,KDatBlock),derp(ip),bgi,
     1                 NBackg(KDatBlock))
      else if(KBackg(KDatBlock).eq.2) then
        Bkg=Bkg+Chebev(BackgPwd(ip,KDatBlock),derp(ip),bgi,
     1                 NBackg(KDatBlock))
      else if(KBackg(KDatBlock).eq.3.or.KBackg(KDatBlock).eq.4) then
        Bkg=Bkg+CosBackg(BackgPwd(ip,KDatBlock),derp(ip),bgi,
     1                   NBackg(KDatBlock))
      else
        Bkg=0.
      endif
      if(KManBackg(KDatBlock).gt.0) then
        Bkg=Bkg+PwdManualBackground(XPwdi)*Sc(2,KDatBlock)
        DerSc=PwdManualBackground(XPwdi)
      else
        DerSc=0.
      endif
9999  return
      end
      subroutine PwdSaveBackground
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'refine.cmn'
      dimension derp(36)
      character*256 FileName
      logical   FeYesNoHeader,ExistFile
      FileName=fln(:ifln)//'.bac'
      ln=0
1000  call FeFileManager('Select file where the background is to be '//
     1                   'saved',FileName,'*.bac',0,.true.,ich)
      if(ich.ne.0) go to 9999
      if(ExistFile(FileName)) then
        call FeCutName(FileName,TextInfo(1),len(TextInfo(1))-35,
     1                 CutTextFromLeft)
        NInfo=1
        TextInfo(1)='The file "'//TextInfo(1)(:idel(TextInfo(1)))//
     1              '" already exists'
        if(.not.FeYesNoHeader(-1.,-1.,'Do you want to rewrite it?',1))
     1    go to 1000
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,FileName,'formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      call PwdM92Nacti
      if(ErrFlag.ne.0) go to 9999
      call PwdSetBackground
      do i=1,Npnts
        call PwdGetBackground(XPwd(i),Bkg,derp,DerSc)
        write(ln,Format92) XPwd(i),Bkg
      enddo
9999  call CloseIfOpened(ln)
      return
      end
      subroutine PwdPrfFun(Type,dt,fwhm,eta,f,dfdt,dfdg,dfde)
      include 'fepc.cmn'
      include 'basic.cmn'
      integer Type
      dtq=dt**2
      if(Type.eq.2.or.Type.eq.3) then
        bunbo=(.5*fwhm)**2+dtq
        tl=fwhm/(pi2*bunbo)
        dfdgl=tl/fwhm-.5*tl/bunbo*fwhm
        dfdtl=-2.*tl/bunbo*dt
      else
        tl=0.
        dfdtl=0.
      endif
      if(Type.eq.1.or.Type.eq.3) then
        sigp=fwhm**2/5.545177
        ex=-2.77259*dtq/fwhm**2
        tg=.9394373*ExpJana(ex,ich)/fwhm
        if(dt.ne.0.) then
          dfdtg=2.*tg*ex/dt
        else
          dfdtg=0.
        endif
        dfdgg=-tg/fwhm-2.*tg*ex/fwhm
      else
        ex=0.
        tg=0.
      endif
      if(Type.eq.1) then
        f=tg
        dfde=0.
        dfdt=dfdtg
        dfdg=dfdgg
      else if(Type.eq.2) then
        f=tl
        dfde=0.
        dfdt=dfdtl
        dfdg=dfdgl
      else if(Type.eq.3) then
        f=eta*tl+(1.-eta)*tg
        dfde=tl-tg
        dfdt=eta*dfdtl+(1.-eta)*dfdtg
        dfdg=eta*dfdgl+(1.-eta)*dfdgg
      endif
      return
      end
      subroutine PwdPrfCalc
      use Powder_mod
      parameter (mxpar=20)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension WCen(5),WRel(5),WGam(5),Th2Cen(5),Th2Gam(5),
     1          WLam(-500:500),Wx(-500:500),WInt(-500:500),
     1          der(mxpar),ps(mxpar),ki(mxpar),
     2          dc(mxpar),am((mxpar*(mxpar+1))/2)
      character*80 SvFile
      logical RefineOn
      data WCen/1.534753,1.540596,1.541058,1.544410,1.544721/
      data WRel/1.59,57.91,7.62,24.17,8.71/
      data WGam/3.69,0.44,0.60,0.52,0.62/
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,Black,
     1                      -1,-1)
      call FeMakeGrWin(0.,40.,14.,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      pom=YMaxGrWin
      dpom=ButYd+6.
      ypom=pom-dpom-2.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,Gray,White)
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Refine')
      nButtRefine=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-dpom
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call UnitMat(F2O,3)
      pom=0.
      do j=1,5
        if(WRel(j).gt.pom) then
          jr=j
          pom=WRel(j)
        endif
      enddo
      nps=5
      nam=(nps*(nps+1))/2
      call SetIntArrayTo(ki,5,1)
      delta=.8
      do 4000ii=1,NBraggPeaks
        i=PeakPor(ii)
        RefineOn=.false.
        pom=0.
        Theta=PeakXPos(i)*.5*ToRad
        do j=1,5
          pom=pom+WCen(j)*WRel(j)*.01
        enddo
        xomn=PeakXPos(i)-delta
        xomx=PeakXPos(i)+delta
        yomn=0.
        yomx=0.
        ysum=0.
        jp=0
        do j=1,NPnts
          XPwdj=XPwd(j)
          if(XPwdj.ge.xomn.and.XPwdj.le.xomx) then
            if(YoPwd(j).gt.yomx) then
              yomx=YoPwd(j)
              jm=j
            endif
            if(jp.eq.0) jp=j
            jk=j
            ysum=ysum+YoPwd(j)
          endif
        enddo
        nn=jk-jp+1
        if(mod(nn,2).ne.1) then
          jk=jk+1
          nn=nn+1
          ysum=ysum+YoPwd(jk)
        endif
        deltap=2.*delta/float(nn-1)
        if(PeakInt(i).lt.0.) then
          PeakBckg(1,i)=(YoPwd(jp)+YoPwd(jk))*.5
          PeakInt(i)=ysum*deltap-PeakBckg(1,i)*deltap*float(nn)
          PeakFWHM(i)=0.1
          PeakEta(i)=0.5
        endif
        xomn=XPwd(jp)
        xomx=XPwd(jk)
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        damp=0.1
2000    if(RefineOn) then
          ij=0
          do i=1,nps
            ij=ij+i
            der(i)=1./sqrt(am(ij))
          enddo
          call znorm(am,der,nps)
          call smi(am,nps,ising)
          call znorm(am,der,nps)
          call nasob(am,ps,dc,nps)
          PeakInt(i)=PeakInt(i)+dc(1)*damp
          PeakBckg(1,i)=PeakBckg(1,i)+dc(2)*damp
          PeakXPos(i)=PeakXPos(i)-dc(3)*damp
          PeakFWHM(i)=PeakFWHM(i)+dc(4)*damp
          PeakEta(i)=PeakEta(i)+dc(5)*damp
        endif
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
        call FeXYPlot(XPwd(jp),YoPwd(jp),nn,NormalLine,NormalPlotMode,
     1                white)
        call SetRealArrayTo(ps,nps,0.)
        call SetRealArrayTo(am,nam,0.)
        Theta=PeakXPos(i)*.5*ToRad
        do j=1,5
          Th2Cen(j)=2.*asin(sin(Theta)*WCen(j)/WCen(jr))/ToRad
          Th2Gam(j)=.002*WGam(j)/WCen(jr)*tan(Theta)/ToRad
        enddo
        jj=jp
        do j=-nn/2,nn/2
          XPwdj=XPwd(jj)
          Wx(j)=XPwdj
          pom=0.
          do k=1,5
            call PwdPrfFun(2,XPwdj-Th2Cen(k),Th2Gam(k),1.,fk5,fk1,
     1                     fk2,fk3)
            pom=pom+WRel(k)*.01*fk5
          enddo
          WLam(j)=pom
          jj=jj+1
        enddo
        do j=-nn/2,nn/2
          XPwdj=Wx(j)
          pom=0.
          pomdt=0.
          pomde=0.
          pomdg=0.
          pomdi=0.
          do k=-nn/2,nn/2
            if(j-k.ge.-nn/2.and.j-k.le.nn/2) then
              XPwdk=Wx(k)-PeakXPos(i)
              call PwdPrfFun(3,XPwdk,PeakFWHM(i),PeakEta(i),Voigt,
     1                       VoigtDT,VoigtDG,VoigtDE)
              pom=pom+Voigt*WLam(j-k)
              pomdt=pomdt+VoigtDT*WLam(j-k)
              pomde=pomde+VoigtDE*WLam(j-k)
              pomdg=pomdg+VoigtDG*WLam(j-k)
            endif
          enddo
          pom=pom*deltap
          pomdt=pomdt*deltap
          pomde=pomde*deltap
          pomdg=pomdg*deltap
          WInt(j)=PeakInt(i)*pom+PeakBckg(1,i)
          der(1)=pom
          der(2)=1.
          der(3)=PeakInt(i)*pomdt
          der(4)=PeakInt(i)*pomdg
          der(5)=PeakInt(i)*pomde
          dy=YoPwd(jp+j+nn/2)-WInt(j)
          wt=1.
          m=0
          do k=1,nps
            wtderk=wt*der(k)
            ps(k)=ps(k)+wtderk*dy
            do l=1,k
              m=m+1
              am(m)=am(m)+wtderk*der(l)
            enddo
          enddo
        enddo
        call FeXYPlot(Wx(-nn/2),WInt(-nn/2),nn,NormalLine,
     1                NormalPlotMode,Green)
        if(RefineOn) then
          nref=nref+1
          if(mod(nref,100).eq.0) then
            RefineOn=.false.
          else
            call FeReleaseOutput
            call FeDeferOutput
            go to 2000
          endif
        endif
3000    call FeQuestEvent(id,ich)
        if(CheckType.eq.EventButton) then
          if(CheckNumber.eq.nButtRefine) then
            nref=0
            RefineOn=.true.
            go to 2000
          else if(CheckNumber.eq.nButtNext) then
            go to 4000
          endif
        endif
        go to 3000
4000  continue
      call FeQuestRemove(id)
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
      subroutine PwdPrfEdit
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      if(DrawInD) then
        TextInfo(1)='      d        Int        B0    FWHM     Eta'
      else
        TextInfo(1)='     2th       Int        B0    FWHM     Eta'
      endif
      do ii=1,NBraggPeaks
        i=PeakPor(ii)
        write(TextInfo(ii+1),'(f8.3,2f10.1,2f8.4)')
     1    PeakXPos(i),PeakInt(i),PeakBckg(1,i),PeakFWHM(i),PeakEta(i)
      enddo
      NInfo=NBraggPeaks+1
      call FeInfoOut(-1.,-1.,'PEAKS','L')
      return
      end
      subroutine PwdPrfTest
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension Wx(-5000:5000),Wint(-5000:5000),WDiv(-5000:5000)
      character*80 SvFile
      character*80 t80
      integer Color
      logical CrwLogicQuest
      Tiskne=.false.
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,Black,
     1                      -1,-1)
      call FeMakeGrWin(0.,40.,14.,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      pom=YMaxGrWin
      dpom=ButYd+6.
      ypom=pom-dpom-2.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-9.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Continue')
      nButtContinue=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-9.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Print')
      nButtPrint=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call UnitMat(F2O,3)
      xomnp=-.2
      xomxp= .2
      n=1000
      deltap=(xomxp-xomnp)/float(n)
      xomn=xomnp
      xomx=xomxp
      nn=n
      Th2=120.
      Rad=215.
      DelR=12.
      DelX=12.
      DelS=25.
      DeltaD=-1.
      DeltaI=-1.
      ic=0
1500  idp=NextQuestId()
      xqd=220.
      il=5
      call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define new values',0,
     1                   LightGray,0,0)
      il=1
      tpom=5.
      t80='%2Theta'
      xpom=tpom+30.
      dpom=30.
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdw2Th=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Th2,.false.,.false.)
      il=il+1
      t80='%X-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelX=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelX,.false.,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      il=1
      t80='R%adius'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwRadius=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Rad,.false.,.false.)
      il=il+1
      t80='%S-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelS=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelS,.false.,.false.)
      il=2
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='%R-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelR=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelR,.false.,.false.)
      il=il+1
      xpom=5.
      tpom=xpom+CrwXd+3.
      t80='Use %primary-beam    Soller slit'
      call FeQuestCrwMake(idp,tpom,il,xpom,il,t80,'L',CrwXd,CrwYd,1,0)
      nCrwSollerI=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,DeltaI.gt.0.)
      il=il+1
      t80='Use %diffracted-beam Soller slit'
      call FeQuestCrwMake(idp,tpom,il,xpom,il,t80,'L',CrwXd,CrwYd,1,0)
      nCrwSollerD=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,DeltaD.gt.0.)
      il=il-1
      tpom=tpom+FeTxLengthUnder(t80)+2.
      t80='=>'
      xpom=tpom+FeTxLengthUnder(t80)+2.
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwSollerI=EdwLastMade
      il=il+1
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwSollerD=EdwLastMade
1600  if(CrwLogicQuest(nCrwSollerI)) then
        if(DeltaI.lt.0.) DeltaI=1.
        call FeQuestRealEdwOpen(nEdwSollerI,DeltaI,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwSollerI)
      endif
      if(CrwLogicQuest(nCrwSollerD)) then
        if(DeltaD.lt.0.) DeltaD=1.
        call FeQuestRealEdwOpen(nEdwSollerD,DeltaD,.false.,.false.)
      else
        call FeQuestEdwClose(nEdwSollerD)
      endif
      call FeQuestEvent(idp,ich)
      if(CheckType.eq.EventCrw) then
        go to 1600
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1600
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdw2Th,Th2)
        call FeQuestRealFromEdw(nEdwRadius,Rad)
        call FeQuestRealFromEdw(nEdwDelX,DelX)
        call FeQuestRealFromEdw(nEdwDelS,DelS)
        call FeQuestRealFromEdw(nEdwDelR,DelR)
        if(CrwLogicQuest(nCrwSollerI)) then
          call FeQuestRealFromEdw(nEdwSollerI,DeltaI)
        else
          DeltaI=-1.
        endif
        if(CrwLogicQuest(nCrwSollerD)) then
          call FeQuestRealFromEdw(nEdwSollerD,DeltaD)
        else
          DeltaD=-1.
        endif
      endif
      call FeQuestRemove(idp)
      if(ich.ne.0) go to 5500
      do i=-n/2,n/2
        Wx(i)=Th2+float(i)*deltap
      enddo
      call PwdAxialDiv(Th2,DelX,DelS,DelR,Rad,DeltaI*ToRad,.true.,
     1                 DeltaD*ToRad,.true.,
     2                 100,n+1,10.,deltap,Wdiv(-n/2))
      xomn=xomnp+th2
      xomx=xomxp+th2
      WIntMax=0.
      yomx=1.
      ic=ic+1
      if(mod(ic,3).eq.0) then
        Color=Yellow
      else if(mod(ic,3).eq.1) then
        Color=Red
      else if(mod(ic,3).eq.2) then
        Color=Green
      endif
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      if(ic.eq.1) call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      call FeXYPlot(Wx(-nn/2),Wint(-nn/2),nn,NormalLine,
     1              NormalPlotMode,Color)
      call FeReleaseOutput
      call FeDeferOutput
5300  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtContinue.or.CheckNumber.eq.nButtPrint)
     1    then
          if(CheckNumber.eq.nButtPrint) then
            call FePrintPicture(ich)
            if(ich.ne.0) go to 5300
          endif
          go to 5400
        else if(CheckNumber.eq.nButtNext) then
          go to 1500
        endif
      endif
      go to 5300
5400  xomn=-0.3
      xomx= 0.02
      yomn=-20.
      yomx= 20.
      call FeMakeAcFrame
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      nn=0
      Posun= -4.
      DelkaS=18.
      xpom=xomn
      dpom=(xomx-xomn)/float(n)
      do i=-n/2,n/2
        nn=nn+1
        Wx(i)=xpom
        WInt(i)= 50.*sqrt(-xpom)+Posun+DelkaS*.5
        WDiv(i)=-50.*sqrt(-xpom)+Posun+DelkaS*.5
        xpom=xpom+dpom
        if(xpom.gt.0.) then
          if(xpom.lt.dpom*.5) then
            xpom=0.
          else
            go to 5420
          endif
        endif
      enddo
5420  Color=White
      call FeHardCopy(HardCopy,'open')
      if(InvertWhiteBlack.or.(HardCopy.ne.HardCopyBMP.and.
     1                        HardCopy.ne.HardCopyPCX)) then
        call FeXYPlot(Wx(-n/2),Wint(-n/2),nn,NormalLine,
     1                NormalPlotMode,Color)
        call FeXYPlot(Wx(-n/2),Wdiv(-n/2),nn,NormalLine,
     1                NormalPlotMode,Color)
        jk=20
        jk=1
        do j=1,jk
        nn=0
        xpom=xomn
        dpom=(xomx-xomn)/float(n)
        do i=-n/2,n/2
          nn=nn+1
          Wx(i)=xpom
          WInt(i)= 50.*sqrt(-xpom)+Posun-DelkaS*.5
          WDiv(i)=-50.*sqrt(-xpom)+Posun-DelkaS*.5
          xpom=xpom+dpom
          if(xpom.gt.0.) then
            if(xpom.lt.dpom*.5) then
              xpom=0.
            else
              go to 5440
            endif
          endif
        enddo
5440    Color=White
        call FeXYPlot(Wx(-n/2),Wint(-n/2),nn,NormalLine,
     1                NormalPlotMode,Color)
        call FeXYPlot(Wx(-n/2),Wdiv(-n/2),nn,NormalLine,
     1                NormalPlotMode,Color)
        enddo
        Wx(1)=xomn
        Wx(2)=xomx
        Wint(1)= 8.
        Wint(2)= 8.
        call FeXYPlot(Wx(1),Wint(1),2,DashedLine,NormalPlotMode,White)
        Wint(1)=-8.
        Wint(2)=-8.
        call FeXYPlot(Wx(1),Wint(1),2,DashedLine,NormalPlotMode,White)
      endif
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
5450  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtContinue) then
          go to 5500
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.ne.0) go to 5450
          go to 5400
        else if(CheckNumber.eq.nButtNext) then
          go to 5500
        endif
      endif
      go to 5450
5500  call FeQuestRemove(id)
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
      subroutine PwdPrfTestDD
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension Wx(1000),Wy(1000),Wz(1000)
      character*80 SvFile
      character*80 t80
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,Black,
     1                      -1,-1)
      call FeMakeGrWin(0.,40.,14.,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      pom=YMaxGrWin
      dpom=ButYd+6.
      ypom=pom-dpom-2.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Next')
      nButtNext=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-9.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Continue')
      nButtContinue=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-9.
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Print')
      nButtPrint=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call UnitMat(F2O,3)
      Th2=120.
      Th2From=119.7
      Th2To=120.3
      Rad=215.
      DelR=12.
      DelX=12.
      DelS=25.
      DelEps=.01
      NBeta=20
      DelSmooth=.001
      ic=0
1500  idp=NextQuestId()
      xqd=220.
      il=5
      call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define new values',0,
     1                   LightGray,0,0)
      il=1
      tpom=5.
      t80='%2Theta'
      xpom=tpom+30.
      dpom=30.
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,1)
      nEdwTh2=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Th2,.false.,.false.)
      xpom=xpom+xqd/3.
      tpom=tpom+xqd/3.
      t80='%From'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwTh2From=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Th2From,.false.,.false.)
      xpom=xpom+xqd/3.
      tpom=tpom+xqd/3.
      t80='%To'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwTh2To=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Th2To,.false.,.false.)
      il=il+1
      tpom=5.
      xpom=tpom+30.
      t80='%X-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelX=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelX,.false.,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='%S-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelS=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelS,.false.,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='%R-Lenght'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelR=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelR,.false.,.false.)
      il=il+1
      tpom=5.
      xpom=tpom+30.
      t80='R%adius'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwRadius=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,Rad,.false.,.false.)
      il=il+1
      tpom=5.
      xpom=tpom+30.
      t80='N%Beta'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwNBeta=EdwLastMade
      call FeQuestIntEdwOpen(EdwLastMade,NBeta,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='Del%Eps'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwDelEps=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelEps,.false.,.false.)
      tpom=tpom+xqd/3.
      xpom=xpom+xqd/3.
      t80='Sm%ooth'
      call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
      nEdwOkenko=EdwLastMade
      call FeQuestRealEdwOpen(EdwLastMade,DelSmooth,.false.,.false.)
1600  call FeQuestEvent(idp,ich)
      if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwTh2) then
        call FeQuestRealFromEdw(nEdwTh2From,Th2From)
        call FeQuestRealFromEdw(nEdwTh2To,Th2To)
        pom=Th2
        call FeQuestRealFromEdw(nEdwTh2,Th2)
        pom=Th2-pom
        Th2From=Th2From+pom
        Th2To  =Th2To  +pom
        call FeQuestRealEdwOpen(nEdwTh2From,Th2From,.false.,.false.)
        call FeQuestRealEdwOpen(nEdwTh2To,Th2To,.false.,.false.)
        go to 1600
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1600
      endif
      if(ich.eq.0) then
        call FeQuestRealFromEdw(nEdwTh2,Th2)
        call FeQuestRealFromEdw(nEdwTh2From,Th2From)
        call FeQuestRealFromEdw(nEdwTh2To,Th2To)
        call FeQuestRealFromEdw(nEdwRadius,Rad)
        call FeQuestRealFromEdw(nEdwDelX,DelX)
        call FeQuestRealFromEdw(nEdwDelS,DelS)
        call FeQuestRealFromEdw(nEdwDelR,DelR)
        call FeQuestRealFromEdw(nEdwDelEps,DelEps)
        call FeQuestRealFromEdw(nEdwOkenko,DelSmooth)
        call FeQuestIntFromEdw(nEdwNBeta,NBeta)
      endif
      call FeQuestRemove(idp)
      if(ich.ne.0) go to 5400
      xomn=Th2From
      xomx=Th2To
      Wx(1)=Th2From
      N=nint((xomx-xomn)/DelEps)+1
      do i=2,N
        Wx(i)=Wx(i-1)+DelEps
      enddo
      call PwdAxialDivT(Th2,DelX,DelS,DelR,Rad,-1.,-1.,
     1                  NBeta,N,DelSmooth*ToRad,DelEps,Wx,Wy,Wz)
      yomn=0.
      yomx=0.
      do i=1,N
        yomx=max(yomx,Wy(i))
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      call FeXYPlot(Wx,Wy,N,NormalLine,NormalPlotMode,Green)
      call FeXYPlot(Wx,Wz,N,NormalLine,NormalPlotMode,Red)
      call FeReleaseOutput
      call FeDeferOutput
5300  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtContinue.or.CheckNumber.eq.nButtPrint)
     1    then
          go to 5400
        else if(CheckNumber.eq.nButtNext) then
          go to 1500
        endif
      endif
      go to 5300
5400  call FeQuestRemove(id)
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
      subroutine PwdAxialDivT(Th2,XLen,SLen,RLen,XRad,SollI,SollD,
     1                        NBeta,N,DelSmooth,EpsStep,Wx,Wy,Wz)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension Wy(*),Wx(*),Wz(*),Wh(1000),Wk(1000)
      real I2P,I2M,I2P0,I2M0
      SecTh2=1./cos(Th2*ToRad)
      TanTh2=tan(Th2*ToRad)
      Beta1=(SLen-XLen)*.5/XRad
      Beta2=(SLen+XLen)*.5/XRad
      if(SollI.gt.0) then
        pom=min(SollI*.5,Beta2)
      else
        pom=Beta2
      endif
      BetaStep=pom/float(NBeta)
      FK=2.*XRad**2*TanTh2
      Factor=abs(BetaStep)/(2.*Beta2)
      EpsFrom=-float(N/2)*EpsStep
      Beta=0.
      call SetRealArrayTo(Wy,N,0.)
      call SetRealArrayTo(Wz,N,0.)
      do IBeta=0,iabs(NBeta)
        if(SollI.gt.0.) then
          SI=PwdTriangleFunction(Beta,SollI*.5)
          if(SI.le.0.) go to 1900
        else
          SI=1.
        endif
        if(IBeta.eq.0) then
          FMult=Factor*SI
        else
          FMult=2.*Factor*SI
        endif
        if(Beta.ge.-Beta2.and.Beta.lt.Beta1) then
          Z0P=XLen*.5+Beta*XRad*(1.+SecTh2)
        else if(Beta.ge.Beta1.and.Beta.le.Beta2) then
          Z0P=SLen*.5+Beta*XRad*SecTh2
        else
          go to 1900
        endif
        if(Beta.ge.-Beta2.and.Beta.lt.-Beta1) then
          Z0M=-SLen*.5+Beta*XRad*SecTh2
        else if(Beta.ge.-Beta1.and.Beta.le.Beta2) then
          Z0M=-XLen*.5+Beta*XRad*(1.+SecTh2)
        else
          go to 1900
        endif
        Eps0=Beta**2*.5*tanTh2
        Eps1P=Eps0-(RLen*.5-Z0P)**2/FK
        Eps2P=Eps0-(RLen*.5-Z0M)**2/FK
        Eps1M=Eps0-(RLen*.5+Z0M)**2/FK
        Eps2M=Eps0-(RLen*.5+Z0P)**2/FK
        EpsDeg=EpsFrom
        Gamma0=Beta*SecTh2
        do i=1,N
          Eps=EpsDeg*ToRad
          if((th2-90.).le.0) then
            if(Beta.ge.0.) then
              call PwdAxialDivI2T(Eps,Eps0,Eps1P,Eps2P,Eps1M,Eps2M,RLen,
     1                            Z0P,Z0M,I2P,I2M,I2P0,I2M0,DelSmooth)
            else
              call PwdAxialDivI2T(Eps,Eps0,Eps1M,Eps2M,Eps1P,Eps2P,Rlen,
     1                            -Z0M,-Z0P,I2M,I2P,I2M0,I2P0,DelSmooth)
            endif
          else if((th2-90.).gt.0) then
            if(Beta.ge.0.) then
              call PwdAxialDivI2T(-Eps,-Eps0,-Eps1M,-Eps2M,-Eps1P,
     1                            -Eps2P,RLen,-Z0M,-Z0P,I2M,I2P,I2M0,
     2                            I2P0,DelSmooth)
            else
              call PwdAxialDivI2T(-Eps,-Eps0,-Eps1P,-Eps2P,-Eps1M,
     1                            -Eps2M,RLen,Z0P,Z0M,I2P,I2M,I2P0,I2M0,
     2                            DelSmooth)
            endif
          endif
          if(SollD.gt.0.) then
            GammaD=sqrt(2.*abs(tanTh2*(Eps0-Eps)))
            GammaP=Gamma0+GammaD
            GammaM=Gamma0-GammaD
            FP=PwdTriangleFunction(GammaP,SollD*.5)
            FM=PwdTriangleFunction(GammaM,SollD*.5)
          else
            FP=1.
            FM=1.
          endif
          Wy(i)=Wy(i)+(I2P*FP+I2M*FM)*FMult
          Wz(i)=Wz(i)+(I2P0*FP+I2M0*FM)*FMult
          Wh(i)=(I2P*FP+I2M*FM)*FMult
          Wk(i)=(I2P0*FP+I2M0*FM)*FMult
          EpsDeg=EpsDeg+EpsStep
        enddo
1900    Beta=Beta+BetaStep
        pom=0.
        do i=1,N
          pom=max(pom,Wh(i))
        enddo
        if(pom.gt.0.) then
          do i=1,N
            Wh(i)=Wh(i)/pom
            Wk(i)=Wk(i)/pom
          enddo
        endif
        xomn=Wx(1)
        xomx=Wx(N)
        yomn=0.
        yomx=1.
        call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
        call FeClearGrWin
        call FeMakeAcFrame
        call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
        call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
        call FeXYPlot(Wx,Wk,N,NormalLine,NormalPlotMode,Green)
        call FeXYPlot(Wx,Wh,N,NormalLine,NormalPlotMode,Red)
        call FeReleaseOutput
        call FeDeferOutput
        call FeWait(.2)
      enddo
      WySum=0.
      do i=1,N
        WySum=WySum+Wy(i)
      enddo
      pom=1./WySum
      do i=1,N
        Wy(i)=Wy(i)*pom
        Wz(i)=Wz(i)*pom
      enddo
      return
      end
      subroutine PwdAxialDivI2T(Eps,Eps0,Eps1P,Eps2P,Eps1M,Eps2M,RLen,
     1                          Z0P,Z0M,I2P,I2M,I2P0,I2M0,DelSmooth)
      include 'fepc.cmn'
      include 'basic.cmn'
      real I2P,I2M,I2P0,I2M0
      integer Case
      character*80 Veta
      Case=0
      I2P=0.
      I2M=0.
      I2P0=0.
      I2M0=0.
      if(RLen.ge.Z0P-Z0M) then
        if(Z0M.ge.-RLen*.5.and.Z0P.le.RLen*.5) then
          EpsA=Eps1P
          EpsB=Eps2P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=1
        else if(Z0M.le.RLen*.5.and.Z0P.gt.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=2
        else if(Z0M.ge.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=3
        else
          write(Veta,'(''Z0P:'',f8.3,'', Z0M:'',f8.3,'', Rlen:'',f8.3)')
     1      Z0P,Z0M,RLen
          call FeWinMessage('Siroka sterbina-podivnost-1',Veta)
        endif
      else
        if(Z0M.le.-RLen*.5.and.Z0P.ge.RLen*.5) then
          EpsA=Eps1M
          EpsB=Eps2P
          EpsC=Eps1P
          EpsD=Eps2M
          Case=1
        else if(Z0M.gt.-RLen*.5.and.Z0M.le.RLen*.5.and.Z0P.gt.RLen*.5)
     1    then
          EpsA=Eps2P
          EpsB=Eps1M
          EpsC=Eps1P
          EpsD=Eps2M
          Case=2
        else if(Z0M.ge.RLen*.5.and.Z0P.gt.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1M
          EpsC=Eps1P
          EpsD=Eps2M
          Case=3
        else
          write(Veta,'(''Z0P:'',f8.3,'', Z0M:'',f8.3,'', Rlen:'',f8.3)')
     1      Z0P,Z0M,RLen
          call FeWinMessage('Uzka sterbina-podivnost-1',Veta)
        endif
      endif
      EpsPlus =Eps+DelSmooth
      EpsMinus=Eps-DelSmooth
      if(Case.eq.1) then
        if(Eps.ge.EpsA.and.Eps.le.Eps0+DelSmooth) then
          I2P0=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsMinus.ge.EpsA) then
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsA,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsA,DelSmooth)
          endif
        else if(Eps.ge.EpsB-DelSmooth.and.Eps.le.EpsA) then
          I2P0=PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsPlus.ge.EpsA) then
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsA,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsA,DelSmooth)
          else if(EpsMinus.ge.EpsB) then
            I2P=PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2P=PwdAxialDivF2(EpsB,Eps0,EpsB,EpsPlus,DelSmooth)
          endif
        endif
        if(Eps.ge.EpsC.and.Eps.le.Eps0+DelSmooth) then
          I2M0=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          I2M0=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      else if(Case.eq.2) then
        if(Eps.ge.EpsA-DelSmooth.and.Eps.le.Eps0+DelSmooth) then
          I2P0=PwdAxialDivF2(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsMinus.ge.EpsA) then
            I2P=PwdAxialDivF2(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsA) then
            I2P=PwdAxialDivF2(EpsA,Eps0,EpsA,EpsPlus,DelSmooth)
          endif
        endif
        if(Eps.ge.EpsB.and.Eps.le.Eps0+DelSmooth) then
          I2M0=PwdAxialDivF3(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsMinus.ge.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          endif
        else if(Eps.ge.EpsC.and.Eps.le.EpsB) then
          I2M0=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsPlus.gt.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          else if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsMinus.le.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          I2M0=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      else if(Case.eq.3) then
        if(Eps.ge.EpsB.and.Eps.le.EpsA+DelSmooth) then
          I2M0=PwdAxialDivF4(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsMinus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          endif
        else if(Eps.ge.EpsC.and.Eps.le.EpsB) then
          I2M0=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          else if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          I2M0=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      endif
9999  return
      end
      function PwdTriangleFunction(x,xmax)
      include 'fepc.cmn'
      if(xmax.le.0.) then
        PwdTriangleFunction=1.
      else if(x.ge.-xmax.and.x.le.xmax) then
        PwdTriangleFunction=1.-abs(x)/xmax
      else
        PwdTriangleFunction=0.
      endif
      return
      end
      subroutine PwdAxialDiv(Th2,XLen,SLen,RLen,XRad,SollI,UseSollI,
     1                       SollD,UseSollD,
     2                       NBeta,N,DelSmooth,EpsStep,Wy)
      include 'fepc.cmn'
      include 'basic.cmn'
      logical UseSollI,UseSollD
      dimension Wy(*)
      real I2P,I2M
      SecTh2=1./cos(Th2*ToRad)
      TanTh2=tan(Th2*ToRad)
      Beta1=(SLen-XLen)*.5/XRad
      Beta2=(SLen+XLen)*.5/XRad
      if(UseSollI) then
        pom=min(SollI*.5,Beta2)
      else
        pom=Beta2
      endif
      BetaStep=pom/float(NBeta)
      FK=2.*XRad**2*TanTh2
      Factor=abs(BetaStep)/(2.*Beta2)
      EpsFrom=-float(N/2)*EpsStep
      Beta=0.
      call SetRealArrayTo(Wy,N,0.)
      do IBeta=0,iabs(NBeta)
        if(UseSollI) then
          SI=PwdTriangleFunction(Beta,SollI*.5)
          if(SI.le.0.) go to 1900
        else
          SI=1.
        endif
        if(IBeta.eq.0) then
          FMult=Factor*SI
        else
          FMult=2.*Factor*SI
        endif
        if(Beta.ge.-Beta2.and.Beta.lt.Beta1) then
          Z0P=XLen*.5+Beta*XRad*(1.+SecTh2)
        else if(Beta.ge.Beta1.and.Beta.le.Beta2) then
          Z0P=SLen*.5+Beta*XRad*SecTh2
        else
          go to 1900
        endif
        if(Beta.ge.-Beta2.and.Beta.lt.-Beta1) then
          Z0M=-SLen*.5+Beta*XRad*SecTh2
        else if(Beta.ge.-Beta1.and.Beta.le.Beta2) then
          Z0M=-XLen*.5+Beta*XRad*(1.+SecTh2)
        else
          go to 1900
        endif
        Eps0=Beta**2*.5*tanTh2
        Eps1P=Eps0-(RLen*.5-Z0P)**2/FK
        Eps2P=Eps0-(RLen*.5-Z0M)**2/FK
        Eps1M=Eps0-(RLen*.5+Z0M)**2/FK
        Eps2M=Eps0-(RLen*.5+Z0P)**2/FK
        EpsDeg=EpsFrom
        Gamma0=Beta*SecTh2
        do i=1,N
          Eps=EpsDeg*ToRad
          if((th2-90.).le.0) then
            if(Beta.ge.0.) then
              call PwdAxialDivI2(Eps,Eps0,Eps1P,Eps2P,Eps1M,Eps2M,RLen,
     1                           Z0P,Z0M,I2P,I2M,DelSmooth)
            else
              call PwdAxialDivI2(Eps,Eps0,Eps1M,Eps2M,Eps1P,Eps2P,Rlen,
     1                           -Z0M,-Z0P,I2M,I2P,DelSmooth)
            endif
          else if((th2-90.).gt.0) then
            if(Beta.ge.0.) then
              call PwdAxialDivI2(-Eps,-Eps0,-Eps1M,-Eps2M,-Eps1P,-Eps2P,
     1                           RLen,-Z0M,-Z0P,I2M,I2P,DelSmooth)
            else
              call PwdAxialDivI2(-Eps,-Eps0,-Eps1P,-Eps2P,-Eps1M,-Eps2M,
     1                           RLen,Z0P,Z0M,I2P,I2M,DelSmooth)
            endif
          endif
          if(UseSollD) then
            GammaD=sqrt(2.*abs(tanTh2*(Eps0-Eps)))
            GammaP=Gamma0+GammaD
            GammaM=Gamma0-GammaD
            FP=PwdTriangleFunction(GammaP,SollD*.5)
            FM=PwdTriangleFunction(GammaM,SollD*.5)
          else
            FP=1.
            FM=1.
          endif
          Wy(i)=Wy(i)+(I2P*FP+I2M*FM)*FMult
          EpsDeg=EpsDeg+EpsStep
        enddo
1900    Beta=Beta+BetaStep
      enddo
      WySum=0.
      do i=1,N
        WySum=WySum+Wy(i)
      enddo
      if(WySum.gt.0.) then
        pom=1./WySum
        do i=1,N
          Wy(i)=Wy(i)*pom
        enddo
      endif
      return
      end
      subroutine PwdAxialDivI2(Eps,Eps0,Eps1P,Eps2P,Eps1M,Eps2M,RLen,
     1                         Z0P,Z0M,I2P,I2M,DelSmooth)
      include 'fepc.cmn'
      include 'basic.cmn'
      real I2P,I2M
      integer Case
      character*80 Veta
      Case=0
      I2P=0.
      I2M=0.
      if(RLen.ge.Z0P-Z0M) then
        if(Z0M.ge.-RLen*.5.and.Z0P.le.RLen*.5) then
          EpsA=Eps1P
          EpsB=Eps2P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=1
        else if(Z0M.le.RLen*.5.and.Z0P.gt.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=2
        else if(Z0M.ge.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1P
          EpsC=Eps1M
          EpsD=Eps2M
          Case=3
        else
          write(Veta,'(''Z0P:'',f8.3,'', Z0M:'',f8.3,'', Rlen:'',f8.3)')
     1      Z0P,Z0M,RLen
          call FeWinMessage('Siroka sterbina-podivnost-2',Veta)
        endif
      else
        if(Z0M.le.-RLen*.5.and.Z0P.ge.RLen*.5) then
          EpsA=Eps1M
          EpsB=Eps2P
          EpsC=Eps1P
          EpsD=Eps2M
          Case=1
        else if(Z0M.gt.-RLen*.5.and.Z0M.le.RLen*.5.and.Z0P.gt.RLen*.5)
     1    then
          EpsA=Eps2P
          EpsB=Eps1M
          EpsC=Eps1P
          EpsD=Eps2M
          Case=2
        else if(Z0M.ge.RLen*.5.and.Z0P.gt.RLen*.5) then
          EpsA=Eps2P
          EpsB=Eps1M
          EpsC=Eps1P
          EpsD=Eps2M
          Case=3
        else
          write(Veta,'(''Z0P:'',f8.3,'', Z0M:'',f8.3,'', Rlen:'',f8.3)')
     1      Z0P,Z0M,RLen
          call FeWinMessage('Uzka sterbina-podivnost-2',Veta)
        endif
      endif
      EpsPlus =Eps+DelSmooth
      EpsMinus=Eps-DelSmooth
      if(Case.eq.1) then
        if(Eps.gt.EpsA.and.Eps.le.Eps0+DelSmooth) then
          if(EpsMinus.ge.EpsA) then
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsA,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsA,DelSmooth)
          endif
        else if(Eps.ge.EpsB-DelSmooth.and.Eps.le.EpsA) then
          if(EpsPlus.ge.EpsA) then
            I2P=PwdAxialDivF1(EpsA,EpsB,Eps0,EpsA,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsA,DelSmooth)
          else if(EpsMinus.ge.EpsB) then
            I2P=PwdAxialDivF2(EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2P=PwdAxialDivF2(EpsB,Eps0,EpsB,EpsPlus,DelSmooth)
          endif
        endif
        if(Eps.gt.EpsC.and.Eps.le.Eps0+DelSmooth) then
          if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      else if(Case.eq.2) then
        if(Eps.ge.EpsA-DelSmooth.and.Eps.le.Eps0+DelSmooth) then
          if(EpsMinus.ge.EpsA) then
            I2P=PwdAxialDivF2(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsA) then
            I2P=PwdAxialDivF2(EpsA,Eps0,EpsA,EpsPlus,DelSmooth)
          endif
        endif
        if(Eps.gt.EpsB.and.Eps.le.Eps0+DelSmooth) then
          if(EpsMinus.ge.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          endif
        else if(Eps.gt.EpsC.and.Eps.le.EpsB) then
          if(EpsPlus.gt.EpsB) then
            I2M=PwdAxialDivF3(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          else if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsMinus.le.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      else if(Case.eq.3) then
        if(Eps.gt.EpsB.and.Eps.le.EpsA+DelSmooth) then
          if(EpsMinus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          endif
        else if(Eps.gt.EpsC.and.Eps.le.EpsB) then
          if(EpsPlus.ge.EpsB) then
            I2M=PwdAxialDivF4(EpsA,Eps0,EpsB,EpsPlus,DelSmooth)+
     1          PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsB,DelSmooth)
          else if(EpsMinus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          endif
        else if(Eps.ge.EpsD-DelSmooth.and.Eps.le.EpsC) then
          if(EpsPlus.ge.EpsC) then
            I2M=PwdAxialDivF1(EpsC,EpsD,Eps0,EpsC,EpsPlus,DelSmooth)+
     1          PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsC,DelSmooth)
          else if(EpsMinus.ge.EpsD) then
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsMinus,EpsPlus,DelSmooth)
          else
            I2M=PwdAxialDivF2(EpsD,Eps0,EpsD,EpsPlus,DelSmooth)
          endif
        endif
      endif
      return
      end
      function PwdAxialDivF1(EpsA,EpsB,Eps0,EpsMinus,EpsPlus,DelSmooth)
      include 'fepc.cmn'
      EpsP=min(Eps0,EpsPlus)
      if(Eps0-EpsA.gt.0..and.Eps0-EpsB.gt.0.) then
        PwdAxialDivF1=PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                (sqrt(Eps0-EpsB)-sqrt(Eps0-EpsA))
      else if(Eps0-EpsB.gt.0.) then
        PwdAxialDivF1=PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                sqrt(Eps0-EpsB)
      else
        PwdAxialDivF1=0.
        go to 9999
      endif
      if(DelSmooth.ne.0.) then
        PwdAxialDivF1=.5*(EpsP-EpsMinus)/DelSmooth*PwdAxialDivF1
      else
        PwdAxialDivF1=0.
      endif
9999  return
      end
      function PwdAxialDivF2(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
      include 'fepc.cmn'
      EpsP=min(Eps0,EpsPlus)
      if((Eps0-EpsA).gt.0.) then
        PwdAxialDivF2=-1.+PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                    sqrt(Eps0-EpsA)
      else
        PwdAxialDivF2=0.
        go to 9999
      endif
      if(DelSmooth.ne.0.) then
        PwdAxialDivF2=.5*(EpsP-EpsMinus)/DelSmooth*PwdAxialDivF2
      else
        PwdAxialDivF2=0.
      endif
9999  return
      end
      function PwdAxialDivF3(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
      include 'fepc.cmn'
      EpsP=min(Eps0,EpsPlus)
      if((Eps0-EpsA).gt.0.) then
        PwdAxialDivF3=1.+PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                   sqrt(Eps0-EpsA)
      else
        PwdAxialDivF3=0.
        go to 9999
      endif
      if(DelSmooth.ne.0.) then
        PwdAxialDivF3=.5*(EpsP-EpsMinus)/DelSmooth*PwdAxialDivF3
      else
        PwdAxialDivF3=0.
      endif
9999  return
      end
      function PwdAxialDivF4(EpsA,Eps0,EpsMinus,EpsPlus,DelSmooth)
      include 'fepc.cmn'
      EpsP=min(Eps0,EpsPlus)
      if((Eps0-EpsA).gt.0.) then
        PwdAxialDivF4=1.-PwdAxialDivFB(Eps0,EpsMinus,EpsP)*
     1                   sqrt(Eps0-EpsA)
      else
        PwdAxialDivF4=0.
        go to 9999
      endif
      if(DelSmooth.ne.0.) then
        PwdAxialDivF4=.5*(EpsP-EpsMinus)/DelSmooth*PwdAxialDivF4
      else
        PwdAxialDivF4=0.
      endif
9999  return
      end
      function PwdAxialDivFB(Eps0,EpsMinus,EpsPlus)
      include 'fepc.cmn'
      include 'basic.cmn'
      pom1=max(Eps0-EpsMinus,0.)
      pom2=max(Eps0-EpsPlus,0.)
      pom1=sqrt(pom1)
      pom2=sqrt(pom2)
      if(pom1+pom2.gt.0.) then
        PwdAxialDivFB=2./(pom1+pom2)
      else
        PwdAxialDivFB=0.
      endif
      return
      end
      subroutine PwdSetLamProfile(KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension isel(2)
      iuz=0
      poml=0.
      do j=1,NAlfa(KDatB)
        pom=0.
        do i=1,NLamSpect(KDatB)
          if(i.ne.iuz.and.LamSpectRel(i,KDatB).gt.pom) then
            k=i
            pom=LamSpectRel(i,KDatB)
          endif
          if(j.eq.1)
     1      poml=poml+LamSpectRel(i,KDatB)*LamSpectLen(i,KDatB)
        enddo
        iuz=k
        isel(j)=k
        LamPwd(j,KDatB)=LamSpectLen(k,KDatB)
      enddo
      LamAvePwd(KDatB)=poml
      DelLam=.00001
      do n=1,NAlfa(KDatB)
        call SetRealArrayTo(LamProfilePwd(-5000,n,KDatB),10001,0.)
        poml1=LamSpectLen(isel(n),KDatB)
        if(NAlfa(KDatB).gt.1) then
          poml2=LamSpectLen(isel(3-n),KDatB)
        else
          poml2=333333.
        endif
        StredLam=poml1
        do 1040i=1,NLamSpect(KDatB)
          SpectRel=LamSpectRel(i,KDatB)
          poml=LamSpectLen(i,KDatB)
          SpectLife=LamSpectLife(i,KDatB)*.001
          if(NAlfa(KDatB).gt.1) then
            if(abs(poml-poml1).gt.abs(poml-poml2)) go to 1040
          endif
          poml=StredLam-poml
          do j=-5000,5000
            pom=1.+(2.*(poml+float(j)*DelLam)/SpectLife)**2
            pom=2.*LamSpectRel(i,KDatB)/(pi*pom*SpectLife)
            LamProfilePwd(j,n,KDatB)=LamProfilePwd(j,n,KDatB)+pom
          enddo
1040    continue
        Suma=0.
        do j=-5000,5000
          Suma=Suma+LamProfilePwd(j,n,KDatB)*.000001
        enddo
        if(n.eq.1) then
          LamRatPwd(KDatB)=1./Suma
        else
          LamRatPwd(KDatB)=Suma*LamRatPwd(KDatB)
        endif
      enddo
      return
      end
      subroutine KresliLamProfile
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80 SvFile
      dimension Wy(-500:500,10),Wx(-500:500)
      SvFile='jcnt'
      if(OpSystem.le.0) call CreateTmpFile(SvFile,i,1)
      call FeSaveImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(40.,20.,30.,30.)
      call UnitMat(F2O,3)
      DelLam=.00001
      StredLam=1.542
      xomn= 100.
      xomx=-100.
      yomn= 0.
      yomx=-100.
      k=NLamSpect(KDatBlock)
      do i=1,NLamSpect(KDatBlock)
        SpectRel=LamSpectRel(i,KDatBlock)
        poml=LamSpectLen(i,KDatBlock)
        SpectLife=LamSpectLife(i,KDatBlock)*.001
        do j=-500,500
          wx(j)=StredLam+float(j)*DelLam
          xomn=min(xomn,wx(j))
          xomx=max(xomx,wx(j))
            pom=1.+(2.*(StredLam-poml+float(j)*DelLam)/SpectLife)**2
            pom=2.*LamSpectRel(i,KDatBlock)/(pi*pom*SpectLife)
            wy(j,i)=pom
          yomx=max(yomx,wy(j,i))
        enddo
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'th')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Int')
      do i=1,k
        if(i.eq.1) then
          j=Blue
        else if(i.eq.2) then
          j=Green
        else if(i.eq.3) then
          j=Khaki
        else if(i.eq.4) then
          j=Red
        else if(i.eq.5) then
          j=Magenta
        endif
        if(i.lt.k+1) then
          call FeXYPlot(Wx(-500),Wy(-500,i),1001,NormalLine,
     1                  NormalPlotMode,j)
        else
        endif
      enddo
      call FeReleaseOutput
      call FeDeferOutput
1046  call FeEvent(0)
      if(EventType.ne.EventKey.or.EventNumber.ne.JeEscape) go to 1046
1050  continue
      call FeMakeGrWin(0.,40.,22.,10.)
      call FeMakeAcWin(0.,0.,0.,3.)
      call UnitMat(F2O,3)
      call FeSetTransXo2X(0.,40.,22.,0.,.false.)
      call FeLoadImage(XMinBasWin,XMaxBasWin,YMinBasWin,YMaxBasWin,
     1                 SvFile,0)
      return
      end
      subroutine PwdLeBail(Klic,Info)
      use Basic_mod
      use Atoms_mod
      use Powder_mod
      use Refine_mod
      use RefPowder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension hh(3),ih(6),IHAr(6,*),MltAr(*),YPeakP(2)
      character*80 RefFileName,t80
      character*75 TextInfoSave(3)
      integer FlgAr(*),KPhAr(*),FeGetSystemTime,StartTimeP
      integer UseTabsIn
      logical CalcDerSave,SatFrModOnly(MxPhases),Generovat,Info,Show
      real LpCorrection,LeBailRFp,LpArg
      save FScal
      Generovat=.true.
      go to 1100
      entry PwdLeBailFromHKL(Klic,Info,IHAr,FlgAr,KPhAr,MltAr,NRef)
      Generovat=.false.
1100  UseTabsIn=UseTabs
      CalcDerSave=CalcDer
      CalcDer=.false.
      ln=0
      RefFileName=fln(:ifln)//'.l0'//Cifry(KDatBlock+1:KDatBlock+1)
      NInfoSave=NInfo
      write(FormRfl(2:2),'(i1)') NDim(KPhase)
      do i=1,3
        TextInfoSave(i)=TextInfo(i)
      enddo
      call PwdSetTOF(KDatBlock)
      if(isTOF.or.isED) then
        FScal=1.
      else
        FScal=1./ToRad
      endif
      if(klic.le.2) then
        if(Generovat) then
          if(Klic.eq.0) then
            if(isTOF) then
              SinTOFThUse=sin(.5*TOFTTh(KDatBlock)*ToRad)
            else if(.not.isED) then
              LPTypeCorrUse=LpFactor(KDatBlock)
              FractPerfMonUse=FractPerfMon(KDatBlock)
              if(LPTypeCorrUse.eq.PolarizedGuinier) then
                snm=sin(AlphaGMon(KDatBlock)*ToRad)
                BetaGMonUse=BetaGMon(KDatBlock)*ToRad
              else
                snm=sin(AngleMon(KDatBlock)*ToRad)
              endif
              Cos2ThMon =1.-2.*snm**2
              Cos2ThMonQ=Cos2ThMon**2
            endif
          endif
          if(Klic.le.1) call PwdM92Nacti
          call OpenFile(91,RefFileName,'formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          NLamPwd=0
          n=npnts
          if(isTOF) then
            do i=1,npnts
              if(YfPwd(i).eq.1) go to 1150
            enddo
            i=1
1150        if(i.gt.1) i=i-1
            stolmx=.5/PwdTOF2D(XPwd(i))
            XPwdMin(KDatBlock)=XPwd(i)
            do i=npnts,1,-1
              if(YfPwd(i).eq.1) then
                XPwdMax(KDatBlock)=XPwd(i)
                exit
              endif
            enddo
          else if(isED) then
            do i=NPnts,1,-1
              if(YfPwd(i).eq.1) go to 1250
            enddo
            i=NPnts
1250        stolmx=.5*XPwd(i)/EDEnergy2D(KDatBlock)
            XPwdMax(KDatBlock)=XPwd(i)
            XPwdMin(KDatBlock)=0.
            do i=1,npnts
              if(YfPwd(i).eq.1) then
                if(i.gt.1) XPwdMin(KDatBlock)=XPwd(i)
                exit
              endif
            enddo
          else
            do i=1,3
              ih(i)=i
              ih(i+3)=0
            enddo
            RadLast=XPwd(npnts)*ToRad
            pom=RadLast*.5
            do KPh=1,NPhase
              KPhase=KPh
              call FromIndSinthl(ih,hh,sinthl,sinthlq,1,0)
              NRefPwd=1
              NLamPwd=1
              if(.not.allocated(ihPwdArr)) then
                n=1
                m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
                l=NAlfa(KDatBlock)
                allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),
     1                   MultPwdArr(n),iqPwdArr(n),KPhPwdArr(n),
     2                   ypeaka(l,n),peaka(l,n),pcota(l,n),rdega(2,l,n),
     3                   shifta(l,n),fnorma(l,n),tntsima(l,n),
     4                   ntsima(l,n),sqsga(l,n),fwhma(l,n),sigpa(l,n),
     5                   etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     6                   dfwdga(l,n),dfwdla(l,n),coef(m,l,n),
     7                   coefp(m,l,n),coefq(2,l,n),Prof0(n),
     8                   cotg2tha(l,n),cotgtha(l,n),cos2tha(l,n),
     9                   cos2thqa(l,n),Alpha12a(l,n),Beta12a(l,n),
     a                   IBroadHKLa(n),stat=i)
                if(i.ne.0) then
                    call FeAllocErr('Error occur in "PwdLeBail" '//
     1                              'point#1.',i)
                  go to 9999
                endif
                if(.not.isTOF.and..not.isED.and.
     1             KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
                  allocate(AxDivProfA(-25:25,l,n),
     1                     DerAxDivProfA(-25:25,7,l,n),
     2                     FDSProf(-25:25,l,n),stat=i)
                  if(i.ne.0) then
                    call FeAllocErr('Error occur in "PwdLeBail" '//
     1                              'point#2.',i)
                    go to 9999
                  endif
                endif
              endif
1400          call SetProfFun(ih,hh,pom)
              if(pom/ToRad.lt.89.98) then
                if(RadLast.ge.rdeg(1)) then
                  pom=pom+.1*ToRad
                  go to 1400
                endif
              endif
            enddo
            pom=min(pom+1.*ToRad,89.99*ToRad)
            stolmx=sin(pom)/LamPwd(1,KDatBlock)
            XPwdMax(KDatBlock)=2.*pom*FScal
            if(NAlfa(KDatBlock).gt.1)
     1        stolmx=max(stolmx,
     2                   stolmx*
     3                   LamPwd(1,KDatBlock)/LamPwd(2,KDatBlock))
            do i=npnts,1,-1
              if(YfPwd(i).eq.1) then
                if(i.lt.npnts) XPwdMax(KDatBlock)=XPwd(i)+.2
                exit
              endif
            enddo
            XPwdMin(KDatBlock)=0.
            do i=1,npnts
              if(YfPwd(i).eq.1) then
                if(i.gt.1) XPwdMin(KDatBlock)=XPwd(i)
                exit
              endif
            enddo
          endif
          if(NAtCalc.gt.0.and.DoLeBail.eq.0.or.isimul.gt.0) then
            do i=1,NPhase
              SatFrModOnly(i)=SatFrMod(i,KDatBlock)
            enddo
          else
            call SetLogicalArrayTo(SatFrModOnly,NPhase,.false.)
          endif
          call GenerRefPwd('Generation of reflections',stolmx,
     1                     MMaxPwd(1,1,KDatBlock),
     2                     SatFrModOnly,SkipFriedel,0)
          if(Console.or.BatchMode) then
            call FeFlowChartRemove
            call FeLstWriteLine(' ',-1)
          endif
          if(ErrFlag.ne.0) go to 9999
          n=0
          rewind 91
1500      n=n+1
          read(91,format91pow,end=1550)(ih(i),i=1,6)
          if(ih(1).gt.900) go to 1550
          go to 1500
1550      NRef91(KDatBlock)=n-1
        else
          n=NRef
        endif
        if(NAlfa(KDatBlock).gt.1) n=2*n
        call FeMouseShape(3)
      else
        RefFileName=fln(:ifln)//'.l0'//Cifry(KDatBlock+1:KDatBlock+1)
        call OpenFile(91,RefFileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        n=(NRef91(KDatBlock)+1)*NAlfa(KDatBlock)
      endif
      if((NAtCalc.gt.0.and.DoLeBail.eq.0.and.klic.ge.2).or.isimul.gt.0)
     1  go to 9999
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa,
     6             stat=i)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf,stat=i)
      NLamPwd=0
      LeBailFast=.false.
      n=n+1
      m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
      l=NAlfa(KDatBlock)
      allocate(ihPwdArr(maxNDim,n),FCalcPwdArr(n),MultPwdArr(n),
     1         iqPwdArr(n),KPhPwdArr(n),ypeaka(l,n),peaka(l,n),
     2         pcota(l,n),rdega(2,l,n),shifta(l,n),fnorma(l,n),
     3         tntsima(l,n),ntsima(l,n),sqsga(l,n),fwhma(l,n),
     4         sigpa(l,n),etaPwda(l,n),dedffga(l,n),dedffla(l,n),
     5         dfwdga(l,n),dfwdla(l,n),coef(m,l,n),coefp(m,l,n),
     6         coefq(2,l,n),Prof0(n),cotg2tha(l,n),cotgtha(l,n),
     7         cos2tha(l,n),cos2thqa(l,n),Alpha12a(l,n),
     8         Beta12a(l,n),IBroadHKLa(n),stat=i)
      if(i.ne.0) then
        call FeAllocErr('Error occur in "PwdLeBail" point#3.',i)
        go to 9999
      endif
      if(.not.isTOF.and..not.isED.and.
     1   KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        allocate(AxDivProfA(-25:25,l,n),DerAxDivProfA(-25:25,7,l,n),
     1           FDSProf(-25:25,l,n),stat=i)
        if(i.ne.0) then
          call FeAllocErr('Error occur in "PwdLeBail" point#4.',i)
          go to 9999
        endif
      endif
      allocate(BraggArr(n),ShiftArr(n),ypeakArr(n),indArr(6,n),
     1         MultArr(n),KPhArr(n),RDeg1Arr(n),RDeg2Arr(n),ICalcArr(n),
     2         CutCoef1Arr(n),CutCoef2Arr(n),FWHMArr(n),stat=i)
      if(i.ne.0) then
        call FeAllocErr('Error occur in "PwdLeBail" point#5.',i)
        go to 9999
      endif
      if(Generovat) rewind 91
      call PwdMakeSkipFlags
      LeBailNBragg=0
      NArr=0
2100  LeBailNBragg=LeBailNBragg+1
2150  if(Generovat) then
        read(91,format91pow,end=2200)(indArr(i,LeBailNBragg),i=1,6),
     1                                ICalcArr(LeBailNBragg),pom,i,i,KPh
        if(indArr(1,LeBailNBragg).gt.900) go to 2200
      else
        NArr=NArr+1
        if(NArr.gt.NRef) go to 2200
        call CopyVekI(IHAr(1,NArr),indArr(1,LeBailNBragg),6)
        if(FlgAr(NArr).ne.1.or.IHAr(1,NArr).gt.900) go to 2150
        KPh=KPhAr(NArr)
        pom=MltAr(NArr)
      endif
      KPhase=KPh
      call FromIndSinthl(indArr(1,LeBailNBragg),hh,sinthl,sinthlq,1,0)
      MultArr(LeBailNBragg)=pom
      KPhArr(LeBailNBragg)=KPh
      if(isTOF) then
        BraggArr(LeBailNBragg)=PwdD2TOF(.5/sinthl)
      else if(isED) then
        BraggArr(LeBailNBragg)=EDEnergy2D(KDatBlock)*2.*sinthl
      else
        poms=sinthl*LamPwd(1,KDatBlock)
        if(poms.lt..9999) then
          BraggArr(LeBailNBragg)=2.*asin(poms)
        else
          LeBailNBragg=LeBailNBragg-1
          go to 2100
        endif
      endif
      if(NAlfa(KDatBlock).gt.1.and..not.isTOF.and..not.isED) then
        call CopyVekI(indArr(1,LeBailNBragg),indArr(1,LeBailNBragg+1),
     1                NDim(KPh))
        LeBailNBragg=LeBailNBragg+1
        MultArr(LeBailNBragg)=pom
        KPhArr(LeBailNBragg)=KPh
        ICalcArr(LeBailNBragg)=ICalcArr(LeBailNBragg-1)
        poms=sinthl*LamPwd(2,KDatBlock)
        if(poms.lt..9999) then
          BraggArr(LeBailNBragg)=2.*asin(poms)
        else
          LeBailNBragg=LeBailNBragg-2
          go to 2100
        endif
      endif
      go to 2100
2200  LeBailNBragg=LeBailNBragg-1
      call CloseIfOpened(91)
      call FeReleaseOutput
      call FeDeferOutput
      call PwdGetAllBackground
      do i=1,LeBailNBragg
        if(KKLeBail(KDatBlock).eq.1.and.NPhase.gt.1.and.KPhArr(i).gt.1
     1     .and.(NAtIndLenAll(KPhArr(i)).gt.0.or.
     2           NAtPosLenAll(KPhArr(i)).gt.0)) then
          pom=ICalcArr(i)
        else
          pom=10.
        endif
        ypeakArr(i)=pom
        if(NAlfa(KDatBlock).eq.2.and.mod(i,2).eq.0)
     1    ypeakArr(i)=pom*LamRat(KDatBlock)
      enddo
      Ninfo=3
      Show=Info.and.Klic.le.1
      StartTimeP=FeGetSystemTime()
      call PwdSetPoints(Show,StartTimeP)
      if(ErrFlag.ne.0) go to 9999
      if(Info) then
        if(NDatBlock.gt.1) then
          TextInfo(1)='LeBail decomposition - '//
     1      DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))
        else
          TextInfo(1)='      LeBail decomposition'
        endif
      endif
      if(Show) then
        ip=0
      else
        ip=2
      endif
      LeBailFast=.true.
      do i=1,500
        LeBailRFp=LeBailRF
        call PwdSetIobs
        call PwdSetPoints(Show,StartTimeP)
        pom=LeBailRFp/LeBailRF
        if(Info.and..not.Show) then
          Show=FeGetSystemTime()-StartTimeP.gt.5000
          ip=i
        endif
        if(Show) then
          if(i.ge.ip) then
            write(TextInfo(2),101) i,LeBailRF
            write(TextInfo(3),102) pom
            call FeMsgShow(3,-1.,300.,i.eq.ip)
          endif
        endif
        if(pom.lt.1.001) go to 2300
      enddo
      i=500
2300  if(Show) then
        if(Klic.le.1) call FeWait(1.)
        if(i.ge.ip) call FeMsgClear(3)
      endif
      pom=0.
      do i=1,LeBailNBragg
        pom=max(pom,ypeakArr(i))
      enddo
      scPwd(KDatBlock)=sqrt(pom)
      if(NAtAll.le.0) then
        sc(1,KDatBlock)=scPwd(KDatBlock)
        call iom40only(1,0,fln(:ifln)//'.m40')
        call SetMag(0)
      endif
      if((NAtCalc.gt.0.and.DoLeBail.eq.0.and.klic.ge.2).or.isimul.gt.0)
     1  go to 9999
      if(Klic.le.1) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.prf','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        if(NDatBlock.gt.1)
     1    write(Ln,104)
     2      DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),
     3      'begin'
        if(isTOF) then
          if(TOFInD) then
            i=2
          else
            i=1
          endif
        else if(isED) then
          i=3
        else
          i=0
        endif
        write(Ln,'(20i5)') 2,NAlfa(KDatBlock)-1,i,NPhase,
     1                     (NDim(i),i=1,NPhase)
        do j=1,LeBailNBragg,NAlfa(KDatBlock)
          BraggPom=BraggArr(j)*FScal
          if(BraggPom.lt.XPwdMin(KDatBlock).or.
     1       BraggPom.gt.XPwdMax(KDatBlock)) cycle
          call SetPref(indArr(1,j),pref)
          pom=MultArr(j)*pref
          do k=1,NAlfa(KDatBlock)
            jk=j+k-1
            if(isTOF) then
              LpArg=PwdTOF2D(BraggArr(jk))
            else if(isED) then
              LpArg=1.
            else
              LpArg=BraggArr(jk)*.5
            endif
            YPeakP(k)=ypeakArr(jk)*pom/LpCorrection(LpArg)
          enddo
          write(ln,PrfFormat)(indArr(i,j),i=1,maxNDim),MultArr(j),
     1      KPhArr(j),
     2      (BraggArr(k)*FScal,ShiftArr(k)*FScal,FWHMArr(k)*FScal,
     3       ypeakp(k-j+1),k=j,j+NAlfa(KDatBlock)-1)
        enddo
        write(ln,'('' 999'')')
        do i=1,npnts
          if(isTOF.or.isED) then
            pom=0.
          else
            th=XPwd(i)*ToRad
            csth=cos(th*.5)
            sin2th=sin(th)
            shift=(shiftPwd(1,KDatBlock)+shiftPwd(2,KDatBlock)*csth+
     1             shiftPwd(3,KDatBlock)*sin2th)*.01
            pom=th/ToRad+shift
          endif
          write(Ln,PrfPointsFormat)
     1      XPwd(i),YoPwd(i),YcPwd(i),YsPwd(i),pom,YfPwd(i),YiPwd(i),
     2      (YcPwdInd(i,j),j=1,NPhase),YbPwd(i)
        enddo
        write(ln,'(''999.'')')
        if(NDatBlock.gt.1) write(Ln,104)
     1    DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock))),'end'
        call CloseIfOpened(ln)
      endif
      if(scPwd(KDatBlock).gt..000001) then
        ln=NextLogicNumber()
        call OpenFile(91,RefFileName,'formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
        call OpenFile(ln,fln(:ifln)//'.m91','formatted','unknown')
        if(ErrFlag.ne.0) go to 9999
!        nn=LeBailLastRef
!        do i=LeBailLastRef,1,-1
!          if(ypeakArr(i).gt.0.) exit
!          nn=i
!        enddo
        nn=LeBailNBragg
        n=0
        do i=1,nn,NAlfa(KDatBlock)
          BraggPom=BraggArr(i)*FScal
          if(BraggPom.lt.XPwdMin(KDatBlock).or.
     1       BraggPom.gt.XPwdMax(KDatBlock)) cycle
          pom=ypeakArr(i)/(scPwd(KDatBlock)*.01)**2
          n=n+1
          write(91,format91pow)(indArr(j,i),j=1,6),pom,MultArr(i),1,0,
     1                          KPhArr(i)
          write(ln,format91pow)(indArr(j,i),j=1,6),pom,1.,1,0,KPhArr(i)
        enddo
        write(91,'('' 999'')')
        write(ln,'('' 999'')')
        call CloseIfOpened(ln)
        call CloseIfOpened(91)
        NRef91(KDatBlock)=n+1
      else
        call FeChybne(-1.,YBottomMessage,'Le Bail decomposition '//
     1                'wasn''t successful.',
     2                'check background and Bragg peak positions.',
     3                SeriousError)
        ErrFlag=1
        go to 9999
      endif
      if(klic.le.1) then
        if(klic.eq.1) then
          KPhase=KPhaseBasic
          ln=NextLogicNumber()
          call OpenFile(ln,fln(:ifln)//'.rfl','formatted','unknown')
          if(ErrFlag.ne.0) go to 9999
          ik=LeBailLastRef
          if(NAlfa(KDatBlock).eq.2) ik=ik-1
          do i=ik,1,-NAlfa(KDatBlock)
            if(ypeakArr(i)/(scPwd(KDatBlock)*.01)**2.gt..0001) then
              nn=i
              exit
            endif
          enddo
          do 3000i=1,nn,NAlfa(KDatBlock)
            if(KPhArr(i).ne.KPhase) go to 3000
            peak=BraggArr(i)
            call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
            if(isTOF.or.isED) then
              th=peak
            else
              th=peak*.5
              peak=peak/torad
              do j=1,NSkipPwd(KDatBlock)
                if(peak.ge.SkipPwdFr(j,KDatBlock).and.
     1             peak.le.SkipPwdTo(j,KDatBlock)) go to 3000
              enddo
            endif
            NRefPwd=(i-1)/NAlfa(KDatBlock)+1
            NLamPwd=mod(i-1,NAlfa(KDatBlock))+1
            call SetProfFun(indArr(1,i),hh,th)
            pom=ypeakArr(i)/(scPwd(KDatBlock)*.01)**2
            if(isTOF.or.isED) then
              pomw=fwhm
            else
              pomw=fwhm/torad
            endif
            write(ln,FormRfl)(indArr(j,i),j=1,NDim(KPhase)),pom,pomw
3000      continue
        endif
        close(91,status='delete')
      else
        rewind 91
      endif
      go to 9999
9999  call CloseIfOpened(ln)
      CalcDer=CalcDerSave
      KPhase=KPhaseBasic
      if(allocated(BraggArr))
     1  deallocate(BraggArr,ShiftArr,ypeakArr,indArr,MultArr,KPhArr,
     2             RDeg1Arr,RDeg2Arr,ICalcArr,CutCoef1Arr,CutCoef2Arr,
     3             FWHMArr,stat=i)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa,
     6             stat=i)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf,stat=i)
      if(allocated(LeBailPrfArr)) deallocate(LeBailPrfArr,stat=i)
      do i=1,3
        TextInfo(i)=TextInfoSave(i)
      enddo
      NInfo=NInfoSave
      UseTabs=UseTabsIn
      return
101   format('Step#',i3,'     Rp(i)     =',2f8.3)
102   format(8x,        ' Rp(i)/Rp(i-1) =',f8.4)
103   format('%struct ',80a1)
104   format(a,1x,a)
      end
      subroutine PwdSetPoints(Show,StartTimeP)
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      include 'powder.cmn'
      dimension ypeakNew(:),hh(3)
      integer StartTimeP,FeGetSystemTime
      real Inorm(:),LpCorrection,LeBailPrfArrPom(:),LpArg
      logical BylUvnitr,Points,Show
      allocatable LeBailPrfArrPom,ypeakNew,INorm
      save scp,nalloc
      Points=.true.
      do i=1,npnts
        if(YfPwd(i).eq.0) then
          YbPwd(i)=0.
          YcPwd(i)=YoPwd(i)
          YcPwdInd(i,1:NPhase)=0.
          YcPwdInd(i,1)=YoPwd(i)
        else
          YcPwd(i)=YbPwd(i)
          YcPwdInd(i,1:NPhase)=0.
        endif
      enddo
      if(.not.LeBailFast) then
        nalloc=50000
        allocate(LeBailPrfArr(nalloc))
        TextInfo(1)='Setup of profile points - '//
     1    DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))
        TextInfo(3)=' '
      endif
      go to 500
      entry PwdSetIobs
      Points=.false.
500   allocate(ypeakNew(LeBailNBragg),Inorm(LeBailNBragg))
      n=1
      if(isTOF) then
        jfr=npnts
        jto=1
        jst=-1
      else
        jfr=1
        jto=npnts
        jst=1
      endif
      nn=0
      ip=0
      do 1000i=1,LeBailNBragg
        peak=BraggArr(i)
        ypeak=ypeakArr(i)
        BylUvnitr=.false.
        if(.not.Points) then
          ypeakNew(i)=0.
          Inorm(i)=0.
        endif
        KPhase=KPhArr(i)
        if(LeBailFast) then
          rdeg1=RDeg1Arr(i)
          rdeg2=RDeg2Arr(i)
          CutCoef1=1.+1./PCutOff(KPhase,KDatBlock)
          CutCoef2=1.-1./PCutOff(KPhase,KDatBlock)
        else
          nn=nn+1
          write(TextInfo(2),'(''Bragg peak#'',i5)') i
          if(Show.and.ip.ne.0) then
            if(mod(nn,100).eq.0) call FeMsgShow(3,-1.,300.,.false.)
          else
            if(.not.Show) Show=FeGetSystemTime()-StartTimeP.gt.5000
            if(Show) call FeMsgShow(3,-1.,300.,.true.)
            ip=1
          endif
          if(isTOF.or.isED) then
            th=peak
          else
            th=peak*.5
          endif
          call FromIndSinthl(indArr(1,i),hh,sinthl,sinthlq,1,0)
          NRefPwd=(i-1)/NAlfa(KDatBlock)+1
          NLamPwd=mod(i-1,NAlfa(KDatBlock))+1
          call SetProfFun(indArr(1,i),hh,th)
          ShiftArr(i)=shift
          FWHMArr(i)=FWHM
          rdeg1=rdeg(1)
          rdeg2=rdeg(2)
          RDeg1Arr(i)=rdeg1
          RDeg2Arr(i)=rdeg2
          CutCoef1=1.+1./PCutOff(KPhase,KDatBlock)
          CutCoef2=1.-1./PCutOff(KPhase,KDatBlock)
          CutCoef1Arr(i)=CutCoef1
          CutCoef2Arr(i)=CutCoef2
          call SetPref(indArr(1,i),pref)
          if(isTOF) then
            LpArg=PwdTOF2D(th)
          else if(isED) then
            LpArg=1.
          else
            LpArg=th
          endif
          pref=pref*MultArr(i)/LpCorrection(LpArg)
        endif
        rdegc1=(rdeg1*CutCoef1+rdeg2*CutCoef2)*.5
        rdegc2=(rdeg2*CutCoef1+rdeg1*CutCoef2)*.5
        do j=jfr,jto,jst
          if(YfPwd(j).eq.0) cycle
          if(IsTOF.or.isED) then
            pom=XPwd(j)
          else
            pom=XPwd(j)*ToRad
          endif
          if(isTOF) then
            if(pom.gt.rdeg2) then
              jfr=j
              cycle
            else if(pom.lt.rdeg1) then
              go to 900
            endif
          else
            if(pom.lt.rdeg1) then
              jfr=j
              cycle
            else if(pom.gt.rdeg2) then
              go to 900
            endif
          endif
          if(.not.BylUvnitr) then
            if(isTOF) then
              if(pom.lt.rdegc1) then
                ypeakNew(i)=0.
                go to 1000
              endif
            else
              if(pom.gt.rdegc2) then
                ypeakNew(i)=0.
                go to 1000
              endif
            endif
            BylUvnitr=pom.gt.rdegc1.and.pom.lt.rdegc2
          endif
          if(LeBailFast) then
            pprofile=LeBailPrfArr(n)
            n=n+1
          else
            if(isTOF) then
              if(KUseTOFAbs(KDatBlock).gt.0) then
                Absorption=PwdTOFAbsorption(pom,TOFAbsPwd(KDatBlock),
     1                                      DAbsorption)
              else
                Absorption=1.
              endif
            else if(isED) then
              Absorption=1.
            else
              Absorption=1./PwdAbsorption(pom)
            endif
            pprofile=prfl(pom-peak)*pref*Absorption
            LeBailPrfArr(n)=pprofile
            if(n.ge.nalloc) then
              allocate(LeBailPrfArrPom(nalloc))
              call CopyVek(LeBailPrfArr,LeBailPrfArrPom,nalloc)
              nallocp=nalloc
              nalloc =nalloc+50000
              deallocate(LeBailPrfArr)
              allocate(LeBailPrfArr(nalloc))
              call CopyVek(LeBailPrfArrPom,LeBailPrfArr,nallocp)
              deallocate(LeBailPrfArrPom)
            endif
            n=n+1
          endif
          if(Points) then
            YcPwd(j)=YcPwd(j)+ypeak*pprofile
            YcPwdInd(j,KPhase)=YcPwdInd(j,KPhase)+ypeak*pprofile
          else
            yical=YcPwd(j)-YbPwd(j)
            yiobs=YoPwd(j)-YbPwd(j)
            if(yical.lt.1.e-15.or.yiobs.lt.1.e-15) then
              zlomek=0.
            else
              zlomek=ypeak*yiobs/yical
            endif
            if(KWleBail(KDatBlock).eq.1) then
              wt=pprofile**2
            else
              wt=pprofile
            endif
            ypeakNew(i)=ypeakNew(i)+wt*zlomek
            Inorm(i)=Inorm(i)+wt
          endif
        enddo
900     if(isTOF) then
          if((pom.gt.rdegc1.or..not.BylUvnitr).and..not.points) then
            ypeakNew(i)=0.
            INorm(i)=0.
          endif
        else
          if((pom.lt.rdegc1.or..not.BylUvnitr).and..not.points) then
            ypeakNew(i)=0.
            INorm(i)=0.
          endif
        endif
1000  continue
      if(Points) then
        RFNom=0.
        RFDen=0.
        do j=1,npnts
          if(YfPwd(j).eq.0) cycle
          YoPwdj=YoPwd(j)
          YsPwdj=YsPwd(j)
          if(YsPwdj.le.0.) then
            if(YoPwdj.gt.0.) then
              YsPwdj=sqrt(YoPwdj)
            else
              cycle
            endif
          endif
          wt=1./YsPwdj**2
          yical=max(YcPwd(j)-YbPwd(j),0.)
          yiobs=max(YoPwd(j)-YbPwd(j),0.)
          RFNom=RFNom+wt*yiobs*yical
          RFDen=RFDen+wt*yical**2
        enddo
        if(RFDen.ne.0.) then
          scp=RFNom/RFDen
        else
          scp=1.
        endif
        RFNom=0.
        RFDen=0.
        RFNomObs=0.
        RFDenObs=0.
        do j=1,npnts
          if(YfPwd(j).eq.0) cycle
          YcPwd(j)=(YcPwd(j)-YbPwd(j))*scp+YbPwd(j)
          RFNom=RFNom+abs(YcPwd(j)-YoPwd(j))
          RFDen=RFDen+YoPwd(j)
          if(YoPwd(j)-YbPwd(j).gt.3.*YsPwd(j)) then
            RFNomObs=RFNomObs+abs(YcPwd(j)-YoPwd(j))
            RFDenObs=RFDenObs+YoPwd(j)
          endif
        enddo
        LeBailRF=RFNom/RFDen*100.
        if(RFDenObs.ne.0.) then
          LeBailRFObs=RFNomObs/RFDenObs*100.
        else
          LeBailRFObs=99.99
        endif
        do i=1,LeBailNBragg
          ypeakArr(i)=ypeakArr(i)*scp
        enddo
      else
        LeBailLastRef=0
        do i=1,LeBailNBragg
          if(Inorm(i).gt.0.) then
            ypeakArr(i)=ypeakNew(i)/Inorm(i)
            LeBailLastRef=i
          else
            ypeakArr(i)=0.
          endif
        enddo
        if(NPhase.gt.1.and.KKLeBail(KDatBlock).eq.1) then
          do KPh=1,NPhase
            if((NAtIndLenAll(KPh).le.0.and.NAtPosLenAll(KPh).le.0).or.
     1         KPhaseSolve.eq.KPh) cycle
            Suma1=0.
            Suma2=0.
            do i=1,LeBailNBragg,NAlfa(KDatBlock)
              if(KPhArr(i).ne.KPh) cycle
              Suma1=Suma1+ypeakArr(i)
              Suma2=Suma2+ICalcArr(i)
            enddo
            Suma1=Suma1/Suma2
            do i=1,LeBailNBragg,NAlfa(KDatBlock)
              if(KPhArr(i).ne.KPh) cycle
              ypeakArr(i)=ICalcArr(i)*Suma1
            enddo
          enddo
        endif
        if(NAlfa(KDatBlock).gt.1) then
          do i=1,LeBailNBragg,Nalfa(KDatBlock)
            ypeakArr(i)=(ypeakArr(i)+ypeakArr(i+1))/
     1                  (1.+LamRat(KDatBlock))
            ypeakArr(i+1)=ypeakArr(i)*LamRat(KDatBlock)
          enddo
        endif
      endif
      if(allocated(ypeakNew)) deallocate(ypeakNew,Inorm)
9999  return
      end
      function PwdTOF2D(TOF)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      ff(x,y)=PwdD2TOF(x)*1000.-y
      if(TOFInD) then
        PwdTOF2D=TOF
      else
        TOFp=TOF*1000.
        if(KUseTOFJason(KDatBlock).le.0) then
          pom=4.*ShiftPwd(3,KDatBlock)/ShiftPwd(2,KDatBlock)**2*
     1          (TOFp-ShiftPwd(1,KDatBlock))
          if(abs(pom).gt.1.e-3) then
            PwdTOF2D=ShiftPwd(2,KDatBlock)/(2.*ShiftPwd(3,KDatBlock))*
     1               (sqrt(1.+pom)-1.)
          else
            PwdTOF2D=1./ShiftPwd(2,KDatBlock)*
     1               (TOFp-ShiftPwd(1,KDatBlock))
          endif
        else
          dd=(TOFp-ShiftPwd(4,KDatBlock))/ShiftPwd(5,KDatBlock)
          x1=dd
          f1=ff(x1,TOFp)
          x2=x1
          zn=1.
          n=0
1100      x2=x2+zn*.001
          f2=ff(x2,TOFp)
          f12=f1*f2
          n=n+1
          if(f12.gt.0.) then
            if(n.gt.1.and.zn.gt.0.) then
              if(f12.gt.f12old) then
                x2=x1
                zn=-1.
                n=0
              endif
            endif
            f12old=f12
            go to 1100
          endif
          n=0
1200      xp=(f2*x1-f1*x2)/(f2-f1)
          n=n+1
          fp=ff(xp,TOFp)
          if(fp*f1.gt.0.) then
            if(abs(x1-xp).gt..00001) then
              f1=fp
              x1=xp
            else
              PwdTOF2D=(x1+xp)*.5
              go to 9999
            endif
          else if(fp*f1.lt.0.) then
            if(abs(x2-xp).gt..00001) then
              f2=fp
              x2=xp
            else
              PwdTOF2D=(x2+xp)*.5
              go to 9999
            endif
          else
            PwdTOF2D=xp
            go to 9999
          endif
          go to 1200
        endif
      endif
9999  return
      end
      function PwdD2TOF(dd)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      if(TOFInD) then
        PwdD2TOF=dd
      else
        if(KUseTOFJason(KDatBlock).le.0) then
          PwdD2TOF=(ShiftPwd(2,KDatBlock)*dd+
     1              ShiftPwd(3,KDatBlock)*dd**2+
     1              ShiftPwd(1,KDatBlock))*.001
        else
          fn=.5*erfc(ShiftPwd(6,KDatBlock)*
     1       (ShiftPwd(7,KDatBlock)-1./dd))
          TE=ShiftPwd(4,KDatBlock)+ShiftPwd(5,KDatBlock)*dd
          TT=ShiftPwd(1,KDatBlock)+ShiftPwd(2,KDatBlock)*dd
     1      -ShiftPwd(3,KDatBlock)/dd
          PwdD2TOF=(fn*TE+(1.-fn)*TT)*.001
        endif
      endif
      return
      end
      logical function PwdCheckTOFInD(KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      PwdCheckTOFInD=abs(ShiftPwd(1,KDatBlock)).lt..0001.and.
     1               abs(ShiftPwd(3,KDatBlock)).lt..0001.and.
     2               abs(ShiftPwd(2,KDatBlock)-1.).lt..0001
      return
      end
      function PwdTwoTh2D(TwoTh,Lambda)
      include 'fepc.cmn'
      include 'basic.cmn'
      real Lambda
      PwdTwoTh2D=.5*Lambda/sin(.5*TwoTh*ToRad)
      return
      end
      function PwdD2TwoTh(D,Lambda)
      include 'fepc.cmn'
      include 'basic.cmn'
      real Lambda
      PwdD2TwoTh=2.*asin(Lambda*.5/D)/ToRad
      return
      end
      subroutine PwdMakeSkipFlags
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      call PwdSetTOF(KDatBlock)
      call PwdOrderSkipRegions
      do 2200j=1,Npnts
        do i=1,NskipPwd(KDatBlock)
          if(XPwd(j).ge.SkipPwdFr(i,KDatBlock).and.
     1       XPwd(j).le.SkipPwdTo(i,KDatBlock)) then
            YfPwd(j)=0
            go to 2200
          endif
        enddo
        if(UseCutoffPwd) then
          if(isTOF) then
            if(TOFInD) then
              dpom=XPwd(j)
            else
              dpom=PwdTOF2D(XPwd(j))
            endif
          else
            dpom=PwdTwoTh2D(XPwd(j),LamA1(KDatBlock))
          endif
          if(dpom.lt.CutoffMinPwd.or.dpom.gt.CutoffMaxPwd) then
            YfPwd(j)=0
            go to 2200
          endif
        endif
        YfPwd(j)=1
2200  continue
      return
      end
      subroutine KresliGrafNew
      parameter (mxpar=20)
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension XArr(361),YArr(361)
      character*80 t80
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,Black,
     1                      -1,-1)
      call FeMakeGrWin(0.,40.,YBottomMargin,14.)
      call FeMakeAcWin(20.,10.,10.,10.)
      pom=YMaxGrWin
      dpom=ButYd+6.
      ypom=pom-dpom-2.
      wpom=34.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,pom,
     1                       Gray,White)
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Return')
      nButtReturn=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      ypom=ypom-dpom
      call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,'%Define')
      nButtDefine=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      call UnitMat(F2O,3)
      xomn=0.
      xomx=360.
      yomn=-.1
      yomx=1.1
      Pref1=1.5
      Pref2=0.
2000  pom=0.
      dpom=1.
      yomx=0.
      do i=1,361
        XArr(i)=pom
        csq=cos(Pom*ToRad)**2
        qq=csq*Pref1**2+(1.-csq)/Pref1
        YArr(i)=Pref2+(1.-Pref2)/qq**1.5
        yomx=max(yomx,YArr(i))
        pom=pom+dpom
      enddo
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeClearGrWin
      call FeMakeAcFrame
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,'Angle')
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,'Pref')
      call FeXYPlot(XArr,YArr,361,NormalLine,NormalPlotMode,Red)
3000  call FeQuestEvent(id,icont,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtReturn) then
          go to 5000
        else if(CheckNumber.eq.nButtDefine) then
          call FeQuestButtonOff(CheckNumber)
          idp=NextQuestId()
          xqd=150.
          il=2
          call FeQuestCreate(idp,-1.,-1.,xqd,il,'Define',0,LightGray,
     1                       0,0)
          tpom=5.
          t80='Pref%1'
          xpom=tpom+FeTxLength(t80)+5.
          dpom=30.
          il=1
          call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,Pref1,.false.,.false.)
          nEdwPref1=EdwLastMade
          il=il+1
          t80='Pref%2'
          call FeQuestEdwMake(idp,tpom,il,xpom,il,t80,'L',dpom,EdwYd,0)
          call FeQuestRealEdwOpen(EdwLastMade,Pref2,.false.,.false.)
          nEdwPref2=EdwLastMade
3500      call FeQuestEvent(idp,icont,ich)
          if(CheckType.ne.0) then
            call NebylOsetren
            go to 3500
          endif
          if(ich.eq.0) then
            call FeQuestRealFromEdw(nEdwPref1,Pref1)
            call FeQuestRealFromEdw(nEdwPref2,Pref2)
          endif
          call FeQuestRemove(idp)
          EventType=0
          go to 2000
        endif
      endif
      go to 3000
5000  call FeQuestRemove(id)
      call FeMakeGrWin(0.,0.,YBottomMargin,0.)
      call UnitMat(F2O,3)
      return
      end
      subroutine PwdOptions(ich)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      include 'datred.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*256 Veta
      character*80  t80
      integer :: KartIdZpet=1
      logical Zpet,PwdCheckTOFInD,EqIgCase,Wild,EqWild
      external PwdOptionsRefresh
      save /PwdOptionsC/
      ln=0
      if(allocated(PhaseGr)) deallocate(PhaseGr)
      allocate(PhaseGr(NPhase))
      PhaseGr=0
      if(NPhase.gt.1) then
        ln=NextLogicNumber()
        call OpenFile(ln,fln(:ifln)//'.m50','formatted','unknown')
        if(ErrFlag.ne.0) go to 2000
1100    read(ln,FormA,end=2000) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(.not.EqIgCase(Cislo,'refine')) go to 1100
        NGr=0
1200    read(ln,FormA,end=2000) Veta
        k=0
        call kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,'restric')) then
          call kus(Veta,k,t80)
          if(k.ge.len(Veta)) go to 1200
          call kus(Veta,k,Cislo)
          if(.not.EqIgCase(Cislo,'profile')) go to 1200
          NGr=NGr+1
          n=0
1300      Wild=index(t80,'*').gt.0.or.index(t80,'?').gt.0
          do KPh=1,NPhase
            if(PhaseGr(KPh).ne.0) cycle
            if(Wild) then
              if(EqWild(PhaseName(KPh),t80,.false.)) then
                PhaseGr(KPh)=NGr
                n=n+1
              endif
            else
              if(EqIgCase(PhaseName(KPh),t80)) then
                PhaseGr(KPh)=NGr
                n=n+1
                exit
              endif
            endif
          enddo
          if(k.lt.len(Veta)) then
            call kus(Veta,k,t80)
            go to 1300
          endif
1400      if(n.le.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.NGr) PhaseGr(KPh)=0
            enddo
            NGr=NGr-1
          endif
          go to 1200
        else if(EqIgCase(Cislo,'end')) then
          go to 2000
        else
          go to 1200
        endif
      endif
2000  if(ln.gt.0) call CloseIfOpened(ln)
      MenuPhaseUse=1
      KeyMenuPhaseUse=0
      do i=1,NPhase
        n=0
        do j=1,NPhase
          if(PhaseGr(j).eq.i) then
            if(n.gt.0) MenuPhaseUse(j)=0
            n=n+1
          endif
        enddo
      enddo
      KDatBlockIn=KDatBlock
      KPhaseIn=KPhase
      KartIdZpet=1
      if(NDatBlock.gt.1) then
        KDatB=0
        do i=1,NDatBlock
          if(DataType(i).eq.1) then
            MenuDatBlockUse(i)=0
          else
            if(KDatB.le.0) KDatB=i
            MenuDatBlockUse(i)=1
          endif
        enddo
        if(MenuDatBlockUse(KDatBlock).eq.0) KDatBlock=KDatB
      endif
2100  do KRefB=1,NRefBlock
        if(RefDatCorrespond(KRefB).eq.KDatBlock) then
          KRefBlock=KRefB
          exit
        endif
      enddo
      call PwdSetTOF(KDatBlock)
      if(kcommenMax.ne.0) call comsym(0,0,ich)
      LastAsymProfile=KAsym(KDatBlock)
      if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        do i=1,7
          if(i.eq.2.or.i.eq.6.or.i.eq.7)
     1      AsymPwd(i,KDatBlock)=AsymPwd(i,KDatBlock)/ToRad
        enddo
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
          GaussPwd(1,KPhase,KDatBlock)=1000.
          GaussPwd(3,KPhase,KDatBlock)=.01
        else if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss) then
          LorentzPwd(1,KPhase,KDatBlock)=1000.
          LorentzPwd(3,KPhase,KDatBlock)=.01
        endif
      endif
      call PwdGetLamFiles
      IZdvihRec=(KDatBlock-1)*NParRecPwd
      IZdvihCell=(KPhase-1)*NParCellProfPwd
      IZdvihProf=IZdvihCell+(KDatBlock-1)*NParProfPwd
      xqd=600.
      Veta='Powder options'
      if(NPhase.gt.1) Veta=Veta(:idel(Veta))//' for phase : '//
     1                     PhaseName(KPhase)
      if(NDatBlock.gt.1) then
        if(NPhase.gt.1) then
          Veta=Veta(:idel(Veta))//' and datblock : '//
     1         MenuDatBlock(KDatBlock)
        else
          Veta=Veta(:idel(Veta))//' for datblock : '//
     1         MenuDatBlock(KDatBlock)
        endif
      endif
      il=16
      call FeKartCreate(-1.,-1.,xqd,il,Veta,0,OKForBasicFiles)
      n=0
      nRolMenuDatBlock=0
      nRolMenuPhase=0
      if(NPhase.gt.1) n=n+1
      if(NDatBlock.gt.1) n=n+1
      if(n.gt.0) then
        ypom=35.
        if(NDatBlock.gt.1) then
          dpom=0.
          do i=1,NDatBlock
            dpom=max(dpom,FeTxLength(MenuDatBlock(i)))
          enddo
          if(MenuDatBlockUse(KDatBlock).eq.0) KDatBlock=KDatB
          dpom=dpom+2.*EdwMarginSize+EdwYd
          Veta=' '
          xpom=xqd*.5-dpom*.5
          if(n.gt.1) xpom=xpom-dpom*.5-5.
          tpom=xpom
          call FeQuestAbsRolMenuMake(KartId0,tpom,ypom+3.,xpom,ypom,
     1                               Veta,'L',dpom,EdwYd,1)
          nRolMenuDatBlock=RolMenuLastMade+RolMenuFr-1
          call FeQuestRolMenuWithEnableOpen(RolMenuLastMade,
     1      MenuDatBlock,MenuDatBlockUse,NDatBlock,KDatBlock)
        endif
        if(NPhase.gt.1) then
          dpom=0.
          do i=1,NPhase
            dpom=max(dpom,FeTxLength(PhaseName(i)))
          enddo
          dpom=dpom+2.*EdwMarginSize+EdwYd
          Veta=' '
          xpom=xqd*.5-dpom*.5
          if(n.gt.1) xpom=xpom+dpom*.5+5.
          tpom=xpom
          call FeQuestAbsRolMenuMake(KartId0,tpom,ypom+3.,xpom,ypom,
     1                               Veta,'L',dpom,EdwYd,1)
          nRolMenuPhase=RolMenuLastMade+RolMenuFr-1
          call FeQuestRolMenuOpen(RolMenuLastMade,PhaseName,NPhase,
     1                            KPhase)
        endif
      endif
      xcq=xqd*.5-KartSidePruh
      xqdp=xqd-2.*KartSidePruh
      pom=(xqdp-3.*75.-10.)/3.
      pom=(xqdp-80.-pom)/2.
      xpoma(3)=xqdp-80.-CrwXd
      do i=2,1,-1
        xpoma(i)=xpoma(i+1)-pom
      enddo
      do i=1,3
        tpoma(i)=xpoma(i)-5.
      enddo
      call FeCreateListek('Cell',0)
      KartIdCell=KartLastId
      call PwdOptionsCellMake(KartIdCell)
      call FeCreateListek('Radiation',0)
      KartIdRadiation=KartLastId
      call PwdOptionsRadiationMake(KartIdRadiation)
      call FeCreateListek('Profile',0)
      KartIdProf=KartLastId
      call PwdOptionsProfMake(KartIdProf)
      call FeCreateListek('Asymmetry/Diffractometer',0)
      KartIdAsym=KartLastId
      call PwdOptionsAsymMake(KartIdAsym)
      Veta='Sample'
      if(PwdMethod(KDatBlock).eq.IdPwdMethodUnknown)
     1  Veta=Veta(:idel(Veta))//'/Experiment'
      call FeCreateListek(Veta,0)
      KartIdSample=KartLastId
      call PwdOptionsSampleMake(KartIdSample)
      call FeCreateListek('Corrections',0)
      KartIdCorr=KartLastId
      call PwdOptionsCorrMake(KartIdCorr)
      call FeCreateListek('Various',0)
      KartIdVarious=KartLastId
      call PwdOptionsVariousMake(KartIdVarious)
      call FeCompleteKart(KartIdZpet)
6000  Zpet=.false.
      if(nRolMenuDatBlock.gt.0) RolMenuAlways(nRolMenuDatBlock)=.true.
      if(nRolMenuPhase   .gt.0) RolMenuAlways(nRolMenuPhase)   =.true.
      call FeQuestEventWithKartUpdate(KartId,ich,PwdOptionsRefresh)
      if(nRolMenuDatBlock.gt.0) RolMenuAlways(nRolMenuDatBlock)=.false.
      if(nRolMenuPhase   .gt.0) RolMenuAlways(nRolMenuPhase)   =.false.
      if(CheckType.eq.EventRolMenu.and.
     1   CheckNumberAbs.eq.nRolMenuDatBlock) then
        KDatBlockNew=RolMenuSelected(nRolMenuDatBlock)
        KPhaseNew=0
        if(KDatBlockNew.ne.KDatBlock.and.KDatBlockNew.ne.0) then
          Zpet=.true.
          KartIdZpet=KartId-KartIdCell+1
          go to 8000
        else
          go to 6000
        endif
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumberAbs.eq.nRolMenuPhase) then
        KDatBlockNew=0
        KPhaseNew=RolMenuSelected(nRolMenuPhase)
        if(KPhaseNew.ne.KPhase.and.KPhaseNew.ne.0) then
          Zpet=.true.
          KartIdZpet=KartId-KartIdCell+1
          go to 8000
        else
          go to 6000
        endif
      else if(CheckType.ne.0) then
        if(KartId.eq.KartIdCell) then
          call PwdOptionsCellCheck
        else if(KartId.eq.KartIdRadiation) then
          call PwdOptionsRadiationCheck
        else if(KartId.eq.KartIdProf) then
          call PwdOptionsProfCheck
        else if(KartId.eq.KartIdAsym) then
          call PwdOptionsAsymCheck
        else if(KartId.eq.KartIdSample) then
          call PwdOptionsSampleCheck
        else if(KartId.eq.KartIdCorr) then
          call PwdOptionsCorrCheck
        else if(KartId.eq.KartIdVarious) then
          call PwdOptionsVariousCheck
        else
        endif
        go to 6000
      else if(CheckType.ne.0.and.CheckNumber.ne.0) then
        call NebylOsetren
        go to 6000
      endif
8000  if(kcommenMax.ne.0) call iom50(0,0,fln(:ifln)//'.m50')
!     Obnova SSG uz zde, aby se neprepisovaly vlnove delky
      if(ich.eq.0) then
        call FeDeferOutput
        do i=KartIdCell,KartIdVarious
          call FePrepniListek(i-KartFirstId+1)
          call PwdOptionsRefresh(0)
        enddo
        call FePrepniListek(KartIdCell-KartFirstId+1)
        call PwdOptionsCellUpdate
        call FePrepniListek(KartIdRadiation-KartFirstId+1)
        call PwdOptionsRadiationUpdate
        call FePrepniListek(KartIdProf-KartFirstId+1)
        call PwdOptionsProfUpdate
        call FePrepniListek(KartIdAsym-KartFirstId+1)
        call PwdOptionsAsymUpdate
        call FePrepniListek(KartIdSample-KartFirstId+1)
        call PwdOptionsSampleUpdate
        call FePrepniListek(KartIdCorr-KartFirstId+1)
        call PwdOptionsCorrUpdate
        call FePrepniListek(KartIdVarious-KartFirstId+1)
        call PwdOptionsVariousUpdate
      endif
      call FeDestroyKart
      if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        do i=1,7
          if(i.eq.2.or.i.eq.6.or.i.eq.7)
     1      AsymPwd(i,KDatBlock)=AsymPwd(i,KDatBlock)*ToRad
        enddo
      endif
      if(Zpet) then
        if(KPhaseNew.gt.0) then
          KPhase=KPhaseNew
        else
          KDatBlock=KDatBlockNew
        endif
        go to 2100
      endif
9999  KDatBlock=KDatBlockIn
      KPhase=KPhaseIn
      if(allocated(PhaseGr)) deallocate(PhaseGr)
      return
      end
      subroutine PwdOptionsRefresh(KartIdOld)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      dimension GaussPwdOld(4),GaussPwdNew(4)
      integer CrwStateQuest,EdwStateQuest
!      integer :: KPhaseOld=1
      logical lpom,CrwLogicQuest
      save KLamOld,/PwdOptionsC/
!      if(NPhase.gt.1) then
!        nRolMenu=nRolMenuPhase-RolMenuFr+1
!        if(KartId.eq.KartIdProf) then
!          KPhaseOld=KPhase
!          KPhase=1
!          if(KeyMenuPhaseUse.eq.0) then
!            call FeQuestRolMenuClose(nRolMenu)
!            call FeQuestRolMenuWithEnableOpen(nRolMenu,PhaseName,
!     1                                       MenuPhaseUse,NPhase,1)
!          endif
!          KeyMenuPhaseUse=1
!        else
!          if(KeyMenuPhaseUse.ne.0) then
!            KPhase=KPhaseOld
!            call FeQuestRolMenuClose(nRolMenu)
!            call FeQuestRolMenuOpen(nRolMenu,PhaseName,NPhase,KPhase)
!          endif
!          KeyMenuPhaseUse=0
!        endif
!      endif
      if(KartId.eq.KartIdVarious) then
        if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          klam(KDatBlock)=LocateInArray(LamAve(KDatBlock),LamAveD,7,
     1                                 .0001)
          if(klam(KDatBlock).le.0)
     1      klam(KDatBlock)=LocateInArray(LamA1(KDatBlock),LamA1D,7,
     2                                    .0001)
          if(CrwStateQuest(nCrwUseLamFile).eq.CrwClosed) then
            call FeQuestCrwClose(nCrwRefLam)
            nEdw=nEdwLam
            nCrw=nCrwLam
            do i=1,NAlfa(KDatBlock)
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
              nEdw=nEdw+1
              nCrw=nCrw+1
            enddo
            call FeQuestCrwOpen(nCrwUseLamFile,.false.)
            KLamOld=-1
          else
            if(CrwLogicQuest(nCrwUseLamFile)) then
              if(KLamOld.ne.KLam(KDatBlock)) then
                if(KLam(KDatBlock).gt.0) then
                  call PwdGetLamFiles
                  if(nLamFiles.gt.0) then
                    call FeQuestRolMenuOpen(nRolMenuLamFile,LamNames,
     1                                      nLamFiles,1)
                  else
                    call FeQuestCrwDisable(nCrwUseLamFile)
                    call FeQuestRolMenuClose(nRolMenuLamFile)
                  endif
                  KLamOld=KLam(KDatBlock)
                else
                  call FeQuestRolMenuClose(nRolMenuLamFile)
                  KLamOld=-1
                endif
              endif
            else if(CrwStateQuest(nCrwUseLamFile).eq.CrwDisabled) then
              call FeQuestCrwOpen(nCrwUseLamFile,.false.)
            else
              KLamOld=-1
            endif
          endif
          if(klam(KDatBlock).le.0)
     1      call FeQuestCrwDisable(nCrwUseLamFile)
        else if(nCrwRefLam.ne.0) then
          if(CrwStateQuest(nCrwRefLam).eq.CrwClosed) then
            call FeQuestCrwClose(nCrwUseLamFile)
            call FeQuestRolMenuClose(nRolMenuLamFile)
            call FeQuestCrwOpen(nCrwRefLam,KRefLam(KDatBlock).eq.1)
            if(KRefLam(KDatBlock).eq.1) then
              nEdw=nEdwLam
              nCrw=nCrwLam
              IZdvihRec=(KDatBlock-1)*NParRecPwd
              j=ILamPwd+IZdvihRec
              do i=1,NAlfa(KDatBlock)
                call FeQuestRealEdwOpen(nEdw,LamPwd(i,KDatBlock),
     1                                  .false.,.false.)
                call FeQuestCrwOpen(nCrw,kipwd(j).eq.1)
                j=j+1
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
            endif
          endif
        endif
      else if(KartId.eq.KartIdProf) then
        if(.not.isTOF) then
          if(KAsym(KDatBlock).ne.LastAsymProfile) then
            if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
              lpom=KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt
              nEdw=nEdwLorentz
              nCrw=nCrwLorentz
              do i=1,5
                if(i.ne.5)
     1            call FeQuestEdwLabelChange(nEdw,lLorentzPwdF(i))
                if(mod(i,2).eq.0.or.i.eq.5) then
                  call FeQuestEdwDisable(nEdw)
                  call FeQuestCrwDisable(nCrw)
                else
                  if(lpom) then
                    call FeQuestRealFromEdw(nEdw,pom)
                  else
                    pom=LorentzPwd(i,KPhase,KDatBlock)
                  endif
                  if(i.eq.1) then
                    if(pom.gt.0.) then
                      pom=LorentzToCrySize(KDatBlock)/pom
                    else
                      pom=0.
                    endif
                  else if(i.eq.3) then
                    pom=max(pom*LorentzToStrain,.01)
                  endif
                  if(lpom)
     1              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                  LorentzPwd(i,KPhase,KDatBlock)=pom
                endif
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              lpom=KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt
              nEdw=nEdwGauss
              nCrw=nCrwGauss
              do i=1,4
                call FeQuestEdwLabelChange(nEdw,lGaussPwdF(i))
                if(lpom) then
                  call FeQuestRealFromEdw(nEdw,GaussPwdOld(i))
                else
                  GaussPwdOld(i)=GaussPwd(i,KPhase,KDatBlock)
                endif
                if(mod(i,2).eq.0) then
c                  call FeQuestEdwClose(nEdw)
c                  call FeQuestCrwClose(nCrw)
                  call FeQuestEdwDisable(nEdw)
                  call FeQuestCrwDisable(nCrw)
                endif
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              GaussPwdNew(1)=sqrt(max(GaussPwdOld(4)+GaussPwdOld(3),0.))
              GaussPwdNew(2)=0.
              GaussPwdNew(3)=sqrt(max(GaussPwdOld(1)-
     1                                GaussPwdOld(3),.01))
              GaussPwdNew(4)=0.
              nEdw=nEdwGauss
              nCrw=nCrwGauss
              do i=1,4
                pom=GaussPwdNew(i)
                if(mod(i,2).ne.0) then
                  if(i.eq.1) then
                    if(pom.gt.0.) then
                      pom=GaussToCrySize(KDatBlock)/pom
                    else
                      pom=0.
                    endif
                  else if(i.eq.3) then
                    pom=pom*GaussToStrain
                  endif
                  if(lpom)
     1              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                  GaussPwd(i,KPhase,KDatBlock)=pom
                endif
                GaussPwd(i,KPhase,KDatBlock)=pom
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              call FeQuestLblOff(nLblStrain)
              call FeQuestEdwDisable(nEdwDirBroad)
              call FeQuestCrwDisable(nCrwAniPBroad)
              call FeQuestButtonDisable(nButEditTensor)
              nCrw=nCrwStrain
              do i=1,3
                call FeQuestCrwDisable(nCrw)
                nCrw=nCrw+1
              enddo
            else if(LastAsymProfile.eq.IdPwdAsymFundamental) then
              lpom=KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt
              nEdw=nEdwLorentz
              nCrw=nCrwLorentz
              do i=1,4
                call FeQuestEdwLabelChange(nEdw,
     1            lLorentzPwd(i)(:idel(lLorentzPwd(i))))
                if(mod(i,2).eq.1) then
                  if(lpom) then
                    call FeQuestRealFromEdw(nEdw,pom)
                  else
                    pom=LorentzPwd(i,KPhase,KDatBlock)
                  endif
                  if(i.eq.1) then
                    if(pom.gt.0.) then
                      pom=LorentzToCrySize(KDatBlock)/pom
                    else
                      pom=0.
                    endif
                  else if(i.eq.3) then
                    pom=max(pom/LorentzToStrain,0.)
                  endif
                  if(lpom)
     1              call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
                  LorentzPwd(i,KPhase,KDatBlock)=pom
                endif
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              lpom=KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1             KProfPwd(KPhase,KDatBlock).eq.IdPwdProfVoigt
              nEdw=nEdwGauss
              nCrw=nCrwGauss
              do i=1,4
                call FeQuestEdwLabelChange(nEdw,
     1            lGaussPwd(i)(:idel(lGaussPwd(i))))
                if(lpom.and.EdwStateQuest(nEdw).eq.EdwOpened) then
                  call FeQuestRealFromEdw(nEdw,pom)
                else
                  pom=GaussPwd(i,KPhase,KDatBlock)
                endif
                pom=abs(pom)
                if(i.eq.1) then
                  if(pom.le.0.) pom=100.
                  pom=min(GaussToCrySize(KDatBlock)/pom,10.)
                else if(i.eq.3) then
                  pom=min(pom/GaussToStrain,10.)
                endif
                GaussPwdOld(i)=pom**2
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              GaussPwdNew(1)=GaussPwdOld(3)
              GaussPwdNew(2)=0.
              GaussPwdNew(3)=0.
              GaussPwdNew(4)=GaussPwdOld(1)
              nEdw=nEdwGauss
              nCrw=nCrwGauss
              do i=1,4
                if(lpom) then
                  if(EdwStateQuest(nEdw).ne.EdwOpened)
     1              call FeQuestCrwOpen(nCrw,.false.)
                  call FeQuestRealEdwOpen(nEdw,GaussPwdNew(i),.false.,
     1                                    .false.)
                endif
                GaussPwd(i,KPhase,KDatBlock)=GaussPwdNew(i)
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              call FeQuestLblOn(nLblStrain)
              call FeQuestCrwOpen(nCrwAniPBroad,.false.)
              nCrw=nCrwStrain
              do i=1,3
                call FeQuestCrwOpen(nCrw,i.eq.1)
                nCrw=nCrw+1
              enddo
            endif
          endif
        endif
        LastAsymProfile=KAsym(KDatBlock)
      else if(KartId.eq.KartIdAsym) then
        if(isTOF) then
          if(KUseTOFJason(KDatBlock).le.0) then
            call FeQuestCrwDisable(nCrwAsymFr+2)
            if(KAsym(KDatBlock).eq.IdPwdAsymTOF2) then
              call FeQuestCrwOn(nCrwAsymFr+1)
              call FeUpdateParamAndKeys(nEdwAsymP,nCrwAsymP,
     1                                  AsymOrg(1,2),kiAsymOrg(1,2),8)
              nEdw=nEdwAsymP
              nCrw=nCrwAsymP
              do i=1,8
                if(i.le.4) then
                  call FeQuestEdwLabelChange(nEdw,lAsymTOF1Pwd(i))
                  call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1              AsymOrg(i,1),kiAsymOrg(i,1),.false.)
                else
                  call FeQuestEdwClose(nEdw)
                  call FeQuestCrwClose(nCrw)
                endif
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
              KAsym(KPhase)=IdPwdAsymTOF1
              NAsymOld=4
            endif
          else
            if(CrwStateQuest(nCrwAsymFr+2).ne.CrwOn.and.
     1         CrwStateQuest(nCrwAsymFr+2).ne.CrwOff)
     2        call FeQuestCrwOff(nCrwAsymFr+2)
          endif
        else
          KAsymNew=KAsym(KDatBlock)
          if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz) then
            if(KAsym(KDatBlock).ne.IdPwdAsymNone.and.
     1         KAsym(KDatBlock).ne.IdPwdAsymDebyeInt)
     2        KAsymNew=IdPwdAsymNone
            j=0
            do nCrw=nCrwAsymFr,nCrwAsymTo
              if(nCrw.ne.nCrwAsymFr.and.nCrw.ne.nCrwAsymTo) then
                call FeQuestCrwDisable(nCrw)
              else
                if(j.eq.KAsymNew) then
                  call FeQuestCrwOn(nCrw)
                else
                  call FeQuestCrwOff(nCrw)
                endif
              endif
              j=j+1
            enddo
          else
            if(KAsym(KDatBlock).eq.IdPwdAsymDebyeInt)
     1        KAsymNew=IdPwdAsymNone
            j=0
            do nCrw=nCrwAsymFr,nCrwAsymTo
              if(nCrw.eq.nCrwAsymTo) then
                call FeQuestCrwDisable(nCrw)
              else
                if(j.eq.KAsymNew) then
                  call FeQuestCrwOn(nCrw)
                else
                  call FeQuestCrwOff(nCrw)
                endif
              endif
              j=j+1
            enddo
          endif
          if(KAsym(KDatBlock).ne.KAsymNew)
     1      call PwdOptionsAsymRefresh(KAsymNew)
        endif
      endif
      return
      end
      subroutine PwdOptionsCell
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      integer CrlCentroSymm,EdwStateQuest
      logical CrwLogicQuest
      save nEdwCell,nEdwMMax,nCrwCell,nCrwSatFrMod,nCrwSkipFriedel
      save /PwdOptionsC/
      entry PwdOptionsCellMake(id)
      j=ICellPwd+IZdvihCell
      ilp=2
      tpom=5.
      call FeQuestLblMake(id,tpom,ilp-1,'Cell parameters','L','B')
      do i=1,6+NDimI(KPhase)*3
        if(i.le.6) then
          Veta=lCell(i)
          pom=CellPwd(i,KPhase)
        else
          if(i.eq.7) then
            ilp=3
            Veta='Modulation vector'
            if(NDimI(KPhase).gt.1) Veta=Veta(:idel(Veta))//'s'
            call FeQuestLblMake(id,5.,il+1,Veta,'L','B')
          endif
          n=mod(i-7,3)+1
          m=(i-7)/3+1
          pom=QuPwd(n,m,KPhase)
          write(Veta,'(''q'',2i1)') n,m
          if(NDim(KPhase).eq.4) Veta(3:3)=' '
        endif
        im=mod(i-1,3)+1
        il=(i-1)/3+ilp
        tpom=max(xpoma(im)-5.-FeTxLength(Veta),tpoma(im))
        call FeMakeParEdwCrw(id,tpom,xpoma(im),il,Veta,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kiPwd(j),i.gt.6)
        if(i.eq.1) then
          nEdwCell=nEdw
          nCrwCell=nCrw
        endif
        j=j+1
      enddo
      if(NDimI(KPhase).gt.0) then
        il=il+1
        Veta='Maximal satellite ind'
        i=idel(Veta)
        if(NDim(KPhase).eq.4) then
          Veta=Veta(:i)//'ex'
        else
          Veta=Veta(:i)//'ices'
        endif
        call FeQuestLblMake(id,5.,il,Veta,'L','B')
        if(NAtCalc.gt.0) then
          il=il+1
          Veta='Use only satellites corresponding to existing '//
     1         'modulation waves'
          xpom=5.
          tpom=xpom+CrwXd+10.
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwSatFrMod=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,SatFrMod(KPhase,KDatBlock))
        else
          nCrwSatFrMod=0
          SatFrMod(KPhase,KDatBlock)=.false.
        endif
        il=il+1
        Veta='%m(max)'
        cpom=FeTxLengthUnder(Veta)+10.
        tpom=5.
        dpom=40.
        xpom=tpom+cpom
        do i=1,NDimI(KPhase)
          if(i.eq.2) then
            Veta(2:2)='p'
          else if(i.eq.3) then
            Veta(2:2)='n'
          endif
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',
     1                        dpom,EdwYd,0)
          if(i.eq.1) nEdwMMax=EdwLastMade
          tpom=xpom+dpom+20.
          xpom=tpom+cpom
          if(.not.SatFrMod(KPhase,KDatBlock))
     1      call FeQuestIntEdwOpen(EdwLastMade,
     2                             MMaxPwd(i,KPhase,KDatBlock),.false.)
        enddo
      endif
      if(CrlCentroSymm().le.0) then
        il=il+1
        Veta='Skip Friedel pairs'
        xpom=5.
        tpom=xpom+CrwXd+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwSkipFriedel=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,SkipFriedel(KPhase,KDatBlock))
      else
        nCrwSkipFriedel=0
        SkipFriedel(KPhase,KDatBlock)=.false.
      endif
      go to 9999
      entry PwdOptionsCellCheck
      if(NDimI(KPhase).gt.0) then
        if(nCrwSatFrMod.gt.0) then
          nEdw=nEdwMMax
          if(CrwLogicQuest(nCrwSatFrMod)) then
            do i=1,NDimI(KPhase)
              call FeQuestIntFromEdw(nEdw,MMaxPwd(i,KPhase,KDatBlock))
              call FeQuestEdwDisable(nEdw)
              nEdw=nEdw+1
            enddo
          else
            if(EdwStateQuest(nEdw).ne.EdwOpened) then
              do i=1,NDimI(KPhase)
                call FeQuestIntEdwOpen(nEdw,MMaxPwd(i,KPhase,KDatBlock),
     1                                 .false.)
                nEdw=nEdw+1
              enddo
            endif
          endif
        endif
      endif
      go to 9999
      entry PwdOptionsCellUpdate
      if(nCrwSatFrMod.gt.0)
     1  SatFrMod(KPhase,KDatBlock)=CrwLogicQuest(nCrwSatFrMod)
      if(nCrwSkipFriedel.gt.0)
     1  SkipFriedel(KPhase,KDatBlock)=CrwLogicQuest(nCrwSkipFriedel)
      j=ICellPwd+IZdvihCell
      call FeUpdateParamAndKeys(nEdwCell,nCrwCell,CellPwd(1,KPhase),
     1                          kiPwd(j),6)
      call FeUpdateParamAndKeys(nEdwCell+6,nCrwCell+6,
     1                          QuPwd(1,1,KPhase),kiPwd(j+6),
     2                          3*NDimI(KPhase))
      if(NDimI(KPhase).gt.0) then
        nEdw=nEdwMMax
        do i=1,NDimI(KPhase)
          call FeQuestIntFromEdw(nEdw,MMaxPwd(i,KPhase,KDatBlock))
          nEdw=nEdw+1
        enddo
      endif
      go to 9999
9999  return
      end
      subroutine PwdOptionsRadiation
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      character*22 :: LabelRatio = '%I(#2)/I(#1)'
      character*14 :: LabelWaveLength1 = 'Wave length'
      character*11 :: LabelWaveLength = 'Wave length #1'
      integer RadiationOld,EdwStateQuest,FeMenu
      logical lpom,CrwLogicQuest
      save nEdwMonAngle,nEdwPerfMon,nEdwWL,nEdwWL1,nEdwWL2,nEdwRat,
     1     nEdwGAlpha,nEdwGBeta,
     2     nCrwPolarizationFirst,nCrwPolarizationLast,nCrwRadFirst,
     3     nCrwRadLast,nCrwDouble,nButtSetMonAngle,nButtInfoParallel,
     4     nButtInfoPerpendicular,nButtTube,nLblPol,nLblMono,
     5     RadiationOld,LPFactorOld
      save /PwdOptionsC/
      entry PwdOptionsRadiationMake(id)
      if(isTOF.or.isED) then
        nLblPol=0
        nCrwPolarizationFirst=0
        nCrwPolarizationLast=0
        nLblMono=0
        nEdwMonAngle=0
        nButtSetMonAngle=0
        nEdwPerfMon=0
        nButtInfoPerpendicular=0
        nButtInfoParallel=0
        Veta='Radiaton parameters can be modified only by reimporting'
        call FeQuestLblMake(id,xqdp*.5,8,Veta,'C','B')
        go to 9999
      else
        xpom=5.
        tpom=xpom+CrwgXd+10.
        ichk=1
        igrp=1
        il=1
        do i=1,3
          if(i.eq.1) then
            Veta='%X-rays'
            lpom=Radiation(KDatBlock).eq.XRayRadiation
          else if(i.eq.2) then
            Veta='Ne%utrons'
            lpom=Radiation(KDatBlock).eq.NeutronRadiation
          else if(i.eq.3) then
            Veta='%Electrons'
            lpom=Radiation(KDatBlock).eq.ElectronRadiation
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                        CrwgYd,ichk,igrp)
          if(i.eq.1) then
            nCrwRadFirst=CrwLastMade
            ilb=il
          else
            xpomb=tpom+FeTxLength(Veta)+10.
          endif
          call FeQuestCrwOpen(CrwLastMade,lpom)
          il=il+1
        enddo
        nCrwRadLast=CrwLastMade
        Veta='X%-ray tube'
        dpomb=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,xpomb,ilb,dpomb,ButYd,Veta)
        nButtTube=ButtonLastMade
        tpom=5.
        Veta='Kalpha1/Kalpha2 dou%blet'
        xpom=tpom+CrwXd+10.
        ichk=1
        igrp=0
        call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,
     1                      ichk,igrp)
        nCrwDouble=CrwLastMade
        il=il+1
        xpom=tpom+FeTxLength(LabelWaveLength1)+10.
        dpom=100.
        Veta=LabelWaveLength
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwWL=EdwLastMade
        Veta=LabelWaveLength1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwWL1=EdwLastMade
        il=il+1
        i=idel(Veta)
        Veta(i:i)='2'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwWL2=EdwLastMade
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,LabelRatio,'L',dpom,
     1                      EdwYd,0)
        nEdwRat=EdwLastMade
        il=1
        xpom=xpom+dpom+45.
        tpom=xpom+CrwgXd+10.
        Veta='Polarization correction:'
        call FeQuestLblMake(id,xpom+30.,il,Veta,'L','B')
        nLblPol=LblLastMade
        ichk=1
        igrp=2
        do i=1,5
          il=il+1
          if(i.eq.1) then
            Veta='Circul%ar polarization'
            j=PolarizedCircular
          else if(i.eq.2) then
            Veta='%Perpendicular setting'
           j=PolarizedMonoPer
          else if(i.eq.3) then
            Veta='Pa%rallel setting'
            j=PolarizedMonoPar
          else if(i.eq.4) then
            Veta='Guinier %camera'
            j=PolarizedGuinier
          else
            Veta='%Linearly polarized beam'
            j=PolarizedLinear
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        ichk,igrp)
          call FeQuestCrwOpen(CrwLastMade,LPFactor(KDatBlock).eq.j)
          if(i.eq.1) then
            nCrwPolarizationFirst=CrwLastMade
          else if(i.eq.2.or.i.eq.3) then
            if(i.eq.2) xpomb=tpom+FeTxLengthUnder(Veta)+15.
            Veta='Info'
            dpomb=FeTxLengthUnder(Veta)+10.
            call FeQuestButtonMake(id,xpomb,il,dpomb,ButYd,Veta)
            if(i.eq.2) then
              nButtInfoPerpendicular=ButtonLastMade
            else
              nButtInfoParallel=ButtonLastMade
            endif
            call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
          endif
        enddo
        nCrwPolarizationLast=CrwLastMade
        il=il+1
        Veta='Monochromator parameters:'
        call FeQuestLblMake(id,tpom+30.,il,Veta,'L','B')
        call FeQuestLblOff(LblLastMade)
        nLblMono=LblLastMade
        il=il+1
        Veta='Per%fectness'
        tpom=xpom
        xpom=tpom+FeTxLengthUnder(Veta)+80.
        dpom=100.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwPerfMon=EdwLastMade
        il=il+1
        Veta='Glancing an%gle'
        dpom=100.
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwMonAngle=EdwLastMade
        Veta='%Set glancing angle'
        dpomb=FeTxLengthUnder(Veta)+5.
        call FeQuestButtonMake(id,xpom+dpom+15.,il,
     1                         dpomb,ButYd,Veta)
        nButtSetMonAngle=ButtonLastMade
        Veta='Alp%ha(Guinier)'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwGAlpha=EdwLastMade
        il=il+1
        Veta='Beta(Gui%nier)'
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        nEdwGBeta=EdwLastMade
        LPFactorOld=-1
        RadiationOld=-1
        go to 2500
      endif
      entry PwdOptionsRadiationCheck
      if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwRadFirst.and.CheckNumber.le.nCrwRadLast)
     2  then
        if(CrwLogicQuest(nCrwRadFirst)) then
          Radiation (KDatBlock)=XRayRadiation
        else if(CrwLogicQuest(nCrwRadFirst+1)) then
          Radiation(KDatBlock)=NeutronRadiation
        else
          Radiation(KDatBlock)=ElectronRadiation
        endif
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwPolarizationFirst.and.
     2        CheckNumber.le.nCrwPolarizationLast) then
        nCrw=nCrwPolarizationFirst
        do i=1,5
          if(CrwLogicQuest(nCrw)) then
            if(i.eq.1) then
              LPFactor(KDatBlock)=PolarizedCircular
            else if(i.eq.2) then
              LPFactor(KDatBlock)=PolarizedMonoPer
            else if(i.eq.3) then
              LPFactor(KDatBlock)=PolarizedMonoPar
            else if(i.eq.4) then
              LPFactor(KDatBlock)=PolarizedGuinier
            else
              LPFactor(KDatBlock)=PolarizedLinear
            endif
            exit
          endif
          nCrw=nCrw+1
        enddo
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwDouble) then
        if(CrwLogicQuest(nCrwDouble)) then
          NAlfa(KDatBlock)=2
        else
          NAlfa(KDatBlock)=1
        endif
        RadiationOld=-1
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtTube)
     1  then
        n=7
        ypom=ButtonYMinQuest(nButtTube)-float(n)*MenuLineWidth-3.
        k=FeMenu(ButtonXMinQuest(nButtTube),ypom,LamTypeD,1,n,5,1)
        if(k.ge.1.and.k.le.7) then
          LamA1(KDatBlock)=LamA1D(k)
          LamA2(KDatBlock)=LamA2D(k)
          LamAve(KDatBlock)=LamAveD(k)
          LamRat(KDatBlock)=LamRatD(k)
          if(CrwLogicQuest(nCrwDouble)) then
            call FeQuestRealEdwOpen(nEdwWL1,LamA1(KDatBlock),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwWL2,LamA2(KDatBlock),.false.,
     1                              .false.)
            call FeQuestRealEdwOpen(nEdwRat,LamRat(KDatBlock),.false.,
     1                              .false.)
          else
            call FeQuestRealEdwOpen(nEdwWL,LamA1(KDatBlock),.false.,
     1                              .false.)
          endif
        endif
c        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        (CheckNumber.eq.nButtInfoPerpendicular.or.
     3         CheckNumber.eq.nButtInfoParallel)) then
        if(CheckNumber.eq.nButtInfoPerpendicular) then
          call FeFillTextInfo('completebasic3.txt',0)
        else if(CheckNumber.eq.nButtInfoParallel) then
          call FeFillTextInfo('completebasic4.txt',0)
        endif
        call FeInfoOut(-1.,-1.,'INFORMATION','L')
        go to 9999
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtSetMonAngle) then
        if(NAlfa(KDatBlock).gt.1) then
          call FeQuestRealFromEdw(nEdwWL1,LamA1(KDatBlock))
          call FeQuestRealFromEdw(nEdwWL2,LamA2(KDatBlock))
          call FeQuestRealFromEdw(nEdwRat,LamRat(KDatBlock))
          LamAve(KDatBlock)=(LamA1(KDatBlock)+
     1               LamA2(KDatBlock)*LamRat(KDatBlock))/
     2               (1.+LamRat(KDatBlock))
        else
          call FeQuestRealFromEdw(nEdwWL,LamA1(KDatBlock))
          LamAve(KDatBlock)=LamA1(KDatBlock)
        endif
        AngleMonPom=AngleMon(KDatBlock)
        call CrlCalcMonAngle(AngleMonPom,LamAve(KDatBlock),ichp)
        if(ichp.ne.0) go to 9999
        AngleMon(KDatBlock)=AngleMonPom
        call FeQuestRealEdwOpen(nEdwMonAngle,AngleMon(KDatBlock),
     1                          .false.,.false.)
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwWL) then
        call FeQuestRealFromEdw(nEdwWL,LamA1(KDatBlock))
        LamAve(KDatBlock)=LamA1(KDatBlock)
      else if(CheckType.eq.EventEdw.and.(CheckNumber.eq.nEdwWL1.or.
     1        CheckNumber.eq.nEdwWL2)) then
        call FeQuestRealFromEdw(nEdwWL1,LamA1(KDatBlock))
        call FeQuestRealFromEdw(nEdwWL2,LamA2(KDatBlock))
        call FeQuestRealFromEdw(nEdwRat,LamRat(KDatBlock))
        LamAve(KDatBlock)=(LamA1(KDatBlock)+
     1                     LamRat(KDatBlock)*LamA2(KDatBlock))/
     2                     (1.+LamRat(KDatBlock))
      endif
2500  if(Radiation(KDatBlock).ne.RadiationOld) then
        if(Radiation(KDatBlock).eq.XRayRadiation) then
          call FeQuestButtonOff(nButtTube)
          call FeQuestCrwOpen(nCrwDouble,NAlfa(KDatBlock).gt.1)
          if(NAlfa(KDatBlock).gt.1) then
            if(EdwStateQuest(nEdwWL1).ne.EdwOpened) then
              call FeQuestEdwClose(nEdwWL)
              call FeQuestRealEdwOpen(nEdwWL1,LamA1(KDatBlock),.false.,
     1                                .false.)
              call FeQuestRealEdwOpen(nEdwWL2,LamA2(KDatBlock),.false.,
     1                                .false.)
              call FeQuestRealEdwOpen(nEdwRat,LamRat(KDatBlock),.false.,
     1                                .false.)
            endif
          else
            if(EdwStateQuest(nEdwWL).ne.EdwOpened) then
              call FeQuestEdwClose(nEdwWL1)
              call FeQuestEdwClose(nEdwWL2)
              call FeQuestEdwClose(nEdwRat)
              call FeQuestRealEdwOpen(nEdwWL,LamA1(KDatBlock),.false.,
     1                                .false.)
            endif
          endif
        else
          call FeQuestButtonClose(nButtTube)
          call FeQuestCrwClose(nCrwDouble)
          if(EdwStateQuest(nEdwWL).ne.EdwOpened) then
            call FeQuestEdwClose(nEdwWL1)
            call FeQuestEdwClose(nEdwWL2)
            call FeQuestEdwClose(nEdwRat)
            call FeQuestRealEdwOpen(nEdwWL,LamAve(KDatBlock),.false.,
     1                              .false.)
          endif
        endif
      endif
      if(Radiation(KDatBlock).ne.RadiationOld.or.
     1   LPFactor(KDatBlock).ne.LPFactorOld) then
        if(Radiation(KDatBlock).eq.NeutronRadiation.or.
     1     Radiation(KDatBlock).eq.ElectronRadiation) then
          if(LblStateQuest(nLblPol).eq.LblOn) then
            LPFactor(KDatBlock)=PolarizedLinear
            call FeQuestLblOff(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwClose(nCrw)
            enddo
            call FeQuestButtonClose(nButtInfoPerpendicular)
            call FeQuestButtonClose(nButtInfoParallel)
          endif
        else
          if(LblStateQuest(nLblPol).eq.LblOff) then
            call FeQuestLblOn(nLblPol)
            do nCrw=nCrwPolarizationFirst,nCrwPolarizationLast
              call FeQuestCrwOpen(nCrw,nCrw.eq.nCrwPolarizationFirst)
            enddo
            call FeQuestButtonOpen(nButtInfoPerpendicular,ButtonOff)
            call FeQuestButtonOpen(nButtInfoParallel,ButtonOff)
          endif
        endif
        if(LPFactor(KDatBlock).eq.PolarizedMonoPer.or.
     1     LPFactor(KDatBlock).eq.PolarizedMonoPar) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,FractPerfMon(KDatBlock),
     1                              .false.,.false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).ne.EdwOpened) then
            if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
              call FeQuestEdwClose(nEdwGAlpha)
              call FeQuestEdwClose(nEdwGBeta)
            endif
            call FeQuestRealEdwOpen(nEdwMonAngle,AngleMon(KDatBlock),
     1                              .false.,.false.)
            call FeQuestButtonOpen(nButtSetMonAngle,ButtonOff)
          endif
        else if(LPFactor(KDatBlock).eq.PolarizedGuinier) then
          if(LblStateQuest(nLblMono).eq.LblOff) then
            call FeQuestLblOn(nLblMono)
            call FeQuestRealEdwOpen(nEdwPerfMon,FractPerfMon(KDatBlock),
     1                              .false.,.false.)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          endif
          if(EdwStateQuest(nEdwGAlpha).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwGAlpha,AlphaGMon(KDatBlock),
     1                              .false.,.false.)
            call FeQuestRealEdwOpen(nEdwGBeta,BetaGMon(KDatBlock),
     1                              .false.,.false.)
          endif
        else
          if(LblStateQuest(nLblMono).eq.LblOn) then
            call FeQuestLblOff(nLblMono)
            call FeQuestEdwClose(nEdwPerfMon)
          endif
          if(EdwStateQuest(nEdwMonAngle).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwMonAngle)
            call FeQuestButtonClose(nButtSetMonAngle)
          else if(EdwStateQuest(nEdwGAlpha).eq.EdwOpened) then
            call FeQuestEdwClose(nEdwGAlpha)
            call FeQuestEdwClose(nEdwGBeta)
          endif
        endif
      endif
      RadiationOld=Radiation(KDatBlock)
      LPFactorOld=LPFactor(KDatBlock)
c      go to 9999
      entry PwdOptionsRadiationUpdate
      if(Radiation(KDatBlock).eq.NeutronRadiation) NAlfa(KDatBlock)=1
      if(NAlfa(KDatBlock).gt.1) then
        call FeQuestRealFromEdw(nEdwWL1,LamA1(KDatBlock))
        call FeQuestRealFromEdw(nEdwWL2,LamA2(KDatBlock))
        call FeQuestRealFromEdw(nEdwRat,LamRat(KDatBlock))
        LamAve(KDatBlock)=(LamA1(KDatBlock)+
     1                     LamRat(KDatBlock)*LamA2(KDatBlock))/
     2                     (1.+LamRat(KDatBlock))
      else
        call FeQuestRealFromEdw(nEdwWL,LamA1(KDatBlock))
        LamAve(KDatBlock)=LamA1(KDatBlock)
      endif
      klam(KDatBlock)=LocateInArray(LamAve(KDatBlock),LamAveD,7,.0001)
      if(klam(KDatBlock).le.0)
     1  klam(KDatBlock)=LocateInArray(LamA1(KDatBlock),LamA1D,7,.0001)
      LorentzToCrySize(KDatBlock)=10.*LamAve(KDatBlock)/ToRad
      GaussToCrySize(KDatBlock)=sqrt8ln2*LorentzToCrySize(KDatBlock)
      if(LpFactor(KDatBlock).ne.PolarizedCircular.and.
     1   LpFactor(KDatBlock).ne.PolarizedLinear) then
        call FeQuestRealFromEdw(nEdwPerfMon,FractPerfMon(KDatBlock))
        if(LpFactor(KDatBlock).eq.PolarizedGuinier) then
          call FeQuestRealFromEdw(nEdwGAlpha,AlphaGMon(KDatBlock))
          call FeQuestRealFromEdw(nEdwGBeta,BetaGMon(KDatBlock))
        else
          call FeQuestRealFromEdw(nEdwMonAngle,AngleMon(KDatBlock))
        endif
      endif
      LamAvePwd(KDatBlock)=LamAve(KDatBlock)
      LamPwd(1,KDatBlock)=LamA1(KDatBlock)
      LamPwd(2,KDatBlock)=LamA2(KDatBlock)
9999  return
      end
      subroutine PwdOptionsProf
      use Powder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      character*21 :: StBrDirection = '%Broadening direction'
      dimension StPwdDef(:),StPwdOld(35)
      integer EdwStateQuest,CrwStateQuest
      external NToStrainString,NToStrainString4
      allocatable StPwdDef
      save nEdwCut,nEdwXe,nEdwYe,nEdwZeta,StPwdOld,nCrwXe,nCrwYe,
     1     nCrwZeta,nCrwTypeFirst,nCrwTypeLast,NLorentzW,NGaussW,
     2     KProfPwdNew
      save /PwdOptionsC/
      entry PwdOptionsProfMake(id)
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
        if(NDimI(KPhase).eq.1) then
          n=35
        else
          n=15+NDimI(KPhase)
        endif
        call CopyVek(StPwd(1,KPhase,KDatBlock),StPwdOld,n)
      else
        call SetRealArrayTo(StPwdOld,35,.01)
      endif
      il=1
      xpom=5.
      call FeQuestLblMake(id,xpom,il,'Peak-shape function','L','B')
      if(.not.isED) then
        tpom=xpom+CrwgXd+10.
        do i=1,4
          il=il+1
          if(i.eq.1) then
            Veta='%Gaussian'
          else if(i.eq.2) then
            Veta='%Lorentzian'
          else if(i.eq.3) then
            Veta='Pseudo-%Voigt'
          else if(i.eq.4) then
            Veta='%Modified Lorentzian'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,1)
          if(i.eq.1) nCrwTypeFirst=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,
     1                        i.eq.KProfPwd(KPhase,KDatBlock))
        enddo
        nCrwTypeLast=CrwLastMade
      else
        il=il+3
        tpom=35.
        call FeQuestLblMake(id,tpom,il,'Gaussian =>','L','N')
        KProfPwd(KPhase,KDatBlock)=1
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        KProfPwd(KPh,KDatBlock)=1
          enddo
        endif
        nCrwTypeFirst=0
        nCrwTypeLast=0
      endif
      KProfPwdNew=KProfPwd(KPhase,KDatBlock)
      il=2
      Veta='%Cutoff'
      tpom=xpoma(2)-FeTxLengthUnder(Veta)-10.
      dpom=40.
      call FeQuestEdwMake(id,tpom,il,xpoma(2),il,Veta,'L',dpom,EdwYd,0)
      nEdwCut=EdwLastMade
      call FeQuestRealEdwOpen(nEdwCut,PCutOff(KPhase,KDatBlock),.false.,
     1                        .false.)
      tpom=xpoma(2)+dpom+5.
      call FeQuestLblMake(id,tpom,il,'*FWHM','L','N')
      il=il+1
      if(isTOF.or.isED) then
        NGaussW=3
      else
        NGaussW=4
      endif
      j=IGaussPwd+IZdvihProf
      do i=1,NGaussW
        Veta=' '
        call FeMakeParEdwCrw(id,tpoma(2),xpoma(2),il,Veta,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                    GaussPwd(i,KPhase,KDatBlock),kiPwd(j),.false.)
        if(isTOF) then
          Veta=lGaussPwdTOF(i)(:idel(lGaussPwdTOF(i)))
        else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          Veta=lGaussPwdF(i)
        else
          Veta=lGaussPwd(i)(:idel(lGaussPwd(i)))
        endif
        call FeQuestEdwLabelChange(nEdw,Veta)
        if(i.eq.1) then
          nEdwGauss=nEdw
          nCrwGauss=nCrw
        endif
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfLorentz.or.
     1     KProfPwd(KPhase,KDatBlock).eq.IdPwdProfModLorentz.or.
     2     (i.eq.2.and.(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and.
     3     KPartBroad(KPhase,KDatBlock).le.0).and..not.isTOF).or.
     4     (i.eq.4.and.(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and.
     5      KStrain(KPhase,KDatBlock).ne.IdPwdStrainAxial).and.
     6      .not.isTOF)) then
          call FeQuestEdwDisable(nEdw)
          call FeQuestCrwDisable(nCrw)
        endif
        j=j+1
        il=il+1
      enddo
      j=ILorentzPwd+IZdvihProf
      il=3
      if(isTOF) then
        NLorentzW=5
        nCrwAniPBroad=0
      else
        NLorentzW=4
      endif
      do i=1,NLorentzW
        Veta=' '
        call FeMakeParEdwCrw(id,tpoma(3),xpoma(3),il,Veta,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                       LorentzPwd(i,KPhase,KDatBlock),kiPwd(j),
     2                       .false.)
        if(isTOF) then
          Veta=lLorentzPwdTOF(i)(:idel(lLorentzPwdTOF(i)))
        else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and.i.lt.5)
     1    then
          Veta=lLorentzPwdF(i)
        else
          Veta=lLorentzPwd(i)(:idel(lLorentzPwd(i)))
        endif
        call FeQuestEdwLabelChange(nEdw,Veta)
        if(i.eq.1) then
          nEdwLorentz=nEdw
          nCrwLorentz=nCrw
        endif
        if(i.eq.2) then
          nEdwXe=nEdw
          nCrwXe=nCrw
        else if(i.eq.4) then
          nEdwYe=nEdw
          nCrwYe=nCrw
        endif
        if(isTOF) then
          if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.or.
     1       (KStrain(KPhase,KDatBlock).ne.IdPwdStrainAxial.and.i.gt.3))
     2      then
            call FeQuestEdwDisable(nEdw)
            call FeQuestCrwDisable(nCrw)
          endif
        else
          if((i.eq.2.and.KPartBroad(KPhase,KDatBlock).le.0).or.
     1       (i.eq.4.and.
     2        KStrain(KPhase,KDatBlock).ne.IdPwdStrainAxial).or.
     3        KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss) then
            call FeQuestEdwDisable(nEdw)
            call FeQuestCrwDisable(nCrw)
          endif
        endif
        j=j+1
        il=il+1
      enddo
      if(isTOF) then
        ilp=il-2
      else
        ilp=il
      endif
      Veta=lZetaPwd
      call FeMakeParEdwCrw(id,tpoma(3),xpoma(3),ilp,Veta,nEdw,nCrw)
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor.and.
     1   KProfPwdNew.eq.IdPwdProfVoigt)
     2  call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     3                       ZetaPwd(KPhase,KDatBlock),
     3                       kiPwd(IZetaPwd+IZdvihProf),.false.)
      il=il+1
      nEdwZeta=nEdw
      nCrwZeta=nCrw
      tpom=5.
      if(.not.isTOF) then
        xpom=FeTxLength('Anisotropic particle broadening')+10.
        call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                      'Anisotropic particle broadening','L',CrwXd,
     2                      CrwYd,1,0)
        nCrwAniPBroad=CrwLastMade
        call FeQuestCrwOpen(nCrwAniPBroad,
     1                      KPartBroad(KPhase,KDatBlock).gt.0)
        if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss)
     1    call FeQuestCrwDisable(nCrwAniPBroad)
        il=il+1
      endif
      call FeQuestLblMake(id,tpom,il,
     1                    'Anisotropic strain broadening','L','B')
      nLblStrain=LblLastMade
      if(isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     1  call FeQuestLblOff(nLblStrain)
      xpom=5.
      tpom=xpom+CrwgXd+10.
      Veta='%None'
      if(KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.and.
     1   KStrain(KPhase,KDatBlock).eq.1) then
        KStrain(KPhase,KDatBlock)=0
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        KStrain(KPhase,KDatBlock)=0
          enddo
        endif
      endif
      do i=1,3
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,
     1                      CrwgYd,1,2)
        if(i.eq.1) then
          nCrwStrain=CrwLastMade
          Veta='%Axial method'
        else if(i.eq.2) then
          Veta='%Tensor method'
        endif
        call FeQuestCrwOpen(CrwLastMade,
     1                      i-1.eq.KStrain(KPhase,KDatBlock))
        if((isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental).or.
     1     (KProfPwd(KPhase,KDatBlock).eq.IdPwdProfGauss.and.i.eq.2))
     2    call FeQuestCrwDisable(CrwLastMade)
      enddo
      tpom=tpom+FeTxLengthUnder(Veta)+20.
      Veta='%Edit tensor parameters'
      xpom=FeTxLength(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
      nButEditTensor=ButtonLastMade
      call FeQuestButtonOpen(nButEditTensor,ButtonOff)
      if(KStrain(KPhase,KDatBlock).ne.IdPwdStrainTensor)
     1  call FeQuestButtonDisable(nButEditTensor)
      if(isTOF) then
        il=il-3
      else
        il=il-4
      endif
      dpom=50.
      xpom=xpoma(3)
      tpom=xpom-FeTxLengthUnder(StBrDirection)-10.
      call FeQuestEdwMake(id,tpom,il,xpom,il,StBrDirection,'L',
     1                    dpom,EdwYd,0)
      nEdwDirBroad=EdwLastMade
      call FeQuestRealAEdwOpen(nEdwDirBroad,
     1  DirBroad(1,KPhase,KDatBlock),3,.false.,.false.)
      if((KPartBroad(KPhase,KDatBlock).le.0.and.
     1    KStrain(KPhase,KDatBlock).ne.IdPwdStrainAxial).or.isTOF)
     2   call FeQuestEdwDisable(nEdwDirBroad)
      go to 9999
      entry PwdOptionsProfCheck
      if(CheckType.eq.EventCrw.and.
     1   CheckNumber.ge.nCrwTypeFirst.and.CheckNumber.le.nCrwTypeLast)
     2     then
        KProfPwdNew=CheckNumber-nCrwTypeFirst+1
        go to 2500
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwAniPBroad)
     1  then
        if(KPartBroad(KPhase,KDatBlock).le.0) then
          KPartBroad(KPhase,KDatBlock)=1
        else
          KPartBroad(KPhase,KDatBlock)=0
        endif
        j=ILorentzPwd+IZdvihProf+1
        LorentzPwd(2,KPhase,KDatBlock)=0.
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        LorentzPwd(2,KPh,KDatBlock)=0.
          enddo
        endif
        kiPwd(j)=0
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        kiPwd(j+(KPh-KPhase)*NParCellProfPwd)=0
          enddo
        endif
        if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          j=IGaussPwd+IZdvihProf+1
          GaussPwd(2,KPhase,KDatBlock)=0.
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0) GaussPwd(2,KPh,KDatBlock)=0.
            enddo
          endif
          kiPwd(j)=0
        endif
        go to 2500
      else if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwStrain.and.
     1        CheckNumber.le.nCrwStrain+2) then
        KStrain(KPhase,KDatBlock)=CheckNumber-nCrwStrain
        j=ILorentzPwd+IZdvihProf+3
        LorentzPwd(4,KPhase,KDatBlock)=0.
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        LorentzPwd(4,KPh,KDatBlock)=0.
          enddo
        endif
        kiPwd(j)=0
        if(NPhase.gt.1) then
          do KPh=1,NPhase
            if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1        kiPwd(j+(KPh-KPhase)*NParCellProfPwd)=0
          enddo
        endif
        if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          j=IGaussPwd+IZdvihProf+3
          GaussPwd(4,KPhase,KDatBlock)=0.
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0) GaussPwd(4,KPh,KDatBlock)=0.
            enddo
          endif
          kiPwd(j)=0
        endif
        j=IZetaPwd+IZdvihProf
        if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
          kiPwd(j)=0
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0)
     2          kiPwd(j+(KPh-KPhase)*NParCellProfPwd)=0
            enddo
          endif
          ZetaPwd(KPhase,KDatBlock)=.5
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0) ZetaPwd(KPh,KDatBlock)=.5
            enddo
          endif
        else
          kiPwd(j)=0
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0)
     2          kiPwd(j+(KPh-KPhase)*NParCellProfPwd)=0
            enddo
          endif
          ZetaPwd(KPhase,KDatBlock)=0.
          if(NPhase.gt.1) then
            do KPh=1,NPhase
              if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1           PhaseGr(KPhase).gt.0) ZetaPwd(KPh,KDatBlock)=0.
            enddo
          endif
        endif
        go to 2500
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButEditTensor) then
        if(NDimI(KPhase).eq.1) then
          n=35
        else
          n=15+NDimI(KPhase)
        endif
        call CopyVek(StPwdOld,StPwd(1,KPhase,KDatBlock),n)
        allocate(StPwdDef(35))
        StPwdDef=.1
        call SetRealArrayTo(StPwdDef,35,.01)
        if(NDimI(KPhase).eq.1) then
          call FeEditParameterField(StPwd (1,KPhase,KDatBlock),
     1                              StPwdS(1,KPhase,KDatBlock),StPwdDef,
     2                              kiPwd(IStPwd+IZdvihProf),n,lStPwd,
     3                              xqdp,'Edit strain parameters',tpoma,
     4                              xpoma,NToStrainString4)
        else
          call FeEditParameterField(StPwd(1,KPhase,KDatBlock),
     1                              StPwdS(1,KPhase,KDatBlock),StPwdDef,
     2                              kiPwd(IStPwd+IZdvihProf),n,lStPwd,
     3                              xqdp,'Edit strain parameters',tpoma,
     4                              xpoma,NToStrainString)
        endif
        call CopyVek(StPwd(1,KPhase,KDatBlock),StPwdOld,n)
        deallocate(StPwdDef)
        go to 9999
      endif
2500  j=ILorentzPwd+IZdvihProf
      nEdw=nEdwLorentz
      nCrw=nCrwLorentz
      if(KProfPwdNew.eq.IdPwdProfLorentz.or.
     1   KProfPwdNew.eq.IdPwdProfModLorentz.or.
     2   KProfPwdNew.eq.IdPwdProfVoigt) then
        if(EdwStateQuest(nEdw).ne.EdwOpened) then
          do i=1,NLorentzW
            if(isTOF.or.mod(i,2).ne.0) then
              call FeQuestRealEdwOpen(nEdw,
     1          LorentzPwd(i,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            j=j+1
          enddo
        endif
        if(nCrwAniPBroad.gt.0) then
          if(CrwStateQuest(nCrwAniPBroad).eq.CrwDisabled)
     1      call FeQuestCrwOpen(nCrwAniPBroad,.false.)
        endif
        if(CrwStateQuest(nCrwStrain+1).eq.CrwDisabled)
     1    call FeQuestCrwOpen(nCrwStrain+1,.false.)
        j=ILorentzPwd+IZdvihProf
        if(.not.isTOF) then
          j=j+1
          if(KPartBroad(KPhase,KDatBlock).gt.0) then
            if(EdwStateQuest(nEdwXe).ne.EdwOpened) then
              call FeQuestRealEdwOpen(nEdwXe,
     1          LorentzPwd(2,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrwXe,kiPwd(j).ne.0)
            endif
          else
            call FeQuestEdwDisable(nEdwXe)
            call FeQuestCrwDisable(nCrwXe)
          endif
        endif
        if(KStrain(KPhase,KDatBlock).ne.IdPwdStrainTensor.or.
     1     KProfPwdNew.ne.IdPwdProfVoigt) then
          if(EdwStateQuest(nEdwZeta).eq.EdwOpened) then
            call FeQuestEdwDisable(nEdwZeta)
            call FeQuestCrwDisable(nCrwZeta)
          endif
        endif
        if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
          if(isTOF) then
            nEdw=nEdwLorentz+3
            nCrw=nCrwLorentz+3
            j=ILorentzPwd+IZdvihProf
            if(EdwStateQuest(nEdw).ne.EdwOpened) then
              j=j+3
              do i=4,5
                call FeQuestRealEdwOpen(nEdw,
     1            LorentzPwd(i,KPhase,KDatBlock),.false.,.false.)
                call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
                j=j+1
                nEdw=nEdw+1
                nCrw=nCrw+1
              enddo
            endif
          else
            j=j+2
            if(EdwStateQuest(nEdwYe).ne.EdwOpened) then
              call FeQuestRealEdwOpen(nEdwYe,
     1          LorentzPwd(4,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrwYe,kiPwd(j).ne.0)
            endif
          endif
        else
          if(isTOF) then
            nEdw=nEdwLorentz+3
            nCrw=nCrwLorentz+3
            do i=4,5
              call FeQuestEdwDisable(nEdw)
              call FeQuestCrwDisable(nCrw)
              nEdw=nEdw+1
              nCrw=nCrw+1
            enddo
          else
            j=j+2
            call FeQuestEdwDisable(nEdwYe)
            call FeQuestCrwDisable(nCrwYe)
          endif
        endif
        j=j+1
        if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor.and.
     1     KProfPwdNew.eq.IdPwdProfVoigt) then
          if(EdwStateQuest(nEdwZeta).ne.EdwOpened) then
            call FeQuestRealEdwOpen(nEdwZeta,
     1        ZetaPwd(KPhase,KDatBlock),.false.,.false.)
            call FeQuestCrwOpen(nCrwZeta,kiPwd(j).ne.0)
          endif
        endif
        if((.not.isTOF.and.KPartBroad(KPhase,KDatBlock).gt.0).or.
     1     KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
          if(EdwStateQuest(nEdwDirBroad).eq.EdwDisabled) then
            call FeQuestRealAEdwOpen(nEdwDirBroad,
     1        DirBroad(1,KPhase,KDatBlock),3,.false.,.false.)
          endif
        else
          call FeQuestEdwDisable(nEdwDirBroad)
        endif
      else
        nEdw=nEdwLorentz
        nCrw=nCrwLorentz
        do i=1,NLorentzW
          call FeQuestEdwDisable(nEdw)
          call FeQuestCrwDisable(nCrw)
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
        if(CrwStateQuest(nCrwAniPBroad).ne.CrwDisabled) then
          call FeQuestCrwDisable(nCrwAniPBroad)
          call FeQuestEdwDisable(nEdwDirBroad)
          KPartBroad(KPhase,KDatBlock)=0
          call FeQuestCrwDisable(nCrwStrain+1)
          if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
            call FeQuestCrwOn(nCrwStrain)
            KStrain(KPhase,KDatBlock)=IdPwdStrainNone
            if(NPhase.gt.1) then
              do KPh=1,NPhase
                if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.
     1             PhaseGr(KPhase).gt.0)
     2            KStrain(KPh,KDatBlock)=IdPwdStrainNone
              enddo
            endif
          endif
        endif
        if(EdwStateQuest(nEdwZeta).eq.EdwOpened) then
          call FeQuestEdwDisable(nEdwZeta)
          call FeQuestCrwDisable(nCrwZeta)
        endif
      endif
      j=IGaussPwd+IZdvihProf
      nEdw=nEdwGauss
      nCrw=nCrwGauss
      if(KProfPwdNew.eq.IdPwdProfGauss.or.
     1   KProfPwdNew.eq.IdPwdProfVoigt) then
        if(EdwStateQuest(nEdw).ne.EdwOpened) then
          do i=1,NGaussW
            if(isTOF.or.mod(i,2).ne.0) then
              call FeQuestRealEdwOpen(nEdw,
     1          GaussPwd(i,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            j=j+1
          enddo
        endif
        if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          j=IGaussPwd+IZdvihProf+1
          nEdw=nEdwGauss+1
          nCrw=nCrwGauss+1
          if(KPartBroad(KPhase,KDatBlock).gt.0) then
            if(EdwStateQuest(nEdw).ne.EdwOpened) then
              call FeQuestRealEdwOpen(nEdw,
     1          GaussPwd(2,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
          else
            call FeQuestEdwDisable(nEdw)
            call FeQuestCrwDisable(nCrw)
          endif
          j=j+2
          nEdw=nEdwGauss+3
          nCrw=nCrwGauss+3
          if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial) then
            if(EdwStateQuest(nEdw).ne.EdwOpened) then
              call FeQuestRealEdwOpen(nEdw,
     1          GaussPwd(4,KPhase,KDatBlock),.false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
          else
            call FeQuestEdwDisable(nEdw)
            call FeQuestCrwDisable(nCrw)
          endif
        endif
      else
        do i=1,NGaussW
          call FeQuestEdwDisable(nEdw)
          call FeQuestCrwDisable(nCrw)
          nEdw=nEdw+1
          nCrw=nCrw+1
          j=j+1
        enddo
      endif
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor) then
        call FeQuestButtonOpen(nButEditTensor,ButtonOff)
      else
        call FeQuestButtonDisable(nButEditTensor)
      endif
      KProfPwd(KPhase,KDatBlock)=KProfPwdNew
      if(NPhase.gt.1) then
        do KPh=1,NPhase
          if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1      KProfPwd(KPh,KDatBlock)=KProfPwdNew
        enddo
      endif
      go to 9999
      entry PwdOptionsProfUpdate
      call FeQuestRealFromEdw(nEdwCut,PCutOff(KPhase,KDatBlock))
      if(NPhase.gt.1) then
        do KPh=1,NPhase
          if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1      PCutOff(KPh,KDatBlock)=PCutOff(KPhase,KDatBlock)
        enddo
      endif
      call FeUpdateParamAndKeys(nEdwGauss,nCrwGauss,
     1  GaussPwd(1,KPhase,KDatBlock),kiPwd(IGaussPwd+IZdvihProf),
     2  NGaussW)
      if(NPhase.gt.1) then
        j=IGaussPwd+IZdvihProf
        do KPh=1,NPhase
          if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1      then
            GaussPwd(1:NGaussW,KPh,KDatBlock)=
     1        GaussPwd(1:NGaussW,KPhase,KDatBlock)
            jj=j+(KPh-KPhase)*NParCellProfPwd
            kiPwd(jj:jj+NGaussW)=kiPwd(j:j+NGaussW)
          endif
        enddo
      endif
      call FeUpdateParamAndKeys(nEdwLorentz,nCrwLorentz,
     1  LorentzPwd(1,KPhase,KDatBlock),kiPwd(ILorentzPwd+IZdvihProf),5)
      if(NPhase.gt.1) then
        j=ILorentzPwd+IZdvihProf
        do KPh=1,NPhase
          if(PhaseGr(KPh).eq.PhaseGr(KPhase).and.PhaseGr(KPhase).gt.0)
     1      then
            LorentzPwd(1:5,KPh,KDatBlock)=
     1        LorentzPwd(1:5,KPhase,KDatBlock)
            jj=j+(KPh-KPhase)*NParCellProfPwd
            kiPwd(jj:jj+5)=kiPwd(j:j+5)
          endif
        enddo
      endif
      if(KStrain(KPhase,KDatBlock).eq.IdPwdStrainTensor.and.
     1   KProfPwdNew.eq.IdPwdProfVoigt)
     2  call FeUpdateParamAndKeys(nEdwZeta,nCrwZeta,
     3    ZetaPwd(KPhase,KDatBlock),kiPwd(IZetaPwd+IZdvihProf),1)
      if(KPartBroad(KPhase,KDatBlock).gt.0.or.
     1   KStrain(KPhase,KDatBlock).eq.IdPwdStrainAxial)
     2  call FeQuestRealAFromEdw(nEdwDirBroad,
     3                           DirBroad(1,KPhase,KDatBlock))
9999  return
      end
      subroutine PwdOptionsAsym
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      character*24 :: AsymFundamentalLabel(10) =
     1                (/'Primary radius [mm]   ',
     2                  'Secondary radius [mm] ',
     3                  'RS width [mm]         ',
     4                  'FDS angle [deg]       ',
     5                  'VDS angle [mm]        ',
     6                  'Source length [mm]    ',
     7                  'Sample length [mm]    ',
     8                  'RS length [mm]        ',
     9                  'Primary soller [deg]  ',
     a                  'Secondary soller [deg]'/),
     a                 AsymTOFLabel(3) =
     1                 (/'%None                   ',
     2                   '%Simple rise/decay      ',
     3                   '%Jason Hodges rise/decay'/)
      character*28 :: AsymMethodLabel(6) =
     1                (/'%No correction              ',
     2                  'Si%mpson correction         ',
     3                  '%Berar-Baldinozzi correction',
     4                  'correction by di%vergence   ',
     5                  '%fundamental approach       ',
     6                  '%Debye-Scherer integration  '/)
      character*42 :: AsymDebyeIntLabel(6) =
     1                (/'Relative bandwidth of monochromator     ',
     2                  'Pixel size in microns                   ',
     3                  'Beam height in microns                  ',
     4                  'Capillary diameter in microns           ',
     5                  'Incident beam divergence in milliradians',
     6                  'Radius of sample-to-image arc in mm     '/)
      character*9 :: AsymLabel(2) =
     1               (/'1/tg(th) ',
     2                 '1/tg(2th)'/)
      logical Otevirat,lpom,CrwLogicQuest
      integer CrwStateQuest,EdwStateQuest
      real :: AsymPwdSave(6),
     1        AsymPwdDef (6)=(/.140,42.2,50.,300.,.0714,300./)
      external NToString
      save nCrwFundRefFr,nCrwSSoll,nCrwFundSelFr,nCrwKUseHS,
     1     nCrwFundSelTo,nCrwUseRSW,nCrwUseFDS,nCrwUseVDS,nCrwRSW,
     2     nCrwDS,nCrwPSoll,nCrwFDS,nCrwVDS,nCrwUsePSoll,nCrwUseSSoll,
     4     nEdwFundFr,nEdwRSW,nEdwDS,nEdwPSoll,nEdwSSoll,nEdwNAsym,
     5     nEdwFDS,nEdwVDS,
     6     nLblFundFr,nLblFundTo,nLblFirst,nLinkaFundFr,nLinkaFundTo,
     7     nEdwDebyeIntFr,nEdwDebyeIntTo,AsymPwdSave
      save /PwdOptionsC/
      entry PwdOptionsAsymMake(id)
      il=1
      ilal=il
      do i=1,2
        call FeQuestLblMake(id,xpoma(i+1)+20.,ilal,AsymLabel(i),'C','N')
        call FeQuestLblOff(LblLastMade)
        if(i.eq.1) nLblFirst=LblLastMade
      enddo
      if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymBerar) then
        nLbl=nLblFirst
        do i=1,2
          call FeQuestLblOn(nLbl)
          nLbl=nLbl+1
        enddo
      endif
      il=il+1
      ilp=il
      NAsymNew=NAsym(KDatBlock)
      KAsymNew=KAsym(KDatBlock)
      nLblFundFr=0
      nLinkaFundFr=0
      nCrwFundRefFr=0
      nEdwDebyeIntFr=0
      nEdwFundFr=0
      nEdwRSW=0
      nCrwRSW=0
      nEdwDS=0
      nCrwDS=0
      nEdwPSoll=0
      nCrwPSoll=0
      nEdwSSoll=0
      nCrwSSoll=0
      nCrwFundSelFr=0
      nCrwFundSelTo=0
      nCrwUseRSW=0
      nCrwUseFDS=0
      nCrwUseVDS=0
      nEdwFDS=0
      nCrwFDS=0
      nEdwVDS=0
      nCrwVDS=0
      nEdwNAsym=0
      xpomc=0.
      if(isTOF) then
        ik=3
        xpom=5.
        tpom=xpom+CrwgXd+10.
        do i=1,ik
          call FeQuestCrwMake(id,tpom,il,xpom,il,
     1                        AsymTOFLabel(i),'L',CrwgXd,CrwgYd,1,2)
          if(i.eq.1) nCrwAsymFr=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.KAsym(KDatBlock)+1)
          il=il+1
        enddo
        nCrwAsymTo=CrwLastMade
        il=2
        im=2
        j=IAsymPwd+IZdvihRec
        AsymOrg=0.
        KiAsymOrg=0
        AsymOrg(1,1)=.05
        AsymOrg(3,1)=.05
        AsymOrg(1,2)=2.
        AsymOrg(3,2)=2.
        AsymOrg(5,2)=50.
        AsymOrg(7,2)=50.
        do i=1,8
          if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
            if(i.le.4) then
              Veta=lAsymTOF1Pwd(i)
            else
              Veta=' '
            endif
          else
            Veta=lAsymTOF2Pwd(i)
          endif
          call FeMakeParEdwCrw(id,tpoma(im),xpoma(im),il,Veta,nEdw,nCrw)
          if(i.eq.1) then
            nEdwAsymP=nEdw
            nCrwAsymP=nCrw
          endif
          if(KAsym(KDatBlock).ne.IdPwdAsymTOFNone.and.
     1      (KAsym(KDatBlock).ne.IdPwdAsymTOF1.or.i.le.4)) then
            call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                           AsymPwd(i,KDatBlock),kiPwd(j),.false.)
            if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
              AsymOrg(i,1)=AsymPwd(i,KDatBlock)
              KiAsymOrg(i,1)=kiPwd(j)
            else
              AsymOrg(i,2)=AsymPwd(i,KDatBlock)
              KiAsymOrg(i,2)=kiPwd(j)
            endif
          endif
          if(mod(i,2).eq.0) then
            il=il+1
            im=2
          else
            im=im+1
          endif
          j=j+1
        enddo
        if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
          NAsymOld=4
        else
          NAsymOld=8
        endif
      else
        xpomc=5.
        tpom=xpomc+CrwgXd+10.
        do i=1,6
          call FeQuestCrwMake(id,tpom,il,xpomc,il,
     1                        AsymMethodLabel(i),'L',CrwgXd,CrwgYd,1,2)
          if(i.eq.1) nCrwAsymFr=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.KAsym(KDatBlock)+1)
          il=il+1
          if((i.eq.5.and.PwdMethod(KDatBlock).eq.IdPwdMethodDS).or.
     1       (i.eq.6.and.PwdMethod(KDatBlock).ne.IdPwdMethodDS))
     2      call FeQuestCrwDisable(CrwLastMade)
        enddo
        nCrwAsymTo=CrwLastMade
        il=3
        xpom=288.
        tpom=xpom+CrwgXd+10.
        Veta='%Use H/L and S/L'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      0)
        nCrwKUseHS=CrwLastMade
        il=2
        xpom=0.
        do i=1,10
          xpom=max(xpom,FeTxLengthUnder(AsymFundamentalLabel(i)))
        enddo
        tpom=tpoma(2)+5.
        xpom=tpom+xpom+30.
        dpom=50.
        xpomu=xpom-CrwgXd-15.
        xpomr=xpom+dpom+3.
        j=IAsymPwd+IZdvihRec
        do i=1,10
          Otevirat=(i.ne.4.and.i.ne.5).or.
     1      PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2      (i.eq.4.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3      (i.eq.5.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
          call FeQuestLblMake(id,tpom,il,
     1                        AsymFundamentalLabel(i),'L','N')
          if(nLblFundFr.le.0) nLblFundFr=LblLastMade
          nLblFundTo=LblLastMade
          if(KAsym(KDatBlock).ne.IdPwdAsymFundamental.or..not.Otevirat)
     1      call FeQuestLblOff(LblLastMade)
          if(i.le.2) then
            call FeQuestEdwMake(id,xpom,il,xpom,il,' ','L',dpom,EdwYd,0)
            if(nEdwFundFr.le.0) nEdwFundFr=EdwLastMade
          else
            call FeMakeParEdwCrw(id,xpom,xpom,il,' ',nEdw,nCrw)
            if(nCrwFundRefFr.le.0) nCrwFundRefFr=nCrw
            if(i.eq.3) then
              nEdwRSW=nEdw
              nCrwRSW=nCrw
            else if(i.eq.4) then
              nEdwFDS=nEdw
              nCrwFDS=nCrw
            else if(i.eq.5) then
              nEdwVDS=nEdw
              nCrwVDS=nCrw
            else if(i.eq.9) then
              nEdwPSoll=nEdw
              nCrwPSoll=nCrw
            else if(i.eq.10) then
              nEdwSSoll=nEdw
              nCrwSSoll=nCrw
            endif
          endif
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
            if(i.eq.1) then
              pom=RadPrim(KDatBlock)
            else if(i.eq.2) then
              pom=RadSec(KDatBlock)
            else if(i.eq.3.or.i.eq.4) then
              pom=AsymPwd(i-2,KDatBlock)
            else
              pom=AsymPwd(i-3,KDatBlock)
            endif
          endif
          if(i.le.2) then
            if(KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     1        call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
          else
            if(Otevirat) then
              if(KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     1          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kiPwd(j),
     2                              .false.)
            else
              il=il-1
            endif
            if(i.ne.5) j=j+1
          endif
          if(i.eq.2.or.i.eq.5) then
            il=il+1
            call FeQuestLinkaFromToMake(id,tpom,xqdp-5.,il)
            if(nLinkaFundFr.le.0) nLinkaFundFr=LinkaLastMade
            nLinkaFundTo=LinkaLastMade
            if(KAsym(KDatBlock).ne.IdPwdAsymFundamental)
     1        call FeQuestLinkaOff(LinkaLastMade)
          endif
          il=il+1
        enddo
        il=5
        do i=1,5
          Otevirat=(i.ne.2.and.i.ne.3).or.
     1      PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2      (i.eq.2.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3      (i.eq.3.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
          if(i.eq.1) then
            lpom=KUseRSW(KDatBlock).eq.1
          else if(i.eq.2) then
            lpom=KUseDS(KDatBlock).eq.1
            il=il+1
          else if(i.eq.3) then
            lpom=KUseDS(KDatBlock).eq.2
            il=il+1
          else if(i.eq.4) then
            lpom=KUsePSoll(KDatBlock).eq.1
            il=il+5
          else if(i.eq.5) then
            lpom=KUseSSoll(KDatBlock).eq.1
            il=il+1
          endif
          call FeQuestCrwMake(id,xpomu,il,xpomu,il,' ','L',CrwXd,CrwYd,
     1                        1,0)
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and.Otevirat)
     1      call FeQuestCrwOpen(CrwLastMade,lpom)
          if(.not.Otevirat) il=il-1
          if(i.eq.1) then
            nCrwFundSelFr=CrwLastMade
            nCrwUseRSW=CrwLastMade
            nEdw=nEdwRSW
            nCrw=nCrwRSW
          else if(i.eq.2) then
            nCrwUseFDS=CrwLastMade
            nEdw=nEdwFDS
            nCrw=nCrwFDS
          else if(i.eq.3) then
            nCrwUseVDS=CrwLastMade
            nEdw=nEdwVDS
            nCrw=nCrwVDS
          else if(i.eq.4) then
            nCrwUsePSoll=CrwLastMade
            nEdw=nEdwPSoll
            nCrw=nCrwPSoll
          else if(i.eq.5) then
            nCrwUseSSoll=CrwLastMade
            nEdw=nEdwSSoll
            nCrw=nCrwSSoll
          endif
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental.and..not.lpom)
     1      then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
        enddo
        nCrwFundSelTo=CrwLastMade
        il=2
        im=2
        j=IAsymPwd+IZdvihRec
        do i=1,12
          if(KAsym(KDatBlock).eq.IdPwdAsymDivergence.and.i.le.2)
     1      then
            if(KUseHS(KDatBlock).ge.1) then
              ii=0
            else
              ii=2
            endif
            Veta=LAsymPwdD(i+ii)
          else
            Veta=LAsymPwd
            call NToString(i,Veta(5:))
          endif
          call FeMakeParEdwCrw(id,tpoma(im),xpoma(im),il,Veta,nEdw,nCrw)
          if(i.eq.1) then
            nEdwAsymP=nEdw
            nCrwAsymP=nCrw
          endif
          if(i.le.NAsym(KDatBlock).and.
     1       KAsym(KDatBlock).ne.IdPwdAsymFundamental.and.
     2       KAsym(KDatBlock).ne.IdPwdAsymDebyeInt.and.
     3       KAsym(KDatBlock).ne.IdPwdAsymNone) then
            call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1        AsymPwd(i,KDatBlock),kiPwd(j),.false.)
          endif
          j=j+1
          if(mod(i,2).eq.0) then
            il=il+1
            im=2
          else
            im=im+1
          endif
        enddo
        Veta='N%umber of terms'
        xpom=FeTxLengthUnder(Veta)+12.+CrwXd
        tpom=5.
        dpom=40.
        call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
        nEdwNAsym=EdwLastMade
        if(KAsym(KDatBlock).eq.IdPwdAsymBerar) then
          call FeQuestIntEdwOpen(nEdwNAsym,NAsym(KDatBlock),.false.)
          call FeQuestEudOpen(nEdwNAsym,2,12,2,pom,pom,pom)
        endif
        do i=1,5
          xpom=max(xpom,FeTxLengthUnder(AsymDebyeIntLabel(i)))
        enddo
        il=1
        tpom=250.
        xpom=tpom+xpom+10.
        dpom=70.
        do i=1,6
          il=il+1
          call FeQuestEdwMake(id,tpom,il,xpom,il,AsymDebyeIntLabel(i),
     1                        'L',dpom,EdwYd,0)
          if(KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
            call FeQuestRealEdwOpen(EdwLastMade,AsymPwd(i,KDatBlock),
     1                              .false.,.false.)
            AsymPwdSave(i)=AsymPwd(i,KDatBlock)
          else
            AsymPwdSave(i)=AsymPwdDef(i)
          endif
          if(nEdwDebyeIntFr.le.0) nEdwDebyeIntFr=EdwLastMade
        enddo
        nEdwDebyeIntTo=EdwLastMade
      endif
      if(KAsym(KDatBlock).eq.IdPwdAsymDivergence)
     1  call FeQuestCrwOpen(nCrwKUseHS,KUseHS(KDatBlock).ge.1)
      go to 9999
      entry PwdOptionsAsymCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwAsymFr.and.
     1   CheckNumber.le.nCrwAsymTo) then
        KAsymNew=CheckNumber-nCrwAsymFr
        if(isTOF) then
          if(KAsymNew.eq.IdPwdAsymTOFNone) then
            NAsymNew=0
          else if(KAsymNew.eq.IdPwdAsymTOF1) then
            NAsymNew=4
          else if(KAsymNew.eq.IdPwdAsymTOF2) then
            NAsymNew=8
          endif
        else
          if(KAsymNew.eq.IdPwdAsymNone) then
            NAsymNew=0
          else if(KAsymNew.eq.IdPwdAsymSimpson) then
            NAsymNew=1
          else if(KAsymNew.eq.IdPwdAsymBerar) then
            NAsymNew=4
          else if(KAsymNew.eq.IdPwdAsymDivergence) then
            NAsymNew=2
          else if(KAsymNew.eq.IdPwdAsymFundamental) then
            NAsymNew=7
          else if(KAsymNew.eq.IdPwdAsymDebyeInt) then
            NAsymNew=6
          endif
        endif
        NAsymOld=NAsym(KDatBlock)
        NAsym(KDatBlock)=0
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwKUseHS) then
        call FeQuestRealFromEdw(nEdwAsymP,pom1)
        call FeQuestRealFromEdw(nEdwAsymP+1,pom2)
        if(CrwLogicQuest(CheckNumber)) then
          KUseHS(KDatBlock)=1
          AsymPwd(1,KDatBlock)=(pom1+pom2)*.5
          AsymPwd(2,KDatBlock)=(pom1-pom2)*.5
          i=0
        else
          KUseHS(KDatBlock)=0
          AsymPwd(1,KDatBlock)=pom1+pom2
          AsymPwd(2,KDatBlock)=pom1-pom2
          i=2
        endif
        call FeQuestEdwLabelChange(nEdwAsymP,LAsymPwdD(i+1))
        call FeQuestRealEdwOpen(nEdwAsymP,AsymPwd(1,KDatBlock),
     1                          .false.,.false.)
        call FeQuestEdwLabelChange(nEdwAsymP+1,LAsymPwdD(i+2))
        call FeQuestRealEdwOpen(nEdwAsymP+1,AsymPwd(2,KDatBlock),
     1                          .false.,.false.)
      else if(CheckType.eq.EventCrw.and.(CheckNumber.eq.nCrwUseRSW.or.
     1                                   CheckNumber.eq.nCrwUseFDS.or.
     2                                   CheckNumber.eq.nCrwUseVDS.or.
     3                                   CheckNumber.eq.nCrwUsePSoll.or.
     4                                   CheckNumber.eq.nCrwUseSSoll))
     5  then
        if(CheckNumber.eq.nCrwUseRSW) then
          nEdw=nEdwRSW
          nCrw=nCrwRSW
          i=1
        else if(CheckNumber.eq.nCrwUseFDS) then
          nEdw=nEdwFDS
          nCrw=nCrwFDS
          i=2
          if(CrwLogicQuest(CheckNumber).and.
     1       CrwStateQuest(nCrwUseVDS).ne.CrwClosed) then
            call FeQuestCrwOff(nCrwUseVDS)
            call FeQuestEdwClose(nEdwVDS)
            call FeQuestCrwClose(nCrwVDS)
            AsymPwd(i,KDatBlock)=1.
          endif
        else if(CheckNumber.eq.nCrwUseVDS) then
          nEdw=nEdwVDS
          nCrw=nCrwVDS
          i=2
          if(CrwLogicQuest(CheckNumber).and.
     1       CrwStateQuest(nCrwUseFDS).ne.CrwClosed) then
            call FeQuestCrwOff(nCrwUseFDS)
            call FeQuestEdwClose(nEdwFDS)
            call FeQuestCrwClose(nCrwFDS)
            AsymPwd(i,KDatBlock)=10.
          endif
        else if(CheckNumber.eq.nCrwUsePSoll) then
          nEdw=nEdwPSoll
          nCrw=nCrwPSoll
          i=6
        else if(CheckNumber.eq.nCrwUseSSoll) then
          nEdw=nEdwSSoll
          nCrw=nCrwSSoll
          i=7
        endif
        j=IAsymPwd+IZdvihRec+i-1
        if(CrwLogicQuest(CheckNumber)) then
          call FeQuestRealEdwOpen(nEdw,AsymPwd(i,KDatBlock),.false.,
     1                            .false.)
          call FeQuestCrwOpen(nCrw,kipwd(j).eq.1)
          EventType=EventEdw
          EventNumber=nEdw
        else
          if(CrwLogicQuest(nCrw)) then
            kipwd(j)=1
          else
            kipwd(j)=0
          endif
          call FeQuestRealFromEdw(nEdw,AsymPwd(i,KDatBlock))
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
        go to 9999
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwNAsym) then
        call FeQuestIntFromEdw(nEdwNAsym,NAsymNew)
      endif
      go to 2500
      entry PwdOptionsAsymRefresh(KAsymNewIn)
      KAsymNew=KAsymNewIn
      if(isTOF) then
        if(KAsymNew.eq.IdPwdAsymTOFNone) then
          NAsymNew=0
        else if(KAsymNew.eq.IdPwdAsymTOF1) then
          NAsymNew=4
        else if(KAsymNew.eq.IdPwdAsymTOF2) then
          NAsymNew=8
        endif
      else
        if(KAsymNew.eq.IdPwdAsymNone) then
          NAsymNew=0
        else if(KAsymNew.eq.IdPwdAsymSimpson) then
          NAsymNew=1
        else if(KAsymNew.eq.IdPwdAsymBerar) then
          NAsymNew=4
        else if(KAsymNew.eq.IdPwdAsymDivergence) then
          NAsymNew=2
        else if(KAsymNew.eq.IdPwdAsymFundamental) then
          NAsymNew=7
        else if(KAsymNew.eq.IdPwdAsymDebyeInt) then
          NAsymNew=6
        endif
      endif
2500  if(isTOF) then
        if(KAsym(KDatBlock).eq.IdPwdAsymTOFNone) then
          n=0
        else if(KAsym(KDatBlock).eq.IdPwdAsymTOF1) then
          n=1
        else
          n=2
        endif
        if(n.gt.0)
     1    call FeUpdateParamAndKeys(nEdwAsymP,nCrwAsymP,
     2                              AsymOrg(1,n),kiAsymOrg(1,n),
     3                              NAsymOld)
        if(KAsymNew.eq.IdPwdAsymTOFNone) then
          n=0
        else if(KAsymNew.eq.IdPwdAsymTOF1) then
          n=1
        else
          n=2
        endif
        nEdw=nEdwAsymP
        nCrw=nCrwAsymP
        if(KAsymNew.eq.IdPwdAsymTOFNone) then
          do i=1,8
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
            nEdw=nEdw+1
            nCrw=nCrw+1
          enddo
        else if(KAsymNew.eq.IdPwdAsymTOF1) then
          j=IAsymPwd+IZdvihRec
          do i=1,8
            if(i.le.4) then
              call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1          AsymOrg(i,n),kiAsymOrg(i,n),.false.)
              call FeQuestEdwLabelChange(nEdw,lAsymTOF1Pwd(i))
            else
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
            endif
            nEdw=nEdw+1
            nCrw=nCrw+1
            j=j+1
          enddo
          NAsymNew=4
        else if(KAsymNew.eq.IdPwdAsymTOF2) then
          j=IAsymPwd+IZdvihRec
          do i=1,8
            call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1        AsymOrg(i,n),kiAsymOrg(i,n),.false.)
            call FeQuestEdwLabelChange(nEdw,lAsymTOF2Pwd(i))
            nEdw=nEdw+1
            nCrw=nCrw+1
            j=j+1
          enddo
          NAsymNew=8
        endif
      else
        if(KAsymNew.ne.IdPwdAsymFundamental.and.
     1     KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
          do i=nLblFundFr,nLblFundTo
            call FeQuestLblOff(i)
          enddo
          do i=nLinkaFundFr,nLinkaFundTo
            call FeQuestLinkaOff(i)
          enddo
          do i=nCrwFundSelFr,nCrwFundSelTo
            call FeQuestCrwClose(i)
          enddo
          nEdw=nEdwFundFr
          nCrw=nCrwFundRefFr
          do i=1,10
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
            if(i.gt.2) then
              call FeQuestCrwClose(nCrw)
              nCrw=nCrw+1
            endif
          enddo
        endif
        if(KAsymNew.ne.IdPwdAsymDebyeInt.and.
     1     KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
          nEdw=nEdwDebyeIntFr
          do i=1,6
            if(EdwStateQuest(nEdw).eq.EdwOpened)
     1        call FeQuestRealFromEdw(nEdw,AsymPwdSave(i))
            call FeQuestEdwClose(nEdw)
            nEdw=nEdw+1
          enddo
        endif
        if(KAsymNew.ne.IdPwdAsymBerar) then
          nLbl=nLblFirst
          do i=1,2
            call FeQuestLblOff(nLbl)
            nLbl=nLbl+1
          enddo
          if(EdwStateQuest(nEdwNAsym).ne.EdwClosed) then
            call FeQuestEdwClose(nEdwNAsym)
          endif
        else if(KAsymNew.eq.IdPwdAsymBerar) then
          if(EdwStateQuest(nEdwNAsym).ne.EdwOpened) then
            nLbl=nLblFirst
            do i=1,2
              call FeQuestLblOn(nLbl)
              nLbl=nLbl+1
            enddo
            call FeQuestIntEdwOpen(nEdwNAsym,NAsymNew,.false.)
            call FeQuestEudOpen(nEdwNAsym,2,12,2,pom,pom,pom)
          endif
        endif
        if(KAsymNew.ne.IdPwdAsymDivergence.and.
     1     CrwStateQuest(nCrwKUseHS).ne.CrwClosed)
     2     call FeQuestCrwClose(nCrwKUseHS)
        nEdw=nEdwAsymP
        nCrw=nCrwAsymP
        il=2
        im=2
        j=IAsymPwd+IZdvihRec
        do i=1,12
          if((KAsymNew.eq.IdPwdAsymDivergence.or.
     1        KAsym(KDatBlock).eq.IdPwdAsymDivergence).and.
     2        i.le.2.and.KAsymNew.ne.KAsym(KDatBlock)) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
            if(KAsymNew.eq.IdPwdAsymDivergence) then
              if(i.eq.1) then
                AsymPwd(i,KDatBlock)= 0.01
                KUseHS(KDatBlock)=0
              else if(i.eq.2) then
                 AsymPwd(i,KDatBlock)=-0.005
              endif
              Veta=LAsymPwdD(i+2)
            else
              AsymPwd(i,KDatBlock)=0.01
              Veta=lAsymPwd
              call NToString(i,Veta(5:))
            endif
            call FeQuestEdwLabelChange(nEdw,Veta)
            call FeQuestRealEdwOpen(nEdw,AsymPwd(i,KDatBlock),.false.,
     1                              .false.)
            call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
          endif
          if(i.gt.NAsymNew.or.KAsymNew.eq.IdPwdAsymFundamental.or.
     1       KAsymNew.eq.IdPwdAsymDebyeInt) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          else if(i.le.NAsymNew.and.KAsymNew.ne.IdPwdAsymDivergence)
     1      then
            if(i.gt.NAsym(KDatBlock)) then
              AsymPwd(i,KDatBlock)=0.001
              call FeQuestRealEdwOpen(nEdw,AsymPwd(i,KDatBlock),
     1                                .false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          j=j+1
          if(mod(i,2).eq.0) then
            il=il+1
            im=2
          else
            im=im+1
          endif
        enddo
        if(KAsymNew.eq.IdPwdAsymFundamental.and.
     1     KAsym(KDatBlock).ne.IdPwdAsymFundamental) then
          call PwdSetFundamentalParameters
          i=0
          do nLbl=nLblFundFr,nLblFundTo
            i=i+1
            Otevirat=(i.ne.4.and.i.ne.5).or.
     1        PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2       (i.eq.4.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3       (i.eq.5.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
            if(Otevirat) call FeQuestLblOn(nLbl)
          enddo
          do i=nLinkaFundFr,nLinkaFundTo
            call FeQuestLinkaOn(i)
          enddo
          j=IAsymPwd+IZdvihRec
          nEdw=nEdwFundFr
          nCrw=nCrwFundRefFr
          do i=1,10
            Otevirat=(i.ne.4.and.i.ne.5).or.
     1        PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2       (i.eq.4.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3       (i.eq.5.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
            if(i.eq.1) then
              pom=RadPrim(KDatBlock)
            else if(i.eq.2) then
              pom=RadSec(KDatBlock)
            else if(i.eq.3.or.i.eq.4) then
              pom=AsymPwd(i-2,KDatBlock)
            else
              pom=AsymPwd(i-3,KDatBlock)
            endif
            if((i.eq.4.and.KUseDS(KDatBlock).ne.1).or.
     1         (i.eq.5.and.KUseDS(KDatBlock).ne.2)) then
              nCrw=nCrw+1
              nEdw=nEdw+1
              cycle
            endif
            if(i.le.2) then
              if(Otevirat)
     1          call FeQuestRealEdwOpen(nEdw,pom,.false.,.false.)
            else
              if(Otevirat)
     1          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',pom,kiPwd(j),
     2                               .false.)
              nCrw=nCrw+1
            endif
            nEdw=nEdw+1
          enddo
          i=0
          do nCrwP=nCrwFundSelFr,nCrwFundSelTo
            i=i+1
            Otevirat=(i.ne.2.and.i.ne.3).or.
     1        PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.or.
     2       (i.eq.2.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS).or.
     3       (i.eq.3.and.PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)
            if(i.eq.1) then
              lpom=KUseRSW(KDatBlock).eq.1
            else if(i.eq.2) then
              lpom=KUseDS(KDatBlock).eq.1
            else if(i.eq.3) then
              lpom=KUseDS(KDatBlock).eq.2
            else if(i.eq.4) then
              lpom=KUsePSoll(KDatBlock).eq.1
            else if(i.eq.5) then
              lpom=KUseSSoll(KDatBlock).eq.1
            endif
            if(Otevirat) call FeQuestCrwOpen(nCrwP,lpom)
            if(i.eq.1) then
              nEdw=nEdwRSW
              nCrw=nCrwRSW
            else if(i.eq.2) then
              nEdw=nEdwFDS
              nCrw=nCrwFDS
            else if(i.eq.3) then
              nEdw=nEdwVDS
              nCrw=nCrwVDS
            else if(i.eq.4) then
              nEdw=nEdwPSoll
              nCrw=nCrwPSoll
            else if(i.eq.5) then
              nEdw=nEdwSSoll
              nCrw=nCrwSSoll
            endif
            if(.not.lpom) then
              call FeQuestEdwClose(nEdw)
              call FeQuestCrwClose(nCrw)
            endif
          enddo
        else if(KAsymNew.eq.IdPwdAsymDebyeInt.and.
     1          KAsym(KDatBlock).ne.IdPwdAsymDebyeInt) then
          nEdw=nEdwDebyeIntFr
          do i=1,6
            call FeQuestRealEdwOpen(nEdw,AsymPwdSave(i),.false.,.false.)
            nEdw=nEdw+1
          enddo
        endif
      endif
      if(KAsymNew.eq.IdPwdAsymDivergence.and.
     1   CrwStateQuest(nCrwKUseHS).eq.CrwClosed)
     2  call FeQuestCrwOpen(nCrwKUseHS,KUseHS(KDatBlock).ge.1)
      KAsym(KDatBlock)=KAsymNew
      NAsym(KDatBlock)=NAsymNew
      go to 9999
      entry PwdOptionsAsymUpdate
      if(KAsym(KDatBlock).ne.IdPwdAsymFundamental.and.
     1   KAsym(KDatBlock).ne.IdPwdAsymDebyeInt.and.
     2   KAsym(KDatBlock).ne.IdPwdAsymNone) then
        call FeUpdateParamAndKeys(nEdwAsymP,nCrwAsymP,
     1    AsymPwd(1,KDatBlock),kiPwd(IAsymPwd+IZdvihRec),
     2    NAsym(KDatBlock))
      else if(KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        call FeQuestRealFromEdw(nEdwFundFr  ,RadPrim(KDatBlock))
        call FeQuestRealFromEdw(nEdwFundFr+1,RadSec (KDatBlock))
        nEdw=nEdwFundFr+2
        nCrw=nCrwFundRefFr
        j=IAsymPwd+IZdvihRec
        k=1
        do i=3,10
          if(EdwStateQuest(nEdw).ne.EdwClosed) then
            call FeUpdateParamAndKeys(nEdw,nCrw,AsymPwd(k,KDatBlock),
     1                                kiPwd(j),1)
          endif
          if(i.ne.4) then
            j=j+1
            k=k+1
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
        nCrw=nCrwFundSelFr
        KUseRSW(KDatBlock)=0
        KUseDS(KDatBlock)=0
        KUsePSoll(KDatBlock)=0
        KUseSSoll(KDatBlock)=0
        nCrw=nCrwFundSelFr
        do i=1,nCrwFundSelTo-nCrwFundSelFr+1
          if(CrwStateQuest(nCrw).ne.CrwClosed) then
            lpom=CrwLogicQuest(nCrw)
            if(i.eq.1) then
              if(lpom) KUseRSW(KDatBlock)=1
            else if(i.eq.2) then
              if(lpom) KUseDS(KDatBlock)=1
            else if(i.eq.3) then
              if(lpom) KUseDS(KDatBlock)=2
            else if(i.eq.4) then
              if(lpom) KUsePSoll(KDatBlock)=1
            else if(i.eq.5) then
              if(lpom) KUseSSoll(KDatBlock)=1
            endif
          endif
          nCrw=nCrw+1
        enddo
      else if(KAsym(KDatBlock).eq.IdPwdAsymDebyeInt) then
        nEdw=nEdwDebyeIntFr
        j=IAsymPwd+IZdvihRec
        do i=1,6
          call FeQuestRealFromEdw(nEdw,AsymPwd(i,KDatBlock))
          nEdw=nEdw+1
          kiPwd(j)=0
        enddo
      endif
9999  return
      end
      subroutine PwdOptionsSample
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      character*24 :: StAverage = 'Average over equivalents'
      character*5  :: StMir = 'mi*%r'
      logical CrwLogicQuest
      integer CrwStateQuest
      save nEdwPrefP,nEdwDirPref,nEdwTOFAbs,nEdwMir,nEdwRoughP,
     2     nCrwPrefP,nCrwTOFAbs,nCrwRoughP,nCrwKPrefFirst,nCrwKPrefLast,
     3     nCrwKRoughFirst,nCrwKRoughLast,nCrwPrefAve,nCrwKAbsorFirst,
     4     nCrwKAbsorLast,nCrwIllumFirst,nCrwIllumLast,nCrwFocus,
     5     nLblRough,nLblIllum,nLblTOFAbs,
     6     KPrefOld,KRoughOld,KAbsorNew
      save /PwdOptionsC/
      entry PwdOptionsSampleMake(id)
      ICrwGroup=0
      il=1
      call FeQuestLblMake(id,5.,il,'Preferred orientation','L','B')
      KPrefOld=KPref(KPhase,KDatBlock)
      xpom=5.
      tpom=xpom+CrwgXd+10.
      ICrwGroup=ICrwGroup+1
      do i=1,3
        il=il+1
        if(i.eq.1) then
          Veta='%None'
        else if(i.eq.2) then
          Veta='%March-Dollase'
        else
          Veta='%Sasa-Uda'
        endif
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      ICrwGroup)
        if(i.eq.1) nCrwKPrefFirst=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,i-1.eq.KPref(KPhase,KDatBlock))
      enddo
      nCrwKPrefLast=CrwLastMade
      j=IPrefPwd+IZdvihProf
      il=2
      Veta=lPrefPwd
      do i=1,2
        call NToString(i,Veta(5:))
        call FeMakeParEdwCrw(id,tpoma(i+1),xpoma(i+1),il,Veta,nEdw,nCrw)
        call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                     PrefPwd(i,KPhase,KDatBlock),kiPwd(j),.false.)
        if(i.eq.1) then
          nEdwPrefP=nEdw
          nCrwPrefP=nCrw
        endif
        if(KPref(KPhase,KDatBlock).eq.IdPwdPrefNone) then
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
        j=j+1
      enddo
      ilPref=il
      il=il+1
      xpref=tpoma(2)+FeTxLength(StAverage)+5.
      Veta='Direction'
      call FeQuestEdwMake(id,tpoma(2),il,xpref,il,Veta,'L',40.,EdwYd,0)
      nEdwDirPref=EdwLastMade
      il=il+1
      call FeQuestCrwMake(id,tpoma(2),il,xpref,il,StAverage,'L',CrwXd,
     1                    CrwYd,0,0)
      nCrwPrefAve=CrwLastMade
      if(KPref(KPhase,KDatBlock).ne.IdPwdPrefNone) then
        call FeQuestRealAEdwOpen(nEdwDirPref,
     1    DirPref(1,KPhase,KDatBlock),3,.false.,.false.)
        call FeQuestCrwOpen(nCrwPrefAve,SPref(KPhase,KDatBlock).ne.0)
      endif
      il=il+1
      call FeQuestLblMake(id,5.,il,'Absorption correction','L','B')
      if(isTOF) then
        il=il+1
        Veta='Use pseu%do-absorption correction'
        xpomp=5.
        tpom=xpomp+CrwXd+10.
        call FeQuestCrwMake(id,tpom,il,xpomp,il,Veta,'L',CrwXd,CrwgYd,
     1                      1,0)
        nCrwTOFAbsP=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,KUseTOFAbs(KDatBlock).ne.0)
        tpom=tpom+FeTxLengthUnder(Veta)+10.
        Veta='=>'
        call FeQuestLblMake(id,tpom,il,Veta,'L','N')
        nLblTOFAbs=LblLastMade
        xpomp=tpom+FeTxLengthUnder(Veta)+10.
        call FeMakeParEdwCrw(id,tpom,xpomp,il,' ',nEdwTOFAbs,nCrwTOFAbs)
        if(CrwLogicQuest(nCrwTOFAbsP)) then
          j=ITOFAbsPwd+IZdvihRec
          call FeOpenParEdwCrw(nEdwTOFAbs,nCrwTOFAbs,'#keep#',
     1                         TOFAbsPwd(KDatBlock),kiPwd(j),.false.)
        else
          call FeQuestLblOff(nLblTOFAbs)
        endif
        nCrwKAbsorFirst=0
        nCrwKAbsorLast=0
        nEdwMir=0
        il=il+3
      else
        xpom=5.
        tpom=xpom+CrwgXd+10.
        ICrwGroup=ICrwGroup+1
        do i=0,3
          if((PwdMethod(KDatBlock).eq.IdPwdMethodDS.and.
     1        (i.ne.IdPwdAbsorbNone.and.i.ne.IdPwdAbsorbCylinder)).or.
     2       ((PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS.or.
     3         PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS).and.
     4        (i.eq.IdPwdAbsorbCylinder.or.
     5         i.eq.IdPwdAbsorbTransmission))) cycle
          il=il+1
          if(i.eq.0) then
            Veta='N%one'
          else if(i.eq.1) then
            Veta='C%ylindrical sample'
          else if(i.eq.2) then
            Veta='Symmetrical tr%ansmission'
          else if(i.eq.3) then
            Veta='Symmetrical re%flection'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGroup)
          if(i.eq.0) nCrwKAbsorFirst=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,i.eq.KAbsor(KDatBlock))
        enddo
        nCrwKAbsorLast=CrwLastMade
        KAbsorNew=KAbsor(KDatBlock)
        tpom=tpom+FeTxLengthUnder(Veta)+20.
        if(KAbsor(KDatBlock).eq.1) then
          StMir(4:5)='%r'
        else
          StMir(4:5)='%t'
        endif
        xpom=tpom+FeTxLengthUnder(StMir)+20.
        call FeQuestEdwMake(id,tpom,il,xpom,il,StMir,'L',80.,EdwYd,1)
        nEdwMir=EdwLastMade
        if(KAbsor(KDatBlock).ne.IdPwdAbsorbNone) then
          call FeQuestRealEdwOpen(nEdwMir,MirPwd(KDatBlock),.false.,
     1                           .false.)
        else
          MirPwd(KDatBlock)=1.
        endif
        il=il+1
        nCrwTOFAbsP=0
        nCrwTOFAbs =0
        nEdwTOFAbs =0
        nLblTOFAbs= 0
      endif
      if(PwdMethod(KDatBlock).ne.IdPwdMethodDS) then
        call FeQuestLblMake(id,5.,il,
     1                      'Roughness for Bragg-Brentano geometry','L',
     2                      'B')
        nLblRough=LblLastMade
        if(KAbsor(KDatBlock).eq.1) call FeQuestLblOff(nLblRough)
        xpom=5.
        tpom=xpom+CrwgXd+10.
        KRoughOld=KRough(KDatBlock)
        ICrwGroup=ICrwGroup+1
        pom=0.
        do i=1,3
          il=il+1
          if(i.eq.1) then
            Veta='Non%e'
          else if(i.eq.2) then
            Veta='%Pitschke, Hermann & Matter'
          else
            Veta='S%uorti'
          endif
          pom=max(pom,FeTxLengthUnder(Veta))
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGroup)
          if(i.eq.1) nCrwKRoughFirst=CrwLastMade
          if(KAbsor(KDatBlock).ne.1)
     1      call FeQuestCrwOpen(CrwLastMade,i-1.eq.KRough(KDatBlock))
        enddo
        ilp=il
        nCrwKRoughLast=CrwLastMade
        j=IRoughPwd+IZdvihRec
        il=il-3
        Veta=lRoughPwd
        do i=1,2
          il=il+1
          call NToString(i,Veta(6:))
          call FeMakeParEdwCrw(id,tpoma(2),xpoma(2),il,Veta,nEdw,nCrw)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     1                         RoughPwd(i,KDatBlock),kiPwd(j),.false.)
          if(i.eq.1) then
            nEdwRoughP=nEdw
            nCrwRoughP=nCrw
          endif
          if(KRough(KDatBlock).eq.IdPwdRoughNone) then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          j=j+1
        enddo
      else
        nCrwKRoughFirst=0
        nCrwKRoughLast=0
        nEdwRoughP=0
        nCrwRoughP=0
      endif
      if(PwdMethod(KDatBlock).eq.IdPwdMethodUnknown.and..not.isTOF.and.
     1   .not.isED) then
        il=ilp+1
        Veta='Illumination factor:'
        tpom=5.
        call FeQuestLblMake(id,tpom,il,Veta,'L','B')
        nLblIllum=LblLastMade
        if(KAbsor(KDatBlock).eq.1) call FeQuestLblOff(nLblIllum)
        ICrwGroup=ICrwGroup+1
        call FeBoldFont
        xpom=tpom+FeTxLength(Veta)+15.
        call FeNormalFont
        tpom=xpom+CrwgXd+10.
        do i=1,3
          if(i.eq.1) then
            Veta='%1 - none'
          else if(i.eq.2) then
            Veta='%cos(th)'
          else if(i.eq.3) then
            Veta='s%in(th)'
          endif
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,
     1                        1,ICrwGroup)
          if(i.eq.1) nCrwIllumFirst=CrwLastMade
          if(KAbsor(KDatBlock).eq.0.or.
     1       (KAbsor(KDatBlock).eq.2.and.(i.eq.2.or.i.eq.1)).or.
     2       (KAbsor(KDatBlock).eq.3.and.(i.eq.3.or.i.eq.1)))
     3      call FeQuestCrwOpen(CrwLastMade,KIllum(KDatBlock).eq.i-1)
          xpom=xpom+80.
          tpom=tpom+80.
        enddo
        nCrwIllumLast=CrwLastMade
        il=il+1
        xpom=5.
        tpom=xpom+CrwXd+10.
        Veta='Focusing (Bra%gg-Bentano geometry)'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      0)
        nCrwFocus=CrwLastMade
        if(KAbsor(KDatBlock).ne.1)
     1    call FeQuestCrwOpen(CrwLastMade,KFocusBB(KDatBlock).ne.0)
      else
        nCrwIllumFirst=0
        nCrwFocus=0
      endif
      go to 9999
      entry PwdOptionsSampleCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.ge.nCrwKPrefFirst.and.
     1   CheckNumber.le.nCrwKPrefLast) then
        if(KPref(KPhase,KDatBlock).eq.IdPwdPrefNone) then
          do j=1,2
            DirPref(j,KPhase,KDatBlock)=0.
          enddo
          DirPref(3,KPhase,KDatBlock)=1.
          PrefPwd(2,KPhase,KDatBlock)=0.
        endif
        KPref(KPhase,KDatBlock)=CheckNumber-nCrwKPrefFirst
        if(KPref(KPhase,KDatBlock).eq.IdPwdPrefMarchDollase) then
          PrefPwd(1,KPhase,KDatBlock)=1.1
        else
          PrefPwd(1,KPhase,KDatBlock)=0.
        endif
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwKRoughFirst.and.
     2        CheckNumber.le.nCrwKRoughLast) then
        call SetRealArrayTo(RoughPwd(1,KDatBlock),2,0.1)
        KRough(KDatBlock)=CheckNumber-nCrwKRoughFirst
      else if(CheckType.eq.EventCrw.and.
     1        CheckNumber.ge.nCrwKAbsorFirst.and.
     2        CheckNumber.le.nCrwKAbsorLast) then
        KAbsorNew=CheckNumber-nCrwKAbsorFirst
        if((PwdMethod(KDatBlock).eq.IdPwdMethodBBFDS.or.
     1    PwdMethod(KDatBlock).eq.IdPwdMethodBBVDS)) then
          if(KAbsorNew.gt.0) KAbsorNew=KAbsorNew+2
        endif
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwMir) then
        call FeQuestRealFromEdw(nEdwMir,MirPwd(KDatBlock))
        go to 9999
      endif
2500  nEdw=nEdwPrefP
      nCrw=nCrwPrefP
      j=IPrefPwd+IZdvihProf
      do i=1,2
        if(KPref(KPhase,KDatBlock).ne.IdPwdPrefNone) then
          if(KPref(KPhase,KDatBlock).ne.KPrefOld) then
            call FeQuestRealEdwOpen(nEdw,PrefPwd(i,KPhase,KDatBlock),
     1                              .false.,.false.)
            call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
          endif
        else
          call FeQuestEdwClose(nEdw)
          call FeQuestCrwClose(nCrw)
        endif
        nEdw=nEdw+1
        nCrw=nCrw+1
        j=j+1
      enddo
      if(KPref(KPhase,KDatBlock).ne.IdPwdPrefNone) then
        if(KPref(KPhase,KDatBlock).ne.KPrefOld) then
          call FeQuestRealAEdwOpen(nEdwDirPref,
     1      DirPref(1,KPhase,KDatBlock),3,.false.,.false.)
          call FeQuestCrwOpen(nCrwPrefAve,.true.)
        endif
      else
        call FeQuestEdwClose(nEdwDirPref)
        call FeQuestCrwClose(nCrwPrefAve)
      endif
      KPrefOld=KPref(KPhase,KDatBlock)
      if(isTOF) then
        j=ITOFAbsPwd+IZdvihRec
        if(CrwLogicQuest(nCrwTOFAbsP)) then
          KUseTOFAbs(KDatBlock)=1
          call FeQuestLblOn(nLblTOFAbs)
          call FeOpenParEdwCrw(nEdwTOFAbs,nCrwTOFAbs,'#keep#',
     1                         TOFAbsPwd(KDatBlock),kiPwd(j),.false.)
        else
          call FeUpdateParamAndKeys(nEdwTOFAbs,nCrwTOFAbs,
     1                              TOFAbsPwd(KDatBlock),kiPwd(j),1)
          call FeQuestLblOff(nLblTOFAbs)
          call FeQuestEdwClose(nEdwTOFAbs)
          call FeQuestCrwClose(nCrwTOFAbs)
          KUseTOFAbs(KDatBlock)=0
        endif
      else
        if(KAbsorNew.ne.KAbsor(KDatBlock)) then
          if(KAbsorNew.eq.IdPwdAbsorbNone) then
            call FeQuestEdwClose(nEdwMir)
          else
            if(KAbsorNew.eq.IdPwdAbsorbCylinder) then
              StMir(5:5)='r'
            else
              StMir(5:5)='t'
            endif
            call FeQuestEdwLabelChange(nEdwMir,StMir)
            call FeQuestRealEdwOpen(nEdwMir,MirPwd(KDatBlock),.false.,
     1                              .false.)
          endif
          if(PwdMethod(KDatBlock).eq.IdPwdMethodUnknown) then
            if(KAbsorNew.eq.1) then
              if(nLblRough.gt.0) then
                call FeQuestLblOff(nLblRough)
                nCrw=nCrwKRoughFirst
                do i=1,3
                  call FeQuestCrwClose(nCrw)
                  nCrw=nCrw+1
                enddo
              endif
              if(nLblIllum.gt.0) then
                call FeQuestLblOff(nLblIllum)
                nCrw=nCrwIllumFirst
                do i=1,3
                  call FeQuestCrwClose(nCrw)
                  nCrw=nCrw+1
                enddo
              endif
              if(nCrwFocus.gt.0) call FeQuestCrwClose(nCrwFocus)
            else
              if(nLblRough.gt.0) then
                if(CrwStateQuest(nCrwKRoughFirst).eq.CrwClosed) then
                  call FeQuestLblOn(nLblRough)
                  nCrw=nCrwKRoughFirst
                  do i=1,3
                    call FeQuestCrwOpen(nCrw,i-1.eq.KRough(KDatBlock))
                    nCrw=nCrw+1
                  enddo
                endif
              endif
              if(nLblIllum.gt.0) then
                if(CrwStateQuest(nCrwIllumFirst).eq.CrwClosed) then
                  call FeQuestLblOn(nLblIllum)
                  nCrw=nCrwIllumFirst
                  do i=1,3
                    call FeQuestCrwOpen(nCrw,
     1                                  KIllum(KDatBlock).eq.i-1)
                    nCrw=nCrw+1
                  enddo
                endif
              endif
              if(nCrwFocus.gt.0) then
                if(CrwStateQuest(nCrwFocus).eq.CrwClosed)
     1            call FeQuestCrwOpen(nCrwFocus,
     2                                KFocusBB(KDatBlock).ne.0)
              endif
            endif
            if(KAbsorNew.eq.2) then
              if(nCrwIllumLast.gt.0)
     1          call FeQuestCrwClose(nCrwIllumLast)
            else if(KAbsorNew.ne.1) then
              if(nCrwIllumLast.gt.0) then
                if(CrwStateQuest(nCrwIllumLast).eq.CrwClosed) then
                  call FeQuestCrwOpen(nCrwIllumLast,.false.)
                  call FeQuestCrwOpen(nCrwIllumFirst,.true.)
                endif
              endif
            endif
            if(KAbsorNew.eq.3) then
              if(nCrwIllumLast-1.gt.0)
     1          call FeQuestCrwClose(nCrwIllumLast-1)
            else if(KAbsorNew.ne.1) then
              if(nCrwIllumLast-1.gt.0) then
                if(CrwStateQuest(nCrwIllumLast-1).eq.CrwClosed) then
                  call FeQuestCrwOpen(nCrwIllumLast-1,.false.)
                  call FeQuestCrwOpen(nCrwIllumFirst,.true.)
                endif
              endif
            endif
          endif
        endif
        KAbsor(KDatBlock)=KAbsorNew
      endif
      if(nCrwKRoughFirst.ne.0) then
        nEdw=nEdwRoughP
        nCrw=nCrwRoughP
        j=IRoughPwd+IZdvihRec
        do i=1,2
          if(KRough(KDatBlock).gt.0) then
            if(KRough(KDatBlock).ne.KRoughOld) then
              call FeQuestRealEdwOpen(nEdw,RoughPwd(i,KDatBlock),
     1                                .false.,.false.)
              call FeQuestCrwOpen(nCrw,kiPwd(j).ne.0)
            endif
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          j=j+1
        enddo
        KRoughOld=KRough(KDatBlock)
      endif
      entry PwdOptionsSampleUpdate
      if(KPref(KPhase,KDatBlock).ne.IdPwdPrefNone) then
        call FeUpdateParamAndKeys(nEdwPrefP,nCrwPrefP,
     1    PrefPwd(1,KPhase,KDatBlock),kiPwd(IPrefPwd+IZdvihProf),2)
        if(CrwLogicQuest(nCrwPrefAve)) then
          SPref(KPhase,KDatBlock)=1
        else
          SPref(KPhase,KDatBlock)=0
        endif
        call FeQuestRealAFromEdw(nEdwDirPref,
     1                           DirPref(1,KPhase,KDatBlock))
      endif
      if(nCrwKRoughFirst.gt.0)
     1  call FeUpdateParamAndKeys(nEdwRoughP,nCrwRoughP,
     2                            RoughPwd(1,KDatBlock),
     3                            kiPwd(IRoughPwd+IZdvihRec),2)
      if(nCrwIllumFirst.gt.0) then
        nCrw=nCrwIllumFirst
        if(CrwStateQuest(nCrw).ne.CrwClosed) then
          do i=0,2
            if(CrwLogicQuest(nCrw)) then
              KIllum(KDatBlock)=i
              exit
            endif
            nCrw=nCrw+1
          enddo
        else
          KIllum(KDatBlock)=0
        endif
        if(CrwLogicQuest(nCrwFocus)) then
          KFocusBB(KDatBlock)=1
        else
          KFocusBB(KDatBlock)=0
        endif
      endif
      if(isTOF) then
        call FeUpdateParamAndKeys(nEdwTOFAbs,nCrwTOFAbs,
     1                            TOFAbsPwd(KDatBlock),
     2                            kiPwd(ITOFAbsPwd+IZdvihRec),1)
      endif
      if(nCrwKRoughFirst.gt.0) then
        if(KRough(KDatBlock).gt.0) then
          call FeUpdateParamAndKeys(nEdwRoughP,nCrwRoughP,
     1                              RoughPwd(1,KDatBlock),
     2                              kiPwd(IRoughPwd+IZdvihRec),2)
        endif
      endif
9999  return
      end
      subroutine PwdOptionsCorr
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      dimension BackgPwdDef(:)
      character*80 Veta
      integer CrwStateQuest
      logical CrwLogicQuest
      external NToString
      allocatable BackgPwdDef
      save nEdwTerms,nEdwShift,
     1     nCrwShift,nCrwTypeBackg,nCrwUseInvX,nCrwManBackg,nCrwJason,
     2     nButtEditBackg,nButtManBackgImport,nButtSkip,NShift
      save /PwdOptionsC/
      entry PwdOptionsCorrMake(id)
      il=1
      call FeQuestLblMake(id,5.,il,'Background','L','B')
      xpom=5.
      tpom=xpom+CrwgXd+10.
      pom=0.
      do i=1,4
        il=il+1
        if(i.eq.1) then
          Veta='%Legendre polynomials'
        else if(i.eq.2) then
          Veta='%Chebyshev polynomials'
        else if(i.eq.3) then
          Veta='C%os-ortho background'
        else if(i.eq.4) then
          Veta='Co%s-GSAS background'
        endif
        pom=max(pom,FeTxLengthUnder(Veta))
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,0,
     1                      1)
        if(i.eq.1) nCrwTypeBackg=CrwLastMade
        call FeQuestCrwOpen(CrwLastMade,KBackg(KDatBlock).eq.i)
      enddo
      tpom=tpom+pom+40.
      Veta='N%umber of terms'
      xpom=tpom+FeTxLengthUnder(Veta)+CrwXd+10.
      dpom=40.
      il=2
      call FeQuestEudMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,1)
      nEdwTerms=EdwLastMade
      call FeQuestIntEdwOpen(nEdwTerms,NBackg(KDatBlock),.false.)
      call FeQuestEudOpen(nEdwTerms,0,36,1,0.,0.,0.)
      il=il+1
      Veta='Use 1/%x term'
      xpom=tpom+CrwXd+5.
      call FeQuestCrwMake(id,xpom,il,tpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseInvX=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,KUseInvX(KDatBlock).eq.1)
      il=il+1
      Veta='%Edit background'
      xpom=FeTxLengthUnder(Veta)+20.
      call FeQuestButtonMake(id,tpom,il,xpom,ButYd,Veta)
      nButtEditBackg=ButtonLastMade
      if(NBackg(KDatBlock).gt.0)
     1  call FeQuestButtonOpen(nButtEditBackg,ButtonOff)
      if(ExistM90) then
        il=il+2
        Veta='%Import manual background'
        xpom=FeTxLengthUnder(Veta)+10.
        call FeQuestButtonMake(id,5.,il,xpom,ButYd,Veta)
        nButtManBackgImport=ButtonLastMade
        call FeQuestButtonOpen(nButtManBackgImport,ButtonOff)
        call PwdM92Nacti
        Veta='Use %manual background'
        tpom=xcq+5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
        nCrwManBackg=CrwLastMade
        if(NManBackg(KDatBlock).gt.0)
     1    call FeQuestCrwOpen(nCrwManBackg,KManBackg(KDatBlock).ne.0)
      else
        il=il+1
        nButtManBackgImport=0
        nCrwManBackg=0
      endif
      il=il+1
      Veta='%Define excluded regions'
      xpom=FeTxLengthUnder(Veta)+10.
      call FeQuestButtonMake(id,5.,il,xpom,ButYd,Veta)
      nButtSkip=ButtonLastMade
      call FeQuestButtonOpen(nButtSkip,ButtonOff)
      nCrwJason=0
      if(.not.TOFInD) then
        il=il+1
        if(isTOF) then
          Veta='TOF-instrument parameters'
        else
          Veta='Shift parameters'
        endif
        call FeQuestLblMake(id,5.,il,Veta,'L','B')
        if(isTOF) then
          call FeQuestLblMake(id,5.,il,Veta,'L','B')
          if(KUseTOFJason(KDatBlock).le.0) then
            NShift=3
          else
            NShift=7
          endif
          xpom=5.
          il=il+1
          tpom=xpom+CrwXd+10.
          Veta='Use formula developed by Jason %Hodges'
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwJason=CrwLastMade
          call FeQuestCrwOpen(CrwLastMade,KUseTOFJason(KDatBlock).eq.1)
        else
          if(isED) then
            NShift=1
          else
            NShift=3
          endif
        endif
        il=il+1
        j=IShiftPwd+IZdvihRec
        ip=0
        do i=1,7
          ip=ip+1
          Veta=' '
          if(DataType(KdatBlock).eq.2) then
            if(i.le.NShift) Veta=lShiftPwd(i)
          else
            if(KUseTOFJason(KDatBlock).le.0) then
              if(i.le.NShift) Veta=lTOF1Pwd(i)
            else
              if(i.le.NShift) Veta=lTOF2Pwd(i)
            endif
          endif
          if(i.eq.4.or.i.eq.6) then
            ip=1
            il=il+1
          endif
          call FeMakeParEdwCrw(id,tpoma(ip),xpoma(ip),il,Veta,nEdw,nCrw)
          if(i.le.NShift)
     1      call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',
     2                           ShiftPwd(i,KDatBlock),kiPwd(j),.false.)
          if(i.eq.1) then
            nEdwShift=nEdw
            nCrwShift=nCrw
          endif
          j=j+1
        enddo
      else
        nEdwShift=0
        nCrwShift=0
      endif
      go to 9999
      entry PwdOptionsCorrCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseInvX) then
        if(CrwLogicQuest(nCrwUseInvX)) then
          k=IBackgPwd+(KDatBlock-1)*NParRecPwd+NBackg(KDatBlock)-1
          kiPwd(KDatBlock)=0
          do i=NBackg(KDatBlock),1,-1
            BackgPwd(i+1,KDatBlock)=BackgPwd(i,KDatBlock)
            KiPwd(k+1)=kiPwd(k)
            k=k-1
          enddo
          kiPwd(k+1)=0
          NBackg(KDatBlock)=NBackg(KDatBlock)+1
          BackgPwd(1,KDatBlock)=0.
          KUseInvX(KDatBlock)=1
        else
          k=IBackgPwd+(KDatBlock-1)*NParRecPwd+1
          do i=2,NBackg(KDatBlock)
            BackgPwd(i-1,KDatBlock)=BackgPwd(i,KDatBlock)
            KiPwd(k-1)=kiPwd(k)
            k=k+1
          enddo
          kiPwd(k-1)=0
          NBackg(KDatBlock)=NBackg(KDatBlock)-1
          KUseInvX(KDatBlock)=0
        endif
        call FeQuestIntEdwOpen(nEdwTerms,NBackg(KDatBlock),.false.)
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwJason) then
        NShiftOld=NShift
        if(CrwLogicQuest(nCrwJason)) then
          NShift=7
          KUseTOFJason(KDatBlock)=1
        else
          NShift=3
          KUseTOFJason(KDatBlock)=0
        endif
        call FeUpdateParamAndKeys(nEdwShift,nCrwShift,
     1    ShiftPwd(1,KDatBlock),kiPwd(IShiftPwd+IZdvihRec),
     2    NShiftOld)
        nEdw=nEdwShift
        nCrw=nCrwShift
        do i=1,max(NShift,NShiftOld)
          if(i.le.NShift) then
            if(KUseTOFJason(KDatBlock).le.0) then
              Veta=lTOF1Pwd(i)
            else
              Veta=lTOF2Pwd(i)
            endif
            call FeOpenParEdwCrw(nEdw,nCrw,Veta,
     1                           ShiftPwd(i,KDatBlock),kiPwd(j),
     2                           .false.)
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
        enddo
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtEditBackg) then
        allocate(BackgPwdDef(NBackg(KDatBlock)))
        call SetRealArrayTo(BackgPwdDef,NBackg(KDatBlock),0.)
        call FeEditParameterField(BackgPwd(1,KDatBlock),
     1      BackgPwdS(1,KDatBlock),BackgPwdDef,
     2      kiPwd(IBackgPwd+(KDatBlock-1)*NParRecPwd),
     3      NBackg(KDatBlock),lBackgPwd,xqdp,'Edit background',
     4      tpoma,xpoma,NToString)
        deallocate(BackgPwdDef)
      else if(CheckType.eq.EventButton.and.
     1        CheckNumber.eq.nButtManBackgImport) then
        call PwdImportManBackg
      else if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtSkip)
     1  then
        call PwdSkipFrTo
      else if(CheckType.eq.EventEdw.and.CheckNumber.eq.nEdwTerms) then
        call FeQuestIntFromEdw(nEdwTerms,NBackgNew)
        i1=min(NBackg(KDatBlock),NBackgNew)+1
        i2=max(NBackg(KDatBlock),NBackgNew)
        j=IBackgPwd+i1-1
        do i=i1,i2
          kiPwd(j)=0
          BackgPwd(i,KDatBlock)=0.
          j=j+1
        enddo
        NBackg(KDatBlock)=NBackgNew
        if(NBackg(KDatBlock).gt.0) then
          call FeQuestButtonOpen(nButtEditBackg,ButtonOff)
        else
          call FeQuestButtonClose(nButtEditBackg)
        endif
        go to 9999
      endif
2500  if(NManBackg(KDatBlock).gt.0) then
        if(CrwStateQuest(nCrwManBackg).eq.CrwClosed) then
          KManBackg(KDatBlock)=1
          call FeQuestCrwOpen(nCrwManBackg,.true.)
        endif
      else
        call FeQuestCrwClose(nCrwManBackg)
      endif
      go to 9999
      entry PwdOptionsCorrUpdate
      nCrw=nCrwTypeBackg
      do i=1,4
        if(CrwLogicQuest(nCrw)) then
          KBackg(KDatBlock)=i
          exit
        endif
        nCrw=nCrw+1
      enddo
      call FeUpdateParamAndKeys(nEdwShift,nCrwShift,
     1  ShiftPwd(1,KDatBlock),kiPwd(IShiftPwd+IZdvihRec),NShift)
      if(CrwLogicQuest(nCrwManBackg)) then
        KManBackg(KDatBlock)=1
      else
        KManBackg(KDatBlock)=0
      endif
9999  return
      end
      subroutine PwdOptionsVarious
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      common/PwdOptionsC/ IZdvihRec,IZdvihCell,IZdvihProf,tpoma(3),
     1                    xpoma(3),AsymOrg(8,2),KiAsymOrg(8,2),xqdp,xcq,
     2                    KartIdCell,KartIdProf,KartIdAsym,
     3                    KartIdRadiation,KartIdSample,KartIdCorr,
     4                    KartIdVarious,
     5                    nCrwRefLam,nEdwLam,nCrwLam,nCrwUseLamFile,
     6                    nRolMenuLamFile,nEdwLorentz,nCrwLorentz,
     7                    nEdwGauss,nCrwGauss,nCrwAniPBroad,nCrwStrain,
     8                    nLblStrain,nButEditTensor,nEdwDirBroad,
     9                    nEdwAsymP,nCrwAsymP,
     a                    LastAsymProfile,nCrwAsymFr,nCrwAsymTo,
     1                    nRolMenuPhase,KeyMenuPhaseUse,
     2                    MenuPhaseUse(MxPhases)
      character*80 Veta
      integer RolMenuStateQuest,EdwStateQuest,RolMenuSelectedQuest
      logical :: DefInD = .true.,CrwLogicQuest
      save nEdwCutoffMin,nEdwCutoffMax,
     1     nCrwWleBail,nCrwKleBail,
     2     nCrwUseCutoff,nCrwInD,nCrwInSinThL
      save /PwdOptionsC/
      entry PwdOptionsVariousMake(id)
      xpom=5.
      tpom=xpom+CrwXd+3.
      il=1
      Veta='%Apply weights in leBail decomposition'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwWleBail=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,KWleBail(KDatBlock).eq.1)
      il=il+1
      Veta='%Use structure of known phases in leBail decomposition'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,0)
      nCrwKleBail=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,KKleBail(KDatBlock).eq.1)
      if(.not.isTOF) then
        il=il+1
        Veta='Allow refinement of the %wavelength'
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
        nCrwRefLam=CrwLastMade
        if(KAsym(KDatBlock).ne.IdPwdAsymFundamental)
     1    call FeQuestCrwOpen(CrwLastMade,KRefLam(KDatBlock).eq.1)
        il=il+1
        Veta=lLamPwd
        idl=idel(Veta)+1
        j=ILamPwd+IZdvihRec
        do i=1,NAlfa(KDatBlock)
          write(Veta(idl:),'(i1)') i
          call FeMakeParEdwCrw(id,tpoma(i),xpoma(i),il,Veta,nEdw,nCrw)
          call FeOpenParEdwCrw(nEdw,nCrw,'#keep#',LamPwd(1,KDatBlock),
     1                         kiPwd(j),.false.)
          if(i.eq.1) then
            nEdwLam=nEdw
            nCrwLam=nCrw
          endif
          if(KRefLam(KDatBlock).ne.1.or.
     1       (.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental))
     2      then
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          j=j+1
        enddo
        if(nLamFiles.gt.0) then
          il=il-1
          Veta='Use predefined %radiation profile'
          call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,
     1                        0)
          nCrwUseLamFile=CrwLastMade
          il=il+1
          Veta='%File with radiation profile'
          tpom=5.
          xpom=tpom+FeTxLengthUnder(Veta)+10.
          dpom=120.
          call FeQuestRolMenuMake(id,tpom,il,xpom,il,Veta,'L',dpom,
     1                            EdwYd,0)
          nRolMenuLamFile=RolMenuLastMade
          if(KAsym(KDatBlock).eq.IdPwdAsymFundamental)
     1      then
            call FeQuestCrwOpen(CrwLastMade,KUseLamFile(KDatBlock).eq.1)
            if(KUseLamFile(KDatBlock).eq.1) then
              i=LocateInStringArray(LamNames,nLamFiles,
     1                              LamFile(KDatBlock),.true.)
              i=max(i,1)
              call FeQuestRolMenuOpen(nRolMenuLamFile,LamNames,
     1                                nLamFiles,i)
            endif
          endif
        else
          nCrwUseLamFile=0
          nRolMenuLamFile=0
        endif
        xpom=5.
        tpom=xpom+CrwXd+3.
      else
        nCrwRefLam=0
        nEdwLam=0
        nCrwLam=0
        nCrwUseLamFile=0
        nRolMenuLamFile=0
      endif
      il=il+1
      Veta='Use global %cutoff'
      call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      nCrwUseCutoff=CrwLastMade
      call FeQuestCrwOpen(CrwLastMade,UseCutOffPwd)
      Veta='Defined in %d'
      xpom=xpom+10.
      tpom=tpom+10.
      do i=1,2
        il=il+1
        call FeQuestCrwMake(id,tpom,il,xpom,il,Veta,'L',CrwgXd,CrwgYd,1,
     1                      1)
        if(i.eq.1) then
          nCrwInD=CrwLastMade
          Veta='Defined in %sin(th)/lam'
        else
          nCrwInSinThL=CrwLastMade
        endif
        if(UseCutOffPwd)
     1    call FeQuestCrwOpen(CrwLastMade,i.eq.1.eqv.DefInD)
      enddo
      Veta='Cutoff(m%in)'
      tpom=150.
      xpom=tpom+FeTxLengthUnder(Veta)+10.
      dpom=80.
      il=il-2
      do i=1,2
        il=il+1
        call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
        if(i.eq.1) then
          nEdwCutoffMin=EdwLastMade
          Veta='Cutoff(ma%x)'
          if(DefInD) then
            pom=CutOffMinPwd
          else
            if(CutOffMaxPwd.eq.0.) then
              pom=50.
            else
              pom=.5/CutOffMaxPwd
            endif
          endif
        else
          nEdwCutoffMax=EdwLastMade
          if(DefInD) then
            pom=CutOffMaxPwd
          else
            if(CutOffMinPwd.eq.0.) then
              pom=50.
            else
              pom=.5/CutOffMinPwd
            endif
          endif
        endif
        if(UseCutOffPwd)
     1    call FeQuestRealEdwOpen(EdwLastMade,pom,.false.,.false.)
      enddo
      go to 9999
      entry PwdOptionsVariousCheck
      if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwRefLam) then
        if(CrwLogicQuest(nCrwRefLam)) then
          KRefLam(KDatBlock)=1
        else
          KRefLam(KDatBlock)=0
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwUseLamFile)
     1  then
        if(CrwLogicQuest(nCrwUseLamFile)) then
          KUseLamFile(KDatBlock)=1
          call PwdGetLamFiles
        else
          KUseLamFile(KDatBlock)=0
        endif
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwInD.or.
     1        CheckNumber.eq.nCrwInSinThL) then
        call FeQuestRealFromEdw(nEdwCutoffMin,pom)
        if(pom.eq.0.) then
          CutOffMaxPwd=50.
        else
          CutOffMaxPwd=.5/pom
        endif
        call FeQuestRealFromEdw(nEdwCutoffMax,pom)
        if(pom.eq.0.) then
          CutOffMinPwd=50.
        else
          CutOffMinPwd=.5/pom
        endif
        call FeQuestRealEdwOpen(nEdwCutoffMin,CutOffMinPwd,.false.,
     1                          .false.)
        call FeQuestRealEdwOpen(nEdwCutoffMax,CutOffMaxPwd,.false.,
     1                          .false.)
        DefInD=CrwLogicQuest(nCrwInD)
      endif
2500  if(.not.isTOF.and.KAsym(KDatBlock).eq.IdPwdAsymFundamental) then
        if(KUseLamFile(KDatBlock).eq.1) then
          i=LocateInStringArray(LamNames,nLamFiles,
     1                          LamFile(KDatBlock),.true.)
          i=max(i,1)
          call FeQuestRolMenuOpen(nRolMenuLamFile,LamNames,
     1                            nLamFiles,i)
        else
          if(RolMenuStateQuest(nRolMenuLamFile).eq.RolMenuOpened) then
            i=RolMenuSelectedQuest(nRolMenuLamFile)
            LamFile(KDatBlock)=LamNames(i)
          endif
          call FeQuestRolMenuClose(nRolMenuLamFile)
        endif
      else
        nEdw=nEdwLam
        nCrw=nCrwLam
        j=ILamPwd+IZdvihRec
        do i=1,NAlfa(KDatBlock)
          if(KRefLam(KDatBlock).eq.1) then
            call FeQuestRealEdwOpen(nEdw,LamPwd(i,KDatBlock),.false.,
     1                              .false.)
            call FeQuestCrwOpen(nCrw,kipwd(j).eq.1)
          else
            call FeQuestEdwClose(nEdw)
            call FeQuestCrwClose(nCrw)
          endif
          nEdw=nEdw+1
          nCrw=nCrw+1
          j=j+1
        enddo
      endif
      if(CrwLogicQuest(nCrwUseCutoff)) then
        if(EdwStateQuest(nEdwCutoffMin).ne.EdwOpened) then
          call FeQuestCrwOpen(nCrwInD,DefInD)
          call FeQuestCrwOpen(nCrwInSinThL,.not.DefInD)
          call FeQuestRealEdwOpen(nEdwCutoffMin,CutOffMinPwd,.false.,
     1                            .false.)
          call FeQuestRealEdwOpen(nEdwCutoffMax,CutOffMaxPwd,.false.,
     1                            .false.)
        endif
      else
        call FeQuestCrwClose(nCrwInD)
        call FeQuestCrwClose(nCrwInSinThL)
        call FeQuestEdwClose(nEdwCutoffMin)
        call FeQuestEdwClose(nEdwCutoffMax)
      endif
      go to 9999
      entry PwdOptionsVariousUpdate
      if(KRefLam(KDatBlock).eq.1) then
        j=ILamPwd+IZdvihRec
        call FeUpdateParamAndKeys(nEdwLam,nCrwLam,LamPwd(1,KDatBlock),
     1                            kiPwd(j),2)
      endif
      if(CrwLogicQuest(nCrwWleBail)) then
        KWleBail(KDatBlock)=1
      else
        KWleBail(KDatBlock)=0
      endif
      if(CrwLogicQuest(nCrwKleBail)) then
        KKleBail(KDatBlock)=1
      else
        KKleBail(KDatBlock)=0
      endif
      if(KUseLamFile(KDatBlock).eq.1) then
        i=RolMenuSelectedQuest(nRolMenuLamFile)
        LamFile(KDatBlock)=LamNames(i)
      endif
      UseCutOffPwd=CrwLogicQuest(nCrwUseCutoff)
      if(UseCutOffPwd) then
        call FeQuestRealFromEdw(nEdwCutoffMin,pom)
        if(DefInD) then
          CutOffMinPwd=pom
        else
          if(pom.eq.0.) then
            CutOffMaxPwd=0.
          else
            CutOffMaxPwd=.5/pom
          endif
        endif
        call FeQuestRealFromEdw(nEdwCutoffMax,pom)
        if(DefInD) then
          CutOffMaxPwd=pom
        else
          if(pom.eq.0.) then
            CutOffMinPwd=0.
          else
            CutOffMinPwd=.5/pom
          endif
        endif
      endif
9999  return
      end
      subroutine PwdGetLamFiles
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*80  Veta
      character*258 ListDir,ListFile,CurrentDirO
      integer FeChDir
      CurrentDirO=CurrentDir
      if(OpSystem.le.0) then
        Veta=HomeDir(:idel(HomeDir))//'lam'//ObrLom
      else
        Veta=HomeDir(:idel(HomeDir))//'lam/'
      endif
      i=FeChdir(Veta)
      call FeGetCurrentDir()
      ListFile='jfile'
      ListDir='jdir'
      call CreateTmpFile(ListDir,i,0)
      call CreateTmpFile(ListFile,i,0)
      call FeTmpFilesAdd(ListDir)
      call FeTmpFilesAdd(ListFile)
      i=KLam(KDatBlock)
      if(i.gt.0) then
        Veta=LamTypeD(i)(:idel(LamTypeD(i)))//'*.lam'
      else
        Veta='*.lam'
      endif
      call FeDir(ListDir,ListFile,Veta,0)
      call DeleteFile(ListDir)
      call FeTmpFilesClear(ListDir)
      ln=NextLogicNumber()
      open(ln,file=ListFile)
      nLamFiles=0
1120  read(ln,FormA,end=1130) Veta
      if(nLamFiles.lt.20) then
        nLamFiles=nLamFiles+1
        LamNames(nLamFiles)=Veta
        go to 1120
      endif
1130  close(ln)
      call DeleteFile(ListFile)
      call FeTmpFilesClear(ListFile)
      i=FeChdir(CurrentDirO)
      call FeGetCurrentDir
      return
      end
      subroutine PwdMakeIRFFile
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      character*256 Veta
      logical ExistFile,FeYesNoHeader
      ln=0
      Veta=' '
1100  call FeFileManager('Choose out output "irf file',Veta,'*.irf',0,
     1                   .true.,ich)
      if(ich.ne.0) go to 9999
      if(Veta.eq.' ') then
        call FeChybne(-1.,YBottomMessage,'The string is empty, try '//
     1                'again.',' ',SeriousError)
        go to 1100
      else if(ExistFile(Veta)) then
        call FeCutName(Veta,TextInfo(1),len(TextInfo(1))-35,
     1                 CutTextFromLeft)
        NInfo=1
        TextInfo(1)='The file "'//TextInfo(1)(:idel(TextInfo(1)))//
     1              '" already exists'
        if(.not.FeYesNoHeader(-1.,YBottomMessage,
     1                        'Do you want to rewrite it?',1))
     2    go to 1100
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,Veta,'formatted','unknown')
      if(KUseTOFJason(KDatBlock).eq.1) then
        Veta='! To be used with function NPROF=13 in FullProf  (Res=5)'
      else
        Veta='! To be used with function NPROF=9 in FullProf  (Res=5)'
      endif
      write(ln,FormA) Veta(:idel(Veta))
      do i=1,NDatBlock
        KDatBlock=i
        call PwdM92Nacti
        write(Veta,'(''! '',52(''-''),'' Bank'')')
        write(Cislo,FormI15) i
        call Zhusti(Cislo)
        Veta(idel(Veta)+2:)=Cislo
        write(ln,FormA) Veta(:idel(Veta))
        Veta='!       Tof-min(us)    step      Tof-max(us)'
        write(ln,FormA) Veta(:idel(Veta))
        tmin=XPwd(1)
        tmax=XPwd(Npnts)
        tstep=(XPwd(2)-XPwd(1))*1000.
        tmin=tmin*1000.
        tmax=tmax*1000.
        write(Veta,'(3f12.4)') tmin,tstep,tmax
        write(ln,FormA) 'TOFRG'//Veta(:idel(Veta))
        Veta='!          Zero    Dtt1'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,100)(ShiftPwd(j,i),j=4,5)
        write(ln,FormA) 'ZD2TOF'//Veta(:idel(Veta))
        Veta='!          Zerot   Dtt1t         Dtt2t    x-cross '//
     1       'Width'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,100)(ShiftPwd(j,i),j=1,3),
     1                  ShiftPwd(7,i),ShiftPwd(6,i)
        write(ln,FormA) 'ZD2TOT'//Veta(:idel(Veta))
        Veta='!     TOF-TWOTH of the bank'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(f10.2)') TOFTTh(i)
        write(ln,FormA) 'TWOTH'//Veta(:idel(Veta))
        Veta='!           Sig-2       Sig-1       Sig-0'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(3f12.3)')(GaussPwd(4-j,1,i),j=1,3)
        write(ln,FormA) 'SIGMA'//Veta(:idel(Veta))
        Veta='!           Gam-2       Gam-1       Gam-0'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(3f12.3)')(LorentzPwd(4-j,1,i),j=1,3)
        write(ln,FormA) 'GAMMA'//Veta(:idel(Veta))
        Veta='!         alph0       beta0       alph1       beta1'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(4f12.6)') AsymPwd(1,i),AsymPwd(3,i),AsymPwd(2,i),
     1                         AsymPwd(4,i)
        write(ln,FormA) 'ALFBE'//Veta(:idel(Veta))
        Veta='!         alph0t      beta0t      alph1t      beta1t'
        write(ln,FormA) Veta(:idel(Veta))
        write(Veta,'(4f12.6)') AsymPwd(5,i),AsymPwd(7,i),AsymPwd(6,i),
     1                         AsymPwd(8,i)
        write(ln,FormA) 'ALFBT'//Veta(:idel(Veta))
        write(ln,'(''END'')')
      enddo
9999  call CloseIfOpened(ln)
      return
100   format(f10.2,f13.4,f11.4,2f8.4)
      end
      subroutine PwdResetVoigt
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      do KDatB=1,NDatBlock
        if(DataType(KDatB).eq.-2.or.
     1    KAsym(KDatB).eq.IdPwdAsymFundamental) cycle
        do KPh=1,NPhase
         if(KProfPwd(KPh,KDatB).eq.IdPwdProfLorentz.or.
     1      KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
           Znak=0.
           do i=1,3,2
             if(LorentzPwd(i,KPh,KDatB).ne.0.)
     1         Znak=sign(1.,LorentzPwd(i,KPh,KDatB))
           enddo
           do i=1,5
             LorentzPwd(i,KPh,KDatB)=Znak*LorentzPwd(i,KPh,KDatB)
           enddo
         endif
         if(KProfPwd(KPh,KDatB).eq.IdPwdProfGauss.or.
     1      KProfPwd(KPh,KDatB).eq.IdPwdProfVoigt) then
           Znak=0.
           do i=1,4
             if(GaussPwd(i,KPh,KDatB).ne.0.)
     1         Znak=sign(1.,GaussPwd(i,KPh,KDatB))
           enddo
           do i=1,4
             GaussPwd(i,KPh,KDatB)=Znak*GaussPwd(i,KPh,KDatB)
           enddo
         endif
        enddo
      enddo
      return
      end
      subroutine PwdWilliamsonHall
      use Powder_mod
      use RefPowder_mod
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer :: ih(6),RolMenuSelectedQuest,KProfPwdUse=3,
     1           DrawFunction = 1
      logical :: EqIgCase,CrwLogicQuest,UseCalibration=.false.
      real h3(3),xo(3),xpp(3),LorentzPom(4),GaussPom(4)
      real, allocatable :: xx(:),yy(:)
      character*256 :: Veta,EdwStringQuest,FileNormal=' '
      character*12 :: Jmena(4) = (/'%Quit   ',
     1                             '%Save   ',
     2                             '%Print  ',
     3                             '%Options'/)
      double precision suma
      KPhaseIn=KPhase
      KPhaseAct=KPhase
      id=NextQuestId()
      call FeQuestAbsCreate(id,0.,0.,XMaxBasWin,YMaxBasWin,' ',0,0,
     1                      -1,-1)
      XStripWidth=100.
      call FeMakeGrWin(2.,XStripWidth,YBottomMargin,2.)
      call FeFillRectangle(XMaxBasWin-XStripWidth,XmaxBasWin,YMinGrWin,
     1                     YMaxGrWin,4,0,0,LightGray)
      call FeMakeGrWin(0.,100.,YBottomMargin,24.)
      call FeMakeAcWin(120.,20.,30.,30.)
      call FeBottomInfo('#prazdno#')
      wpom=80.
      xpom=(XMaxBasWin+XMaxGrWin-wpom)*.5
      dpom=ButYd+8.
      ypom=YMaxGrWin-dpom
      do i=1,4
        call FeQuestAbsButtonMake(id,xpom,ypom,wpom,ButYd,Jmena(i))
        j=ButtonOff
        if(i.eq.1) then
          nButtQuit=ButtonLastMade
        else if(i.eq.2) then
          nButtSave=ButtonLastMade
        else if(i.eq.3) then
          nButtPrint=ButtonLastMade
        else if(i.eq.4) then
          nButtOptions=ButtonLastMade
        endif
        call FeQuestButtonOpen(ButtonLastMade,j)
        if(i.eq.3.or.i.eq.4) then
          ypom=ypom-8.
          call FeTwoPixLineHoriz(XMaxGrWin+1.,XMaxBasWin,ypom,Gray,
     1                           White)
        endif
        ypom=ypom-dpom
      enddo
1100  call iom41(0,0,fln(:ifln)//'.m41')
      idp=NextQuestId()
      xqd=450.
      Veta='Options'
      if(NPhase.le.1) then
        il=10
      else
        il=12
      endif
      call FeQuestCreate(idp,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      Veta='%Use calibration data'
      xpom=5.
      tpom=xpom+CrwXd+5.
      call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,1,0)
      call FeQuestCrwOpen(CrwLastMade,UseCalibration)
      nCrwCalibration=CrwLastMade
      il=il+1
      Veta='%Calibration file:'
      bpom=50.
      tpom=5.
      xpom=tpom+FeTxLengthUnder(Veta)+5.
      dpom=xqd-bpom-xpom-20.
      call FeQuestEdwMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
      nEdwFile=EdwLastMade
      call FeQuestStringEdwOpen(EdwLastMade,FileNormal)
      xpom=xpom+dpom+10.
      Veta='%Browse'
      call FeQuestButtonMake(idp,xpom,il,bpom,ButYd,Veta)
      nButtBrowse=ButtonLastMade
      call FeQuestButtonOpen(ButtonLastMade,ButtonOff)
      il=il+1
      call FeQuestLinkaMake(idp,il)
      xpom=5.
      tpom=xpom+CrwgXd+5.
      Veta='Li%near curve - Lorentzian dominating case'
      do i=1,3
        il=il+1
        call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,
     1                      1)
        if(i.eq.1) then
          nCrwUseLinear=CrwLastMade
          Veta='%Quadratic curve - Gaussian dominating case'
        else if(i.eq.2) then
          nCrwQuadratic=CrwLastMade
          Veta='%Mixing parameter'
        else if(i.eq.3) then
          nCrwMixParameter=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.DrawFunction)
      enddo
      call FeReleaseOutput
      il=il+1
      call FeQuestLinkaMake(idp,il)
      Veta='Draw %Gaussian component'
      do i=1,3
        il=il+1
        call FeQuestCrwMake(idp,tpom,il,xpom,il,Veta,'L',CrwXd,CrwYd,0,
     1                      2)
        if(i.eq.1) then
          nCrwGauss=CrwLastMade
          Veta='Draw %Lorentzian component'
        else if(i.eq.2) then
          nCrwLorentzian=CrwLastMade
          Veta='Draw pseudo-%Voigt'
        else if(i.eq.3) then
          nCrwVoigt=CrwLastMade
        endif
        call FeQuestCrwOpen(CrwLastMade,i.eq.KProfPwdUse)
      enddo
      if(NPhase.gt.1) then
        il=il+1
        call FeQuestLinkaMake(idp,il)
        dpom=0.
        do i=1,NPhase
          dpom=max(dpom,FeTxLength(PhaseName(i)))
        enddo
        dpom=dpom+2.*EdwMarginSize+EdwYd
        Veta='%Select phase:'
        tpom=5.
        xpom=tpom+FeTxLengthUnder(Veta)+5.
        il=il+1
        call FeQuestRolMenuMake(idp,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,
     1                          1)
        nRolMenuPhase=RolMenuLastMade
        call FeQuestRolMenuOpen(RolMenuLastMade,PhaseName,NPhase,
     1                          KPhaseAct)
      else
        nRolMenuPhase=0
      endif
1130  if(UseCalibration) then
        call FeQuestStringEdwOpen(nEdwFile,FileNormal)
        call FeQuestButtonOpen(nButtBrowse,ButtonOff)
      else
        call FeQuestEdwDisable(nEdwFile)
        call FeQuestButtonDisable(nButtBrowse)
      endif
1140  if(KProfPwd(KPhaseAct,KDatBlock).eq.IdPwdProfVoigt) then
        nCrw=nCrwGauss
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.KProfPwdUse)
          nCrw=nCrw+1
        enddo
!        if(CrwLogicQuest(nCrwMixParameter)) then
!          nCrw=nCrwUseLinear
!          DrawFunction=1
!          do i=1,3
!            call FeQuestCrwOpen(nCrw,i.eq.DrawFunction)
!            nCrw=nCrw+1
!          enddo
!        endif
      else
        nCrw=nCrwGauss
        do i=1,3
          call FeQuestCrwDisable(nCrw)
          nCrw=nCrw+1
        enddo
        nCrw=nCrwUseLinear
        DrawFunction=1
        do i=1,3
          call FeQuestCrwOpen(nCrw,i.eq.DrawFunction)
          nCrw=nCrw+1
        enddo
        call FeQuestCrwDisable(nCrwMixParameter)
      endif
1150  call FeQuestEvent(idp,ichp)
      if(CheckType.eq.EventButton.and.CheckNumber.eq.nButtBrowse) then
        Veta=EdwStringQuest(nEdwFile)
        call FeFileManager('Define the calibration structure',
     1                     Veta,' ',1,.true.,ichp)
        if(ichp.eq.0) call FeQuestStringEdwOpen(nEdwFile,Veta)
        go to 1150
      else if(CheckType.eq.EventCrw.and.CheckNumber.eq.nCrwCalibration)
     1  then
        UseCalibration=CrwLogicQuest(nCrwCalibration)
        go to 1130
      else if(CheckType.eq.EventRolMenu.and.
     1        CheckNumber.eq.nRolMenuPhase) then
        KPhaseAct=RolMenuSelectedQuest(nRolMenuPhase)
        go to 1140
      else if(CheckType.ne.0) then
        call NebylOsetren
        go to 1150
      endif
      if(ichp.eq.0) then
        if(UseCalibration) then
          FileNormal=EdwStringQuest(nEdwFile)
        else
          FileNormal=' '
        endif
        UseCalibration=FileNormal.ne.' '
        nCrw=nCrwUseLinear
        do i=1,3
          if(CrwLogicQuest(nCrw)) then
            DrawFunction=i
            exit
          endif
          nCrw=nCrw+1
        enddo
        if(KProfPwd(KPhaseAct,KDatBlock).eq.IdPwdProfVoigt) then
          nCrw=nCrwGauss
          do i=1,3
            if(CrwLogicQuest(nCrw)) then
              if(i.eq.1) then
                KProfPwdUse=IdPwdProfGauss
              else if(i.eq.2) then
                KProfPwdUse=IdPwdProfLorentz
              else
                KProfPwdUse=IdPwdProfVoigt
              endif
              exit
            endif
            nCrw=nCrw+1
          enddo
        endif
      endif
      call FeQuestRemove(idp)
      NPh=NPhase
      KDatB=KDatBlock
      if(UseCalibration) then
        call iom50(0,0,FileNormal(:idel(FileNormal))//'.m50')
        call iom41(0,0,FileNormal(:idel(FileNormal))//'.m41')
        KStrain(NPh+1,KDatB)=IdPwdStrainNone
        KProfPwd(NPh+1,KDatB)=KProfPwd(1,1)
        do i=1,4
          LorentzPom(i)=LorentzPwd(i,1,1)
        enddo
        do i=1,4
          GaussPom(i)=GaussPwd(i,1,1)
        enddo
        KProfPom=KProfPwd(1,1)
      endif
      call iom50(0,0,fln(:ifln)//'.m50')
      call iom40(0,0,fln(:ifln)//'.m40')
      if(UseCalibration) then
        do i=1,4
          LorentzPwd(i,NPh+1,KDatB)=LorentzPom(i)
        enddo
        do i=1,4
          GaussPwd(i,NPh+1,KDatB)=GaussPom(i)
        enddo
        KProfPwd(NPh+1,KDatB)=KProfPom
      endif
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:idel(fln))//'.prf','formatted','unknown')
      if(ErrFlag.ne.0) go to 9999
      KdeJe=0
1210  if(NDatBlock.gt.1) then
1220    read(ln,FormA,end=1230,err=9000) Veta
        k=0
        call Kus(Veta,k,Cislo)
        if(EqIgCase(Cislo,DatBlockName(KDatBlock))) then
          call Kus(Veta,k,Cislo)
          if(EqIgCase(Cislo,'begin')) then
            if(KdeJe.eq.0) then
              go to 1250
            else
              go to 1280
            endif
          endif
        endif
        go to 1220
1230    Veta='Datablock "'//
     1        DatBlockName(KDatBlock)(:idel(DatBlockName(KDatBlock)))//
     2       '" has not been found, run le Bail or Rietveld refinement '
     3     //'and try once more'
        call FeChybne(-1.,-1.,Veta,' ',SeriousError)
        go to 9999
      endif
1250  read(ln,'(2i5)',end=9000,err=9000) kType,kAlpha2
      if(KdeJe.gt.0) go to 1280
      MxRefPwd=0
1260  read(ln,PrfFormat,err=9000) ih(1:maxNDim)
      if(ih(1).gt.900) go to 1270
      MxRefPwd=MxRefPwd+1
      go to 1260
1270  m=NPhase*NParCellProfPwd+NDatBlock*NParProfPwd
      l=NAlfa(KDatBlock)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,IBroadHKLa)
      if(allocated(AxDivProfA))
     1  deallocate(AxDivProfA,DerAxDivProfA,FDSProf)
      if(allocated(xx)) deallocate(xx,yy)
      allocate(ihPwdArr(maxNDim,MxRefPwd),FCalcPwdArr(MxRefPwd),
     1         MultPwdArr(MxRefPwd),iqPwdArr(MxRefPwd),
     2         KPhPwdArr(MxRefPwd),ypeaka(l,MxRefPwd),peaka(l,MxRefPwd),
     3         pcota(l,MxRefPwd),rdega(2,l,MxRefPwd),shifta(l,MxRefPwd),
     4         fnorma(l,MxRefPwd),tntsima(l,MxRefPwd),
     5         ntsima(l,MxRefPwd),sqsga(l,MxRefPwd),fwhma(l,MxRefPwd),
     6         sigpa(l,MxRefPwd),etaPwda(l,MxRefPwd),
     7         dedffga(l,MxRefPwd),dedffla(l,MxRefPwd),
     8         dfwdga(l,MxRefPwd),dfwdla(l,MxRefPwd),
     9         coef(m,l,MxRefPwd),coefp(m,l,MxRefPwd),
     a         coefq(2,l,MxRefPwd),Prof0(MxRefPwd),cotg2tha(l,MxRefPwd),
     1         cotgtha(l,MxRefPwd),cos2tha(l,MxRefPwd),
     2         cos2thqa(l,MxRefPwd),Alpha12a(l,MxRefPwd),
     3         Beta12a(l,MxRefPwd),xx(MxRefPwd),yy(MxRefPwd),
     4         IBroadHKLa(MxRefPwd))
      rewind ln
      KdeJe=1
      go to 1210
1280  n=0
1300  read(ln,PrfFormat,err=9000,end=1400) ih(1:maxNDim),pom,KPh
      if(ih(1).gt.900) go to 1400
      n=n+1
      ihPwdArr(1:maxNDim,n)=ih(1:maxNDim)
      KPhPwdArr(n)=KPh
      go to 1300
1400  call CloseIfOpened(ln)
      shiftPwd(1:3,KDatBlock)=0.
      KAsym(KDatBlock)=IdPwdAsymNone
      if(KProfPwd(KPhaseAct,KDatBlock).eq.IdPwdProfVoigt)
     1  KProfPwd(KPhaseAct,KDatBlock)=KProfPwdUse
      xxm=0.
      yymn= 9999.
      yymx=-9999.
      n=0
      NLamPwd=1
      do i=1,MxRefPwd
        ih(1:3)=ihPwdArr(1:3,i)
        KPhase=KPhPwdArr(i)
        if(KPhase.ne.KPhaseAct) cycle
        n=n+1
        call FromIndSinthl(ih,h3,sinthl,sinthlq,1,0)
        th=asin(sinthl*LamPwd(1,KDatBlock))
        call SetProfFun(ih,h3,th)
        dth=(rdeg(2)-rdeg(1))/100.
        ProfMax=0.
        do j=-100,100
           tha=float(j)*dth
           pom=prfl(tha)
           ProfMax=max(ProfMax,pom)
        enddo
        if(DrawFunction.eq.1) then
          xx(n)=sin(th)
          yy(n)=cos(th)/ProfMax*100.
        else if(DrawFunction.eq.2) then
          xx(n)=sin(th)**2
          yy(n)=(cos(th)/ProfMax*100.)**2
        else
          xx(n)=sin(th)
          yy(n)=etaPwd
        endif
        if(UseCalibration) then
          KPhase=NPhase+1
          call SetProfFun(ih,h3,th)
          ProfMax=0.
          do j=-100,100
             tha=float(j)*dth
             pom=prfl(tha)
             ProfMax=max(ProfMax,pom)
          enddo
          if(DrawFunction.eq.1) then
            yy(n)=yy(n)-cos(th)/ProfMax*100.
          else if(DrawFunction.eq.2) then
            yy(n)=yy(n)-(cos(th)/ProfMax*100.)**2
          endif
        endif
        xxm=max(xxm,xx(n))
        yymx=max(yymx,yy(n))
        yymn=min(yymn,yy(n))
      enddo
      KPhase=KPhaseAct
      HardCopy=0
2100  call FeHardCopy(HardCopy,'open')
      call FeClearGrWin
      yomn=min(yymn*1.1,0.)
      yomx=yymx*1.1
      xomn=0.
      xomx=xxm*1.1
      call UnitMat(F2O,3)
      call FeSetTransXo2X(xomn,xomx,yomn,yomx,.false.)
      call FeMakeAcFrame
      if(DrawFunction.eq.1.or.DrawFunction.eq.3) then
        Veta='sin(th)'
      else
        Veta='sin(th)^2'
      endif
      if(HardCopy.eq.HardCopyNum) write(85,FormA) '# x axis: '//
     1                                            Veta(:idel(Veta))
      call FeMakeAxisLabels(1,xomn,xomx,yomn,yomx,Veta)
      if(DrawFunction.eq.1) then
        Veta='beta*cos(th)*100'
      else if(DrawFunction.eq.2) then
        Veta='(beta*cos(th)*100)^2'
      else
        Veta='eta'
      endif
      if(HardCopy.eq.HardCopyNum) write(85,FormA) '# y axis: '//
     1                                            Veta(:idel(Veta))
      call FeMakeAxisLabels(2,yomn,yomx,xomn,xomx,Veta)
      xpp(3)=0.
      n=0
      do i=1,MxRefPwd
        ih(1:3)=ihPwdArr(1:3,i)
        KPhase=KPhPwdArr(i)
        if(KPhase.ne.KPhaseAct) cycle
        n=n+1
        xpp(1)=xx(n)
        xpp(2)=yy(n)
        call FeXf2X(xpp,xo)
        call FeCircleOpen(xo(1),xo(2),3.,White)
        if(HardCopy.eq.HardCopyNum) write(85,'(2e15.6)') xpp(1:2)
      enddo
      call FeHardCopy(HardCopy,'close')
      HardCopy=0
2500  call FeQuestEvent(id,ich)
      if(CheckType.eq.EventButton) then
        if(CheckNumber.eq.nButtQuit) then
          go to 8000
        else if(CheckNumber.eq.nButtPrint) then
          call FePrintPicture(ich)
          if(ich.eq.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtSave) then
          call FeSavePicture('picture',7,1)
          if(HardCopy.gt.0) then
            go to 2100
          else
            HardCopy=0
            go to 2500
          endif
        else if(CheckNumber.eq.nButtOptions) then
          go to 1100
        else
          go to 2500
        endif
      else
        go to 2500
      endif
8000  if(id.gt.0) call FeQuestRemove(id)
      go to 9999
9000  call FeReadError(ln)
9999  call CloseIfOpened(ln)
      if(allocated(ihPwdArr))
     1  deallocate(ihPwdArr,FCalcPwdArr,MultPwdArr,iqPwdArr,KPhPwdArr,
     2             ypeaka,peaka,pcota,rdega,shifta,fnorma,tntsima,
     3             ntsima,sqsga,fwhma,sigpa,etaPwda,dedffga,dedffla,
     4             dfwdga,dfwdla,coef,coefp,coefq,Prof0,cotg2tha,
     5             cotgtha,cos2tha,cos2thqa,Alpha12a,Beta12a,xx,yy,
     6             IBroadHKLa)
      KPhase=KPhaseIn
      call iom41(0,0,fln(:ifln)//'.m41')
      return
      end
      subroutine PwdGnuplotPrf
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      integer th2min,th2max,Ymin,Ymax
      character*256 Veta
      character*80  t80
      call PwdPrfNacti(0)
      ln=NextLogicNumber()
      call OpenFile(ln,fln(:ifln)//'_patt.tmp','formatted','unknown')
      th2min=99999
      th2max=0
      Ymin=0
      Ymax=0
      do i=1,Npnts
        th2min=min(th2min,nint(XPwd(i)))
        th2max=max(th2max,nint(XPwd(i)))
!        Ymin=min(Ymin,nint(YoPwd(i)),nint(YcPwd(i)))
        Ymax=max(Ymax,nint(YoPwd(i)),nint(YcPwd(i)))
        write(ln,*) XPwd(i),YoPwd(i),YcPwd(i),YoPwd(i)-YcPwd(i)
      enddo
      call CloseIfOpened(ln)
      ich=0
      id=NextQuestId()
      xqd=350.
      il=3
      Veta='Define parameters for plot window'
      call FeQuestCreate(id,-1.,-1.,xqd,il,Veta,0,LightGray,0,0)
      il=1
      Veta='Procedure designed by Joachim Breternitz'
      call FeQuestLblMake(id,xqd*.5,-10*il+3,Veta,'C','N')
      t80='2th'
      dpom=100.
      do i=1,2
        il=il+1
        tpom=5.
        xpom=tpom+50.
        do j=1,2
          if(j.eq.1) then
            Veta=t80(:idel(t80))//'(min)'
            if(i.eq.1) then
              ipom=th2min
            else
              ipom=Ymin
            endif
          else if(j.eq.2) then
            Veta=t80(:idel(t80))//'(max)'
            if(i.eq.1) then
              ipom=th2max
            else
              ipom=Ymax
            endif
          endif
          call FeQuestEdwMake(id,tpom,il,xpom,il,Veta,'L',dpom,EdwYd,0)
          call FeQuestIntEdwOpen(EdwLastMade,ipom,.false.)
          if(i.eq.1.and.j.eq.1) nEdwFirst=EdwLastMade
          tpom=tpom+xqd*.5
          xpom=tpom+50.
        enddo
        t80='I'
      enddo
1500  call FeQuestEvent(id,ich)
      if(CheckType.ne.0) then
        call NebylOsetren
        go to 1500
      endif
      if(ich.eq.0) then
        nEdw=nEdwFirst
        do i=1,2
          do j=1,2
            call FeQuestIntFromEdw(nEdw,ipom)
            if(i.eq.1) then
              if(j.eq.1) then
                th2min=ipom
              else
                th2max=ipom
              endif
            else
              if(j.eq.1) then
                Ymin=ipom
              else
                Ymax=ipom
              endif
            endif
            nEdw=nEdw+1
          enddo
        enddo
      endif
      call FeQuestRemove(id)
      if(ich.ne.0) go to 9999
      Veta=fln(:ifln)//'_peaks_'
      do i=1,NPhase
        write(Cislo,'(i5)') i
        call Zhusti(Cislo)
        call OpenFile(ln,Veta(:idel(Veta))//Cislo(:idel(Cislo))//'.tmp',
     1                'formatted','unknown')
        do j=1,NBragg,KAlpha2+1
          if(KlicArr(j)/100+1.eq.i)
     1      write(ln,*) BraggArr(j),i*2,' 0.75',indArr(1:3,j)
        enddo
        call CloseIfOpened(ln)
      enddo
      call OpenFile(ln,fln(:ifln)//'.gnu','formatted','unknown')
      write(ln,FormA) 'unset key'
      write(Veta,100) 'x',th2min,th2max
      call Zhusti(Veta)
      write(ln,FormA) 'set '//Veta(:idel(Veta))
      write(Veta,100) 'y',Ymin,Ymax
      call Zhusti(Veta)
      write(ln,FormA) 'set '//Veta(:idel(Veta))
      write(ln,FormA) 'set y2range [1:100]'
      write(ln,FormA) 'set mxtics 5'
      write(ln,FormA) 'set xtics 10'
      write(ln,FormA) 'set xtics out nomirror'
      write(ln,FormA) 'set ytics out nomirror'
      write(ln,FormA) 'set mytics 5'
      write(ln,FormA) 'set xlabel ''Scattering Angle (2{/Symbol q})'''
      write(ln,FormA) 'set ylabel ''Intensity'''
      write(ln,FormA) 'set terminal postscript enhanced color solid '//
     1                '''Arial'' landscape'
      write(ln,FormA) 'set output "'//fln(:ifln)//'_plot.eps"'
      write(ln,FormA) 'set encoding iso_8859_15'
      write(ln,FormA) 'set bars small'
      Veta=fln(:ifln)//'_patt.tmp'
      write(ln,FormA,advance='NO')
     1  'plot "'//Veta(:idel(Veta))//
     2  '" using 1:2 with points lc 000 ps 0.5,'
      write(ln,FormA,advance='NO')
     1  ' "'//Veta(:idel(Veta))//
     2  '" using 1:3 with lines lt 1 lw 2,'
      write(ln,FormA,advance='NO')
     1  ' "'//Veta(:idel(Veta))//
     2  '" using 1:4 with lines lc 025 lw 2,'
      Veta=fln(:ifln)//'_peaks_'
      do i=1,NPhase
        write(Cislo,'(i5)') i
        call Zhusti(Cislo)
        write(ln,'(a,'' using 1:2:3 axes x1y2'',a)',advance='NO')
     1    ' "'//Veta(:idel(Veta))//Cislo(:idel(Cislo))//'.tmp"',
     2    ' with errorbars ls -1,'
      enddo
      write(ln,FormA)
      write(ln,FormA) 'exit'
      call CloseIfOpened(ln)
      call FeSystem(CallGnuplot(:idel(CallGnuplot))//' '//
     1              fln(:ifln)//'.gnu')
      Veta=' -dSAFER -dBATCH -dNOPAUSE -dFIXEDMEDIA -dEPSCrop '//
     1     '-sDEVICE=png16m -r600x600 -sOutputFile='//
     2     '"'//fln(:ifln)//'_plot.png" "'//fln(:ifln)//'_plot.eps"'
     3
      call FeSystem(CallGS(:idel(CallGS))//Veta(:idel(Veta)))
      NInfo=3
      TextInfo(1)='The powder profile files:'
      TextInfo(2)='"'//fln(:ifln)//'_plot.eps" and "'//fln(:ifln)//
     1            '_plot.png"'
      TextInfo(3)='were created.'
      call FeInfoOut(-1.,-1.,'INFORMATION','L')
9999  call DeleteFile(fln(:ifln)//'_patt.tmp')
      Veta=fln(:ifln)//'_peaks_'
      do i=1,NPhase
        write(Cislo,'(i5)') i
        call Zhusti(Cislo)
        call DeleteFile(Veta(:idel(Veta))//Cislo(:idel(Cislo))//'.tmp')
      enddo
      call DeleteFile(fln(:ifln)//'.gnu')
      return
100   format(a1,'range [',i15,':',i15,']')
      end
      subroutine PwdSetTOF(KDatB)
      include 'fepc.cmn'
      include 'basic.cmn'
      logical PwdCheckTOFInD
      if(DataType(KDatB).eq.-2) then
        if(Radiation(KDatB).eq.NeutronRadiation) then
          isTOF=.true.
          TOFInD=PwdCheckTOFInD(KDatB)
        else
          isED=.true.
        endif
      else
        isTOF=.false.
        TOFInD=.false.
        isED=.false.
      endif
      return
      end

