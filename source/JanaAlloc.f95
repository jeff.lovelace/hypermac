      subroutine ReallocEquations(mxeNew,mxepNew)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer      lnpOld(:),lnpoOld(:),pnpOld(:,:),npaOld(:)
      real         pkoOld(:,:),pabOld(:)
      character*20 latOld(:),patOld(:,:)
      character*12 lpaOld(:),ppaOld(:,:)
      logical      eqharOld(:)
      allocatable  lnpOld,lnpoOld,pnpOld,npaOld,pkoOld,pabOld,latOld,
     1             patOld,lpaOld,ppaOld,eqharOld
      if(allocated(pnp)) then
        mxeOld=ubound(pnp,2)
        mxepOld=ubound(pnp,1)
        if(mxeOld.ge.mxeNew.and.mxepOld.ge.mxepNew) go to 9999
        allocate(lnpOld(mxeOld),pnpOld(mxepOld,mxeOld),
     1           npaOld(mxeOld),pkoOld(mxepOld,mxeOld),
     2           latOld(mxeOld),patOld(mxepOld,mxeOld),
     3           lpaOld(mxeOld),ppaOld(mxepOld,mxeOld),
     4           lnpoOld(mxeOld),pabOld(mxeOld),eqharOld(mxeOld))
        do i=1,neq
          lnpOld(i)=lnp(i)
          npaOld(i)=npa(i)
          latOld(i)=lat(i)
          lpaOld(i)=lpa(i)
          pabOld(i)=pab(i)
          lnpoOld(i)=lnpo(i)
          eqharOld(i)=eqhar(i)
          do j=1,npa(i)
            pnpOld(j,i)=pnp(j,i)
            pkoOld(j,i)=pko(j,i)
            patOld(j,i)=pat(j,i)
            ppaOld(j,i)=ppa(j,i)
          enddo
        enddo
        deallocate(lnp,pnp,npa,pko,lat,pat,lpa,ppa,lnpo,pab,eqhar)
      else
        mxeOld=0
        mxepOld=0
      endif
      mxe=mxeNew
      mxep=mxepNew
      allocate(lnp(mxe),pnp(mxep,mxe),
     1         npa(mxe),pko(mxep,mxe),
     2         lat(mxe),pat(mxep,mxe),
     3         lpa(mxe),ppa(mxep,mxe),
     4         lnpo(mxe),pab(mxe),eqhar(mxe))
      if(mxeOld.gt.0) then
        do i=1,neq
          lnp(i)=lnpOld(i)
          npa(i)=npaOld(i)
          lat(i)=latOld(i)
          lpa(i)=lpaOld(i)
          pab(i)=pabOld(i)
          lnpo(i)=lnpoOld(i)
          eqhar(i)=eqharOld(i)
c        if(npa(i).gt.0) then
c          jk=npaOld(i)
c        else
c          jk=mxepOld
c        endif
          do j=1,npaOld(i)
            pnp(j,i)=pnpOld(j,i)
            pko(j,i)=pkoOld(j,i)
            pat(j,i)=patOld(j,i)
            ppa(j,i)=ppaOld(j,i)
          enddo
        enddo
        deallocate(lnpOld,pnpOld,npaOld,pkoOld,latOld,patOld,lpaOld,
     1             ppaOld,lnpoOld,pabOld,eqharOld)
      endif
9999  return
      end
      subroutine AllocateSymm(NSymmNew,NLattVecNew,NCompNew,NPhaseNew)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(rm)) deallocate(rm,rm6,s6,symmc,vt6,ISwSymm,
     1                             BratSymm,rmag,zmag,InvMag,CenterMag)
      allocate(rm                 (9,NSymmNew,NCompNew,NPhaseNew),
     1         rm6  (NDim(KPhase)**2,NSymmNew,NCompNew,NPhaseNew),
     2         s6   (NDim (KPhase)  ,NSymmNew,NCompNew,NPhaseNew),
     3         symmc(NDim (KPhase),  NSymmNew,NCompNew,NPhaseNew),
     4         ISwSymm              (NSymmNew,NCompNew,NPhaseNew),
     5         RMag               (9,NSymmNew,NCompNew,NPhaseNew),
     6         ZMag                 (NSymmNew,NCompNew,NPhaseNew),
     7         BratSymm             (NSymmNew,         NPhaseNew),
     8         vt6  (NDim(KPhase),NLattVecNew,NCompNew,NPhaseNew),
     9         InvMag(NPhaseNew),CenterMag(NPhaseNew))
      do i=1,NSymmNew
        do j=1,NCompNew
          do k=1,NPhaseNew
            call UnitMat(rm  (1,i,j,k),3)
            call UnitMat(rmag(1,i,j,k),3)
            call UnitMat(rm6 (1,i,j,k),NDim(KPhase))
            call SetRealArrayTo(s6(1,i,j,k),NDim(KPhase),0.)
            call SetStringArrayTo(SymmC(1,i,j,k),NDim(KPhase),' ')
            zmag(i,j,k)=1.
            ISwSymm(i,j,k)=j
            BratSymm(i,k)=.true.
            ZMag(i,j,k)=1.
          enddo
        enddo
      enddo
      InvMag(1:NPhaseNew)=0
      CenterMag(1:NPhaseNew)=0
      do i=1,NLattVecNew
        do j=1,NCompNew
          do k=1,NPhaseNew
            call SetRealArrayTo(vt6(1,i,j,k),NDim(KPhase),0.)
          enddo
        enddo
      enddo
      return
      end
      subroutine ReallocSymm(NDimNew,NSymmNew,NLattVecNew,NCompNew,
     1                       NPhaseNew,Klic)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real, allocatable :: rm6o(:,:,:,:),rmo(:,:,:,:),s6o(:,:,:,:),
     1                     vt6o(:,:,:,:),rmago(:,:,:,:),zmago(:,:,:)
      integer, allocatable :: ISwSymmo(:,:,:),InvMagO(:),CenterMagO(:)
      character*30, allocatable :: symmco(:,:,:,:)
      logical, allocatable :: BratSymmO(:,:)
      NDimOld=ubound(s6,1)
      NSymmOld=ubound(s6,2)
      NCompOld=ubound(s6,3)
      NPhaseOld=ubound(s6,4)
      NLattVecOld=ubound(vt6,2)
      if(NDimOld.ge.NDimNew.and.NSymmOld.ge.NSymmNew.and.
     1   NCompOld.ge.NCompNew.and.NPhaseOld.ge.NPhaseNew.and.
     2   NLattVecOld.ge.NLattVecNew) go to 9999
      allocate(   rmo(9         ,NSymmOld,NCompOld,NPhaseOld),
     1           rm6o(NDimOld**2,NSymmOld,NCompOld,NPhaseOld),
     2            s6o(NDimOld   ,NSymmOld,NCompOld,NPhaseOld),
     3         symmco(NDimOld   ,NSymmOld,NCompOld,NPhaseOld),
     4           vt6o(NDimOld   ,NLattVecOld,NCompOld,NPhaseOld),
     5                  ISwSymmo(NSymmOld,NCompOld,NPhaseOld),
     6         BratSymmO(NSymmOld,NPhaseOld),
     7         rmago(9,NSymmOld,NCompOld,NPhaseOld),
     8         zmago(  NSymmOld,NCompOld,NPhaseOld),
     9         InvMagO(NPhaseOld),CenterMagO(NPhaseOld))
      do i=1,NPhaseOld
        if(Klic.eq.0) then
          NCompFirst=1
          NCompLast =NComp(i)
        else
          NCompFirst=Klic
          NCompLast =Klic
        endif
        do j=NCompFirst,NCompLast
          do k=1,min(NSymm(i),NSymmOld)
            call CopyVek(rm (1,k,j,i),rmo (1,k,j,i),9)
            call CopyVek(rm6(1,k,j,i),rm6o(1,k,j,i),NDimOld**2)
            call CopyVek(s6 (1,k,j,i),s6o (1,k,j,i),NDimOld)
            do l=1,NDimOld
              symmco(l,k,j,i)=symmc(l,k,j,i)
            enddo
            ISwSymmO(k,j,i)=ISwSymm(k,j,i)
            call CopyVek(rmag(1,k,j,i),rmago(1,k,j,i),9)
            zmago(k,j,i)=zmag(k,j,i)
            if(j.eq.1) BratSymmO(k,i)=BratSymm(k,i)
          enddo
          do k=1,min(NLattVec(i),NLattVecOld)
            call CopyVek(vt6(1,k,j,i),vt6o(1,k,j,i),NDimOld)
          enddo
        enddo
      enddo
      InvMagO(1:NPhaseOld)=InvMag(1:NPhaseOld)
      CenterMagO(1:NPhaseOld)=CenterMag(1:NPhaseOld)
      deallocate(rm6,rm,s6,symmc,vt6,ISwSymm,BratSymm,rmag,zmag,InvMag,
     1           CenterMag)
      ns=max(NSymmNew,NSymmOld)
      nc=max(NCompNew,NCompOld)
      nvt=max(NLattVecNew,NLattVecOld)
      nd=max(NDimNew,NDimOld)
      nph=max(NPhaseNew,NPhaseOld)
      allocate(rm(9,ns,nc,nph),rm6(nd**2,ns,nc,nph),s6(nd,ns,nc,nph),
     1         symmc(nd,ns,nc,nph),vt6(nd,nvt,nc,nph),
     2         ISwSymm(ns,nc,nph),BratSymm(ns,nph),
     3         rmag(9,ns,nc,nph),zmag(ns,nc,nph),InvMag(nph),
     4         CenterMag(nph))
      do i=1,min(NPhaseOld,NPhaseNew)
        if(Klic.eq.0) then
          NCompFirst=1
          NCompLast =NComp(i)
        else
          NCompFirst=Klic
          NCompLast =Klic
        endif
        do j=NCompFirst,NCompLast
          do k=1,min(NSymm(i),NSymmOld)
            call SetRealArrayTo(rm6(1,k,j,i),nd**2,0.)
            call SetRealArrayTo(s6 (1,k,j,i),nd,   0.)
            call SetStringArrayTo(symmc(1,k,j,i),nd,' ')
            call CopyVek(rmo (1,k,j,i),rm (1,k,j,i),9)
            call CopyVek(rm6o(1,k,j,i),rm6(1,k,j,i),NDimOld**2)
            call CopyVek(s6o (1,k,j,i),s6 (1,k,j,i),NDimOld)
            do l=1,NDimOld
              symmc(l,k,j,i)=symmco(l,k,j,i)
            enddo
            ISwSymm(k,j,i)=ISwSymmO(k,j,i)
            call CopyVek(rmago(1,k,j,i),rmag(1,k,j,i),9)
            zmag(k,j,i)=zmago(k,j,i)
            if(j.eq.1) BratSymm(k,i)=BratSymmO(k,i)
          enddo
          do k=NSymmOld+1,NSymmNew
            call SetRealArrayTo(rm6(1,k,j,i),nd**2,0.)
            call SetRealArrayTo(s6 (1,k,j,i),nd,   0.)
            call SetStringArrayTo(symmc(1,k,j,i),nd,' ')
            if(j.eq.1) BratSymm(k,i)=.true.
          enddo
          do k=1,min(NLattVec(i),NLattVecOld)
            call SetRealArrayTo(vt6(1,k,j,i),nd,0.)
            call CopyVek(vt6o(1,k,j,i),vt6(1,k,j,i),NDimOld)
          enddo
          do k=NLattVecOld+1,NLattVecNew
            call SetRealArrayTo(vt6(1,k,j,i),nd,0.)
          enddo
        enddo
        InvMag(i)=InvMagO(i)
        CenterMag(i)=CenterMagO(i)
      enddo
      deallocate(rm6o,rmo,s6o,symmco,vt6o,IswSymmo,BratSymmO,rmago,
     1           zmago,InvMagO,CenterMagO)
      do i=min(NPhaseOld,NPhaseNew)+1,NPhaseNew
        if(Klic.eq.0) then
          NCompFirst=1
          NCompLast =NComp(i)
        else
          NCompFirst=Klic
          NCompLast =Klic
        endif
        do j=NCompFirst,NCompLast
          do k=1,NSymmNew
            call SetRealArrayTo(rm6(1,k,j,i),nd**2,0.)
            call SetRealArrayTo(s6 (1,k,j,i),nd,   0.)
            ISwSymm(k,j,i)=k
            call SetRealArrayTo(rmag(1,k,j,i),9,0.)
            zmag(k,j,i)=1.
            call SetStringArrayTo(symmc(1,k,1,i),nd,' ')
            if(j.eq.1) BratSymm(k,i)=.true.
          enddo
          do k=1,NLattVecNew
            call SetRealArrayTo(vt6(1,k,j,i),nd,0.)
          enddo
        enddo
        InvMag(i)=0
        CenterMag(i)=0
      enddo
9999  return
      end
      subroutine ReallocFormF(NAtFormulaNew,NPhaseNew,NDatBlockNew)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      real, allocatable :: AtWeightO(:,:),AtRadiusO(:,:),AtNumO(:,:),
     1  AtMultO(:,:),FFBasicO(:,:,:),FFCoreO(:,:,:),FFCoreDO(:,:,:),
     2  FFValO(:,:,:),FFValDO(:,:,:),FFnO(:,:),FFniO(:,:),FFMagO(:,:,:),
     3  FFElO(:,:,:),FFraO(:,:,:),FFiaO(:,:,:), ZSTOAO(:,:,:,:,:),
     4  CSTOAO(:,:,:,:,:),ZSlaterO(:,:,:),HZSlaterO(:,:),FFaO(:,:,:,:),
     5  FFaeO(:,:,:,:),fxO(:),fmO(:),TypicalDistO(:,:,:),
     6  DTypicalDistO(:,:)
      integer, allocatable :: PopCoreO(:,:,:),PopValO(:,:,:),
     1                        HNSlaterO(:,:),NSlaterO(:,:,:),
     2                        AtColorO(:,:),TypeCoreO(:,:),
     3                        TypeValO(:,:),AtValO(:,:),
     4                        NSTOAO(:,:,:,:,:),NCoefSTOAO(:,:,:,:),
     5                        TypeFFMagO(:,:)
      character*256, allocatable :: CoreValSourceO(:,:)
      character*7, allocatable :: AtTypeFullO(:,:),AtTypeMagO(:,:),
     1                            AtTypeMagJO(:,:)
      character*2, allocatable :: AtTypeO(:,:)
      if(Allocated(AtType)) then
        NAtFormulaOld=ubound(AtType,1)
        NPhaseOld=ubound(AtType,2)
        NDatBlockOld=ubound(FFra,3)
      else
        NAtFormulaOld=0
        NPhaseOld=0
        NDatBlockOld=0
      endif
      if(NAtFormulaOld.ge.NAtFormulaNew.and.NPhaseOld.ge.NPhaseNew.and.
     1   NDatBlockOld.ge.NDatBlockNew) go to 9999
      if(NAtFormulaOld.le.0) go to 1800
      n=MaxNAtFormula
      m=121
      i=NDatBlock
      mm=NPhaseOld
      allocate(AtTypeO(n,mm),AtTypeFullO(n,mm),
     1         AtTypeMagO(n,mm),AtTypeMagJO(n,mm),AtWeightO(n,mm),
     2         AtRadiusO(n,mm),AtColorO(n,mm),
     3         AtNumO(n,mm),AtValO(n,mm),AtMultO(n,mm),
     4         FFBasicO(m,n,mm),
     5         FFCoreO(m,n,mm),FFCoreDO(m,n,mm),
     6         FFValO(m,n,mm),FFValDO(m,n,mm),
     7         FFraO(n,mm,i),FFiaO(n,mm,i),
     8         FFnO(n,mm),FFniO(n,mm),FFMagO(7,n,mm),
     9         TypeFFMagO(n,mm),
     a         FFaO(4,m,n,mm),FFaeO(4,m,n,mm),fxO(n),fmO(n),
     1         FFElO(m,n,mm),TypicalDistO(n,n,mm))
      allocate(TypeCoreO(n,mm),TypeValO(n,mm),
     1         PopCoreO(28,n,mm),PopValO(28,n,mm),
     2         ZSlaterO(8,n,mm),NSlaterO(8,n,mm),
     3         HNSlaterO(n,mm),HZSlaterO(n,mm),
     4         ZSTOAO(7,7,40,n,mm),CSTOAO(7,7,40,n,mm),
     5         NSTOAO(7,7,40,n,mm),NCoefSTOAO(7,7,n,mm),
     6         CoreValSourceO(n,mm))
      do i=1,NPhaseOld
        do j=1,min(NAtFormula(i),NAtFormulaOld)
          AtTypeO    (j,i)=AtType    (j,i)
          AtTypeFullO(j,i)=AtTypeFull(j,i)
          AtTypeMagO (j,i)=AtTypeMag (j,i)
          AtTypeMagJO(j,i)=AtTypeMagJ(j,i)
          AtWeightO  (j,i)=AtWeight  (j,i)
          AtRadiusO  (j,i)=AtRadius  (j,i)
          AtColorO   (j,i)=AtColor   (j,i)
          AtNumO     (j,i)=AtNum     (j,i)
          AtValO     (j,i)=AtVal     (j,i)
          AtMultO    (j,i)=AtMult    (j,i)
          FFnO       (j,i)=FFn       (j,i)
          FFniO      (j,i)=FFni      (j,i)
          do k=1,iabs(FFType(i))
            FFBasicO(k,j,i)=FFBasic(k,j,i)
            if(ChargeDensities) then
              FFCoreO (k,j,i)=FFCore (k,j,i)
              FFCoreDO(k,j,i)=FFCoreD(k,j,i)
              FFValO (k,j,i)=FFVal (k,j,i)
              FFValDO(k,j,i)=FFValD(k,j,i)
            endif
          enddo
          do k=1,62
            FFElO(k,j,i)=FFEl(k,j,i)
          enddo
          if(AtTypeMag(j,i).ne.' ') then
            do k=1,7
              FFMagO(k,j,i)=FFMag(k,j,i)
            enddo
            TypeFFMagO(j,i)=TypeFFMag(j,i)
          endif
          do k=1,NDatBlockOld
            FFraO(j,i,k)=FFra(j,i,k)
            FFiaO(j,i,k)=FFia(j,i,k)
          enddo
          do k=1,min(NAtFormula(i),NAtFormulaOld)
            TypicalDistO(j,k,i)=TypicalDist(j,k,i)
          enddo
          CoreValSourceO(j,i)=CoreValSource(j,i)
        enddo
      enddo
      if(ChargeDensities) then
        do i=1,NPhaseOld
          do j=1,min(NAtFormula(i),NAtFormulaOld)
            TypeCoreO (j,i)=TypeCore (j,i)
            TypeValO  (j,i)=TypeVal  (j,i)
            HNSlaterO (j,i)=HNSlater (j,i)
            HZSlaterO(j,i)=HZSlater(j,i)
            do k=1,28
              PopCoreO(k,j,i)=PopCore(k,j,i)
              PopValO (k,j,i)=PopVal (k,j,i)
            enddo
            do k=1,8
              ZSlaterO(k,j,i)=ZSlater(k,j,i)
              NSlaterO (k,j,i)=NSlater (k,j,i)
            enddo
            do i1=1,7
              do i2=1,7
                NCoefSTOAO(i1,i2,j,i)=NCoefSTOA(i1,i2,j,i)
                do k=1,40
                  ZSTOAO(i1,i2,k,j,i)=ZSTOA(i1,i2,k,j,i)
                  CSTOAO(i1,i2,k,j,i)=CSTOA(i1,i2,k,j,i)
                  NSTOAO(i1,i2,k,j,i)=NSTOA(i1,i2,k,j,i)
                enddo
              enddo
            enddo
          enddo
        enddo
      endif
1800  if(allocated(AtType))
     1  deallocate(AtType,AtTypeFull,AtTypeMag,AtTypeMagJ,AtWeight,
     2             AtRadius,AtColor,AtNum,AtVal,AtMult,FFBasic,FFCore,
     3             FFCoreD,FFVal,FFValD,FFra,FFia,FFn,FFni,FFMag,
     4             TypeFFMag,FFa,FFae,fx,fm,FFEl,TypicalDist)
      if(allocated(TypeCore))
     1  deallocate(TypeCore,TypeVal,PopCore,PopVal,ZSlater,NSlater,
     2             HNSlater,HZSlater,ZSTOA,CSTOA,NSTOA,NCoefSTOA,
     3             CoreValSource)
2000  n=max(NAtFormulaNew,NAtFormulaOld)
      mm=max(NPhaseNew,NPhaseOld)
      m=121
      i=max(NDatBlockNew,NDatBlockOld)
      allocate(AtType(n,mm),AtTypeFull(n,mm),
     1         AtTypeMag(n,mm),AtTypeMagJ(n,mm),AtWeight(n,mm),
     2         AtRadius(n,mm),AtColor(n,mm),
     3         AtNum(n,mm),AtVal(n,mm),AtMult(n,mm),
     4         FFBasic(m,n,mm),
     5         FFCore(m,n,mm),FFCoreD(m,n,mm),
     6         FFVal(m,n,mm),FFValD(m,n,mm),
     7         FFra(n,mm,i),FFia(n,mm,i),
     8         FFn(n,mm),FFni(n,mm),FFMag(7,n,mm),TypeFFMag(n,mm),
     9         FFa(4,m,n,mm),FFae(4,m,n,mm),fx(n),fm(n),
     a         FFEl(m,n,mm),TypicalDist(n,n,mm))
      allocate(TypeCore(n,mm),TypeVal(n,mm),
     1         PopCore(28,n,mm),PopVal(28,n,mm),
     2         ZSlater(8,n,mm),NSlater(8,n,mm),
     3         HNSlater(n,mm),HZSlater(n,mm),
     4         ZSTOA(7,7,40,n,mm),CSTOA(7,7,40,n,mm),
     5         NSTOA(7,7,40,n,mm),NCoefSTOA(7,7,n,mm),
     6         CoreValSource(n,mm))
      do i=1,min(NPhaseOld,NPhaseNew)
        do j=1,min(NAtFormula(i),NAtFormulaOld)
          AtType    (j,i)=AtTypeO    (j,i)
          AtTypeFull(j,i)=AtTypeFullO(j,i)
          AtTypeMag (j,i)=AtTypeMagO (j,i)
          AtTypeMagJ(j,i)=AtTypeMagJO(j,i)
          AtWeight  (j,i)=AtWeightO  (j,i)
          AtRadius  (j,i)=AtRadiusO  (j,i)
          AtColor   (j,i)=AtColorO   (j,i)
          AtNum     (j,i)=AtNumO     (j,i)
          AtVal     (j,i)=AtValO     (j,i)
          AtMult    (j,i)=AtMultO    (j,i)
          FFn       (j,i)=FFnO       (j,i)
          FFni      (j,i)=FFniO      (j,i)
          do k=1,iabs(FFType(i))
            FFBasic(k,j,i)=FFBasicO(k,j,i)
            if(ChargeDensities) then
              FFCore(k,j,i) =FFCoreO (k,j,i)
              FFCoreD(k,j,i)=FFCoreDO(k,j,i)
              FFVal  (k,j,i)=FFValO  (k,j,i)
              FFValD (k,j,i)=FFValDO (k,j,i)
              FFVal  (k,j,i)=FFValO  (k,j,i)
            endif
          enddo
          do k=1,62
            FFEl(k,j,i)=FFElO(k,j,i)
          enddo
          if(AtTypeMagO(j,i).ne.' ') then
            do k=1,7
              FFMag(k,j,i)=FFMagO(k,j,i)
            enddo
            TypeFFMag(j,i)=TypeFFMagO(j,i)
          endif
          do k=1,NDatBlockOld
            FFra(j,i,k)=FFraO(j,i,k)
            FFia(j,i,k)=FFiaO(j,i,k)
          enddo
          do k=1,min(NAtFormula(i),NAtFormulaOld)
            TypicalDist(j,k,i)=TypicalDistO(j,k,i)
          enddo
          CoreValSource(j,i)=CoreValSourceO(j,i)
        enddo
      enddo
      if(ChargeDensities) then
        do i=1,min(NPhaseOld,NPhaseNew)
          do j=1,min(NAtFormula(i),NAtFormulaOld)
            TypeCore (j,i)=TypeCoreO (j,i)
            TypeVal  (j,i)=TypeValO  (j,i)
            HNSlater (j,i)=HNSlaterO (j,i)
            HZSlater(j,i)=HZSlaterO(j,i)
            do k=1,28
              PopCore(k,j,i)=PopCoreO(k,j,i)
              PopVal (k,j,i)=PopValO (k,j,i)
            enddo
            do k=1,8
              ZSlater(k,j,i)=ZSlaterO(k,j,i)
              NSlater (k,j,i)=NSlaterO (k,j,i)
            enddo
            do i1=1,7
              do i2=1,7
                NCoefSTOA(i1,i2,j,i)=NCoefSTOAO(i1,i2,j,i)
                do k=1,40
                  ZSTOA(i1,i2,k,j,i)=ZSTOAO(i1,i2,k,j,i)
                  CSTOA(i1,i2,k,j,i)=CSTOAO(i1,i2,k,j,i)
                  NSTOA(i1,i2,k,j,i)=NSTOAO(i1,i2,k,j,i)
                enddo
              enddo
            enddo
          enddo
        enddo
      endif
      if(allocated(AtTypeO))
     1  deallocate(AtTypeO,AtTypeFullO,AtTypeMagO,AtTypeMagJO,AtWeightO,
     2             AtRadiusO,AtColorO,AtNumO,AtValO,AtMultO,FFBasicO,
     3             FFCoreO,FFCoreDO,FFValO,FFValDO,FFraO,FFiaO,FFnO,
     4             FFniO,FFMagO,TypeFFMagO,FFaO,FFaeO,fxO,fmO,FFElO,
     5             TypicalDistO)
      if(allocated(TypeCoreO))
     1  deallocate(TypeCoreO,TypeValO,PopCoreO,PopValO,ZSlaterO,
     2             NSlaterO,HNSlaterO,HZSlaterO,ZSTOAO,CSTOAO,NSTOAO,
     3             NCoefSTOAO,CoreValSourceO)
9999  return
      end
      subroutine ReallocateTypicalDist(NTypicalDistNew)
      use Basic_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension DTypicalDistO(:,:),NTypicalDistO(:)
      character*2 AtTypicalDistO(:,:,:)
      allocatable DTypicalDistO,NTypicalDistO,AtTypicalDistO
      if(NTypicalDistNew.le.NTypicalDistMax) go to 9999
      if(NTypicalDistMax.gt.0) then
        n=NTypicalDistNew
        allocate(DTypicalDistO(n,NPhase),NTypicalDistO(NPhase),
     1           AtTypicalDistO(2,n,NPhase))
        do i=1,NPhase
          NTypicalDistO(i)=NTypicalDist(i)
          do j=1,NTypicalDist(i)
            DTypicalDistO(j,i)=DTypicalDist(j,i)
            do k=1,2
              AtTypicalDistO(k,j,NPhase)=AtTypicalDist(k,j,NPhase)
            enddo
          enddo
        enddo
      endif
      if(allocated(NTypicalDist))
     1  deallocate(DTypicalDist,NTypicalDist,AtTypicalDist)
      n=NTypicalDistNew
      allocate(DTypicalDist(n,NPhase),NTypicalDist(NPhase),
     1         AtTypicalDist(2,n,NPhase))
      if(NTypicalDistMax.gt.0) then
        do i=1,NPhase
          NTypicalDist(i)=NTypicalDistO(i)
          do j=1,NTypicalDist(i)
            DTypicalDist(j,i)=DTypicalDistO(j,i)
            do k=1,2
              AtTypicalDist(k,j,NPhase)=AtTypicalDistO(k,j,NPhase)
            enddo
          enddo
        enddo
        if(allocated(NTypicalDistO))
     1    deallocate(DTypicalDistO,NTypicalDistO,AtTypicalDistO)
      endif
      NTypicalDistMax=NTypicalDistNew
9999  return
      end
      subroutine ReallocateSavedPoints(MaxSavedPointsNew)
      use Basic_mod
      dimension XSavedPointOld(:,:)
      character*80 StSavedPointOld(:)
      allocatable XSavedPointOld,StSavedPointOld
      if(MaxSavedPointsNew.le.MaxSavedPoints) go to 9999
      if(NSavedPoints.gt.0) then
        allocate(XSavedPointOld(3,NSavedPoints),
     1          StSavedPointOld(NSavedPoints))
        call CopyVek(XSavedPoint,XSavedPointOld,3*NSavedPoints)
        call CopyStringArray(StSavedPoint,StSavedPointOld,NSavedPoints)
      endif
      MaxSavedPoints=MaxSavedPointsNew
      if(allocated(XSavedPoint)) deallocate(XSavedPoint,StSavedPoint)
      allocate(XSavedPoint(3,MaxSavedPoints),
     1         StSavedPoint(MaxSavedPoints))
      if(NSavedPoints.gt.0) then
        call CopyVek(XSavedPointOld,XSavedPoint,3*NSavedPoints)
        call CopyStringArray(StSavedPointOld,StSavedPoint,NSavedPoints)
        deallocate(XSavedPointOld,StSavedPointOld)
      endif
9999  return
      end
      subroutine ReallocatePDF(NPlus)
      use Contour_mod
      character*80, allocatable :: scpdfo(:)
      integer, allocatable :: iapdfo(:),ispdfo(:),idpdfo(:)
      real, allocatable :: xpdfo(:,:),popaspdfo(:,:)
      if(NPDF.lt.MaxPDF) go to 9999
      if(NPDF.gt.0) then
        allocate(iapdfo(NPDF),ispdfo(NPDF),idpdfo(NPDF),
     1           scpdfo(NPDF),xpdfo(3,NPDF),popaspdfo(64,NPDF))
        call CopyVekI(iapdf,iapdfo,NPDF)
        call CopyVekI(ispdf,ispdfo,NPDF)
        call CopyVekI(idpdf,idpdfo,NPDF)
        call CopyVek (xpdf,xpdfo,3*NPDF)
        call CopyVek (popaspdf,popaspdfo,64*NPDF)
        call CopyStringArray(scpdf,scpdfo,NPDF)
      endif
      if(allocated(iapdf))
     1  deallocate(iapdf,ispdf,ipor,BratAnharmPDF,xpdf,popaspdf,dpdf,
     2             idpdf,fpdf,ypdf,SelPDF,scpdf)
      n=MaxPDF+NPlus
      allocate(iapdf(n),ispdf(n),ipor(n),BratAnharmPDF(5,n),xpdf(3,n),
     1         popaspdf(64,n),dpdf(n),idpdf(n),fpdf(n),ypdf(3,n),
     2         SelPDF(n),scpdf(n))
      if(NPDF.gt.0) then
        call CopyVekI(iapdfo,iapdf,NPDF)
        call CopyVekI(ispdfo,ispdf,NPDF)
        call CopyVekI(idpdfo,idpdf,NPDF)
        call CopyVek (xpdfo,xpdf,3*NPDF)
        call CopyStringArray(scpdfo,scpdf,NPDF)
        call CopyVek (popaspdfo,popaspdf,64*NPDF)
        deallocate(iapdfo,ispdfo,idpdfo,scpdfo,xpdfo,popaspdfo)
      endif
      MaxNPDF=n
9999  return
      end
      subroutine ReallocateRunSENJU(NPlus)
      use Datred_mod
      integer RunSENJUO(:)
      allocatable RunSENJUO
      if(NRunSENJU.lt.NRunSENJUMax) go to 9999
      if(NRunSENJU.gt.0) then
        allocate(RunSENJUO(NRunSENJU))
        call CopyVekI(RunSENJU,RunSENJUO,NRunSENJU)
      endif
      if(allocated(RunSENJU)) deallocate(RunSENJU)
      n=NRunSENJUMax+NPlus
      allocate(RunSENJU(n))
      if(NRunSENJU.gt.0) then
        call CopyVekI(RunSENJUO,RunSENJU,NRunSENJU)
        deallocate(RunSENJUO)
      endif
      NRunSENJUMax=n
9999  return
      end
      subroutine ReallocSGTest(n)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real, allocatable :: riarp(:),rsarp(:),RunCCDArP(:)
      integer, allocatable :: RefBlockArP(:),ICullArP(:)
      allocate(riarp(NRefAlloc),rsarp(NRefAlloc),ICullArP(NRefAlloc),
     1         RefBlockArP(NRefAlloc),RunCCDArP(NRefAlloc))
      call CopyVek (riar,riarp,NRefAlloc)
      call CopyVek (rsar,rsarp,NRefAlloc)
      call CopyVekI(ICullAr,ICullArP,NRefAlloc)
      call CopyVekI(RefBlockAr,RefBlockArP,NRefAlloc)
      call CopyVek(RunCCDAr,RunCCDArP,NRefAlloc)
      NRefAllocp=NRefAlloc
      NRefAlloc=NRefAlloc+n
      deallocate(riar,rsar,ICullAr,RefBlockAr,RunCCDAr)
      allocate(riar(NRefAlloc),rsar(NRefAlloc),ICullAr(NRefAlloc),
     1         RefBlockAr(NRefAlloc),RunCCDAr(NRefAlloc))
      call CopyVek (riarp,riar,NRefAllocp)
      call CopyVek (rsarp,rsar,NRefAllocp)
      call CopyVekI(ICullArP,ICullAr,NRefAllocp)
      call CopyVekI(RefBlockArP,RefBlockAr,NRefAllocp)
      call CopyVek(RunCCDArP,RunCCDAr,NRefAllocP)
      deallocate(riarp,rsarp,ICullArP,RefBlockArP,RunCCDArP)
      return
      end
      subroutine ReallocIndInt(n)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension iharp(:,:),riarp(:),rsarp(:),ICullArP(:)
      allocatable iharp,riarp,rsarp,ICullArP
      allocate(iharp(NDim(KPhase),NRefAlloc),riarp(NRefAlloc),
     1         rsarp(NRefAlloc),ICullArP(NRefAlloc))
      call CopyVekI(ihar,iharp,NRefAlloc*NDim(KPhase))
      call CopyVek (riar,riarp,NRefAlloc)
      call CopyVek (rsar,rsarp,NRefAlloc)
      call CopyVekI(ICullAr,ICullArP,NRefAlloc)
      NRefAllocp=NRefAlloc
      NRefAlloc=NRefAlloc+n
      deallocate(ihar,riar,rsar,ICullAr)
      allocate(ihar(NDim(KPhase),NRefAlloc),riar(NRefAlloc),
     1         rsar(NRefAlloc),ICullAr(NRefAlloc))
      call CopyVekI(iharp,ihar,NRefAllocp*NDim(KPhase))
      call CopyVek (riarp,riar,NRefAllocp)
      call CopyVek (rsarp,rsar,NRefAllocp)
      call CopyVekI(ICullArP,ICullAr,NRefAllocp)
      deallocate(iharp,riarp,rsarp)
      return
      end
      subroutine ReallocateDRAve(NAvePlus,NAveRedPlus)
      use Datred_mod
      dimension RIAveArO(:),RSAveArO(:),DiffAveArO(:)
      integer HCondAveArO(:),Ave2OrgO(:,:),NAveRedArO(:)
      allocatable RIAveArO,RSAveArO,HCondAveArO,Ave2OrgO,NAveRedArO,
     1            DiffAveArO
      if(NAvePlus.le.0.and.NAveRedPlus.le.0) go to 9999
      if(NAve.gt.0) then
        n=NAve
        m=NAveRed
        allocate(HCondAveArO(n),RIAveArO(n),RSAveArO(n),NAveRedArO(n),
     1           DiffAveArO(n),Ave2OrgO(m,n))
        call CopyVekI(HCondAveAr,HCondAveArO,n)
        call CopyVekI(NAveRedAr,NAveRedArO,n)
        call CopyVek(RIAveAr,RIAveArO,n)
        call CopyVek(RSAveAr,RSAveArO,n)
        call CopyVek(DiffAveAr,DiffAveArO,n)
        do i=1,n
          call CopyVekI(Ave2Org(1,i),Ave2OrgO(1,i),NAveRedAr(i))
        enddo
      endif
      if(allocated(HCondAveAr))
     1  deallocate(HCondAveAr,RIAveAr,RSAveAr,NAveRedAr,DiffAveAr,
     2             Ave2Org)
      n=NAveMax+NAvePlus
      m=NAveRedMax+NAveRedPlus
      allocate(HCondAveAr(n),RIAveAr(n),RSAveAr(n),NAveRedAr(n),
     1         DiffAveAr(n),Ave2Org(m,n))
      if(NAve.gt.0) then
        nn=NAve
        call CopyVekI(HCondAveArO,HCondAveAr,nn)
        call CopyVekI(NAveRedArO,NAveRedAr,nn)
        call CopyVek(RIAveArO,RIAveAr,nn)
        call CopyVek(RSAveArO,RSAveAr,nn)
        call CopyVek(DiffAveArO,DiffAveAr,nn)
        do i=1,nn
          call CopyVekI(Ave2OrgO(1,i),Ave2Org(1,i),NAveRedAr(i))
        enddo
        deallocate(HCondAveArO,RIAveArO,RSAveArO,NAveRedArO,DiffAveArO,
     1             Ave2OrgO)
      endif
      NAveMax=n
      NAveRedMax=m
9999  return
      end
      subroutine ReallocDrPoints(n)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: HDrP(:,:),HDrTwP(:),ColDrP(:)
      real, allocatable :: RIDrP(:),RSDrP(:),XDrP(:),YDrP(:),RadDrP(:)
      allocate(HDrP(NDim(KPhase),NDrAlloc),HDrTwP(NDrAlloc),
     1         RIDrP(NDrAlloc),RSDrP(NDrAlloc),XDrP(NDrAlloc),
     2         YDrP(NDrAlloc),RadDrP(NDrAlloc),ColDrP(NDrAlloc))
      call CopyVekI(HDr,HDrP,NDrAlloc*NDim(KPhase))
      call CopyVekI(HDrTw,HDrTwP,NDrAlloc)
      call CopyVekI(HDr,HDrP,NDrAlloc*NDim(KPhase))
      call CopyVek (RIDr,RIDrP,NDrAlloc)
      call CopyVek (RSDr,RSDrP,NDrAlloc)
      call CopyVek (XDr,XDrP,NDrAlloc)
      call CopyVek (YDr,YDrP,NDrAlloc)
      call CopyVek (RadDr,RadDrP,NDrAlloc)
      call CopyVekI(ColDr,ColDrP,NDrAlloc)
      NDrAllocP=NDrAlloc
      NDrAlloc=NDrAlloc+n
      deallocate(HDr,HDrTw,RIDr,RSDr,XDr,YDr,RadDr,ColDr,PorDr,IXDr)
      allocate(HDr(NDim(NPhase),NDrAlloc),HDrTw(NDrAlloc),
     1         RIDr(NDrAlloc),RSDr(NDrAlloc),XDr(NDrAlloc),
     2         YDr(NDrAlloc),RadDr(NDrAlloc),ColDr(NDrAlloc),
     3         PorDr(NDrAlloc),IXDr(NDrAlloc))
      call CopyVekI(HDrP,HDr,NDrAllocP*NDim(KPhase))
      call CopyVekI(HDrTwP,HDrTw,NDrAllocP)
      call CopyVek (RIDrP,RIDr,NDrAllocP)
      call CopyVek (RSDrP,RSDr,NDrAllocP)
      call CopyVek (XDrP,XDr,NDrAllocP)
      call CopyVek (YDrP,YDr,NDrAllocP)
      call CopyVek (RadDrP,RadDr,NDrAllocP)
      call CopyVekI(ColDrP,ColDr,NDrAllocP)
      deallocate(HDrP,HDrTwP,RIDrP,RSDrP,XDrP,YDrP,RadDrP,ColDrP)
      return
      end
      subroutine ReallocateWdFAr(NRefWdFNew)
      use Datred_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real, allocatable :: FoArO(:),FcArO(:),FsArO(:),WdFArO(:),
     1                     IcArO(:)
      integer, allocatable :: HCondWdFArO(:),FoInd2AveO(:)
      if(NRefWdFNew.le.NRefWdFMax) go to 9999
      if(NRefWdFMax.gt.0) then
        n=NRefWdFMax
        allocate(FoArO(n),FcArO(n),FsArO(n),WdFArO(n),HCondWdFArO(n),
     1           FoInd2AveO(n),IcArO(n))
        call CopyVek (FoAr,FoArO,NRefWdF)
        call CopyVek (FcAr,FcArO,NRefWdF)
        call CopyVek (FsAr,FsArO,NRefWdF)
        call CopyVek (IcAr,IcArO,NRefWdF)
        call CopyVek (WdFAr,WdFArO,NRefWdF)
        call CopyVekI(HCondWdFAr,HCondWdFArO,NRefWdF)
        call CopyVekI(FoInd2Ave,FoInd2AveO,NRefWdF)
      endif
      if(allocated(FoAr)) deallocate(FoAr,FcAr,FsAr,WdFAr,HCondWdFAr,
     1                               FoInd2Ave,IcAr)
      n=NRefWdFNew
      allocate(FoAr(n),FcAr(n),FsAr(n),WdFAr(n),HCondWdFAr(n),
     1         FoInd2Ave(n),IcAr(n))
      if(NRefWdFMax.gt.0) then
        call CopyVek (FoArO,FoAr,NRefWdF)
        call CopyVek (FcArO,FcAr,NRefWdF)
        call CopyVek (IcArO,IcAr,NRefWdF)
        call CopyVek (WdFArO,WdFAr,NRefWdF)
        call CopyVekI(HCondWdFArO,HCondWdFAr,NRefWdF)
        call CopyVekI(HCondWdFArO,HCondWdFAr,NRefWdF)
        call CopyVekI(FoInd2AveO,FoInd2Ave,NRefWdF)
        deallocate(FoArO,FcArO,FsArO,WdFArO,HCondWdFArO,FoInd2AveO,
     1             IcArO)
      endif
      NRefWdFMax=NRefWdFNew
9999  return
      end
      subroutine ReallocDirDef(n)
      use Dist_mod
      real DirRefO(:,:)
      allocatable DirRefO
      if(NDirRef.gt.0) then
        allocate(DirRefO(3,NDirRef))
        call CopyVek(DirRef,DirRefO,3*NDirRef)
      endif
      if(allocated(DirRef)) deallocate(DirRef)
      NDirRefMax=NDirRefMax+n
      allocate(DirRef(3,NDirRefMax))
      if(NDirRef.gt.0) then
        call CopyVek(DirRefO,DirRef,3*NDirRef)
        deallocate(DirRefO)
      endif
      return
      end
      subroutine PwdImportReallocate(nalloc)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension XPwdP(:),YoPwdP(:),YsPwdP(:),YiPwdP(:)
      allocatable XPwdP,YoPwdP,YsPwdP,YiPwdP
      allocate(XPwdP(nalloc),YoPwdP(nalloc),YsPwdP(nalloc),
     1         YiPwdP(nalloc))
      call CopyVek(XPwd,XPwdP,NPnts)
      call CopyVek(YoPwd,YoPwdP,NPnts)
      call CopyVek(YsPwd,YsPwdP,NPnts)
      call CopyVek(YiPwd,YiPwdP,NPnts)
!      nallocp=nalloc
      nalloc=2*nalloc
      deallocate(XPwd,YoPwd,YsPwd,YiPwd)
      allocate(XPwd(nalloc),YoPwd(nalloc),YsPwd(nalloc),YiPwd(nalloc))
      call CopyVek(XPwdP,XPwd,NPnts)
      call CopyVek(YoPwdP,YoPwd,NPnts)
      call CopyVek(YsPwdP,YsPwd,NPnts)
      call CopyVek(YiPwdP,YiPwd,NPnts)
      deallocate(XPwdP,YoPwdP,YsPwdP,YiPwdP)
      return
      end
      subroutine ReallocManBackg(NManBackgNew)
      use Powder_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'powder.cmn'
      dimension   XManBackgO(:,:),YManBackgO(:,:)
      allocatable XManBackgO,YManBackgO
      NManBackgOld=ubound(XManBackg,1)
      if(NManBackgOld.ge.NManBackgNew) go to 9999
      n=NManBackgOld
      allocate(XManBackgO(n,NDatBlock),YManBackgO(n,NDatBlock))
      do i=1,NDatBlock
        do j=1,NManBackg(i)
          XManBackgO(j,i)=XManBackg(j,i)
          YManBackgO(j,i)=YManBackg(j,i)
        enddo
      enddo
      deallocate(XManBackg,YManBackg)
      n=NManBackgNew+100
      allocate(XManBackg(n,NDatBlock),YManBackg(n,NDatBlock))
      do i=1,NDatBlock
        do j=1,NManBackg(i)
          XManBackg(j,i)=XManBackgO(j,i)
          YManBackg(j,i)=YManBackgO(j,i)
        enddo
      enddo
      deallocate(XManBackgO,YManBackgO)
9999  return
      end
      subroutine ReallocExclHKLCommands(n)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer ihmo(:,:),ihmao(:,:),
     1        ihsno(:,:),ieqno(:),imdno(:),
     2        ihsvo(:,:),ieqvo(:),imdvo(:)
      logical DontUseO(:)
      character*20 SkupinaO(:),NevzitO(:),VzitO(:)
      allocatable ihmo,ihmao,ihsno,ihsvo,ieqno,imdno,ieqvo,
     1            imdvo,SkupinaO,NevzitO,VzitO,DontUseO
      if(NSkrt.gt.0) then
        allocate(ihmo(36,NSkrt),ihmao(6,NSkrt),ihsno(6,NSkrt),
     1           ihsvo(6,NSkrt),
     2           ieqno(NSkrt),imdno(NSkrt),ieqvo(NSkrt),
     3           imdvo(NSkrt),SkupinaO(NSkrt),NevzitO(NSkrt),
     4           VzitO(NSkrt),DontUseO(NSkrt))
        do i=1,NSkrt
          call CopyVekI(ihm (1,i),ihmo (1,i),MaxNDim**2)
          call CopyVekI(ihma(1,i),ihmao(1,i),MaxNDim)
          call CopyVekI(ihsn(1,i),ihsno(1,i),MaxNDim)
          call CopyVekI(ihsv(1,i),ihsvo(1,i),MaxNDim)
          ieqno(i)=ieqn(i)
          imdno(i)=imdn(i)
          ieqvo(i)=ieqv(i)
          imdvo(i)=imdv(i)
          SkupinaO(i)=Skupina(i)
          NevzitO(i)=Nevzit(i)
          VzitO(i)=Vzit(i)
          DontUseO(i)=DontUse(i)
        enddo
      endif
      if(allocated(ihm))
     1  deallocate(ihm,ihma,ihsn,ihsv,ieqn,imdn,ieqv,
     2             imdv,Skupina,Nevzit,Vzit,DontUse)
      allocate(ihm(36,n),ihma(6,n),ihsn(6,n),ihsv(6,n),
     1         ieqn(n),imdn(n),ieqv(n),imdv(n),Skupina(n),
     2         Nevzit(n),Vzit(n),DontUse(n))
      if(NSkrt.gt.0) then
        do i=1,NSkrt
          call CopyVekI(ihmo (1,i),ihm (1,i),MaxNDim**2)
          call CopyVekI(ihmao(1,i),ihma(1,i),MaxNDim)
          call CopyVekI(ihsno(1,i),ihsn(1,i),MaxNDim)
          call CopyVekI(ihsvo(1,i),ihsv(1,i),MaxNDim)
          ieqn(i)=ieqno(i)
          imdn(i)=imdno(i)
          ieqv(i)=ieqvo(i)
          imdv(i)=imdvo(i)
          Skupina(i)=SkupinaO(i)
          Nevzit(i)=NevzitO(I)
          Vzit(i)=VzitO(I)
          DontUse(i)=DontUseO(i)
        enddo
        deallocate(ihmo,ihmao,ihsno,ihsvo,ieqno,imdno,
     1             ieqvo,imdvo,SkupinaO,NevzitO,VzitO,DontUseO)
      endif
      NSkrtMax=n
      return
      end
      subroutine ReallocScaleCommands(n)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: iqso(:),ihmso (:,:),ihssvo(:,:),ieqsvo(:),
     1                        imdsvo(:),ihmaso(:,:),ihssno(:,:),
     2                        ieqsno(:),imdsno(:)
      character*20, allocatable :: scsko(:),scvzito(:),scnevzito(:)
      if(NScales.gt.0) then
        allocate(iqso(NScales),ihmso(36,NScales),ihmaso(6,NScales),
     1           ihssvo(6,NScales),ieqsvo(NScales),imdsvo(NScales),
     2           ihssno(6,NScales),ieqsno(NScales),imdsno(NScales),
     2           scsko(NScales),scvzito(NScales),scnevzito(NScales))
        do i=1,NScales
          iqso(i)=iqs(i)
          scsko(i)=scsk(i)
          scvzito(i)=scvzit(i)
          scnevzito(i)=scnevzit(i)
          call CopyVekI(ihms(1,i),ihmso(1,i),MaxNDim**2)
          call CopyVekI(ihmas(1,i),ihmaso(1,i),MaxNDim)
          call CopyVekI(ihssv(1,i),ihssvo(1,i),MaxNDim)
          ieqsvo(i)=ieqsv(i)
          imdsvo(i)=imdsv(i)
          call CopyVekI(ihssn(1,i),ihssno(1,i),MaxNDim)
          ieqsno(i)=ieqsn(i)
          imdsno(i)=imdsn(i)
        enddo
      endif
      if(allocated(iqs)) deallocate(iqs,ihms,ihmas,
     1                              ihssv,ieqsv,imdsv,
     2                              ihssn,ieqsn,imdsn,
     3                              scsk,scvzit,scnevzit)
      allocate(iqs(n),ihms(36,n),ihmas(6,n),
     1         ihssv(6,n),ieqsv(n),imdsv(n),
     1         ihssn(6,n),ieqsn(n),imdsn(n),
     3         scsk(n),scvzit(n),scnevzit(n))
      if(NScales.gt.0) then
        do i=1,NScales
          iqs(i)=iqso(i)
          scsk(i)=scsko(i)
          scvzit(i)=scvzito(i)
          scnevzit(i)=scnevzito(i)
          call CopyVekI(ihmso(1,i),ihms(1,i),MaxNDim**2)
          call CopyVekI(ihmaso(1,i),ihmas(1,i),MaxNDim)
          call CopyVekI(ihssvo(1,i),ihssv(1,i),MaxNDim)
          ieqsv(i)=ieqsvo(i)
          imdsv(i)=imdsvo(i)
          call CopyVekI(ihssno(1,i),ihssn(1,i),MaxNDim)
          ieqsn(i)=ieqsno(i)
          imdsn(i)=imdsno(i)
        enddo
        deallocate(iqso,ihmso,ihmaso,
     1             ihssvo,ieqsvo,imdsvo,
     2             ihssno,ieqsno,imdsno,
     3             scsko,scvzito,scnevzito)
      endif
      NScalesMax=n
      return
      end
      subroutine ReallocRFactorsCommands(n)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: RFactMatrixO(:,:),RFactVectorO(:,:),
     1                        CondRFactInclO(:,:),AbsRFactInclO(:),
     2                        ModRFactInclO(:),CondRFactExclO(:,:),
     3                        AbsRFactExclO(:),ModRFactExclO(:)
      character*20, allocatable :: StRFactO(:),StRFactInclO(:),
     1                             StRFactExclO(:)
      if(NRFactors.gt.0) then
        allocate(RFactMatrixO(36,NRFactors),RFactVectorO(6,NRFactors),
     1           StRFactO(NRfactors),CondRFactInclO(6,NRfactors),
     2           AbsRFactInclO(NRfactors),ModRFactInclO(NRfactors),
     3           StRFactInclO(NRfactors),CondRFactExclO(6,NRfactors),
     4           AbsRFactExclO(NRfactors),ModRFactExclO(NRfactors),
     5           StRFactExclO(NRfactors))
        do i=1,NRfactors
          call CopyVekI(RFactMatrix(1,i),RFactMatrixO(1,i),MaxNDim**2)
          call CopyVekI(RFactVector(1,i),RFactVectorO(1,i),MaxNDim)
          StRFactO(i)=StRFact(i)
          call CopyVekI(CondRFactIncl(1,i),CondRFactInclO(1,i),MaxNDim)
          AbsRFactInclO(i)=AbsRFactIncl(i)
          ModRFactInclO(i)=ModRFactIncl(i)
          StRFactInclO(i)=StRFactIncl(i)
          call CopyVekI(CondRFactIncl(1,i),CondRFactInclO(1,i),MaxNDim)
          AbsRFactExclO(i)=AbsRFactExcl(i)
          ModRFactExclO(i)=ModRFactExcl(i)
          StRFactExclO(i)=StRFactExcl(i)
        enddo
      endif
      if(allocated(RFactMatrix))
     1  deallocate(RFactMatrix,RFactVector,StRFact,
     2             CondRFactIncl,AbsRFactIncl,
     3             ModRFactIncl,StRFactIncl,
     4             CondRFactExcl,AbsRFactExcl,
     5             ModRFactExcl,StRFactExcl)
      if(allocated(nRPartAll))
     1  deallocate(nRPartAll,nRPartObs,
     2              RDenPartAll, RNumPartAll,
     3              RDenPartObs, RNumPartObs,
     4             wRDenPartAll,wRNumPartAll,
     5             wRDenPartObs,wRNumPartObs)
      allocate(RFactMatrix(36,n),RFactVector(6,n),StRFact(n),
     1         CondRFactIncl(6,n),AbsRFactIncl(n),
     2         ModRFactIncl(n),StRFactIncl(n),
     3         CondRFactExcl(6,n),AbsRFactExcl(n),
     4         ModRFactExcl(n),StRFactExcl(n))
      allocate(nRPartAll(n),nRPartObs(n),
     1          RDenPartAll(n), RNumPartAll(n),
     2          RDenPartObs(n), RNumPartObs(n),
     3         wRDenPartAll(n),wRNumPartAll(n),
     4         wRDenPartObs(n),wRNumPartObs(n))
      if(NRfactors.gt.0) then
        do i=1,NRfactors
          call CopyVekI(RFactMatrixO(1,i),RFactMatrix(1,i),MaxNDim**2)
          call CopyVekI(RFactVectorO(1,i),RFactVector(1,i),MaxNDim)
          StRFact(i)=StRFactO(i)
          call CopyVekI(CondRFactInclO(1,i),CondRFactIncl(1,i),MaxNDim)
          AbsRFactIncl(i)=AbsRFactInclO(i)
          ModRFactIncl(i)=ModRFactInclO(i)
          StRFactIncl(i)=StRFactInclO(i)
          call CopyVekI(CondRFactInclO(1,i),CondRFactIncl(1,i),MaxNDim)
          AbsRFactExcl(i)=AbsRFactExclO(i)
          ModRFactExcl(i)=ModRFactExclO(i)
          StRFactExcl(i)=StRFactExclO(i)
        enddo
        deallocate(RFactMatrixO,RFactVectorO,StRFactO,
     1             CondRFactInclO,AbsRFactInclO,
     2             ModRFactInclO,StRFactInclO,
     3             CondRFactExclO,AbsRFactExclO,
     4             ModRFactExclO,StRFactExclO)
      endif
      NRfactorsMax=n
      return
      end
      subroutine ReallocKeepCommands(n,m)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'refine.cmn'
      character*80 KeepAtO(:,:),KeepAtCentrO(:),KeepAtNeighO(:,:),
     1             KeepAtHO(:,:),KeepAtAnchorO(:)
      integer KeepTypeO(:),KeepNO(:),KeepNAtO(:,:),KeepKiXO(:,:),
     1        KeepNAtCentrO(:),KeepKiXCentrO(:),
     3        KeepNNeighO(:),KeepNAtNeighO(:,:),KeepKiXNeighO(:,:),
     4        KeepNHO(:),KeepNAtHO(:,:),KeepKiXHO(:,:),
     5        KeepNAtAnchorO(:),KeepKiXAnchorO(:)
      real KeepDistHO(:),KeepAngleHO(:),KeepADPExtFacO(:)
      allocatable KeepAtO,KeepAtCentrO,KeepAtNeighO,KeepAtHO,
     1            KeepAtAnchorO,KeepTypeO,KeepNO,KeepNAtO,KeepKiXO,
     2            KeepNAtCentrO,KeepKiXCentrO,KeepNNeighO,KeepNAtNeighO,
     3            KeepKiXNeighO,KeepNHO,KeepNAtHO,KeepKiXHO,
     4            KeepNAtAnchorO,KeepKiXAnchorO,KeepDistHO,KeepAngleHO,
     5            KeepADPExtFacO
      if(NKeep.gt.0) then
        allocate(KeepTypeO(NKeep),KeepNO(NKeep),
     1           KeepNAtO(NKeep,NKeepAtMax),
     1           KeepAtO(NKeep,NKeepAtMax),KeepKiXO(NKeep,NKeepAtMax),
     2           KeepNAtCentrO(NKeep),KeepKiXCentrO(NKeep),
     3           KeepAtCentrO(NKeep),
     4           KeepNNeighO(NKeep),KeepNAtNeighO(NKeep,5),
     5           KeepAtNeighO(NKeep,5),
     6           KeepKiXNeighO(NKeep,5),
     7           KeepNHO(NKeep),KeepNAtHO(NKeep,4),
     8           KeepAtHO(NKeep,4),
     9           KeepKiXHO(NKeep,4),
     a           KeepNAtAnchorO(NKeep),KeepKiXAnchorO(NKeep),
     1           KeepAtAnchorO(NKeep),
     2           KeepDistHO(NKeep),KeepAngleHO(NKeep),
     9           KeepADPExtFacO(NKeep))
        do i=1,NKeep
          KeepTypeO(i)=KeepType(i)
          k=KeepType(i)/10
          KeepNAtCentrO(i)=KeepNAtCentr(i)
          if(k.eq.IdKeepHydro.or.k.eq.IdKeepADP) then
            KeepAtCentrO(i)=KeepAtCentr(i)
            if(k.eq.IdKeepHydro) then
              KeepKiXCentrO(i)=KeepKiXCentr(i)
              KeepNNeighO(i)=KeepNNeigh(i)
              do j=1,KeepNNeigh(i)
                KeepNAtNeighO(i,j)=KeepNAtNeigh(i,j)
                KeepKiXNeighO(i,j)=KeepKiXNeigh(i,j)
                KeepAtNeighO(i,j)=KeepAtNeigh(i,j)
              enddo
              KeepNAtAnchorO(i)=KeepNAtAnchor(i)
              KeepAtAnchorO(i)=KeepAtAnchor(i)
              KeepKiXAnchorO(i)=KeepKiXAnchor(i)
              KeepDistHO(i)=KeepDistH(i)
              KeepAngleHO(i)=KeepAngleH(i)
            else
              KeepADPExtFacO(i)=KeepADPExtFac(i)
            endif
            KeepNHO(i)=KeepNH(i)
            do j=1,KeepNH(i)
              KeepNAtHO(i,j)=KeepNAtH(i,j)
              KeepAtHO(i,j)=KeepAtH(i,j)
              if(k.eq.IdKeepHydro) KeepKiXHO(i,j)=KeepKiXH(i,j)
            enddo
          else
            KeepNO(i)=KeepN(i)
            do j=1,KeepN(i)
              KeepNAtO(i,j)=KeepNAt(i,j)
              KeepAtO(i,j)=KeepAt(i,j)
              KeepKiXO(i,j)=KeepKiX(i,j)
            enddo
          endif
        enddo
      endif
      if(allocated(KeepType))
     1  deallocate(KeepType,KeepN,KeepNAt,KeepAt,KeepKiX,KeepNAtCentr,
     2             KeepAtCentr,KeepKiXCentr,KeepNNeigh,KeepNAtNeigh,
     3             KeepAtNeigh,KeepKiXNeigh,KeepNH,KeepNAtH,KeepAtH,
     4             KeepKiXH,KeepNAtAnchor,KeepAtAnchor,KeepKiXAnchor,
     5             KeepDistH,KeepAngleH,KeepADPExtFac)
      allocate(KeepType(n),KeepN(n),KeepNAt(n,m),KeepAt(n,m),
     1         KeepKiX(n,m),KeepNAtCentr(n),KeepAtCentr(n),
     2         KeepKiXCentr(n),KeepNNeigh(n),KeepNAtNeigh(n,5),
     3         KeepAtNeigh(n,5),KeepKiXNeigh(n,5),KeepNH(n),
     4         KeepNAtH(n,4),KeepAtH(n,4),KeepKiXH(n,4),
     5         KeepNAtAnchor(n),KeepAtAnchor(n),KeepKiXAnchor(n),
     6         KeepDistH(n),KeepAngleH(n),KeepADPExtFac(n))
      if(NKeep.gt.0) then
        do i=1,NKeep
          KeepType(i)=KeepTypeO(i)
          k=KeepTypeO(i)/10
          KeepNAtCentr(i)=KeepNAtCentrO(i)
          if(k.eq.IdKeepHydro.or.k.eq.IdKeepADP) then
            KeepAtCentr(i)=KeepAtCentrO(i)
            if(k.eq.IdKeepHydro) then
              KeepKiXCentr(i)=KeepKiXCentrO(i)
              KeepNNeigh(i)=KeepNNeighO(i)
              do j=1,KeepNNeighO(i)
                KeepNAtNeigh(i,j)=KeepNAtNeighO(i,j)
                KeepAtNeigh(i,j)=KeepAtNeighO(i,j)
                KeepKiXNeigh(i,j)=KeepKiXNeighO(i,j)
              enddo
              KeepNAtAnchor(i)=KeepNAtAnchorO(i)
              KeepAtAnchor(i)=KeepAtAnchorO(i)
              KeepKiXAnchor(i)=KeepKiXAnchorO(i)
              KeepDistH(i)=KeepDistHO(i)
              KeepAngleH(i)=KeepAngleHO(i)
            else
              KeepADPExtFac(i)=KeepADPExtFacO(i)
            endif
            KeepNH(i)=KeepNHO(i)
            do j=1,KeepNHO(i)
              KeepNAtH(i,j)=KeepNAtHO(i,j)
              KeepAtH(i,j)=KeepAtHO(i,j)
              if(k.eq.IdKeepHydro) KeepKiXH(i,j)=KeepKiXHO(i,j)
            enddo
          else
            KeepN(i)=KeepNO(i)
            do j=1,KeepN(i)
              KeepNAt(i,j)=KeepNAtO(i,j)
              KeepAt(i,j)=KeepAtO(i,j)
              KeepKiX(i,j)=KeepKiXO(i,j)
            enddo
          endif
        enddo
        if(allocated(KeepType))
     1    deallocate(KeepTypeO,KeepNO,KeepNAtO,KeepAtO,KeepKiXO,
     2               KeepNAtCentrO,KeepAtCentrO,KeepKiXCentrO,
     3               KeepNNeighO,KeepNAtNeighO,KeepAtNeighO,
     4               KeepKiXNeighO,KeepNHO,KeepNAtHO,KeepAtHO,KeepKiXHO,
     5               KeepNAtAnchorO,KeepAtAnchorO,KeepKiXAnchorO,
     6               KeepDistHO,KeepAngleHO,KeepADPExtFacO)
      endif
      NKeepMax=n
      NKeepAtMax=m
      return
      end
      subroutine ReallocRestricCommands(n)
      use Refine_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      integer, allocatable :: naio(:,:),naixbo(:),maio(:),nailso(:)
      real, allocatable :: sumaio(:)
      character*12, allocatable :: atvaio(:,:),RestTypeO(:)
      if(nvai.gt.0) then
        allocate(naio(NAtCalc,nvai),naixbo(nvai),sumaio(nvai),
     1           maio(nvai),atvaio(NAtCalc,nvai),nailso(nvai),
     2           RestTypeO(nvai))
        call CopyVekI(naixb,naixbo,nvai)
        call CopyVekI(mai,maio,nvai)
        call CopyVekI(nails,nailso,nvai)
        call CopyVek(sumai,sumaio,nvai)
        do i=1,nvai
          RestTypeO(i)=RestType(i)
          do j=1,mai(i)
            naio(j,i)=nai(j,i)
            atvaio(j,i)=atvai(j,i)
          enddo
        enddo
      endif
      if(allocated(nai)) deallocate(nai,naixb,sumai,mai,atvai,nails,
     1                              RestType)
      m=max(NAtCalc,NPhase)
      allocate(nai(m,n),naixb(n),sumai(n),mai(n),atvai(m,n),nails(n),
     1         RestType(n))
      if(nvai.gt.0) then
        call CopyVekI(naixbo,naixb,nvai)
        call CopyVekI(maio,mai,nvai)
        call CopyVek(sumaio,sumai,nvai)
        call CopyVekI(nailso,nails,nvai)
        do i=1,nvai
          RestType(i)=RestTypeO(i)
          do j=1,maio(i)
            nai(j,i)=naio(j,i)
            atvai(j,i)=atvaio(j,i)
          enddo
        enddo
        deallocate(naio,naixbo,sumaio,maio,atvaio,nailso,RestTypeO)
      endif
      nvaiMax=n
      return
      end
!      subroutine ReallocRestricCommands(n)
!      use Refine_mod
!      include 'fepc.cmn'
!      include 'basic.cmn'
!      integer, allocatable :: naio(:,:),naixbo(:),maio(:),nailso(:)
!      real, allocatable :: sumaio(:),TLocSymmO(:,:)
!      character*12, allocatable :: atvaio(:,:),RestTypeO(:)
!      character*80, allocatable :: StLocSymmO(:)
!      if(nvai.gt.0) then
!        allocate(naio(NAtCalc,nvai),naixbo(nvai),sumaio(nvai),
!     1           maio(nvai),atvaio(NAtCalc,nvai),nailso(nvai),
!     2           RestTypeO(nvai),TLocSymmO(6,nvai),StLocSymmO(nvai))
!        call CopyVekI(naixb,naixbo,nvai)
!        call CopyVekI(mai,maio,nvai)
!        call CopyVekI(nails,nailso,nvai)
!        call CopyVek(sumai,sumaio,nvai)
!        do i=1,nvai
!          StLocSymmO(i)=StLocSymm(i)
!          RestTypeO(i)=RestType(i)
!          TLocSymmO(1:6,i)=TLocSymm(1:6,i)
!          do j=1,mai(i)
!            naio(j,i)=nai(j,i)
!            atvaio(j,i)=atvai(j,i)
!          enddo
!        enddo
!      endif
!      if(allocated(nai)) deallocate(nai,naixb,sumai,mai,atvai,nails,
!     1                              RestType,TLocSymm,StLocSymm)
!      m=max(NAtCalc,NPhase)
!      allocate(nai(m,n),naixb(n),sumai(n),mai(n),atvai(m,n),nails(n),
!     1         RestType(n),TLocSymm(6,n),StLocSymm(n))
!      if(nvai.gt.0) then
!        call CopyVekI(naixbo,naixb,nvai)
!        call CopyVekI(maio,mai,nvai)
!        call CopyVek(sumaio,sumai,nvai)
!        call CopyVekI(nailso,nails,nvai)
!        do i=1,nvai
!          StLocSymm(i)=StLocSymmO(i)
!          RestType(i)=RestTypeO(i)
!          TLocSymm(1:6,i)=TLocSymmO(1:6,i)
!          do j=1,maio(i)
!            nai(j,i)=naio(j,i)
!            atvai(j,i)=atvaio(j,i)
!          enddo
!        enddo
!        deallocate(naio,naixbo,sumaio,maio,atvaio,nailso,RestTypeO,
!     1             TLocSymmO,StLocSymmO)
!      endif
!      nvaiMax=n
!      return
!      end
      subroutine AllocateAtoms(n)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(LocAtSystSt))
     1  deallocate(LocAtSystSt,SmbPGAt,TrAt,TriAt,TroAt,TroiAt,
     2             LocAtSystAx,LocAtSense,kapa1,kapa2,skapa1,skapa2,
     3             popc,spopc,popv,spopv,popas,spopas,NPGAt,RPGAt,
     4             LocAtSystX)
      if(ChargeDensities) then
        allocate(LocAtSystSt(2,n),SmbPGAt(n),TrAt(9,n),TriAt(9,n),
     1           TrOAt(9,n),TrOiAt(9,n),
     2           LocAtSystAx(n),LocAtSense(n),kapa1(n),kapa2(n),
     3           skapa1(n),skapa2(n),popc(n),spopc(n),popv(n),spopv(n),
     4           popas(64,n),spopas(64,n),NPGAt(n),RPGAt(9,48,n),
     5           LocAtSystX(3,2,n))
        mxda=78
      else
        mxda=10
      endif
      if(allocated(Atom))
     1  deallocate(Atom,isf,itf,lasmax,ifr,kmol,iswa,kswa,kfa,kmoda,
     2             kmodao,MagPar,TypeModFun,KUsePolar,
     3             isa,WyckoffMult,WyckoffSmb,PrvniKiAtomu,
     4             DelkaKiAtomu,KiA,ai,sai,a0,sa0,x,sx,beta,sbeta,
     5             AtSiteMult,xfr,sxfr)
      allocate(Atom(n),isf(n),itf(n),lasmax(n),ifr(n),kmol(n),
     1         iswa(n),kswa(n),KFA(7,n),KModA(7,n),kmodao(7,n),
     2         MagPar(n),TypeModFun(n),KUsePolar(n),
     3         isa(MaxNSymm,n),WyckoffMult(n),WyckoffSmb(n),
     4         PrvniKiAtomu(n),DelkaKiAtomu(n),KiA(mxda,n),
     5         ai(n),sai(n),a0(n),sa0(n),x(3,n),sx(3,n),beta(6,n),
     6         sbeta(6,n),AtSiteMult(n),xfr(n),sxfr(n))
      if(allocated(qcnt)) deallocate(qcnt,phf,sphf,OrthoX40,OrthoDelta,
     1                               OrthoEps)
      if(MaxNDimI.gt.0)
     1  allocate(qcnt(3,n),phf(n),sphf(n),OrthoX40(n),OrthoDelta(n),
     2           OrthoEps(n))
      if(allocated(NamePolar)) deallocate(NamePolar,sm0,ssm0)
      if(MaxMagneticType.gt.0) allocate(NamePolar(n),sm0(3,n),ssm0(3,n))
      if(allocated(tztl)) deallocate(tztl,tzts,durdr)
      if(NMolec.gt.0) allocate(tztl(36,n),tzts(54,n),durdr(9,n))
      if(allocated(ax)) deallocate(ax,ay,sax,say)
      if(allocated(ux)) deallocate(ux,uy,sux,suy)
      if(allocated(bx)) deallocate(bx,by,sbx,sby)
      if(allocated(c3x)) deallocate(c3x,c3y,sc3x,sc3y)
      if(allocated(c4x)) deallocate(c4x,c4y,sc4x,sc4y)
      if(allocated(c5x)) deallocate(c5x,c5y,sc5x,sc5y)
      if(allocated(c6x)) deallocate(c6x,c6y,sc6x,sc6y)
      MxAtInd=n
      MxAtAll=n
      NAtAllocMod=0
      return
      end
      subroutine ReallocateAtoms(NAtPlus)
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
c
c   Musi byt alokovany vzdy
c
      character*8  AtomOld(:)
      allocatable AtomOld
      integer isfOld(:),itfOld(:),ifrOld(:),kmolOld(:),lasmaxOld(:),
     1        iswaOld(:),kswaOld(:),kfaOld(:,:),kmodaOld(:,:),
     2        kmodaoOld(:,:),MagParOld(:),TypeModFunOld(:),
     3        KUsePolarOld(:),isaOld(:,:),AtSiteMultOld(:),
     4        PrvniKiAtomuOld(:),DelkaKiAtomuOld(:),KiAOld(:,:),
     5        NPGAtOld(:)
      real    aiOld(:),saiOld(:),a0Old(:),sa0Old(:),xOld(:,:),
     1        sxOld(:,:),betaOld(:,:),sbetaOld(:,:),durdrOld(:,:),
     2        xfrOld(:),sxfrOld(:),RPGAtOld(:,:,:)
      allocatable isfOld,itfOld,lasmaxOld,ifrOld,kmolOld,iswaOld,
     1            kswaOld,kfaOld,kmodaOld,kmodaoOld,MagParOld,
     2            isaOld,AtSiteMultOld,PrvniKiAtomuOld,DelkaKiAtomuOld,
     3            KiAOld,aiOld,saiOld,a0Old,sa0Old,xOld,sxOld,betaOld,
     4            sbetaOld,TypeModFunOld,KUsePolarOld,durdrOld,
     5            xfrOld,sxfrOld,NPGAtOld,RPGAtOld
c
c  Musi byt alokovany pro multipolove upresnovani
c
      character*27 LocAtSystStOld(:,:)
      character*8  SmbPGAtOld(:)
      character*2  LocAtSystAxOld(:)
      character*1  LocAtSenseOld(:)
      real TrAtOld(:,:),TriAtOld(:,:),TroAtOld(:,:),TroiAtOld(:,:),
     1     kapa1Old(:),kapa2Old(:),skapa1Old(:),skapa2Old(:),popcOld(:),
     2     spopcOld(:),popvOld(:),spopvOld(:),popasOld(:,:),
     3     spopasOld(:,:),LocAtSystXOld(:,:,:)
      allocatable LocAtSystStOld,LocAtSystAxOld,LocAtSenseOld,
     1            SmbPGAtOld,TrAtOld,TriAtOld,TroAtOld,TroiAtOld,
     2            kapa1Old,kapa2Old,skapa1Old,skapa2Old,popcOld,
     3            spopcOld,popvOld,spopvOld,popasOld,spopasOld,
     4            LocAtSystXOld
c
c  Musi byt alokovany pro modulovane struktury
c
      real qcntOld(:,:),phfOld(:),sphfOld(:),OrthoX40Old(:),
     1     OrthoDeltaOld(:),OrthoEpsOld(:)
      allocatable qcntOld,phfOld,sphfOld,OrthoX40Old,OrthoDeltaOld,
     1            OrthoEpsOld
c
c  Musi byt alokovany pro magneticke struktury
c
      character*8 NamePolarOld(:)
      real sm0Old(:,:),ssm0Old(:,:)
      allocatable NamePolarOld,sm0Old,ssm0Old
      real p0(:,:),sp0(:,:),px(:,:,:),py(:,:,:),spx(:,:,:),spy(:,:,:)
      allocatable p0,sp0,px,py,spx,spy
      n=NAtAll
      allocate(AtomOld(n),isfOld(n),itfOld(n),ifrOld(n),kmolOld(n),
     1         lasmaxOld(n),iswaOld(n),kswaOld(n),kfaOld(7,n),
     2         kmodaOld(7,n),kmodaoOld(7,n),MagParOld(n),
     3         KUsePolarOld(n),TypeModFunOld(n),
     3         isaOld(MaxNSymm,n),AtSiteMultOld(n),PrvniKiAtomuOld(n),
     4         DelkaKiAtomuOld(n),KiAOld(mxda,n),
     5         aiOld(n),saiOld(n),a0Old(n),sa0Old(n),xOld(3,n),
     6         sxOld(3,n),betaOld(6,n),sbetaOld(6,n),durdrOld(9,n),
     7         xfrOld(n),sxfrOld(n))
      mw=0
      do i=1,7
        mw=max(mw,KModAMax(i))
      enddo
      do ia=1,NAtAll
        if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
        AtomOld(ia)=Atom(ia)
        isfOld(ia)=isf(ia)
        itfOld(ia)=itf(ia)
        ifrOld(ia)=ifr(ia)
        kmolOld(ia)=kmol(ia)
        lasmaxOld(ia)=lasmax(ia)
        iswaOld(ia)=iswa(ia)
        kswaOld(ia)=kswa(ia)
        MagParOld(ia)=MagPar(ia)
        KUsePolarOld(ia)=KUsePolar(ia)
        TypeModFunOld(ia)=TypeModFun(ia)
        AtSiteMultOld(ia)=AtSiteMult(ia)
        PrvniKiAtomuOld(ia)=PrvniKiAtomu(ia)
        DelkaKiAtomuOld(ia)=DelkaKiAtomu(ia)
        aiOld(ia)=ai(ia)
        saiOld(ia)=sai(ia)
        a0Old(ia)=a0(ia)
        sa0Old(ia)=sa0(ia)
        xfrOld(ia)=xfr(ia)
        sxfrOld(ia)=sxfr(ia)
        call CopyVekI(kfa(1,ia),kfaOld(1,ia),7)
        call CopyVekI(kmoda(1,ia),kmodaOld(1,ia),7)
        call CopyVekI(kmodao(1,ia),kmodaoOld(1,ia),7)
        call CopyVekI(isa(1,ia),isaOld(1,ia),MaxNSymm)
        call CopyVekI(KiA(1,ia),KiAOld(1,ia),mxda)
        call CopyVek( x(1,ia), xOld(1,ia),3)
        call CopyVek(sx(1,ia),sxOld(1,ia),3)
        call CopyVek( beta(1,ia), betaOld(1,ia),6)
        call CopyVek(sbeta(1,ia),sbetaOld(1,ia),6)
        if(NMolec.gt.0) then
          if(allocated(durdr)) then
            call CopyVek(durdr(1,ia),durdrOld(1,ia),9)
          else
            call SetRealArrayTo(durdrOld(1,ia),9,0.)
          endif
        endif
      enddo
      deallocate(Atom,isf,itf,lasmax,ifr,kmol,iswa,kswa,kfa,kmoda,
     1           kmodao,MagPar,TypeModFun,KUsePolar,
     2           isa,WyckoffMult,WyckoffSmb,PrvniKiAtomu,
     3           DelkaKiAtomu,KiA,ai,sai,a0,sa0,x,sx,beta,sbeta,
     4           AtSiteMult,xfr,sxfr)
      n=MxAtAll+NAtPlus
      allocate(Atom(n),isf(n),itf(n),lasmax(n),ifr(n),kmol(n),
     1         iswa(n),kswa(n),KFA(7,n),KModA(7,n),kmodao(7,n),
     2         MagPar(n),TypeModFun(n),KUsePolar(n),
     3         isa(MaxNSymm,n),WyckoffMult(n),WyckoffSmb(n),
     4         PrvniKiAtomu(n),DelkaKiAtomu(n),KiA(mxda,n),
     5         ai(n),sai(n),a0(n),sa0(n),x(3,n),sx(3,n),beta(6,n),
     6         sbeta(6,n),AtSiteMult(n),xfr(n),sxfr(n))
      do ia=1,NAtAll
        if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
        Atom(ia)=AtomOld(ia)
        isf(ia)=isfOld(ia)
        itf(ia)=itfOld(ia)
        ifr(ia)=ifrOld(ia)
        kmol(ia)=kmolOld(ia)
        lasmax(ia)=lasmaxOld(ia)
        iswa(ia)=iswaOld(ia)
        kswa(ia)=kswaOld(ia)
        MagPar(ia)=MagParOld(ia)
        KUsePolar(ia)=KUsePolarOld(ia)
        TypeModFun(ia)=TypeModFunOld(ia)
        AtSiteMult(ia)=AtSiteMultOld(ia)
        PrvniKiAtomu(ia)=PrvniKiAtomuOld(ia)
        DelkaKiAtomu(ia)=DelkaKiAtomuOld(ia)
        ai(ia)=aiOld(ia)
        sai(ia)=saiOld(ia)
        a0(ia)=a0Old(ia)
        sa0(ia)=sa0Old(ia)
        xfr(ia)=xfrOld(ia)
        sxfr(ia)=sxfrOld(ia)
        call CopyVekI(kfaOld(1,ia),kfa(1,ia),7)
        call CopyVekI(kmodaOld(1,ia),kmoda(1,ia),7)
        call CopyVekI(kmodaoOld(1,ia),kmodao(1,ia),7)
        call CopyVekI(isaOld(1,ia),isa(1,ia),MaxNSymm)
        call CopyVekI(KiAOld(1,ia),KiA(1,ia),mxda)
        call CopyVek( xOld(1,ia), x(1,ia),3)
        call CopyVek(sxOld(1,ia),sx(1,ia),3)
        call CopyVek( betaOld(1,ia), beta(1,ia),6)
        call CopyVek(sbetaOld(1,ia),sbeta(1,ia),6)
      enddo
      if(NMolec.gt.0) then
        if(allocated(tztl)) deallocate(tztl,tzts,durdr)
        allocate(tztl(36,n),tzts(54,n),durdr(9,n))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          call CopyVek(durdrOld(1,ia),durdr(1,ia),9)
        enddo
      endif
      deallocate(AtomOld,isfOld,itfOld,ifrOld,kmolOld,lasmaxOld,
     1           iswaOld,kswaOld,kfaOld,kmodaOld,kmodaoOld,
     2           MagParOld,KUsePolarOld,TypeModFunOld,isaOld,
     3           AtSiteMultOld,PrvniKiAtomuOld,DelkaKiAtomuOld,KiAOld,
     4           aiOld,saiOld,a0Old,sa0Old,xOld,sxOld,betaOld,sbetaOld,
     5           durdrOld,xfrOld,sxfrOld)
      if(ChargeDensities) then
        n=NAtAll
        allocate(LocAtSystStOld(2,n),SmbPGAtOld(n),TrAtOld(9,n),
     1           TriAtOld(9,n),TroAtOld(9,n),TroiAtOld(9,n),
     2           LocAtSystAxOld(n),LocAtSenseOld(n),
     3           kapa1Old(n),kapa2Old(n),skapa1Old(n),skapa2Old(n),
     4           popcOld(n),spopcOld(n),popvOld(n),spopvOld(n),
     5           popasOld(64,n),spopasOld(64,n),NPGAtOld(n),
     6           RPGAtOld(9,48,n),LocAtSystXOld(3,2,n))
        do ia=1,NAtInd
          NPGAtOld(ia)=NPGAt(ia)
          do j=1,NPGAt(ia)
            call CopyMat(RPGAt(1,j,ia),RPGAtOld(1,j,ia),3)
          enddo
          call CopyMat(TrAt(1,ia),TrAtOld(1,ia),3)
          call CopyMat(TriAt(1,ia),TriAtOld(1,ia),3)
          call CopyMat(TroAt(1,ia),TroAtOld(1,ia),3)
          call CopyMat(TroiAt(1,ia),TroiAtOld(1,ia),3)
          kk=lasmax(ia)
          if(kk.le.0) then
            cycle
          else
            LocAtSystStOld(1,ia)=LocAtSystSt(1,ia)
            LocAtSystStOld(2,ia)=LocAtSystSt(2,ia)
            LocAtSystAxOld(ia)=LocAtSystAx(ia)
            LocAtSenseOld(ia)=LocAtSense(ia)
            SmbPGAtOld(ia)=SmbPGAt(ia)
            call CopyVek(LocAtSystX(1,1,ia),LocAtSystXOld(1,1,ia),6)
            popcOld(ia)=popc(ia)
            spopcOld(ia)=spopc(ia)
            popvOld(ia)=popv(ia)
            spopvOld(ia)=spopv(ia)
            kapa1Old(ia)=kapa1(ia)
            skapa1Old(ia)=skapa1(ia)
            call CopyVek( popas(1,ia), popasOld(1,ia),(kk-1)**2)
            call CopyVek(spopas(1,ia),spopasOld(1,ia),(kk-1)**2)
            kapa2Old(ia)=kapa2(ia)
            skapa2Old(ia)=skapa2(ia)
          endif
        enddo
        deallocate(LocAtSystSt,SmbPGAt,TrAt,TriAt,TroAt,TroiAt,
     1             LocAtSystAx,LocAtSense,kapa1,kapa2,skapa1,skapa2,
     2             popc,spopc,popv,spopv,popas,spopas,NPGAt,RPGAt,
     3             LocAtSystX)
        n=NAtAll+NAtPlus
        allocate(LocAtSystSt(2,n),SmbPGAt(n),TrAt(9,n),TriAt(9,n),
     1           TroAt(9,n),TroiAt(9,n),
     2           LocAtSystAx(n),LocAtSense(n),kapa1(n),kapa2(n),
     3           skapa1(n),skapa2(n),popc(n),spopc(n),popv(n),spopv(n),
     4           popas(64,n),spopas(64,n),NPGAt(n),RPGAt(9,48,n),
     5           LocAtSystX(3,2,n))
        do ia=1,NAtInd
          NPGAt(ia)=NPGAtOld(ia)
          do j=1,NPGAt(ia)
            call CopyMat(RPGAtOld(1,j,ia),RPGAt(1,j,ia),3)
          enddo
          call CopyMat(TrAtOld(1,ia),TrAt(1,ia),3)
          call CopyMat(TriAtOld(1,ia),TriAt(1,ia),3)
          call CopyMat(TroAtOld(1,ia),TroAt(1,ia),3)
          call CopyMat(TroiAtOld(1,ia),TroiAt(1,ia),3)
          kk=lasmax(ia)
          if(kk.le.0) then
            cycle
          else
            popc(ia)=popcOld(ia)
            spopc(ia)=spopcOld(ia)
            popv(ia)=popvOld(ia)
            spopv(ia)=spopvOld(ia)
            kapa1(ia)=kapa1Old(ia)
            skapa1(ia)=skapa1Old(ia)
            call CopyVek( popasOld(1,ia), popas(1,ia),(kk-1)**2)
            call CopyVek(spopasOld(1,ia),spopas(1,ia),(kk-1)**2)
            kapa2(ia)=kapa2Old(ia)
            skapa2(ia)=skapa2Old(ia)
            LocAtSystSt(1,ia)=LocAtSystStOld(1,ia)
            LocAtSystSt(2,ia)=LocAtSystStOld(2,ia)
            LocAtSystAx(ia)=LocAtSystAxOld(ia)
            LocAtSense(ia)=LocAtSenseOld(ia)
            SmbPGAt(ia)=SmbPGAtOld(ia)
            call CopyVek(LocAtSystXOld(1,1,ia),LocAtSystX(1,1,ia),6)
          endif
        enddo
        deallocate(LocAtSystStOld,SmbPGAtOld,TrAtOld,TriAtOld,
     1             TroAtOld,TroiAtOld,
     2             LocAtSystAxOld,LocAtSenseOld,kapa1Old,kapa2Old,
     3             skapa1Old,skapa2Old,popcOld,spopcOld,popvOld,
     4             spopvOld,popasOld,spopasOld,NPGAtOld,RPGAtOld,
     5             LocAtSystXOld)
      endif
      if(MaxNDimI.gt.0) then
        n=NAtAll
        allocate(qcntOld(3,n),phfOld(n),sphfOld(n),
     1           OrthoX40Old(n),OrthoDeltaOld(n),OrthoEpsOld(n))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          if(NDimI(kswa(ia)).gt.0) then
            call CopyVek(qcnt(1,ia),qcntOld(1,ia),NDimI(kswa(ia)))
             phfOld(ia)= phf(ia)
            sphfOld(ia)=sphf(ia)
            OrthoX40Old(ia)=OrthoX40(ia)
            OrthoDeltaOld(ia)=OrthoDelta(ia)
            OrthoEpsOld(ia)=OrthoEps(ia)
          endif
        enddo
        deallocate(qcnt,phf,sphf,OrthoX40,OrthoDelta,OrthoEps)
        n=NAtAll+NAtPlus
        allocate(qcnt(3,n),phf(n),sphf(n),
     1           OrthoX40(n),OrthoDelta(n),OrthoEps(n))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          if(NDimI(kswa(ia)).gt.0) then
            call CopyVek(qcntOld(1,ia),qcnt(1,ia),NDimI(kswa(ia)))
              phf(ia)= phfOld(ia)
            sphf(ia)=sphfOld(ia)
            OrthoX40(ia)=OrthoX40Old(ia)
            OrthoDelta(ia)=OrthoDeltaOld(ia)
            OrthoEps(ia)=OrthoEpsOld(ia)
          endif
        enddo
        deallocate(qcntOld,phfOld,sphfOld,OrthoX40Old,OrthoDeltaOld,
     1             OrthoEpsOld)
      endif
      if(MaxMagneticType.gt.0) then
        n=NAtAll
        allocate(NamePolarOld(n),sm0Old(3,n),ssm0Old(3,n))
        do ia=1,NAtInd
          if(MagPar(ia).gt.0) then
            call CopyVek( sm0(1,ia), sm0Old(1,ia),3)
            call CopyVek(ssm0(1,ia),ssm0Old(1,ia),3)
          endif
        enddo
        do ia=1,NPolar
          NamePolarOld(ia)=NamePolar(ia)
        enddo
        deallocate(NamePolar,sm0,ssm0)
        n=NAtAll+NAtPlus
        allocate(NamePolar(n),sm0(3,n),ssm0(3,n))
        do ia=1,NAtInd
          if(MagPar(ia).gt.0) then
            call CopyVek( sm0Old(1,ia), sm0(1,ia),3)
            call CopyVek(ssm0Old(1,ia),ssm0(1,ia),3)
          endif
        enddo
        do ia=1,NPolar
          NamePolar(ia)=NamePolarOld(ia)
        enddo
        deallocate(NamePolarOld,sm0Old,ssm0Old)
      endif
      nn=NAtAll+NAtPlus
      do i=0,itfmax
        n=TRank(i)
        KModP=KModAMax(i+1)
        if(i.eq.0) then
          if(KModP.gt.0) then
            allocate(p0(1,NAtAll),px(n,KModP,NAtAll),py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( ax(1,ia), px(1,1,ia),m)
              call CopyVek( ay(1,ia), py(1,1,ia),m)
              call CopyVek(sax(1,ia),spx(1,1,ia),m)
              call CopyVek(say(1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(ax)) deallocate(ax,ay,sax,say)
          if(mw.gt.0) then
            allocate( ax(mw,nn), ay(mw,nn))
            allocate(sax(mw,nn),say(mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), ax(1,ia),m)
              call CopyVek( py(1,1,ia), ay(1,ia),m)
              call CopyVek(spx(1,1,ia),sax(1,ia),m)
              call CopyVek(spy(1,1,ia),say(1,ia),m)
            enddo
            deallocate(p0,px,py,spx,spy)
          endif
        else if(i.eq.1) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( ux(1,1,ia), px(1,1,ia),m)
              call CopyVek( uy(1,1,ia), py(1,1,ia),m)
              call CopyVek(sux(1,1,ia),spx(1,1,ia),m)
              call CopyVek(suy(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(ux)) deallocate(ux,uy,sux,suy)
          if(mw.gt.0) then
            allocate(ux(n,mw,nn),uy(n,mw,nn))
            allocate(sux(n,mw,nn),suy(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), ux(1,1,ia),m)
              call CopyVek( py(1,1,ia), uy(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sux(1,1,ia),m)
              call CopyVek(spy(1,1,ia),suy(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.2) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( bx(1,1,ia), px(1,1,ia),m)
              call CopyVek( by(1,1,ia), py(1,1,ia),m)
              call CopyVek(sbx(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sby(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(bx)) deallocate(bx,by,sbx,sby)
          if(mw.gt.0) then
            allocate(bx(n,mw,nn),by(n,mw,nn))
            allocate(sbx(n,mw,nn),sby(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), bx(1,1,ia),m)
              call CopyVek( py(1,1,ia), by(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sbx(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sby(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.3) then
          allocate(p0(n,NAtAll),sp0(n,NAtAll))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.2) then
              call CopyVek(c3(1,ia),p0(1,ia),n)
              call CopyVek(sc3(1,ia),sp0(1,ia),n)
            endif
          enddo
          deallocate(c3,sc3)
          nn=NAtAll+NAtPlus
          allocate(c3(n,nn),sc3(n,nn))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.2) then
              call CopyVek(p0(1,ia),c3(1,ia),n)
              call CopyVek(sp0(1,ia),sc3(1,ia),n)
            endif
          enddo
          deallocate(p0,sp0)
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( c3x(1,1,ia), px(1,1,ia),m)
              call CopyVek( c3y(1,1,ia), py(1,1,ia),m)
              call CopyVek(sc3x(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sc3y(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(c3x)) deallocate(c3x,c3y,sc3x,sc3y)
          if(mw.gt.0) then
            allocate(c3x(n,mw,nn),c3y(n,mw,nn))
            allocate(sc3x(n,mw,nn),sc3y(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), c3x(1,1,ia),m)
              call CopyVek( py(1,1,ia), c3y(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sc3x(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sc3y(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.4) then
          allocate(p0(n,NAtAll),sp0(n,NAtAll))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.3) then
              call CopyVek(c4(1,ia),p0(1,ia),n)
              call CopyVek(sc4(1,ia),sp0(1,ia),n)
            endif
          enddo
          deallocate(c4,sc4)
          nn=NAtAll+NAtPlus
          allocate(c4(n,nn),sc4(n,nn))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.3) then
              call CopyVek(p0(1,ia),c4(1,ia),n)
              call CopyVek(sp0(1,ia),sc4(1,ia),n)
            endif
          enddo
          deallocate(p0,sp0)
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( c4x(1,1,ia), px(1,1,ia),m)
              call CopyVek( c4y(1,1,ia), py(1,1,ia),m)
              call CopyVek(sc4x(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sc4y(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(c4x)) deallocate(c4x,c4y,sc4x,sc4y)
          if(mw.gt.0) then
            allocate(c4x(n,mw,nn),c4y(n,mw,nn))
            allocate(sc4x(n,mw,nn),sc4y(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), c4x(1,1,ia),m)
              call CopyVek( py(1,1,ia), c4y(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sc4x(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sc4y(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.5) then
          allocate(p0(n,NAtAll),sp0(n,NAtAll))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.4) then
              call CopyVek(c5(1,ia),p0(1,ia),n)
              call CopyVek(sc5(1,ia),sp0(1,ia),n)
            endif
          enddo
          deallocate(c5,sc5)
          nn=NAtAll+NAtPlus
          allocate(c5(n,nn),sc5(n,nn))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.4) then
              call CopyVek(p0(1,ia),c5(1,ia),n)
              call CopyVek(sp0(1,ia),sc5(1,ia),n)
            endif
          enddo
          deallocate(p0,sp0)
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( c5x(1,1,ia), px(1,1,ia),m)
              call CopyVek( c5y(1,1,ia), py(1,1,ia),m)
              call CopyVek(sc5x(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sc5y(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(c5x)) deallocate(c5x,c5y,sc5x,sc5y)
          if(mw.gt.0) then
            allocate(c5x(n,mw,nn),c5y(n,mw,nn))
            allocate(sc5x(n,mw,nn),sc5y(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), c5x(1,1,ia),m)
              call CopyVek( py(1,1,ia), c5y(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sc5x(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sc5y(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(i.eq.6) then
          allocate(p0(n,NAtAll),sp0(n,NAtAll))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.5) then
              call CopyVek(c6(1,ia),p0(1,ia),n)
              call CopyVek(sc6(1,ia),sp0(1,ia),n)
            endif
          enddo
          deallocate(c6,sc6)
          nn=NAtAll+NAtPlus
          allocate(c6(n,nn),sc6(n,nn))
          do ia=1,NAtAll
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            if(itf(ia).gt.5) then
              call CopyVek(p0(1,ia),c6(1,ia),n)
              call CopyVek(sp0(1,ia),sc6(1,ia),n)
            endif
          enddo
          deallocate(p0,sp0)
          if(KModP.gt.0) then
            allocate( px(n,KModP,NAtAll), py(n,KModP,NAtAll),
     1               spx(n,KModP,NAtAll),spy(n,KModP,NAtAll))
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( c6x(1,1,ia), px(1,1,ia),m)
              call CopyVek( c6y(1,1,ia), py(1,1,ia),m)
              call CopyVek(sc6x(1,1,ia),spx(1,1,ia),m)
              call CopyVek(sc6y(1,1,ia),spy(1,1,ia),m)
            enddo
          endif
          if(allocated(c6x)) deallocate(c6x,c6y,sc6x,sc6y)
          if(mw.gt.0) then
            allocate(c6x(n,mw,nn),c6y(n,mw,nn))
            allocate(sc6x(n,mw,nn),sc6y(n,mw,nn))
          endif
          if(KModP.gt.0) then
            do ia=1,NAtAll
              if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
              m=KModA(i+1,ia)*n
              call CopyVek( px(1,1,ia), c6x(1,1,ia),m)
              call CopyVek( py(1,1,ia), c6y(1,1,ia),m)
              call CopyVek(spx(1,1,ia),sc6x(1,1,ia),m)
              call CopyVek(spy(1,1,ia),sc6y(1,1,ia),m)
            enddo
            deallocate(px,py,spx,spy)
          endif
        endif
      enddo
      if(MagParMax.gt.1) then
        n=3
        nn=NAtAll+NAtPlus
        allocate( px(n,MagParMax-1,NAtAll), py(n,MagParMax-1,NAtAll),
     1           spx(n,MagParMax-1,NAtAll),spy(n,MagParMax-1,NAtAll))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          m=(MagPar(ia)-1)*n
          if(m.gt.0) then
            call CopyVek( smx(1,1,ia), px(1,1,ia),m)
            call CopyVek( smy(1,1,ia), py(1,1,ia),m)
            call CopyVek(ssmx(1,1,ia),spx(1,1,ia),m)
            call CopyVek(ssmy(1,1,ia),spy(1,1,ia),m)
          endif
        enddo
        if(allocated(smx)) deallocate(smx,smy,ssmx,ssmy)
        allocate( smx(n,MagParMax-1,nn), smy(n,MagParMax-1,nn),
     1           ssmx(n,MagParMax-1,nn),ssmy(n,MagParMax-1,nn))
        do ia=1,NAtAll
          if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
          m=(MagPar(ia)-1)*n
          if(m.gt.0) then
            call CopyVek( px(1,1,ia), smx(1,1,ia),m)
            call CopyVek( py(1,1,ia), smy(1,1,ia),m)
            call CopyVek(spx(1,1,ia),ssmx(1,1,ia),m)
            call CopyVek(spy(1,1,ia),ssmy(1,1,ia),m)
          endif
        enddo
        deallocate(px,py,spx,spy)
      endif
      MxAtAll=MxAtAll+NAtPlus
9999  return
      end
      subroutine ReallocateAtomParams(NAtLast,itfn,ifrn,kmodan,MagParN)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension kmodan(*),px(:,:,:),py(:,:,:),spx(:,:,:),spy(:,:,:),
     1          KiP(:,:)
      allocatable px,py,spx,spy,KiP
      md=0
      mw=0
      do i=0,max(itfn,itfmax,2)
        kmod=max(kmodan(i+1),KModAMax(i+1))
        n=TRank(i)
        mw=max(mw,kmod)
        md=md+n*(2*kmod+1)
        if(i.eq.0.and.kmod.ne.0) md=md+1
        if(ifrn.ne.0) md=md+1
      enddo
      if(mw.gt.0) md=md+1
      kmod=max(MagParN,MagParMax)
      mw=max(mw,kmod-1)
      md=md+3*(2*kmod+1)
      if(ChargeDensities) md=md+68
      if(md.le.mxda.and.MxAtAll.le.NAtAllocMod) go to 9999
      do i=0,max(itfn,itfmax,2)
        n=TRank(i)
        KModP=KModAMax(i+1)
        if(i.eq.0) then
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( ax(1,ia), px(1,1,ia),m)
                call CopyVek( ay(1,ia), py(1,1,ia),m)
                call CopyVek(sax(1,ia),spx(1,1,ia),m)
                call CopyVek(say(1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(ax)) deallocate(ax,ay,sax,say)
            if(mw.gt.0) then
              allocate( ax(mw,MxAtAll), ay(mw,MxAtAll))
              ax=0.
              ay=0.
              allocate(sax(mw,MxAtAll),say(mw,MxAtAll))
              sax=0.
              say=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), ax(1,ia),m)
                call CopyVek( py(1,1,ia), ay(1,ia),m)
                call CopyVek(spx(1,1,ia),sax(1,ia),m)
                call CopyVek(spy(1,1,ia),say(1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.1) then
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( ux(1,1,ia), px(1,1,ia),m)
                call CopyVek( uy(1,1,ia), py(1,1,ia),m)
                call CopyVek(sux(1,1,ia),spx(1,1,ia),m)
                call CopyVek(suy(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(ux)) deallocate(ux,uy,sux,suy)
            if(mw.gt.0) then
              allocate(ux(n,mw,MxAtAll),uy(n,mw,MxAtAll))
              ux=0.
              uy=0.
              allocate(sux(n,mw,MxAtAll),suy(n,mw,MxAtAll))
              sux=0.
              suy=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), ux(1,1,ia),m)
                call CopyVek( py(1,1,ia), uy(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sux(1,1,ia),m)
                call CopyVek(spy(1,1,ia),suy(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.2) then
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( bx(1,1,ia), px(1,1,ia),m)
                call CopyVek( by(1,1,ia), py(1,1,ia),m)
                call CopyVek(sbx(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sby(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(bx)) deallocate(bx,by,sbx,sby)
            if(mw.gt.0) then
              allocate( bx(n,mw,MxAtAll), by(n,mw,MxAtAll))
              bx=0.
              by=0.
              allocate(sbx(n,mw,MxAtAll),sby(n,mw,MxAtAll))
              sbx=0.
              sby=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), bx(1,1,ia),m)
                call CopyVek( py(1,1,ia), by(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sbx(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sby(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.3) then
          if(.not.allocated(c3))
     1      allocate(c3(n,MxAtAll),sc3(n,MxAtAll))
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( c3x(1,1,ia), px(1,1,ia),m)
                call CopyVek( c3y(1,1,ia), py(1,1,ia),m)
                call CopyVek(sc3x(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sc3y(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(c3x)) deallocate(c3x,c3y,sc3x,sc3y)
            if(mw.gt.0) then
              allocate( c3x(n,mw,MxAtAll), c3y(n,mw,MxAtAll))
              c3x=0.
              c3y=0.
              allocate(sc3x(n,mw,MxAtAll),sc3y(n,mw,MxAtAll))
              sc3x=0.
              sc3y=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), c3x(1,1,ia),m)
                call CopyVek( py(1,1,ia), c3y(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sc3x(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sc3y(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.4) then
          if(.not.allocated(c4))
     1      allocate(c4(n,MxAtAll),sc4(n,MxAtAll))
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( c4x(1,1,ia), px(1,1,ia),m)
                call CopyVek( c4y(1,1,ia), py(1,1,ia),m)
                call CopyVek(sc4x(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sc4y(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(c4x)) deallocate(c4x,c4y,sc4x,sc4y)
            if(mw.gt.0) then
              allocate( c4x(n,mw,MxAtAll), c4y(n,mw,MxAtAll))
              c4x=0.
              c4y=0.
              allocate(sc4x(n,mw,MxAtAll),sc4y(n,mw,MxAtAll))
              sc4x=0.
              sc4y=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), c4x(1,1,ia),m)
                call CopyVek( py(1,1,ia), c4y(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sc4x(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sc4y(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.5) then
          if(.not.allocated(c5))
     1      allocate(c5(n,MxAtAll),sc5(n,MxAtAll))
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( c5x(1,1,ia), px(1,1,ia),m)
                call CopyVek( c5y(1,1,ia), py(1,1,ia),m)
                call CopyVek(sc5x(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sc5y(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(c5x)) deallocate(c5x,c5y,sc5x,sc5y)
            if(mw.gt.0) then
              allocate( c5x(n,mw,MxAtAll), c5y(n,mw,MxAtAll))
              c5x=0.
              c5y=0.
              allocate(sc5x(n,mw,MxAtAll),sc5y(n,mw,MxAtAll))
              sc5x=0.
              sc5y=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), c5x(1,1,ia),m)
                call CopyVek( py(1,1,ia), c5y(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sc5x(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sc5y(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        else if(i.eq.6) then
          if(.not.allocated(c6))
     1      allocate(c6(n,MxAtAll),sc6(n,MxAtAll))
          if(mw.gt.KModP.or.MxAtAll.gt.NAtAllocMod) then
            if(KModP.gt.0) then
              allocate( px(n,KModP,NAtLast),
     1                  py(n,KModP,NAtLast),
     2                 spx(n,KModP,NAtLast),
     3                 spy(n,KModP,NAtLast))
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( c6x(1,1,ia), px(1,1,ia),m)
                call CopyVek( c6y(1,1,ia), py(1,1,ia),m)
                call CopyVek(sc6x(1,1,ia),spx(1,1,ia),m)
                call CopyVek(sc6y(1,1,ia),spy(1,1,ia),m)
              enddo
            endif
            if(allocated(c6x)) deallocate(c6x,c6y,sc6x,sc6y)
            if(mw.gt.0) then
              allocate( c6x(n,mw,MxAtAll), c6y(n,mw,MxAtAll))
              c6x=0.
              c6y=0.
              allocate(sc6x(n,mw,MxAtAll),sc6y(n,mw,MxAtAll))
              sc6x=0.
              sc6y=0.
            endif
            if(KModP.gt.0) then
              do ia=1,NAtLast
                if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
                m=KModA(i+1,ia)*n
                call CopyVek( px(1,1,ia), c6x(1,1,ia),m)
                call CopyVek( py(1,1,ia), c6y(1,1,ia),m)
                call CopyVek(spx(1,1,ia),sc6x(1,1,ia),m)
                call CopyVek(spy(1,1,ia),sc6y(1,1,ia),m)
              enddo
              deallocate(px,py,spx,spy)
            endif
          endif
        endif
      enddo
      if(MagParN.gt.MagParMax.or.MxAtAll.gt.NAtAllocMod) then
        n=3
        if(MagParMax.gt.1) then
          allocate( px(n,MagParMax-1,NAtLast),
     1              py(n,MagParMax-1,NAtLast),
     2             spx(n,MagParMax-1,NAtLast),
     3             spy(n,MagParMax-1,NAtLast))
          do ia=1,NAtLast
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            m=(MagPar(ia)-1)*n
            if(m.gt.0) then
              call CopyVek( smx(1,1,ia), px(1,1,ia),m)
              call CopyVek( smy(1,1,ia), py(1,1,ia),m)
              call CopyVek(ssmx(1,1,ia),spx(1,1,ia),m)
              call CopyVek(ssmy(1,1,ia),spy(1,1,ia),m)
            endif
          enddo
        endif
        if(allocated(smx)) deallocate(smx,smy,ssmx,ssmy)
        allocate( smx(n,MagParN-1,MxAtAll),
     1            smy(n,MagParN-1,MxAtAll))
        allocate(ssmx(n,MagParN-1,MxAtAll),
     1           ssmy(n,MagParN-1,MxAtAll))
        if(MagParMax.gt.1) then
          do ia=1,NAtLast
            if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
            m=(MagPar(ia)-1)*n
            if(m.gt.0) then
              call CopyVek( px(1,1,ia), smx(1,1,ia),m)
              call CopyVek( py(1,1,ia), smy(1,1,ia),m)
              call CopyVek(spx(1,1,ia),ssmx(1,1,ia),m)
              call CopyVek(spy(1,1,ia),ssmy(1,1,ia),m)
            endif
          enddo
          deallocate(px,py,spx,spy)
        endif
      endif
      allocate(KiP(mxda,NAtLast))
      do ia=1,NAtLast
        if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
        call CopyVekI(KiA(1,ia),KiP(1,ia),mxda)
      enddo
      deallocate(KiA)
      allocate(KiA(md,MxAtAll))
      do ia=1,NAtLast
        if(ia.gt.NAtCalc.and.ia.lt.NAtMolFr(1,1)) cycle
        call CopyVekI(KiP(1,ia),KiA(1,ia),mxda)
        if(md.gt.mxda) call SetIntArrayTo(KiA(mxda+1,ia),md-mxda,0)
      enddo
      deallocate(KiP)
      mxda=md
      do i=1,7
        KModAMax(i)=max(KModAMax(i),KModAN(i))
      enddo
      MagParMax=max(MagParMax,MagParN)
      NAtAllocMod=MxAtAll
9999  return
      end
      subroutine DeallocateAtomParams
      use Atoms_mod
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      do i=0,6
        if(i.eq.0) then
          if(allocated(ax)) deallocate(ax,ay,sax,say)
        else if(i.eq.1) then
          if(allocated(ux)) deallocate(ux,uy,sux,suy)
        else if(i.eq.2) then
          if(allocated(bx)) deallocate(bx,by,sbx,sby)
        else if(i.eq.3) then
          if(allocated(c3)) deallocate(c3,sc3)
          if(allocated(c3x)) deallocate(c3x,sc3x,c3y,sc3y)
        else if(i.eq.4) then
          if(allocated(c4)) deallocate(c4,sc4)
          if(allocated(c4x)) deallocate(c4x,sc4x,c4y,sc4y)
        else if(i.eq.5) then
          if(allocated(c5)) deallocate(c5,sc5)
          if(allocated(c5x)) deallocate(c5x,sc5x,c5y,sc5y)
        else if(i.eq.6) then
          if(allocated(c6)) deallocate(c6,sc6)
          if(allocated(c6x)) deallocate(c6x,sc6x,c6y,sc6y)
        endif
      enddo
      if(allocated(smx)) deallocate(smx,smy,ssmx,ssmy)
      return
      end
      subroutine AllocateOrtho(n)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(orx40)) deallocate(orx40,ordel,oreps,ora)
      allocate(orx40(n),ordel(n),oreps(n),ora(n))
      mxo=n
      end
      subroutine ReallocOrtho(n)
      use Atoms_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      real orx40o(:),ordelo(:),orepso(:)
      character*12 orao(:)
      allocatable orx40o,ordelo,orepso,orao
      if(n.le.mxo) go to 9999
      if(nor.gt.0) then
        allocate(orx40o(nor),ordelo(nor),orepso(nor),orao(nor))
        call CopyVek(OrX40,OrX40O,nor)
        call CopyVek(OrDel,OrDelO,nor)
        call CopyVek(OrEps,OrEpsO,nor)
        do i=1,nor
          OrAO(i)=OrA(i)
        enddo
      endif
      if(allocated(OrX40)) deallocate(OrX40,OrDel,OrEps,OrA)
      allocate(OrX40(n),OrDel(n),OrEps(n),OrA(n))
      mxo=n
      if(nor.gt.0) then
        call CopyVek(OrX40O,OrX40,nor)
        call CopyVek(OrDelO,OrDel,nor)
        call CopyVek(OrEpsO,OrEps,nor)
        do i=1,nor
          OrA(i)=OrAO(i)
        enddo
        deallocate(OrX40O,OrDelO,OrEpsO,OrAO)
      endif
9999  return
      end
      subroutine AllocateMolecules(n,m)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(MolName))
     1  deallocate(MolName,ISwMol,KSwMol,KModM,KFM,KTLS,
     2             StRefPoint,RefPoint,TypeModFunMol,
     3             LocMolSystType,LocMolSystSt,LocMolSystAx,
     4             LocMolSystX,
     5             RotSign,RotMol,RotIMol,TrMol,TrIMol,
     6             DRotF,DRotC,DRotP,RotB,DRotBF,DRotBC,DRotBP,
     7             PrvniKiMolekuly,DelkaKiMolekuly,KiMol,
     8             UsePGSyst,SmbPGMol,LocPGSystSt,LocPGSystAx,TrPG,
     9             TriPG,LocPGSystX,
     a             NPoint,KPoint,IPoint,RPoint,SPoint,TPoint,
     1             AiMol,SAiMol,Xm,AtTrans,Trans,STrans,Euler,SEuler,
     2             a0m,sa0m)
      mxm=n
      mxp=m
      mw=0
      mxdm=7
      nm=n*m
      mxpm=nm
      allocate(MolName(n),ISwMol(n),KSwMol(n),KModM(3,nm),KFM(3,nm),
     1         KTLS(n),StRefPoint(n),RefPoint(n),
     2         LocMolSystSt(2,2,nm),LocMolSystType(nm),
     3         LocMolSystAx(2,nm),LocMolSystX(3,2,2,nm),
     4         RotSign(nm),RotMol(9,nm),RotiMol(9,nm),TrMol(9,nm),
     5         TriMol(9,nm),TypeModFunMol(nm),
     6         DRotF(9,nm),DRotC(9,nm),DRotP(9,nm),RotB(36,nm),
     7         DRotBF(36,nm),DRotBC(36,nm),DRotBP(36,nm),
     8         PrvniKiMolekuly(nm),DelkaKiMolekuly(nm),KiMol(mxdm,nm),
     9         UsePGSyst(n),SmbPGMol(n),LocPGSystSt(2,n),LocPGSystAx(n),
     a         TrPG(9,n),TriPG(9,n),LocPGSystX(3,2,n),
     1         NPoint(n),KPoint(n),IPoint(mxpg,n),RPoint(9,mxpg,n),
     2         SPoint(3,mxpg,n),TPoint(36,mxpg,n),
     3         AiMol(nm),SAiMol(nm),Xm(3,n),AtTrans(nm),Trans(3,nm),
     4         STrans(3,nm),Euler(3,nm),SEuler(3,nm),a0m(nm),sa0m(nm))
      if(allocated(phfm)) deallocate(phfm,sphfm,OrthoX40Mol,
     1                               OrthoDeltaMol,OrthoEpsMol)
      if(MaxNDimI.gt.0) allocate(phfm(nm),sphfm(nm),OrthoX40Mol(nm),
     1                           OrthoDeltaMol(nm),OrthoEpsMol(nm))
      call DeallocateMoleculeParams
      return
      end
      subroutine ReallocateMolecules(NPlus,MPlus)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
c
c   Musi byt alokovany vzdy
c
      character*27 LocMolSystStOld(:,:,:),LocPGSystStOld(:,:)
      character*8  MolNameOld(:),StRefPointOld(:),AtTransOld(:),
     1             SmbPGMolOld(:)
      character*2  LocMolSystAxOld(:,:),LocPGSystAxOld(:)
      integer ISwMolOld(:),KSwMolOld(:),KTLSOld(:),
     1        RefPointOld(:),KModMOld(:,:),KFMOld(:,:),KiMolOld(:,:),
     2        LocMolSystTypeOld(:),PrvniKiMolekulyOld(:),
     3        DelkaKiMolekulyOld(:),iamOld(:),mamOld(:),
     4        RotSignOld(:),NPointOld(:),KPointOld(:),IPointOld(:,:),
     5        TypeModFunMolOld(:)
      real    RotMolOld(:,:),RotIMolOld(:,:),TrMolOld(:,:),
     1        TrIMolOld(:,:),DRotFOld(:,:),DRotCOld(:,:),DRotPOld(:,:),
     2        RotBOld(:,:),DRotBFOld(:,:),DRotBCOld(:,:),DRotBPOld(:,:),
     3        TrPGOld(:,:),TriPGOld(:,:),RPointOld(:,:,:),
     4        SPointOld(:,:,:),TPointOld(:,:,:),LocPGSystXOld(:,:,:),
     5        AiMolOld(:),SAiMolOld(:),XmOld(:,:),TransOld(:,:),
     6        STransOld(:,:),EulerOld(:,:),SEulerOld(:,:),a0mOld(:),
     7        sa0mOld(:),LocMolSystXOld(:,:,:,:)
      logical UsePGSystOld(:)
      allocatable LocMolSystStOld,LocPGSystStOld,MolNameOld,
     1            StRefPointOld,AtTransOld,SmbPGMolOld,LocPGSystXOld,
     2            LocMolSystAxOld,LocPGSystAxOld,ISwMolOld,KSwMolOld,
     3            KTLSOld,RefPointOld,KModMOld,KFMOld,KiMolOld,
     4            LocMolSystTypeOld,PrvniKiMolekulyOld,
     5            DelkaKiMolekulyOld,iamOld,mamOld,RotSignOld,NPointOld,
     6            KPointOld,IPointOld,RotMolOld,RotIMolOld,TrMolOld,
     1            TrIMolOld,DRotFOld,DRotCOld,DRotPOld,RotBOld,
     2            DRotBFOld,DRotBCOld,DRotBPOld,TrPGOld,TriPGOld,
     3            RPointOld,SPointOld,TPointOld,AiMolOld,SAiMolOld,
     4            XmOld,TransOld,STransOld,EulerOld,SEulerOld,a0mOld,
     5            sa0mOld,UsePGSystOld,LocMolSystXOld,TypeModFunMolOld
c
c  Musi byt alokovany pro modulovane struktury
c
      real phfmOld(:),sphfmOld(:),OrthoX40MolOld(:),OrthoDeltaMolOld(:),
     1     OrthoEpsMolOld(:)
      allocatable phfmOld,sphfmOld,OrthoX40MolOld,OrthoDeltaMolOld,
     1            OrthoEpsMolOld
c
c  Musi byt alokovany pro TLS
c
      real ttOld(:,:),tlOld(:,:),tsOld(:,:),sttOld(:,:),stlOld(:,:),
     1     stsOld(:,:)
      allocatable ttOld,tlOld,tsOld,sttOld,stlOld,stsOld 
      real p0(:,:),px(:,:,:),py(:,:,:),spx(:,:,:),spy(:,:,:),
     1             rx(:,:,:),ry(:,:,:),srx(:,:,:),sry(:,:,:),
     1             qx(:,:,:),qy(:,:,:),sqx(:,:,:),sqy(:,:,:)
      allocatable p0,px,py,spx,spy,rx,ry,srx,sry,qx,qy,sqx,sqy
      n=mxm
      m=mxp
      nm=n*m
      mw=0
      do i=1,3
        mw=max(mw,KModMMax(i))
      enddo
      allocate(MolNameOld(n),ISwMolOld(n),KSwMolOld(n),KModMOld(3,nm),
     1         KFMOld(3,nm),KTLSOld(n),StRefPointOld(n),
     2         RefPointOld(n),iamOld(n),mamOld(n),
     3         LocMolSystStOld(2,2,nm),LocMolSystTypeOld(nm),
     4         LocMolSystAxOld(2,nm),LocMolSystXOld(3,2,2,nm),
     5         RotSignOld(nm),RotMolOld(9,nm),RotIMolOld(9,nm),
     6         TrMolOld(9,nm),TriMolOld(9,nm),TypeModFunMolOld(nm),
     7         DRotFOld(9,nm),DRotCOld(9,nm),DRotPOld(9,nm),
     8         RotBOld(36,nm),DRotBFOld(36,nm),DRotBCOld(36,nm),
     9         DRotBPOld(36,nm),PrvniKiMolekulyOld(nm),
     a         DelkaKiMolekulyOld(nm),KiMolOld(mxdm,nm),UsePGSystOld(n),
     1         SmbPGMolOld(n),LocPGSystStOld(2,n),LocPGSystAxOld(n),
     2         TrPGOld(9,n),TriPGOld(9,n),LocPGSystXOld(3,2,n),
     3         NPointOld(n),KPointOld(n),IPointOld(mxpg,n),
     4         RPointOld(9,mxpg,n),SPointOld(3,mxpg,n),
     5         TPointOld(36,mxpg,n),
     6         AiMolOld(nm),SAiMolOld(nm),XmOld(3,n),AtTransOld(nm),
     7         TransOld(3,nm),STransOld(3,nm),EulerOld(3,nm),
     8         SEulerOld(3,nm),a0mOld(nm),sa0mOld(nm))
      do i=1,NMolec
        MolNameOld(i)=MolName(i)
        ISwMolOld(i)=ISwMol(i)
        KSwMolOld(i)=KSwMol(i)
        KTLSOld(i)=KTLS(i)
        StRefPointOld(i)=StRefPoint(i)
        RefPointOld(i)=RefPoint(i)
        KPointOld(i)=KPoint(i)
        iamOld(i)=iam(i)
        mamOld(i)=mam(i)
        UsePGSystOld(i)=UsePGSyst(i)
        NPointOld(i)=NPoint(i)
        call CopyVek(Xm(1,i),XmOld(1,i),3)
        SmbPGMolOld(i)=SmbPGMol(i)
        do k=1,NPoint(i)
          IPointOld(k,i)=IPoint(k,i)
          call CopyVek(RPoint(1,k,i),RPointOld(1,k,i),9)
          call CopyVek(SPoint(1,k,i),SPointOld(1,k,i),3)
          call CopyVek(TPoint(1,k,i),TPointOld(1,k,i),36)
        enddo
        LocPGSystStOld(1,i)=LocPGSystSt(1,i)
        LocPGSystStOld(2,i)=LocPGSystSt(2,i)
        LocPGSystAxOld(i)=LocPGSystAx(i)
        call CopyVek(LocPGSystX(1,1,i),LocPGSystXOld(1,1,i),6)
        call CopyVek(TrPG(1,i),TrPGOld(1,i),9)
        call CopyVek(TrIPG(1,i),TrIPGOld(1,i),9)
        ji=(i-1)*mxp
        do j=1,mam(i)
          ji=ji+1
          call CopyVekI(KModM(1,ji),KModMOld(1,ji),3)
          TypeModFunMolOld(ji)=TypeModFunMol(ji)
          call CopyVekI(KFM(1,ji),KFMOld(1,ji),3)
          LocMolSystTypeOld(ji)=LocMolSystType(ji)
          RotSignOld(ji)=RotSign(ji)
          call CopyVek(RotMol(1,ji),RotMolOld(1,ji),9)
          call CopyVek(RotIMol(1,ji),RotIMolOld(1,ji),9)
          call CopyVek(TrMol(1,ji),TrMolOld(1,ji),9)
          call CopyVek(TrIMol(1,ji),TrIMolOld(1,ji),9)
          call CopyVek(DRotF(1,ji),DRotFOld(1,ji),9)
          call CopyVek(DRotC(1,ji),DRotCOld(1,ji),9)
          call CopyVek(DRotP(1,ji),DRotPOld(1,ji),9)
          call CopyVek(RotB(1,ji),RotBOld(1,ji),36)
          call CopyVek(DRotBF(1,ji),DRotBFOld(1,ji),36)
          call CopyVek(DRotBC(1,ji),DRotBCOld(1,ji),36)
          call CopyVek(DRotBP(1,ji),DRotBPOld(1,ji),36)
          PrvniKiMolekulyOld(ji)=PrvniKiMolekuly(ji)
          DelkaKiMolekulyOld(ji)=DelkaKiMolekuly(ji)
          call CopyVekI(KiMol(1,ji),KiMolOld(1,ji),mxdm)
          AiMolOld(ji)=AiMol(ji)
          SAiMolOld(ji)=SAiMol(ji)
          AtTransOld(ji)=AtTrans(ji)
          a0mOld(ji)=a0m(ji)
          sa0mOld(ji)=sa0m(ji)
          call CopyVek(Trans(1,ji),TransOld(1,ji),3)
          call CopyVek(STrans(1,ji),STransOld(1,ji),3)
          call CopyVek(Euler(1,ji),EulerOld(1,ji),3)
          call CopyVek(SEuler(1,ji),SEulerOld(1,ji),3)
          do k=1,2
            LocMolSystAxOld(k,ji)=LocMolSystAx(k,ji)
            do l=1,2
              LocMolSystStOld(l,k,ji)=LocMolSystSt(l,k,ji)
              call CopyVek(LocMolSystX(1,l,k,ji),
     1                     LocMolSystXOld(1,l,k,ji),3)
            enddo
          enddo
        enddo
      enddo
      deallocate(MolName,ISwMol,KSwMol,KModM,KFM,KTLS,StRefPoint,
     1           RefPoint,iam,mam,LocMolSystSt,LocMolSystType,
     2           LocMolSystX,LocMolSystAx,TypeModFunMol,
     3           RotSign,RotMol,RotIMol,TrMol,TriMol,
     4           DRotF,DRotC,DRotP,RotB,DRotBF,DRotBC,DRotBP,
     5           PrvniKiMolekuly,DelkaKiMolekuly,KiMol,UsePGSyst,
     6           SmbPGMol,LocPGSystSt,LocPGSystAx,TrPG,TriPG,LocPGSystX,
     7           NPoint,KPoint,IPoint,RPoint,SPoint,TPoint,AiMol,SAiMol,
     8           Xm,AtTrans,Trans,STrans,Euler,SEuler,a0m,sa0m)
      n=n+NPlus
      m=m+MPlus
      nm=n*m
      allocate(MolName(n),ISwMol(n),KSwMol(n),KModM(3,nm),KFM(3,nm),
     1         KTLS(n),StRefPoint(n),RefPoint(n),iam(n),mam(n),
     2         LocMolSystSt(2,2,nm),LocMolSystType(nm),
     3         LocMolSystAx(2,nm),LocMolSystX(3,2,2,nm),
     4         RotSign(nm),RotMol(9,nm),RotiMol(9,nm),TrMol(9,nm),
     5         TriMol(9,nm),TypeModFunMol(nm),
     6         DRotF(9,nm),DRotC(9,nm),DRotP(9,nm),RotB(36,nm),
     7         DRotBF(36,nm),DRotBC(36,nm),DRotBP(36,nm),
     8         PrvniKiMolekuly(nm),DelkaKiMolekuly(nm),KiMol(mxdm,nm),
     9         UsePGSyst(n),SmbPGMol(n),LocPGSystSt(2,n),LocPGSystAx(n),
     a         TrPG(9,n),TriPG(9,n),LocPGSystX(3,2,n),
     1         NPoint(n),KPoint(n),IPoint(mxpg,n),RPoint(9,mxpg,n),
     2         SPoint(3,mxpg,n),TPoint(36,mxpg,n),
     3         AiMol(nm),SAiMol(nm),Xm(3,n),AtTrans(nm),Trans(3,nm),
     4         STrans(3,nm),Euler(3,nm),SEuler(3,nm),a0m(nm),sa0m(nm))
      do i=1,NMolec
        MolName(i)=MolNameOld(i)
        ISwMol(i)=ISwMolOld(i)
        KSwMol(i)=KSwMolOld(i)
        KTLS(i)=KTLSOld(i)
        StRefPoint(i)=StRefPointOld(i)
        RefPoint(i)=RefPointOld(i)
        iam(i)=iamOld(i)
        mam(i)=mamOld(i)
        UsePGSyst(i)=UsePGSystOld(i)
        NPoint(i)=NPointOld(i)
        call CopyVek(XmOld(1,i),Xm(1,i),3)
        SmbPGMol(i)=SmbPGMolOld(i)
        KPoint(i)=KPointOld(i)
        LocPGSystSt(1,i)=LocPGSystStOld(1,i)
        LocPGSystSt(2,i)=LocPGSystStOld(2,i)
        LocPGSystAx(i)=LocPGSystAxOld(i)
        call CopyVek(LocPGSystXOld(1,1,i),LocPGSystX(1,1,i),6)
        call CopyVek(TrPGOld(1,i),TrPG(1,i),9)
        call CopyVek(TrIPGOld(1,i),TrIPG(1,i),9)
        do k=1,NPoint(i)
          IPoint(k,i)=IPointOld(k,i)
          call CopyVek(RPointOld(1,k,i),RPoint(1,k,i),9)
          call CopyVek(SPointOld(1,k,i),SPoint(1,k,i),3)
          call CopyVek(TPointOld(1,k,i),TPoint(1,k,i),36)
        enddo
        ji=(i-1)*(mxp+MPlus)
        jio=(i-1)*mxp
        do j=1,mam(i)
          ji=ji+1
          jio=jio+1
          TypeModFunMol(ji)=TypeModFunMolOld(jio)
          call CopyVekI(KModMOld(1,jio),KModM(1,ji),3)
          call CopyVekI(KFMOld(1,jio),KFM(1,ji),3)
          LocMolSystType(ji)=LocMolSystTypeOld(jio)
          RotSign(ji)=RotSignOld(jio)
          call CopyVek(RotMolOld(1,jio),RotMol(1,ji),9)
          call CopyVek(RotIMolOld(1,jio),RotIMol(1,ji),9)
          call CopyVek(TrMolOld(1,jio),TrMol(1,ji),9)
          call CopyVek(TrIMolOld(1,jio),TrIMol(1,ji),9)
          call CopyVek(DRotFOld(1,jio),DRotF(1,ji),9)
          call CopyVek(DRotCOld(1,jio),DRotC(1,ji),9)
          call CopyVek(DRotPOld(1,jio),DRotP(1,ji),9)
          call CopyVek(RotBOld(1,jio),RotB(1,ji),36)
          call CopyVek(DRotBFOld(1,jio),DRotBF(1,ji),36)
          call CopyVek(DRotBCOld(1,jio),DRotBC(1,ji),36)
          call CopyVek(DRotBPOld(1,jio),DRotBP(1,ji),36)
          PrvniKiMolekuly(ji)=PrvniKiMolekulyOld(jio)
          DelkaKiMolekuly(ji)=DelkaKiMolekulyOld(jio)
          call CopyVekI(KiMolOld(1,jio),KiMol(1,ji),mxdm)
          AiMol(ji)=AiMolOld(jio)
          SAiMol(ji)=SAiMolOld(jio)
          AtTrans(ji)=AtTransOld(jio)
          a0m(ji)=a0mOld(jio)
          sa0m(ji)=sa0mOld(jio)
          call CopyVek(TransOld(1,jio),Trans(1,ji),3)
          call CopyVek(STransOld(1,jio),STrans(1,ji),3)
          call CopyVek(EulerOld(1,jio),Euler(1,ji),3)
          call CopyVek(SEulerOld(1,jio),SEuler(1,ji),3)
          do k=1,2
            LocMolSystAx(k,ji)=LocMolSystAxOld(k,jio)
            do l=1,2
              LocMolSystSt(l,k,ji)=LocMolSystStOld(l,k,jio)
              call CopyVek(LocMolSystXOld(1,l,k,jio),
     1                     LocMolSystX(1,l,k,ji),3)

            enddo
          enddo
        enddo
      enddo
      deallocate(MolNameOld,ISwMolOld,KSwMolOld,KModMOld,KFMOld,KTLSOld,
     1           StRefPointOld,RefPointOld,iamOld,mamOld,
     2           LocMolSystStOld,LocMolSystTypeOld,LocMolSystAxOld,
     3           LocMolSystXOld,TypeModFunMolOld,
     4           RotSignOld,RotMolOld,RotIMolOld,TrMolOld,TriMolOld,
     5           DRotFOld,DRotCOld,DRotPOld,RotBOld,DRotBFOld,DRotBCOld,
     6           DRotBPOld,PrvniKiMolekulyOld,DelkaKiMolekulyOld,
     7           KiMolOld,UsePGSystOld,SmbPGMolOld,LocPGSystStOld,
     8           LocPGSystAxOld,TrPGOld,TriPGOld,LocPGSystXOld,
     9           NPointOld,KPointOld,
     a           IPointOld,RPointOld,SPointOld,TPointOld,AiMolOld,
     1           SAiMolOld,XmOld,AtTransOld,TransOld,STransOld,EulerOld,
     2           SEulerOld,a0mOld,sa0mOld)
      if(KTLSMax.gt.0) then
        n=mxm
        m=mxp
        nm=n*m
        allocate( ttOld(6,nm), tlOld(6,nm), tsOld(9,nm),
     1           sttOld(6,nm),stlOld(6,nm),stsOld(9,nm))
        do i=1,NMolec
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            if(KTLS(i).gt.0) then
              call CopyVek(tt(1,ji),ttOld(1,ji),6)
              call CopyVek(tl(1,ji),tlOld(1,ji),6)
              call CopyVek(ts(1,ji),tsOld(1,ji),6)
              call CopyVek(stt(1,ji),sttOld(1,ji),6)
              call CopyVek(stl(1,ji),stlOld(1,ji),6)
              call CopyVek(sts(1,ji),stsOld(1,ji),6)
            endif
          enddo
        enddo
        n=n+NPlus
        m=m+MPlus
        nmNew=n*m
        deallocate(tt,tl,ts,stt,stl,sts)
        allocate( tt(6,nmNew), tl(6,nmNew), ts(9,nmNew),
     1           stt(6,nmNew),stl(6,nmNew),sts(9,nmNew))
        do i=1,NMolec
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            if(KTLS(i).gt.0) then
              call CopyVek(ttOld(1,ji),tt(1,ji),6)
              call CopyVek(tlOld(1,ji),tl(1,ji),6)
              call CopyVek(tsOld(1,ji),ts(1,ji),6)
              call CopyVek(sttOld(1,ji),stt(1,ji),6)
              call CopyVek(stlOld(1,ji),stl(1,ji),6)
              call CopyVek(stsOld(1,ji),sts(1,ji),6)
            endif
          enddo
        enddo
        deallocate(ttOld,tlOld,tsOld,sttOld,stlOld,stsOld)
      endif
      if(MaxNDimI.gt.0) then
        n=mxm
        m=mxp
        nm=n*m
        allocate(phfmOld(nm),sphfmOld(nm),OrthoX40MolOld(nm),
     1           OrthoDeltaMolOld(nm),OrthoEpsMolOld(nm))
        do i=1,NMolec
          ji=(i-1)*mxp
          do j=1,mam(i)
            ji=ji+1
            phfmOld(ji)=phfm(ji)
            sphfmOld(ji)=sphfm(ji)
            OrthoX40MolOld(ji)=OrthoX40Mol(ji)
            OrthoDeltaMolOld(ji)=OrthoDeltaMol(ji)
            OrthoEpsMolOld(ji)=OrthoEpsMol(ji)
          enddo
        enddo
        deallocate(phfm,sphfm,OrthoX40Mol,OrthoDeltaMol,OrthoEpsMol)
        n=n+NPlus
        m=m+MPlus
        nm=n*m
        allocate(phfm(nm),sphfm(nm),OrthoX40Mol(nm),
     1           OrthoDeltaMol(nm),OrthoEpsMol(nm))
        do i=1,NMolec
          ji=(i-1)*mxp
          jio=(i-1)*(mxp+MPlus)
          do j=1,mam(i)
            ji=ji+1
            jio=jio+1
            phfm(jio)=phfmOld(ji)
            sphfm(jio)=sphfmOld(ji)
            OrthoX40Mol(jio)=OrthoX40MolOld(ji)
            OrthoDeltaMol(jio)=OrthoDeltaMolOld(ji)
            OrthoEpsMol(jio)=OrthoEpsMolOld(ji)
          enddo
        enddo
        deallocate(phfmOld,sphfmOld,OrthoX40MolOld,OrthoDeltaMolOld,
     1             OrthoEpsMolOld)
      endif
      n=mxm
      m=mxp
      nm=n*m
      n=n+NPlus
      m=m+MPlus
      nmNew=n*m
      do it=0,2
        n=TRank(it)
        KModP=KModMMax(it+1)
        if(it.eq.0) then
          if(KModP.gt.0) then
            allocate(p0(1,nm),px(n,KModP,nm),py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm))
            do i=1,NMolec
              jio=(i-1)*mxp
              ji=(i-1)*(mxp+MPlus)
              do j=1,mam(i)
                jio=jio+1
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( axm(1,jio), px(1,1,jio),m)
                call CopyVek( aym(1,jio), py(1,1,jio),m)
                call CopyVek(saxm(1,jio),spx(1,1,jio),m)
                call CopyVek(saym(1,jio),spy(1,1,jio),m)
              enddo
            enddo
          endif
          if(allocated(axm)) deallocate(axm,aym,saxm,saym)
          if(mw.gt.0) then
            allocate( axm(mw,nmNew), aym(mw,nmNew))
            allocate(saxm(mw,nmNew),saym(mw,nmNew))
          endif
          if(KModP.gt.0) then
            do i=1,NMolec
              ji=(i-1)*(mxp+MPlus)
              jio=(i-1)*mxp
              do j=1,mam(i)
                ji=ji+1
                jio=jio+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,jio), axm(1,ji),m)
                call CopyVek( py(1,1,jio), aym(1,ji),m)
                call CopyVek(spx(1,1,jio),saxm(1,ji),m)
                call CopyVek(spy(1,1,jio),saym(1,ji),m)
              enddo
            enddo
            deallocate(p0,px,py,spx,spy)
          endif
        else if(it.eq.1) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,nm), py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm),
     2                rx(n,KModP,nm), ry(n,KModP,nm),
     3               srx(n,KModP,nm),sry(n,KModP,nm))
            do i=1,NMolec
              jio=(i-1)*mxp
              ji=(i-1)*(mxp+MPlus)
              do j=1,mam(i)
                jio=jio+1
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( utx(1,1,jio), px(1,1,jio),m)
                call CopyVek( uty(1,1,jio), py(1,1,jio),m)
                call CopyVek(sutx(1,1,jio),spx(1,1,jio),m)
                call CopyVek(suty(1,1,jio),spy(1,1,jio),m)
                call CopyVek( urx(1,1,jio), rx(1,1,jio),m)
                call CopyVek( ury(1,1,jio), ry(1,1,jio),m)
                call CopyVek(surx(1,1,jio),srx(1,1,jio),m)
                call CopyVek(sury(1,1,jio),sry(1,1,jio),m)
              enddo
            enddo
          endif
          if(allocated(utx))
     1      deallocate(utx,uty,sutx,suty,urx,ury,surx,sury)
          if(mw.gt.0) then
            allocate( utx(n,mw,nmNew), uty(n,mw,nmNew))
            allocate(sutx(n,mw,nmNew),suty(n,mw,nmNew))
            allocate( urx(n,mw,nmNew), ury(n,mw,nmNew))
            allocate(surx(n,mw,nmNew),sury(n,mw,nmNew))
          endif
          if(KModP.gt.0) then
            do i=1,NMolec
              ji=(i-1)*(mxp+MPlus)
              jio=(i-1)*mxp
              do j=1,mam(i)
                ji=ji+1
                jio=jio+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,jio), utx(1,1,ji),m)
                call CopyVek( py(1,1,jio), uty(1,1,ji),m)
                call CopyVek(spx(1,1,jio),sutx(1,1,ji),m)
                call CopyVek(spy(1,1,jio),suty(1,1,ji),m)
                call CopyVek( rx(1,1,jio), urx(1,1,ji),m)
                call CopyVek( ry(1,1,jio), ury(1,1,ji),m)
                call CopyVek(srx(1,1,jio),surx(1,1,ji),m)
                call CopyVek(sry(1,1,jio),sury(1,1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy,rx,ry,srx,sry)
          endif
          if(allocated(durdx)) deallocate(durdx)
          allocate(durdx(18,mw,nmNew))
        else if(it.eq.2) then
           if(KModP.gt.0) then
            allocate( px(n,KModP,nm), py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm),
     2                rx(n,KModP,nm), ry(n,KModP,nm),
     3               srx(n,KModP,nm),sry(n,KModP,nm),
     4                qx(9,KModP,nm), qy(9,KModP,nm),
     5               sqx(9,KModP,nm),sqy(9,KModP,nm))
            do i=1,NMolec
              jio=(i-1)*mxp
              ji=(i-1)*(mxp+MPlus)
              do j=1,mam(i)
                jio=jio+1
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( ttx(1,1,jio), px(1,1,jio),m)
                call CopyVek( tty(1,1,jio), py(1,1,jio),m)
                call CopyVek(sttx(1,1,jio),spx(1,1,jio),m)
                call CopyVek(stty(1,1,jio),spy(1,1,jio),m)
                call CopyVek( tlx(1,1,jio), rx(1,1,jio),m)
                call CopyVek( tly(1,1,jio), ry(1,1,jio),m)
                call CopyVek(stlx(1,1,jio),srx(1,1,jio),m)
                call CopyVek(stly(1,1,jio),sry(1,1,jio),m)
                m=KModM(it+1,ji)*9
                call CopyVek( tsx(1,1,jio), qx(1,1,jio),m)
                call CopyVek( tsy(1,1,jio), qy(1,1,jio),m)
                call CopyVek(stsx(1,1,jio),sqx(1,1,jio),m)
                call CopyVek(stsy(1,1,jio),sqy(1,1,jio),m)
              enddo
            enddo
          endif
          if(allocated(ttx))
     1      deallocate(ttx,tty,sttx,stty,tlx,tly,stlx,stly,tsx,tsy,stsx,
     2                 stsy)
          if(mw.gt.0) then
            allocate( ttx(n,mw,nmNew), tty(n,mw,nmNew))
            allocate(sttx(n,mw,nmNew),stty(n,mw,nmNew))
            allocate( tlx(n,mw,nmNew), tly(n,mw,nmNew))
            allocate(stlx(n,mw,nmNew),stly(n,mw,nmNew))
            allocate( tsx(9,mw,nmNew), tsy(9,mw,nmNew))
            allocate(stsx(9,mw,nmNew),stsy(9,mw,nmNew))
          endif
          if(KModP.gt.0) then
            do i=1,NMolec
              ji=(i-1)*(mxp+MPlus)
              jio=(i-1)*mxp
              do j=1,mam(i)
                ji=ji+1
                jio=jio+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,jio), ttx(1,1,ji),m)
                call CopyVek( py(1,1,jio), tty(1,1,ji),m)
                call CopyVek(spx(1,1,jio),sttx(1,1,ji),m)
                call CopyVek(spy(1,1,jio),stty(1,1,ji),m)
                call CopyVek( rx(1,1,jio), tlx(1,1,ji),m)
                call CopyVek( ry(1,1,jio), tly(1,1,ji),m)
                call CopyVek(srx(1,1,jio),stlx(1,1,ji),m)
                call CopyVek(sry(1,1,jio),stly(1,1,ji),m)
                m=KModM(it+1,ji)*9
                call CopyVek( qx(1,1,jio), tsx(1,1,ji),m)
                call CopyVek( qy(1,1,jio), tsy(1,1,ji),m)
                call CopyVek(sqx(1,1,jio),stsx(1,1,ji),m)
                call CopyVek(sqy(1,1,jio),stsy(1,1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy,rx,ry,srx,sry,qx,qy,sqx,sqy)
          endif
        endif
      enddo
      mxm=mxm+NPlus
      mxp=mxp+MPlus
      mxpm=mxm*mxp
      return
      end
      subroutine DeallocateMoleculeParams
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(allocated(tt)) deallocate(tt,tl,ts,stt,stl,sts)
      do i=0,2
        if(i.eq.0) then
          if(allocated(axm)) deallocate(axm,aym,saxm,saym)
        else if(i.eq.1) then
          if(allocated(utx)) deallocate(utx,uty,sutx,suty,urx,ury,surx,
     1                                  sury)
        else if(i.eq.2) then
          if(allocated(ttx)) deallocate(ttx,tty,sttx,stty,tlx,tly,stlx,
     1                                  stly,tsx,tsy,stsx,stsy)
        endif
      enddo
      return
      end
      subroutine ReallocateMoleculeParams(NMolLast,NPosLast,KTLSN,KModN)
      use Molec_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      dimension px(:,:,:),py(:,:,:),spx(:,:,:),spy(:,:,:),KModN(*),
     1          rx(:,:,:),ry(:,:,:),srx(:,:,:),sry(:,:,:),
     2          qx(:,:,:),qy(:,:,:),sqx(:,:,:),sqy(:,:,:),
     3          KiP(:,:)
      allocatable px,py,spx,spy,rx,ry,srx,sry,qx,qy,sqx,sqy,KiP
      md=7
      if(KTLSN.gt.0) md=md+21
      mw=0
      do i=0,2
        kmod=max(KModN(i+1),KModMMax(i+1))
        n=TRank(i)
        mw=max(mw,kmod)
        md=md+2*n*kmod
        if(i.eq.0.and.kmod.ne.0) md=md+1
        if(i.gt.0) md=md+2*n*kmod
        if(i.gt.1) md=md+18*kmod
      enddo
      if(mw.gt.0) md=md+1
      if(md.le.mxdm) go to 9999
      nm=mxm*mxp
      if(KTLSN.gt.KTLSMax) then
        if(.not.allocated(tt))
     1    allocate( tt(6,nm), tl(6,nm), ts(9,nm),
     2             stt(6,nm),stl(6,nm),sts(9,nm))
        KTLSMax=KTLSN
      endif
      do it=0,2
        n=TRank(it)
        KModP=KModMMax(it+1)
        if(mw.le.KModP) cycle
        if(it.eq.0) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,nm),py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm))
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( axm(1,ji), px(1,1,ji),m)
                call CopyVek( aym(1,ji), py(1,1,ji),m)
                call CopyVek(saxm(1,ji),spx(1,1,ji),m)
                call CopyVek(saym(1,ji),spy(1,1,ji),m)
              enddo
            enddo
          endif
          if(allocated(axm)) deallocate(axm,aym,saxm,saym)
          if(mw.gt.0) then
            allocate( axm(mw,nm), aym(mw,nm))
            allocate(saxm(mw,nm),saym(mw,nm))
          endif
          if(KModP.gt.0) then
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,ji), axm(1,ji),m)
                call CopyVek( py(1,1,ji), aym(1,ji),m)
                call CopyVek(spx(1,1,ji),saxm(1,ji),m)
                call CopyVek(spy(1,1,ji),saym(1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy)
          endif
        else if(it.eq.1) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,nm), py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm),
     2                rx(n,KModP,nm), ry(n,KModP,nm),
     3               srx(n,KModP,nm),sry(n,KModP,nm))
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( utx(1,1,ji), px(1,1,ji),m)
                call CopyVek( uty(1,1,ji), py(1,1,ji),m)
                call CopyVek(sutx(1,1,ji),spx(1,1,ji),m)
                call CopyVek(suty(1,1,ji),spy(1,1,ji),m)
                call CopyVek( urx(1,1,ji), rx(1,1,ji),m)
                call CopyVek( ury(1,1,ji), ry(1,1,ji),m)
                call CopyVek(surx(1,1,ji),srx(1,1,ji),m)
                call CopyVek(sury(1,1,ji),sry(1,1,ji),m)
              enddo
            enddo
          endif
          if(allocated(utx))
     1      deallocate(utx,uty,sutx,suty,urx,ury,surx,sury)
          if(mw.gt.0) then
            allocate( utx(n,mw,nm), uty(n,mw,nm))
            allocate(sutx(n,mw,nm),suty(n,mw,nm))
            allocate( urx(n,mw,nm), ury(n,mw,nm))
            allocate(surx(n,mw,nm),sury(n,mw,nm))
          endif
          if(KModP.gt.0) then
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,ji), utx(1,1,ji),m)
                call CopyVek( py(1,1,ji), uty(1,1,ji),m)
                call CopyVek(spx(1,1,ji),sutx(1,1,ji),m)
                call CopyVek(spy(1,1,ji),suty(1,1,ji),m)
                call CopyVek( rx(1,1,ji), urx(1,1,ji),m)
                call CopyVek( ry(1,1,ji), ury(1,1,ji),m)
                call CopyVek(srx(1,1,ji),surx(1,1,ji),m)
                call CopyVek(sry(1,1,ji),sury(1,1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy,rx,ry,srx,sry)
          endif
          if(allocated(durdx)) deallocate(durdx)
          allocate(durdx(18,mw,nm))
        else if(it.eq.2) then
          if(KModP.gt.0) then
            allocate( px(n,KModP,nm), py(n,KModP,nm),
     1               spx(n,KModP,nm),spy(n,KModP,nm),
     2                rx(n,KModP,nm), ry(n,KModP,nm),
     3               srx(n,KModP,nm),sry(n,KModP,nm),
     4                qx(9,KModP,nm), qy(9,KModP,nm),
     5               sqx(9,KModP,nm),sqy(9,KModP,nm))
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( ttx(1,1,ji), px(1,1,ji),m)
                call CopyVek( tty(1,1,ji), py(1,1,ji),m)
                call CopyVek(sttx(1,1,ji),spx(1,1,ji),m)
                call CopyVek(stty(1,1,ji),spy(1,1,ji),m)
                call CopyVek( tlx(1,1,ji), rx(1,1,ji),m)
                call CopyVek( tly(1,1,ji), ry(1,1,ji),m)
                call CopyVek(stlx(1,1,ji),srx(1,1,ji),m)
                call CopyVek(stly(1,1,ji),sry(1,1,ji),m)
                m=KModM(it+1,ji)*9
                call CopyVek( tsx(1,1,ji), qx(1,1,ji),m)
                call CopyVek( tsy(1,1,ji), qy(1,1,ji),m)
                call CopyVek(stsx(1,1,ji),sqx(1,1,ji),m)
                call CopyVek(stsy(1,1,ji),sqy(1,1,ji),m)
              enddo
            enddo
          endif
          if(allocated(ttx))
     1      deallocate(ttx,tty,sttx,stty,tlx,tly,stlx,stly,tsx,tsy,stsx,
     2                 stsy)
          if(mw.gt.0) then
            allocate( ttx(n,mw,nm), tty(n,mw,nm))
            allocate(sttx(n,mw,nm),stty(n,mw,nm))
            allocate( tlx(n,mw,nm), tly(n,mw,nm))
            allocate(stlx(n,mw,nm),stly(n,mw,nm))
            allocate( tsx(9,mw,nm), tsy(9,mw,nm))
            allocate(stsx(9,mw,nm),stsy(9,mw,nm))
          endif
          if(KModP.gt.0) then
            do i=1,NMolLast
              ji=(i-1)*mxp
              if(i.eq.NMolLast) then
                jk=min(NPosLast,mam(i))
              else
                jk=mam(i)
              endif
              do j=1,jk
                ji=ji+1
                m=KModM(it+1,ji)*n
                call CopyVek( px(1,1,ji), ttx(1,1,ji),m)
                call CopyVek( py(1,1,ji), tty(1,1,ji),m)
                call CopyVek(spx(1,1,ji),sttx(1,1,ji),m)
                call CopyVek(spy(1,1,ji),stty(1,1,ji),m)
                call CopyVek( rx(1,1,ji), tlx(1,1,ji),m)
                call CopyVek( ry(1,1,ji), tly(1,1,ji),m)
                call CopyVek(srx(1,1,ji),stlx(1,1,ji),m)
                call CopyVek(sry(1,1,ji),stly(1,1,ji),m)
                m=KModM(it+1,ji)*9
                call CopyVek( qx(1,1,ji), tsx(1,1,ji),m)
                call CopyVek( qy(1,1,ji), tsy(1,1,ji),m)
                call CopyVek(sqx(1,1,ji),stsx(1,1,ji),m)
                call CopyVek(sqy(1,1,ji),stsy(1,1,ji),m)
              enddo
            enddo
            deallocate(px,py,spx,spy,rx,ry,srx,sry,qx,qy,sqx,sqy)
          endif
        endif
      enddo
      allocate(KiP(mxdm,nm))
      do i=1,NMolLast
        ji=(i-1)*mxp
        if(i.eq.NMolLast) then
          jk=min(NPosLast,mam(i))
        else
          jk=mam(i)
        endif
        do j=1,jk
          ji=ji+1
          call CopyVekI(KiMol(1,ji),KiP(1,ji),mxdm)
        enddo
      enddo
      deallocate(KiMol)
      allocate(KiMol(md,nm))
      do i=1,NMolLast
        ji=(i-1)*mxp
        if(i.eq.NMolLast) then
          jk=min(NPosLast,mam(i))
        else
          jk=mam(i)
        endif
        do j=1,jk
          ji=ji+1
          call CopyVekI(KiP(1,ji),KiMol(1,ji),mxdm)
          call SetIntArrayTo(KiMol(mxdm+1,ji),md-mxdm,0)
        enddo
      enddo
      deallocate(KiP)
      mxdm=md
      do i=1,3
        KModMMax(i)=max(KModMMax(i),KModN(i))
      enddo
9999  return
      end
      subroutine ReallocateXYZAMode(NXYZAModeNew,MXYZAModeNew)
      use Atoms_mod
      character*8 LXYZAModeO(:)
      dimension MXYZAModeO(:),XYZAModeO(:,:)
      allocatable LXYZAModeO,MXYZAModeO,XYZAModeO
      if(NXYZAModeNew.le.NXYZAModeMax.and.
     1   MXYZAModeNew.le.MXYZAModeMax) go to 9999
      if(NXYZAModeMax.gt.0) then
        allocate(MXYZAModeO(NXYZAMode),LXYZAModeO(NXYZAMode),
     1           XYZAModeO(MXYZAModeMax,NXYZAMode))
        do i=1,NXYZAMode
          MXYZAModeO(i)=MXYZAMode(i)
          LXYZAModeO(i)=LXYZAMode(i)
          call CopyVek(XYZAMode(1,i),XYZAModeO(1,i),MXYZAMode(i))
        enddo
      endif
      if(allocated(MXYZAMode))
     1  deallocate(MXYZAMode,LXYZAMode,XYZAMode)
      n=max(NXYZAModeNew,NXYZAModeMax)
      m=max(MXYZAModeNew,MXYZAModeMax)
      allocate(MXYZAMode(n),LXYZAMode(n),XYZAMode(m,n))
      if(NXYZAModeMax.gt.0) then
        do i=1,NXYZAMode
          MXYZAMode(i)=MXYZAModeO(i)
          LXYZAMode(i)=LXYZAModeO(i)
          call CopyVek(XYZAModeO(1,i),XYZAMode(1,i),MXYZAMode(i))
        enddo
        if(allocated(MXYZAModeO))
     1    deallocate(MXYZAModeO,LXYZAModeO,XYZAModeO)
      endif
      NXYZAModeMax=n
      MXYZAModeMax=m
9999  return
      end
      subroutine ReallocateMagAMode(NMagAModeNew,MMagAModeNew)
      use Atoms_mod
      character*8 LMagAModeO(:)
      dimension MMagAModeO(:)
      real MagAModeO(:,:)
      allocatable LMagAModeO,MMagAModeO,MagAModeO
      if(NMagAModeNew.le.NMagAModeMax.and.
     1   MMagAModeNew.le.MMagAModeMax) go to 9999
      if(NMagAModeMax.gt.0) then
        allocate(MMagAModeO(NMagAMode),LMagAModeO(NMagAMode),
     1           MagAModeO(MMagAModeMax,NMagAMode))
        do i=1,NMagAMode
          MMagAModeO(i)=MMagAMode(i)
          LMagAModeO(i)=LMagAMode(i)
          call CopyVek(MagAMode(1,i),MagAModeO(1,i),MMagAMode(i))
        enddo
      endif
      if(allocated(MMagAMode))
     1  deallocate(MMagAMode,LMagAMode,MagAMode)
      n=max(NMagAModeNew,NMagAModeMax)
      m=max(MMagAModeNew,MMagAModeMax)
      allocate(MMagAMode(n),LMagAMode(n),MagAMode(m,n))
      if(NMagAModeMax.gt.0) then
        do i=1,NMagAMode
          MMagAMode(i)=MMagAModeO(i)
          LMagAMode(i)=LMagAModeO(i)
          call CopyVek(MagAModeO(1,i),MagAMode(1,i),MMagAMode(i))
        enddo
        if(allocated(MMagAModeO))
     1    deallocate(MMagAModeO,LMagAModeO,MagAModeO)
      endif
      NMagAModeMax=n
      MMagAModeMax=m
9999  return
      end
      subroutine ReallocateAtXYZMode(NPlus)
      use Atoms_mod
      character*8 LAtXYZModeO(:,:)
      integer MAtXYZModeO(:),NMAtXYZModeO(:),JAtXYZModeO(:),
     1        IAtXYZModeO(:,:),KiAtXYZModeO(:,:)
      real AtXYZModeO(:,:)
      allocatable LAtXYZModeO,MAtXYZModeO,NMAtXYZModeO,IAtXYZModeO,
     1            KiAtXYZModeO,AtXYZModeO,JAtXYZModeO
      if(NAtXYZMode.lt.NAtXYZModeMax) go to 9999
      if(NAtXYZModeMax.gt.0) then
        allocate(MAtXYZModeO(NAtXYZMode),NMAtXYZModeO(NAtXYZMode),
     1           IAtXYZModeO(MXYZAModeMax/3,NAtXYZMode),
     2           LAtXYZModeO(MXYZAModeMax,NAtXYZMode),
     3           KiAtXYZModeO(MXYZAModeMax,NAtXYZMode),
     4           AtXYZModeO(MXYZAModeMax,NAtXYZMode),
     5           JAtXYZModeO(NAtXYZMode))
        do i=1,NAtXYZMode
          MAtXYZModeO(i)=MAtXYZMode(i)
          JAtXYZModeO(i)=JAtXYZMode(i)
          NMAtXYZModeO(i)=NMAtXYZMode(i)
          call CopyVekI(IAtXYZMode(1,i),IAtXYZModeO(1,i),
     1                  MAtXYZMode(i))
          do j=1,NMAtXYZMode(i)
            LAtXYZModeO(j,i)=LAtXYZMode(j,i)
          enddo
          call CopyVekI(KiAtXYZMode(1,i),KiAtXYZModeO(1,i),
     1                  NMAtXYZMode(i))
          call CopyVek(AtXYZMode(1,i),AtXYZModeO(1,i),
     1                 NMAtXYZMode(i))
        enddo
      endif
      if(allocated(MAtXYZMode))
     1  deallocate(MAtXYZMode,NMAtXYZMode,IAtXYZMode,LAtXYZMode,
     2             KiAtXYZMode,AtXYZMode,SAtXYZMode,FAtXYZMode,
     3             KAtXYZMode,JAtXYZMode)
      n=NAtXYZModeMax+NPlus
      allocate(MAtXYZMode(n),NMAtXYZMode(n),
     1         IAtXYZMode(MXYZAModeMax/3,n),
     2         LAtXYZMode(MXYZAModeMax,n),
     3         KiAtXYZMode(MXYZAModeMax,n),
     4         AtXYZMode(MXYZAModeMax,n),
     5         SAtXYZMode(MXYZAModeMax,n),
     6         FAtXYZMode(MXYZAModeMax,n),
     7         KAtXYZMode(MXYZAModeMax,n),
     8         JAtXYZMode(n))
      if(NAtXYZModeMax.gt.0) then
        do i=1,NAtXYZMode
          MAtXYZMode(i)=MAtXYZModeO(i)
          JAtXYZMode(i)=JAtXYZModeO(i)
          NMAtXYZMode(i)=NMAtXYZModeO(i)
          call CopyVekI(IAtXYZModeO(1,i),IAtXYZMode(1,i),
     1                  MAtXYZMode(i))
          do j=1,NMAtXYZMode(i)
            LAtXYZMode(j,i)=LAtXYZModeO(j,i)
          enddo
          call CopyVekI(KiAtXYZModeO(1,i),KiAtXYZMode(1,i),
     1                  NMAtXYZMode(i))
          call CopyVek(AtXYZModeO(1,i),AtXYZMode(1,i),
     1                 NMAtXYZMode(i))
        enddo
        deallocate(MAtXYZModeO,NMAtXYZModeO,IAtXYZModeO,LAtXYZModeO,
     1             KiAtXYZModeO,AtXYZModeO,JAtXYZModeO)
      endif
      NAtXYZModeMax=n
9999  return
      end
      subroutine ReallocateAtMagMode(NPlus)
      use Atoms_mod
      character*8 LAtMagModeO(:,:)
      integer MAtMagModeO(:),NMAtMagModeO(:),JAtMagModeO(:),
     1        IAtMagModeO(:,:),KiAtMagModeO(:,:)
      real AtMagModeO(:,:)
      allocatable LAtMagModeO,MAtMagModeO,NMAtMagModeO,IAtMagModeO,
     1            KiAtMagModeO,AtMagModeO,JAtMagModeO
      if(NAtMagMode.lt.NAtMagModeMax) go to 9999
      if(NAtMagModeMax.gt.0) then
        allocate(MAtMagModeO(NAtMagMode),NMAtMagModeO(NAtMagMode),
     1           IAtMagModeO(MMagAModeMax/3,NAtMagMode),
     2           LAtMagModeO(MMagAModeMax,NAtMagMode),
     3           KiAtMagModeO(MMagAModeMax,NAtMagMode),
     4           AtMagModeO(MMagAModeMax,NAtMagMode),
     5           JAtMagModeO(NAtMagMode))
        do i=1,NAtMagMode
          MAtMagModeO(i)=MAtMagMode(i)
          JAtMagModeO(i)=JAtMagMode(i)
          NMAtMagModeO(i)=NMAtMagMode(i)
          call CopyVekI(IAtMagMode(1,i),IAtMagModeO(1,i),
     1                  MAtMagMode(i))
          do j=1,NMAtMagMode(i)
            LAtMagModeO(j,i)=LAtMagMode(j,i)
          enddo
          call CopyVekI(KiAtMagMode(1,i),KiAtMagModeO(1,i),
     1                  NMAtMagMode(i))
          call CopyVek(AtMagMode(1,i),AtMagModeO(1,i),
     1                 NMAtMagMode(i))
        enddo
      endif
      if(allocated(MAtMagMode))
     1  deallocate(MAtMagMode,NMAtMagMode,IAtMagMode,LAtMagMode,
     2             KiAtMagMode,AtMagMode,SAtMagMode,FAtMagMode,
     3             KAtMagMode,JAtMagMode)
      n=NAtMagModeMax+NPlus
      allocate(MAtMagMode(n),NMAtMagMode(n),
     1         IAtMagMode(MMagAModeMax/3,n),
     2         LAtMagMode(MMagAModeMax,n),
     3         KiAtMagMode(MMagAModeMax,n),
     4         AtMagMode(MMagAModeMax,n),
     5         SAtMagMode(MMagAModeMax,n),
     6         FAtMagMode(MMagAModeMax,n),
     7         KAtMagMode(MMagAModeMax,n),
     8         JAtMagMode(n))
      if(NAtMagModeMax.gt.0) then
        do i=1,NAtMagMode
          MAtMagMode(i)=MAtMagModeO(i)
          JAtMagMode(i)=JAtMagModeO(i)
          NMAtMagMode(i)=NMAtMagModeO(i)
          call CopyVekI(IAtMagModeO(1,i),IAtMagMode(1,i),
     1                  MAtMagMode(i))
          do j=1,NMAtMagMode(i)
            LAtMagMode(j,i)=LAtMagModeO(j,i)
          enddo
          call CopyVekI(KiAtMagModeO(1,i),KiAtMagMode(1,i),
     1                  NMAtMagMode(i))
          call CopyVek(AtMagModeO(1,i),AtMagMode(1,i),
     1                 NMAtMagMode(i))
        enddo
        deallocate(MAtMagModeO,NMAtMagModeO,IAtMagModeO,KiAtMagModeO,
     1             AtMagModeO,JAtMagModeO)
      endif
      NAtMagModeMax=n
9999  return
      end
      subroutine ReallocateStoreSymmetry(IStore,NSymmP,NLattVecP,NDimP,
     1                                   NExtRefCondP)
      use Basic_mod
      use MatStore_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      if(IStore.le.NStore.and.NDimP.le.NDimStoreMax.and.
     1   NLattVecP.le.NLattVecStoreMax.and.
     2     NSymmP.le.NSymmStoreMax.and.
     3     NExtRefCond.le.NExtRefCondStoreMax) go to 9000
      if(NStore.gt.0) then
        ns=NSymmStoreMax
        nvt=NLattVecStoreMax
        m=NExtRefCondStoreMax
        n=NStore
        allocate(NSymmStoreO(n),NLattVecStoreO(n),
     1           NDimStoreO(n),NExtRefCondStoreO(n),
     2           RM6StoreO(36,ns,n),S6StoreO(6,ns,n),
     3           RMStoreO(9,ns,n),ProjStoreO(6,ns,n),
     3           VT6StoreO(6,nvt,n),
     4           ExtRefGroupStoreO(36,m,n),
     5           ExtRefCondStoreO(7,m,n))
        do i=1,NStore
          do is=1,NSymmStore(i)
            call CopyMat(RM6Store(1,is,i),RM6StoreO(1,is,i),
     1                   NDimStore(i))
            call CopyMat(RMStore(1,is,i),RMStoreO(1,is,i),3)
            call CopyVek( S6Store(1,is,i), S6StoreO(1,is,i),
     1                   NDimStore(i))
            call CopyVek(ProjStore(1,is,i),ProjStoreO(1,is,i),
     1                   NDimStore(i))
          enddo
          do ivt=1,NLattVecStore(i)
            call CopyVek(VT6Store(1,ivt,i),VT6StoreO(1,ivt,i),
     1                   NDimStore(i))
          enddo
          do iext=1,NExtRefCondStore(i)
            call CopyMat(ExtRefGroupStore (1,iext,i),
     1                   ExtRefGroupStoreO(1,iext,i),NDimStore(i))
            call CopyVek(ExtRefCondStore (1,iext,i),
     1                   ExtRefCondStoreO(1,iext,i),NDimStore(i)+1)
          enddo
          NSymmStoreO(i)=NSymmStore(i)
          NLattVecStoreO(i)=NLattVecStore(i)
          NDimStoreO(i)=NDimStore(i)
          NExtRefCondStoreO(i)=NExtRefCondStore(i)
        enddo
      endif
      if(allocated(NSymmStore))
     1  deallocate(NSymmStore,NLattVecStore,NDimStore,NExtRefCondStore,
     2             RM6Store,RMStore,S6Store,ProjStore,VT6Store,
     3             ExtRefGroupStore,ExtRefCondStore)
      ns=max(NSymmStoreMax,NSymmP)
      nvt=max(NLattVecStoreMax,NLattVecP)
      nd=max(NDimStoreMax,NDimP)
      n=max(NStore,IStore)
      m=max(NExtRefCondStoreMax,NExtRefCond)
      allocate(NSymmStore(n),NLattVecStore(n),
     1         NDimStore(n),NExtRefCondStore(n),
     2         RM6Store(36,ns,n),RMStore(9,ns,n),S6Store(6,ns,n),
     3         ProjStore(6,ns,n),
     4         VT6Store(6,nvt,n),
     5         ExtRefGroupStore(36,m,n),
     6         ExtRefCondStore(7,m,n))
      if(NStore.gt.0) then
        do i=1,NStore
          if(i.eq.IStore) cycle
          do is=1,NSymmStoreO(i)
            call CopyMat(RM6StoreO(1,is,i),RM6Store(1,is,i),
     1                   NDimStoreO(i))
            call CopyMat(RMStoreO(1,is,i),RMStore(1,is,i),3)
            call CopyVek( S6StoreO(1,is,i), S6Store(1,is,i),
     1                   NDimStoreO(i))
            call CopyVek(ProjStoreO(1,is,i),ProjStore(1,is,i),
     1                   NDimStoreO(i))
          enddo
          do ivt=1,NLattVecStoreO(i)
            call CopyVek(VT6StoreO(1,ivt,i),VT6Store(1,ivt,i),
     1                   NDimStoreO(i))
          enddo
          do iext=1,NExtRefCondStoreO(i)
            call CopyMat(ExtRefGroupStoreO(1,iext,i),
     1                   ExtRefGroupStore (1,iext,i),NDimStoreO(i))
            call CopyVek(ExtRefCondStoreO(1,iext,i),
     1                   ExtRefCondStore (1,iext,i),NDimStoreO(i)+1)
          enddo
          NSymmStore(i)=NSymmStoreO(i)
          NLattVecStore(i)=NLattVecStoreO(i)
          NDimStore(i)=NDimStoreO(i)
          NExtRefCondStore(i)=NExtRefCondStoreO(i)
        enddo
        deallocate(NSymmStoreO,NLattVecStoreO,NDimStoreO,
     1             NExtRefCondStoreO,
     2             RM6StoreO,RMStoreO,S6StoreO,VT6StoreO,ProjStoreO,
     3             ExtRefGroupStoreO,ExtRefCondStoreO)
      endif
9000  NSymmStore(IStore)=NSymmP
      NLattVecStore(IStore)=NLattVecP
      NDimStore(IStore)=NDimP
      NSymmStoreMax=ns
      NLattVecStoreMax=nvt
      NDimStoreMax=nd
      NExtRefCondStoreMax=m
      NStore=max(NStore,IStore)
      return
      end
      subroutine ReallocateED(KRefB)
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer, allocatable :: NEDZoneO(:),EDIntStepsO(:),
     1                        EDThreadsO(:),EDTiltcorrO(:),
     2                        EDGeometryIEDTO(:)
      real, allocatable :: OrMatEDZoneO(:,:,:),GMaxEDZoneO(:),
     1                     SGMaxMEDZoneO(:),SGMaxREDZoneO(:),
     2                     CSGMaxREDZoneO(:),OmEDZoneO(:)
      if(KRefB.le.NRefBlockED) go to 9999
      if(NRefBlockED.gt.0) then
        allocate(NEDZoneO(NRefBlock),EDIntStepsO(NRefBlock),
     1           OrMatEDZoneO(3,3,NRefBlock),GMaxEDZoneO(NRefBlock),
     2           SGMaxMEDZoneO(NRefBlock),SGMaxREDZoneO(NRefBlock),
     3           CSGMaxREDZoneO(NRefBlock),OmEDZoneO(NRefBlock),
     4           EDThreadsO(NRefBlock),EDTiltcorrO(NRefBlock),
     5           EDGeometryIEDTO(NRefBlock))
        do i=1,NRefBlockED
          if(RadiationRefBlock(i).ne.ElectronRadiation) cycle
          NEDZoneO(i)=NEDZone(i)
          EDIntStepsO(i)=EDIntSteps(i)
          EDThreadsO(i)=EDThreads(i)
          EDTiltcorrO(i)=EDTiltcorr(i)
          EDGeometryIEDTO(i)=EDGeometryIEDT(i)
          OrMatEDZoneO(1:3,1:3,i)=OrMatEDZone(1:3,1:3,i)
          GMaxEDZoneO(i)=GMaxEDZone(i)
          SGMaxMEDZoneO(i)=SGMaxMEDZone(i)
          SGMaxREDZoneO(i)=SGMaxREDZone(i)
          CSGMaxREDZoneO(i)=CSGMaxREDZone(i)
          OmEDZoneO(i)=OmEDZone(i)
        enddo
      endif
      if(Allocated(NEDZone))
     1  deallocate(NEDZone,EDIntSteps,OrMatEDZone,GMaxEDZone,
     2             SGMaxMEDZone,SGMaxREDZone,CSGMaxREDZone,OmEDZone,
     3             EDThreads,EDTiltcorr,EDGeometryIEDT)
      allocate(NEDZone(KRefB),EDIntSteps(KRefB),
     1         OrMatEDZone(3,3,KRefB),GMaxEDZone(KRefB),
     2         SGMaxMEDZone(KRefB),SGMaxREDZone(KRefB),
     3         CSGMaxREDZone(KRefB),OmEDZone(KRefB),
     4         EDThreads(KRefB),EDTiltcorr(KRefB),EDGeometryIEDT(KRefB))
      if(NRefBlockED.gt.0) then
        do i=1,NRefBlockED
          if(RadiationRefBlock(i).ne.ElectronRadiation) cycle
          NEDZone(i)=NEDZoneO(i)
          EDIntSteps(i)=EDIntStepsO(i)
          EDThreads(i)=EDThreadsO(i)
          EDTiltcorr(i)=EDTiltcorrO(i)
          EDGeometryIEDT(i)=EDGeometryIEDTO(i)
          OrMatEDZone(1:3,1:3,i)=OrMatEDZoneO(1:3,1:3,i)
          GMaxEDZone(i)=GMaxEDZoneO(i)
          SGMaxMEDZone(i)=SGMaxMEDZoneO(i)
          SGMaxREDZone(i)=SGMaxREDZoneO(i)
          CSGMaxREDZone(i)=CSGMaxREDZoneO(i)
          OmEDZone(i)=OmEDZoneO(i)
        enddo
        deallocate(NEDZoneO,EDIntStepsO,OrMatEDZoneO,GMaxEDZoneO,
     1             SGMaxMEDZoneO,SGMaxREDZoneO,CSGMaxREDZoneO,OmEDZoneO,
     2             EDThreadsO,EDTiltcorrO,EDGeometryIEDTO)
      endif
9999  return
      end
      subroutine ReallocateEDZones(NPlus)
      use EDZones_mod
      include 'fepc.cmn'
      include 'basic.cmn'
      include 'datred.cmn'
      integer, allocatable ::  KiEDO(:,:,:),NThickEDZoneO(:,:)
      real, allocatable :: HEDZoneO(:,:,:),AlphaEDZoneO(:,:),
     1                     BetaEDZoneO(:,:),PrAngEDZoneO(:,:),
     2                     ScEDZoneO(:,:),ScEDZoneSO(:,:),
     2                     RFacEDZoneO(:,:),
     3                     ThickEDZoneO(:,:),ThickEDZoneSO(:,:),
     4                     XNormEDZoneO(:,:),XNormEDZoneSO(:,:),
     5                     YNormEDZoneO(:,:),YNormEDZoneSO(:,:),
     6                     PhiEDZoneO(:,:),PhiEDZoneSO(:,:),
     7                     ThetaEDZoneO(:,:),ThetaEDZoneSO(:,:)
      logical, allocatable :: UseEDZoneO(:,:)
      if(allocated(KiED)) then
        KRefBlockO=ubound(KiED,3)
      else
        KRefBlockO=0
      endif
      if(NPlus.le.0.and.KRefBlock.le.KRefBlockO) go to 9999
      NEDZoneAll=0
      do i=1,KRefBlockO
        NEDZoneAll=NEDZoneAll+NEDZone(i)
      enddo
      if(NEDZoneAll.gt.0) then
        n=NEDZoneAll
        m=KRefBlockO
        allocate(HEDZoneO(3,n,m),RFacEDZoneO(n,m),
     1           AlphaEDZoneO(n,m),NThickEDZoneO(n,m),
     2           BetaEDZoneO(n,m),PrAngEDZoneO(n,m),
     3           ScEDZoneO(n,m),ScEDZoneSO(n,m),
     4           ThickEDZoneO(n,m),ThickEDZoneSO(n,m),
     5           XNormEDZoneO(n,m),XNormEDZoneSO(n,m),
     6           YNormEDZoneO(n,m),YNormEDZoneSO(n,m),
     7           PhiEDZoneO(n,m),PhiEDZoneSO(n,m),
     8           ThetaEDZoneO(n,m),ThetaEDZoneSO(n,m),
     9           KiEDO(EDNPar,n,m),UseEDZoneO(n,m))
        do KRefB=1,KRefBlockO
          n=NEDZone(KRefB)
          call CopyVek(HEDZone(1,1,KRefB),HEDZoneO(1,1,KRefB),3*n)
          call CopyVek(RFacEDZone(1,KRefB),RFacEDZoneO(1,KRefB),n)
          call CopyVek(AlphaEDZone(1,KRefB),AlphaEDZoneO(1,KRefB),n)
          call CopyVek(BetaEDZone(1,KRefB),BetaEDZoneO(1,KRefB),n)
          call CopyVek(PrAngEDZone(1,KRefB),PrAngEDZoneO(1,KRefB),n)
          call CopyVek(ScEDZone(1,KRefB),ScEDZoneO(1,KRefB),n)
          call CopyVek(ScEDZoneS(1,KRefB),ScEDZoneSO(1,KRefB),n)
          call CopyVek(ThickEDZone(1,KRefB),ThickEDZoneO(1,KRefB),n)
          call CopyVek(ThickEDZoneS(1,KRefB),ThickEDZoneSO(1,KRefB),n)
          call CopyVek(XNormEDZone(1,KRefB),XNormEDZoneO(1,KRefB),n)
          call CopyVek(XNormEDZoneS(1,KRefB),XNormEDZoneSO(1,KRefB),n)
          call CopyVek(YNormEDZone(1,KRefB),YNormEDZoneO(1,KRefB),n)
          call CopyVek(YNormEDZoneS(1,KRefB),YNormEDZoneSO(1,KRefB),n)
          call CopyVek(PhiEDZone(1,KRefB),PhiEDZoneO(1,KRefB),n)
          call CopyVek(PhiEDZoneS(1,KRefB),PhiEDZoneSO(1,KRefB),n)
          call CopyVek(ThetaEDZone(1,KRefB),ThetaEDZoneO(1,KRefB),n)
          call CopyVek(ThetaEDZoneS(1,KRefB),ThetaEDZoneSO(1,KRefB),n)
          call CopyVekI(KiED(1,1,KRefB),KiEDO(1,1,KRefB),EDNPar*n)
          call CopyVekI(NThickEDZone(1,KRefB),NThickEDZoneO(1,KRefB),n)
          do i=1,n
            UseEDZoneO(i,KRefB)=UseEDZone(i,KRefB)
          enddo
        enddo
      endif
      if(allocated(KiED))
     1  deallocate(HEDZone,RFacEDZone,AlphaEDZone,
     2             BetaEDZone,PrAngEDZone,
     3             ScEDZone,ScEDZoneS,NThickEDZone,
     4             ThickEDZone,ThickEDZoneS,
     5             XNormEDZone,XNormEDZoneS,
     6             YNormEDZone,YNormEDZoneS,
     7             PhiEDZone,PhiEDZoneS,
     8             ThetaEDZone,ThetaEDZoneS,
     9             KiED,UseEdZone)
      n=NMaxEDZone+NPlus
      m=NRefBlock
      allocate(HEDZone(3,n,m),RFacEDZone(n,m),AlphaEDZone(n,m),
     1         BetaEDZone(n,m),PrAngEDZone(n,m),
     2         ScEDZone(n,m),ScEDZoneS(n,m),NThickEDZone(n,m),
     3         ThickEDZone(n,m),ThickEDZoneS(n,m),
     4         XNormEDZone(n,m),XNormEDZoneS(n,m),
     5         YNormEDZone(n,m),YNormEDZoneS(n,m),
     6         PhiEDZone(n,m),PhiEDZoneS(n,m),
     7         ThetaEDZone(n,m),ThetaEDZoneS(n,m),
     8         KiED(EDNPar,n,m),UseEdZone(n,m))
      NMaxEDZone=n
      if(NEDZoneAll.gt.0) then
        n=NEDZoneAll
        m=KRefBlockO
        do KRefB=1,KRefBlockO
          n=NEDZone(KRefB)
          call CopyVek(HEDZoneO(1,1,KRefB),HEDZone(1,1,KRefB),3*n)
          call CopyVek(RFacEDZoneO(1,KRefB),RFacEDZone(1,KRefB),n)
          call CopyVek(AlphaEDZoneO(1,KRefB),AlphaEDZone(1,KRefB),n)
          call CopyVek(BetaEDZoneO(1,KRefB),BetaEDZone(1,KRefB),n)
          call CopyVek(PrAngEDZoneO(1,KRefB),PrAngEDZone(1,KRefB),n)
          call CopyVek(ScEDZoneO(1,KRefB),ScEDZone(1,KRefB),n)
          call CopyVek(ScEDZoneSO(1,KRefB),ScEDZoneS(1,KRefB),n)
          call CopyVek(ThickEDZoneO(1,KRefB),ThickEDZone(1,KRefB),n)
          call CopyVek(ThickEDZoneSO(1,KRefB),ThickEDZoneS(1,KRefB),n)
          call CopyVek(XNormEDZoneO(1,KRefB),XNormEDZone(1,KRefB),n)
          call CopyVek(XNormEDZoneSO(1,KRefB),XNormEDZoneS(1,KRefB),n)
          call CopyVek(YNormEDZoneO(1,KRefB),YNormEDZone(1,KRefB),n)
          call CopyVek(YNormEDZoneSO(1,KRefB),YNormEDZoneS(1,KRefB),n)
          call CopyVek(PhiEDZoneO(1,KRefB),PhiEDZone(1,KRefB),n)
          call CopyVek(PhiEDZoneSO(1,KRefB),PhiEDZoneS(1,KRefB),n)
          call CopyVek(ThetaEDZoneO(1,KRefB),ThetaEDZone(1,KRefB),n)
          call CopyVek(ThetaEDZoneSO(1,KRefB),ThetaEDZoneS(1,KRefB),n)
          call CopyVekI(KiEDO(1,1,KRefB),KiED(1,1,KRefB),EDNPar*n)
          call CopyVekI(NThickEDZoneO(1,KRefB),NThickEDZone(1,KRefB),n)
          do i=1,n
            UseEDZone(i,KRefB)=UseEDZoneO(i,KRefB)
          enddo
        enddo
        deallocate(HEDZoneO,RFacEDZoneO,AlphaEDZoneO,
     1             BetaEDZoneO,PrAngEDZoneO,
     2             ScEDZoneO,ScEDZoneSO,NThickEDZoneO,
     3             ThickEDZoneO,ThickEDZoneSO,
     4             XNormEDZoneO,XNormEDZoneSO,
     5             YNormEDZoneO,YNormEDZoneSO,
     6             PhiEDZoneO,PhiEDZoneSO,
     7             ThetaEDZoneO,ThetaEDZoneSO,
     8             KiEDO,UseEDZoneO)
      endif
      go to 9999
      entry DeallocateEDZones
      if(allocated(KiED))
     1  deallocate(HEDZone,RFacEDZone,AlphaEDZone,
     2             BetaEDZone,PrAngEDZone,
     3             ScEDZone,ScEDZoneS,NThickEDZone,
     4             ThickEDZone,ThickEDZoneS,
     5             XNormEDZone,XNormEDZoneS,
     6             YNormEDZone,YNormEDZoneS,
     7             PhiEDZone,PhiEDZoneS,
     8             ThetaEDZone,ThetaEDZoneS,
     9             KiED,UseEDZone)
      NMaxEDZone=0
9999  return
      end
      subroutine ReallocateGrtDraw(NPlus)
      use Grapht_mod
      integer, allocatable  :: DrawColorO(:),DrawParamO(:)
      character*80, allocatable :: DrawSymStO(:),DrawSymStUO(:)
      character*256, allocatable ::  DrawAtomO(:,:)
      if(DrawN+NPlus.lt.DrawNMax) go to 9999
      if(DrawN.gt.0) then
        allocate(DrawAtomO(4,DrawN),DrawSymStO(DrawN),
     1           DrawSymStUO(DrawN),DrawColorO(DrawN),
     2           DrawParamO(DrawN))
        DrawAtomO(1:4,1:DrawN)=DrawAtom(1:4,1:DrawN)
        DrawSymStO(1:DrawN)=DrawSymSt(1:DrawN)
        DrawSymStUO(1:DrawN)=DrawSymStU(1:DrawN)
        DrawColorO(1:DrawN)=DrawColor(1:DrawN)
        DrawParamO(1:DrawN)=DrawParam(1:DrawN)
      endif
      if(allocated(DrawAtom))
     1  deallocate(DrawAtom,DrawSymSt,DrawSymStU,
     2             DrawColor,DrawParam)
      n=DrawN+NPlus
      allocate(DrawAtom(4,n),DrawSymSt(n),DrawSymStU(n),DrawColor(n),
     1         DrawParam(n))
      if(DrawN.gt.0) then
        DrawAtom(1:4,1:DrawN)=DrawAtomO(1:4,1:DrawN)
        DrawSymSt(1:DrawN)=DrawSymStO(1:DrawN)
        DrawSymStU(1:DrawN)=DrawSymStUO(1:DrawN)
        DrawColor(1:DrawN)=DrawColorO(1:DrawN)
        DrawParam(1:DrawN)=DrawParamO(1:DrawN)
        deallocate(DrawAtomO,DrawSymStO,DrawSymStUO,DrawColorO,
     2             DrawParamO)
      endif
      DrawNMax=n
9999  return
      end

      subroutine ReallocateAtSplit(NPlus,MAtSplitMaxNew)
      use Atoms_mod
      integer, allocatable :: MAtSplitO(:),IAtSplitO(:,:)
      character*80, allocatable :: AtSplitO(:,:)
      if(NPlus.le.0.and.MAtSplitMaxNew.le.MAtSplitMax) go to 9999
      n=NAtSplit
      m=MAtSplitMax
      if(n.gt.0) then
        allocate(MAtSplitO(n),IAtSplitO(m,n),AtSplitO(m,n))
        MAtSplitO(1:n)=MAtSplit(1:n)
        do i=1,n
          do j=1,MAtSplit(i)
            IAtSplitO(j,i)=IAtSplit(j,i)
            AtSplitO(j,i)=AtSplit(j,i)
          enddo
        enddo
      endif
      if(allocated(MAtSplit)) deallocate(MAtSplit,IAtSplit,AtSplit)
      n=NAtSplitMax+NPlus
      m=MAtSplitMaxNew
      allocate(MAtSplit(n),IAtSplit(m,n),AtSplit(m,n))
      if(NAtSplit.gt.0) then
        MAtSplit(1:NAtSplit)=MAtSplitO(1:NAtSplit)
        do i=1,NAtSplit
          do j=1,MAtSplitO(i)
            IAtSplit(j,i)=IAtSplitO(j,i)
            AtSplit(j,i)=AtSplitO(j,i)
          enddo
        enddo
      endif
      NAtSplitMax=n
      MAtSplitMax=m
      if(allocated(MAtSplitO))
     1  deallocate(MAtSplitO,IAtSplitO,AtSplitO)
9999  return
      end

      subroutine ReallocateMolSplit(NPlus,MMolSplitMaxNew)
      use Molec_mod
      integer, allocatable :: MMolSplitO(:),IMolSplitO(:,:)
      character*80, allocatable :: MolSplitO(:,:)
      if(NPlus.le.0.and.MMolSplitMaxNew.le.MMolSplitMax) go to 9999
      n=NMolSplit
      m=MMolSplitMax
      if(n.gt.0) then
        allocate(MMolSplitO(n),IMolSplitO(m,n),MolSplitO(m,n))
        MMolSplitO(1:n)=MMolSplit(1:n)
        do i=1,n
          do j=1,MMolSplit(i)
            IMolSplitO(j,i)=IMolSplit(j,i)
            MolSplitO(j,i)=MolSplit(j,i)
          enddo
        enddo
      endif
      if(allocated(MMolSplit)) deallocate(MMolSplit,IMolSplit,MolSplit)
      n=NMolSplitMax+NPlus
      m=MMolSplitMaxNew
      allocate(MMolSplit(n),IMolSplit(m,n),MolSplit(m,n))
      if(NMolSplit.gt.0) then
        MMolSplit(1:NMolSplit)=MMolSplitO(1:NMolSplit)
        do i=1,NMolSplit
          do j=1,MMolSplitO(i)
            IMolSplit(j,i)=IMolSplitO(j,i)
            MolSplit(j,i)=MolSplitO(j,i)
          enddo
        enddo
      endif
      NMolSplitMax=n
      MMolSplitMax=m
      if(allocated(MMolSplitO))
     1  deallocate(MMolSplitO,IMolSplitO,MolSplitO)
9999  return
      end
